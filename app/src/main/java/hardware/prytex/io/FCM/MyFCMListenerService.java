package hardware.prytex.io.FCM;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import hardware.prytex.io.AgentChatActivity.UsersChatActivity;
import hardware.prytex.io.R;
import hardware.prytex.io.activity.DashBoardActivity;

import hardware.prytex.io.util.SPManager;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.util.Map;

import hardware.prytex.io.activity.AuthenticationActivity;


@SuppressWarnings("ALL")
public class MyFCMListenerService extends FirebaseMessagingService {

    private static final String[] TOPICS = {"global"};
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

//            String message = data.getString("message");
            Map<String,String> data = remoteMessage.getData();
//            Log.d(TAG, "From: " + from);
//            Log.d(TAG, "Message: " + message);

    String message = data.get("message");
    //    String message = remoteMessage.getNotification().getBody();
//            if (from.startsWith("/topics/")) {
//                // message received from some topic.
//            } else {
//                // normal downstream message.
//            }

            // [START_EXCLUDE]

        String type = data.get("notification_type");
        if(type != null && type.equalsIgnoreCase("chat_notification")){
            String title = data.get("title");
            String msg = data.get("message");

            Intent intent = new Intent(this, UsersChatActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);
            showNotification(this, title, msg, intent,pendingIntent);

        }else {
            sendNotification(message);
        }

        // [END_EXCLUDE]
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message) {

        AuthenticationActivity.alertsList.add(message);

        Intent intent = new Intent(this, DashBoardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        showNotification(this, message, intent);
    }

    public void showNotification(Context context, String title, String body, Intent intent,PendingIntent pendingIntent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = 0;
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body)
                .setPriority(Notification.PRIORITY_HIGH)
                .setDefaults(Notification.FLAG_AUTO_CANCEL | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
                .setContentIntent(pendingIntent);
        long[] pattern = {500, 500, 500};

        mBuilder.setVibrate(pattern);

        int count = 0;
        NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle();



        style.setBigContentTitle("Prytex Hardware");
        if (count < 1) {
            mBuilder.setContentText(body);
        } else {
            style.setSummaryText("You have " + count + " unseen alerts.");
        }

        mBuilder.setStyle(style);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        mBuilder.setContentIntent(resultPendingIntent);

        notificationManager.notify(notificationId, mBuilder.build());
    }

    private void showNotification(Context context, String body, Intent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = 0;
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.app_icon)
                .setContentTitle("Prytex Hardware")
                .setContentText(body);

        long[] pattern = {500, 500, 500};

        mBuilder.setVibrate(pattern);

        int count = 0;
        NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle();

        for (int i = 0; i < AuthenticationActivity.alertsList.size(); i++) {
            style.addLine(AuthenticationActivity.alertsList.get(i));

            count++;

        }


        style.setBigContentTitle("Prytex Hardware");
        if (count < 1) {
            mBuilder.setContentText(body);
        } else {
//            notificationBuilder.setContentText("You have " + count + " unseen d.moat alerts.");
//            notificationBuilder.setContentText(count + " new alerts");
            style.setSummaryText("You have " + count + " unseen alerts.");
        }

        mBuilder.setStyle(style);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        mBuilder.setContentIntent(resultPendingIntent);

        notificationManager.notify(notificationId, mBuilder.build());
    }

    public static void removeNotificaitonFromStatusBar(Context context, int id) {
        NotificationManager nMgr = (NotificationManager) context.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
//        nMgr.cancel(id);
        nMgr.cancelAll();
        AuthenticationActivity.alertsList.clear();

    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Log.d("Token", s);
        try {
            // sendRegistrationToServer(refreshedToken);
            SPManager.setDeviceToken(getApplicationContext(), s);
            subscribeTopics(s);
            sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, true).apply();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.e("NEW_TOKEN",s);
    }

    private void subscribeTopics(String token) throws IOException {
        for (String topic : TOPICS) {
            FirebaseMessaging.getInstance().subscribeToTopic("/topics/" + topic);
        }
    }
}