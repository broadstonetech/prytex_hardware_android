package hardware.prytex.io.chart_config;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

/**
 * Created by macbookpro13 on 15/08/2017.
 */

public class XaxisTempTimeScale implements IAxisValueFormatter
{

    private final DecimalFormat mFormat;

    public XaxisTempTimeScale() {
        mFormat = new DecimalFormat("###,###,###,##0");
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return mFormat.format(value) + "";
    }
}