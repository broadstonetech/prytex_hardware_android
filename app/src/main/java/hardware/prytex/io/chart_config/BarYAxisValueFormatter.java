package hardware.prytex.io.chart_config;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

/**
 * Created by macbookpro13 on 08/08/2017.
 */

public class BarYAxisValueFormatter implements IAxisValueFormatter
{

    private final DecimalFormat mFormat;

    public BarYAxisValueFormatter() {
        mFormat = new DecimalFormat("###,###,###,##0");
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return mFormat.format(value) + "";
    }
}
