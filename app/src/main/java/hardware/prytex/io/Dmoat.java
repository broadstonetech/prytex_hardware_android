package hardware.prytex.io;


import android.content.Context;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

//import com.crashlytics.android.Crashlytics;
import hardware.prytex.io.config.ZendeskAccountKey;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.util.MyRandom;
import com.zopim.android.sdk.api.ZopimChat;
import com.zopim.android.sdk.model.VisitorInfo;

//import io.fabric.sdk.android.Fabric;
import io.realm.Realm;


/**
 * Created by abubaker on 8/4/16.
 */

public class Dmoat extends MultiDexApplication {

    public static boolean isAppRunning = false;
    public static String APP_ID = "";
    public Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
//        Fabric.with(this, new Crashlytics());
        UserManager.initUserManager(getApplicationContext());

       // PubNubHandler.getInstance().init();

      //  RealmConfiguration realmConfig = new RealmConfiguration.Builder(this).deleteRealmIfMigrationNeeded().build();
  //      Realm.setDefaultConfiguration(realmConfig);
         Realm.init(this);
        ZopimChat.trackEvent("Application created");

        ZopimChat.init(ZendeskAccountKey.ACCOUNT_KEY);
        // clear visitor info. Visitor info storage can be disabled at chat initialization
        VisitorInfo emptyVisitorInfo = new VisitorInfo.Builder().build();
        ZopimChat.setVisitorInfo(emptyVisitorInfo);

    }

    public static String getAppId() {
        if (APP_ID == null) {
            APP_ID = "";
        }
        return APP_ID;
    }


    public static int getNewRequestId() {
        return new MyRandom().nextNonNegative();
    }
    public static int getNewRequestIdd() {
        return new MyRandom().nextNonNegative();
    }

    public static int getUniquePolicyId() {
        return new MyRandom().nextNonNegative();
    }
}
