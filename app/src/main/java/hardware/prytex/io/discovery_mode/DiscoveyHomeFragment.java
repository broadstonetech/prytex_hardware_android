package hardware.prytex.io.discovery_mode;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.SystemClock;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import hardware.prytex.io.R;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.fragment.LoginFragment;
import hardware.prytex.io.model.User;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.parser.BeaconRequest;
import hardware.prytex.io.util.GeneralUtils;
import com.zopim.android.sdk.api.ZopimChat;
import com.zopim.android.sdk.prechat.PreChatForm;
import com.zopim.android.sdk.prechat.ZopimChatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@SuppressWarnings("ALL")
public class DiscoveyHomeFragment extends BaseFragment implements View.OnClickListener {

    private Button btnDiscoverDevice;
    private String product_id;
    private String beacon_key;
    private String savedTime;
    private Calendar calendar;
    private SimpleDateFormat sdf;
    private JSONArray numList;
    private int secApi;
    private int numberCount = 0;
    private int retryAckCount;
    private int retryAuthCount;
    private boolean isFirstBeaconTry = true;
    private boolean isSecondNum = false;
    private boolean isThirdNum = false;
    private boolean retryAckUi = false;
    private boolean retryAuthUi = false;
    private SharedPreferences pref;
    private ProgressDialog dialog;
    private String userEmail = "";

    @Override
    public int getLayoutId() {
        return R.layout.fragment_discovey_home;
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);


        numberCount = 0;
        retryAckCount = 0;
        retryAuthCount = 0;
        String pattern = "yyyy-MM-dd HH:mm:ss";
        sdf = new SimpleDateFormat(pattern, Locale.getDefault());
        product_id = pref.getString("prodID", "565a-8535-3e49-4a3a-aee0-dlkf-40mb-fhjk");
        userEmail = pref.getString("userEmail", "abc@test.co");
        btnDiscoverDevice = parent.findViewById(R.id.btnDiscoverDevice);
        Button btnOpenChat = parent.findViewById(R.id.btnOpenChat);
        parent.findViewById(R.id.btnDiscoverDevice).setOnClickListener(this);
        User user = UserManager.getInstance().getLoggedInUser();
        Log.d("useremail", userEmail);

        btnOpenChat.setOnClickListener(v -> {
            try {
                openConversationWin(userEmail);
            } catch (Exception e) {
                Log.d("FreshchatException", e.toString());
            }
        });
//        successfulLogin(userEmail);
//show freshchat launcer/messanger
    }

    @Override
    public String getTitle() {
        return "Discover Prytex Hardware";
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnDiscoverDevice) {
            showOkAlert(getResources().getString(R.string.startDiscoveryMsg), true, false, false, false);
        }
    }

    private static String sha256() {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                hash = digest.digest("DMOAT FRAMING KEY".getBytes(StandardCharsets.UTF_8));
            }else{
                hash = digest.digest("DMOAT FRAMING KEY".getBytes(Charset.forName("UTF-8")));

            }
            StringBuilder hexString = new StringBuilder();

            for (byte b : hash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    //get current time and add 10 min
    private void getCurrentTime() {
        //get curent time. min+10 and start api's calling
        calendar = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//        formattedDate = df.format(calendar.getTime());
        // formattedDate have current date/time
        calendar.add(Calendar.MINUTE, 10);
        savedTime = df.format(calendar.getTime());
    }

    //if 200 returns namespace call this acknowledge api to inform server that namspace receiced
    private void callBeaconApi() {
        if (isFirstBeaconTry) {
            Log.d("Beacon", "Don't sleep for 1st beacon call");
            isFirstBeaconTry = false;
        } else {
            Log.d("Beacon", "sleep for 5 sec on every beacon call");
            SystemClock.sleep(5000);
        }

        if (checkTimeStatus()) {
            if (GeneralUtils.isConnected(getActivity())) {
                Api.beaconrequest(getActivity(), getBeaconParams(beacon_key, product_id), beaconListener);
            } else {
                showOkAlert(getResources().getString(R.string.internet_connection), false, true, false, false);
            }

        } else {
            showOkAlert(getResources().getString(R.string.time_out), false, false, true, false);
        }

    }

    private JSONObject getBeaconParams(String beaconKey, String prodId) {
        JSONObject js = new JSONObject();
        try {
            js.put("beacon_key", beaconKey);
            js.put("prod_id", prodId);
            js.put("app_mode", pref.getString("appMode", "prod"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }

    //listener
    private final AsyncTaskListener beaconListener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            Log.d("BeaconStatusCode", String.valueOf(result.code));
            if (result.code == 2 || result.code == 200) {
                btnDiscoverDevice.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                if (BeaconRequest.isNameSpace200) {
                    if (checkTimeStatus()) {
                        callAckUiApi();
                    } else {
                        showOkAlert(getResources().getString(R.string.time_out), false, false, true, false);
                    }
                } else if (BeaconRequest.isSuccessComplete) {
                    showOkAlert(  getResources().getString(R.string.succesDiscoveryMsg), false, false, false, true);

                } else {
                    numList = BeaconRequest.numListArray;
                    isSecondNum = false;
                    isThirdNum = false;
                    callNumListAuth(0);
                }

            } else {
                if (result.code == 4 || result.code == 400) {
                    if (checkTimeStatus()) {
                        callBeaconApi();
                    } else {
                        showOkAlert(getResources().getString(R.string.time_out), false, false, true, false);
                    }

                } else if (result.code > 500 && result.code < 600) {
                    showOkAlert(getResources().getString(R.string.serverOffText), false, true, false, false);
                } else {
//                    MyToast.showMessage(getActivity(), result.message);
                    callBeaconApi();
                }
            }
        }

    };

    //if 200 gives namespace call this acknowledge api to inform server that namspace receiced
    private void callAckUiApi() {
        if (GeneralUtils.isConnected(getActivity())) {
            Api.ackuiRequest(getActivity(), getAckUIParams(beacon_key, BeaconRequest.nameSpace), ackUiListener);
        } else {
            showOkAlert(getResources().getString(R.string.internet_connection), false, true, false, false);
        }
    }

    //get paramas for ack ui api
    private JSONObject getAckUIParams(String beaconKey, String nameSpace) {
        JSONObject js = new JSONObject();
        try {
            js.put("uikey", Api.ACCOUNT_CREATE_UI_KEY);
            js.put("beacon_key", beaconKey);
            js.put("namespace", nameSpace);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;

    }


    //listener
    private final AsyncTaskListener ackUiListener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (!isAdded()) {
                return;
            }
            if (result.code == 2 || result.code == 200) {
                callBeaconApi();
            } else {
                if (result.code == 4 || result.code == 400) {
                    //if failed call again beacon api if time<10 min
                    if (checkTimeStatus()) {
                        callBeaconApi();
                    } else {
                        showOkAlert(getResources().getString(R.string.time_out), false, false, true, false);
                    }

                } else if (result.code >= 5 && result.code <= 6) {
//                    Log.d("DM", "ack response b/w 500-600 retry 5 times");
                    if (retryAckUi) {
                        while (retryAckCount < 5) {
//                            Log.d("DM", "retrying ack ui 5 times");
                            callAckUiApi();
                            retryAckCount++;
                        }
                    }
                } else {
                    showOkAlert(result.message, false, true, false, false);
                }
            }
        }
    };

    //
    private void callNumListAuth(int num) {
        try {
            secApi = (int) numList.get(num);
            Log.d("Beacon Auth SecApi", String.valueOf(secApi));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (checkTimeStatus()) {
            callNumList(secApi);
        } else {
            showOkAlert(getResources().getString(R.string.time_out), false, false, true, false);
        }

    }

    //make a numlist for list size(3)
    private void callNumList(int n) {
        int secMs = n * 1000;
        SystemClock.sleep(secMs);
        if (GeneralUtils.isConnected(getActivity())) {
            callAuthApi();
        } else {
            showOkAlert(getResources().getString(R.string.internet_connection), false, true, false, false);
        }

    }

    //authetication api call
    private void callAuthApi() {

        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            js.put("uikey", Api.ACCOUNT_CREATE_UI_KEY);

            data.put("req_mode", "UI");
            data.put("req_time", String.valueOf(secApi));
            data.put("key", BeaconRequest.KeyThirtyTwo);
            js.put("data", data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        showProgressDialog(true);
        Api.authrequest(getActivity(), js, authCallback);
    }

    //listener call back for auth
    //listener
    private final AsyncTaskListener authCallback = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (!isAdded()) {
                return;
            }
            if (result.code == 2) {
                //again call back beacon api to get next expected response (num list)
                // and so on for all three . if success move to beacon else exit from discovery with error
                numberCount++;
                if (!(isSecondNum)) {
                    isSecondNum = true;
                    callNumListAuth(1);
                } else if (!(isThirdNum)) {
                    isThirdNum = true;
                    callNumListAuth(2);
                    retryAuthUi = true;
                } else {
                    //all three completed back to beacon.to get success>true
                    callBeaconApi();
                }

            } else {
                if (result.code == 4) {
                    //if failed call again beacon api if time<10 min
                    if (checkTimeStatus()) {
                        callBeaconApi();
                    } else {
                        showOkAlert(getResources().getString(R.string.time_out), false, false, true, false);

                    }
                } else if (result.code >= 5 && result.code <= 6) {
                    if (!(retryAuthUi)) {
                        while (retryAuthCount < 2) {
//                            Log.d("DM", "AUTH retry 2 times");
                            callAckUiApi();
                            retryAuthCount++;
                        }
                    }
                } else {
                    showOkAlert(result.message, false, true, false, false);
                }
            }
        }
    };

    private boolean checkTimeStatus() {
        Date date1 = null;
        Date date2 = null;
        calendar = Calendar.getInstance();
        try {
            date1 = sdf.parse(savedTime);

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String formatedDate = df.format(calendar.getTime());
            date2 = sdf.parse(formatedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date2.before(date1)) {
            Log.d("discover time", "time remaining" + date1.toString() +"===" + date2.toString());
            return true;
        }
        return false;
    }


    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "Discovering Prytex Hardware", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private void resetAllBool() {

        isFirstBeaconTry = true;
        isSecondNum = false;
        isThirdNum = false;
        retryAckUi = false;
        retryAuthUi = false;
        BeaconRequest.isSuccessComplete = false;
        BeaconRequest.isMessage200 = false;
        BeaconRequest.isNameSpace200 = false;
        BeaconRequest.isSuccess200 = false;
    }


    private void showOkAlert(String message, final Boolean startDiscovering, final Boolean failedDiscovery, final Boolean requestTimeOut, final Boolean succesDiscovery) {

        if (TextUtils.isEmpty(message) || message.equalsIgnoreCase("false")){
            message = getActivity().getResources().getString(R.string.discovery_incomplete);
        }

        btnDiscoverDevice.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        btnDiscoverDevice.setEnabled(true);
        showProgressDialog(false);
        resetAllBool();
       // SoundPool sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        SoundPool sp;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            sp = new SoundPool.Builder()
                    .setMaxStreams(10)
                    .build();
        }else{
            sp = new SoundPool(10, AudioManager.STREAM_MUSIC, 1);
        }

        int iTmp = sp.load(getContext(), R.raw.discovery_notification, 1); // in 2nd param u have to pass your desire ringtone

        sp.play(iTmp, 1, 1, 0, 0, 1);

        //MediaPlayer mPlayer = MediaPlayer.create(getContext(), R.raw.discovery_notification); // in 2nd param u have to pass your desire ringtone
        //mPlayer.prepare();
        //mPlayer.start();
        new AlertDialog.Builder(getContext())
                .setTitle("Prytex Hardware")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", (dialog, whichButton) -> {
                    if (startDiscovering) {
                        BeaconRequest.isSuccessComplete = false;
                        BeaconRequest.isMessage200 = false;
                        BeaconRequest.isNameSpace200 = false;
                        BeaconRequest.isSuccess200 = false;
                        startDiscovery();
                    }
                    if (failedDiscovery) {
                        dialog.dismiss();
                        //error from server failed
                    }
                    if (requestTimeOut) {
                        dialog.dismiss();
                        //10 min timeed out
                    }
                    if (succesDiscovery) {

                        getHelper().replaceFragment(new LoginFragment(), R.id.fragment_container, true, false);
                        //every thing success, show message and go to login
                    }
                }).show();

    }

    private void startDiscovery() {
        btnDiscoverDevice.setBackgroundColor(getResources().getColor(R.color.colorBtnPressed));
        btnDiscoverDevice.setEnabled(false);
        showProgressDialog(true);

        Log.d("DM", "Discovering Device.." + product_id);
        String beacon_text = "DMOAT FRAMING KEY";
        beacon_key = sha256();

        if (GeneralUtils.isConnected(getActivity())) {
            getCurrentTime();
            if (checkTimeStatus()) {
                callBeaconApi();
            } else {
                showOkAlert(getResources().getString(R.string.time_out), false, false, true, false);
            }

        } else {
            showOkAlert(getResources().getString(R.string.internet_connection), false, true, false, false);
        }
    }


    private void openConversationWin(String userEmail) {

        // set pre chat fields as mandatory
        PreChatForm preChatForm = new PreChatForm.Builder()
                .name(PreChatForm.Field.REQUIRED_EDITABLE)
                .email(PreChatForm.Field.REQUIRED_EDITABLE)
                .phoneNumber(PreChatForm.Field.OPTIONAL_EDITABLE)
                .department(PreChatForm.Field.REQUIRED_EDITABLE)
                .message(PreChatForm.Field.REQUIRED_EDITABLE)
                .build();
//        startActivity(new Intent(getContext(), ZopimChatActivity.class));


//         build chat config
        ZopimChat.SessionConfig config = new ZopimChat.SessionConfig().preChatForm(preChatForm);

        // start chat activity with config
        ZopimChatActivity.startActivity(getContext(), config);

        // Sample breadcrumb
        ZopimChat.trackEvent("Started chat with mandatory pre-chat form");
    }

}
