package hardware.prytex.io.AgentChatActivity;


import android.os.Bundle;
import android.os.Handler;

import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AlertDialog;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import java.util.ArrayList;
import java.util.Collections;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hardware.prytex.io.AgentChatActivity.ChatApi.GetUserMessagesModelCLass;
import hardware.prytex.io.AgentChatActivity.ChatApi.LiveSupport;
import hardware.prytex.io.AgentChatActivity.model.Message;
import hardware.prytex.io.AgentChatActivity.model.User;
import hardware.prytex.io.R;
import hardware.prytex.io.activity.BaseActivity;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;

import static hardware.prytex.io.AgentChatActivity.ChatApi.GetUserMessagesParser.getUserChatObj;
import static hardware.prytex.io.AgentChatActivity.ChatApi.PushUserChatParser.pushUserMsgObj;
import static hardware.prytex.io.AgentChatActivity.ChatApi.StartChatParser.startChatObj;


public class UsersChatActivity extends BaseActivity {

    MessagesListAdapter<Message> adapter;
    final FirebaseFirestore db = FirebaseFirestore.getInstance();
    TextView tvEndChat;
    private String agentID = "agent2489";
    private String documentId="snajnsnan";
    private boolean isUserExist = false;
    private String msg;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MessageInput messageInput = findViewById(R.id.input);
        MessagesList messagesList = findViewById(R.id.messagesList);
        tvEndChat = findViewById(R.id.tvEndChat);


        messageInput.setInputListener(input -> {
            msg = input.toString().trim();
            sendMessageTofireBase(input.toString().trim());
            return true;
        });


        tvEndChat.setOnClickListener(v -> endChatDialog());

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        String mCurrentUserId = UserManager.getInstance().getLoggedInUser().nameSpace;

        ImageLoader imageLoader = (imageView, url, payload) -> Picasso.get().load(url).into(imageView);
        adapter = new MessagesListAdapter<>(mCurrentUserId, imageLoader);
        messagesList.setAdapter(adapter);

    //    checkAndInsertUserinFireBase();

        loadMessages();

        final Handler ha=new Handler();
        ha.postDelayed(new Runnable() {

            @Override
            public void run() {
                loadFirebaseMessages();
                ha.postDelayed(this, 10000);
            }
        }, 10000);

    }

    @Override
    public int getLayoutId() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return R.layout.activity_default_messages;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void endChatDialog() {
        AlertDialog.Builder optionDialog = new AlertDialog.Builder(UsersChatActivity.this);

        optionDialog.setTitle(getResources().getString(R.string.app_name));
        optionDialog.setMessage("Are you sure you want to exit?");

        // Specifying a listener allows you to take an action before dismissing the dialog.
        // The dialog is automatically dismissed when a dialog button is clicked.
        optionDialog.setPositiveButton(android.R.string.yes, (dialog, which) -> {
            showProgressDialog(true);
            LiveSupport.endChat(UsersChatActivity.this,documentId, endChatListner);
        })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, (dialog, which) -> {

                })
                .show();
    }

    private void addMessages(List<Message> baseMessages) {
        adapter.addToEnd(baseMessages, true);
    }

    private void addMessage1(String textMessage) {
        adapter.addToStart(new Message(getMessageUser(), textMessage), true);
    }

    private User getMessageUser() {
        return new User(UserManager.getInstance().getLoggedInUser().nameSpace, UserManager.getInstance().getLoggedInUser().email, true);
    }


    private static User getUser(GetUserMessagesModelCLass.Data.Chat.Agent obj) {
        return new User(obj.getSender_id(), obj.getSender_name(), true);
    }

    private void sendMessageTofireBase(String message) {

        LiveSupport.pushChat(UsersChatActivity.this,documentId,message,UserManager.getInstance().getLoggedInUser().nameSpace,UserManager.getInstance().getLoggedInUser().name,pushMsglistner);
//        Map messageMap = new HashMap();
//        messageMap.put("content", message);
//        messageMap.put("sender_name", UserManager.getInstance().getLoggedInUser().name);
//        messageMap.put("created", System.currentTimeMillis()/1000L);
//        messageMap.put("sender_id", mCurrentUserId);
//
//        db.collection("channels").document(documentId).collection("thread").add(messageMap)
//                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                    @Override
//                    public void onSuccess(DocumentReference documentReference) {
//
//                        //   Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
//                    }
//                })
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        //  Log.w(TAG, "Error adding document", e);
//                    }
//                });
    }

    private void loadFirebaseMessages() {
        LiveSupport.getUserChat(UsersChatActivity.this,documentId,getUserChatListner);
//            db.collection("channels").document(documentId).collection("thread")
//                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
//                        @Override
//                        public void onEvent(@Nullable QuerySnapshot documentSnapshots, @Nullable FirebaseFirestoreException error) {
//
//                            if (documentSnapshots.isEmpty()) {
//
//                                return;
//                            } else {
//                                if (documentSnapshots.getDocumentChanges() != null) {
//                                    adapter.clear();
//                                    adapter.notifyDataSetChanged();
//                                    List<MessageFromFirebase> downloadInfoList = documentSnapshots.toObjects(MessageFromFirebase.class);
//
//                                    Collections.sort(downloadInfoList, new Comparator<MessageFromFirebase>() {
//                                        public int compare(MessageFromFirebase obj1, MessageFromFirebase obj2) {
//                                            return obj1.getCreated().compareTo(obj2.getCreated());
//                                        }
//                                    });
//
//                                    List<Message> messageWrappers = new ArrayList<>();
//                                    for (MessageFromFirebase message : downloadInfoList) {
//                                        messageWrappers.add(new Message(getUser(message), message.getContent(),message.getCreated()));
//
//                                    }
//
//                                    addMessages(messageWrappers);
//
//                                }
//                            }
//                        }
//                    });
    }

    private void loadMessages(){
        LiveSupport.startChat(UsersChatActivity.this,UserManager.getInstance().getLoggedInUser().name,
                UserManager.getInstance().getLoggedInUser().name,
                UserManager.getInstance().getLoggedInUser().email,
                UserManager.getInstance().getLoggedInUser().nameSpace, documentId, agentID, "support@prytex.io", startChatListner);
    }

    final AsyncTaskListener startChatListner = result -> {
        showProgressDialog(false);
        if(startChatObj.isStatus()) {
            agentID = startChatObj.getData().getReciever_info().getReciever_id();
            documentId = startChatObj.getData().getFirebase_id();
            loadFirebaseMessages();
        }else {
            Toast.makeText(UsersChatActivity.this,"All support officer are busy please try again after sometime !",Toast.LENGTH_LONG).show();
            finish();
        }
    };

    final AsyncTaskListener endChatListner = result -> {
        showProgressDialog(false);
        finish();
//            if (endChatObj.getMessage().equalsIgnoreCase("success")) {
//                deleteDataFromFirebase();
//            }
    };

    final AsyncTaskListener getUserChatListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if(getUserChatObj.isStatus()){
                adapter.clear();
                adapter.notifyDataSetChanged();
                List<GetUserMessagesModelCLass.Data.Chat.Agent> chatList = new ArrayList<>();
                chatList.clear();
                chatList.addAll(getUserChatObj.getData().getChat().getAgent());
                chatList.addAll(getUserChatObj.getData().getChat().getClient());
                Collections.sort(chatList, (obj1, obj2) -> obj1.getCreated().compareTo(obj2.getCreated()));


                List<Message> messageWrappers = new ArrayList<>();

                messageWrappers.clear();
                for (GetUserMessagesModelCLass.Data.Chat.Agent  message : chatList) {
                    if(message.getCreated() != 0L)
                        messageWrappers.add(new Message(getUser(message), message.getContent(),message.getCreated()));

                }
                addMessages(messageWrappers);
            }else {
                Toast.makeText(UsersChatActivity.this,"Tesingt",Toast.LENGTH_LONG).show();
            }
        }
    };

    final AsyncTaskListener pushMsglistner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if(result.code == 200 || result.code == 2) {
                if (pushUserMsgObj.isStatus()) {
                    addMessage1(msg);
                    Toast.makeText(UsersChatActivity.this,"Message sent",Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(UsersChatActivity.this,"Message not sent",Toast.LENGTH_LONG).show();
                }
            }else {
                Toast.makeText(UsersChatActivity.this,"Something went wrong Please try again !",Toast.LENGTH_LONG).show();
            }
        }
    };

}
