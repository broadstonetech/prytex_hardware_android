package hardware.prytex.io.AgentChatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import hardware.prytex.io.R;
import hardware.prytex.io.activity.BaseActivity;
import hardware.prytex.io.activity.DashBoardActivity;
import hardware.prytex.io.activity.VideoViewActivity;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.fragment.FaqsFragment;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.util.GeneralUtils;
import hardware.prytex.io.util.SPManager;

import static hardware.prytex.io.activity.DashBoardActivity.llBottomTabs;

@SuppressWarnings("ALL")
public class HelpTutorialFragment extends BaseFragment implements View.OnClickListener {

    FirebaseAuth mauth;
    private Intent intent;
    ProgressDialog progressDialog;
    private DatabaseReference mDatabase;

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        TextView tvWatchCompleteTutorial = parent.findViewById(R.id.tvWatchCompleteTutorial);
        TextView tvWatchDashboardTutorial = parent.findViewById(R.id.tvWatchDashboardTutorial);
        TextView tvWatchAlertsTutorial = parent.findViewById(R.id.tvWatchAlertsTutorial);
        TextView tvWatchCnctdDevicesTutorial = parent.findViewById(R.id.tvWatchCnctdDevicesTutorial);
        TextView tvWatchNetworkUsgTutorial = parent.findViewById(R.id.tvWatchNetworkUsageTutorial);
        TextView tvWatchSensorDataTutorial = parent.findViewById(R.id.tvWatchSensorDataTutorial);
        TextView tvWatchProfileTutorial = parent.findViewById(R.id.tvWatchProfileTutorial);
        TextView tvWatchPolicyTutorial = parent.findViewById(R.id.tvWatchPolicyTutorial);
        TextView tvWatchAccountSetupTutorial = parent.findViewById(R.id.tvWatchAccountSetupTutorial);

        TextView tvReadFeatureGuideDocument = parent.findViewById(R.id.tvReadHelpGuide);
        TextView tvReadQuickStartDocument = parent.findViewById(R.id.tvReadquickStartGuide);

        ImageView imgBtnDmoatweb = parent.findViewById(R.id.imgDmoatWeb);
        ImageView imgBtnDmoatFb = parent.findViewById(R.id.imgDmoatFb);
        ImageView imgBtnDmoatTwitter = parent.findViewById(R.id.imgDmoatTwitter);
        ImageView imgBtnDmoatInsta = parent.findViewById(R.id.imgDmoatInstagram);


        LinearLayout llFaqs = parent.findViewById(R.id.llFaqs);
        LinearLayout llLiveChat = parent.findViewById(R.id.llLiveSupport);

        llBottomTabs.setVisibility(View.VISIBLE);
        imgBtnDmoatweb.setOnClickListener(this);
        imgBtnDmoatFb.setOnClickListener(this);
        imgBtnDmoatTwitter.setOnClickListener(this);
        imgBtnDmoatInsta.setOnClickListener(this);
        tvWatchCompleteTutorial.setOnClickListener(this);
        tvWatchDashboardTutorial.setOnClickListener(this);
        tvWatchAlertsTutorial.setOnClickListener(this);
        tvWatchCnctdDevicesTutorial.setOnClickListener(this);
        tvWatchSensorDataTutorial.setOnClickListener(this);
        tvWatchNetworkUsgTutorial.setOnClickListener(this);
        tvWatchProfileTutorial.setOnClickListener(this);
        tvWatchPolicyTutorial.setOnClickListener(this);
        tvWatchAccountSetupTutorial.setOnClickListener(this);
        tvReadFeatureGuideDocument.setOnClickListener(this);
        tvReadQuickStartDocument.setOnClickListener(this);

        intent = new Intent(getContext(), VideoViewActivity.class);

        llFaqs.setOnClickListener(this);
        llLiveChat.setOnClickListener(this);
        DashBoardActivity.btnHomeDashboard.setOnClickListener(v -> {
            try {
                getActivity().finish();
                getActivity().getIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivityForResult(getActivity().getIntent(), 0);
                getActivity().overridePendingTransition(0, 0);

            } catch (Exception e) {
                Log.e("DashboardApiException", e.toString());
            }
        });

        mauth=FirebaseAuth.getInstance();
        mDatabase= FirebaseDatabase.getInstance().getReference().child("users");
    }

    @Override
    public String getTitle() {
        return "Help & Tutorials";
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_help_tutorial;
    }

    @Override
    public void onClick(View v) {

        GeneralUtils.ScaleInAnimation(getActivity(),v);

        switch (v.getId()) {
            case R.id.imgDmoatWeb:
                openUrl(Api.DMOAT_WEB_URL);
                break;
            case R.id.imgDmoatFb:
                openUrl(Api.DMOAT_FACEBOOK_URL);
                break;
            case R.id.imgDmoatTwitter:
                openUrl(Api.DMOAT_TWITTER_URL);
                break;
            case R.id.imgDmoatInstagram:
                openUrl(Api.DMOAT_INSTAGRAM_URL);
                break;
            case R.id.tvWatchCompleteTutorial:
                intent.putExtra("video_url", Api.video_full_url);
                getContext().startActivity(intent);
                break;
            case R.id.tvWatchDashboardTutorial:
                intent.putExtra("video_url", Api.video_dashboard_url);
                getContext().startActivity(intent);
                break;
            case R.id.tvWatchAlertsTutorial:
                intent.putExtra("video_url", Api.video_dmoat_alerts_url);
                getContext().startActivity(intent);
                break;
            case R.id.tvWatchCnctdDevicesTutorial:
                intent.putExtra("video_url", Api.video_connected_devices_url);
                getContext().startActivity(intent);
                break;
            case R.id.tvWatchNetworkUsageTutorial:
                intent.putExtra("video_url", Api.video_network_usage_url);
                getContext().startActivity(intent);
                break;
            case R.id.tvWatchSensorDataTutorial:
                intent.putExtra("video_url", Api.video_sensors_data_url);
                getContext().startActivity(intent);
                break;
            case R.id.tvWatchProfileTutorial:
                intent.putExtra("video_url", Api.video_profile_settings_url);
                getContext().startActivity(intent);
                break;
            case R.id.tvWatchPolicyTutorial:
                intent.putExtra("video_url", Api.video_policy_url);
                getContext().startActivity(intent);
                break;

            case R.id.tvReadHelpGuide:
                openUrl(Api.dmoatFeatureGuideDocURL);
                break;

            case R.id.tvReadquickStartGuide:
                openUrl(Api.dmoatQuickStartDocURL);
                //GeneralUtils.FadeInAnimation(getActivity(),v);
                break;
            case R.id.llFaqs:
                //GeneralUtils.rotationAnimation(v);
                getHelper().addFragment(new FaqsFragment(), true, true);
                break;
            case R.id.llLiveSupport:
//                Intent myIntent = new Intent(getActivity(), ZopimChatActivity.class);
//                getActivity().startActivity(myIntent);
//
                ((BaseActivity)getActivity()).showProgressDialog(true);
                checkUser();
                break;
        }
    }

    private void openUrl(String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    private void checkUser(){
//        mDatabase.orderByChild("email").equalTo(UserManager.getInstance().getLoggedInUser().email).addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot snapshot) {
//                if(snapshot.exists()){
//                    loginUser(UserManager.getInstance().getLoggedInUser().email,"123456789");
//                }else {
//                    registerUser(UserManager.getInstance().getLoggedInUser().email,"123456789");
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//
//            }
//        });
        ((BaseActivity)getActivity()).showProgressDialog(false);
        Intent intent=new Intent(getActivity(),UsersChatActivity.class);
        startActivity(intent);
    }
    private void registerUser( String email, String password) {

        mauth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(getActivity(),new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                //------IF USER IS SUCCESSFULLY REGISTERED-----
                if(task.isSuccessful()){

                    FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
                    final String uid=current_user.getUid();

                    Map userMap=new HashMap();
                    userMap.put("device_token", SPManager.getDeviceToken(getActivity()));
                    userMap.put("name", UserManager.getInstance().getLoggedInUser().name + "\'s Prytex Hardware"+"new user");
                    userMap.put("online","true");
                    userMap.put("email",email);
                    userMap.put("id",mauth.getCurrentUser().getUid());

                    mDatabase.child(uid).setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task1) {
                            if(task1.isSuccessful()){

                                loginUser(email,"123456789");



                            }
                            else{

                                Toast.makeText(getActivity(), "Please try again ", Toast.LENGTH_SHORT).show();

                            }

                        }
                    });


                }
                //---ERROR IN ACCOUNT CREATING OF NEW USER---
                else{
//                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "ERROR REGISTERING USER....", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void loginUser(String email, String password) {

        //---SIGN IN FOR THE AUTHENTICATE EMAIL-----
        mauth.signInWithEmailAndPassword(email,password).addOnCompleteListener(Objects.requireNonNull(getActivity()),
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {


                        if(task.isSuccessful()){

                            //---ADDING DEVICE TOKEN ID AND SET ONLINE TO BE TRUE---
                            //---DEVICE TOKEN IS USED FOR SENDING NOTIFICATION----
                            String user_id=mauth.getCurrentUser().getUid();
                            Map addValue = new HashMap();
                            addValue.put("device_token",SPManager.getDeviceToken(getActivity()));
                            addValue.put("online","true");

                            //---IF UPDATE IS SUCCESSFULL , THEN OPEN MAIN ACTIVITY---
                            mDatabase.child(user_id).updateChildren(addValue, new DatabaseReference.CompletionListener(){

                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                    if(databaseError==null){
                                        ((BaseActivity)getActivity()).showProgressDialog(false);
                                        Intent intent=new Intent(getActivity(),UsersChatActivity.class);
                                        startActivity(intent);
                                        // move to chat activity
                                    }
                                    else{
                                        Toast.makeText(getActivity(), databaseError.toString()  , Toast.LENGTH_SHORT).show();
                                        Log.e("Error is : ",databaseError.toString());

                                    }
                                }
                            });



                        }
                        else{
                            //---IF AUTHENTICATION IS WRONG----
                            Toast.makeText(getActivity(), "Try Again" +
                                    "", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
