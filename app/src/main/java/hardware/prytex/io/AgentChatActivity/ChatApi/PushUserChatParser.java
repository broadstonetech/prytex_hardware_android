package hardware.prytex.io.AgentChatActivity.ChatApi;


import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;


public class PushUserChatParser implements BaseParser {
    public static PushChaModelClass pushUserMsgObj = new PushChaModelClass();
    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        result.code = httpCode;
        try {
            JSONObject jsonResponse = new JSONObject(response);
            result.code = httpCode;
            if (httpCode == 200 || httpCode == 2) {
                result.setData(jsonResponse);
                result.success(true);

                Gson gson = new Gson();
                pushUserMsgObj =  gson.fromJson(String.valueOf(jsonResponse), PushChaModelClass.class);
            }else if (httpCode == 400){
                Gson gson = new Gson();
            }
        }catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }
}
