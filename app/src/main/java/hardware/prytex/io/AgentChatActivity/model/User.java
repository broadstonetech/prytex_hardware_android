package hardware.prytex.io.AgentChatActivity.model;

import com.stfalcon.chatkit.commons.models.IUser;

/*
 * Created by troy379 on 04.04.17.
 */
public class User implements IUser {

    private final String id;
    private final String name;
    private String avatar;
    private final boolean online;

    public User(String id, String name, boolean online) {
        this.id = id;
        this.name = name;
        this.online = online;
    }
    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getAvatar() {
        return avatar;
    }

}
