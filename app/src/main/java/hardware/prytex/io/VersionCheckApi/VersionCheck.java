package hardware.prytex.io.VersionCheckApi;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.HttpAsyncRequest;

import static hardware.prytex.io.VersionCheckApi.CheckEnvirenmentParser.environemntObj;


public class VersionCheck {

    private static PackageInfo pInfo;

    public static void getVersionCheck(Context context, AsyncTaskListener callback){
        versionCheck(context,getParams(context),callback);
    }

    public static void getEnvironMent(Context context, AsyncTaskListener callback){

        getEnvironment(context,checkEnvironmentParams(),callback);
    }


    public static void versionCheck(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        String url = "https://api.parsl.io/check/version";
        HttpAsyncRequest req = new HttpAsyncRequest(context, url,
                HttpAsyncRequest.RequestType.JSONDATA, new VersionCheckParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getEnvironment(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        String urlCheckEnvironment = "https://api.parsl.io/change/environment";
        HttpAsyncRequest req = new HttpAsyncRequest(context, urlCheckEnvironment,
                HttpAsyncRequest.RequestType.JSONDATA, new CheckEnvirenmentParser(), callback);
        req.setJsonData(data);
        req.execute();
    }


    private static JSONObject getParams(Context con) {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            pInfo = con.getPackageManager().getPackageInfo(con.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        try {
            data.put("app_id", "prytex_hw");
            data.put("platform", "android");
            data.put("version_no", pInfo.versionName);
            data.put("environment", environemntObj.getData().getEnvironment());
            data.put("build_no", pInfo.versionCode);
            js.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;
    }

    private static JSONObject checkEnvironmentParams() {

        return new JSONObject();
    }
}
