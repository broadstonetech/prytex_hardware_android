package hardware.prytex.io.config;

import android.annotation.SuppressLint;
import android.app.Activity;

/**
 * Created by abubaker on 5/17/16.
 */
public class Constants {

    public static final String PREFS_NAME = "app_prefs";
    public static final String APP_ID_PREFS_NAME = "app_id_prefs";
    public static final String SETTINGS_PREFS_NAME = "app_settings_prefs";
    public static final String TAG = "bottom_sheet";
    //public static final String PATTERN_PASSWORD = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[~`!@#$%^&*()-_+={}[]|\\/:;\"'<>,.?)(?=\\S+$).{8,}";
    public static final String PATTERN_PASSWORD = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&*;:|?.,()-_{}[]'/><+=!])(?=\\S+$).{4,}$";

    public static final String FCM_TOKEN = "";
    @SuppressLint("StaticFieldLeak")
    public static Activity activityContext = null;
    public static boolean isFromDashboardFragment = false;

    @SuppressLint("StaticFieldLeak")
    public static  Activity dashBoardActivity = null;

    public static final String DMOAT_REBOOT_SUCCESS = "Request has been forwarded to Prytex Hardware";
    public static final String DMOAT_REBOOT_ERROR = "Error rebooting Prytex Hardware";
    public static final String DMOAT_RESET_SUCCESS = "Prytex Hardware changed its settings to factory defaults.";
}


