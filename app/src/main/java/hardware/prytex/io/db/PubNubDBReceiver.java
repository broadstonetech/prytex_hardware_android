//package com.prytex.io.db;
//
//import android.io.NotificationManager;
//import android.io.PendingIntent;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Color;
//import android.media.RingtoneManager;
//import android.os.Bundle;
//import androidx.core.io.NotificationCompat;
//import android.util.Log;
//
//import com.prytex.io.R;
//import com.prytex.io.activity.DashBoardActivity;
//import com.prytex.io.model.DMoatAlert;
//import com.prytex.io.model.UserManager;
//import com.prytex.io.network.L;
//
//import java.util.List;
//import java.util.Random;
//
//public class PubNubDBReceiver extends BroadcastReceiver {
//
//    public final static String GROUP_KEY_NOTIFICATIONS = "group_key_notifications";
//    public final static int GROUP_NOTIFICATION_ID = 007;
//
//    public PubNubDBReceiver() {
//
//    }
//
//    @Override
//    public void onReceive(Context context, Intent intent) {
//
//        String actionChannel = intent.getAction();
//        Bundle extras = intent.getExtras();
//        if (extras == null || !extras.containsKey("data")) {
//            return;
//        }
//
//        PubNubResult result = (PubNubResult) extras.get("data");
//
////        if (result.mAlert.channel != null &&
////                (result.mAlert.channel.startsWith(PubNubDataHandler.CHANNEL_INFO))) {
////
////            if (result.mAlert.recordId == null || result.mAlert.recordId.length() == 0) {
////                result.mAlert.recordId = new Random().nextInt() + "";
//////                L.d("Alert_id assigned to " + result.mAlert.channel);
////            }
////
////        }
////
////        if (result.mAlert.channel != null &&
////                result.mAlert.channel.startsWith(PubNubDataHandler.CHANNEL_INFO)) {
////
//////            result.mAlert.setIsVisited(1);
////
////        }
//
////        if (result.mAlert.recordId != null && result.mAlert.recordId.length() > 0) {
//        if (result.mAlert.description != null) {
//            // Saving alert into DB
//
//            if (!UserManager.getInstance().isUserLoggedIn()) {
//                Log.d("User", "Not logged In");
//                return;
//            }
//
////            if (result.mAlert.is24HoursOldAlert()) {
////                Log.d("Alert", "delete " + result.mAlert.eveSec);
////                return;
////            }
////            DBClient.getInstance().saveNewAlert(result.mAlert);
//
////            if (!ViewAlertsAdvancedFragment.isViewingAlerts) {
////                SharedPreferences prefs = context.getSharedPreferences(Constants.SETTINGS_PREFS_NAME, Context
////                        .MODE_PRIVATE);
////                boolean muteAll = prefs.getBoolean(appendUser("mute_all"), false);
////                if (!muteAll) {
////                    showNotification(context, result.mAlert);
////                } else {
////                    Log.d("Mute All", "muted");
////                }
////            }
//        } else {
//            L.d("Ignoring DB transaction coz no description was set");
//        }
//
//    }
//
//    public static void showNotification(Context context, DMoatAlert alert) {
//        NotificationCompat.Builder mBuilder =
//                new NotificationCompat.Builder(context)
//                        .setSmallIcon(R.drawable.logo_notifications)
//                        .setContentTitle("Prytex Hardware")
//                        .setContentText(alert.description)
////                        .setGroup(PubNubDBReceiver.GROUP_KEY_NOTIFICATIONS)
////                        .setGroupSummary(true)
//                        .setLights(Color.BLUE, 500, 2000)
//                        .setAutoCancel(true).setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
//        long[] pattern = {500, 500, 500};
//        mBuilder.setVibrate(pattern);
//
//        List<DMoatAlert> alerts = DBClient.getInstance().getAllAlerts();
//        int count = 0;
//        NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle();
//        for (DMoatAlert notification : alerts) {
//            if (!notification.isVistedByUser()) {
//                style.addLine(notification.getDescription());
//                count++;
//            }
//        }
//        if (count == 1) {
//            mBuilder.setContentText(alert.description);
//        } else {
//            mBuilder.setContentText(count + " new alerts");
//        }
//
//        style.setBigContentTitle("Prytex Hardware");
//        style.setSummaryText("You have " + count + " unseen alerts.");
//        mBuilder.setStyle(style);
//
//        // Creates an explicit intent for an Activity in your io
//        Intent resultIntent = new Intent(context, DashBoardActivity.class);
//        resultIntent.putExtra("alert", alert.recordId);
//
//        PendingIntent resultPendingIntent =
//                PendingIntent.getActivity(
//                        context,
//                        0,
//                        resultIntent,
//                        PendingIntent.FLAG_UPDATE_CURRENT
//                );
//
//        mBuilder.setContentIntent(resultPendingIntent);
//        NotificationManager mNotificationManager =
//                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        // mId allows you to update the notification later on.
//        mNotificationManager.notify(GROUP_NOTIFICATION_ID, mBuilder.build());
//
//
//    }
//
//    public static void removeNotificaitonFromStatusBar(Context context, int id) {
//        NotificationManager nMgr = (NotificationManager) context.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
////        nMgr.cancel(id);
//        nMgr.cancelAll();
//        DBClient.getInstance().deleteAllAlerts();
//    }
//
//
//    private String appendUser(String key) {
//        return key + "_" + UserManager.getInstance().getLoggedInUser().userName;
//    }
//
//}
