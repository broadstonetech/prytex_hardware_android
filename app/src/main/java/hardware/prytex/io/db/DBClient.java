package hardware.prytex.io.db;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.model.DMoatAlert;
import hardware.prytex.io.model.DmoatPolicy;
import hardware.prytex.io.model.SensorData;

import java.util.List;


import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by abubaker on 9/23/16.
 */

public class DBClient {

    private static DBClient instance = null;


    private final Realm realm;

    private DBClient() {
        realm = Realm.getDefaultInstance();
    }

    public static DBClient getInstance() {
        if (instance == null) {
            instance = new DBClient();
        }
        return instance;
    }


    public void deleteAllAlerts() {
        synchronized (realm) {
            try {
                realm.executeTransaction(realm -> realm.delete(DMoatAlert.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public DmoatPolicy createNewPolicy(final DmoatPolicy policy) {
        DmoatPolicy dbPolicy = null;
        synchronized (realm) {
            try {
                // increment index
                int nextID = getUniquePolicyID();
                realm.beginTransaction();
//                policy.id = policy.getId() != 0 ? policy.getId() : nextID;
                dbPolicy = realm.copyToRealmOrUpdate(policy);
                realm.commitTransaction();
            } catch (Exception e) {
                e.printStackTrace();
                realm.cancelTransaction();
            }
        }
        return dbPolicy;
    }

    public void deleteAllPolicies() {
        synchronized (realm) {
            try {
                realm.executeTransaction(realm -> realm.delete(DmoatPolicy.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public RealmResults<DmoatPolicy> getAllPolicies() {
        return realm.where(DmoatPolicy.class).findAll();
    }

    public DmoatPolicy getPolicyById(int id) {
        return realm.where(DmoatPolicy.class).equalTo("id", id).findFirst();
    }

    private int getUniquePolicyID() {
        return Dmoat.getUniquePolicyId();

    }

}
