package hardware.prytex.io.fragment.AlertsTabs;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

/**
 * Created by macbookpro13 on 17/01/2018.
 */

public class AlertsPagerAdapter extends FragmentStatePagerAdapter {
    private final int mNumOfTabs;

    public AlertsPagerAdapter(FragmentManager fm, int NumOfTabs,int alertFrgId) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new ViewAlertsAdvancedFragment();
            case 1:
                return new BlockedAlertsFragment();

            case 2:
                return new PreMutedAlertFragment();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
