//package com.prytex.io.fragment;
//
//import android.io.Activity;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.os.Bundle;
//import androidx.localbroadcastmanager.content.LocalBroadcastManager;
//import android.util.Log;
//
//import com.prytex.io.pubnub.PubNubResult;
//
//import java.util.ArrayList;
//
//import static com.prytex.io.pubnub.PubNubDataHandler.CHANNEL_NET_CONFIG;
//
///**
// * Created by Khawar on 11/12/2016.
// */
//
//public abstract class PubNubBaseFragment extends BaseFragment {
//
//    private ArrayList<String> channels = new ArrayList<>();
//
//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//     //   registerForPubNub();
//    }
//
//    private void registerForPubNub() {
//        channels = getChannels();
//        if (channels.size() == 0) {
//            return;
//        }
//        IntentFilter filter = new IntentFilter();
//        for (String channel : channels) {
//            filter.addAction(channel);
//        }
//        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(pubNubMessageReceiver, filter);
//    }
//
//    @Override
//    public void onDetach() {
//        try {
//            if (channels.size() > 0) {
//                LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(pubNubMessageReceiver);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        super.onDetach();
//    }
//
//    private final BroadcastReceiver pubNubMessageReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            if (!isAdded()) {
//                return;
//            }
//            String actionChannel = intent.getAction();
//            Bundle extras = intent.getExtras();
//            if (extras == null || !extras.containsKey("data")) {
//                return;
//            }
//            PubNubResult result = (PubNubResult) extras.get("data");
//            onMessageReceived(actionChannel, result);
//
//            if (result.channel.startsWith(CHANNEL_NET_CONFIG)){
//                Log.d("Result is== ", result.toString());
//                Log.d("Result is== ", result.netConfig.toString());
//           //     NetConfigFragment.alertNetconfig = result.netConfig;
//
//                Log.d("actionChannel is== ", actionChannel);
//            }
//        }
//    };
//
//    public void onMessageReceived(String channel, PubNubResult message) {
//
//    }
//
//    protected ArrayList<String> getChannels() {
//        return channels;
//    }
//
//    public void FragmentPopStackDashboardIntent(){
//
//        getActivity().finish();
//        startActivity(getActivity().getIntent());
//
//
//    }
//
//
//}
