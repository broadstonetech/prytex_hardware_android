package hardware.prytex.io.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.activity.BaseActivity;


import static hardware.prytex.io.activity.DashBoardActivity.llBottomTabs;

public class TermsPolicyAgreementFragment extends BaseActivity {

    @Override
    public int getLayoutId() {
        return R.layout.fragment_terms_policy_agreement;
    }

    @Override
    public void initViews(Bundle savedInstanceState) {
        super.initViews( savedInstanceState);

        SharedPreferences pref = getSharedPreferences("MyPref", 0);


        llBottomTabs.setVisibility(View.GONE);
        TextView tvAgreeBtn = findViewById(R.id.tvAgreeButtons);
        TextView tvTitle = findViewById(R.id.privacy_policy_agreement);
        TextView tvLastRevised = findViewById(R.id.term_service_heading_agreement);
        TextView tvTextDescription = findViewById(R.id.tvText);
        WebView web_View = findViewById(R.id.webview);

        if (pref.getString("file_clicked", "t").equalsIgnoreCase("tos")){
//            tvLastRevised.setText("Last Revised: December 18, 2017");
//            tvTitle.setText(getResources().getString(R.string.title_tos));
//            tvTextDescription.setText(getResources().getString(R.string.terms_of_services));
//            URI myURI = new URI();
//            pdfView.fromUri(Uri.parse(myURI));
            WebSettings webSettings = web_View.getSettings();
            webSettings.setJavaScriptEnabled(true);
            web_View.loadUrl("https://bstlegal.s3.amazonaws.com/dmoat/pdfs/d.moat_Terms+of+Service.pdf");
        }
        else if (pref.getString("file_clicked", "t").equalsIgnoreCase("pp")){
            tvLastRevised.setText(getResources().getString(R.string.last_received));
            tvTitle.setText(getResources().getString(R.string.title_pp));
            tvTextDescription.setText(getResources().getString(R.string.text_pp));

        }else if (pref.getString("file_clicked", "t").equalsIgnoreCase("eula")){
            tvLastRevised.setText(getResources().getString(R.string.last_received));
            tvTitle.setText(getResources().getString(R.string.title_eula));
            tvTextDescription.setText(getResources().getString(R.string.text_eula));
        }

        tvAgreeBtn.setOnClickListener(v -> {
            if (AboutFragment.isFromAboutScreen){
              //  getHelper().replaceFragment(new AboutFragment(), R.id.fragment_container, true, false);
                Intent myIntent = new Intent(TermsPolicyAgreementFragment.this, AboutFragment.class);
                startActivity(myIntent);
                finish();
                AboutFragment.isFromAboutScreen = false;

            }else {

                replaceFragment(new SignUpFragment(), R.id.fragment_container, true, false);
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
