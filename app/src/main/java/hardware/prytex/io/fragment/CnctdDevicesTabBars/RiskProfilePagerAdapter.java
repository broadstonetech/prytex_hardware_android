package hardware.prytex.io.fragment.CnctdDevicesTabBars;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

class RiskProfilePagerAdapter extends FragmentStatePagerAdapter {
    private final int mNumOfTabs;

    public RiskProfilePagerAdapter(FragmentManager fm, int NumOfTabs, Context ctx) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new OpenPortsFragment();
            case 1:
                return new DefaultPwdsFragment();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
