package hardware.prytex.io.fragment.Policy;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.adapter.PolicyDurationAdapter;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.GeneralUtils;
import hardware.prytex.io.util.MyToast;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;
import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;
import static hardware.prytex.io.parser.AlertsDashboardParser.modeStatus;
import static hardware.prytex.io.parser.PolicyDurationParser.listOfPolicyDurations;


@SuppressWarnings("ALL")
public class PauseInternetFragment extends BaseFragment {

    private ExpandableListView listViewExpandable;
    private ProgressDialog dialog;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    Calendar calendar;
    SimpleDateFormat sdf;
    public static String savedPauseTime = "";
    private static List<String> listDataHeader;
    private static List<String> listDataHeaderDuration;
    private HashMap<String, List<String>> listDataChild;
    private HashMap<String, List<String>> listDataChildDest;
    public static String PolicyCallDuration = "";
    public static String PARENTAL_CONTROL_STATUS = "";
    private TextView tvPolicyDataDuration;
    private SharedPreferences pref;
    private SwitchCompat parentalControlSwitchBtn;
    private ImageView imgParentalSwitchBtn;
    private static final String[] PAUSE_INTERNET_INTERVALS = {"10 minutes", "20 minutes", "30 minutes"};
    private static final String[] PAUSE_INTERNET_INTERVALS_VALUES = {"10", "20", "30"};
    public static final int PAUSE_10_MINUTES = 10;
    public static final int PAUSE_20_MINUTES = 20;
    public static final int PAUSE_30_MINUTES = 30;
    private String PARENTAL_CONTROL_TYPE_SELECTED = "";
    private boolean isParentalControlEnabledFromServer = false;
    private LinearLayout llDataDuration;
    private boolean boolListActiveDevices=true;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_pause_internet;
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);
        imgInfoBtn.setVisibility(View.VISIBLE);
        Button btnPauseInternet = parent.findViewById(R.id.btnPauseInternet);
        tvPolicyDataDuration = parent.findViewById(R.id.tvDataDuration);
        mSwipeRefreshLayout = parent.findViewById(R.id.swipe_refresh_layout);
        listViewExpandable = parent.findViewById(R.id.lvExpPolicyUsage);
        llDataDuration = parent.findViewById(R.id.llDataDuration);

        parentalControlSwitchBtn = parent.findViewById(R.id.swicth_parentalControlStatus);
        imgParentalSwitchBtn = parent.findViewById(R.id.imgParentalControlStatus);
        try {
            showProgressDialog(true);
            Api.getPolicyDuration(getActivity(), getRequestParams(), listener);
        } catch (Exception e) {
            Log.d("PolicyUsageExcep", e.toString());
        }

        btnPauseInternet.setOnClickListener(v -> {
            GeneralUtils.ScaleInAnimation(getActivity(),v);
            if (isDmoatOnline) {
                showIntervalDialog();
            } else {
                showAlertMsgDialog(getContext(), getResources().getString(R.string.dmoat_offline));
            }

        });


        mSwipeRefreshLayout.setOnRefreshListener(() -> refreshContent());

        imgParentalSwitchBtn.setOnClickListener(v -> {

            if (!isParentalControlEnabledFromServer) {
                if(modeStatus.equalsIgnoreCase("1") )
                {
                    showParentalControlDialogForAdvanceMode("Please make sure you have added 165.227.193.150 as your DNS in your router settings.","Enable");
                    PARENTAL_CONTROL_TYPE_SELECTED = "active";
                }else {
                    showParentalControlDialog("Enable");
                    PARENTAL_CONTROL_TYPE_SELECTED = "active";
                }
            } else {
                if(modeStatus.equalsIgnoreCase("1") )
                {
                    showParentalControlDialogForAdvanceMode("Please make sure you have removed 165.227.193.150 as your DNS from your router settings.","Disable");
                    PARENTAL_CONTROL_TYPE_SELECTED = "inactive";
                }else {
                    showParentalControlDialog("Disable");
                    PARENTAL_CONTROL_TYPE_SELECTED = "inactive";
                }
            }
        });

        parent.findViewById(R.id.rlActiveDeviceSession).setOnClickListener(v -> {
            if(boolListActiveDevices) {
                listViewExpandable.setVisibility(View.VISIBLE);
                llDataDuration.setVisibility(View.VISIBLE);
                boolListActiveDevices = false;
            }else
            {
                listViewExpandable.setVisibility(View.GONE);
                llDataDuration.setVisibility(View.GONE);
                boolListActiveDevices = true;
            }
        });
        parent.findViewById(R.id.tvAlertsElipses).setOnClickListener(v -> {
            if(boolListActiveDevices) {
                listViewExpandable.setVisibility(View.VISIBLE);
                llDataDuration.setVisibility(View.VISIBLE);
                boolListActiveDevices = false;
            }else
            {
                listViewExpandable.setVisibility(View.GONE);
                llDataDuration.setVisibility(View.GONE);
                boolListActiveDevices = true;
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "policy_usage", null /* class override */);

    }

    @Override
    public String getTitle() {
        return "Policy";
    }

    private void showIntervalDialog() {
        int selectedInterval = 0;
        new AlertDialog.Builder(getActivity())
                .setTitle("Internet Pause Interval:")
                .setSingleChoiceItems(PAUSE_INTERNET_INTERVALS, selectedInterval, null)
                .setPositiveButton("Save", (dialog, whichButton) -> {
                    int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                    showPauseInternetDialog(selectedPosition);
                }).setNegativeButton("Cancel", null)
                .show();
    }

    private void showParentalControlDialog(String status) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Parental Control:")
                .setMessage("Do you wan to " + status + " the parental control?")
                .setPositiveButton(status, (dialog, whichButton) -> {
                    if (GeneralUtils.isConnected(getContext())) {
                        Api.pauseInternetRequest(getContext(), getPauseInternetParams("00", true), pauseInternetListener);
                    } else {
                        showAlertMsgDialog(getContext(), getResources().getString(R.string.internet_connection));
                    }
                }).setNegativeButton("Cancel", null)
                .show();
    }
    private void showParentalControlDialogForAdvanceMode(String message,String status) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Parental Control:")
                .setMessage(message)
                .setPositiveButton("Accept", (dialog, whichButton) -> {
                    if (GeneralUtils.isConnected(getContext())) {
                        showParentalControlDialog(status);
                    } else {
                        showAlertMsgDialog(getContext(), getResources().getString(R.string.internet_connection));
                    }
                }).setNegativeButton("Cancel", null)
                .show();
    }
    private void showPauseInternetDialog(int selectedPosition) {
        final String timeInterval = PAUSE_INTERNET_INTERVALS[selectedPosition];
        final String timeIntervalVALUE = PAUSE_INTERNET_INTERVALS_VALUES[selectedPosition];
        if (UserManager.checkPauseInternetTimeStatus()) {
            new AlertDialog.Builder(getContext())
                    .setTitle("Pause Internet")
                    .setMessage(getResources().getString(R.string.pause_internet) + " " + timeInterval + ".")
                    .setPositiveButton("Pause", (dialog, whichButton) -> {
                        switch (whichButton) {
                            case DialogInterface.BUTTON_POSITIVE:
                                if (GeneralUtils.isConnected(getContext())) {
                                    PARENTAL_CONTROL_TYPE_SELECTED = "";
                                    Api.pauseInternetRequest(getContext(), getPauseInternetParams(timeIntervalVALUE, false), pauseInternetListener);
                                } else {
                                    showAlertMsgDialog(getContext(), getResources().getString(R.string.internet_connection));
                                }
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;

                        }
                    })
                    .setNegativeButton("Cancel", null).show();

        } else {
            MyToast.showMessage(getContext(), getResources().getString(R.string.paused_internet_msg));
        }
    }

    //pause internet req params
    private JSONObject getPauseInternetParams(String timeInterval, Boolean isParentalControl) {
        Calendar mCalendar = Calendar.getInstance(TimeZone.getTimeZone("utc"));
        long millies = mCalendar.getTimeInMillis() / 1000;
        JSONObject requestData = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            requestData.put("request_id", Dmoat.getNewRequestId());
            requestData.put("app_id", pref.getString("app_id", ""));
            requestData.put("token", UserManager.getInstance().getLoggedInUser().token);
            if (isParentalControl) {
                data.put("id", "parental_control");
                data.put("type", PARENTAL_CONTROL_TYPE_SELECTED);
            } else {
                data.put("id", "pause");
                data.put("type", timeInterval);
                data.put("eve_sec", String.valueOf(millies));
            }
            requestData.put("data", data);

            Log.d("pauseInternetParams", String.valueOf(requestData));
//            requestData.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestData;

    }

    //listener pause internet
    private final AsyncTaskListener pauseInternetListener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

//            getParentView().findViewById(R.id.btnDiscoverDevice).setVisibility(View.VISIBLE);
            if (result.code == 2 || result.code == 200) {
                if (PARENTAL_CONTROL_TYPE_SELECTED.equalsIgnoreCase("active")) {
                    isParentalControlEnabledFromServer = true;
                    parentalControlSwitchBtn.setSelected(false);
                    parentalControlSwitchBtn.setEnabled(false);
                    imgParentalSwitchBtn.setImageResource(R.drawable.switch_on);
                } else if (PARENTAL_CONTROL_TYPE_SELECTED.equalsIgnoreCase("inactive")) {
                    isParentalControlEnabledFromServer = false;
                    parentalControlSwitchBtn.setSelected(true);
                    parentalControlSwitchBtn.setEnabled(true);
                    imgParentalSwitchBtn.setImageResource(R.drawable.switch_off);
                }
//                calendar = Calendar.getInstance();
//                pauseInternetSwitch.setChecked(true);
//                isPaused = true;
//                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
////        formattedDate = df.format(calendar.getTime());
//                // formattedDate have current date/time
//                calendar.add(Calendar.MINUTE, 10);
//                savedPauseTime = df.format(calendar.getTime());
//                Log.d("PauseInternetTime", "saved time fr pause internet is = " + savedPauseTime);
            } else {
                boolean isPaused = true;
                if (result.message.equalsIgnoreCase("")) {
                    MyToast.showMessage(getContext(), "Error occured, try again later.");
                }
                MyToast.showMessage(getContext(), result.message);
            }
        }

    };

    private JSONObject getRequestParams() {
        JSONObject requestData = new JSONObject();
        Log.d("TimezoneShort", Calendar.getInstance().getTimeZone().getDisplayName(false, TimeZone.SHORT));
        String timeZone = Calendar.getInstance().getTimeZone().getDisplayName(false, TimeZone.SHORT);


        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"),
                Locale.getDefault());
        Date currentLocalTime = calendar.getTime();
        @SuppressLint("SimpleDateFormat") DateFormat date = new SimpleDateFormat("Z");
        String localTime = date.format(currentLocalTime);

        DateFormat dates = new SimpleDateFormat("z", Locale.getDefault());
        String localTimes = dates.format(currentLocalTime);
        Log.d("TimezoneLocal", localTime);
        Log.d("TimezoneLocalLength", String.valueOf(localTime.length()));
        String str = new StringBuffer(localTime).insert(localTime.length() - 2, ":").toString();

        Log.d("TimezoneLocalFormat", str);
        Log.d("TimezoneLocals", localTimes);
        JSONObject data = new JSONObject();
        try {
            data.put("time_zone", str);
            requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            requestData.put("request_id", Dmoat.getNewRequestId());
            requestData.put("app_id", pref.getString("app_id", ""));
            requestData.put("token", UserManager.getInstance().getLoggedInUser().token);
            requestData.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestData;
    }

    private final AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (!isAdded()) {
                return;
            }
            showProgressDialog(false);
            if (result.code == 200) {
                if (result.message.equalsIgnoreCase("No data detected")) {
                    getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
//                    showAlertMsgDialog(getContext(), "No alerts were detected.");
                    setParentalControlSwitch();
                } else {
                    getParentView().findViewById(R.id.error_label).setVisibility(View.GONE);
                    tvPolicyDataDuration.setVisibility(View.VISIBLE);
                    tvPolicyDataDuration.setText(PolicyCallDuration);
                    setParentalControlSwitch();

                    prepareListData();
                    PolicyDurationAdapter policyDurationAdapter = new PolicyDurationAdapter(getContext(), listDataHeader, listDataHeaderDuration, listDataChild, listDataChildDest, listOfPolicyDurations);
                    // setting list adapter
                    listViewExpandable.setAdapter(policyDurationAdapter);
                }
            } else if (result.code == 503) {
                getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
                showAlertMsgDialog(getContext(), result.message);
            } else if (result.code == 409) {
                getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
                showAlertMsgDialog(getContext(), result.message);
            } else {
//                showAlertMsgDialog(getContext(), "No alerts were detected.");
                getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
            }

        }
    };

    private void setParentalControlSwitch() {
        if (PARENTAL_CONTROL_STATUS.equalsIgnoreCase("active")) {
            parentalControlSwitchBtn.setSelected(false);
            parentalControlSwitchBtn.setEnabled(false);
            isParentalControlEnabledFromServer = true;
            imgParentalSwitchBtn.setImageResource(R.drawable.switch_on);
        } else {
            parentalControlSwitchBtn.setSelected(true);
            parentalControlSwitchBtn.setEnabled(true);
            isParentalControlEnabledFromServer = false;
            imgParentalSwitchBtn.setImageResource(R.drawable.switch_off);
        }
    }

    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private void refreshContent() {
        Api.getPolicyDuration(getActivity(), getRequestParams(), listener);
        mSwipeRefreshLayout.setRefreshing(false);

    }

    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        List<Integer> listDataHeaderID = new ArrayList<>();
        listDataHeaderDuration = new ArrayList<>();
        listDataChild = new HashMap<>();
        listDataChildDest = new HashMap<>();

        for (int i = 0; i < listOfPolicyDurations.size(); i++) {

            String hostName = listOfPolicyDurations.get(i).getHostName();
            String totalDuration = listOfPolicyDurations.get(i).getTotalDuration();
            int id = listOfPolicyDurations.get(i).getId();
            if (listDataHeaderID.contains(id)) {
//                    Log.d("PreMuted", "element exist");
            } else {
//                    Log.d("PreMuted", "element donot exist");
                listDataHeaderID.add(id);
                listDataHeader.add(hostName);
                listDataHeaderDuration.add(totalDuration);
            }

        }
        // Adding child data
        for (int i = 0; i < listDataHeader.size(); i++) {
            List<String> connectTimeList = new ArrayList<>();
            List<String> disConnectTimeList = new ArrayList<>();

            for (int j = 0; j < listOfPolicyDurations.size(); j++) {
//                Log.d("premuted--header", listDataHeader.get(i));
                if (listOfPolicyDurations.get(j).getHostName().equals(listDataHeader.get(i))) {
                    connectTimeList.add(listOfPolicyDurations.get(j).getConnectTime());
                    disConnectTimeList.add(listOfPolicyDurations.get(j).getSlotUsage());
                }
                listDataChild.put(listDataHeader.get(i), connectTimeList);
                listDataChildDest.put(listDataHeader.get(i), disConnectTimeList);
            }

        }
    }
}
