package hardware.prytex.io.fragment.CnctdDevicesTabBars;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.View;

import hardware.prytex.io.R;
import hardware.prytex.io.fragment.BaseFragment;

import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;

public class RiskProfileMainFragment extends BaseFragment {

    @Override
    public int getLayoutId() {
        return R.layout.fragment_risk_profile_main;
    }

    @Override
    public String getTitle() {
        return "Risk Profile";
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        TabLayout tabLayout = parent.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Open Ports"));
        tabLayout.addTab(tabLayout.newTab().setText("Default Passwords"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        imgInfoBtn.setVisibility(View.VISIBLE);

        final ViewPager viewPager = parent.findViewById(R.id.pager);
        final RiskProfilePagerAdapter adapter = new RiskProfilePagerAdapter(getFragmentManager(), tabLayout.getTabCount(), getContext());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 0) {
                    Log.d("Tab", "Ports TAB selsected");
                } else {
                    Log.d("Tab", "Default passwords TAB selsected");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        if (OpenPortsFragment.listOfActivePorts.size() == 0){
            viewPager.setCurrentItem(1);
        }

    }
}
