package hardware.prytex.io.fragment.Policy.ProtocolsPolicy;



import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.adapter.ProtocolPolicyAdapter;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.fragment.Policy.PoliciesMainFragment;
import hardware.prytex.io.model.ProtocolsListModal;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.parser.FrequentProtocols;
import hardware.prytex.io.util.MyToast;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.fragment.Policy.AccessControl.PolicyPlanFragment.gernerateRandomKey;
import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;
import static hardware.prytex.io.parser.FrequentProtocols.listOfFrequentProtocolsReceived;

@SuppressWarnings("ALL")
public class ProtocolPolicyControlFragment extends BaseFragment {

    private ProgressDialog dialog;
    private SharedPreferences pref;
    private RecyclerView mRecyclerViewFrquent;
    private EditText etPolicyTitle;
    private TextView tvViewAdditional;
    private String policeyKey = "";
    private String policyType = "";
    private String policyTitle = "";
    private JSONArray jsonArrSelectedProtocols;
    public boolean isForDevice = false;
    private final ArrayList<ProtocolsListModal> listOfFrequentProtocols = new ArrayList<>();
    private final ArrayList<ProtocolsListModal> listOfFrequentProtocolsUiNames = new ArrayList<>();
    private final ArrayList<ProtocolsListModal> listOfAdditionalProtocols = new ArrayList<>();
    private final ArrayList<ProtocolsListModal> listOfAllProtocolsCombined = new ArrayList<>();
    public static final List<String> listOfSelectedProtocolsForBlocking = new ArrayList<>();
    private final List<String> listOfSelectedProtocolsKeysForBlocking = new ArrayList<>();
    public String hostID = "";
    public String hostName = "";
    public String hostIp = "";
    public static final HashMap<String, String> protocolsValuesDisctionarForBlocking = new HashMap<>();
    private int FREQUENT_PROTOCOLS_COUNT = 0;


    @Override
    public int getLayoutId() {
        return R.layout.fragment_protocol_policy_control;
    }

    @Override
    public String getTitle() {
        return "Protocol Manager";
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);


        mRecyclerViewFrquent = parent.findViewById(R.id.recycler_viewFrequent);
        Button btnSavePolicy = parent.findViewById(R.id.btnSaveProtocolPolicy);
        etPolicyTitle = parent.findViewById(R.id.etPolicyTitle);
        TextView tvAppliedOn = parent.findViewById(R.id.tvAppliedOn);
        TextView tvFrequentlyUsed = parent.findViewById(R.id.tvFrequentlyUsedProtocols);
        tvViewAdditional = parent.findViewById(R.id.tvViewAdditional);
        imgInfoBtn.setVisibility(View.VISIBLE);

        listOfFrequentProtocolsUiNames.clear();
        listOfSelectedProtocolsForBlocking.clear();
        prepareProtoclValuesForBlocking();
        policeyKey = gernerateRandomKey();

        if (isForDevice) {
            tvAppliedOn.setText(hostName);
        } else {
            tvAppliedOn.setText(getActivity().getResources().getString(R.string.network));
        }


        showProgressDialog(true);
        Api.getFrequentProtocols(getActivity(), getParams(), listener);


        mRecyclerViewFrquent.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManagerFrequent = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerViewFrquent.setLayoutManager(mLayoutManagerFrequent);

        btnSavePolicy.setOnClickListener(v -> {
            String action = "create";
            saveProtocolPolicy(action);
            if (!etPolicyTitle.getText().toString().equalsIgnoreCase("")) {
                if (!listOfSelectedProtocolsKeysForBlocking.isEmpty()) {

                    if (isDmoatOnline) {
                        showCreateDelPrompt(getActivity(), getContext().getString(R.string.submit_protocol_policy), action);
                    } else {
                        showAlertMsgDialog(getContext(), getResources().getString(R.string.dmoat_offline));
                    }

                } else {
                    MyToast.showMessage(getContext(), "Select protocols to add in policy.");
                }
            } else {
                MyToast.showMessage(getContext(), "Please enter policy title.");
            }

        });

        tvViewAdditional.setOnClickListener(v -> {
            tvViewAdditional.setTextColor(Color.GRAY);
            tvViewAdditional.setEnabled(false);
            tvViewAdditional.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.minimize, 0);

            FREQUENT_PROTOCOLS_COUNT = listOfAllProtocolsCombined.size();
            ProtocolPolicyAdapter mAdapter = new ProtocolPolicyAdapter(getContext(), listOfAllProtocolsCombined, FREQUENT_PROTOCOLS_COUNT, protocolsValuesDisctionarForBlocking, mSelectedListener);
            mRecyclerViewFrquent.setAdapter(mAdapter);
            mRecyclerViewFrquent.smoothScrollToPosition(listOfFrequentProtocols.size());

        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
//        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "protocol_policy_create", null /* class override */);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, "protocol_policy_create");
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, this.getClass().getName());

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);
    }

    private void cleanFrequentProtocols() {

        Log.d("protocolForPolicy", String.valueOf(FrequentProtocols.listOfFrequentProtocolsReceived.size()));
        String protocolsString = listOfFrequentProtocolsReceived.get(0).getProtocolsList();
        String nString = protocolsString.replace("{", "");
        String n1String = nString.replace("[", "");
        String n2String = n1String.replace("]", "");
        final String oString = n2String.replace("}", "");
        List<String> protocolSplitStringList = Arrays.asList(oString.split(","));
        for (int i = 0; i < protocolSplitStringList.size(); i++) {
            String pString = protocolSplitStringList.get(i);
            final String qString = pString.replace(":", ",");
            List<String> as = Arrays.asList(qString.split(","));
            String protocolName = as.get(0).toString();
            String newName = protocolName.replace("\"", "");

            //    public static boolean isFromBandwidthDetails = false;
            ProtocolsListModal protocolsListModel = new ProtocolsListModal();
//            protocolsListModel.setId(i + 1);
            protocolsListModel.setProtocolName(newName);
            listOfFrequentProtocols.add(protocolsListModel);

        }
    }

    private void getFrequentProtocolNames(ArrayList<ProtocolsListModal> list) {
        ProtocolsListModal model;

        for (int i = 0; i < list.size(); i++) {
            String protocolNameKey = list.get(i).getProtocolName();
            for (int j = 0; j < protocolsValuesDisctionarForBlocking.size(); j++) {
                model = new ProtocolsListModal();
                if (protocolsValuesDisctionarForBlocking.containsKey(protocolNameKey)) {
                    String protocolUiName = protocolsValuesDisctionarForBlocking.get(protocolNameKey);
                    model.setProtocolName(protocolUiName);
                    listOfFrequentProtocolsUiNames.add(model);
                    break;
                } else {
                    Log.d("ProtocolControl", "not found in dictionary");
                }
            }
        }
        FREQUENT_PROTOCOLS_COUNT = listOfFrequentProtocols.size();
    }

    //additional protocols
    private void getAdditionalProtocols(ArrayList<ProtocolsListModal> list) {
        ProtocolsListModal model;
        for (String key : protocolsValuesDisctionarForBlocking.keySet()) {
            model = new ProtocolsListModal();

            String protocolNameKey = protocolsValuesDisctionarForBlocking.get(key);

//            String protocolUiName = protocolsValuesDisctionarForBlocking.get(protocolNameKey);
            model.setProtocolName(protocolNameKey);
            if (protocolNameKey.equalsIgnoreCase("")) {

            } else {
                listOfAdditionalProtocols.add(model);
            }

        }

        Log.d("ProtocolControl Size", String.valueOf(listOfAdditionalProtocols.size()));

        for (int j = 0; j < listOfFrequentProtocolsUiNames.size(); j++) {
            String frequentPname = listOfFrequentProtocolsUiNames.get(j).getProtocolName();
            for (int k = 0; k < listOfAdditionalProtocols.size(); k++) {
                if (listOfAdditionalProtocols.get(k).getProtocolName().equalsIgnoreCase(frequentPname)) {
//                    listOfAdditionalProtocols.remove(listOfFrequentProtocolsUiNames.get(j).getProtocolName());
                    listOfAdditionalProtocols.remove(k);
                    break;
                }
            }
        }
        Log.d("ProtocolControl Size", String.valueOf(listOfAdditionalProtocols.size()));
        listOfAllProtocolsCombined.addAll(listOfFrequentProtocolsUiNames);
        listOfAllProtocolsCombined.addAll(listOfAdditionalProtocols);
    }

    private final AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (!isAdded()) {
                return;
            }
            showProgressDialog(false);
            if (result.code == 200 || result.code == 2) {
                cleanFrequentProtocols();
                getFrequentProtocolNames(listOfFrequentProtocols);
                getAdditionalProtocols(listOfFrequentProtocols);
////        ProtocolPolicyAdapter mAdapter = new ProtocolPolicyAdapter(getContext(), listOfSelectedHostProtocols, protocolsValuesDisctionarForBlocking, protocolsDesscriptionDisctionar, mSelectedListener);
                if (listOfFrequentProtocols.size() == 0) {
                    FREQUENT_PROTOCOLS_COUNT = listOfAllProtocolsCombined.size();
                }
                ProtocolPolicyAdapter mAdapter = new ProtocolPolicyAdapter(getContext(), listOfAllProtocolsCombined, FREQUENT_PROTOCOLS_COUNT, protocolsValuesDisctionarForBlocking, mSelectedListener);
                mRecyclerViewFrquent.setAdapter(mAdapter);


            } else if (result.code == 503) {
                showAlertMsgDialog(getContext(), result.message);
            } else if (result.code == 409) {
                showAlertMsgDialog(getContext(), result.message);
            } else {
                if (result.message.equalsIgnoreCase("")) {
                    showAlertMsgDialog(getContext(), getResources().getString(R.string.error_message_generic));
                } else {
                    showAlertMsgDialog(getContext(), result.message);
                }
            }
        }
    };

    private JSONObject getParams() {
        JSONObject js = new JSONObject();
        try {
            js.put("app_id", pref.getString("app_id", ""));
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }


    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private final ProtocolPolicyAdapter.OnAlertSelectedListener mSelectedListener = new ProtocolPolicyAdapter.OnAlertSelectedListener() {
    };


    private void saveProtocolPolicy(String actions) {
        listOfSelectedProtocolsKeysForBlocking.clear();

        policyTitle = etPolicyTitle.getText().toString();

        for (int i = 0; i < listOfSelectedProtocolsForBlocking.size(); i++) {
            String value = listOfSelectedProtocolsForBlocking.get(i);

            for (Map.Entry entry : protocolsValuesDisctionarForBlocking.entrySet()) {
                if (value.equals(entry.getValue())) {
                    Object key = entry.getKey();
                    String keyName = key.toString();
                    listOfSelectedProtocolsKeysForBlocking.add(keyName);
                    break; //breaking because its one to one map
                }
            }
        }

    }

    private void callSubmitApi(String actions) {

        if (policyType.equalsIgnoreCase("")) {
            if (isForDevice) {
                policyType = "device";
            } else {
                policyType = "network";
            }
        }

        if (actions.equalsIgnoreCase("delete")) {
            Api.createProtocolPolicy(getActivity(), getRequestparams(policyType, actions), listenerPostPolicy);

        } else if (!policyTitle.equalsIgnoreCase("")) {
            if (!listOfSelectedProtocolsKeysForBlocking.isEmpty()) {

                Api.createProtocolPolicy(getActivity(), getRequestparams(policyType, actions), listenerPostPolicy);


            } else {
                MyToast.showMessage(getContext(), "Select protocols to add in policy.");
            }
        } else {
            MyToast.showMessage(getContext(), "Please enter policy title.");
        }
    }

    private JSONObject getRequestparams(String type, String action) {

        JSONObject j = new JSONObject();
        JSONArray jsArrayHosts = new JSONArray(listOfSelectedProtocolsForBlocking);

        try {
            JSONObject data = new JSONObject();

            data.put("type", type);
            data.put("procedure", action);
            data.put("title", policyTitle);
            data.put("id", policeyKey);

            if (action.equalsIgnoreCase("delete")) {
                data.put("protocols", jsonArrSelectedProtocols);
            } else {
                data.put("protocols", getHOstsJson());
            }

            if (type.equalsIgnoreCase("device")) {
                data.put("hostname", hostName);
                data.put("hostid", hostID);
                data.put("title", policyTitle);
                data.put("ip", hostIp);
            }

            j.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            j.put("request_id", Dmoat.getNewRequestId());
            j.put("app_id", pref.getString("app_id", ""));
            j.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            j.put("token", UserManager.getInstance().getLoggedInUser().token);
            j.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("PolicPlanCreateParam", j.toString());
        return j;
    }

    private JSONArray getHOstsJson() {

        JSONObject jResult = new JSONObject();// main object
        JSONArray jArray = new JSONArray();// /ItemDetail jsonArray

        for (int i = 0; i < listOfSelectedProtocolsKeysForBlocking.size(); i++) {
            JSONObject jGroup = new JSONObject();// /sub Object

            try {
                jGroup.put("protocols", listOfSelectedProtocolsKeysForBlocking.get(i));

                jArray.put(jGroup);
                jResult.put("protocol", jArray);
                // /itemDetail Name is JsonArray Name
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jArray;
    }

    private final AsyncTaskListener listenerPostPolicy = result -> {
        if (result.message.equalsIgnoreCase("")) {
            result.message = getContext().getResources().getString(R.string.error_message_generic);
        }

        showProgressDialog(false);
        if (result.code == 200 || result.code == 2) {
            //policy create successfully go back
//                showPolicySuccessDialog(getContext(), getContext().getResources().getString(R.string.policy_created_text));
            //go back to previous page and show protocols policies list


            if (isForDevice) {
                showAlertMsgDialog(getContext(), "Policy created successfully for a device.");

            } else {
//            go back to protocol policices list page
                showAlertMsgDialog(getContext(), "Successfully created a policy.");

            }
        } else if (result.code == 503) {
            showAlertMsgDialog(getContext(), result.message);
        } else if (result.code == 409) {
            showAlertMsgDialog(getContext(), result.message);
        } else {
            showAlertMsgDialog(getContext(), result.message);
        }
    };

    public static void prepareProtoclValuesForBlocking() {

        protocolsValuesDisctionarForBlocking.put("afp", "AFP");
        protocolsValuesDisctionarForBlocking.put("aimini", "Aimini");
        protocolsValuesDisctionarForBlocking.put("amazon", "Amazon");
        protocolsValuesDisctionarForBlocking.put("tls", "TLS");
        protocolsValuesDisctionarForBlocking.put("microsoft365", "Microsoft 365");
        protocolsValuesDisctionarForBlocking.put("googlehangoutduo", "Google Hangouts");

        protocolsValuesDisctionarForBlocking.put("dnscrypt","DNScrypt");
        protocolsValuesDisctionarForBlocking.put("bjnp","BJNP");
        protocolsValuesDisctionarForBlocking.put("ookla","Ookla");
        protocolsValuesDisctionarForBlocking.put("signal","Signal");
        protocolsValuesDisctionarForBlocking.put("nestlogsink","NestLogSink");
        protocolsValuesDisctionarForBlocking.put("http_activesync","HTTP_ActiveSync");
        protocolsValuesDisctionarForBlocking.put("bloomberg","Bloomberg");
        protocolsValuesDisctionarForBlocking.put("smbv3","SMBv23");
        protocolsValuesDisctionarForBlocking.put("ntop","ntop");
        protocolsValuesDisctionarForBlocking.put("mining","Mining");
        protocolsValuesDisctionarForBlocking.put("nintendo","Nintendo");
        protocolsValuesDisctionarForBlocking.put("amqp","AMQP");
        protocolsValuesDisctionarForBlocking.put("genericprotocol","GenericProtocol");
        protocolsValuesDisctionarForBlocking.put("checkmk","CHECKMK");
        protocolsValuesDisctionarForBlocking.put("lisp","LISP");
        protocolsValuesDisctionarForBlocking.put("playstation","Playstation");
        protocolsValuesDisctionarForBlocking.put("targus_dataspeed","Targus Dataspeed");
        protocolsValuesDisctionarForBlocking.put("memcached","Memcached");
        protocolsValuesDisctionarForBlocking.put("smpp","SMPP");
        protocolsValuesDisctionarForBlocking.put("iflix","IFLIX");
        protocolsValuesDisctionarForBlocking.put("googleservices","GoogleServices");
        protocolsValuesDisctionarForBlocking.put("smbv1","SMBv1");
        protocolsValuesDisctionarForBlocking.put("googledrive","GoogleDrive");
        protocolsValuesDisctionarForBlocking.put("csgo","CSGO");
        protocolsValuesDisctionarForBlocking.put("messenger","Messenger");
        protocolsValuesDisctionarForBlocking.put("skypecall","SkypeCall");
        protocolsValuesDisctionarForBlocking.put("applepush","ApplePush");
        protocolsValuesDisctionarForBlocking.put("linkedin","LinkedIn");
        protocolsValuesDisctionarForBlocking.put("amazonvideo","AmazonVideo");
        protocolsValuesDisctionarForBlocking.put("youtubeupload","YouTubeUpload");
        protocolsValuesDisctionarForBlocking.put("ajp","AJP");
        protocolsValuesDisctionarForBlocking.put("soundcloud","SoundCloud");
        protocolsValuesDisctionarForBlocking.put("googleplus","GooglePlus");
        protocolsValuesDisctionarForBlocking.put("playstore","PlayStore");
        protocolsValuesDisctionarForBlocking.put("pastebin","Pastebin");
        protocolsValuesDisctionarForBlocking.put("modbus","Modbus");
        protocolsValuesDisctionarForBlocking.put("iec60870","IEC60870");
        protocolsValuesDisctionarForBlocking.put("zabbix","Zabbix");
        protocolsValuesDisctionarForBlocking.put("websocket","WebSocket");
        protocolsValuesDisctionarForBlocking.put("s7comm","s7comm");
        protocolsValuesDisctionarForBlocking.put("fix","FIX");
        protocolsValuesDisctionarForBlocking.put("facebookzero","FacebookZero");
        protocolsValuesDisctionarForBlocking.put("tinc","TINC");
        protocolsValuesDisctionarForBlocking.put("diameter","Diameter");
        protocolsValuesDisctionarForBlocking.put("wechat","WeChat");
        protocolsValuesDisctionarForBlocking.put("github","Github");
        protocolsValuesDisctionarForBlocking.put("teams","Microsoft Teams");
        protocolsValuesDisctionarForBlocking.put("whatsappfiles","WhatsAppFiles");
        protocolsValuesDisctionarForBlocking.put("dnp3","DNP3");
        protocolsValuesDisctionarForBlocking.put("applestore","AppleStore");
        protocolsValuesDisctionarForBlocking.put("tiktok","TikTok");
        protocolsValuesDisctionarForBlocking.put("someip","SOMEIP");
        protocolsValuesDisctionarForBlocking.put("capwap","CAPWAP");

        protocolsValuesDisctionarForBlocking.put("apple", "Apple");
        protocolsValuesDisctionarForBlocking.put("appleicloud", "iCloud");
        protocolsValuesDisctionarForBlocking.put("appleitunes", "iTunes");
        protocolsValuesDisctionarForBlocking.put("applejuice", "Apple juice");
        protocolsValuesDisctionarForBlocking.put("armagetron", "Armagetron");
        protocolsValuesDisctionarForBlocking.put("avi", "Avi");
        protocolsValuesDisctionarForBlocking.put("ayiya", "AYIYA");
        protocolsValuesDisctionarForBlocking.put("bgp", "BGP");
        protocolsValuesDisctionarForBlocking.put("bittorrent", "BitTorrent");
        protocolsValuesDisctionarForBlocking.put("ciscoskinny", "SCCP");
        protocolsValuesDisctionarForBlocking.put("Ciscovpn", "Cisco vpn");
        protocolsValuesDisctionarForBlocking.put("citrix", "Citrix");
        protocolsValuesDisctionarForBlocking.put("citrix_online", "Citrix Online");
        protocolsValuesDisctionarForBlocking.put("cnn", "CNN");
        protocolsValuesDisctionarForBlocking.put("coap", "CoAP");
        protocolsValuesDisctionarForBlocking.put("collectd", "collectd");
        protocolsValuesDisctionarForBlocking.put("corba", "CORBA");
        protocolsValuesDisctionarForBlocking.put("crossfire", "CrossFire");
        protocolsValuesDisctionarForBlocking.put("dce_rpc", "DCE/RPC");
        protocolsValuesDisctionarForBlocking.put("dhcp", "DHCP");
        protocolsValuesDisctionarForBlocking.put("dhcpv6", "DHCPv6");
        protocolsValuesDisctionarForBlocking.put("direct_download_link", "DDL");
        protocolsValuesDisctionarForBlocking.put("directconnect", "DC");
        protocolsValuesDisctionarForBlocking.put("dns", "DNS");
        protocolsValuesDisctionarForBlocking.put("drda", "DRDA");
        protocolsValuesDisctionarForBlocking.put("dropbox", "Dropbox");
        protocolsValuesDisctionarForBlocking.put("eaq", "EAQ");
        protocolsValuesDisctionarForBlocking.put("edonkey", "eDonkey");
        protocolsValuesDisctionarForBlocking.put("egp", "EGP");
        protocolsValuesDisctionarForBlocking.put("epp", "EPP");
        protocolsValuesDisctionarForBlocking.put("facebook", "Facebook");
        protocolsValuesDisctionarForBlocking.put("fasttrack", "");
        protocolsValuesDisctionarForBlocking.put("filetopia", "Filetopia");
        protocolsValuesDisctionarForBlocking.put("filetopia", "Flash Video");
        protocolsValuesDisctionarForBlocking.put("florensia", "");
        protocolsValuesDisctionarForBlocking.put("ftp_control", "FTP Control ");
        protocolsValuesDisctionarForBlocking.put("ftp_data", "FTP Data");
        protocolsValuesDisctionarForBlocking.put("git", "Git");
        protocolsValuesDisctionarForBlocking.put("gmail", "Gmail");
        protocolsValuesDisctionarForBlocking.put("gnutella", "");
        protocolsValuesDisctionarForBlocking.put("google", "Google");
        protocolsValuesDisctionarForBlocking.put("googlehangout", "Google Hangouts");
        protocolsValuesDisctionarForBlocking.put("Googlesmaps", "Google Maps");
        protocolsValuesDisctionarForBlocking.put("gre", "GRE");
        protocolsValuesDisctionarForBlocking.put("gtp", "GTP");
        protocolsValuesDisctionarForBlocking.put("h323", "h323");
        protocolsValuesDisctionarForBlocking.put("halflife2", "Half-Life 2");
        protocolsValuesDisctionarForBlocking.put("hep", "Hep");
        protocolsValuesDisctionarForBlocking.put("hotmail", "Hotmail");
        protocolsValuesDisctionarForBlocking.put("hotspotshield", "Hotspot Shield");
        protocolsValuesDisctionarForBlocking.put("https", "HTTPS");
        protocolsValuesDisctionarForBlocking.put("http", "HTTP");
        protocolsValuesDisctionarForBlocking.put("http_connect", "HTTP Connect");
        protocolsValuesDisctionarForBlocking.put("httpdownload", "HTTP Download");
        protocolsValuesDisctionarForBlocking.put("iax", "IAX ");
        protocolsValuesDisctionarForBlocking.put("icecast", "Icecast");
        protocolsValuesDisctionarForBlocking.put("icmpv6", "ICMPv6");
        protocolsValuesDisctionarForBlocking.put("igmp", "IGMP");
        protocolsValuesDisctionarForBlocking.put("imap", "IMAP");
        protocolsValuesDisctionarForBlocking.put("imaps", "IMAP");
        protocolsValuesDisctionarForBlocking.put("imesh", "iMesh");
        protocolsValuesDisctionarForBlocking.put("instagram", "Instagram");
        protocolsValuesDisctionarForBlocking.put("ip_in_ip", "IP in IP");
        protocolsValuesDisctionarForBlocking.put("ipsec", "IPsec");
        protocolsValuesDisctionarForBlocking.put("kerberos", "Kerberos");
        protocolsValuesDisctionarForBlocking.put("kontiki", "Kontiki ");
        protocolsValuesDisctionarForBlocking.put("lastfm", "Lastfm");
        protocolsValuesDisctionarForBlocking.put("ldap", "LDAP");
        protocolsValuesDisctionarForBlocking.put("llmnr", "LLMNR");
        protocolsValuesDisctionarForBlocking.put("lotusnotes", "Lotus Notes ");
        protocolsValuesDisctionarForBlocking.put("lync", "Lync");
        protocolsValuesDisctionarForBlocking.put("maplestory", "MapleStory");
        protocolsValuesDisctionarForBlocking.put("mdns", "mDNS");
        protocolsValuesDisctionarForBlocking.put("megaco", "Megaco");
        protocolsValuesDisctionarForBlocking.put("mgcp", "MGCP ");
        protocolsValuesDisctionarForBlocking.put("microsoft", "Microsoft");
        protocolsValuesDisctionarForBlocking.put("mms", "MMS");
        protocolsValuesDisctionarForBlocking.put("move", "");
        protocolsValuesDisctionarForBlocking.put("mpeg", "MPEG transport stream");
        protocolsValuesDisctionarForBlocking.put("mpeg_ts", "MPEG transport stream");
        protocolsValuesDisctionarForBlocking.put("mqtt", "MQTT");
        protocolsValuesDisctionarForBlocking.put("ms_one_drive", "One drive");
        protocolsValuesDisctionarForBlocking.put("msn", "MSN");
        protocolsValuesDisctionarForBlocking.put("mssql-tds", "TDS");
        protocolsValuesDisctionarForBlocking.put("mysql", "MySQL");
        protocolsValuesDisctionarForBlocking.put("netbios", "NBNS");
        protocolsValuesDisctionarForBlocking.put("netflix", "Netflix");
        protocolsValuesDisctionarForBlocking.put("netflow", "NetFlow");
        protocolsValuesDisctionarForBlocking.put("nfs", "NFS");
        protocolsValuesDisctionarForBlocking.put("noe", "Noe");
        protocolsValuesDisctionarForBlocking.put("ntp", "NTP");
        protocolsValuesDisctionarForBlocking.put("ocs", "OCS");
        protocolsValuesDisctionarForBlocking.put("office365", "Office 365");
        protocolsValuesDisctionarForBlocking.put("oggvorbis", "Ogg Vorbis ");
        protocolsValuesDisctionarForBlocking.put("opendns", "OpenDNS");
        protocolsValuesDisctionarForBlocking.put("openft", "OpenFT");
        protocolsValuesDisctionarForBlocking.put("openvpn", "Openvpn");
        protocolsValuesDisctionarForBlocking.put("oracle", "Oracle");
        protocolsValuesDisctionarForBlocking.put("oscar", "OSCAR");
        protocolsValuesDisctionarForBlocking.put("ospf", "OSPF");
        protocolsValuesDisctionarForBlocking.put("pando_media_booster", "PMB");
        protocolsValuesDisctionarForBlocking.put("pandora", "Pandora");
        protocolsValuesDisctionarForBlocking.put("pcanywhere", "Pc Anywhere");
        protocolsValuesDisctionarForBlocking.put("pop3", "POP3");
        protocolsValuesDisctionarForBlocking.put("pops", "POP");
        protocolsValuesDisctionarForBlocking.put("postgresql", "Postgres");
        protocolsValuesDisctionarForBlocking.put("pplive", "PPLIVE");
        protocolsValuesDisctionarForBlocking.put("ppstream", "PPStream");
        protocolsValuesDisctionarForBlocking.put("pptp", "PPTP");
        protocolsValuesDisctionarForBlocking.put("qq", "QQ");
        protocolsValuesDisctionarForBlocking.put("qqlive", "QQLive");
        protocolsValuesDisctionarForBlocking.put("quic", "QUIC");
        protocolsValuesDisctionarForBlocking.put("quicktime", "QuickTime");
        protocolsValuesDisctionarForBlocking.put("radius", "RADIUS");
        protocolsValuesDisctionarForBlocking.put("rdp", "RDP");
        protocolsValuesDisctionarForBlocking.put("realmedia", "RealMedia");
        protocolsValuesDisctionarForBlocking.put("redis", "Redis");
        protocolsValuesDisctionarForBlocking.put("rsync", "Rsync");
        protocolsValuesDisctionarForBlocking.put("rtcp", "RTCP");
        protocolsValuesDisctionarForBlocking.put("rtmp", "RTMP");
        protocolsValuesDisctionarForBlocking.put("rtp", "RTP");
        protocolsValuesDisctionarForBlocking.put("rtsp", "RTSP");
        protocolsValuesDisctionarForBlocking.put("rx", "Rx");
        protocolsValuesDisctionarForBlocking.put("sap", "SAP");
        protocolsValuesDisctionarForBlocking.put("sctp", "SCTP");
        protocolsValuesDisctionarForBlocking.put("sflow", "sFlow");
        protocolsValuesDisctionarForBlocking.put("shoutcast", "SHOUTcast");
        protocolsValuesDisctionarForBlocking.put("sina(weibo)", "Sina Weibo");
        protocolsValuesDisctionarForBlocking.put("skyfile_postpaid", "SkyFile Mail");
        protocolsValuesDisctionarForBlocking.put("skyfile_prepaid", "SkyFile Mail");
        protocolsValuesDisctionarForBlocking.put("skyfile_rudics", "SkyFile Mail");
        protocolsValuesDisctionarForBlocking.put("skype", "Skype");
        protocolsValuesDisctionarForBlocking.put("slack", "Slack");
        protocolsValuesDisctionarForBlocking.put("smb", "SMB");
        protocolsValuesDisctionarForBlocking.put("smtp", "SMTP");
        protocolsValuesDisctionarForBlocking.put("smtps", "SMTPS");
        protocolsValuesDisctionarForBlocking.put("snapchat", "Snapchat");
        protocolsValuesDisctionarForBlocking.put("snmp", "SNMP");
        protocolsValuesDisctionarForBlocking.put("socks", "Socket Secure");
        protocolsValuesDisctionarForBlocking.put("socrates", "SOCRATES");
        protocolsValuesDisctionarForBlocking.put("sopcast", "Sopcast");
        protocolsValuesDisctionarForBlocking.put("soulseek", "Soulseek");
        protocolsValuesDisctionarForBlocking.put("spotify", "Spotify");
        protocolsValuesDisctionarForBlocking.put("ssdp", "SSDP");
        protocolsValuesDisctionarForBlocking.put("ssh", "SSH");
        protocolsValuesDisctionarForBlocking.put("ssl", "SSL");
        protocolsValuesDisctionarForBlocking.put("stealthnet", "Stealthnet");
        protocolsValuesDisctionarForBlocking.put("steam", "Steam");
        protocolsValuesDisctionarForBlocking.put("stun", "STUN");
        protocolsValuesDisctionarForBlocking.put("syslog", "Syslog");
        protocolsValuesDisctionarForBlocking.put("teamspeak", "TeamSpeak");
        protocolsValuesDisctionarForBlocking.put("teamviewer", "TeamViewer");
        protocolsValuesDisctionarForBlocking.put("telegram", "Telegram");
        protocolsValuesDisctionarForBlocking.put("telnet", "Telnet");
        protocolsValuesDisctionarForBlocking.put("teredo", "Teredo");
        protocolsValuesDisctionarForBlocking.put("tftp", "TFTP");
        protocolsValuesDisctionarForBlocking.put("thunder", "Thunder");
        protocolsValuesDisctionarForBlocking.put("tor", "Tor");
        protocolsValuesDisctionarForBlocking.put("truphone", "Truphone");
        protocolsValuesDisctionarForBlocking.put("tuenti", "Tuenti");
        protocolsValuesDisctionarForBlocking.put("tvants", "TVants");
        protocolsValuesDisctionarForBlocking.put("tvuplayer", "TVUPlayer");
        protocolsValuesDisctionarForBlocking.put("twitch", "Twitch");
        protocolsValuesDisctionarForBlocking.put("twitter", "Twitter");
        protocolsValuesDisctionarForBlocking.put("ubntac2", "Ubntac2");
        protocolsValuesDisctionarForBlocking.put("ubuntuone", "Ubuntuone");
        protocolsValuesDisctionarForBlocking.put("unencryped_jabber", "Jabber");
        protocolsValuesDisctionarForBlocking.put("unknown", "Unknown");
        protocolsValuesDisctionarForBlocking.put("upnp", "UPnP");
        protocolsValuesDisctionarForBlocking.put("usenet", "Usenet");
        protocolsValuesDisctionarForBlocking.put("vevo", "Vevo");
        protocolsValuesDisctionarForBlocking.put("vhua", "Vhua");
        protocolsValuesDisctionarForBlocking.put("viber", "Viber");
        protocolsValuesDisctionarForBlocking.put("vmware", "VMWare");
        protocolsValuesDisctionarForBlocking.put("vnc", "VNC");
        protocolsValuesDisctionarForBlocking.put("vrrp", "VRRP");
        protocolsValuesDisctionarForBlocking.put("warcraft3", "Warcraft3");
        protocolsValuesDisctionarForBlocking.put("waze", "Waze");
        protocolsValuesDisctionarForBlocking.put("webex", "Webex");
        protocolsValuesDisctionarForBlocking.put("webm", "");
        protocolsValuesDisctionarForBlocking.put("whatsapp", "Whatsapp");
        protocolsValuesDisctionarForBlocking.put("whatsappvoice", "Whatsapp Voice");
        protocolsValuesDisctionarForBlocking.put("whois-das", "Whois");
        protocolsValuesDisctionarForBlocking.put("wikipedia", "Wikipedia");
        protocolsValuesDisctionarForBlocking.put("windowsmedia", "Windows Media ");
        protocolsValuesDisctionarForBlocking.put("windowsupdate", "Windows Update");
        protocolsValuesDisctionarForBlocking.put("worldofwarcraft", "World Of War Craft");
        protocolsValuesDisctionarForBlocking.put("xbox", "Xbox");
        protocolsValuesDisctionarForBlocking.put("xdmcp", "XDMCP");
        protocolsValuesDisctionarForBlocking.put("yahoo", "Yahoo");
        protocolsValuesDisctionarForBlocking.put("youtube", "Youtube");
        protocolsValuesDisctionarForBlocking.put("zattoo", "Zattoo");
        protocolsValuesDisctionarForBlocking.put("zeromq", "Zeromq");

    }

    private void showAlertMsgDialog(Context ctx, String msg) {
        if (msg.equalsIgnoreCase("") || msg == null || msg.isEmpty() || msg.equals(null)) {
            msg = ctx.getResources().getString(R.string.error_message_generic);
        } else {
        }
        new AlertDialog.Builder(ctx)
                .setTitle("Prytex Hardware")
                .setMessage(msg)

                .setPositiveButton("Ok", (dialog, whichButton) -> {
                    PoliciesMainFragment.isBackFromProtocolPlan = true;
                    getHelper().addFragment(new PoliciesMainFragment(), true, true);


                }).show();

    }

    private void showCreateDelPrompt(Context ctx, String msg, final String action) {
        if (msg.equalsIgnoreCase("") || msg == null || msg.isEmpty() || msg.equals(null)) {
            msg = ctx.getResources().getString(R.string.error_message_generic);
        } else {
        }
        new AlertDialog.Builder(ctx)
                .setTitle("Prytex Hardware")
                .setMessage(msg)

                .setPositiveButton("Ok", (dialog, whichButton) -> {
//                        saveProtocolPolicy(action);
                    callSubmitApi(action);

                }).setNegativeButton("Cancel", (dialog, which) -> {

                }).show();

    }

}
