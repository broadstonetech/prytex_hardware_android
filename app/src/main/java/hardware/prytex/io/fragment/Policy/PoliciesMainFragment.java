package hardware.prytex.io.fragment.Policy;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.View;

import hardware.prytex.io.R;
import hardware.prytex.io.fragment.BaseFragment;

import static hardware.prytex.io.activity.DashBoardActivity.btnHomeDashboard;
import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.activity.DashBoardActivity.llBottomTabs;


@SuppressWarnings("ALL")
public class PoliciesMainFragment extends BaseFragment {

    public boolean isBackFromPlan = false;
    public static boolean isBackFromProtocolPlan = false;
    @Override
    public int getLayoutId() {
        return R.layout.fragment_policies_main;
    }

    @Override
    public String getTitle() {
        return "Policy";
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        TabLayout tabLayout = parent.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Policy"));
        tabLayout.addTab(tabLayout.newTab().setText("Access Control"));
        tabLayout.addTab(tabLayout.newTab().setText("Protocol Control")); // tab made visible for testing purposes
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);                //   tab made visible for testing purposes
        imgInfoBtn.setVisibility(View.VISIBLE);
        llBottomTabs.setVisibility(View.VISIBLE);
        final ViewPager viewPager = parent.findViewById(R.id.pager);
        final PolicyPagerAdapter adapter = new PolicyPagerAdapter(getFragmentManager(), tabLayout.getTabCount(), getContext());
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(adapter);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);


        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

//        btnHomeDashboard.setTextColor(getActivity().getResources().getColor(R.color.unselected_text_color));
//        imgHomeTab.setImageResource(R.drawable.tab_home_unselected);
//
//        imgPolicyTab.setImageResource(R.drawable.tab_policy_selected);
//        btnPolicyDashboard.setTextColor(getActivity().getResources().getColor(R.color.selected_text_color));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 0) {
                    Log.d("Tab", "policy usage TAB selsected");
                } else if (tab.getPosition() == 1) {
                    Log.d("Tab", "Prental control TAB selsected");
                } else {
                    Log.d("Tab", "protocol control TAB selsected");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        if (isBackFromPlan){
            tabLayout.getTabAt(1).select();
        }else if (isBackFromProtocolPlan) {
            tabLayout.getTabAt(2).select();
            isBackFromProtocolPlan = false;
        }else{
            tabLayout.getTabAt(0).select();
        }

        btnHomeDashboard.setOnClickListener(v -> {
            try {
                getActivity().finish();
                getActivity().getIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivityForResult(getActivity().getIntent(), 0);
                getActivity().overridePendingTransition(0,0);

            } catch (Exception e) {
                Log.e("DashboardApiException", e.toString());
            }
        });
    }
}
