package hardware.prytex.io.fragment.BandwidthFrags;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

/**
 * Created by macbookpro13 on 16/02/2018.
 */

class TabsPagerListAdapter extends FragmentStatePagerAdapter {
    private final int mNumOfTabs;

    public TabsPagerListAdapter(FragmentManager fm, int NumOfTabs, Context ctx) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new ProtocolListCurrentDataFragment();
            case 1:
                return new ProtocolListViewFragment();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}