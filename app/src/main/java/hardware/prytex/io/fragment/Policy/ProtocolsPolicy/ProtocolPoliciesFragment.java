package hardware.prytex.io.fragment.Policy.ProtocolsPolicy;



import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.activity.DashBoardActivity;
import hardware.prytex.io.adapter.ProtocolPolicyListAdapter;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;
import static hardware.prytex.io.parser.PotocolPoliciesParser.listOfProtocolPolicies;
import static hardware.prytex.io.fragment.DashBoardFragment.ConnectedDevicesCount;

@SuppressWarnings("ALL")
public class ProtocolPoliciesFragment extends BaseFragment implements View.OnClickListener {


    private ProgressDialog dialog;
    private TextView tvNoPOilicy;
    private RecyclerView mRecyclerView;
    private SearchView searchViewBlockedProtocols;
    private SharedPreferences pref;
    List<String> listOfSelectedProtocolsInPolicy = new ArrayList<>();
    ProtocolPolicyListAdapter mAdapter;
    @Override
    public String getTitle() {
        return "Policy";
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_protocol_policies;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "protocol_policies", null /* class override */);
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);


        mRecyclerView = parent.findViewById(R.id.recycler_view);
        searchViewBlockedProtocols = parent.findViewById(R.id.searchView);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        tvNoPOilicy = parent.findViewById(R.id.tvNoPolicyText);

        Button btnProtocolPolicy = parent.findViewById(R.id.btnProtocolPolicy);
        btnProtocolPolicy.setOnClickListener(this);

        searchBlockedProtocols();
        Api.getAllProtocolPolicies(getActivity(), getParams(), listener);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnProtocolPolicy) {
            showCreatePolicyPrompt(getContext(), getResources().getString(R.string.protocol_policy_type));
            return;
        }
    }

    private void searchBlockedProtocols() {
        View closeButton = searchViewBlockedProtocols.findViewById(R.id.search_close_btn);

        EditText et= (EditText) searchViewBlockedProtocols.findViewById(R.id.search_src_text);
        et.setHint("Search...");

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchViewBlockedProtocols.onActionViewCollapsed();
            }
        });

        searchViewBlockedProtocols.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchViewBlockedProtocols.onActionViewExpanded();
            }
        });
        searchViewBlockedProtocols.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    closeButton.performClick();
                }
            }
        });

        searchViewBlockedProtocols.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchViewBlockedProtocols.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                mAdapter.getFilter().filter(query);
                return false;
            }
        });

    }

    private JSONObject getParams() {
        JSONObject js = new JSONObject();
        try {
            js.put("app_id", pref.getString("app_id", ""));
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }


    private final AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (!isAdded()) {
                return;
            }
            if (result.code == 200) {
                if (listOfProtocolPolicies.size() > 0) {
                    tvNoPOilicy.setVisibility(View.GONE);
                    mAdapter = new ProtocolPolicyListAdapter(getContext(), listOfProtocolPolicies, mSelectedListener);
                    mRecyclerView.setAdapter(mAdapter);
                } else {
                    tvNoPOilicy.setVisibility(View.VISIBLE);
                    if (result.message.equalsIgnoreCase("")) {
                        tvNoPOilicy.setText(result.message);
                    }
                }

            }
        }
    };

    private final ProtocolPolicyListAdapter.OnAlertSelectedListener mSelectedListener = (model, position) -> {

//            ProtocolPolicyControlFragment fragment = new ProtocolPolicyControlFragment();
//            fragment.policeyKey = model.getPolicyId();
//            fragment.policyTitle = model.getPolicyTitle();
//            fragment.policyType = model.getType();
//            fragment.hostIp = model.getHostIpAdress();
//            fragment.hostName = model.getHostName();
//            fragment.hostID = model.getHostId();
//            fragment.jsonArrSelectedProtocols = model.policyProtocolsLists;
//
////            for (int i = 0; i < model.policyProtocolsLists.length(); i++)
//            getHelper().replaceFragment(fragment, false, true);
        Log.d("PolicyPlanIndex", String.valueOf(position));

    };

    private void showCreatePolicyPrompt(final Context ctx, String msg) {

        new AlertDialog.Builder(ctx)
                .setTitle("d.moat")
                .setMessage(msg)
                .setPositiveButton("devices", (dialog, whichButton) -> {
                    if (ConnectedDevicesCount>0){
                        HostSelectionFragment fragment = new HostSelectionFragment();
                        DashBoardActivity.llBottomTabs.setVisibility(View.GONE);
                        getHelper().addFragmentwitoutTabbar(fragment, false, true);
                    }else{
                        dialog.dismiss();
                        showAlertMsgDialog(getActivity(), getResources().getString(R.string.no_connected_devices));
                    }
                })
                .setNeutralButton("Cancel", (dialog, which) -> dialog.dismiss())
                .setNegativeButton("Network", (dialog, which) -> {
                    ProtocolPolicyControlFragment fragment = new ProtocolPolicyControlFragment();
                    fragment.isForDevice = false;
                    DashBoardActivity.llBottomTabs.setVisibility(View.GONE);
                    getHelper().addFragmentwitoutTabbar(fragment, false, true);

                }).show();

    }


    public static JSONObject getDeleteRequestparams(String appId, String action, int position) {

        JSONObject j = new JSONObject();

        try {
            JSONObject data = new JSONObject();

            data.put("type", listOfProtocolPolicies.get(position).getType());
            data.put("procedure", action);
            data.put("title", listOfProtocolPolicies.get(position).getPolicyTitle());
            data.put("id", listOfProtocolPolicies.get(position).getPolicyId());

            data.put("protocols", listOfProtocolPolicies.get(position).policyProtocolsLists);


            if (listOfProtocolPolicies.get(position).getType().equalsIgnoreCase("device")) {
                data.put("hostname", listOfProtocolPolicies.get(position).getHostName());
                data.put("hostid", listOfProtocolPolicies.get(position).getHostId());
                data.put("ip", listOfProtocolPolicies.get(position).getHostIpAdress());
            }

            j.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            j.put("request_id", Dmoat.getNewRequestId());
            j.put("app_id", appId);
            j.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            j.put("token", UserManager.getInstance().getLoggedInUser().token);
            j.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("PolicPlanCreateParam", j.toString());
        return j;
    }

}
