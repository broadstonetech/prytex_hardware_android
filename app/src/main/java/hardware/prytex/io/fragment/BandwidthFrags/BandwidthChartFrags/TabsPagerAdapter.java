package hardware.prytex.io.fragment.BandwidthFrags.BandwidthChartFrags;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import android.view.MotionEvent;
import android.view.View;

/**
 * Created by macbookpro13 on 14/02/2018.
 */

public class TabsPagerAdapter extends FragmentStatePagerAdapter implements View.OnTouchListener {
    private final int mNumOfTabs;

    public TabsPagerAdapter(FragmentManager fm, int NumOfTabs, Context ctx) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new ProtocolCurrentChartFragment();
            case 1:
                return new ProtocolChartFragment();

            default:
                return null;
        }
    }
    public int getCount() {
        return mNumOfTabs;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}