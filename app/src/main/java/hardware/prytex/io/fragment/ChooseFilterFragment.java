package hardware.prytex.io.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import hardware.prytex.io.R;
import hardware.prytex.io.model.DMoatAlert;

/**
 * Created by khawarraza on 10/11/2016.
 */

@SuppressWarnings("ALL")
public class ChooseFilterFragment extends BaseFragment {

    private SharedPreferences.Editor editor;

    private OnFilterTypeSelectionListener listener;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_choose_alert_type;
    }

    @Override
    public String getTitle() {
        return "View Alerts";
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        SharedPreferences pref = getContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();

        parent.findViewById(R.id.alert_type_dropdown).setOnClickListener(v -> getHelper().onBack());

        parent.findViewById(R.id.filter_all).setOnClickListener(v -> {
            getHelper().onBack();
            editor.putString("mFilterType", "all_alerts");
            editor.apply();
            listener.onFilterItemSelected(new AllFilterType());
        });

        parent.findViewById(R.id.filter_new_host).setOnClickListener(v -> {
            getHelper().onBack();
            editor.putString("mFilterType", "new_host");
            editor.commit();
            listener.onFilterItemSelected(new NewHostFilterType());
        });

        parent.findViewById(R.id.filter_block_ips).setOnClickListener(v -> {
            getHelper().onBack();
            editor.putString("mFilterType", "block_ips");
            editor.commit();
            listener.onFilterItemSelected(new BlockIpsFilterType());
        });

        parent.findViewById(R.id.filter_dmoat_info).setOnClickListener(v -> {
            getHelper().onBack();
            editor.putString("mFilterType", "device_info");
            editor.commit();
//                listener.onFilterItemSelected(new DeviceRestartedFilterType());
            listener.onFilterItemSelected(new DeviceInfoFilterType());
        });

        parent.findViewById(R.id.filter_alert_ids).setOnClickListener(v -> {
            getHelper().onBack();
            editor.putString("mFilterType", "alert_ids");
            editor.commit();
            listener.onFilterItemSelected(new AlertIdsFilterType());
        });

        parent.findViewById(R.id.filter_alert_flows).setOnClickListener(v -> {
            getHelper().onBack();
            editor.putString("mFilterType", "alert_flow");
            editor.commit();
            listener.onFilterItemSelected(new AlertFlowFilterType());
        });

        parent.findViewById(R.id.filter_temperature).setOnClickListener(v -> {
            getHelper().onBack();
            editor.putString("mFilterType", "alert_temp");
            editor.commit();
            listener.onFilterItemSelected(new TemperatureReadingFilter("temperature", "Tempeature"));
        });

        parent.findViewById(R.id.filter_air_quality).setOnClickListener(v -> {
            getHelper().onBack();
            editor.putString("mFilterType", "alert_air");
            editor.commit();
            listener.onFilterItemSelected(new TemperatureReadingFilter("air_quality", "Air Quality"));
        });

        parent.findViewById(R.id.filter_humidity).setOnClickListener(v -> {
            getHelper().onBack();
            editor.putString("mFilterType", "alert_humi");
            editor.commit();
            listener.onFilterItemSelected(new TemperatureReadingFilter("humidity", "Humidity"));
        });

        parent.findViewById(R.id.parent).setOnClickListener(v -> getHelper().onBack());

    }

    public void setFilterTyleListener(OnFilterTypeSelectionListener listener) {
        this.listener = listener;
    }

    interface OnFilterTypeSelectionListener {
        void onFilterItemSelected(FilterType filter);
    }

    public static abstract class FilterType {

        public abstract boolean filter(DMoatAlert alert);

        public String getTitle() {
            return "";
        }
    }

    public static class TemperatureReadingFilter extends FilterType {

        private final String title;
        final String category;

        TemperatureReadingFilter(String category, String title) {
            this.category = category;
            this.title = title;
        }


        @Override
        public boolean filter(DMoatAlert alert) {
            return alert.category != null && alert.category.equals(category);
        }

        @Override
        public String getTitle() {
            return title;
        }
    }

    public static class AllFilterType extends FilterType {

        @Override
        public boolean filter(DMoatAlert alert) {
            return true;
        }

        @Override
        public String getTitle() {
            return "All Alerts";
        }
    }

    public static class NewHostFilterType extends FilterType {

        public String title = "New Host";

        @Override
        public boolean filter(DMoatAlert alert) {
            return alert.inputSrc != null && alert.inputSrc.equals(DMoatAlert.CONNECTED_HOSTS);
        }

        @Override
        public String getTitle() {
            return "New Hosts";
        }
    }

    public static class DeviceInfoFilterType extends FilterType {

//        @Override
//        public boolean filter(DMoatAlert alert) {
//            return alert.channel.startsWith(PubNubDataHandler.CHANNEL_INFO);
//        }

        @Override
        public boolean filter(DMoatAlert alert) {
            return false;
        }

        @Override
        public String getTitle() {
            return "Prytex Hardware Info";
        }
    }

    public static class BlockIpsFilterType extends FilterType {

        @Override
        public boolean filter(DMoatAlert alert) {
////            if (alert.channel.startsWith(PubNubDataHandler.CHANNEL_BLOCK_IPS)) {
//                return true;
//            }
            return false;
        }

        @Override
        public String getTitle() {
            return "Blocked IP Addresses";
        }
    }

    public static class AlertIdsFilterType extends FilterType {

        @Override
        public boolean filter(DMoatAlert alert) {

            return false;
        }

        @Override
        public String getTitle() {
            return "IDS Alert";
        }
    }

    public static class AlertFlowFilterType extends FilterType {

        @Override
        public boolean filter(DMoatAlert alert) {

            return false;
        }

        @Override
        public String getTitle() {
            return "Flow Alert";
        }
    }
}
