package hardware.prytex.io.fragment.themes;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import hardware.prytex.io.R;
import hardware.prytex.io.fragment.BaseFragment;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ALL")
public class ThemeFragment extends BaseFragment {
    ListView theme_lv;
    List<String> list;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_themes;
    }

    @Override
    public String getTitle() {
        return "Themes";
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);
        Log.d("Today","Fragment Created");
        theme_lv = parent.findViewById(R.id.lv_themes);

        list = new ArrayList<>();
        list.add("Advanced");
        list.add("Simplified");

        ThemeAdapter adapter = new ThemeAdapter(getActivity().getApplicationContext(),list);
        theme_lv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    //    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
//        final LayoutInflater layoutInflater = (LayoutInflater)
//                getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        final View v = layoutInflater.inflate(R.layout.fragment_themes, container, false);
//
//        Log.d("Today","Fragment Created");
//        theme_lv = v.findViewById(R.id.lv_themes);
//
//        ThemeAdapter adapter = new ThemeAdapter(getActivity().getApplicationContext());
//        theme_lv.setAdapter(adapter);
//
//
//        return v;
//    }
}
