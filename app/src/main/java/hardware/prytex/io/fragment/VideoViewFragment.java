package hardware.prytex.io.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.VideoView;

import hardware.prytex.io.R;
import hardware.prytex.io.network.Api;


public class VideoViewFragment extends BaseFragment {

    private VideoView videoView;
    @Override
    public int getLayoutId() {
        return R.layout.fragment_video_view;
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        videoView = parent.findViewById(R.id.videoView);

        Uri video = Uri.parse(Api.video_full_url);

        videoView.setVideoURI(video);
        videoView.setOnPreparedListener(mp -> {
            mp.setLooping(true);
            videoView.start();
        });

    }


}
