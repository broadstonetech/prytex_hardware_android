package hardware.prytex.io.fragment.BandwidthFrags;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.activity.DashBoardActivity;
import hardware.prytex.io.adapter.BandwidthAdapter;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.model.BandWidth;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;


import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;
import static hardware.prytex.io.parser.BandwidthParser.listOfProtocolDevices;
import static hardware.prytex.io.parser.BandwidthParser.listOfProtocolDevicesCurrentHour;
import static hardware.prytex.io.util.GeneralUtils.searchBandwidth;

import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("ALL")
public class ProtocolListViewFragment extends BaseFragment {

    private ProgressDialog dialog;
    private RecyclerView mRecyclerView;
    private BandwidthAdapter mAdapter;
    private static final ArrayList<BandWidth> listOfAllBandwidthDevicesUnion = new ArrayList<>();
    private SharedPreferences pref;
    private SearchView searchView24HoursData;

    @Override
    public String getTitle() {
        return "Network Usage";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "network_usage_24hour_list", null /* class override */);

    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);
        DashBoardActivity.isOnDashboardScreen = false;
        imgInfoBtn.setVisibility(View.VISIBLE);
        mRecyclerView = parent.findViewById(R.id.recycler_view);
        searchView24HoursData = parent.findViewById(R.id.searchView);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        listOfAllBandwidthDevicesUnion.clear();
        listOfAllBandwidthDevicesUnion.addAll(listOfProtocolDevicesCurrentHour);
        listOfAllBandwidthDevicesUnion.addAll(listOfProtocolDevices);

        mAdapter = new BandwidthAdapter(getContext(), listOfProtocolDevices, mSelectedListener);
        mRecyclerView.setAdapter(mAdapter);

        searchBandwidth(searchView24HoursData,mAdapter);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_protocol_list_view;
    }

    AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (!isAdded()) {
                return;
            }
            showProgressDialog();
            if (result.code == 200) {
                if (listOfProtocolDevices.size() > 0) {
                    mAdapter = new BandwidthAdapter(getContext(), listOfProtocolDevices, mSelectedListener);
                    mRecyclerView.setAdapter(mAdapter);
                } else {
                    showAlertMsgDialog(getContext(), getResources().getString(R.string.no_bandwidth));
                }

            } else if (result.code == 503) {
                showAlertMsgDialog(getContext(), result.message);
            } else if (result.code == 409) {
                showAlertMsgDialog(getContext(), result.message);
            } else {
                if (result.message.equalsIgnoreCase("")) {
                    showAlertMsgDialog(getContext(), getResources().getString(R.string.error_message_generic));
                } else {
                    showAlertMsgDialog(getContext(), result.message);
                }
            }
        }
    };


    private JSONObject getBandwidthParams() {
        JSONObject js = new JSONObject();
        try {
            js.put("app_id", pref.getString("app_id", ""));
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    private void showProgressDialog() {
        if (false) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private final BandwidthAdapter.OnAlertSelectedListener mSelectedListener = (alert, position) -> {

        BandwidthAdapter.posClicked = position;
        BandwidthHostDetailsFragment fragment = new BandwidthHostDetailsFragment();
        getHelper().replaceFragment(fragment, false, true);

    };


}
