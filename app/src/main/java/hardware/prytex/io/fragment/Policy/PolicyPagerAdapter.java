package hardware.prytex.io.fragment.Policy;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import hardware.prytex.io.fragment.Policy.AccessControl.PolicyControlFragment;
import hardware.prytex.io.fragment.Policy.ProtocolsPolicy.ProtocolPoliciesFragment;

class PolicyPagerAdapter extends FragmentStatePagerAdapter {
    private final int mNumOfTabs;

    public PolicyPagerAdapter(FragmentManager fm, int NumOfTabs, Context ctx) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
              //  DashBoardActivity.llBottomTabs.setVisibility(View.VISIBLE);
                return new PauseInternetFragment();
            case 1:
                  //            PauseInternetFragment tab2 = new PauseInternetFragment();
                return new PolicyControlFragment();
            case 2:
           //              PauseInternetFragment tab3 = new PauseInternetFragment();
                return new ProtocolPoliciesFragment();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
