package hardware.prytex.io.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.discovery_mode.DiscoveyHomeFragment;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.parser.AccountSetup;
import hardware.prytex.io.util.MyToast;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONObject;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;

/**
 * Created by abubaker on 9/27/16.
 */

@SuppressWarnings("ALL")
public class SignUpFragment extends BaseFragment implements View.OnClickListener {


    private EditText nameTxt;
    private EditText usernameTxt;
    private EditText emailTxt;
    private EditText macTxt;
    private TextInputEditText repeatPasswordTxt;
    private TextInputEditText passwordTxt;
    private RadioButton rbTerms;
    private Button btnSignUp;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @Override
    public int getLayoutId() {
        return R.layout.sign_up_fragment;
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();

        nameTxt = parent.findViewById(R.id.et_name);
        usernameTxt = parent.findViewById(R.id.et_username);
        emailTxt = parent.findViewById(R.id.et_email);
        passwordTxt = parent.findViewById(R.id.et_password);
        repeatPasswordTxt = parent.findViewById(R.id.et_repeat_password);
        macTxt = parent.findViewById(R.id.et_mac_address);

        rbTerms = parent.findViewById(R.id.rbTOS);
        RadioButton rbNotAccept = parent.findViewById(R.id.rbNotAccept);
        TextView tvEULA = parent.findViewById(R.id.tvEULA);
        TextView tvPP = parent.findViewById(R.id.tvPrivacyPolicy);
        TextView tvTOS = parent.findViewById(R.id.tvTermsOfServices);

        btnSignUp = parent.findViewById(R.id.create_my_accout);

        registerAfterMacTextChangedCallback();

//        macTxt.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);

        emailTxt.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS);


        parent.findViewById(R.id.create_my_accout).setOnClickListener(this);
        parent.findViewById(R.id.btnProductIdQrScanner).setOnClickListener(this);

        parent.findViewById(R.id.txt_login).setOnClickListener(this);

        watchVideoAlertDialog();

        tvPP.setOnClickListener(this);
        tvTOS.setOnClickListener(this);
        tvEULA.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.txt_login) {

            getHelper().replaceFragment(new LoginFragment(), R.id.fragment_container, true, false);
        }else if(v.getId() == R.id.tvPrivacyPolicy) {


//                AssetsFileName = "dmoat_pp_v1.pdf";
            editor.putString("file_clicked", "pp");
            editor.commit();
            showLegaldoc(Api.dmoatprivacyPolicyDocURL);
            //getHelper().addFragment(new TermsPolicyAgreementFragment(), true, true);
        }else if(v.getId() == R.id.tvTermsOfServices) {


//                AssetsFileName = "dmoat_tos_v1.pdf";
            editor.putString("file_clicked", "tos");
            editor.commit();
            showLegaldoc(Api.dmoatTOSDocURL);
            //getHelper().addFragment(new TermsPolicyAgreementFragment(), true, true);

        }else if(v.getId() == R.id.tvEULA) {


//                AssetsFileName = "dmoat_eula_v1.pdf";
            editor.putString("file_clicked", "eula");
            editor.commit();
            showLegaldoc(Api.dmoatEULADocURL);
            //getHelper().addFragment(new TermsPolicyAgreementFragment(), true, true);
//                getHelper().replaceFragment(new TermsPolicyAgreementFragment(), R.id.fragment_container, true, false);

        }else if(v.getId() == R.id.btnProductIdQrScanner) {


            showAlertMsgDialog(getContext(), getResources().getString(R.string.qr_code_scanning_error));
//                IntentIntegrator.forSupportFragment(this).initiateScan();
        }else if(v.getId() == R.id.create_my_accout){



////                prod_id = macTxt.getText().toString();
//                            getHelper().replaceFragment(new DiscoveyHomeFragment(), R.id.fragment_container, true, false);
                String prod_id = macTxt.getText().toString();
                editor.putString("prodID", prod_id);
                editor.putString("userEmail", emailTxt.getText().toString().toLowerCase());
                editor.commit();
                editor.commit();

                if (validateFields()) {    // allow specail characters so it will be removed by bilawal
                    if (rbTerms.isChecked()) {
                        btnSignUp.setBackgroundColor(getResources().getColor(R.color.colorBtnPressed));
                        JSONObject js = new JSONObject();
                        JSONObject data = new JSONObject();

                        byte[] dataPW;
                        String password = passwordTxt.getText().toString();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            dataPW = password.getBytes(StandardCharsets.UTF_8);
                        }else{

                            dataPW = password.getBytes(Charset.forName("UTF-8"));
                        }
                        String base64Pass = Base64.encodeToString(dataPW, Base64.NO_WRAP);
                        Log.d("Base64PwdReQ", base64Pass);


                        try {
                            js.put("uikey", Api.ACCOUNT_CREATE_UI_KEY);
                            js.put("request_id", Dmoat.getNewRequestId());
                            js.put("app_id",  pref.getString("app_id", ""));

                            data.put("prod_id", macTxt.getText().toString());
                            data.put("email", emailTxt.getText().toString());
                            data.put("password", base64Pass);
                            data.put("user_name", usernameTxt.getText().toString());
                            data.put("name", nameTxt.getText().toString());
                            js.put("data", data);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        getHelper().showProgressBar();
                        Api.accountSetup(getActivity(), js, registerCallback);

                    } else {
                        Toast.makeText(getContext(), "Please accept terms of service to continue.", Toast.LENGTH_LONG).show();
                    }
                }
        }
    }

    private void showLegaldoc(String urlString) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(urlString));
        intent.setPackage("com.android.chrome");
        startActivity(intent);
    }
    private final AsyncTaskListener registerCallback = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            btnSignUp.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            if (!isAdded()) {
                return;
            }
            getHelper().hideProgressBar();
            if (result.message.equalsIgnoreCase("")){
                MyToast.showMessage(getActivity(), getContext().getResources().getString(R.string.error_message_generic));
            }else {
                MyToast.showMessage(getActivity(), result.message);
            }
            //go to discovery home screen after successfull signup
            if (AccountSetup.isSuccessFullySignedUp) {
                if (result.code == 200 || result.code == 2) {
//                    registerForFreshchat(getContext(), emailTxt.getText().toString(), nameTxt.getText().toString(), usernameTxt.getText().toString());
//                getHelper().replaceFragment(new DiscoveyHomeFragment(), R.id.fragment_container, true, false);
                    getHelper().addFragment(new DiscoveyHomeFragment(), true, true);
                }
            }
        }
    };

    private boolean validateFields() {
        if (nameTxt.getText().toString().length() == 0) {
            nameTxt.setError(getResources().getString(R.string.err_msg_name));
            return false;
        }
        if (usernameTxt.getText().toString().length() == 0) {
            usernameTxt.setError(getResources().getString(R.string.err_msg_username));
            return false;
        }
        if (emailTxt.getText().toString().length() == 0) {
            emailTxt.setError(getResources().getString(R.string.err_msg_email));
            return false;
        }
        if (!InvitePeopleFragment.isEmailValid(emailTxt.getText().toString())) {
            emailTxt.setError(getResources().getString(R.string.err_msg_email));
            return false;
        }
        if (passwordTxt.getText().toString().length() == 0) {
            passwordTxt.setError(getResources().getString(R.string.err_msg_password));
            return false;
        }
        if (passwordTxt.getText().toString().length() < 8) {
            passwordTxt.setError(getResources().getString(R.string.err_msg_password_validation));
            return false;
        }

        if (isValidPassword(passwordTxt.getText().toString())) {
            passwordTxt.setError(getResources().getString(R.string.err_msg_password_validation));
            return false;
        }
        if (repeatPasswordTxt.getText().toString().length() == 0) {
            repeatPasswordTxt.setError("Retype password!");
            return false;
        }
        if (!passwordTxt.getText().toString().equals(repeatPasswordTxt.getText().toString())) {
            passwordTxt.setError(getResources().getString(R.string.err_msg_password_mismatch));
            passwordTxt.setText("");
            repeatPasswordTxt.setText("");
            return false;
        }
        if (macTxt.getText().toString().length() < 8 ) {
            macTxt.setError(getResources().getString(R.string.qr_code_mismatch));
            return false;
        }

        return true;
    }



    private void registerAfterMacTextChangedCallback() {
        macTxt.addTextChangedListener(new TextWatcher() {
            String mPreviousMac = null;

            /* (non-Javadoc)
             * Does nothing.
             * @see android.text.TextWatcher#afterTextChanged(android.text.Editable)
             */
            @Override
            public void afterTextChanged(Editable arg0) {
            }

            /* (non-Javadoc)
             * Does nothing.
             * @see android.text.TextWatcher#beforeTextChanged(java.lang.CharSequence, int, int, int)
             */
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            /* (non-Javadoc)
             * Formats the MAC address and handles the cursor position.
             * @see android.text.TextWatcher#onTextChanged(java.lang.CharSequence, int, int, int)
             */
            @SuppressLint("DefaultLocale")
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String enteredMac = macTxt.getText().toString().toUpperCase().trim();
                String cleanMac = clearNonMacCharacters(enteredMac);
                String formattedMac = formatMacAddress(enteredMac);

                int selectionStart = macTxt.getSelectionStart();
                formattedMac = handleColonDeletion(enteredMac, formattedMac, selectionStart);
                int lengthDiff = formattedMac.length() - enteredMac.length();

                setMacEdit(enteredMac, formattedMac, selectionStart, lengthDiff);
            }

            /**
             * Strips all characters from a string except A-F and 0-9.
             * @param mac       User input string.
             * @return String containing MAC-allowed characters.
             */
            private String clearNonMacCharacters(String mac) {
                return mac.replaceAll("[^*A-Za-z0-9@#$&]", "");
            }

            /**
             * Adds a colon character to an unformatted MAC address after
             * every second character (strips full MAC trailing colon)
             * @param cleanMac      Unformatted MAC address.
             * @return Properly formatted MAC address.
             */
            private String formatMacAddress(String cleanMac) {
                int grouppedCharacters = 0;
                StringBuilder formattedMac = new StringBuilder();

                for (int i = 0; i < cleanMac.length(); ++i) {
                    formattedMac.append(cleanMac.charAt(i));
                    ++grouppedCharacters;

                    if (grouppedCharacters == 4) {
//                        formattedMac += "-";
                        grouppedCharacters = 0;
                    }else{
                    }
                }

                // Removes trailing colon for complete MAC address
                if (cleanMac.length() == 9) {
                    formattedMac = new StringBuilder(formattedMac.substring(0, formattedMac.length() - 1));
                }


                return formattedMac.toString();
            }

            /**
             * Upon users colon deletion, deletes MAC character preceding deleted colon as well.
             * @param enteredMac            User input MAC.
             * @param formattedMac          Formatted MAC address.
             * @param selectionStart        MAC EditText field cursor position.
             * @return Formatted MAC address.
             */
            private String handleColonDeletion(String enteredMac, String formattedMac, int selectionStart) {
                if (mPreviousMac != null && mPreviousMac.length() > 1) {
                    int previousColonCount = colonCount(mPreviousMac);
                    int currentColonCount = colonCount(enteredMac);

                    if (currentColonCount < previousColonCount) {
                        formattedMac = formattedMac.substring(0, selectionStart - 1) + formattedMac.substring(selectionStart);
                        String cleanMac = clearNonMacCharacters(formattedMac);
                        formattedMac = formatMacAddress(cleanMac);
                    }
                }
                return formattedMac;
            }

            /**
             * Gets MAC address current colon count.
             * @param formattedMac      Formatted MAC address.
             * @return Current number of colons in MAC address.
             */
            private int colonCount(String formattedMac) {
                return formattedMac.replaceAll("[^:]", "").length();
            }

            /**
             * Removes TextChange listener, sets MAC EditText field value,
             * sets new cursor position and re-initiates the listener.
             * @param cleanMac          Clean MAC address.
             * @param formattedMac      Formatted MAC address.
             * @param selectionStart    MAC EditText field cursor position.
             * @param lengthDiff        Formatted/Entered MAC number of characters difference.
             */
            private void setMacEdit(String cleanMac, String formattedMac, int selectionStart, int lengthDiff) {
                macTxt.removeTextChangedListener(this);
                if (cleanMac.length() <= 32) {
                    macTxt.setText(formattedMac);
                    macTxt.setSelection(selectionStart + lengthDiff < 0 ? 0 : selectionStart + lengthDiff);
                    mPreviousMac = formattedMac;
                } else {
                    macTxt.setText(mPreviousMac);
                    macTxt.setSelection(mPreviousMac.length());
                }
                macTxt.addTextChangedListener(this);
            }
        });
    }

    // Get the results of qr code scanner:
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            Log.d("RequestCodeResultCode==", String.valueOf(resultCode) + requestCode);
            if (data == null ){

            }
           else if (result.getContents() == null) {
                if (data == null || resultCode == 0){
                    Toast.makeText(getContext(), "Camera permission required for scanning.", Toast.LENGTH_LONG).show();
                    TurnCameraPermissionOn();
                }else{

                }


            } else {
//                Toast.makeText(getContext(), "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
                macTxt.setText(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void TurnCameraPermissionOn() {
        new AlertDialog.Builder(getContext())
                .setTitle("Prytex Hardware")
                .setMessage("Camera permission required for scanning.Turn ON for scanning qr code.")
                .setPositiveButton("Turn ON", (dialog, whichButton) -> {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", getContext().getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                })
                .setNegativeButton("cancel", null).show();
    }

    private void watchVideoAlertDialog() {
        new AlertDialog.Builder(getContext())
                .setTitle("Prytex Hardware")
                .setMessage("Let\'s get started with account setup..")
                .setPositiveButton("View", (dialog, whichButton) -> {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Api.video_account_setup_url));
                    startActivity(browserIntent);
                })
                .setNegativeButton("Skip", null).show();
    }

}