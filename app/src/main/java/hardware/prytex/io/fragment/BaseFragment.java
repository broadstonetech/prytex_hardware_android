package hardware.prytex.io.fragment;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import hardware.prytex.io.activity.DashBoardActivity;
import hardware.prytex.io.config.Constants;
import hardware.prytex.io.util.ImageUtils;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class BaseFragment extends Fragment  {

    private FragmentNavigationHelper fragmentHelper;
    private SharedPreferences prefs, settingsPrefs;
    private View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentHelper = (FragmentNavigationHelper) context;
        } catch (Exception ignored) {
        }

        prefs = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        settingsPrefs = context.getSharedPreferences(Constants.SETTINGS_PREFS_NAME, Context.MODE_PRIVATE);
//        settingsPrefs = activity.getSharedPreferences(UserManager.USER_PREFS, Context.MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(getLayoutId(), container, false);
        view.setClickable(true);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view, savedInstanceState);

    }

    protected FragmentNavigationHelper getHelper() {
        return this.fragmentHelper;
    }

    protected View getParentView() {
        return this.view;
    }

    protected abstract int getLayoutId();


    public void initViews(View parent, Bundle savedInstanceState) {

    }


    protected SharedPreferences getUserSettingsSharedPreferences() {
        return settingsPrefs;
    }



    public String getTitle() {
        return "";
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        hideKeyboard();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return false;
    }

    void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(Objects.requireNonNull(getView()).getWindowToken(), 0);
    }

    void setCursorVisibility(EditText editText) {
        editText.setCursorVisible(true);
    }

    public boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[$&+,:~{}_\";=\\\\?@#|/'<>.^*()%!-])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return !matcher.matches();

    }

    /**
     * An interface to load and make navigation. The parent activity must provide an implementation for this interface.
     */
    public interface FragmentNavigationHelper {

        void addFragment(BaseFragment f, boolean clearBackStack, boolean addToBackstack);

        void replaceFragment(BaseFragment f, boolean clearBackStack, boolean addToBackstack);

        void replaceFragment(BaseFragment f, int layoutId, boolean clearBackStack, boolean addToBackstack);

        void onBack();

        void showProgressBar();

        void hideProgressBar();

        void addFragmentwitoutTabbar(BaseFragment f, boolean clearBackStack, boolean addToBackstack);


    }


}
