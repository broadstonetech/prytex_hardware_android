package hardware.prytex.io.fragment;


import android.app.ProgressDialog;
import android.content.Context;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.activity.DashBoardActivity;
import hardware.prytex.io.model.DMoatAlert;
import hardware.prytex.io.model.NetConfig;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.util.DmoatUtils;
import hardware.prytex.io.util.FieldValidator;
import hardware.prytex.io.util.GeneralUtils;
import hardware.prytex.io.util.MyToast;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Objects;

import static hardware.prytex.io.parser.NetConfigParser.netConfigObj;

/**
 * Created by khawarraza on 15/12/2016.
 */

@SuppressWarnings("ALL")
public class NetConfigFragment extends BaseFragment {

    private EditText ipTxt;
    private EditText ip_bTxt;
    private EditText startIpTxt;
    private EditText startIpTxt_b;
    private EditText endIpTxt;
    private EditText endIpTxt_b;
    private EditText defaultLeaseTimeTxt;
    private EditText maxLeaseTimeTxt;
    private TextView netMaskTxt;
    private TextView routerIpTxt;
    private TextView broadcastTxt;
    private TextView subNetTxt;
    private String ip;
    private String netMask;
    private String subNet;
    private String startIp;
    private String endIp;
    private String routerIp;
    private String defaultLeaseTime;
    private String maxLeaseTime;
    private String broadcast;
    DMoatAlert alert;
    private static final String FIRST_IP_START = "192.168";
    private static final String SECOND_IP_START = "172.16.";
    private static final String THIRD_IP_START = "10.";

    //    public static NetConfig alertNetconfig;
    private ProgressDialog dialog;
    private SharedPreferences pref;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_net_config;
    }

    @Override
    public String getTitle() {
        return "Advanced Mode";
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);

        ipTxt = parent.findViewById(R.id.ip);
        ip_bTxt = parent.findViewById(R.id.ip_b);
        netMaskTxt = parent.findViewById(R.id.netmask);
        subNetTxt = parent.findViewById(R.id.subnet);
        startIpTxt = parent.findViewById(R.id.start_ip_range);
        startIpTxt_b = parent.findViewById(R.id.start_ip_range_b);
        endIpTxt = parent.findViewById(R.id.end_ip_range);
        endIpTxt_b = parent.findViewById(R.id.end_ip_range_b);
        routerIpTxt = parent.findViewById(R.id.router_ip);
        defaultLeaseTimeTxt = parent.findViewById(R.id.default_lease_time);
        maxLeaseTimeTxt = parent.findViewById(R.id.max_lease_time);
        broadcastTxt = parent.findViewById(R.id.broadcast);


//        String actionChannel = intent.getAction();
//        Bundle extras = intent.getExtras();
//        PubNubResult result = (PubNubResult) extras.get("data");
//        Log.d("NetConfig==", alertNetconfig.toString());
//        subNet = alertNetconfig.subnet;
//        ip = alertNetconfig.ip;
//        netMask = alertNetconfig.netmask;
//        startIp = alertNetconfig.start_ip_range;
//        endIp = alertNetconfig.end_ip_range;
//        routerIp = alertNetconfig.router_ip;
//        maxLeaseTime = "1";//alertNetconfig.max_lease_time;
//        defaultLeaseTime = "2";//alertNetconfig.default_lease_time;
//        broadcast = alertNetconfig.broadcast;


////        ip, netMask,, startIp, endIp, routerIp, defaultLeaseTime, maxLeaseTime, broadcast
//        splitAndSetIp(alertNetconfig.ip, ipTxt, ip_bTxt, false);
//        if (alertNetconfig.start_ip_range.equalsIgnoreCase("")) {
//            splitAndSetIp(alertNetconfig.router_ip, startIpTxt, startIpTxt_b, true);
//        } else {
//            splitAndSetIp(alertNetconfig.start_ip_range, startIpTxt, startIpTxt_b, false);
//        }
//        if (alertNetconfig.end_ip_range.equalsIgnoreCase("")) {
//            splitAndSetIp(alertNetconfig.router_ip, endIpTxt, endIpTxt_b, true);
//        } else {
//            splitAndSetIp(alertNetconfig.end_ip_range, endIpTxt, endIpTxt_b, false);
//        }
//        netMaskTxt.setText(alertNetconfig.netmask);
//        subNetTxt.setText(alertNetconfig.subnet);
//        routerIpTxt.setText(alertNetconfig.router_ip);
//        defaultLeaseTimeTxt.setText(String.valueOf(alertNetconfig.default_lease_time));
//        maxLeaseTimeTxt.setText(String.valueOf(alertNetconfig.max_lease_time));
//        broadcastTxt.setText(alertNetconfig.broadcast);
//
//        ipTxt.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
//        netMaskTxt.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
//        subNetTxt.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
//        startIpTxt.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
//        endIpTxt.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
//        routerIpTxt.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
//        broadcastTxt.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
//
        parent.findViewById(R.id.save_net_config).setOnClickListener(v -> {
            ip = ipTxt.getText().toString() + ip_bTxt.getText().toString();
            startIp = startIpTxt.getText().toString() + startIpTxt_b.getText().toString();
            endIp = endIpTxt.getText().toString() + endIpTxt_b.getText().toString();
            netMask = netMaskTxt.getText().toString();
            subNet = subNetTxt.getText().toString();
            routerIp = routerIpTxt.getText().toString();
            defaultLeaseTime = defaultLeaseTimeTxt.getText().toString();
            maxLeaseTime = maxLeaseTimeTxt.getText().toString();
            broadcast = broadcastTxt.getText().toString();

            if (dataValidated()) {

                submitAdvanceModeNetConfig();
            }
            Toast.makeText(getContext(),"Dmoat is in Advance mode,Please make sure Your local router DHCP server is disabled",Toast.LENGTH_LONG).show();

        });
//
//
//        ipTxt.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                int cursorPosition = ipTxt.getSelectionStart();
//
//                // Check if the previous character is "," and move your cursor
//                if (s.charAt(cursorPosition - 1) == '.') {
//                    ipTxt.setSelection(cursorPosition - 2);
//                    Log.d("NetConfg", "is . dot");
//                }
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
        showProgressDialog(true);
        callAdvanceModeApi();

    }
    private final AsyncTaskListener mGetNetConfigListener = result -> {

        showProgressDialog(false);
        if (result.code == 200 || result.code == 2) {
            if(netConfigObj != null)
            netConfigdata(netConfigObj);
            else
            {
                Toast.makeText(getContext(),"No data found",Toast.LENGTH_LONG).show();
            }
            Log.d("NET_CONFIG", result.toString().trim());
        }
        else if(result.code == 400) {

              showAlertMsgDialog(getContext(),getResources().getString(R.string.error_message_generic));
        }
        else
        {
            showAlertMsgDialog(getContext(),getResources().getString(R.string.error_message_generic));

        }
    };

    public void showAlertMsgDialog(Context ctx, String msg) {
        if (msg.equalsIgnoreCase("") || msg == null || msg.isEmpty() || msg.equals(null)) {
            msg = ctx.getResources().getString(R.string.error_message_generic);
        } else {
        }
        new AlertDialog.Builder(ctx)
                .setTitle("Prytex Hardware")
                .setMessage(msg)
                .setPositiveButton("Ok", (dialogInterface, i) -> gotoProfileFragment())
                .show();

    }

    private void gotoProfileFragment(){
        DashBoardActivity.setBottomNavTextColors(getActivity(), false, false, true, false);
        ((DashBoardActivity) Objects.requireNonNull(getActivity())).addFragment(new ProfileFragment(), true, true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
//        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "advance_mode", null /* class override */);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, "advance_mode");
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, this.getClass().getName());

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);
    }

    private boolean dataValidated() {
        if (!FieldValidator.isValidFieldData(ipTxt.getText().toString() + ip_bTxt.getText().toString(), ip_bTxt, getResources().getString(R.string.err_msg_all_fields_req)))
            return false;
//        if (!FieldValidator.isValidFieldData(netMaskTxt, getResources().getString(R.string.err_msg_all_fields_req)))
//            return false;
//        if (!FieldValidator.isValidFieldData(subNetTxt, getResources().getString(R.string.err_msg_all_fields_req)))
//            return false;
        if (!FieldValidator.isValidFieldData(startIpTxt.getText().toString() + startIpTxt_b.getText().toString(), startIpTxt_b, getResources().getString(R.string.err_msg_all_fields_req)))
            return false;
        if (!FieldValidator.isValidFieldData(endIpTxt.getText().toString() + endIpTxt_b.getText().toString(), endIpTxt_b, getResources().getString(R.string.err_msg_all_fields_req)))
            return false;
//        if (!FieldValidator.isValidFieldData(routerIpTxt, getResources().getString(R.string.err_msg_all_fields_req)))
//            return false;
//        if (!FieldValidator.isValidFieldData(broadcastTxt, getResources().getString(R.string.err_msg_all_fields_req)))
//            return false;
        if (!FieldValidator.validLeaseTime(defaultLeaseTimeTxt, maxLeaseTimeTxt))
            return false;
        if (!validateIPClass()) {
            MyToast.showMessage(getActivity(), "Please enter IP addresses within the allowed range.");
            return false;
        }
        if (defaultLeaseTime.equalsIgnoreCase("") || maxLeaseTime.equalsIgnoreCase("")) {
            MyToast.showMessage(getActivity(), "Default lease time should less than Max lease time.");
            return false;
        }
        if (defaultLeaseTime.equalsIgnoreCase("0") || maxLeaseTime.equalsIgnoreCase("0")) {
            MyToast.showMessage(getActivity(), "Lease time should not be zero.");
            return false;
        }
        if (defaultLeaseTime.equalsIgnoreCase(maxLeaseTime) || maxLeaseTime.equalsIgnoreCase(defaultLeaseTime)) {
            MyToast.showMessage(getActivity(), "Default lease time and Max lease time should not same.");
            return false;
        }
        if (startIp.equalsIgnoreCase(endIp)) {
            MyToast.showMessage(getActivity(), "Start IP and end IP should not be same.");
            return false;
        }
        return compareIPs();
    }

    private boolean validateIPClass() {
        if (!FieldValidator.isPrivateIP(ipTxt, ip_bTxt)) return false;
        if (!FieldValidator.isPrivateIP(startIpTxt, startIpTxt_b)) return false;
        if (!FieldValidator.isPrivateIP(endIpTxt, endIpTxt_b)) return false;
//        if (!FieldValidator.isPrivateIP(subNetTxt)) return false;
//        if (!FieldValidator.isPrivateIP(startIpTxt)) return false;
//        if (!FieldValidator.isPrivateIP(endIpTxt)) return false;
//        if (!FieldValidator.isPrivateIP(routerIpTxt)) return false;
//        if (!FieldValidator.isPrivateIP(broadcastTxt)) return false;

        return validateIPGroup();
    }

    private boolean validateIPGroup() {
        String[] ipTokens = ip.split("\\.");
        String ipToken = "";
        if (ipTokens.length > 0) {
            ipToken = ipTokens[0] + "." + ipTokens[1];
        }
        if (!subNet.startsWith(ipToken)) {
            return false;
        } else if (!routerIp.startsWith(ipToken)) {
            return false;
        } else if (!startIp.startsWith(ipToken)) {
            return false;
        } else if (!endIp.startsWith(ipToken)) {
            return false;
        } else return broadcast.startsWith(ipToken);
    }


    private boolean compareIPs() {
        if (!FieldValidator.ipComparsionValidity(ipTxt, ip_bTxt, startIpTxt, startIpTxt_b, "IP Address must be smaller that start IP " +
                "Address")) return false;
        if (!FieldValidator.ipComparsionValidity(startIpTxt, startIpTxt_b, endIpTxt, endIpTxt_b, "Start IP must be smaller than End IP"))
            return false;
        try {
            if (DmoatUtils.ipToLong(InetAddress.getByName(routerIp)) >= DmoatUtils.ipToLong(InetAddress.getByName
                    (startIp)) && DmoatUtils.ipToLong(InetAddress.getByName(routerIp)) <= DmoatUtils.ipToLong
                    (InetAddress.getByName(endIp))) {
//                routerIpTxt.setError("Router Ip must not lie between start and end Ip");
                startIpTxt_b.setError("Router Ip must not lie between start and end Ip");
                return false;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        if (ip.equalsIgnoreCase(routerIp)) {
//            routerIpTxt.setError("Router Ip must not be equal to IP");
            ip_bTxt.setError("Ip must not be equal to Router IP");
            return false;
        }
        return true;
    }

    private void submitAdvanceModeNetConfig() {
        String ip = ipTxt.getText().toString() + ip_bTxt.getText().toString();
        String netMask = netMaskTxt.getText().toString();
        String subnet = subNetTxt.getText().toString();
        String startIp = startIpTxt.getText().toString() + startIpTxt_b.getText().toString();
        String endIp = endIpTxt.getText().toString() + endIpTxt_b.getText().toString();
        String routerIp = routerIpTxt.getText().toString();
        String defaultLeaseTime = defaultLeaseTimeTxt.getText().toString();
        String maxLeaseTime = maxLeaseTimeTxt.getText().toString();
        String broadcast = broadcastTxt.getText().toString();


        JSONObject js = new JSONObject();
        int last_request_id = js.optInt("request_id");
        JSONObject data = new JSONObject();
        try {
            JSONObject n = new JSONObject();
            n.put("iface", "eth0");
            n.put("ip", ip);
            n.put("netmask", netMask);
            n.put("subnet", subnet);
            n.put("start_ip_range", startIp);
            n.put("end_ip_range", endIp);
            n.put("router_ip", routerIp);
            n.put("default_lease_time", defaultLeaseTime);
            n.put("max_lease_time", maxLeaseTime);
            n.put("broadcast", broadcast);

            data.put("net_config", n);
            data.put("request", "change_boot_mode");
            data.put("mode", 1);

            js.put("request_id", Dmoat.getNewRequestId());
            js.put("app_id", pref.getString("app_id", ""));
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);

            js.put("data", data);

            if (GeneralUtils.isConnected(getContext())) {
                showProgressDialog(true);
                Api.changeMode(getActivity(), js, mChangeModeListener);
            } else {
                showAlertMsgDialog(getContext(), getResources().getString(R.string.internet_connection));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final AsyncTaskListener mChangeModeListener = result -> {

        showProgressDialog(false);
        if (result.code == 200 || result.code == 2) {
            getUserSettingsSharedPreferences().edit().putInt(DashBoardFragment.MODE + "_" + UserManager
                    .getInstance().getLoggedInUser().userName, DashBoardFragment
                    .MODE_ADVANCE).apply();
//                showGuideAlert();
//                MyToast.showMessage(getActivity(), "Mode Changed Successfully!!!");
            getHelper().replaceFragment(new ProfileFragment(), false, false);
            Log.d("MODE", "Mode changed successfully");

        } else if (result.code == 503) {
            showAlertMsgDialog(getContext(), result.message);
        } else if (result.code == 409) {
            showAlertMsgDialog(getContext(), result.message);
        } else {
            if (result.message.equalsIgnoreCase("")) {
                MyToast.showMessage(getActivity(), getResources().getString(R.string.error_message_generic));
            } else {
                MyToast.showMessage(getActivity(), result.message);
            }
        }
    };

    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }


    private void splitAndSetIp(String ipRcvd, TextView tvA, TextView tvB, boolean isStartEndRouter) {
        String guess = ".";
        int finalIndex = -1;
        for (int index = ipRcvd.indexOf(guess);
             index >= 0;
             index = ipRcvd.indexOf(guess, index + 1)) {
            Log.d("lastIndex", String.valueOf(index));
            finalIndex = index;
        }
        int splitPoint = finalIndex + 1;
        String firstPart = ipRcvd.substring(0, splitPoint);  // get everything upto the first space character
        String lastPart = ipRcvd.substring(splitPoint).trim();

        tvA.setText(firstPart);
        if (isStartEndRouter) {
            tvB.setText("");
        } else {
            tvB.setText(lastPart);
        }
    }



    private void callAdvanceModeApi() {
        try {
            JSONObject js = new JSONObject();
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("app_id", pref.getString("app_id", ""));
//                js.put("app_id", Dmoat.getAppId());
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            int last_mode_request_id = js.optInt("request_id");
            Api.getNetConfig(getActivity(), js, mGetNetConfigListener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void netConfigdata(NetConfig alertNetconfig)
    {


        subNet = alertNetconfig.getSubnet();
        ip = alertNetconfig.getIp();
        netMask = alertNetconfig.getNetmask();
        startIp = alertNetconfig.getStart_ip_range();
        endIp = alertNetconfig.getEnd_ip_range();
        routerIp = alertNetconfig.getRouter_ip();
        maxLeaseTime = "1";//alertNetconfig.max_lease_time;
        defaultLeaseTime = "2";//alertNetconfig.default_lease_time;
        broadcast = alertNetconfig.getBroadcast();


//        ip, netMask,, startIp, endIp, routerIp, defaultLeaseTime, maxLeaseTime, broadcast
        splitAndSetIp(alertNetconfig.ip, ipTxt, ip_bTxt, false);
        if (alertNetconfig.start_ip_range.equalsIgnoreCase("")) {
            splitAndSetIp(alertNetconfig.router_ip, startIpTxt, startIpTxt_b, true);
        } else {
            splitAndSetIp(alertNetconfig.start_ip_range, startIpTxt, startIpTxt_b, false);
        }
        if (alertNetconfig.end_ip_range.equalsIgnoreCase("")) {
            splitAndSetIp(alertNetconfig.router_ip, endIpTxt, endIpTxt_b, true);
        } else {
            splitAndSetIp(alertNetconfig.end_ip_range, endIpTxt, endIpTxt_b, false);
        }
        netMaskTxt.setText(alertNetconfig.netmask);
        subNetTxt.setText(alertNetconfig.subnet);
        routerIpTxt.setText(alertNetconfig.router_ip);
        defaultLeaseTimeTxt.setText(String.valueOf(alertNetconfig.default_lease_time));
        maxLeaseTimeTxt.setText(String.valueOf(alertNetconfig.max_lease_time));
        broadcastTxt.setText(alertNetconfig.broadcast);

                ipTxt.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        netMaskTxt.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        subNetTxt.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        startIpTxt.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        endIpTxt.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        routerIpTxt.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        broadcastTxt.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);


    }
}


