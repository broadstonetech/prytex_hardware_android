package hardware.prytex.io.fragment.AlertsTabs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.activity.AlertsMainActivity;
import hardware.prytex.io.activity.DashBoardActivity;
import hardware.prytex.io.adapter.ViewAlertsAdapter;
import hardware.prytex.io.fragment.AlertActionsFragment;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.fragment.ChooseFilterFragment;
import hardware.prytex.io.model.AlertAction;
import hardware.prytex.io.model.DMoatAlert;
import hardware.prytex.io.model.MutedBlockedAlertsModel;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.util.GeneralUtils;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;
import static hardware.prytex.io.activity.AlertsMainActivity.isFirstCallViewAlerts;
import static hardware.prytex.io.activity.AlertsMainActivity.tabLayout;
import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;
import static hardware.prytex.io.parser.ViewAlersParser.listOfAllViewAlerts;

/**
 * Created by khawarraza on 28/11/2016.
 */

@SuppressWarnings("ALL")
public class ViewAlertsAdvancedFragment extends BaseFragment {

    private RecyclerView mRecyclerView;
    private ViewAlertsAdapter mAdapter;
    List<DMoatAlert> mData = null;
    ArrayList<DMoatAlert> sortedList = null;
    private Context context;
    private SharedPreferences pref;
    private ChooseFilterFragment.FilterType typeFilter;
    private ChooseFilterFragment.FilterType mFilter;
    private ProgressDialog dialog;
    private static boolean isViewingAlerts = false;
    public static boolean isSuccesAlertResponse = false;
    private MaterialSpinner spinner;
    private String appID = "";
    private RecyclerView mRecyclerViewswip;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isViewingAlerts = true;

        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
//        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "view_alerts", null /* class override */);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, "view_alerts");
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, this.getClass().getName());

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_view_alerts_advanced;
    }

    @Override
    public String getTitle() {
        return "View Alerts";
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);

        appID = pref.getString("app_id", "");
        context = getContext();

        AlertsMainActivity.alertfilterItems.add("All");
        AlertsMainActivity.alertfilterItems.add("Sensor");
        AlertsMainActivity.alertfilterItems.add("Muted");
//        AlertsMainActivity.alertfilterItems.add("Blocked");
//        AlertsMainActivity.alertfilterItems.add("Ids");
        AlertsMainActivity.alertfilterItems.add("Devices");

        TextView selectedFilterTitle = parent.findViewById(R.id.selected_type);

        LinearLayout llViewAlert = parent.findViewById(R.id.llViewAlerts);

        spinner = parent.findViewById(R.id.spinner);

        mRecyclerView = parent.findViewById(R.id.recycler_view);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        parent.findViewById(R.id.alert_type_dropdown).setVisibility(View.GONE);


        imgInfoBtn.setVisibility(View.VISIBLE);
        DashBoardActivity.isOnDashboardScreen = false;
//        PubNubDBReceiver.removeNotificaitonFromStatusBar(getActivity().getApplicationContext(), PubNubDBReceiver.GROUP_NOTIFICATION_ID);

        mRecyclerView.setOnTouchListener((view, motionEvent) -> {
//                mAdapter.notifyDataSetChanged();
            return false;
        });
        if (isFirstCallViewAlerts || listOfAllViewAlerts.size() < 1) {
            showProgressDialog(true);
            Api.getViewAlerts(getActivity(), getRequestParams(), listener);
            isFirstCallViewAlerts = false;
        } else {
            if (listOfAllViewAlerts.size() > 0) {
                loadDataInAdapter();
            } else {
                getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
            }
        }

                spinner.setItems("All Alerts", "New Host", "Sensor", "Prytex Hardware Info");
                spinner.setOnItemSelectedListener((MaterialSpinner.OnItemSelectedListener<String>) (view, position, id, item) -> updateAlertsFilter(item));

        mRecyclerViewswip = parent.findViewById(R.id.recycler_viewswip);
        mSwipeRefreshLayout = parent.findViewById(R.id.cnctdhosts_swipe_refresh_layout);
        mRecyclerViewswip.setLayoutManager(new LinearLayoutManager(getActivity()));

        mRecyclerViewswip.setVisibility(View.GONE);
        if (GeneralUtils.isConnected(getActivity())) {
           spinner.setSelectedIndex(0);

        } else {
           showAlertMsgDialog(getActivity(), getResources().getString(R.string.internet_connection));
        }

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mRecyclerViewswip.setVisibility(View.GONE);
            refreshContent();
            spinner.setSelectedIndex(0);
        });
    }

    @Override
    public void onDestroy() {
        isViewingAlerts = false;
        super.onDestroy();
    }

    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private final ViewAlertsAdapter.OnAlertSelectedListener mAlertSelectedListener = new ViewAlertsAdapter.OnAlertSelectedListener() {
        @Override
        public void onAlertSelected(MutedBlockedAlertsModel alert) {

            ArrayList<AlertAction> actions = null;
            if (alert.getInput_src() == MutedBlockedAlertsModel.NEW_HOST_ALERT && alert.getDeviceCategory().equalsIgnoreCase("Router")) {
                showAlertMsgDialog(context, context.getResources().getString(R.string.router_no_response));
            } else {
                if (alert.getInput_src().equals(DMoatAlert.FLOW)) {
                    // add flow actions allow  and mute if type is normal
                    actions = DMoatAlert.getFlowAllAction();
                } else if (alert.getInput_src().equals(MutedBlockedAlertsModel.IDS_ALERT) && alert.getIdsBlockType().equals(DMoatAlert.BLOCKTYPE_PRE)) {
                    actions = DMoatAlert.getIDSPreActions();
                } else if (alert.getInput_src().equals(MutedBlockedAlertsModel.IDS_ALERT) && alert.getIdsBlockType().equals(DMoatAlert.BLOCKTYPE_POST)) {
                    actions = DMoatAlert.getIDSPostActions();
                } else if (alert.getInput_src().equals(DMoatAlert.CONNECTED_HOSTS)) {
                    actions = DMoatAlert.getIDSPostActions();
                } else if (alert.getInput_src().equalsIgnoreCase(DMoatAlert.BLOCK_IPs)) {
                    actions = DMoatAlert.getBlockIpsAction();
                }

                if (actions != null) {
                    AlertActionsFragment actionsFragment = new AlertActionsFragment();
                    actionsFragment.setActionItems(actions);
                    actionsFragment.setAlert(alert);
                    actionsFragment.setAlertActionListener((action, alert1) -> {

                        String appId = pref.getString("app_id", "");
                        final JSONObject data = DMoatAlert.createResponsePayload(getActivity(), alert1, action, appId);

                        Log.d("JSON==>", data.toString());
                        String url = DMoatAlert.getResponseUrl(alert1);
                        if (url != null) {
//                                showProgressDialog(true);
                            if (isDmoatOnline) {
                                if (UserManager.checkPauseInternetTimeStatus()) {
                                    Intent intent = new Intent(getActivity(), AlertsMainActivity.class);
                                    startActivity(intent);
                                    mAdapter.alertResponseCall(alert1, url, data);
                                } else {
                                    Toast.makeText(getContext(), getResources().getString(R.string.paused_internet_msg), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), getResources().getString(R.string.dmoat_offline), Toast.LENGTH_SHORT).show();
                            }

                        }

                    });
                    getHelper().addFragment(actionsFragment, true, true);
                }

            }
        }
    };


    //new UI implementation
    private JSONObject getRequestParams() {
        JSONObject requestData = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            requestData.put("request_id", Dmoat.getNewRequestId());
            requestData.put("app_id", pref.getString("app_id", ""));
            requestData.put("token", UserManager.getInstance().getLoggedInUser().token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestData;
    }


    private final AsyncTaskListener listener = result -> {
        if (!isAdded()) {
            return;
        }
        showProgressDialog(false);
        tabLayout.getTabAt(0).setText("New Alerts" + " (0)");
        if (result.code == 200) {
            if (result.message.equalsIgnoreCase("No data detected")) {
                getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
//                    showAlertMsgDialog(getContext(), "No alerts were detected.");
            } else {

                tabLayout.getTabAt(0).setText("New Alerts" + " (" + listOfAllViewAlerts.size() + ")");
//                    MyToast.showMessage(getContext(), "view alerts received==" + listOfAllViewAlerts.size());
                if (listOfAllViewAlerts.size() > 0) {
                    getParentView().findViewById(R.id.error_label).setVisibility(View.GONE);
                    loadDataInAdapter();
                } else {
                    getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
                }
            }
        } else if (result.code == 503) {
            getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
            showAlertMsgDialog(getContext(), result.message);
        } else if (result.code == 409) {
            getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
            showAlertMsgDialog(getContext(), result.message);
        } else {
            getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
        }

    };

    private void updateAlertsFilter(String item) {
        ArrayList<MutedBlockedAlertsModel> filteredListOfAlerts = new ArrayList<>();
        for (int i = 0; i < listOfAllViewAlerts.size(); i++) {

            if (item == "New Host") {
                if (listOfAllViewAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.NEW_HOST_ALERT)) {
                    filteredListOfAlerts.add(listOfAllViewAlerts.get(i));
                } else {
                }
            } else if (item == "Sensor") {
                if (listOfAllViewAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.SENSOR_ALERT)) {
                    filteredListOfAlerts.add(listOfAllViewAlerts.get(i));
                } else {
                }
            } else if (item == "Prytex Hardware Info") {
                if (listOfAllViewAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.INFO_ALERT)) {
                    filteredListOfAlerts.add(listOfAllViewAlerts.get(i));
                } else {
                }
            } else if (item == "Blocked IP Adresses") {
                if (listOfAllViewAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.BLOCK_IPS_ALERT)) {
                    filteredListOfAlerts.add(listOfAllViewAlerts.get(i));
                } else {
                }
            } else if (item == "Network Alert") {
                if (listOfAllViewAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.IDS_ALERT)) {
                    filteredListOfAlerts.add(listOfAllViewAlerts.get(i));
                } else {
                }
            } else if (item == "Devices Alerts") {
                if (listOfAllViewAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.FLOW_ALERT)) {
                    filteredListOfAlerts.add(listOfAllViewAlerts.get(i));
                } else {
                }
            } else if (item == "All Alerts") {
                filteredListOfAlerts.add(listOfAllViewAlerts.get(i));
            }

        }
        AlertsMainActivity.filterCountAlerts = filteredListOfAlerts.size();

        mAdapter = new ViewAlertsAdapter(getContext(), mRecyclerView, filteredListOfAlerts, mAlertSelectedListener, appID);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void loadDataInAdapter() {

        mAdapter = new ViewAlertsAdapter(getContext(), mRecyclerView, listOfAllViewAlerts, mAlertSelectedListener, appID);
        mRecyclerView.setAdapter(mAdapter);

    }
    private void refreshContent() {
        Api.getViewAlerts(getActivity(), getRequestParams(), listener);
        mSwipeRefreshLayout.setRefreshing(false);

    }

    private void SpinnerItems()
    {
        spinner.setItems("All Alerts", "New Host", "Sensor", "Prytex Hardware Info");
        spinner.setOnItemSelectedListener((MaterialSpinner.OnItemSelectedListener<String>) (view, position, id, item) -> updateAlertsFilter(item));
    }
}
