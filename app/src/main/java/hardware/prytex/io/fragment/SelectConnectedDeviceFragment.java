package hardware.prytex.io.fragment;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import hardware.prytex.io.R;
import hardware.prytex.io.adapter.HostsAdapter;

import static hardware.prytex.io.parser.ConnectedDevicesParser.listOfConnetcedDevice;

public class SelectConnectedDeviceFragment extends BaseFragment {


    public static Button btnDoneSelectinHosts;
    public static ImageView btnSelectAll;


    @Override
    public int getLayoutId() {
        return R.layout.fragment_select_connected_device;
    }

    @Override
    public String getTitle() {
        return "Create Policy";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        RecyclerView mRecyclerView = parent.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        btnSelectAll = parent.findViewById(R.id.btnSelectAllHosts);
        btnDoneSelectinHosts = parent.findViewById(R.id.btnDoneSelectingHosts);


        HostsAdapter mAdapter = new HostsAdapter(getContext(), listOfConnetcedDevice);
        mRecyclerView.setAdapter(mAdapter);

    }
}