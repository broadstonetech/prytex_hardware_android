package hardware.prytex.io.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import hardware.prytex.io.AgentChatActivity.HelpTutorialFragment;
import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.activity.AlertsMainActivity;
import hardware.prytex.io.activity.ConnectedDeviceActivity;
import hardware.prytex.io.activity.DashBoardActivity;
import hardware.prytex.io.adapter.ConnectedHostsSimplifiedAdapter;
import hardware.prytex.io.fragment.Policy.PoliciesMainFragment;
import hardware.prytex.io.fragment.SensorTabBars.SensorMainFragment;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.DeviceUtils;
import hardware.prytex.io.util.GeneralUtils;
import hardware.prytex.io.util.UserPrefs;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.activity.DashBoardActivity.llBottomTabs;
import static hardware.prytex.io.activity.DashBoardActivity.setBottomNavTextColors;
import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;
import static hardware.prytex.io.fragment.DashBoardFragment.AirQualityValue;
import static hardware.prytex.io.fragment.DashBoardFragment.HumidityValue;
import static hardware.prytex.io.fragment.DashBoardFragment.TemperatureValue;
import static hardware.prytex.io.fragment.DashBoardFragment.isAlertsCall;
import static hardware.prytex.io.fragment.DashBoardFragment.isConnectedDevicesCall;
import static hardware.prytex.io.fragment.DashBoardFragment.isSensorsCall;
import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;
import static hardware.prytex.io.parser.BlockedAlertParser.listOfMutedBlockedAlerts;
import static hardware.prytex.io.parser.ConnectedDevicesParser.isDevicesListReceived;
import static hardware.prytex.io.parser.ConnectedDevicesParser.listOfConnetcedDevice;
import static hardware.prytex.io.parser.Pre_MutedAlertsParser.listOfPreMutedAlerts;

@SuppressWarnings("ALL")
public class FragmentThemeOne extends BaseFragment implements View.OnClickListener {

    private SharedPreferences pref;
    private ProgressDialog dialog;
    private String appId = "";
    private static String DEVICE_IP;
    //ImageView dmoatIcon,imgObservedEvent,imgBlockeddAlerts;
    ImageView imgIcon, imgBlokedAlerts, imgObservedEventAlerts;
    private String PARENTAL_CONTROL_TYPE_SELECTED = "";
    private static final String[] PAUSE_INTERNET_INTERVALS = {"10 minutes", "20 minutes", "30 minutes"};
    private static final String[] PAUSE_INTERNET_INTERVALS_VALUES = {"10", "20", "30"};
    Toolbar toolbar;

    TextView tv_dmoat_online, tvBlockedEvents,tvBlockedEventsViewDetails,tvObservedEventsViewDetails, tvNoConnectedDevice,tvObservedEvents, tvSimplified, tvDetailed, temperature, humidity, air_quality,tvConnectedDevice,tvDmoatStatusText2,tvNoBlockedEvent,tvNoNetworkEventData;
    RelativeLayout rlBlockedAlerts, rlObservedEvents,rldmoatOnlineOffline;
    RecyclerView mRecyclerView;
    LinearLayout llObservedEvents,llThreatBlocked;
    View view1;
    @Override
    protected int getLayoutId() {

        int height = Resources.getSystem().getDisplayMetrics().heightPixels;
        if (height < 1794)
            return R.layout.fragment_theme_one_small_screen;
        else
            return R.layout.theme_one_fragment;
    }

    @Override
    public String getTitle() {
        return UserManager.getInstance().getLoggedInUser().name + "\'s Prytex Hardware";
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
//        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "Dashboard", null /* class override */);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, "Dashboard");
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, this.getClass().getName());

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);
        //   DashBoardActivity.menu.setVisibility(View.VISIBLE);
        isDmoatOnline = false;
        pref = getContext().getSharedPreferences("MyPref", 0);

        appId = pref.getString("app_id", "");
        DEVICE_IP = DeviceUtils.getIPAddress(true);
    }

    @SuppressLint({"ClickableViewAccessibility", "CommitPrefEdits"})
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);
        pref = getContext().getSharedPreferences("MyPref", 0);
        temperature = parent.findViewById(R.id.temp_id_theme1);
        humidity = parent.findViewById(R.id.humidity_theme1_id);
        air_quality = parent.findViewById(R.id.airq_theme1_id);

        imgIcon = parent.findViewById(R.id.imgIcon);
        imgBlokedAlerts = parent.findViewById(R.id.imgIcon2);
        imgObservedEventAlerts = parent.findViewById(R.id.imgIcon3);

        tv_dmoat_online = parent.findViewById(R.id.tvDmoatStatus);
        tvBlockedEvents = parent.findViewById(R.id.tvBlockedEvents);
        tvObservedEvents = parent.findViewById(R.id.tvObservedEvents);
        tvDetailed = parent.findViewById(R.id.tvDetailed);
        tvSimplified = parent.findViewById(R.id.tvSimplified);
        tvConnectedDevice = parent.findViewById(R.id.tvConnectedDevice);
        tvDmoatStatusText2 = parent.findViewById(R.id.tvDmoatStatusText2);
        tvNoNetworkEventData = parent.findViewById(R.id.tvNoNetworkEventData);
        tvNoBlockedEvent = parent.findViewById(R.id.tvNoBlockedEvent);
        tvNoConnectedDevice = parent.findViewById(R.id.tvNoConnectedDevice);
        tvBlockedEventsViewDetails = parent.findViewById(R.id.tvBlockedEventsViewDetails);
        tvObservedEventsViewDetails = parent.findViewById(R.id.tvObservedEventsViewDetails);
        llThreatBlocked = parent.findViewById(R.id.llThreatBlocked);
        llObservedEvents = parent.findViewById(R.id.llObservedEvents);
        rlBlockedAlerts = parent.findViewById(R.id.rlBlockedAlerts);
        rlObservedEvents = parent.findViewById(R.id.rlObservedEvents);
        rldmoatOnlineOffline = parent.findViewById(R.id.rldmoatOnlineOffline);
        mRecyclerView = parent.findViewById(R.id.rvConectedHost);
        view1 = parent.findViewById(R.id.view1);

         toolbar = parent.findViewById(R.id.toolbar_theme_one);
        ((DashBoardActivity)getActivity()).setSupportActionBar(toolbar);

       // setHasOptionsMenu(true);

        changeStatusBarColor(R.color.dmoat_theme1_status_bar_online);

        if (UserManager.getInstance().isUserLoggedIn()){
            toolbar.setTitle(UserManager.getInstance().getLoggedInUser().name + "\'s Prytex Hardware");
        }

        runAlertAsyncTask();

        runSensorsAsyncTask();

        if (UserPrefs.getTheme(getActivity()) == 1) {
            {
                tvSimplified.setBackground(getActivity().getResources().getDrawable(R.drawable.left_curved_corner));
                tvDetailed.setBackground(null);
            }
        } else {
            tvDetailed.setBackground(getActivity().getResources().getDrawable(R.drawable.right_curved_corner));
            tvSimplified.setBackground(null);
        }

        tvSimplified.setOnClickListener(this);
        tvDetailed.setOnClickListener(this);


        rlBlockedAlerts.setOnTouchListener((v, event) -> {
            AddActivityandRemoveIntentTransition(AlertsMainActivity.class);
            return false;
        });
        rlObservedEvents.setOnTouchListener((v, event) -> {
            AddActivityandRemoveIntentTransition(AlertsMainActivity.class);
            return false;
        });
        rldmoatOnlineOffline.setOnTouchListener((v, event) -> {
            AddActivityandRemoveIntentTransition(ConnectedDeviceActivity.class);
            return false;
        });
    }

    private JSONObject getParams() {
        JSONObject js = new JSONObject();
        try {

            js.put("app_id", pref.getString("app_id", ""));
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    @Override
    public void onClick(View v) {
       if (v.getId() == R.id.ll_sensors_theme1) {
            GeneralUtils.ScaleInAnimation(getActivity(), v);
            goToSensorsView();
        } else if ((v.getId() == R.id.tvSimplified)) {
            UserPrefs.setTheme(getActivity(), 1);
            tvSimplified.setBackground(getActivity().getResources().getDrawable(R.drawable.left_curved_corner));
            tvDetailed.setBackground(null);
        } else if (v.getId() == R.id.tvDetailed) {
            UserPrefs.setTheme(getActivity(), 0);
            tvDetailed.setBackground(getActivity().getResources().getDrawable(R.drawable.right_curved_corner));
            tvSimplified.setBackground(null);
            ((DashBoardActivity) getActivity()).loadTheme();
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.overflow_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.menu_home){
            getActivity().finish();
            getActivity().getIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivityForResult(getActivity().getIntent(), 0);
            getActivity().overridePendingTransition(0,0);
            return true;
        }else if(id == R.id.menu_policy){

            if (UserManager.checkPauseInternetTimeStatus()) {
                imgInfoBtn.setImageResource(R.drawable.info);
                setBottomNavTextColors(getActivity(), false, true, false, false);
                ((DashBoardActivity) getActivity()).addFragment(new PoliciesMainFragment(), true, true);
            }else
            {
                showAlertMsgDialog(getActivity(), getResources().getString(R.string.paused_internet_msg));
            }
            return true;
        }else if(id == R.id.menu_account){
            imgInfoBtn.setImageResource(R.drawable.info);
            setBottomNavTextColors(getActivity(), false, false, true, false);
            ((DashBoardActivity) getActivity()).addFragment(new ProfileFragment(), true, true);
            return true;
        }else if(id == R.id.menu_support){
            setBottomNavTextColors(getActivity(), false, false, false, true);
            imgInfoBtn.setImageResource(R.drawable.info);
            ((DashBoardActivity) getActivity()).addFragment(new HelpTutorialFragment(), true, true);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void AddActivityandRemoveIntentTransition(Class activity) {
        Intent intent = new Intent(getActivity(), activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivityForResult(intent, 0);
        getActivity().overridePendingTransition(0, 0);
        getActivity().finish();
    }

    private void goToSensorsView() {
        imgInfoBtn.setImageResource(R.drawable.info);
        llBottomTabs.setVisibility(View.GONE);
        setBottomNavTextColors(getContext(), false, false, false, false);
        getHelper().addFragmentwitoutTabbar(new SensorMainFragment(), true, true);
    }

    private class SensorsAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            Log.i("AsyncTask", "FirstOnPreExecute() FragmentTheme1");
        }

        @Override
        protected Void doInBackground(Void... params) {

            Log.i("AsyncTask", "AlertsAsyncTask FragmentTheme1");

            Handler mainHandler = new Handler(Looper.getMainLooper());
            Runnable myRunnable = () -> {
                //Code that uses AsyncHttpClient in your case ConsultaCaract()
                isSensorsCall = true;
                Api.getSensorValuesDashboard(getActivity(), getParams(), mListenerB);
            };
            mainHandler.post(myRunnable);

            //call alerts get api
//            Thread.sleep(100); //call connected devices count api


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.d("AsyncTask", "FirstonPostExecute() FragmentTheme1");
        }
    }

    private final ConnectedHostsSimplifiedAdapter.OnAlertSelectedListener mAlertSelectedListener = (alert, position, isDefaultPasswords, isActivePorts) -> {

        AddActivityandRemoveIntentTransition(ConnectedDeviceActivity.class);

    };



    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }


    private void runSensorsAsyncTask() // Run Multiple Async Task
    {
        isSensorsCall = true;
        SensorsAsyncTask asyncTask = new SensorsAsyncTask(); // Second
        asyncTask.execute();
    }

//    private void runConnectedDevicesAsyncTask() {
////        ConnectedDevicesAsyncTask asyncTask = new ConnectedDevicesAsyncTask();
////        asyncTask.execute();
//    }

    private void runAlertAsyncTask() {
        isConnectedDevicesCall = true;
        showProgressDialog(true);
        Api.getConnectedDevicesDashboard(getActivity(), getParams(), connectedDeviceDashboardListner);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void changeStatusBarColor(int color) {
        Window window = getActivity().getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(getActivity(), color));
    }

    @Override
    public void onResume() {
        super.onResume();
        DashBoardActivity.showSwitchButton();
    }

    @Override
    public void onStop() {
        super.onStop();
        DashBoardActivity.hideSwitchButton();
    }

    //region api calls and listners
    private final AsyncTaskListener mListenerB = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            Log.d("AsyncTaskResponse", "response FragmentTheme1");

            Log.d("FragmentTheme1", result.message);
            Log.d("FragmentTheme1", result + "");
            if (result.code == 200 || result.code == 2) {
                runAlertAsyncTask();
                try {
                    Log.d("AsyncTask", "success");

                    if (isSensorsCall) {
                        Log.d("FragmentTheme1", "In Sensor Call");
                        float fahrenhietCelciusTemp;
                        if (TemperatureValue == 0 && HumidityValue == 0) {
//                            pieChartTemp.setCenterText("0");
                        } else {
                            fahrenhietCelciusTemp = ((TemperatureValue * 9) / 5 + 32);
                            String convertedFahrnhtTemp = String.format("%.0f", fahrenhietCelciusTemp);
                            TemperatureValue = (int) fahrenhietCelciusTemp;
                            //pieChartTemp.setCenterText(convertedFahrnhtTemp);
                            temperature.setText(convertedFahrnhtTemp);
                            humidity.setText(String.valueOf(HumidityValue));
                            air_quality.setText(String.valueOf(AirQualityValue));

                        }

                        isSensorsCall = false;
                    } else {
                    }
                } catch (Exception e) {
                    Log.d("AsyncExcepti9on2", String.valueOf(e));
                }
            } else {
//                MyToast.showMessage(getActivity(), getResources().getString(R.string.error_message_generic));
            }
        }
    };

    // listener
    private final AsyncTaskListener connectedDeviceDashboardListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            Log.d("AsyncTaskResponse", "response");
            Api.getAlertsDashboard(getActivity(), getParams(), alertListner);
            if (result.code == 200 || result.code == 2) {
                Log.d("AsyncTask", "success");

                try {
                    if (!isDmoatOnline) {

                        tvNoBlockedEvent.setVisibility(View.VISIBLE);
                        llThreatBlocked.setVisibility(View.GONE);
                        tvNoBlockedEvent.setText(getActivity().getResources().getString(R.string.reconnect_to_prytex));


                        llObservedEvents.setVisibility(View.GONE);
                        view1.setVisibility(View.VISIBLE);
                        tvNoNetworkEventData.setVisibility(View.VISIBLE);
                        tvNoNetworkEventData.setText(getActivity().getResources().getString(R.string.reconnect_to_prytex));
                    }

                } catch (Exception e) {
                    Log.d("AsyncException", String.valueOf(e));
                }

//                MyToast.showMessage(getContext(), "successfuly received data");
            } else {
//                MyToast.showMessage(getActivity(), getResources().getString(R.string.error_message_generic));
            }
        }
    };
    // 1st api call
    private final AsyncTaskListener conctedListener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            showProgressDialog(false);
            if (result.code == 200 || result.code == 2) {
                Api.getBlockedAlerts(getActivity(), getParams(), listener);
                if (isDevicesListReceived) {
                    if (listOfConnetcedDevice.size() > 0) {
                        tvNoConnectedDevice.setVisibility(View.GONE);
                        LinearLayoutManager suugestedModelsManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                        mRecyclerView.setLayoutManager(suugestedModelsManager);

                        ConnectedHostsSimplifiedAdapter mAdapter = new ConnectedHostsSimplifiedAdapter(getActivity(), listOfConnetcedDevice, new ArrayList<>(), mAlertSelectedListener, appId, DEVICE_IP);
                        mRecyclerView.setAdapter(mAdapter);
                        if (listOfConnetcedDevice.size() > 9)
                            tvConnectedDevice.setText(String.valueOf(listOfConnetcedDevice.size()));
                        else
                            tvConnectedDevice.setText("0" + listOfConnetcedDevice.size());
                    } else {
                        tvNoConnectedDevice.setVisibility(View.VISIBLE);
                        DashBoardActivity.showAlertMsgDialog(getActivity(), getResources().getString(R.string.no_connected_devices));
                    }
                }
//                else {
//                    tvNoConnectedDevice.setVisibility(View.VISIBLE);
//                    DashBoardActivity.showAlertMsgDialog(getActivity(), getResources().getString(R.string.no_connected_devices));
//                }
            } else if (result.code == 503) {
                DashBoardActivity.showAlertMsgDialog(getActivity(), result.message);
            } else if (result.code == 409) {
                DashBoardActivity.showAlertMsgDialog(getActivity(), result.message);
            } else {
                tvNoConnectedDevice.setVisibility(View.VISIBLE);
                DashBoardActivity.showAlertMsgDialog(getActivity(), getResources().getString(R.string.no_connected_devices));
            }
        }
    };

    private final AsyncTaskListener conctedListener2 = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            showProgressDialog(false);
            if (result.code == 200 || result.code == 2) {
                if (isDevicesListReceived) {
                    if (listOfConnetcedDevice.size() > 0) {
                        tvNoConnectedDevice.setVisibility(View.GONE);
                        LinearLayoutManager suugestedModelsManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                        mRecyclerView.setLayoutManager(suugestedModelsManager);

                        ConnectedHostsSimplifiedAdapter mAdapter = new ConnectedHostsSimplifiedAdapter(getActivity(), listOfConnetcedDevice, new ArrayList<>(), mAlertSelectedListener, appId, DEVICE_IP);
                        mRecyclerView.setAdapter(mAdapter);
                        if (listOfConnetcedDevice.size() > 9)
                            tvConnectedDevice.setText(String.valueOf(listOfConnetcedDevice.size()));
                        else
                            tvConnectedDevice.setText("0" + listOfConnetcedDevice.size());
                    } else {
                        tvNoConnectedDevice.setVisibility(View.VISIBLE);
                        DashBoardActivity.showAlertMsgDialog(getActivity(), getResources().getString(R.string.no_connected_devices));
                    }
                } else {
                    tvNoConnectedDevice.setVisibility(View.VISIBLE);
                    DashBoardActivity.showAlertMsgDialog(getActivity(), getResources().getString(R.string.no_connected_devices));
                }
            } else if (result.code == 503) {
                DashBoardActivity.showAlertMsgDialog(getActivity(), result.message);
            } else if (result.code == 409) {
                DashBoardActivity.showAlertMsgDialog(getActivity(), result.message);
            }
//            else {
//                tvNoConnectedDevice.setVisibility(View.VISIBLE);
//                DashBoardActivity.showAlertMsgDialog(getActivity(), getResources().getString(R.string.no_connected_devices));
//            }
        }
    };

    private final AsyncTaskListener listenerPREMUTED = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.code == 200) {
                isAlertsCall = true;

                Api.getContactedHosts(getActivity(), getParams(), conctedListener2);
                {
                    view1.setVisibility(View.GONE);
                    rlObservedEvents.setBackgroundColor(getResources().getColor(R.color.light_grey_profile));
                    if (listOfPreMutedAlerts.size() > 0) {
                        tvObservedEvents.setText(String.valueOf(listOfPreMutedAlerts.size()));
                        llObservedEvents.setVisibility(View.VISIBLE);
                        tvNoNetworkEventData.setVisibility(View.GONE);
                    } else {
                        llObservedEvents.setVisibility(View.GONE);
                        tvNoNetworkEventData.setVisibility(View.VISIBLE);
                    }

                    if (!isDmoatOnline) {
                        rlObservedEvents.setBackgroundColor(getResources().getColor(R.color.dashboard_background));
                        llObservedEvents.setVisibility(View.GONE);
                        view1.setVisibility(View.VISIBLE);
                        tvNoNetworkEventData.setVisibility(View.VISIBLE);
                        tvNoNetworkEventData.setText(getActivity().getResources().getString(R.string.reconnect_to_prytex));
                    }

                }
            }


        }

    };
    private final AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.code == 200 || result.code == 2) {
                Api.getPreMutedAlerts(getActivity(), getParams(), listenerPREMUTED);
                {
                    rlObservedEvents.setBackgroundColor(getResources().getColor(R.color.white));
                    if (listOfMutedBlockedAlerts.size() > 0) {
                        tvBlockedEvents.setText(String.valueOf(listOfMutedBlockedAlerts.size()));
                        tvNoBlockedEvent.setVisibility(View.GONE);
                        llThreatBlocked.setVisibility(View.VISIBLE);
                    } else {
                        tvNoBlockedEvent.setVisibility(View.VISIBLE);
                        llThreatBlocked.setVisibility(View.GONE);
                    }
                    if (!isDmoatOnline) {
                        rlBlockedAlerts.setBackgroundColor(getResources().getColor(R.color.dashboard_background));
                        tvNoBlockedEvent.setVisibility(View.VISIBLE);
                        llThreatBlocked.setVisibility(View.GONE);
                        tvNoBlockedEvent.setText(getActivity().getResources().getString(R.string.reconnect_to_prytex));
                    }
                }
            }

        }
    };

    private final AsyncTaskListener alertListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            Log.d("AsyncTaskResponse", "response");


            if (result.code == 200 || result.code == 2) {
                Log.d("AsyncTask", "success");
                Api.getContactedHosts(getActivity(), getParams(), conctedListener);
                try {
                    if (isDmoatOnline) {
                        imgIcon.setImageResource(R.drawable.dmoat_simplfied);
                        imgObservedEventAlerts.setImageResource(R.drawable.networkevent);
                        imgBlokedAlerts.setImageResource(R.drawable.blocked_alerts);
                        tvDmoatStatusText2.setVisibility(View.VISIBLE);
                        tv_dmoat_online.setText("Prytex Hardware");
                    } else {
                        //DMoat Offline
                        imgIcon.setImageResource(R.drawable.dmoat_simplfied_offline);
                        tvDmoatStatusText2.setVisibility(View.GONE);
                        tv_dmoat_online.setText(getActivity().getResources().getString(R.string.prytex_offline));
                    }


                } catch (Exception e) {
                    Log.d("AsyncException", String.valueOf(e));
                }

            } else {
            }
        }
    };
    //endregion

}
