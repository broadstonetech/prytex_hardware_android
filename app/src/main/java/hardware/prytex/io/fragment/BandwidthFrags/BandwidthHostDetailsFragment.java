package hardware.prytex.io.fragment.BandwidthFrags;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.adapter.BandwidthHostDetailAdapter;
import hardware.prytex.io.adapter.ProtocolsDetailExpandabaleAdapter;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.model.ProtocolsListModal;
import hardware.prytex.io.util.MyToast;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.adapter.BandwidthAdapter.posClicked;
import static hardware.prytex.io.parser.BandwidthParser.listOfProtocolDevices;
import static hardware.prytex.io.parser.BandwidthParser.listOfProtocolDevicesCurrentHour;


@SuppressWarnings("ALL")
public class BandwidthHostDetailsFragment extends BaseFragment implements View.OnClickListener{

    private BandwidthHostDetailAdapter mAdapter;
    TextView tvHostId;
    private String protocolsString;
    private static final ArrayList<ProtocolsListModal> listOfSelectedHostProtocols = new ArrayList<>();
    private static final HashMap<String, String> protocolsValuesDisctionar = new HashMap<>();
    private static final HashMap<String, String> protocolsDesscriptionDisctionar = new HashMap<>();
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;

    public static boolean isCurrentBandwidthDatList = false;
    public static boolean isAllBandwidthDataList = false;


    @Override
    public int getLayoutId() {
        return R.layout.fragment_bandwidth_host_details;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
//        mFirebaseAnalytics.setCurrentScreen(getActivity(), "protocol_details", null /* class override */);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, "protocol_details");
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, getActivity().getClass().getName());

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);
    }


    @Override
    public String getTitle() {
        return "Protocol Details";
    }


    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        RecyclerView mRecyclerView = parent.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        TextView tvHostname = parent.findViewById(R.id.tvHostName);
        TextView tvHostIp = parent.findViewById(R.id.tvHostIP);
        ImageView imgOSlogo = parent.findViewById(R.id.imgOsLogo);
        mRecyclerView = parent.findViewById(R.id.recycler_view);
        Button btnProtocolPolicy = parent.findViewById(R.id.btnProtocolPolicy);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        imgInfoBtn.setVisibility(View.VISIBLE);


        listOfSelectedHostProtocols.clear();
        ExpandableListView expListView = parent.findViewById(R.id.lvExp);
        // preparing list data
        prepareProtoclValues();
        prepareProtoclDescription();

        String hostOS = "";
        if (isCurrentBandwidthDatList) {
            hostOS = listOfProtocolDevicesCurrentHour.get(posClicked).getHostOS();
            tvHostname.setText(listOfProtocolDevicesCurrentHour.get(posClicked).getHostName());
            tvHostIp.setText(listOfProtocolDevicesCurrentHour.get(posClicked).getHostIP());
            protocolsString = listOfProtocolDevicesCurrentHour.get(posClicked).getProtocolsList();
        } else if (isAllBandwidthDataList) {
            hostOS = listOfProtocolDevices.get(posClicked).getHostOS();
            tvHostname.setText(listOfProtocolDevices.get(posClicked).getHostName());
            tvHostIp.setText(listOfProtocolDevices.get(posClicked).getHostIP());
            protocolsString = listOfProtocolDevices.get(posClicked).getProtocolsList();

        }

        if (hostOS.contains("android") || hostOS.contains("Android")) {
            imgOSlogo.setImageResource(R.drawable.android_logo);
        } else if ((hostOS.contains("apple")) || hostOS.contains("Apple") || hostOS.contains("iphone") || hostOS.contains("mac") || hostOS.contains("Mac")) {
            imgOSlogo.setImageResource(R.drawable.apple);
        } else if ((hostOS.contains("windows")) || hostOS.contains("Windows")) {
            imgOSlogo.setImageResource(R.drawable.windows);
        } else if ((hostOS.contains("linux")) || hostOS.contains("Linux")) {
            imgOSlogo.setImageResource(R.drawable.linux);
        } else {
            imgOSlogo.setImageResource(R.drawable.laptop_logo);
        }


        String nString = protocolsString.replace("{", "");
        final String oString = nString.replace("}", "");
        List<String> protocolSplitStringList = Arrays.asList(oString.split(","));
        for (int i = 0; i < protocolSplitStringList.size(); i++) {
            String pString = protocolSplitStringList.get(i);
            final String qString = pString.replace(":", ",");
            List<String> as = Arrays.asList(qString.split(","));
            String protocolName = as.get(0).toString();
            String newName = protocolName.replace("\"", "");

            Float in = Float.valueOf(String.valueOf((as.get(1))));

            in = in /1048576;//;
            if(in <= 0.1){
                in = Float.parseFloat(String.valueOf(0.1));
            }
            ProtocolsListModal protocolsListModel = new ProtocolsListModal();
            protocolsListModel.setId(i + 1);
            protocolsListModel.setProtocolName(newName);
            protocolsListModel.setProtocolUsage(in);
            listOfSelectedHostProtocols.add(protocolsListModel);
        }

        Collections.sort(listOfSelectedHostProtocols, new DateComparator());

        ProtocolsDetailExpandabaleAdapter listAdapter = new ProtocolsDetailExpandabaleAdapter(getContext(), listOfSelectedHostProtocols, protocolsValuesDisctionar, protocolsDesscriptionDisctionar, listDataHeader, listDataChild, mAlertSelectedListener);

        // setting list adapter
        expListView.setAdapter(listAdapter);
        btnProtocolPolicy.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnProtocolPolicy) {
            MyToast.showMessage(getContext(), "link removed from here");
        }
    }

    static class DateComparator implements Comparator<ProtocolsListModal> {

        @Override
        public int compare(ProtocolsListModal lhs, ProtocolsListModal rhs) {
            Double distance = Double.valueOf(lhs.getProtocolUsage());
            Double distance1 = Double.valueOf(rhs.getProtocolUsage());
            return Integer.compare(distance1.compareTo(distance), 0);
        }
    }

    private static void prepareProtoclValues() {

        protocolsValuesDisctionar.put("afp", "AFP");
        protocolsValuesDisctionar.put("aimini", "Aimini");
        protocolsValuesDisctionar.put("amazon", "Amazon");

        protocolsValuesDisctionar.put("tls", "TLS");
        protocolsValuesDisctionar.put("microsoft365", "Microsoft 365");
        protocolsValuesDisctionar.put("googlehangoutduo", "Google Hangouts");

        protocolsValuesDisctionar.put("dnscrypt","DNScrypt");
        protocolsValuesDisctionar.put("bjnp","BJNP");
        protocolsValuesDisctionar.put("ookla","Ookla");
        protocolsValuesDisctionar.put("signal","Signal");
        protocolsValuesDisctionar.put("nestlogsink","NestLogSink");
        protocolsValuesDisctionar.put("http_activesync","HTTP_ActiveSync");
        protocolsValuesDisctionar.put("bloomberg","Bloomberg");
        protocolsValuesDisctionar.put("smbv3","SMBv23");
        protocolsValuesDisctionar.put("ntop","ntop");
        protocolsValuesDisctionar.put("mining","Mining");
        protocolsValuesDisctionar.put("nintendo","Nintendo");
        protocolsValuesDisctionar.put("amqp","AMQP");
        protocolsValuesDisctionar.put("genericprotocol","GenericProtocol");
        protocolsValuesDisctionar.put("checkmk","CHECKMK");
        protocolsValuesDisctionar.put("lisp","LISP");
        protocolsValuesDisctionar.put("playstation","Playstation");
        protocolsValuesDisctionar.put("targus_dataspeed","Targus Dataspeed");
        protocolsValuesDisctionar.put("memcached","Memcached");
        protocolsValuesDisctionar.put("smpp","SMPP");
        protocolsValuesDisctionar.put("iflix","IFLIX");
        protocolsValuesDisctionar.put("googleservices","GoogleServices");
        protocolsValuesDisctionar.put("smbv1","SMBv1");
        protocolsValuesDisctionar.put("googledrive","GoogleDrive");
        protocolsValuesDisctionar.put("csgo","CSGO");
        protocolsValuesDisctionar.put("messenger","Messenger");
        protocolsValuesDisctionar.put("skypecall","SkypeCall");
        protocolsValuesDisctionar.put("applepush","ApplePush");
        protocolsValuesDisctionar.put("linkedin","LinkedIn");
        protocolsValuesDisctionar.put("amazonvideo","AmazonVideo");
        protocolsValuesDisctionar.put("youtubeupload","YouTubeUpload");
        protocolsValuesDisctionar.put("ajp","AJP");
        protocolsValuesDisctionar.put("soundcloud","SoundCloud");
        protocolsValuesDisctionar.put("googleplus","GooglePlus");
        protocolsValuesDisctionar.put("playstore","PlayStore");
        protocolsValuesDisctionar.put("pastebin","Pastebin");
        protocolsValuesDisctionar.put("modbus","Modbus");
        protocolsValuesDisctionar.put("iec60870","IEC60870");
        protocolsValuesDisctionar.put("zabbix","Zabbix");
        protocolsValuesDisctionar.put("websocket","WebSocket");
        protocolsValuesDisctionar.put("s7comm","s7comm");
        protocolsValuesDisctionar.put("fix","FIX");
        protocolsValuesDisctionar.put("facebookzero","FacebookZero");
        protocolsValuesDisctionar.put("tinc","TINC");
        protocolsValuesDisctionar.put("diameter","Diameter");
        protocolsValuesDisctionar.put("wechat","WeChat");
        protocolsValuesDisctionar.put("github","Github");
        protocolsValuesDisctionar.put("teams","Microsoft Teams");
        protocolsValuesDisctionar.put("whatsappfiles","WhatsAppFiles");
        protocolsValuesDisctionar.put("dnp3","DNP3");
        protocolsValuesDisctionar.put("applestore","AppleStore");
        protocolsValuesDisctionar.put("tiktok","TikTok");
        protocolsValuesDisctionar.put("someip","SOMEIP");
        protocolsValuesDisctionar.put("capwap","CAPWAP");

        protocolsValuesDisctionar.put("apple", "Apple");
        protocolsValuesDisctionar.put("appleicloud", "iCloud");
        protocolsValuesDisctionar.put("appleitunes", "iTunes");
        protocolsValuesDisctionar.put("applejuice", "Apple juice");
        protocolsValuesDisctionar.put("armagetron", "Armagetron");
        protocolsValuesDisctionar.put("avi", "Avi");
        protocolsValuesDisctionar.put("ayiya", "AYIYA");
        protocolsValuesDisctionar.put("Battlefield", "Battlefield");
        protocolsValuesDisctionar.put("bgp", "BGP");
        protocolsValuesDisctionar.put("bittorrent", "BitTorrent");
        protocolsValuesDisctionar.put("ciscoskinny", "SCCP");
        protocolsValuesDisctionar.put("Ciscovpn", "Cisco vpn");
        protocolsValuesDisctionar.put("citrix", "Citrix");
        protocolsValuesDisctionar.put("citrix_online", "Citrix Online");
        protocolsValuesDisctionar.put("Cloudflare", "Cloudflare");
        protocolsValuesDisctionar.put("cnn", "CNN");
        protocolsValuesDisctionar.put("coap", "CoAP");
        protocolsValuesDisctionar.put("collectd", "collectd");
        protocolsValuesDisctionar.put("corba", "CORBA");
        protocolsValuesDisctionar.put("crossfire", "CrossFire");
        protocolsValuesDisctionar.put("dce_rpc", "DCE/RPC");
        protocolsValuesDisctionar.put("deezer", "Deezer");
        protocolsValuesDisctionar.put("dhcp", "DHCP");
        protocolsValuesDisctionar.put("dhcpv6", "DHCPv6");
        protocolsValuesDisctionar.put("direct_download_link", "DDL");
        protocolsValuesDisctionar.put("directconnect", "DC");
        protocolsValuesDisctionar.put("dns", "DNS");
        protocolsValuesDisctionar.put("dofus", "Dofus");
        protocolsValuesDisctionar.put("drda", "DRDA");
        protocolsValuesDisctionar.put("dropbox", "Dropbox");
        protocolsValuesDisctionar.put("eaq", "EAQ");
        protocolsValuesDisctionar.put("ebay", "Ebay");
        protocolsValuesDisctionar.put("edonkey", "eDonkey");
        protocolsValuesDisctionar.put("egp", "EGP");
        protocolsValuesDisctionar.put("epp", "EPP");
        protocolsValuesDisctionar.put("facebook", "Facebook");
        protocolsValuesDisctionar.put("fasttrack", "");
        protocolsValuesDisctionar.put("fiesta", "Fiesta");
     //   protocolsValuesDisctionar.put("filetopia", "Filetopia");
        protocolsValuesDisctionar.put("filetopia", "Flash Video");
        protocolsValuesDisctionar.put("florensia", "");
        protocolsValuesDisctionar.put("ftp_control", "FTP Control ");
        protocolsValuesDisctionar.put("ftp_data", "FTP Data");
        protocolsValuesDisctionar.put("git", "Git");
        protocolsValuesDisctionar.put("gmail", "Gmail");
        protocolsValuesDisctionar.put("gnutella", "");
        protocolsValuesDisctionar.put("google", "Google");
        protocolsValuesDisctionar.put("googlehangout", "Google Hangouts");
        protocolsValuesDisctionar.put("Googlesmaps", "Google Maps");
        protocolsValuesDisctionar.put("gre", "GRE");
        protocolsValuesDisctionar.put("gtp", "GTP");
        protocolsValuesDisctionar.put("guildwars", "Guild Wars");
        protocolsValuesDisctionar.put("h323", "h323");
        protocolsValuesDisctionar.put("halflife2", "Half-Life 2");
        protocolsValuesDisctionar.put("hep", "Hep");
        protocolsValuesDisctionar.put("hotmail", "Hotmail");
        protocolsValuesDisctionar.put("hotspotshield", "Hotspot Shield");
        protocolsValuesDisctionar.put("https", "HTTPS");
        protocolsValuesDisctionar.put("http", "HTTP");
        protocolsValuesDisctionar.put("http_application_activesync", "HTTP Application Active Sync");
        protocolsValuesDisctionar.put("http_connect", "HTTP Connect");
        protocolsValuesDisctionar.put("http_proxy", "HTTP Proxy");
        protocolsValuesDisctionar.put("httpdownload", "HTTP Download");
        protocolsValuesDisctionar.put("iax", "IAX ");
        protocolsValuesDisctionar.put("icecast", "Icecast");
        protocolsValuesDisctionar.put("icmp", "ICMP");
        protocolsValuesDisctionar.put("icmpv6", "ICMPv6");
        protocolsValuesDisctionar.put("igmp", "IGMP");
        protocolsValuesDisctionar.put("imap", "IMAP");
        protocolsValuesDisctionar.put("imaps", "IMAP");
      //  protocolsValuesDisctionar.put("imesh", "iMesh");
        protocolsValuesDisctionar.put("instagram", "Instagram");
        protocolsValuesDisctionar.put("ip_in_ip", "IP in IP");
        protocolsValuesDisctionar.put("ipp", "IPP");
        protocolsValuesDisctionar.put("ipsec", "IPsec");
        protocolsValuesDisctionar.put("irc", "IRC");
        protocolsValuesDisctionar.put("kakaotalk", "kakaotalk");
        protocolsValuesDisctionar.put("kakaotalk_voice", "Kakaotalk Voice");
        protocolsValuesDisctionar.put("kerberos", "Kerberos");
        protocolsValuesDisctionar.put("kontiki", "Kontiki ");
        protocolsValuesDisctionar.put("lastfm", "Lastfm");
        protocolsValuesDisctionar.put("ldap", "LDAP");
        protocolsValuesDisctionar.put("llmnr", "LLMNR");
        protocolsValuesDisctionar.put("lotusnotes", "Lotus Notes ");
        protocolsValuesDisctionar.put("lync", "Lync");
        protocolsValuesDisctionar.put("maplestory", "MapleStory");
        protocolsValuesDisctionar.put("mdns", "mDNS");
        protocolsValuesDisctionar.put("megaco", "Megaco");
        protocolsValuesDisctionar.put("mgcp", "MGCP ");
        protocolsValuesDisctionar.put("microsoft", "Microsoft");
        protocolsValuesDisctionar.put("mms", "MMS");
        protocolsValuesDisctionar.put("move", "");
      //  protocolsValuesDisctionar.put("mpeg", "MPEG transport stream");
        protocolsValuesDisctionar.put("mpeg_ts", "MPEG transport stream");
        protocolsValuesDisctionar.put("mqtt", "MQTT");
        protocolsValuesDisctionar.put("ms_one_drive", "One drive");
        protocolsValuesDisctionar.put("msn", "MSN");
        protocolsValuesDisctionar.put("mssql-tds", "TDS");
        protocolsValuesDisctionar.put("mysql", "MySQL");
        protocolsValuesDisctionar.put("netbios", "NBNS");
        protocolsValuesDisctionar.put("netflix", "Netflix");
        protocolsValuesDisctionar.put("netflow", "NetFlow");
        protocolsValuesDisctionar.put("nfs", "NFS");
        protocolsValuesDisctionar.put("noe", "Noe");
        protocolsValuesDisctionar.put("ntp", "NTP");
        protocolsValuesDisctionar.put("ocs", "OCS");
        protocolsValuesDisctionar.put("office365", "Office 365");
       // protocolsValuesDisctionar.put("oggvorbis", "Ogg Vorbis ");
        protocolsValuesDisctionar.put("opendns", "OpenDNS");
        protocolsValuesDisctionar.put("openft", "OpenFT");
        protocolsValuesDisctionar.put("openvpn", "Openvpn");
        protocolsValuesDisctionar.put("oracle", "Oracle");
        protocolsValuesDisctionar.put("oscar", "OSCAR");
        protocolsValuesDisctionar.put("ospf", "OSPF");
        protocolsValuesDisctionar.put("pando_media_booster", "PMB");
        protocolsValuesDisctionar.put("pandora", "Pandora");
        protocolsValuesDisctionar.put("pcanywhere", "Pc Anywhere");
        protocolsValuesDisctionar.put("pop3", "POP3");
        protocolsValuesDisctionar.put("pops", "POP");
        protocolsValuesDisctionar.put("postgresql", "Postgres");
        protocolsValuesDisctionar.put("pplive", "PPLIVE");
        protocolsValuesDisctionar.put("ppstream", "PPStream");
        protocolsValuesDisctionar.put("pptp", "PPTP");
        protocolsValuesDisctionar.put("qq", "QQ");
        protocolsValuesDisctionar.put("qqlive", "QQLive");
        //protocolsValuesDisctionar.put("quake", "Quake");
        protocolsValuesDisctionar.put("quic", "QUIC");
        protocolsValuesDisctionar.put("quickplay", "Quickplay");
        //protocolsValuesDisctionar.put("quicktime", "QuickTime");
        protocolsValuesDisctionar.put("radius", "RADIUS");
        protocolsValuesDisctionar.put("rdp", "RDP");
        protocolsValuesDisctionar.put("realmedia", "RealMedia");
        protocolsValuesDisctionar.put("redis", "Redis");
        protocolsValuesDisctionar.put("remotescan", "Remotescan");
        protocolsValuesDisctionar.put("rsync", "Rsync");
        protocolsValuesDisctionar.put("rtcp", "RTCP");
        protocolsValuesDisctionar.put("rtmp", "RTMP");
        protocolsValuesDisctionar.put("rtp", "RTP");
        protocolsValuesDisctionar.put("rtsp", "RTSP");
        protocolsValuesDisctionar.put("rx", "Rx");
        protocolsValuesDisctionar.put("sap", "SAP");
        protocolsValuesDisctionar.put("sctp", "SCTP");
        protocolsValuesDisctionar.put("sflow", "sFlow");
        protocolsValuesDisctionar.put("shoutcast", "SHOUTcast");
        protocolsValuesDisctionar.put("sina(weibo)", "Sina Weibo");
        protocolsValuesDisctionar.put("sip", "SIP");
        protocolsValuesDisctionar.put("skyfile_postpaid", "SkyFile Mail");
        protocolsValuesDisctionar.put("skyfile_prepaid", "SkyFile Mail");
      //  protocolsValuesDisctionar.put("skyfile_rudics", "SkyFile Mail");
        protocolsValuesDisctionar.put("skype", "Skype");
        protocolsValuesDisctionar.put("slack", "Slack");
        protocolsValuesDisctionar.put("smb", "SMB");
        protocolsValuesDisctionar.put("smtp", "SMTP");
        protocolsValuesDisctionar.put("smtps", "SMTPS");
        protocolsValuesDisctionar.put("snapchat", "Snapchat");
        protocolsValuesDisctionar.put("snmp", "SNMP");
        protocolsValuesDisctionar.put("socks", "Socket Secure");
        protocolsValuesDisctionar.put("socrates", "SOCRATES");
        protocolsValuesDisctionar.put("sopcast", "Sopcast");
        protocolsValuesDisctionar.put("soulseek", "Soulseek");
        protocolsValuesDisctionar.put("spotify", "Spotify");
        protocolsValuesDisctionar.put("ssdp", "SSDP");
        protocolsValuesDisctionar.put("ssh", "SSH");
        protocolsValuesDisctionar.put("ssl", "SSL");
        protocolsValuesDisctionar.put("ssl_no_cert", "SSL NO CERT");
        protocolsValuesDisctionar.put("starcraft", "Starcraft");
        protocolsValuesDisctionar.put("stealthnet", "Stealthnet");
        protocolsValuesDisctionar.put("steam", "Steam");
        protocolsValuesDisctionar.put("stun", "STUN");
        protocolsValuesDisctionar.put("syslog", "Syslog");
        protocolsValuesDisctionar.put("teamspeak", "TeamSpeak");
        protocolsValuesDisctionar.put("teamviewer", "TeamViewer");
        protocolsValuesDisctionar.put("telegram", "Telegram");
        protocolsValuesDisctionar.put("telnet", "Telnet");
        protocolsValuesDisctionar.put("teredo", "Teredo");
        protocolsValuesDisctionar.put("tftp", "TFTP");
        protocolsValuesDisctionar.put("thunder", "Thunder");
        protocolsValuesDisctionar.put("tor", "Tor");
        protocolsValuesDisctionar.put("truphone", "Truphone");
        protocolsValuesDisctionar.put("tuenti", "Tuenti");
        protocolsValuesDisctionar.put("tvants", "TVants");
        protocolsValuesDisctionar.put("tvuplayer", "TVUPlayer");
        protocolsValuesDisctionar.put("twitch", "Twitch");
        protocolsValuesDisctionar.put("twitter", "Twitter");
        protocolsValuesDisctionar.put("ubntac2", "Ubntac2");
        protocolsValuesDisctionar.put("ubuntuone", "Ubuntuone");
        protocolsValuesDisctionar.put("unencryped_jabber", "Jabber");
        //protocolsValuesDisctionar.put("unknown", "Unknown");
        protocolsValuesDisctionar.put("upnp", "UPnP");
        protocolsValuesDisctionar.put("usenet", "Usenet");
        protocolsValuesDisctionar.put("vevo", "Vevo");
        protocolsValuesDisctionar.put("vhua", "Vhua");
        protocolsValuesDisctionar.put("viber", "Viber");
        protocolsValuesDisctionar.put("vmware", "VMWare");
        protocolsValuesDisctionar.put("vnc", "VNC");
        protocolsValuesDisctionar.put("vrrp", "VRRP");
        protocolsValuesDisctionar.put("warcraft3", "Warcraft3");
        protocolsValuesDisctionar.put("waze", "Waze");
        protocolsValuesDisctionar.put("webex", "Webex");
        protocolsValuesDisctionar.put("webm", "");
        protocolsValuesDisctionar.put("whatsapp", "Whatsapp");
        protocolsValuesDisctionar.put("whatsappvoice", "Whatsapp Voice");
        protocolsValuesDisctionar.put("whois-das", "Whois");
        protocolsValuesDisctionar.put("wikipedia", "Wikipedia");
        protocolsValuesDisctionar.put("windowsmedia", "Windows Media ");
        protocolsValuesDisctionar.put("windowsupdate", "Windows Update");
        protocolsValuesDisctionar.put("worldofkungfu", "World Of Kung Fu");
        protocolsValuesDisctionar.put("worldofwarcraft", "World Of War Craft");
        protocolsValuesDisctionar.put("xbox", "Xbox");
        protocolsValuesDisctionar.put("xdmcp", "XDMCP");
        protocolsValuesDisctionar.put("yahoo", "Yahoo");
        protocolsValuesDisctionar.put("youtube", "Youtube");
        protocolsValuesDisctionar.put("zattoo", "Zattoo");
        protocolsValuesDisctionar.put("zeromq", "Zeromq");

    }

    private void prepareProtoclDescription() {

        protocolsDesscriptionDisctionar.put("afp", "Apple Filing Protocol is an Apple File Service Network Protocol that offers file services for macOS.");
        protocolsDesscriptionDisctionar.put("aimini", "Aimini is an online solution to store, send and share files. ");
        protocolsDesscriptionDisctionar.put("amazon", "This protocol plug-in classifies the generic web traffic related to Amazon services.");
        protocolsDesscriptionDisctionar.put("tls", "Transport Layer Security, and its now-deprecated predecessor, Secure Sockets Layer, are cryptographic protocols designed to provide communications security over a computer network.");
        protocolsDesscriptionDisctionar.put("microsoft365", "Office 365 is a line of subscription services offered by Microsoft as part of the Microsoft Office product line.");
        protocolsDesscriptionDisctionar.put("googlehangoutduo", "Google Duo is a free, simple video calling io that brings you face-to-face with the people who matter most.");

        protocolsDesscriptionDisctionar.put("dnscrypt","Dnscrypt authenticates, encrypts, and optionally anonymizes communication between a DNS client and DNS resolver.");
        protocolsDesscriptionDisctionar.put("bjnp","BJNP is a LAN service discovery protocol used by Canon printers and scanners.");
        protocolsDesscriptionDisctionar.put("ookla","Ookla provides speed test and network performance data.");
        protocolsDesscriptionDisctionar.put("signal","Signal is a non-federated cryptographic protocol used for end-to-end encryption for voice calls and instant messaging.");
        protocolsDesscriptionDisctionar.put("nestlogsink","Proprietary binary protocol used by Google Nest for logging data for Nest devices.");
        protocolsDesscriptionDisctionar.put("http_activesync","Protocol used for synchronization of email, contacts, calendar, tasks, and notes from a messaging server to a smartphone or device.");
        protocolsDesscriptionDisctionar.put("bloomberg","Protocol used by financial data and news distribution platform.");
        protocolsDesscriptionDisctionar.put("smbv3","Server Message Block protocol used in windows client server environments.");
        protocolsDesscriptionDisctionar.put("ntop","Network detection protocol.");
        protocolsDesscriptionDisctionar.put("mining","Protocol used for pooling resources by cryptocurrency miners.");
        protocolsDesscriptionDisctionar.put("nintendo","Nintendo gaming console platform.");
        protocolsDesscriptionDisctionar.put("amqp","Advanced Messaging Queing Protocol used by messaging platforms and services.");
        protocolsDesscriptionDisctionar.put("genericprotocol","A generic communication protocol.");
        protocolsDesscriptionDisctionar.put("checkmk","Checkmk is a tool for infrastructure and application monitoring.");
        protocolsDesscriptionDisctionar.put("lisp","Locator ID Separation Protocol is a network architecture and protocol .");
        protocolsDesscriptionDisctionar.put("playstation","Sony Playstation gaming protocol.");
        protocolsDesscriptionDisctionar.put("targus_dataspeed","Targus dataspeed protocol.");
        protocolsDesscriptionDisctionar.put("memcached","General purpose distributed memory-caching system.");
        protocolsDesscriptionDisctionar.put("smpp","Short Message Peer to Peer Protocol.");
        protocolsDesscriptionDisctionar.put("iflix","Video streaming service.");
        protocolsDesscriptionDisctionar.put("googleservices","Google search services and protocols.");
        protocolsDesscriptionDisctionar.put("smbv1","Server Message Block protocol used in windows client server environments.");
        protocolsDesscriptionDisctionar.put("googledrive","Google cloud storage for storing documents.");
        protocolsDesscriptionDisctionar.put("csgo","Counterstrike Global Offensive protocol.");
        protocolsDesscriptionDisctionar.put("messenger","Microsoft instant messenger.");
        protocolsDesscriptionDisctionar.put("skypecall","Skype VOIP call protocol.");
        protocolsDesscriptionDisctionar.put("applepush","Apple Push Message Services for message notification on iOS devices.");
        protocolsDesscriptionDisctionar.put("linkedin","LinkedIn social media.");
        protocolsDesscriptionDisctionar.put("amazonvideo","Amazon streaming video service.");
        protocolsDesscriptionDisctionar.put("youtubeupload","YouTube streaming video.");
        protocolsDesscriptionDisctionar.put("ajp","Apache JServ Protocol for proxy.");
        protocolsDesscriptionDisctionar.put("soundcloud","Streaming music service.");
        protocolsDesscriptionDisctionar.put("googleplus","GooglePlus social network by Google.");
        protocolsDesscriptionDisctionar.put("playstore","Android io store for content distribution.");
        protocolsDesscriptionDisctionar.put("pastebin","Website where you can store text online for a set period of time.");
        protocolsDesscriptionDisctionar.put("modbus","Communication protocol for use with programmable logic controllers (PLC).");
        protocolsDesscriptionDisctionar.put("iec60870","Communication protocol for sending data between two systems.");
        protocolsDesscriptionDisctionar.put("zabbix","Open source monitoring software tool for use with XMPP protocol.");
        protocolsDesscriptionDisctionar.put("websocket","Websocket enables two-way communication between a client and server.");
        protocolsDesscriptionDisctionar.put("s7comm","Siemens proprietary protocol for communication between S7 PLCs.");
        protocolsDesscriptionDisctionar.put("fix","Financial Information Exchange protocol is an open protocol for exchange of financial information.");
        protocolsDesscriptionDisctionar.put("facebookzero","Protocol that enables users to access Facebook services without incurring any charges.");
        protocolsDesscriptionDisctionar.put("tinc","Open source self-routing, mesh networking protocol.");
        protocolsDesscriptionDisctionar.put("diameter","Diameter is used to exchange authentication, authorization, and accounting information in LTE and IMS networks.");
        protocolsDesscriptionDisctionar.put("wechat","Multi-purpose messaging, social media, and mobile payment io developed by Tencent..");
        protocolsDesscriptionDisctionar.put("github","Online repository to store data, code, and files.");
        protocolsDesscriptionDisctionar.put("teams","Communication and collaboration platfrom by Microsoft.");
        protocolsDesscriptionDisctionar.put("whatsappfiles","WhatsApp communication io.");
        protocolsDesscriptionDisctionar.put("dnp3","Distributed Network Protocol 3 used by water and electric utility companies.");
        protocolsDesscriptionDisctionar.put("applestore","Apple store io.");
        protocolsDesscriptionDisctionar.put("tiktok","Video sharing social media network developed by ByteDance..");
        protocolsDesscriptionDisctionar.put("someip","Automotive and embedded communication protocol.");
        protocolsDesscriptionDisctionar.put("capwap","Control and Provisioning of Wireless Access Points is used to configure wireless access points.");



        protocolsDesscriptionDisctionar.put("apple", "Apple Inc. is a technology provider of consumer electronics, software and services.");
        protocolsDesscriptionDisctionar.put("appleicloud", "iCloud is a cloud storage and cloud computing service from Apple Inc.");
        protocolsDesscriptionDisctionar.put("appleitunes", "iTunes is a media player/library, online radio, and mobile device management io by Apple Inc.");
        protocolsDesscriptionDisctionar.put("applejuice", "AppleJuice is a peer-to-peer file exchange protocol.");
        protocolsDesscriptionDisctionar.put("armagetron", "Armagetron is a multiplayer snake game in 3D.");
        protocolsDesscriptionDisctionar.put("avi", "Audio Video Interleave (avi) is a multimedia container format as part of Video for Windows software.");
        protocolsDesscriptionDisctionar.put("ayiya", "Anything In Anything (AYIYA) is a networking protocol for managing IP tunneling protocols.");
        protocolsDesscriptionDisctionar.put("Battlefield", "Battlefield is a series of first-person shooter video games.");
        protocolsDesscriptionDisctionar.put("bgp", "BGP; exterior gateway protocol (EGP) used to exchange routing information among routers in different autonomous systems (ASs).");
        protocolsDesscriptionDisctionar.put("bittorrent", "BitTorrent is a communication protocol for peer-to-peer file sharing over the Internet.");
        protocolsDesscriptionDisctionar.put("ciscoskinny", "Skinny Client Control Protocol (SCCP) is a Cisco proprietary standard for terminal control for use with VoIP.");
        protocolsDesscriptionDisctionar.put("Ciscovpn", "Citrix is a provider of servers, application and desktop virtualization, networking, software as a service, and cloud computing technologies.");
        protocolsDesscriptionDisctionar.put("citrix", "Citrix is a provider of servers, application and desktop virtualization, networking, software as a service, and cloud computing technologies.");
        protocolsDesscriptionDisctionar.put("citrix_online", "Citrix Online is a web-based remote access, support and collaboration software.");
        protocolsDesscriptionDisctionar.put("Cloudflare", "Cloudflare is a provider of internet security services, and content delivery network.");
        protocolsDesscriptionDisctionar.put("cnn", "Cable News Network is a basic cable and satellite television news channel.");
        protocolsDesscriptionDisctionar.put("coap", "Constrained Application Protocol (CoAP) is a specialized web transfer protocol for use with constrained nodes and constrained (e.g., low-power) networks.");
        protocolsDesscriptionDisctionar.put("collectd", "collectd is a Unix daemon that collects, transfers and stores performance data of computers and network equipment.");
        protocolsDesscriptionDisctionar.put("corba", "Common Object Request Broker Architecture (CORBA) is a middleware solution standard to provide interoperability among distributed objects.");
        protocolsDesscriptionDisctionar.put("crossfire", "CrossFire is an online shooter game for Microsoft Windows.");
        protocolsDesscriptionDisctionar.put("dce_rpc", "DCE/RPC is a specification for a remote procedure call mechanism that defines both APIs and an over-the-network protocol.");
        protocolsDesscriptionDisctionar.put("deezer", "Deezer is an internet based music streaming service.");
        protocolsDesscriptionDisctionar.put("dhcp", "Dynamic Host Configuration Protocol is a standardized network protocol used on Internet Protocol networks.");
        protocolsDesscriptionDisctionar.put("dhcpv6", "Dynamic Host Configuration Protocol version 6 (DHCPv6) is a network protocol for configuring IPv6 required to operate in an IPv6 network.");
        protocolsDesscriptionDisctionar.put("direct_download_link", "Direct download link (DDL), is a term used within the Internet-based file sharing community and uses client–server architecture.");
        protocolsDesscriptionDisctionar.put("directconnect", "Direct Connect (DC) is a peer-to-peer file sharing protocol.");
        protocolsDesscriptionDisctionar.put("dns", "The DNS protocol is used to translate internet names (www.site.com) into IP addresses and vice versa.");
        protocolsDesscriptionDisctionar.put("dofus", "Dofus is a multiplayer onilne game.");
        protocolsDesscriptionDisctionar.put("drda", "DRDA is a universal, distributed data protocol.");
        protocolsDesscriptionDisctionar.put("dropbox", "Dropbox is a cloud based file hosting service.");
        protocolsDesscriptionDisctionar.put("eaq", "EAQ measures the quality indicators of telecommunication networks that support the fixed and mobile Broadband internet access.");
        protocolsDesscriptionDisctionar.put("ebay", "Ebay is an online commerce service.");
        protocolsDesscriptionDisctionar.put("edonkey", "eDonkey Network is a decentralized, server-based, peer-to-peer file sharing network.");
        protocolsDesscriptionDisctionar.put("egp", "EGP is used to interconnect autonomous systems.");
        protocolsDesscriptionDisctionar.put("epp", "The Extensible Provisioning Protocol (EPP) is a flexible protocol designed for allocating objects within registries over the Internet.");
        protocolsDesscriptionDisctionar.put("facebook", "Facebook is an online social and social networking service.");
        protocolsDesscriptionDisctionar.put("fasttrack", "FastTrack is a peer-to-peer protocol for file sharing.");
        protocolsDesscriptionDisctionar.put("fiesta", "No description found");
    //    protocolsDesscriptionDisctionar.put("filetopia", "Filetopia is a free, multi-platform peer-to-peer file sharing client, and networking tool.");
        protocolsDesscriptionDisctionar.put("filetopia", "Flash Video is a container file format used to deliver digital video content over the internet. OR Adobe Flash Media Playback is a dynamic HTTP streaming protocol used to access video contents from a smart client application.");
        protocolsDesscriptionDisctionar.put("florensia", "No description found");
        protocolsDesscriptionDisctionar.put("ftp_control", "FTP Control is an advanced FTP client supporting passive mode (PASV) download between the client/server or host, and cross server transfers.");
        protocolsDesscriptionDisctionar.put("ftp_data", "FTP data protocol is used for transfer of files on a computer network.");
        protocolsDesscriptionDisctionar.put("git", "Git is a version control system for tracking changes in computer files and coordinating work on those files among multiple people.");
        protocolsDesscriptionDisctionar.put("gmail", "Gmail is a free email service.");
        protocolsDesscriptionDisctionar.put("gnutella", "Gnutella is a peer-to-peer protocol.");
        protocolsDesscriptionDisctionar.put("google", "Google provides internet related services including search, cloud computing, software and hardware.");
        protocolsDesscriptionDisctionar.put("googlehangout", "Google Hangouts is a communication platform for instant messaging, video chat, SMS and VOIP features.");
        protocolsDesscriptionDisctionar.put("googlemaps", "Google Maps is a web mapping service by Google, offering GPS, satellite imagery and real-time traffic.");
        protocolsDesscriptionDisctionar.put("gre", "Generic Routing Encapsulation is a tunneling protocol used to encapsulate a variety of network layer protocols inside virtual point-to-point links over an Internet Protocol network.");
        protocolsDesscriptionDisctionar.put("gtp", "GTP is used to establish a GTP tunnel, for user equipment, between a Serving Gateway (S-GW) and Packet Data Network Gateway (P-GW). OR GTP' (GTP prime) is an IP based protocol used within GSM and UMTS networks");
        protocolsDesscriptionDisctionar.put("guildwars", "Guild Wars is a multiplayer online game.");
        protocolsDesscriptionDisctionar.put("h323", "h323 defines the protocols to provide audio visual communication sessions on any packet network.");
        protocolsDesscriptionDisctionar.put("halflife2", "Half Life 2 is a first person shooter game.");
        protocolsDesscriptionDisctionar.put("hep", "HEP/EEP Extensible Provisioning Protocol is a flexible protocol designed for allocating objects within registries over the Internet.");
        protocolsDesscriptionDisctionar.put("hotmail", "Hotmail is a free email service by Microsoft.");
        protocolsDesscriptionDisctionar.put("hotspotshield", "Hotspot Shield is used for securing Internet connections, often in unsecured networks.");
        protocolsDesscriptionDisctionar.put("https", "HTTPS (HTTP Secure) is an adaptation of the Hypertext Transfer Protocol (HTTP) for secure communication over a computer network, and is widely used on the Internet.");
        protocolsDesscriptionDisctionar.put("http", "The Hypertext Transfer Protocol (HTTP) is an application protocol for distributed, collaborative, and hypermedia information systems.");
        protocolsDesscriptionDisctionar.put("http_application_activesync", "HTTP Application Active Sync is a mobile data synchronization io.");
        protocolsDesscriptionDisctionar.put("http_connect", "HTTP Connect method serves for a handshake between the client and the remote server.");
        protocolsDesscriptionDisctionar.put("phttp_proxy", "HTTP Proxy is a content filter that examines web traffic");
        protocolsDesscriptionDisctionar.put("httpdownload", "HTTP download protocol is used to transfer files from a server to client.");
        protocolsDesscriptionDisctionar.put("iax", "IAX is a communications protocol used for transporting VoIP telephony sessions between servers and to terminal devices.");
        protocolsDesscriptionDisctionar.put("icecast", "Icecast is a protocol used to stream audio files over HTTP.");
        protocolsDesscriptionDisctionar.put("icmp", "ICMP is a supporting protocol in the internet protocol suite.");
        protocolsDesscriptionDisctionar.put("icmpv6", "ICMPv6 is used by IPv6 nodes to report errors encountered in processing packets, and to perform other internet-layer functions.");
        protocolsDesscriptionDisctionar.put("igmp", "The Internet Group Management Protocol (IGMP) is used by IP hosts to report their multicast group membership to routers.");
        protocolsDesscriptionDisctionar.put("imap", "Internet Message Access Protocol (IMAP) is an Internet standard protocol used by e-mail clients to retrieve e-mail messages from a mail server over a TCP/IP connection.");
        protocolsDesscriptionDisctionar.put("imaps", "Internet Message Access Protocol (IMAP) is an Internet standard protocol used by e-mail clients to retrieve e-mail messages from a mail server over encrypted SSL.");
     //   protocolsDesscriptionDisctionar.put("imesh", "iMesh is a peer-to-peer protocol.");
        protocolsDesscriptionDisctionar.put("instagram", "Instagram is a cross platform internet based photo sharing application/service.");
        protocolsDesscriptionDisctionar.put("ip_in_ip", "IP in IP is an IP tunneling protocol that encapsulates one IP packet in another IP packet.");
        protocolsDesscriptionDisctionar.put("ipp", "IPP is an internet protocol for communication between computers and printers.");
        protocolsDesscriptionDisctionar.put("ipsec", "Internet Protocol security (IPsec) uses cryptographic security services to protect communications over Internet Protocol (IP) networks.");
        protocolsDesscriptionDisctionar.put("irc", "IRC facilitates communication (chat process) in the form of text.");
        protocolsDesscriptionDisctionar.put("kakaotalk", "kakaotalk is a free mobile instant messaging application.");
        protocolsDesscriptionDisctionar.put("kakaotalk_voice", "Kakaotalk Voice is a service for telephony using the internet.");
        protocolsDesscriptionDisctionar.put("kerberos", "Kerberos is a network authentication protocol");
        protocolsDesscriptionDisctionar.put("kontiki", "Kontiki peer-to-peer protocol drives the Kontiki Delivery Management System - a video and content delivery platform.");
        protocolsDesscriptionDisctionar.put("lastfm", "Lastfm is a music website.");
        protocolsDesscriptionDisctionar.put("ldap", "LDAP, Lightweight Directory Access Protocol, is an Internet protocol that email and other programs use to look up information from a server.");
        protocolsDesscriptionDisctionar.put("llmnr", "Link-Local Multicast Name Resolution (LLMNR) protocol allows IPv4/IPv6 hosts to perform name resolution for hosts on the same local link.");
        protocolsDesscriptionDisctionar.put("lotusnotes", "Lotus Notes refers to the Notes client used to access mails and notes.");
        protocolsDesscriptionDisctionar.put("lync", "Lync provides instant messaging, audio, video and instant messaging communications.");
        protocolsDesscriptionDisctionar.put("maplestory", "MapleStory is a free-to-play multiplayer online role-playing game.");
        protocolsDesscriptionDisctionar.put("mdns", "Multicast Domain Name System (mDNS) resolves host names to IP addresses within small networks that do not include a local name server.");
        protocolsDesscriptionDisctionar.put("megaco", "Megaco is a signaling protocol that enables switching of voice, fax and multimedia calls between the PSTN and IP networks.");
        protocolsDesscriptionDisctionar.put("mgcp", "The Media Gateway Control Protocol is a signaling and call control communications protocol used in voice over IP telecommunication systems.");
        protocolsDesscriptionDisctionar.put("microsoft", "Microsoft is a technology provider of software and hardware.");
        protocolsDesscriptionDisctionar.put("mms", "Multimedia Messaging Services (MMS) communications allows users to exchange multimedia communications.");
        protocolsDesscriptionDisctionar.put("move", "No description found");
     //   protocolsDesscriptionDisctionar.put("mpeg", "MPEG-Transport Stream is a protocol used for MPEG flows transmission.");
        protocolsDesscriptionDisctionar.put("mpeg_ts", "MPEG-Transport Stream is a protocol used for MPEG flows transmission.");
        protocolsDesscriptionDisctionar.put("mqtt", "MQTT is an ISO standard publish-subscribe-based messaging protocol.");
        protocolsDesscriptionDisctionar.put("ms_one_drive", "OneDrive is an online file-hosting service by Microsoft.");
        protocolsDesscriptionDisctionar.put("msn", "MSN is a web portal and collection of internet services and apps for windows.");
        protocolsDesscriptionDisctionar.put("mssql-tds", "Tabular Data Stream (TDS) is an application layer protocol, used to transfer data between a database server and a client.");
        protocolsDesscriptionDisctionar.put("mysql", "MySQL protocol is used between MySQL Clients and a MySQL Server.");
        protocolsDesscriptionDisctionar.put("netbios", "NetBios Name Service (NBNS) is a protocol which makes it possible to manage the names in a Microsoft NetBios network. ");
        protocolsDesscriptionDisctionar.put("netflix", "Netflix service provides online streaming media and video-on-demand.");
        protocolsDesscriptionDisctionar.put("netflow", "NetFlow is a network protocol for the collection and monitoring of network traffic flow data.");
        protocolsDesscriptionDisctionar.put("nfs", "The NFS protocol is one of several distributed file system standards for network-attached storage ");
        protocolsDesscriptionDisctionar.put("noe", "No description found");
        protocolsDesscriptionDisctionar.put("ntp", "Network Time Protocol is used to synchronize system clocks among a set of distributed time servers and clients.");
        protocolsDesscriptionDisctionar.put("ocs", "Open Collaboration Services (OCS) allows exchange of relevant data and applications use this protocol to access multiple services providing OCS APIs.");
        protocolsDesscriptionDisctionar.put("office365", "Office 365 is a group of Microsoft services and software’s.");
      //  protocolsDesscriptionDisctionar.put("oggvorbis", "Ogg Vorbis is a free general-purpose compressed audio format.");
        protocolsDesscriptionDisctionar.put("opendns", "OpenDNS is a company /service that extends the Domain Name System by adding features such as phishing protection and optional content filtering.");
        protocolsDesscriptionDisctionar.put("openft", "openFT is Fujitsu's product family of a company-wide file transfer solution  ");
        protocolsDesscriptionDisctionar.put("openvpn", "Openvpn is an open source application that provides remote access facilities.");
        protocolsDesscriptionDisctionar.put("oracle", "Oracle develops hardware systems and enterprise software products, particularly its own brand of database management systems.");
        protocolsDesscriptionDisctionar.put("oscar", "OSCAR (Open System for Communication in Realtime) is used in both ICQ and AIM services.");
        protocolsDesscriptionDisctionar.put("ospf", "OSPF (Open Short Path First) is a link state routing protocol used within large autonomous system networks.");
        protocolsDesscriptionDisctionar.put("pando_media_booster", "Pando Media Booster (PMB) is an application used by Pando game and software publishers to ensure safe, complete and speedy downloads of large files.");
        protocolsDesscriptionDisctionar.put("pandora", "Pandora is a music streaming and automated music service");
        protocolsDesscriptionDisctionar.put("pcanywhere", "PC Anywhere is a suite of computer programs.");
        protocolsDesscriptionDisctionar.put("pop3", "POP3 is a client/server standard protocol for receiving e-mails.");
        protocolsDesscriptionDisctionar.put("pops", "Post Office Protocol is an application-layer Internet standard protocol used by local e-mail clients to retrieve e-mail from a remote server over a TCP/IP connection.");
        protocolsDesscriptionDisctionar.put("postgresql", "PostgreSQL is an object-relational database management system (ORDBMS) ");
        protocolsDesscriptionDisctionar.put("pplive", "PPLIVE is an application intended to watch TV in peer-to-peer. ");
        protocolsDesscriptionDisctionar.put("ppstream", "PPStream protocol provides audio and video streaming. It is based on bittorent (peer-to-peer) technology. ");
        protocolsDesscriptionDisctionar.put("pptp", "Point-to-Point Tunneling Protocol is a method for implementing virtual private networks, with many known security issues.");
        protocolsDesscriptionDisctionar.put("qq", "QQ is the most popular free instant messaging computer program in China.");
        protocolsDesscriptionDisctionar.put("qqlive", "QQLive is an application intended to watch TV in Peer-to-Peer mode.");
      //  protocolsDesscriptionDisctionar.put("quake", "Quake is a video game.");
        protocolsDesscriptionDisctionar.put("quic", "QUIC is an experimental protocol, and it supports a set of multiplexed connections over UDP.");
        protocolsDesscriptionDisctionar.put("quickplay", "Quickplay provides services for premium multi-screen video to IP-connected devices.");
      //  protocolsDesscriptionDisctionar.put("quicktime", "QuickTime is an extensible multimedia framework developed by Apple Inc.");
        protocolsDesscriptionDisctionar.put("radius", "RADIUS is an AAA protocol that manages network access. ");
        protocolsDesscriptionDisctionar.put("rdp", "RDP allows a personal computer to be used remotely.");
        protocolsDesscriptionDisctionar.put("realmedia", "RealMedia is a proprietary multimedia container format created by RealNetworks.");
        protocolsDesscriptionDisctionar.put("redis", "Redis is an open-source in-memory database project implementing a distributed, in-memory key-value store with optional durability.");
        protocolsDesscriptionDisctionar.put("remotescan", "RemoteScan is the most stable and seamless solution for document scanning and image acquisition within complex network environments.");
        protocolsDesscriptionDisctionar.put("rsync", "Rsync is a protocol used by various services performing updates.");
        protocolsDesscriptionDisctionar.put("rtcp", "RTCP provides out-of-band statistics and control information for an RTP session.");
        protocolsDesscriptionDisctionar.put("rtmp", "Real-Time Messaging Protocol (RTMP) used for streaming audio, video and data over the Internet, between a Flash player and a server.");
        protocolsDesscriptionDisctionar.put("rtp", "The Real-time Transport Protocol is a network protocol for delivering audio and video over IP networks.");
        protocolsDesscriptionDisctionar.put("rtsp", "The Real Time Streaming Protocol is a network control protocol designed for use in entertainment and communications systems to control streaming media servers. ");
        protocolsDesscriptionDisctionar.put("rx", "no description found");
        protocolsDesscriptionDisctionar.put("sap", "Session Announcement Protocol (SAP) is an experimental protocol for broadcasting multicast session information.");
        protocolsDesscriptionDisctionar.put("sctp", "SCTP (Stream Control Transmission Protocol) is a protocol designed to transport PSTN signaling messages over IP networks.");
        protocolsDesscriptionDisctionar.put("sflow", "sFlow is an industry standard technology for monitoring high speed switched networks.");
        protocolsDesscriptionDisctionar.put("shoutcast", "SHOUTcast DNAS is cross-platform proprietary software for streaming media over the Internet. ");
        protocolsDesscriptionDisctionar.put("sina(weibo)", "Sina Weibo is a Chinese microblogging website.");
        protocolsDesscriptionDisctionar.put("sip", "SIP communication protocol is used for signaling and controlling communication sessions in applications of internet telephony for voice/video calls.");
        protocolsDesscriptionDisctionar.put("skyfile_postpaid", "SkyFile Mail is a free messaging and compression tool that allows users to send reliable and cost-effective emails, e-faxes and SMS messages. ");
        protocolsDesscriptionDisctionar.put("skyfile_prepaid", "SkyFile Mail is a free messaging and compression tool that allows users to send reliable and cost-effective emails, e-faxes and SMS messages. ");
        //protocolsDesscriptionDisctionar.put("skyfile_rudics", "SkyFile Mail uses Iridium Rudics to boost connection quality, service availability and bandwidth");
        protocolsDesscriptionDisctionar.put("skype", "Skype is a telecommunications application providing video and voice chat over the internet to different devices and regular telephones.");
        protocolsDesscriptionDisctionar.put("slack", "Slack is a cloud based set of tools and services.");
        protocolsDesscriptionDisctionar.put("smb", "Stands for \"Server Message Block.\" SMB is a network protocol used by Windows-based computers that allows systems within the same network to share files.");
        protocolsDesscriptionDisctionar.put("smtp", "Simple Mail Transfer Protocol is an Internet standard for electronic mail transmission. ");
        protocolsDesscriptionDisctionar.put("smtps", "SMTPS is used to secure SMTP at the transport layer. ");
        protocolsDesscriptionDisctionar.put("snapchat", "Snapchat is an image messaging and multimedia mobile application");
        protocolsDesscriptionDisctionar.put("snmp", "Simple Network Management Protocol (SNMP) is the protocol governing network management and the monitoring of network devices and their functions.");
        protocolsDesscriptionDisctionar.put("socks", "Socket Secure is an Internet protocol that exchanges network packets between a client and server through a proxy server. ");
        protocolsDesscriptionDisctionar.put("socrates", "SOCRATES is a software system for testing correctness of implementations of IP routing protocols such as RIP, OSPF and BGP.");
        protocolsDesscriptionDisctionar.put("sopcast", "Sopcast is a video streaming service based on a peer-to-peer protocol.");
        protocolsDesscriptionDisctionar.put("soulseek", "Soulseek is a peer-to-peer file-sharing network and application");
        protocolsDesscriptionDisctionar.put("spotify", "Spotify is a music, podcase and video streaming service.");
        protocolsDesscriptionDisctionar.put("ssdp", "Simple Service Discovery Protocol (SSDP) provides a mechanism whereby network clients can discover desired network services");
        protocolsDesscriptionDisctionar.put("ssh", "SSH is a cryptographic network protocol for operating network services securely.");
        protocolsDesscriptionDisctionar.put("ssl", "SSL is a standard security protocol for establishing encrypted links between server and a browser in an online coommunication.");
        protocolsDesscriptionDisctionar.put("ssl_no_cert", "no description found");
        protocolsDesscriptionDisctionar.put("starcraft", "Starcraft is a video game.");
        protocolsDesscriptionDisctionar.put("stealthnet", "Stealthnet is a file sharing application between two or more hosts.");
        protocolsDesscriptionDisctionar.put("steam", "Steam offers digital rights management, multiplayer gaming, video streaming and social networking services. ");
        protocolsDesscriptionDisctionar.put("stun", "Simple Traversal of UDP through NATs (Network Address Translation) is a protocol for assisting devices behind a NAT firewall or router with their packet routing.");
        protocolsDesscriptionDisctionar.put("syslog", "Syslog is a standard for message logging.");
        protocolsDesscriptionDisctionar.put("teamspeak", "TeamSpeak is proprietary voice-over-Internet Protocol software for audio communications.");
        protocolsDesscriptionDisctionar.put("teamviewer", "TeamViewer is a software for remote control, desktop sharing, online meetings, web conferencing and file transfers.");
        protocolsDesscriptionDisctionar.put("telegram", "Telegram is a cloud based instant messaging service.");
        protocolsDesscriptionDisctionar.put("telnet", "Telnet is a protocol that provides bidirectional interactive text oriented communication facilities using virtual terminal connection. ");
        protocolsDesscriptionDisctionar.put("teredo", "Teredo is a transition technology that gives full IPv6 connectivity for IPv6-capable hosts that are on the IPv4 Internet.");
        protocolsDesscriptionDisctionar.put("tftp", "Trivial File Transfer Protocol (TFTP) is an Internet software utility for transferring files that is simpler to use than the File Transfer Protocol (FTP) but less capable.");
        protocolsDesscriptionDisctionar.put("thunder", "Thunder is a Chinese multi-protocol download manager.");
        protocolsDesscriptionDisctionar.put("tor", "Tor is a freeware enabling anonymous communication.");
        protocolsDesscriptionDisctionar.put("truphone", "Truphone is a global mobile network that operates its service internationally. ");
        protocolsDesscriptionDisctionar.put("tuenti", "Tuenti protocol plug-in classifies the http traffic to the host tuenti.com.");
        protocolsDesscriptionDisctionar.put("tvants", "TVants is a peer-to-peer TV software application that provides music, movies, news and more.");
        protocolsDesscriptionDisctionar.put("tvuplayer", "TVUPlayer is an application intended to watch TV in peer_to-peer.");
        protocolsDesscriptionDisctionar.put("twitch", "Twitch is a live streaming video platform.");
        protocolsDesscriptionDisctionar.put("twitter", "Twitter is an online news and social networking service.");
        protocolsDesscriptionDisctionar.put("ubntac2", "No description found");
        protocolsDesscriptionDisctionar.put("ubuntuone", "Ubuntuone is an OpenID-based single sign-on service for all services and sites related to Ubuntu.");
        protocolsDesscriptionDisctionar.put("unencryped_jabber", "Jabber is the instant messaging service based on extensible messaging and presence protocol.");
       // protocolsDesscriptionDisctionar.put("unknown", "No description found");
        protocolsDesscriptionDisctionar.put("upnp", "Universal Plug and Play (UPnP) is a standard that uses Internet and Web protocol s to enable devices .");
        protocolsDesscriptionDisctionar.put("usenet", "Usenet is a worldwide distributed discussion system available on computers.");
        protocolsDesscriptionDisctionar.put("vevo", "Vevo is a video hosting service.");
        protocolsDesscriptionDisctionar.put("vhua", "No description found");
        protocolsDesscriptionDisctionar.put("viber", "Viber is a cross-platform instant messaging and VoIP application.");
        protocolsDesscriptionDisctionar.put("vmware", "VMWare protocol allows network interfaces and remote access to a virtual machine.");
        protocolsDesscriptionDisctionar.put("vnc", "VNC is a graphical desktop sharing system.");
        protocolsDesscriptionDisctionar.put("vrrp", "Virtual Router Redundancy Protocol (VRRP) protocol eliminates the single point of failure inherent in the static default routed environment.");
        protocolsDesscriptionDisctionar.put("warcraft3", "Warcraft 3 is a video game.");
        protocolsDesscriptionDisctionar.put("waze", "Waze is a GPS maps & traffic navigation software.");
        protocolsDesscriptionDisctionar.put("webex", "Webex is a radio broadcasting service.");
        protocolsDesscriptionDisctionar.put("webm", "WebM is a media file format.");
        protocolsDesscriptionDisctionar.put("whatsapp", "Whatsapp is a freeware cross-platform instant messaging service using the internet.");
        protocolsDesscriptionDisctionar.put("whatsappvoice", "Whatsapp Voice is a service for telephony using the internet.");
        protocolsDesscriptionDisctionar.put("whois-das", "The Whois service provides a way for the public to lookup information about registered data. The DAS provides a way for the public to check whether a domain can be registered.");
        protocolsDesscriptionDisctionar.put("wikipedia", "Wikipedia is a free online encyclopedia.");
        protocolsDesscriptionDisctionar.put("windowsmedia", "This protocol plug-in classifies the http traffic to the host windowsmedia.com.");
        protocolsDesscriptionDisctionar.put("windowsupdate", "Windows Update is a service by Microsoft for providing updates for windows components.");
        protocolsDesscriptionDisctionar.put("worldofkungfu", "World Of Kung Fu is a 3D martial arts online game.");
        protocolsDesscriptionDisctionar.put("worldofwarcraft", "World of War Craft is a multiplayer online role playing game.");
        protocolsDesscriptionDisctionar.put("xbox", "Xbox is a home video game console by Microsoft.");
        protocolsDesscriptionDisctionar.put("xdmcp", "XDMCP protocol addresses the issue of discovering and authenticating available X servers on the LAN.");
        protocolsDesscriptionDisctionar.put("yahoo", "Yahoo is a web services provider.");
        protocolsDesscriptionDisctionar.put("youtube", "Youtube is a video sharing website.");
        protocolsDesscriptionDisctionar.put("zattoo", "Zattoo is website and a set of peer-to-peer TV applications.");
        protocolsDesscriptionDisctionar.put("zeromq", "zmq-prebuilt simplifies creating communications for a Node.js application by providing well-tested, ready to use ØMQ bindings.");

    }

    private final ProtocolsDetailExpandabaleAdapter.OnAlertSelectedListener mAlertSelectedListener = new ProtocolsDetailExpandabaleAdapter.OnAlertSelectedListener() {
    };

}
