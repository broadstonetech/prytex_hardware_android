package hardware.prytex.io.fragment.BandwidthFrags;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import hardware.prytex.io.R;
import hardware.prytex.io.fragment.BaseFragment;

import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;

public class ProtocolsListViewMainFragment extends BaseFragment {

    @Override
    public String getTitle() {
        return "Network Usage";
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_protocols_list_view_main;
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);
        TabLayout tabLayout = parent.findViewById(R.id.tab_layout);
        Button btnShowCahrt = parent.findViewById(R.id.btnHostsChart);
        tabLayout.addTab(tabLayout.newTab().setText("Current Data"));
        tabLayout.addTab(tabLayout.newTab().setText("24 hour Data"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        imgInfoBtn.setVisibility(View.VISIBLE);
        boolean isFromBandwidthListViewDetails = true;
        final ViewPager viewPager = parent.findViewById(R.id.pager);
        final TabsPagerListAdapter adapter = new TabsPagerListAdapter
                (getFragmentManager(), tabLayout.getTabCount(), getContext());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 0) {
                    Log.d("Tab", "curent hour TAB selsected");
                } else {
                    Log.d("Tab", "24 hour TAB selsected");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        btnShowCahrt.setOnClickListener(v -> getActivity().finish());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }




}
