package hardware.prytex.io.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.MyToast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;

/**
 * Created by abubaker on 10/4/16.
 */

@SuppressWarnings("ALL")
public class ForgotPasswordFragment extends BaseFragment {

    private EditText edittextUsername;
    private Button btnForgotPassword;
    private SharedPreferences pref;

    @Override
    public int getLayoutId() {
        return R.layout.forgot_passowrd_fragment;
    }

    @Override
    public String getTitle() {
        return "Forgot Password";
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);

        btnForgotPassword = parent.findViewById(R.id.submit);
        TextView tvHeading = parent.findViewById(R.id.txt_title);
        TextView imgBack = parent.findViewById(R.id.imgBack);

        imgBack.setOnClickListener(view -> getHelper().replaceFragment(new LoginFragment(), R.id.fragment_container, true, false));
        parent.findViewById(R.id.submit).setOnClickListener(v -> {
            edittextUsername = getParentView().findViewById(R.id.username_edit);
            if (edittextUsername.length() == 0) {
                MyToast.showMessage(getActivity(), getString(R.string.err_msg_username));
                return;
            }
            btnForgotPassword.setBackgroundColor(getResources().getColor(R.color.colorBtnPressed));
            JSONObject data = new JSONObject();
            try {
                data.put("app_id", pref.getString("app_id", ""));
                data.put("request_id", Dmoat.getNewRequestId());
                JSONObject userNameData = new JSONObject();
                userNameData.put("username", edittextUsername.getText().toString());
                data.put("data", userNameData);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            getParentView().findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
            Api.forgotPassword(getActivity(), data, mListener);
//                getParentView().findViewById(R.id.submit).setVisibility(View.GONE);
        });
    }

    private final AsyncTaskListener mListener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            btnForgotPassword.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            if (!isAdded()) {
                return;
            }
            getParentView().findViewById(R.id.progressBar).setVisibility(View.GONE);
//            getParentView().findViewById(R.id.submit).setVisibility(View.VISIBLE);
            String stringToSearch = result.message;
            Pattern p, pInProgress;
            Matcher m, mInProgress;
            p = Pattern.compile("Please request again afte");   // the pattern to search for request after after specified time
            pInProgress = Pattern.compile("Request is already in process");   // the pattern to search for request after after specified time
            m = p.matcher(stringToSearch);
            mInProgress = pInProgress.matcher(stringToSearch);

            if (result.code == 400 && m.find()) {
                MyToast.showMessage(getActivity(), result.message);
//                VerifyCodeFragment verifyCodeFragment = new VerifyCodeFragment();
//                Bundle userNameBunle = new Bundle();
//                userNameBunle.putBoolean("forgot_password", true);
//                userNameBunle.putString("username", edittextUsername.getText().toString());
//                verifyCodeFragment.setArguments(userNameBunle);
//                getHelper().onBack();
//                getHelper().addFragment(verifyCodeFragment, false, true);
                /*if (result.code == 400)
                    MyToast.showMessage(getActivity(), "Invalid Username!");
                else if (result.code == 503)
                    MyToast.showMessage(getActivity(), "Unable to proceed! Please try again");
                else*/
            } else if (result.code == 200 && mInProgress.find()) {
//                MyToast.showMessage(getActivity(), getResources().getString(R.string.forgotUsernamePwdSuccess));
                VerifyCodeFragment verifyCodeFragment = new VerifyCodeFragment();
                String descriptionText = getContext().getResources().getString(R.string.verify_code_description_a)+ " "+result.userEmailForDiscovery+getContext().getResources().getString(R.string.verify_code_description_b);
                Bundle userNameBunle = new Bundle();
                userNameBunle.putBoolean("forgot_password", true);
                userNameBunle.putString("username", edittextUsername.getText().toString());
                userNameBunle.putString("desription", descriptionText);
                verifyCodeFragment.setArguments(userNameBunle);
//                getHelper().onBack();
                getHelper().addFragment(verifyCodeFragment, false, true);
                /*if (result.code == 400)
                    MyToast.showMessage(getActivity(), "Invalid Username!");
                else if (result.code == 503)
                    MyToast.showMessage(getActivity(), "Unable to proceed! Please try again");
                else*/
            } else if (result.code == 200) {
//                MyToast.showMessage(getActivity(), getResources().getString(R.string.forgotUsernamePwdSuccess));
                VerifyCodeFragment verifyCodeFragment = new VerifyCodeFragment();
                String descriptionText = getContext().getResources().getString(R.string.verify_code_description_a)+ " "+result.userEmailForDiscovery+getContext().getResources().getString(R.string.verify_code_description_b);
                Bundle userNameBunle = new Bundle();
                userNameBunle.putBoolean("forgot_password", true);
//                userNameBunle.putString("registered_email", result.message);
                userNameBunle.putString("username", edittextUsername.getText().toString());
                userNameBunle.putString("desription", descriptionText);
                verifyCodeFragment.setArguments(userNameBunle);
//                getHelper().onBack();
                getHelper().addFragment(verifyCodeFragment, false, true); //security code expired after three tries, please request
            } else if (result.code == 503) {
                showAlertMsgDialog(getContext(), result.message);
            } else if (result.code == 409) {
                showAlertMsgDialog(getContext(), result.message);
            } else {
                if (result.message.equalsIgnoreCase("")) {
                    result.message = getResources().getString(R.string.error_message_generic);
                } else {
                }
                MyToast.showMessage(getActivity(), result.message);
            }
        }
    };


}
