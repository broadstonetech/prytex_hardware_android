package hardware.prytex.io.fragment.AlertsTabs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.activity.AlertsMainActivity;
import hardware.prytex.io.adapter.BlockedAndMutedAdapter;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.model.MutedBlockedAlertsModel;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.GeneralUtils;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;


import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;
import static hardware.prytex.io.activity.AlertsMainActivity.isFirstCallBlockedAlerts;
import static hardware.prytex.io.activity.AlertsMainActivity.tabLayout;
import static hardware.prytex.io.parser.BlockedAlertParser.listOfMutedBlockedAlerts;

/**
 * Created by khawarraza on 10/11/2016.
 */

@SuppressWarnings("ALL")
public class BlockedAlertsFragment extends BaseFragment {

    private ProgressDialog dialog;
    private BlockedAndMutedAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private MaterialSpinner spinner1;
    //    TextView tvBlockedAlertsCount;
    private SharedPreferences pref;
    private String appID = "";
    private RecyclerView mRecyclerViewswip;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    @Override
    public int getLayoutId() {
        return R.layout.blocked_alert_fragment;
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getActivity().getSharedPreferences("MyPref", 0);

        appID = pref.getString("app_id", "");
        LinearLayout container = parent.findViewById(R.id.alerts_container);
//        tvBlockedAlertsCount = (TextView) parent.findViewById(R.id.tvAlertsCount);
        spinner1 = parent.findViewById(R.id.spinner);
        mRecyclerView = parent.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        imgInfoBtn.setVisibility(View.VISIBLE);
        if (GeneralUtils.isConnected(getContext())) {
            if (isFirstCallBlockedAlerts || listOfMutedBlockedAlerts.size() < 1) {
                showProgressDialog(true);
                Api.getBlockedAlerts(getActivity(), getRequestParams(), listener);
            } else {
                listOfMutedBlockedAlerts.size();
//                    tvBlockedAlertsCount.setText(listOfMutedBlockedAlerts.size());
                tabLayout.getTabAt(1).setText("Blocked Events" + " (" + listOfMutedBlockedAlerts.size() + ")");
                mAdapter = new BlockedAndMutedAdapter(getContext(), listOfMutedBlockedAlerts, new ArrayList<>(), "blocked_fragment", appID);
                mRecyclerView.setAdapter(mAdapter);
            }

        } else {
            showAlertMsgDialog(getContext(), getResources().getString(R.string.internet_connection));
        }


                spinner1.setItems("All", "New Host", "Blocked IP Adresses", "Network Alert");
                spinner1.setOnItemSelectedListener((MaterialSpinner.OnItemSelectedListener<String>) (view, position, id, item) -> updateAlertsFilter(item));
        mRecyclerViewswip = parent.findViewById(R.id.recycler_viewswip);
        mSwipeRefreshLayout = parent.findViewById(R.id.cnctdhosts_swipe_refresh_layout);
        mRecyclerViewswip.setLayoutManager(new LinearLayoutManager(getActivity()));

        mRecyclerViewswip.setVisibility(View.GONE);
        if (GeneralUtils.isConnected(Objects.requireNonNull(getActivity()))) {

            spinner1.setSelectedIndex(0);

        }

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mRecyclerViewswip.setVisibility(View.GONE);
            refreshContent();
            spinner1.setSelectedIndex(0);
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(Objects.requireNonNull(getContext()));
        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "blocked_alerts", null /* class override */);
    }

    @Override
    public String getTitle() {
        return "Blocked Alerts";
    }

    private final AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (!isAdded()) {
                return;
            }

            showProgressDialog(false);

            if (result.code == 200 || result.code == 2) {
                if (result.message.equalsIgnoreCase("No data detected")) {
                    getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
//                    showAlertMsgDialog(getContext(), "No malicious traffic was detected or blocked.");
                } else {
                    getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
                    AlertsMainActivity.filterCountBlocked = listOfMutedBlockedAlerts.size();
                    Objects.requireNonNull(tabLayout.getTabAt(1)).setText("Blocked Events" + " (" + listOfMutedBlockedAlerts.size() + ")");
                    if (listOfMutedBlockedAlerts.size() > 0) {
                        getParentView().findViewById(R.id.error_label).setVisibility(View.GONE);
                        mAdapter = new BlockedAndMutedAdapter(getContext(), listOfMutedBlockedAlerts, new ArrayList<>(), "blocked_fragment", appID);
                        mRecyclerView.setAdapter(mAdapter);
                    }
                }
            } else if (result.code == 503) {
                getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
                showAlertMsgDialog(getContext(), result.message);
            } else if (result.code == 409) {
                getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
                showAlertMsgDialog(getContext(), result.message);
            } else {
                getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
            }

        }
    };

    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private JSONObject getRequestParams() {
        JSONObject requestData = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            requestData.put("request_id", Dmoat.getNewRequestId());
            requestData.put("app_id", pref.getString("app_id", ""));
            requestData.put("token", UserManager.getInstance().getLoggedInUser().token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestData;
    }

    private void updateAlertsFilter(String item) {
        ArrayList<MutedBlockedAlertsModel> filteredListOfAlerts = new ArrayList<>();
        for (int i = 0; i < listOfMutedBlockedAlerts.size(); i++) {

            if (item == "New Host") {
                if (listOfMutedBlockedAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.NEW_HOST_ALERT)) {
                    filteredListOfAlerts.add(listOfMutedBlockedAlerts.get(i));
                }
            } else if (item == "Sensor") {
                if (listOfMutedBlockedAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.SENSOR_ALERT)) {
                    filteredListOfAlerts.add(listOfMutedBlockedAlerts.get(i));
                }
            } else if (item == "Prytex Hardware Info") {
                if (listOfMutedBlockedAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.INFO_ALERT)) {
                    filteredListOfAlerts.add(listOfMutedBlockedAlerts.get(i));
                }
            } else if (item == "Blocked IP Adresses") {
                if (listOfMutedBlockedAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.BLOCK_IPS_ALERT)) {
                    filteredListOfAlerts.add(listOfMutedBlockedAlerts.get(i));
                }
            } else if (item == "Network Alert") {
                if (listOfMutedBlockedAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.IDS_ALERT)) {
                    filteredListOfAlerts.add(listOfMutedBlockedAlerts.get(i));
                }
            } else if (item == "Flow Alerts") {
                if (listOfMutedBlockedAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.FLOW_ALERT)) {
                    filteredListOfAlerts.add(listOfMutedBlockedAlerts.get(i));
                }
            } else if (item == "All Alerts") {
                filteredListOfAlerts.add(listOfMutedBlockedAlerts.get(i));
            }

            AlertsMainActivity.filterCountBlocked = filteredListOfAlerts.size();
            mAdapter = new BlockedAndMutedAdapter(getContext(), filteredListOfAlerts, new ArrayList<>(), "blocked_fragment", appID);
            mRecyclerView.setAdapter(mAdapter);
        }
    }
    private void refreshContent() {
        Api.getBlockedAlerts(getActivity(), getRequestParams(), listener);
        mSwipeRefreshLayout.setRefreshing(false);

    }
}
