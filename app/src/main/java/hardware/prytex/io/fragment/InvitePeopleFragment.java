package hardware.prytex.io.fragment;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.model.Subscriber;
import hardware.prytex.io.model.User;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.util.MyToast;

import org.json.JSONException;
import org.json.JSONObject;

import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;

/**
 * Created by khawarraza on 23/11/2016.
 */
@SuppressWarnings("ALL")
public class InvitePeopleFragment extends BaseFragment {

    private LinearLayout container;
    private TextView errorLabel;
    private View view;
    private SharedPreferences pref;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_invite_people;
    }

    @Override
    public String getTitle() {
        return "Invites";
    }



    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);

        container = parent.findViewById(R.id.people_container);
        errorLabel = parent.findViewById(R.id.error_label);

        parent.findViewById(R.id.invite_new).setOnClickListener(v -> {
            if (UserManager.getInstance().getLoggedInUser().subscribers.size() >= 3) {
                MyToast.showMessage(getActivity(), "You can add max 3 subscribers!");
                return;
            }
            showNewInviteDialog();
        });

        updateInvites();
    }

    private void updateInvites() {
        container.removeAllViews();

        User user = UserManager.getInstance().getLoggedInUser();
        if (user.subscribers == null && user.subscribers.size() == 0) {
            errorLabel.setVisibility(View.VISIBLE);
            return;
        } else if (user.subscribers != null && user.subscribers.size() == 0){
            errorLabel.setVisibility(View.VISIBLE);
            return;
        }

        for (int i = 0; i < user.subscribers.size(); i++) {
            addSubscriberItemInContainer(user.subscribers.get(i));
        }
    }

    private void addSubscriberItemInContainer(Subscriber subscriber) {
        final View view = View.inflate(getActivity(), R.layout.invite_item, null);
        view.setTag(subscriber);
        TextView email = view.findViewById(R.id.email);
        email.setText(subscriber.email);
        container.addView(view);
        View delete = view.findViewById(R.id.delete_invite);
        delete.setOnClickListener(mDeleteClickListener);
        delete.setTag(view);

        errorLabel.setVisibility(View.GONE);
    }

    private final View.OnClickListener mDeleteClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            view = (View) v.getTag();
            final Subscriber subscriber = (Subscriber) view.getTag();

            JSONObject requestData = new JSONObject();
            JSONObject data = new JSONObject();
            try {
                requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
                requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
                requestData.put("request_id", Dmoat.getNewRequestId());
                requestData.put("app_id", pref.getString("app_id", ""));
                requestData.put("token", UserManager.getInstance().getLoggedInUser().token);

                data.put("username", UserManager.getInstance().getLoggedInUser().userName);
                data.put("subscriber_id", subscriber.email);
                requestData.put("data", data);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            showProgressDialog(true);

            Api.removeSubscriber(getActivity(), requestData, result -> {
                if (!isAdded()) {
                    return;
                }
                showProgressDialog(false);
                if (result.code == 200 || result.code == 2) {
                    MyToast.showMessage(getActivity(), "Waiting for device to make changes");
                } else if (result.code == 503) {
                    showAlertMsgDialog(getContext(), result.message);
                }
                else if (result.code == 409){
                    showAlertMsgDialog(getContext(), result.message);
                }else {
                    MyToast.showMessage(getActivity(), result.message);
                }
            });
        }
    };

    private void showNewInviteDialog() {
        final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setMessage("Please enter a valid email to subscribe.");
        alert.setTitle("Invite Users");

        final EditText edittext = new EditText(getActivity());
        edittext.setSingleLine();
        edittext.setSingleLine(true);
        edittext.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        alert.setView(edittext);

        alert.setPositiveButton("Send", (dialog, whichButton) -> {
            final String email = edittext.getText().toString();
            if (!isEmailValid(email)) {
                MyToast.showMessage(getActivity(), "Please enter a valid email to subscribe.");
                showNewInviteDialog();
                return;
            }

            JSONObject requestData = new JSONObject();
            JSONObject data = new JSONObject();
            try {
                requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
                requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
                requestData.put("request_id", Dmoat.getNewRequestId());
                requestData.put("app_id", pref.getString("app_id", ""));
                requestData.put("token", UserManager.getInstance().getLoggedInUser().token);
                data.put("username", UserManager.getInstance().getLoggedInUser().userName);
                data.put("subscriber_id", email);
                requestData.put("data", data);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            showProgressDialog(true);

            Api.addSubscriber(getActivity(), requestData, result -> {
                if (!isAdded()) {
                    return;
                }
                showProgressDialog(false);
                if (result.code == 200 || result.code == 2) {
                    MyToast.showMessage(getActivity(), "Subscriber added successfull.");
                } else {
                    if (result.code == 403)
                        MyToast.showMessage(getActivity(), "The email address you have entered is already subscribed to receive alerts.");
                    else
                        MyToast.showMessage(getActivity(), result.message);
                }
            });
        });
        alert.setNegativeButton("Cancel", (dialog, which) -> {

        });

        alert.show();
    }

    private ProgressDialog dialog;

    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    public static boolean isEmailValid(String email) {
        return !(email == null || TextUtils.isEmpty(email)) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

}
