package hardware.prytex.io.fragment.Policy.AccessControl;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.db.DBClient;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.fragment.CreateNewPolicy;
import hardware.prytex.io.model.DmoatPolicy;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Arrays;

import io.realm.RealmResults;

import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.util.DmoatUtils.PROTOCOLS;
import static hardware.prytex.io.util.DmoatUtils.PROTOCOLS_KEYS;
import static hardware.prytex.io.util.DmoatUtils.PROTOCOLS_VALUE;

/**
 * Created by khawarraza on 16/12/2016.
 */
@SuppressWarnings("ALL")
public class PoliciesFragment extends BaseFragment {

    //    private PoliciesAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private TextView textViewNoPoliciesDescription;
    private final Paint p = new Paint();
    private ProgressDialog dialog;
    public static boolean isCreatePolicy = false;
    private SharedPreferences pref;


    private RealmResults<DmoatPolicy> policies;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_policies;
    }

    @Override
    public String getTitle() {
        return "Policies Management";
    }


    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);


        textViewNoPoliciesDescription = parent.findViewById(R.id.textview_fragment_policies_no_disp);
        mRecyclerView = parent.findViewById(R.id.recycler_view);

        setProcolosArrays();
        setRecyclerView();
//        getAppPoliciesFromDB(parent);
        imgInfoBtn.setVisibility(View.VISIBLE);

        //swipe function to delete a policy on swipe left
//        initSwipe();

        parent.findViewById(R.id.create_new_policy).setOnClickListener(v -> {
            isCreatePolicy = true;
            getHelper().replaceFragment(new CreateNewPolicy(), false, true);
        });

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
//        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "access_control", null /* class override */);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, "access_control");
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, this.getClass().getName());

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);

    }

    private void setProcolosArrays() {
        PROTOCOLS_KEYS = getResources().getStringArray(R.array.protocols_keys);
        Arrays.sort(PROTOCOLS_KEYS);
        PROTOCOLS_VALUE = getResources().getStringArray(R.array.protocols_value);
        Arrays.sort(PROTOCOLS_VALUE);
        for (int i = 0; i < PROTOCOLS_KEYS.length; i++) {
            PROTOCOLS.put(PROTOCOLS_KEYS[i], PROTOCOLS_VALUE[i]);
        }
    }

    private void setRecyclerView() {
        policies = DBClient.getInstance().getAllPolicies();
        if (policies.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            textViewNoPoliciesDescription.setVisibility(View.GONE);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//            mAdapter = new PoliciesAdapter(getActivity(), policies);
//            mAdapter.setClickListener(policyClickListener);
//            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.setHasFixedSize(true);
        }
    }

    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    interface PolicyClickListener {
        void onClick(DmoatPolicy policy);
    }
}
