package hardware.prytex.io.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.activity.BaseActivity;
import hardware.prytex.io.activity.VideoViewActivity;
import hardware.prytex.io.config.Constants;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;

/**
 * Created by khawarraza on 11/11/2016.
 */

public class AboutFragment extends BaseActivity implements  View.OnClickListener{

    public static  boolean isFromAboutScreen  = false;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private TextView tvOtaVersions;
    public static String otaCurrentVersion = "---";
    public static String otaInstalledVersion = "---";
    private Intent intent;

    @SuppressLint("CommitPrefEdits")
    @Override
    public void initViews( Bundle savedInstanceState) {
        super.initViews( savedInstanceState);
        Constants.activityContext = this;
        pref = getSharedPreferences("MyPref", 0);
        editor = pref.edit();
        TextView tvTosAbout = findViewById(R.id.tvTOSAbout);
        TextView tvPpAbout = findViewById(R.id.tvPPAbout);
        TextView tvEulaAbout = findViewById(R.id.tvEULAAbout);
        tvOtaVersions = findViewById(R.id.product_ota_name);
        TextView tvDmoatVersion = findViewById(R.id.tv_DmoatVersion);

        TextView tvOpenSrcPakgs = findViewById(R.id.tvOpenSrcPkgs);

        imgInfoBtn.setVisibility(View.GONE);

        try {
            String versionName;
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            tvDmoatVersion.setText(versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        intent = new Intent(this, VideoViewActivity.class);
        Api.getOtaVersion(this, getParams(), mListener);

        tvTosAbout.setOnClickListener(this);
        tvPpAbout.setOnClickListener(this);
        tvEulaAbout.setOnClickListener(this);
        tvOpenSrcPakgs.setOnClickListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
//        mFirebaseAnalytics.setCurrentScreen(this, "about", null /* class override */);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, "about");
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, this.getClass().getName());
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_about;
    }

//    @Override
//    public String getTitle() {
//        return "About";
//    }

    private JSONObject getParams() {
        JSONObject js = new JSONObject();
        try {
            js.put("app_id", pref.getString("app_id", ""));
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    // listenerB
    private final AsyncTaskListener mListener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            Log.d("OtaApiResponse", "response");

            if (result.code == 200 || result.code == 2) {
                try{
                    Log.d("OtaApiResponse", "success");
                    tvOtaVersions.setText(getString(R.string.installed)+ otaInstalledVersion + " / Latest : "+otaCurrentVersion);

                }catch (Exception e){
                    Log.d("OtaApiExcepti9on2", String.valueOf(e));
                }
            } else if (result.code == 503){
                showAlertMsgDialog(AboutFragment.this, result.message);
//                MyToast.showMessage(getActivity(), getResources().getString(R.string.error_message_generic));
            }
        }
    };

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.tvTOSAbout){
            isFromAboutScreen = true;
                editor.putString("file_clicked", "tos");
                editor.commit();

                showLegaldoc();
        }else if(v.getId() == R.id.tvPPAbout){
            isFromAboutScreen = true;
                editor.putString("file_clicked", "pp");
                editor.commit();
                showLegaldoc();

        }else if(v.getId()==R.id.tvEULAAbout){
            isFromAboutScreen = true;
            editor.putString("file_clicked", "eula");
            editor.commit();

            showLegaldoc();
        }else if(v.getId() == R.id.tvOpenSrcPkgs){
            intent = new Intent(this, OpenSrcPackagesFragment.class);
                startActivity(intent);

        }
    }


    private void showLegaldoc() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(Api.dmoatEULADocURL));
        intent.setPackage("com.android.chrome");
        startActivity(intent);
    }
    
    @Override
    public void onBackPressed()
    {
        backButoonPressed();

    }
}
