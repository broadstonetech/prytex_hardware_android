package hardware.prytex.io.fragment.SensorTabBars;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.chart_config.XaxisTempTimeScale;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.GeneralUtils;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import hardware.prytex.io.fragment.ProfileFragment;

import static hardware.prytex.io.adapter.ConnectedHostsAdapter.getrTimeStamp;
import static hardware.prytex.io.parser.SensorDataParser.listOfSensorData;

//fragment to show 24 hour sensor data
@SuppressWarnings("ALL")
public class SensorDayDataFragment extends BaseFragment implements IAxisValueFormatter {

    private ProgressDialog dialog;
    private LineChart mChart;
    private LineChart mChart_humi;
    private LineChart mChart_air;
    private TextView avgTempValue;
    private TextView avgHumidityValue;
    private TextView avgAirValue;
    private TextView tvMaxHumi;
    private TextView tvMinHumi;
    private TextView tvMaxTemp;
    private TextView tvMinTemp;
    private TextView tvMaxAir;
    private TextView tvMinAir;
    private TextView tvAirQualityHeading;
    private int valAvgAir;
    private float valAvgTemp;
    private float valAvgHumidity;
    private LinearLayout llMainChartView, llSyncDateTime;
    private TextView tvLastSync, tvNextSync;
    private SharedPreferences pref;

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);

//        mSwipeRefreshLayout = (SwipeRefreshLayout) parent.findViewById(R.id.sensor_current_swipe_refresh_layout);
        llMainChartView = parent.findViewById(R.id.llMainChartsView);
        mChart = parent.findViewById(R.id.chart);
        mChart_humi = parent.findViewById(R.id.humidity_chart);
        mChart_air = parent.findViewById(R.id.air_chart);


        mChart.setNoDataText("No temperature values detected.");
        mChart_air.setNoDataText("No air quality values detected.");
        mChart_humi.setNoDataText("No humidity values detected.");

        llSyncDateTime = parent.findViewById(R.id.llSyncDateTime);
        tvLastSync = parent.findViewById(R.id.tvLastSyncd);
        tvNextSync = parent.findViewById(R.id.tvNextSync);

        avgTempValue = parent.findViewById(R.id.value);
        avgHumidityValue = parent.findViewById(R.id.humidity_value);
        avgAirValue = parent.findViewById(R.id.air_value);

        tvMaxHumi = parent.findViewById(R.id.tvMaxHumi);
        tvMinHumi = parent.findViewById(R.id.tvMinHumi);
        tvMaxTemp = parent.findViewById(R.id.tvMaxTemp);
        tvMinTemp = parent.findViewById(R.id.tvMinTemp);
        tvMaxAir = parent.findViewById(R.id.tvMaxAir);
        tvMinAir = parent.findViewById(R.id.tvMinAir);
        tvAirQualityHeading = parent.findViewById(R.id.air_title);

        if (GeneralUtils.isConnected(getContext())) {
//            showProgressDialog(true);
            Api.getSensorData(getActivity(), getSensorDataParams(), listener);
        } else {
//            showAlertMsgDialog(getContext(), getResources().getString(R.string.internet_connection));
        }

//        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                refreshContent();
//            }
//        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
//        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "sensor_data_24hour", null /* class override */);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, "sensor_data_24hour");
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, this.getClass().getName());

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);
    }

    @Override
    public String getTitle() {
        return "Sensor Data";
    }


    @Override
    public int getLayoutId() {
        return R.layout.fragment_sensor;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return null;
    }

    private JSONObject getSensorDataParams() {
        JSONObject js = new JSONObject();
        try {
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("app_id", pref.getString("app_id", ""));
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    private final AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            showProgressDialog();
            if (result.code == 200) {
//            MyToast.showMessage(getContext(), String.valueOf(result.code));
                if (listOfSensorData.size() > 0) {
                    llSyncDateTime.setVisibility(View.VISIBLE);
                    int eveSec = listOfSensorData.get(0).getEve_sec();
                    String curTime = "00:00";
                    try {
                        curTime = getrTimeStamp(eveSec);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    tvLastSync.setText(curTime);
                    String myTime = "14:10";
                    SimpleDateFormat df = new SimpleDateFormat("MMM-dd-yyyy h:mm a", Locale.getDefault());
                    Date d = null;
                    try {
                        d = df.parse(curTime);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(d);
                    cal.add(Calendar.MINUTE, 15);
                    String newTime = df.format(cal.getTime());
                    tvNextSync.setText(newTime);
                    llMainChartView.setVisibility(View.VISIBLE);
                    decoreateChartView(mChart, mChart_humi, mChart_air);
                } else {
//                    MyToast.showMessage(getContext(), "No data detected");
                }
            } else {
//                MyToast.showMessage(getContext(), result.message);
            }

        }
    };


    private void decoreateChartView(LineChart mChart, LineChart mChart_humi, LineChart mChart_air) {
        // enable description text
        mChart.getDescription().setEnabled(false);
        mChart_humi.getDescription().setEnabled(false);
        mChart_air.getDescription().setEnabled(false);
        mChart.setDescription(null);

        List<String> xVals = new ArrayList<>();
//        List<BarEntry> yVals = new ArrayList();
        for (int i = 0; i < listOfSensorData.size(); i++) {
            int hName = listOfSensorData.get(i).getEve_sec();

            float fTime = Float.parseFloat(String.valueOf(hName));
            long time = (long) fTime;
            time = time * 1000;
            Date date = new Date(time);

            SimpleDateFormat sourceFormat = new SimpleDateFormat("MMM-dd-yyyy HH:mm:ss", Locale.getDefault());
//        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            String dateString = sourceFormat.format(date); // => Date is in UTC now
            Date parsed = null; // => Date is in UTC now
            try {
                parsed = sourceFormat.parse(dateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            SimpleDateFormat destFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
//        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
            destFormat.setTimeZone(Calendar.getInstance().getTimeZone());
//        destFormat.setTimeZone(TimeZone.getDefault());
            String bar = destFormat.format(parsed);
            xVals.add(bar);
        }


        XaxisTempTimeScale custom = new XaxisTempTimeScale();

        XAxis xl = mChart.getXAxis();
        XAxis xlHumidity = mChart_humi.getXAxis();
        XAxis xlAir = mChart_air.getXAxis();

        xl.setAvoidFirstLastClipping(true);
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        xl.setValueFormatter(new IndexAxisValueFormatter(xVals));
        xl.setDrawGridLines(false);

        xlHumidity.setAvoidFirstLastClipping(true);
        xlHumidity.setPosition(XAxis.XAxisPosition.BOTTOM);
        xlHumidity.setValueFormatter(new IndexAxisValueFormatter(xVals));
        xlHumidity.setDrawGridLines(false);

        xlAir.setAvoidFirstLastClipping(true);
        xlAir.setPosition(XAxis.XAxisPosition.BOTTOM);
        xlAir.setValueFormatter(new IndexAxisValueFormatter(xVals));
        xlAir.setDrawGridLines(false);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);
//        leftAxis.setSpaceMin(20);
//        leftAxis.setSpaceMax(40);

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);

        YAxis leftAxisHumidity = mChart_humi.getAxisLeft();
        leftAxisHumidity.setDrawGridLines(false);

        YAxis rightAxisHumidity = mChart_humi.getAxisRight();
        rightAxisHumidity.setEnabled(false);

        YAxis leftAxisAir = mChart_air.getAxisLeft();
        leftAxisAir.setDrawGridLines(false);

        YAxis rightAxisAir = mChart_air.getAxisRight();
        rightAxisAir.setEnabled(false);

//        mChart.setData(new LineData());
        setData();
        Legend l = mChart.getLegend();
        Legend l_humidity = mChart_humi.getLegend();
        Legend l_air = mChart_air.getLegend();
        // modify the legend ...
        l.setForm(Legend.LegendForm.NONE);
        l_humidity.setForm(Legend.LegendForm.NONE);
        l_air.setForm(Legend.LegendForm.NONE);
        // dont forget to refresh the drawing
//        mChart.setVisibleYRange(0,40, true);
//        mChart.getAxisLeft().setAxisMaxValue(40f);
//        mChart.getAxisLeft().setAxisMinValue(0f);
        Log.d("MinMAx==", "Ymin==" + mChart.getYMin() + "Ymax==" + mChart.getYMax() + "Xmax==" + mChart.getYMax() + "Xmax==" + mChart.getYMax());

        tvMaxHumi.setText(getActivity().getResources().getString(R.string.max)+ mChart_humi.getYMax() + "");
        tvMinHumi.setText(getActivity().getResources().getString(R.string.min)+ mChart_humi.getYMin() + "");
        tvMaxTemp.setText(getActivity().getResources().getString(R.string.max)+ mChart.getYMax() + "");
        tvMinTemp.setText(getActivity().getResources().getString(R.string.min)+ mChart.getYMin() + "");

        NumberFormat nf = NumberFormat.getInstance();

        tvMaxAir.setText(getActivity().getResources().getString(R.string.max)+ nf.format(mChart_air.getYMax()) + "");
        tvMinAir.setText(getActivity().getResources().getString(R.string.min)+ nf.format(mChart_air.getYMin()) + "");


        mChart.getAxisLeft().setDrawLabels(false);
        mChart.getAxisRight().setDrawLabels(false);
        mChart.getXAxis().setDrawLabels(false);

        mChart_humi.getAxisLeft().setDrawLabels(false);
        mChart_humi.getAxisRight().setDrawLabels(false);
        mChart_humi.getXAxis().setDrawLabels(false);

        mChart_air.getAxisLeft().setDrawLabels(false);
        mChart_air.getAxisRight().setDrawLabels(false);
        mChart_air.getXAxis().setDrawLabels(false);

        mChart.getLegend().setEnabled(false);
        mChart_humi.getLegend().setEnabled(false);
        mChart_air.getLegend().setEnabled(false);

        mChart.invalidate();
        mChart_humi.invalidate();
        mChart_air.invalidate();
    }

    private void setData() {

        ArrayList<Entry> values = new ArrayList<>();
        ArrayList<Entry> values_humi = new ArrayList<>();
        ArrayList<Entry> values_air = new ArrayList<>();

        for (int i = 0; i < listOfSensorData.size(); i++) {

//            float val = (float) (Math.random() * range) + 3;

            values.add(new Entry(i, listOfSensorData.get(i).getTemperature(), getResources().getDrawable(R.drawable.alert)));
            values_humi.add(new Entry(i, listOfSensorData.get(i).getHumidity(), getResources().getDrawable(R.drawable.alert)));
            values_air.add(new Entry(i, listOfSensorData.get(i).getCoppm(), getResources().getDrawable(R.drawable.alert)));

            valAvgTemp = valAvgTemp + listOfSensorData.get(i).getTemperature();
            valAvgHumidity = valAvgHumidity + listOfSensorData.get(i).getHumidity();
            valAvgAir = valAvgAir + listOfSensorData.get(i).getCoppm();
        }

        float avgTemp = valAvgTemp / listOfSensorData.size();
        float avgHumidity = valAvgHumidity / listOfSensorData.size();
        int avgAir = valAvgAir / listOfSensorData.size();
        float fahrenhietCelciusTemp = ((avgTemp * 9) / 5 + 32);
        String convertedCelciusTemp = String.format("%.1f", avgTemp);
        String convertedFahrnhtTemp = String.format("%.1f", fahrenhietCelciusTemp);
        avgTempValue.setText(convertedCelciusTemp + "°C" + "\n" + convertedFahrnhtTemp + "°F");
        String convertedHumi = String.format("%.1f", avgHumidity);
        avgHumidityValue.setText(convertedHumi + "%");
        avgAirValue.setText(avgAir + "\nCO2PPM");

        if (avgAir >= 1000) {
            tvAirQualityHeading.setText(getActivity().getResources().getString(R.string.hazardous));
        } else if (avgAir >= 600 && avgAir <= 999) {
            tvAirQualityHeading.setText(getActivity().getResources().getString(R.string.bad));
        } else if (avgAir >= 450 && avgAir <= 599) {
            tvAirQualityHeading.setText(getActivity().getResources().getString(R.string.fair));
        } else if (avgAir < 450) {
            tvAirQualityHeading.setText(getActivity().getResources().getString(R.string.good));
        } else {
            tvAirQualityHeading.setText("");
        }

        LineDataSet setTemp, setHumi, setAir;

        if ((mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) || (mChart_humi.getData() != null &&

                mChart_humi.getData().getDataSetCount() > 0) || (mChart_air.getData() != null &&

                mChart_air.getData().getDataSetCount() > 0)) {
            setTemp = (LineDataSet) mChart.getData().getDataSetByIndex(0);
            setTemp.setValues(values);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();

            setHumi = (LineDataSet) mChart_humi.getData().getDataSetByIndex(0);
            setHumi.setValues(values_humi);
            mChart_humi.getData().notifyDataChanged();
            mChart_humi.notifyDataSetChanged();

            setAir = (LineDataSet) mChart_air.getData().getDataSetByIndex(0);
            setAir.setValues(values_air);
            mChart_air.getData().notifyDataChanged();
            mChart_air.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            setTemp = new LineDataSet(values, "");
            // set the line to be drawn like this "- - - - - -"
//            set1.enableDashedLine(10f, 5f, 0f);
            setTemp.enableDashedHighlightLine(10f, 5f, 0f);
            setTemp.setColor(Color.BLACK);
//            set1.setCircleColor(Color.BLACK);
            setTemp.setLineWidth(0f);
            setTemp.setCircleRadius(0f);
            setTemp.setDrawCircleHole(false);
            setTemp.setValueTextSize(0f);
            setTemp.setDrawFilled(true);
            setTemp.setFormLineWidth(1f);
            setTemp.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setTemp.setFormSize(0.f);
            setTemp.setFillColor(getResources().getColor(R.color.torrent));

            setTemp.isHighlightEnabled();
            setTemp.setHighlightEnabled(false);
            setHumi = new LineDataSet(values_humi, "");
            setAir = new LineDataSet(values_air, "");

            setTemp.setDrawIcons(false);
            setTemp.setDrawCircles(false);
//      set humi
            setHumi.setDrawIcons(false);
            setHumi.setDrawCircles(false);
            // set the line to be drawn like this "- - - - - -"
//            set3.enableDashedLine(10f, 5f, 0f);
            setHumi.enableDashedHighlightLine(10f, 5f, 0f);
            setHumi.setColor(Color.BLACK);
//            set3.setCircleColor(Color.BLACK);
            setHumi.setLineWidth(0f);
            setHumi.setCircleRadius(0f);
            setHumi.setDrawCircleHole(false);
            setHumi.setValueTextSize(0f);
            setHumi.setDrawFilled(true);
            setHumi.setFormLineWidth(1f);
            setHumi.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setHumi.setFormSize(00.f);
            setTemp.setFillColor(getResources().getColor(R.color.torrent));

//set air
            setAir.setDrawIcons(false);
            setAir.setDrawCircles(false);
            // set the line to be drawn like this "- - - - - -"
//            set3.enableDashedLine(10f, 5f, 0f);
            setAir.enableDashedHighlightLine(10f, 5f, 0f);
            setAir.setColor(Color.BLACK);
//            set3.setCircleColor(Color.BLACK);
            setAir.setLineWidth(0f);
            setAir.setCircleRadius(0f);
            setAir.setDrawCircleHole(false);
            setAir.setValueTextSize(0f);
            setAir.setDrawFilled(true);
            setAir.setFormLineWidth(1f);
            setAir.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setAir.setFormSize(0.f);
            setTemp.setFillColor(getResources().getColor(R.color.torrent));


            if (Utils.getSDKInt() >= 18) {
                // fill drawable only supported on api level 18 and above
//                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.intercom_empty_state_fade);

            } else {
//                set1.setFillColor(Color.CYAN);
//                set2.setFillColor(Color.BLACK);
//                set3.setFillColor(Color.BLACK);
            }

            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(setTemp); // add the datasets
            ArrayList<ILineDataSet> dataSets_humidity = new ArrayList<>();
            dataSets_humidity.add(setHumi);
            ArrayList<ILineDataSet> dataSets_air = new ArrayList<>();
            dataSets_air.add(setAir);

            // create a data object with the datasets
            LineData data = new LineData(dataSets);
            LineData data_humidity = new LineData(dataSets_humidity);
            LineData data_air = new LineData(dataSets_air);

            // set data
            mChart.setData(data);
//            mChart_humidity.setData(data);
            mChart_humi.setData(data_humidity);
            mChart_air.setData(data_air);
        }
    }


    private void showProgressDialog() {
        if (false) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private String appendUser() {
        return ProfileFragment.TEMPERATURE_SCALE + "_" + UserManager.getInstance().getLoggedInUser().userName;
    }

}
