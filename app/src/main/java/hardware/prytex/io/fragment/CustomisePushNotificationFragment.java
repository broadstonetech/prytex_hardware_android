package hardware.prytex.io.fragment;

import android.os.Bundle;
import android.view.View;

import hardware.prytex.io.R;

public class CustomisePushNotificationFragment extends BaseFragment {
    @Override
    public int getLayoutId() {
        return R.layout.activity_customise_push_notification_fragment;
    }

    @Override
    public String getTitle() {
        return "Push Notifications";
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);
    }
}
