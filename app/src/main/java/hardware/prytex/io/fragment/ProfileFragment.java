package hardware.prytex.io.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.UiModeManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.activity.AuthenticationActivity;
import hardware.prytex.io.activity.BandwidthChartMainActivity;
import hardware.prytex.io.activity.DashBoardActivity;
import hardware.prytex.io.config.Constants;
import hardware.prytex.io.fragment.themes.ThemeFragment;
import hardware.prytex.io.model.PushNotificationsClass;
import hardware.prytex.io.model.User;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.GeneralUtils;
import hardware.prytex.io.util.MyToast;
import hardware.prytex.io.util.SPManager;
import hardware.prytex.io.util.UserPrefs;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static hardware.prytex.io.FCM.MyFCMListenerService.removeNotificaitonFromStatusBar;
import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.activity.DashBoardActivity.llBottomTabs;
import static hardware.prytex.io.activity.DashBoardActivity.setBottomNavTextColors;
import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;
import static hardware.prytex.io.fragment.DashBoardFragment.clearDashboardData;
import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;
import static hardware.prytex.io.parser.PushNotificationsParser.objNoti;
import static io.realm.internal.SyncObjectServerFacade.getApplicationContext;

/**
 * Created by abubaker on 9/29/16.
 */

@SuppressWarnings("ALL")
public class ProfileFragment extends BaseFragment implements View.OnClickListener {

    public static final int MODE_P_AND_P = 0;
    public static final int MODE_ADVANCE = 1;
    public static final String LED = "led";
    public static final int LED_OFF = 2;
    public static final int LED_ON_5_SECONDS = 0;
    public static final int LED_ON_10_SECONDS = 1;
    public static final String TEMPERATURE_SCALE = "temperature_scale";
    public static final String[] temperatureScales = {"Celcius", "Fahrenheit"};
    private static final String MODE = "mode";
    private static final String[] LED_MODES = {"5 seconds", "10 seconds", "Turn Led's off"};
    private static final String[] modes = {"Plug & Play", "Advanced"};
    public static String ledFrequency = "";
    public static String ledMode = "";
    private static boolean isPmodeOK = false;
    private static boolean isAdvanceModeOK = false;
    RelativeLayout rl_themes, rl_actionbar;
    private ProgressDialog dialog;
    private final AsyncTaskListener resetSettingsListener = result -> {
        showProgressDialog(false);
        if (!isAdded()) {
            return;
        }
        String stringToSearch = result.message;
        Pattern p, pInProgress;
        Matcher m, mInProgress;
        p = Pattern.compile("Please request again after");   // the pattern to search for request after after specified time
        pInProgress = Pattern.compile("Request is already in process");   // the pattern to search for request after after specified time
        m = p.matcher(stringToSearch);
        mInProgress = pInProgress.matcher(stringToSearch);

        showProgressDialog(false);
        Log.d("ResponseCode", result.code + "");
        if (result.code == 400 && m.find()) {
            MyToast.showMessage(getActivity(), result.message);
//                VerifyCodeFragment verifyCodeFragment = new VerifyCodeFragment();
//                Bundle userNameBunle = new Bundle();
//                userNameBunle.putBoolean("forgot_password", true);
//                userNameBunle.putString("username", edittextUsername.getText().toString());
//                verifyCodeFragment.setArguments(userNameBunle);
//                getHelper().onBack();
//                getHelper().addFragment(verifyCodeFragment, false, true);
            /*if (result.code == 400)
                MyToast.showMessage(getActivity(), "Invalid Username!");
            else if (result.code == 503)
                MyToast.showMessage(getActivity(), "Unable to proceed! Please try again");
            else*/
        }
//        else if (result.code == 200 && mInProgress.find()) {
////                MyToast.showMessage(getActivity(), getResources().getString(R.string.forgotUsernamePwdSuccess));
////            VerifyCodeFragment verifyCodeFragment = new VerifyCodeFragment();
////            Bundle userNameBunle = new Bundle();
////            userNameBunle.putBoolean("forgot_password", false);
////            userNameBunle.putString("desription", getContext().getResources().getString(R.string.verify_code_already_sent_description));
////            verifyCodeFragment.setArguments(userNameBunle);
////            getHelper().onBack();
////            getHelper().addFragment(verifyCodeFragment, false, true);
//            /*if (result.code == 400)
//                MyToast.showMessage(getActivity(), "Invalid Username!");
//            else if (result.code == 503)
//                MyToast.showMessage(getActivity(), "Unable to proceed! Please try again");
//            else*/
//        }
        else if (result.code == 200) {
//            MyToast.showMessage(getActivity(), getContext().getResources().getString(R.string.forgot_username_pwd_success));
//            Log.d("Reset Settings", "Code sent successfully");
//            Bundle resetSettingsBundle = new Bundle();
//            resetSettingsBundle.putBoolean("forgot_password", false);
//            resetSettingsBundle.putString("desription", getContext().getResources().getString(R.string.verify_code_description_a) + getContext().getResources().getString(R.string.verify_code_description_a));
//            VerifyCodeFragment mVerifyCodeFragment = new VerifyCodeFragment();
//            mVerifyCodeFragment.setArguments(resetSettingsBundle);
//            getHelper().addFragment(mVerifyCodeFragment, false, true);
            if (TextUtils.isEmpty(result.message)) {
                showAlertMsgDialog(getActivity(), Constants.DMOAT_RESET_SUCCESS);
            } else {
                showAlertMsgDialog(getActivity(), result.message);
            }
        } else {
            showAlertMsgDialog(getActivity(), result.message);
        }
    };
    private final AsyncTaskListener mGetPushNotifications = new AsyncTaskListener() {

        @Override
        public void onComplete(TaskResult result) {
            {
                showProgressDialog(false);
                if (result.code == 200 || result.code == 2) {
                    if (objNoti.getSensors() == 1) {
                        switchSensors.setChecked(true);
                        switchSensors.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.colorPrimary)));
                    }
                    if (objNoti.getConnectedHost() == 1) {
                        switchConnectedDevices.setChecked(true);
                        switchConnectedDevices.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.colorPrimary)));
                    }
                    if (objNoti.getDevices() == 1) {
                        switchDevices.setChecked(true);
                        switchDevices.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.colorPrimary)));
                    }
                    if (objNoti.getBlocked() == 1) {
                        switchBlockedAletrts.setChecked(true);
                        switchBlockedAletrts.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.colorPrimary)));
                    }
                    if (objNoti.getInfo() == 1) {
                        switchDmoatInfo.setChecked(true);
                        switchDmoatInfo.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.colorPrimary)));
                    }
                    if (!objNoti.getCheck())
                        showAlertMsgDialog(getActivity(), "Your response has been submitted");
                } else if (result.code == 400) {
                    if (result.message.equalsIgnoreCase("no data found")) {
                        switchConnectedDevices.setChecked(true);
                        switchBlockedAletrts.setChecked(true);
                        switchDmoatInfo.setChecked(true);
                        switchDevices.setChecked(true);
                        switchSensors.setChecked(true);
                    }
                }
            }
        }
    };
    private final AsyncTaskListener mRebootListner = result -> {
        {
            showProgressDialog(false);
            if (result.code == 200 || result.code == 2) {
                showAlertMsgDialog(getActivity(), Constants.DMOAT_REBOOT_SUCCESS);
            } else {
                showAlertMsgDialog(getActivity(), Constants.DMOAT_REBOOT_ERROR);
            }
        }
    };
    private final AsyncTaskListener mClearBaselineListener = result -> {
        if (!isAdded()) {
            return;
        }
        showProgressDialog(false);
        if (result.code == 200 || result.code == 2) {
            Log.d("BaseLine", "baseline cleared");
            //     showAlertMsgDialog(getContext(), "Baseline Cleared.");
        } else {
//                if (result.message.equalsIgnoreCase("")) {
//                    showAlertMsgDialog(getContext(), getResources().getString(R.string.error_message_generic));
//                } else {
//                    showAlertMsgDialog(getContext(), result.message);
//                }
        }
    };
    private final AsyncTaskListener mChangeModeListener = result -> {
        showProgressDialog(false);
        if (result.code == 200 || result.code == 2) {
            getUserSettingsSharedPreferences().edit().putInt(DashBoardFragment.MODE + "_" + UserManager
                    .getInstance().getLoggedInUser().userName, DashBoardFragment
                    .MODE_P_AND_P).apply();

            MyToast.showMessage(getContext(), "d.moat is in Plug & Play mode");
            Log.d("MODE", "Mode changed successfully");
        } else {
//                MyToast.showMessage(getActivity(), "Failed. Please try later!");
        }
    };
    private SwitchCompat switchThemes, switchConnectedDevices, switchBlockedAletrts, switchDmoatInfo, switchDevices, switchSensors;
    private TextView selectedTemperatureLabel;
    private TextView selectedModeLabel;
    private TextView selectedLedLabel;
    private Boolean boolRlPushNoti = false;
    private int conctedDevice = 0;
    private int blockedAlerts = 0;
    private int dmoatInfo = 0;
    private int sensors = 0;
    private int devices = 0;
    private LinearLayout llStartStopPushNoti;
    private int selectedLEDMode = LED_ON_5_SECONDS;
    private final AsyncTaskListener mChangeLEDModeListener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (!isAdded()) {
                return;
            }
            showProgressDialog(false);
            if (result.code == 200 || result.code == 2) {
                Log.d("LED", "LED mode changed successfully " + selectedLEDMode);
                selectedLedLabel.setText(LED_MODES[selectedLEDMode]);
            } else {
                showAlertMsgDialog(getActivity(), getResources().getString(R.string.error_message_generic));
            }
        }
    };
    private final AsyncTaskListener mGetLEDModeListener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (ledMode.equalsIgnoreCase("OFF")) {
                selectedLEDMode = 2;
                selectedLedLabel.setText(LED_MODES[selectedLEDMode]);
            } else if (ledMode.equalsIgnoreCase("ON") && ledFrequency.equalsIgnoreCase("5")) {
                selectedLEDMode = 0;
                selectedLedLabel.setText(LED_MODES[selectedLEDMode]);
            } else {
                selectedLEDMode = 1;
                selectedLedLabel.setText(LED_MODES[selectedLEDMode]);
            }

        }
    };
    private SharedPreferences pref;
    private final AsyncTaskListener signOutListener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            showProgressDialog(false);
            if (result.code == 200 || result.code == 2) {
                UserManager.getInstance().logout();
                getPrefs().edit().clear().commit();
                DashBoardActivity.BASE_URL = "";
                disableFCM();
                startActivity(new Intent(getApplicationContext(), AuthenticationActivity.class));
                getActivity().finish();

            } else
                MyToast.showMessage(getActivity(), result.message);
        }
    };
    private SharedPreferences.Editor editor;
    private String modeFetched = "";
    //getModeConfig listener
    private final AsyncTaskListener mGetModeConfigListener = result -> {
        if (!isAdded()) {
            return;
        }
        showProgressDialog(false);
        if (result.code == 200 || result.code == 2) {
            Log.d("Mode received", "get config mode");
            if (result.NetConfigType.equalsIgnoreCase("P&P") || result.NetConfigType.equalsIgnoreCase("Plug & Play")) {
                Log.d("Mode received", "p&P");
                getUserSettingsSharedPreferences().edit().putInt(DashBoardFragment.MODE + "_" + UserManager
                        .getInstance().getLoggedInUser().userName, DashBoardFragment
                        .MODE_P_AND_P).apply();
                modeFetched = "P&P";
                showModeSelectionDialog("P&P");
                showProgressDialog(false);

            } else {
                Log.d("Mode received", "advance");
                getUserSettingsSharedPreferences().edit().putInt(DashBoardFragment.MODE + "_" + UserManager
                        .getInstance().getLoggedInUser().userName, DashBoardFragment
                        .MODE_ADVANCE).apply();
                modeFetched = "Advance";
                showModeSelectionDialog("Advance");
            }
        } else {
            if (result.message != null) {
                MyToast.showMessage(getActivity(), result.message);
            } else
                MyToast.showMessage(getActivity(), getResources().getString(R.string.error_message_generic));
        }
    };

    @Override
    public String getTitle() {
        return "Profile & Setting";
    }

    @Override
    public int getLayoutId() {
        return R.layout.profile_fragment;
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();

        llBottomTabs.setVisibility(View.VISIBLE);
        imgInfoBtn.setVisibility(View.VISIBLE);
        llStartStopPushNoti = parent.findViewById(R.id.llStartStopPushNoti);

        switchConnectedDevices = parent.findViewById(R.id.switch_connected_device);
        switchBlockedAletrts = parent.findViewById(R.id.switch_blockedAlertsNoti);
        switchDmoatInfo = parent.findViewById(R.id.switch_dmoatInfo);
        switchDevices = parent.findViewById(R.id.switch_devices);
        switchSensors = parent.findViewById(R.id.switch_sensorsNoti);
        switchThemes = parent.findViewById(R.id.switch_night_theme);


        rl_actionbar = parent.findViewById(R.id.rl_actionbar);
        rl_actionbar.setVisibility(View.VISIBLE);

        Button btnSetPushNotification = parent.findViewById(R.id.btnSetPushNotification);

        switchButtonListner();
        ((DashBoardActivity) getActivity()).changeMenuBar();
        setBottomNavTextColors(getContext(), false, false, true, false);
        parent.findViewById(R.id.change_password).setOnClickListener(v -> {

            llStartStopPushNoti.setVisibility(View.GONE);
            ChangePasswordFragment.isFromProfile = true;
            DashBoardActivity.llBottomTabs.setVisibility(View.GONE);
            getHelper().addFragmentwitoutTabbar(new ChangePasswordFragment(), false, true);

        });

        parent.findViewById(R.id.invite_users).setOnClickListener(v -> {

            llStartStopPushNoti.setVisibility(View.GONE);
            if (UserManager.checkPauseInternetTimeStatus()) {
                if (isDmoatOnline) {
                    getHelper().addFragment(new InvitePeopleFragment(), false, true);
                } else {
                    showAlertMsgDialog(getContext(), getResources().getString(R.string.dmoat_offline));
                }
            } else {
                showAlertMsgDialog(getContext(), getResources().getString(R.string.paused_internet_msg));
            }
        });

        parent.findViewById(R.id.temperature_scale).setOnClickListener(v -> {

            llStartStopPushNoti.setVisibility(View.GONE);
            if (isDmoatOnline) {
                showTemperatureScaleSelectionDialog();
            } else {
                showAlertMsgDialog(getContext(), getResources().getString(R.string.dmoat_offline));
            }
        });

        //changes by umer, start
        //This is the weirdest way to replace fragment

        parent.findViewById(R.id.rl_themes).setOnClickListener(v -> {
            DashBoardActivity.llBottomTabs.setVisibility(View.GONE);
            rl_actionbar.setVisibility(View.GONE);
            getHelper().addFragmentwitoutTabbar(new ThemeFragment(), false, true);
            //ReplaceFragment.replace(getActivity(), new ThemeFragment(),R.id.fragment_fullscreen);
        });

        //changes by umer, end

        parent.findViewById(R.id.led).setOnClickListener(v -> {

            llStartStopPushNoti.setVisibility(View.GONE);
            if (isDmoatOnline) {
                showLEDModeSelectionDialog();
            } else {
                showAlertMsgDialog(getContext(), getResources().getString(R.string.dmoat_offline));
            }
        });

        parent.findViewById(R.id.changeMode).setOnClickListener(v -> {

            //llStartStopPushNoti.setVisibility(View.GONE);
            if (isDmoatOnline) {
                if (UserManager.checkPauseInternetTimeStatus()) {
                    showProgressDialog(true);
                    Api.getModeType(getActivity(), getModeConfigParams(), mGetModeConfigListener);
                } else {
                    MyToast.showMessage(getContext(), getResources().getString(R.string.paused_internet_msg));
                }
            } else {
                showAlertMsgDialog(getContext(), getResources().getString(R.string.dmoat_offline));
            }
        });

        parent.findViewById(R.id.rl_pushnotifications).setOnClickListener(v -> {
//                getHelper().addFragment(new CustomisePushNotificationFragment(), false, true);

            if (!boolRlPushNoti) {
                llStartStopPushNoti.setVisibility(View.GONE);
                boolRlPushNoti = true;
            } else {
                llStartStopPushNoti.setVisibility(View.VISIBLE);
                boolRlPushNoti = false;
            }
        });

        parent.findViewById(R.id.rl_restToFactory).setOnClickListener(v -> {
            llStartStopPushNoti.setVisibility(View.GONE);
            new AlertDialog.Builder(getActivity())
                    .setTitle("d.moat")
                    .setMessage("Are you sure you want to reset d.moat to its default settings?")
                    .setPositiveButton("Reset", (dialog, whichButton) -> {

                        if (isDmoatOnline) {
                            if (UserManager.checkPauseInternetTimeStatus()) {
                                //   callClearbaselineApi();
                                callResetDmoatSettingsApi();

                            } else {
                                showAlertMsgDialog(getContext(), getResources().getString(R.string.paused_internet_msg));
                            }
                        } else {
                            showAlertMsgDialog(getContext(), getResources().getString(R.string.dmoat_offline));
                        }
                    })
                    .setNegativeButton("cancel", null).show();

        });
        parent.findViewById(R.id.rl_signOut).setOnClickListener(v -> {
            //llStartStopPushNoti.setVisibility(View.GONE);

            new AlertDialog.Builder(getActivity())
                    .setTitle("Signing Out")
                    .setMessage("Are you sure you want to sign out?")
                    .setPositiveButton("sign out", (dialog, whichButton) -> {
                        isDmoatOnline = true;
                        if (GeneralUtils.isConnected(getActivity())) {
                            logout();
                            //getActivity().finish();
                        }
                    })
                    .setNegativeButton("cancel", null).show();

        });

        parent.findViewById(R.id.rl_about).setOnClickListener(v -> {
            //llStartStopPushNoti.setVisibility(View.GONE);
            //   ((DashBoardActivity) getActivity()).replaceFragments();
            Intent myIntent = new Intent(getActivity(), AboutFragment.class);
            getActivity().startActivity(myIntent);
            getActivity().finish();
        });
        parent.findViewById(R.id.clear_baseline).setOnClickListener(v -> {

            //llStartStopPushNoti.setVisibility(View.GONE);
            if (isDmoatOnline) {
                if (UserManager.checkPauseInternetTimeStatus()) {
                    callClearbaselineApi();
                } else {
                    showAlertMsgDialog(getContext(), getResources().getString(R.string.paused_internet_msg));
                }
            } else {
                showAlertMsgDialog(getContext(), getResources().getString(R.string.dmoat_offline));
            }
        });

        parent.findViewById(R.id.layout_reset_device).setOnClickListener(v -> {

            llStartStopPushNoti.setVisibility(View.GONE);
            if (isDmoatOnline) {
                if (UserManager.checkPauseInternetTimeStatus()) {
                    if (GeneralUtils.isConnected(getContext())) {
                        callResetDmoatSettingsApi();
                    }
                } else {
                    showAlertMsgDialog(getContext(), getResources().getString(R.string.paused_internet_msg));
                }
            } else {
                showAlertMsgDialog(getContext(), getResources().getString(R.string.dmoat_offline));
            }
        });

        parent.findViewById(R.id.rlRebootdmoat).setOnClickListener(v -> {
            llStartStopPushNoti.setVisibility(View.GONE);
            new AlertDialog.Builder(getActivity())
                    .setTitle("Reboot d.moat")
                    .setMessage("Are you sure you want to reboot?")
                    .setPositiveButton("reboot", (dialog, whichButton) -> {
                        if (isDmoatOnline) {
                            if (UserManager.checkPauseInternetTimeStatus()) {
                                if (GeneralUtils.isConnected(getContext())) {
                                    showProgressDialog(true);
                                    Api.rebootDmoat(getActivity(), getRequestParams(), mRebootListner);
                                }
                            } else {
                                showAlertMsgDialog(getContext(), getResources().getString(R.string.paused_internet_msg));
                            }
                        } else {
                            showAlertMsgDialog(getContext(), getResources().getString(R.string.dmoat_offline));
                        }
                    })
                    .setNegativeButton("cancel", null).show();
        });

        btnSetPushNotification.setOnClickListener(v -> {

            objNoti = new PushNotificationsClass();
            showProgressDialog(true);
            Api.setNotifications(getActivity(), getRequestParamsForsetNoti(), mGetPushNotifications);

        });

        Switch mMuteAllSwitch = parent.findViewById(R.id.mute_all_notifications);
        mMuteAllSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> getUserSettingsSharedPreferences().edit().putBoolean(appendUser("mute_all"), isChecked).commit());

        Switch mPromptDeleteSwitch = parent.findViewById(R.id.confirm_unblock);
        mPromptDeleteSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> getUserSettingsSharedPreferences().edit().putBoolean(appendUser("prompt_unblock"), isChecked).commit());

        mMuteAllSwitch.setChecked(getUserSettingsSharedPreferences().getBoolean(appendUser("mute_all"), false));
        mPromptDeleteSwitch.setChecked(getUserSettingsSharedPreferences().getBoolean(appendUser("prompt_unblock"),
                false));

        TextView name = parent.findViewById(R.id.txt_name);
        TextView email = parent.findViewById(R.id.txt_email);
        TextView userId = parent.findViewById(R.id.txt_userId);
        selectedTemperatureLabel = parent.findViewById(R.id.selected_temperature_scale);
        selectedLedLabel = parent.findViewById(R.id.selected_led_label);

        User user = UserManager.getInstance().getLoggedInUser();

        userId.setText(user.userName);
        name.setText(user.name);
        email.setText(user.email);

        if (Constants.isFromDashboardFragment) {
            parent.findViewById(R.id.changeMode).performClick();
            Constants.isFromDashboardFragment = false;
        }
        updateTemperatureScaleTypeLabel();
        Api.getLEDMode(getActivity(), getRequestParams(), mGetLEDModeListener);
        DashBoardActivity.btnHomeDashboard.setOnClickListener(v -> {
            try {
                getActivity().finish();
                getActivity().getIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivityForResult(getActivity().getIntent(), 0);
                getActivity().overridePendingTransition(0, 0);

            } catch (Exception e) {
                Log.e("DashboardApiException", e.toString());
            }
        });


        showProgressDialog(true);
        Api.getNotifications(getActivity(), getRequestParams(), mGetPushNotifications);

        rl_themes = parent.findViewById(R.id.rl_themes);
        switchThemes.setOnClickListener(this);

        loadThemePref();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "profile_settings", null /* class override */);


    }

    private void switchButtonListner() {
        switchSensors.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                sensors = 1;
                switchSensors.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.colorPrimary)));

            } else {
                sensors = 0;
                switchSensors.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.white)));

            }
        });

        switchDevices.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                devices = 1;
                switchDevices.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.colorPrimary)));

            } else {
                devices = 0;
                switchDevices.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.white)));

            }
        });

        switchDmoatInfo.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                dmoatInfo = 1;
                switchDmoatInfo.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.colorPrimary)));

            } else {
                dmoatInfo = 0;
                switchDmoatInfo.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.white)));

            }
        });

        switchBlockedAletrts.setOnCheckedChangeListener((buttonView, isChecked) -> {

            if (isChecked) {
                blockedAlerts = 1;
                switchBlockedAletrts.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.colorPrimary)));

            } else {
                blockedAlerts = 0;
                switchBlockedAletrts.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.white)));

            }
        });

        switchConnectedDevices.setOnCheckedChangeListener((buttonView, isChecked) -> {

            if (isChecked) {
                conctedDevice = 1;
                switchConnectedDevices.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.colorPrimary)));

            } else {
                conctedDevice = 0;
                switchConnectedDevices.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.white)));

            }
        });
    }

    private String appendUser(String key) {
        return key + "_" + UserManager.getInstance().getLoggedInUser().userName;
    }

    private void updateTemperatureScaleTypeLabel() {
        int selectedScale = getUserSettingsSharedPreferences().getInt(appendUser(TEMPERATURE_SCALE), 0);
        selectedTemperatureLabel.setText(temperatureScales[selectedScale]);
    }

    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private void showLEDModeSelectionDialog() {
        new AlertDialog.Builder(getActivity())
                .setTitle("LED blinking interval:")
                .setSingleChoiceItems(LED_MODES, selectedLEDMode, null)
                .setPositiveButton("Save", (dialog, whichButton) -> {
                    int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                    changeLEDMode(selectedPosition);
                }).setNegativeButton("Cancel", null)
                .show();
    }

    private void showTemperatureScaleSelectionDialog() {
        int selectedScale = getUserSettingsSharedPreferences().getInt(appendUser(TEMPERATURE_SCALE), 0);
        new AlertDialog.Builder(getActivity())
                .setTitle("Select temperature scale:")
                .setSingleChoiceItems(temperatureScales, selectedScale, null)
                .setPositiveButton("Save", (dialog, whichButton) -> {

                    int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                    getUserSettingsSharedPreferences().edit().putInt(appendUser(TEMPERATURE_SCALE),
                            selectedPosition).commit();

                    updateTemperatureScaleTypeLabel();

                }).setNegativeButton("Cancel", null)
                .show();
    }

    private void changeLEDMode(int selectedPosition) {
        Log.d("LedSelectedMode", String.valueOf(selectedPosition));
        selectedLEDMode = selectedPosition;
        JSONObject j = getRequestParams();
        int last_led_request_id = j.optInt("request_id");
        JSONObject data = new JSONObject();
        try {
            if (selectedPosition == LED_OFF) {
                data.put("mode", "OFF");
            } else if (selectedPosition == LED_ON_5_SECONDS) {
                data.put("mode", "ON");
                data.put("frequency", "5");
            } else if (selectedPosition == LED_ON_10_SECONDS) {
                data.put("mode", "ON");
                data.put("frequency", "10");
            }
            j.put("data", data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isDmoatOnline) {
            if (UserManager.checkPauseInternetTimeStatus()) {
                showProgressDialog(true);
                Api.changeLEDMode(getActivity(), j, mChangeLEDModeListener);
            } else {
                showAlertMsgDialog(getContext(), getResources().getString(R.string.paused_internet_msg));
            }
        } else {
            showAlertMsgDialog(getContext(), getResources().getString(R.string.dmoat_offline));
        }
    }

    // sign out
    private void disableFCM() {
        // Disable auto init
        FirebaseMessaging.getInstance().setAutoInitEnabled(false);
        new Thread(() -> {
            try {
                // Remove InstanceID initiate to unsubscribe all topic
                // TODO: May be a better way to use FirebaseMessaging.getInstance().unsubscribeFromTopic()
                FirebaseInstanceId.getInstance().deleteInstanceId();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void logout() {

        editor.putString("ShowDeviceDialog", "yes");
        editor.commit();
        clearDashboardData();
        removeNotificaitonFromStatusBar(getApplicationContext(), 0);
        //    if(){
        showProgressDialog(true);
        Api.logout(getActivity(), getSignOutParams(), signOutListener);
        //   }else {
        //      logoutLocally();
        //      getActivity().finish();
        //  }
    }

    private JSONObject getSignOutParams() {
        JSONObject js = new JSONObject();
        try {
            js.put("app_id", pref.getString("app_id", ""));
            js.put("fcm_device_token", SPManager.getDeviceToken(getActivity()));
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("device_uuid", GeneralUtils.getDeviceID(getActivity()));
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;

    }

    private SharedPreferences getPrefs() {
        return pref;
    }
    //clear baseline

    //reset d.moat
    private void callResetDmoatSettingsApi() {
//        new AlertDialog.Builder(getContext())
//                .setTitle("d.moat")
//                .setMessage(getContext().getResources().getString(R.string.reset_dmoat))
//                .setPositiveButton("Reset", new DialogInterface.OnClickListener() {
//
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        showProgressDialog(true);
//                        Api.resetDeviceSettings(getActivity(), getRequestParams(), resetSettingsListener);
//                        showProgressDialog(true);
//                        Api.clearBaseline(getActivity(), getClearBaselineParams(), mClearBaselineListener);
//
//                    }
//                })
//                .setNegativeButton("Cancel", null).show();
        showProgressDialog(true);
//        Api.resetDeviceSettings(getActivity(), getRequestParams(), resetSettingsListener);
//        showProgressDialog(true);
//        Api.clearBaseline(getActivity(), getClearBaselineParams(), mClearBaselineListener);
        Api.deviceReset(getActivity(), getDeviceResetParams(), resetSettingsListener);
    }

    private JSONObject getRequestParams() {
        JSONObject requestData = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            requestData.put("request_id", Dmoat.getNewRequestId());
            requestData.put("app_id", pref.getString("app_id", ""));
            requestData.put("token", UserManager.getInstance().getLoggedInUser().token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestData;
    }

    private JSONObject getRequestParamsForsetNoti() {
        JSONObject requestData = new JSONObject();
        JSONObject dataObj = new JSONObject();
        try {
            requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            requestData.put("request_id", Dmoat.getNewRequestId());
            requestData.put("app_id", pref.getString("app_id", ""));
            requestData.put("token", UserManager.getInstance().getLoggedInUser().token);

            dataObj.put("sensors", sensors);
            dataObj.put("cnctd_host", conctedDevice);
            dataObj.put("blocked", blockedAlerts);
            dataObj.put("info", dmoatInfo);
            dataObj.put("devices", devices);

            requestData.put("data", dataObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestData;
    }

    private void callClearbaselineApi() {
        new AlertDialog.Builder(getContext())
                .setTitle("d.moat")
                .setMessage(getContext().getResources().getString(R.string.relearn_behaviour))
                .setPositiveButton("Relearn", (dialog, whichButton) -> {
                    showProgressDialog(true);
                    Api.clearBaseline(getActivity(), getClearBaselineParams(), mClearBaselineListener);
                })
                .setNegativeButton("Cancel", null).show();
    }

    // request params
    private JSONObject getClearBaselineParams() {
        JSONObject requestData = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            requestData.put("request_id", Dmoat.getNewRequestId());
            requestData.put("app_id", pref.getString("app_id", ""));
            requestData.put("token", UserManager.getInstance().getLoggedInUser().token);
            data.put("resource", "clear_baseline");
            data.put("request", "get");
            requestData.put("data", data);
            Log.d("ClearBaselineParams", String.valueOf(requestData));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestData;
    }

    private JSONObject getDeviceResetParams() {
        JSONObject requestData = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            requestData.put("request_id", Dmoat.getNewRequestId());
            requestData.put("app_id", pref.getString("app_id", ""));
            requestData.put("token", UserManager.getInstance().getLoggedInUser().token);
            data.put("resource", "clear_baseline");
            data.put("request", "get");
            requestData.put("data", data);
            Log.d("DeviceResetParams", String.valueOf(requestData));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestData;
    }

    //params for get mode
    private JSONObject getModeConfigParams() {
        JSONObject js = new JSONObject();
        try {
            js.put("app_id", pref.getString("app_id", ""));
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    private void showModeSelectionDialog(final String modeType) {
        int selectedScale = getUserSettingsSharedPreferences().getInt(appendUser(MODE), 0);
        new AlertDialog.Builder(getActivity())
                .setTitle("Select mode:")
                .setSingleChoiceItems(modes, selectedScale, null)
                .setPositiveButton("Switch", (dialog, whichButton) -> {
                    int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                    if (isDmoatOnline) {
                        if (selectedPosition == 0) {

                            changeMode(selectedPosition, "P&P");
                        } else {
                            changeMode(selectedPosition, "Advance");
                        }
                    } else {
                        MyToast.showMessage(getContext(), getResources().getString(R.string.dmoat_offline));
                    }
                }).setNegativeButton("Cancel", null)
                .show();
    }

    private void changeMode(int selectedPosition, String modeType) {
        if (modeType.equalsIgnoreCase("P&P")) {
            //show dialog and  then make a service call
            isPmodeOK = true;
            isAdvanceModeOK = false;
            if (modeFetched.equalsIgnoreCase("P&P")) {
                showAlertMsgDialog(getContext(), getResources().getString(R.string.already_pnp));
//                showGuideAlert(getActivity(), selectedPosition, getResources().getString(R.string.already_pnp), modeType);
            } else {
                showGuideAlert(getActivity(), selectedPosition, getResources().getString(R.string.alert_guide_pnp), modeType);
            }

        } else if (modeType.equalsIgnoreCase("Advance")) {
            //show dialog and  then make a service call
            isAdvanceModeOK = true;
            isPmodeOK = false;
            showGuideAlert(getActivity(), selectedPosition, getResources().getString(R.string.advance_mode_guide_pnp), modeType);
        }
    }

    private void showGuideAlert(Activity activity, final int selectedScale, String msg, final String modeType) {

        new AlertDialog.Builder(activity)
                .setTitle("d.moat mode")
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Switch", (dialog, whichButton) -> {
                    if (isPmodeOK) {
                        if (selectedScale == 0) {

                            callPlugPlayApi();
                        } else {
//                            MyToast.showMessage(getContext(), "calling pnp mode api");
                        }
                    } else {

                        getHelper().addFragment(new NetConfigFragment(), false, true);
//                            MyToast.showMessage(getContext(), "calling advance mode api");
                    }
                })
                .setNegativeButton(android.R.string.no, (dialogInterface, i) -> {
                    //finish alert dialog and go back
                    isPmodeOK = false;
                    isAdvanceModeOK = false;
                    showModeSelectionDialog(modeType);
                }).show();
    }

    private void callPlugPlayApi() {

        try {
            JSONObject js = new JSONObject();
            JSONObject data = new JSONObject();
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("app_id", pref.getString("app_id", ""));
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            data.put("request", "change_boot_mode");
            data.put("mode", 0);

            js.put("data", data);
            showProgressDialog(true);
            Api.changeMode(getActivity(), js, mChangeModeListener);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == switchThemes.getId()) {
            if (switchThemes.isChecked()) {
                switchThemes.setChecked(true);
                setNightMode(getActivity(), true);
                UserPrefs.setDarkTheme(getActivity(),"dark");
                switchThemes.setChecked(true);
                switchThemes.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.colorPrimary)));
            } else {
                switchThemes.setChecked(false);
                setNightMode(getActivity(), false);
                UserPrefs.setDarkTheme(getActivity(),"light");
                switchThemes.setChecked(false);
                switchThemes.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.white)));
            }
        }
    }

    public void setNightMode(Context target, boolean state) {

//        UiModeManager uiManager = (UiModeManager) target.getSystemService(Context.UI_MODE_SERVICE);


        if (state) {

            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            UserPrefs.setDarkTheme(getActivity(), "dark");
            getActivity().recreate();


        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

//            Intent i = new Intent(getActivity(), DashBoardActivity.class);
//            startActivity(i);
            UserPrefs.setDarkTheme(getActivity(), "light");
            getActivity().recreate();

        }
    }

    private void loadThemePref() {
        if (UserPrefs.getDarkTheme(getActivity()).equals("light")) {
            switchThemes.setChecked(false);
            switchThemes.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.white)));
        } else if (UserPrefs.getDarkTheme(getActivity()).equals("dark")) {
            switchThemes.setChecked(true);
            switchThemes.setTrackTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(), R.color.colorPrimary)));
        }

    }

}