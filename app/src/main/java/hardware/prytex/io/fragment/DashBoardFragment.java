package hardware.prytex.io.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import hardware.prytex.io.Analytics.AnalyticsTrackers;
import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.activity.AlertsMainActivity;
import hardware.prytex.io.activity.BandwidthChartMainActivity;
import hardware.prytex.io.activity.ConnectedDeviceActivity;
import hardware.prytex.io.activity.DashBoardActivity;
import hardware.prytex.io.adapter.DashboardAlertsAdapter;
import hardware.prytex.io.adapter.DashboardBandwidthAdapter;
import hardware.prytex.io.chart_config.BarYAxisValueFormatter;
import hardware.prytex.io.config.Constants;
import hardware.prytex.io.fragment.BandwidthFrags.BandwidthChartFrags.ProtocolChartFragment;
import hardware.prytex.io.fragment.Policy.AccessControl.PoliciesFragment;
import hardware.prytex.io.fragment.Policy.PoliciesMainFragment;
import hardware.prytex.io.fragment.SensorTabBars.SensorMainFragment;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.GeneralUtils;
import hardware.prytex.io.util.MyToast;
import hardware.prytex.io.util.SPManager;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.activity.DashBoardActivity.imgTopLogo;
import static hardware.prytex.io.activity.DashBoardActivity.mTitle;
import static hardware.prytex.io.activity.DashBoardActivity.setBottomNavTextColors;
import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;
import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;
import static hardware.prytex.io.parser.AlertsDashboardParser.listOfAllViewAlertsForDashboard;
import static hardware.prytex.io.parser.AlertsDashboardParser.modeStatus;
import static hardware.prytex.io.parser.AlertsDashboardParser.parentalModeStatus;
import static hardware.prytex.io.parser.BandwidthDashboardParser.listOfBandwidthForDashboard;
import static io.realm.internal.SyncObjectServerFacade.getApplicationContext;

/**
 * Created by abubaker on 9/28/16.
 */

@SuppressWarnings("ALL")
public class DashBoardFragment extends BaseFragment implements View.OnClickListener   {

    public static final String MODE = "mode";
    public static final int MODE_P_AND_P = 0;
    public static final int MODE_ADVANCE = 1;
    public static final String savedPauseTime = "";
    public static int InfoAlertsCount = 0;
    public static int BlockedAlertsCount = 0;
    public static int FlowAlertsCount = 0;
    public static int TemperatureValue = 0;
    public static int HumidityValue = 0;
    public static int AirQualityValue = 0;
    private Context mContext;
    public static int ConnectedDevicesCount = 0;
    public static int AndroidDevicesCount = 0;
    public static int MacDevicesCount = 0;
    public static int WindowDevicesCount = 0;
    public static int OtherDevicesCount = 0;

    public static boolean isAlertsCall = false;
    public static boolean isConnectedDevicesCall = false;
    public static boolean isSensorsCall = false;

    //new dashboard
    private RecyclerView mRecyclerView;
    private RecyclerView mRecyclerViewBandwidth;
    private PieChart pieChart;
    private PieChart pieChartTemp;
    private PieChart pieChartHumidity;
    private PieChart pieChartAirQuality;
    private BarChart barchart;
    private ProgressDialog dialog;
    private RelativeLayout rlAndroidView, rlMacView, rlWindowsView, rlOthersView;
    private TextView tvConnectedDevicesCount, tvAndroidCount, tvMacCount, tvWindowsCount, tvOtherCount, tvMBUnitLabel;
    private SharedPreferences pref;
    private TextView tvNoAlertsData, tvNoBandwidthDetected;
    private float[] yData;// = {35f, 20, 45};
    private final String[] xData = {"NewAlerts", "Blocked", "Muted"};
    private TextView tvNoConctedDevieData;
    private TextView tvDmoatStatus;
    private TextView tvParentalControl;
    private TextView tvDmoatMode;
    private TextView tv_no_active_devices;
    private ImageView imgdmoatStatusIcon;
    private ImageView imgParentalControl;
    private ImageView imgDmoatMode;

    private LinearLayout llCnctdDevicesDashboard;
    private LinearLayout llSensorView;
    private RelativeLayout rlNewAlerts;
    private LinearLayout llBandwidthView;
    private RelativeLayout rlDmoatParentalControl;
    private RelativeLayout rlAdvancedMode;



    @SuppressLint("CommitPrefEdits")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        AnalyticsTrackers.initialize(getContext());
//        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
//        trackScreenView("DashboardFragment");
        // Obtain the FirebaseAnalytics instance.

        int height = Resources.getSystem().getDisplayMetrics().heightPixels;

        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
//        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "Dashboard", null /* class override */);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, "Dashboard");
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, this.getClass().getName());

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);
     //   DashBoardActivity.menu.setVisibility(View.VISIBLE);
        isDmoatOnline = false;
        pref = getContext().getSharedPreferences("MyPref", 0);
        setBottomNavTextColors(getContext(), true, false, false, false);

    }

    @SuppressLint({"ClickableViewAccessibility", "CommitPrefEdits"})
    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        mContext = getActivity().getApplicationContext();
        pref = getContext().getSharedPreferences("MyPref", 0);

        boolean isChartFromDashBoard = false;


        setBottomNavTextColors(getContext(), true, false, false, false);
        parent.findViewById(R.id.sensor_layout).setOnClickListener(this);
        parent.findViewById(R.id.blocked_muted_alerts_layout).setOnClickListener(this);
        parent.findViewById(R.id.muted_alerts_layout).setOnClickListener(this);
        parent.findViewById(R.id.vulnerability_layout).setOnClickListener(this);
        parent.findViewById(R.id.policy_layout).setOnClickListener(this);
        parent.findViewById(R.id.connected_devices_layout).setOnClickListener(this);
        parent.findViewById(R.id.mode_layout).setOnClickListener(this);
        parent.findViewById(R.id.pause_internet_layout).setOnClickListener(this);


        llCnctdDevicesDashboard = parent.findViewById(R.id.llCnctdDevicesDashboard);
        llCnctdDevicesDashboard.setOnClickListener(this);



        mTitle.setVisibility(View.VISIBLE);
        imgTopLogo.setVisibility(View.GONE);

        //new dashboard UI

        LinearLayout llAlertsView = parent.findViewById(R.id.llAlertMain);
        llSensorView = parent.findViewById(R.id.llSensorsView);
        llBandwidthView = parent.findViewById(R.id.llBandwidth);
        rlNewAlerts = parent.findViewById(R.id.rlNewAlerts);
        rlDmoatParentalControl = parent.findViewById(R.id.rlDmoatParentalControl);
        rlAdvancedMode = parent.findViewById(R.id.rlAdvancedMode);
        mRecyclerView = parent.findViewById(R.id.listNewAlerts);
        mRecyclerViewBandwidth = parent.findViewById(R.id.listBandwidth);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        LinearLayoutManager mLayoutManagerBandwidth = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerViewBandwidth.setLayoutManager(mLayoutManagerBandwidth);

        pieChart = parent.findViewById(R.id.donutchart);
        pieChartTemp = parent.findViewById(R.id.donutchartTemp);
        pieChartHumidity = parent.findViewById(R.id.donutchartHumidity);
        pieChartAirQuality = parent.findViewById(R.id.donutchartAirQuality);
        barchart = parent.findViewById(R.id.bandwidthchart);
        tvMBUnitLabel = parent.findViewById(R.id.tv_mb_label);
        tvNoConctedDevieData = parent.findViewById(R.id.tvNoConctedDevice);

        tvDmoatStatus = parent.findViewById(R.id.tvDmoatStatus);
        imgdmoatStatusIcon =  parent.findViewById(R.id.imgdmoatStatusIcon);

        tvParentalControl = parent.findViewById(R.id.tvParentalControl);
        imgParentalControl =  parent.findViewById(R.id.imgParentalControl);

        imgDmoatMode =  parent.findViewById(R.id.imgDmoatMode);
        tvDmoatMode = parent.findViewById(R.id.tvDmoatMode);

        tv_no_active_devices = parent.findViewById(R.id.tv_no_active_devices);


        parent.findViewById(R.id.imgParentalControl).setOnTouchListener((v, event) -> {

            GeneralUtils.ScaleInAnimation(getActivity(),rlDmoatParentalControl);
            imgInfoBtn.setImageResource(R.drawable.info);
            setBottomNavTextColors(getApplicationContext(), false, true, false, false);
            ((DashBoardActivity)getActivity()).addFragment(new PoliciesMainFragment(), true, true);

            return false;
        });
        parent.findViewById(R.id.imgDmoatMode).setOnTouchListener((v, event) -> {
            GeneralUtils.ScaleInAnimation(getActivity(),rlAdvancedMode);
            Constants.isFromDashboardFragment = true;
            imgInfoBtn.setImageResource(R.drawable.info);
            setBottomNavTextColors(getActivity(), false, false, true, false);
            ((DashBoardActivity)getActivity()).addFragment(new ProfileFragment(), true, true);
            return false;
        });
        parent.findViewById(R.id.tvAlertHeading).setOnTouchListener((v, event) -> {
            goToAlerts();
            return false;
        });
        //heading click listeners
        RelativeLayout rlConnectedDevicesHeading = parent.findViewById(R.id.rlConnectedDevicesHeading);
        LinearLayout tvBandwidthHeading = parent.findViewById(R.id.llBandwidthHeading);
        RelativeLayout tvSensorHeading = parent.findViewById(R.id.rlSensorHeading);
        tvNoAlertsData = parent.findViewById(R.id.tvNoAlertsData);
        tvNoBandwidthDetected = parent.findViewById(R.id.tvNoBandwidthDetected);

        pieChart.setNoDataText("");
        pieChartTemp.setNoDataText("");
        pieChartAirQuality.setNoDataText("");
        pieChartAirQuality.setHoleColor(getResources().getColor(R.color.background_nested_card_color));
        pieChartHumidity.setNoDataText("");
        pieChartHumidity.setHoleColor(getResources().getColor(R.color.background_nested_card_color));
        barchart.setNoDataText("");

        ImageView imgAlertIcon = parent.findViewById(R.id.imgAlertsIcon);
        ImageView imgCnctdDeviceIcon = parent.findViewById(R.id.imgCnctdDeviceIcon);
        ImageView imgBandwidthIcon = parent.findViewById(R.id.imgBandwidthIcon);
        ImageView imgSensorsIcon = parent.findViewById(R.id.imgSensorIcon);

        rlAndroidView = parent.findViewById(R.id.rl_android_view);
        rlMacView = parent.findViewById(R.id.rl_mac_view);
        rlWindowsView = parent.findViewById(R.id.rl_windows_view);
        rlOthersView = parent.findViewById(R.id.rl_others_view);

        tvConnectedDevicesCount = parent.findViewById(R.id.tvConnectedDevicesCount);
        tvAndroidCount = parent.findViewById(R.id.tvAndroidDevicesCount);
        tvMacCount = parent.findViewById(R.id.tvMacDevicesCount);
        tvWindowsCount = parent.findViewById(R.id.tvWindowsDevicesCount);
        tvOtherCount = parent.findViewById(R.id.tvOtherDevicesCount);
        Button btnAlertsDetail = parent.findViewById(R.id.tvAlertsElipses);
        Button btnCnctdDevicesDetail = parent.findViewById(R.id.tvCnctdDeviceElipses);
        Button btnBandwidthDetail = parent.findViewById(R.id.tvBandwidthElipses);
        Button btnSensorsDetail = parent.findViewById(R.id.tvSensorsElipses);

        fillOfflineCharts();
        try {
            setBottomNavTextColors(getContext(), true, false, false, false);
            runMultipleAsyncTaskC();
            runMultipleAsyncTaskA();
            runMultipleAsyncTaskB();
        } catch (Exception e) {
            Log.e("DashboardApiException", e.toString());
        }

        DashBoardActivity.btnHomeDashboard.setOnClickListener(v -> {
            try {
                setBottomNavTextColors(getContext(), true, false, false, false);
                runMultipleAsyncTaskC();
                runMultipleAsyncTaskA();
                runMultipleAsyncTaskB();

            } catch (Exception e) {
                Log.e("DashboardApiException", e.toString());
            }
        });

        llAlertsView.setOnClickListener(v -> goToAlerts());

        pieChart.setOnClickListener(v -> goToAlerts());

        btnAlertsDetail.setOnClickListener(v -> goToAlerts());
        imgAlertIcon.setOnClickListener(v -> goToAlerts());
        rlConnectedDevicesHeading.setOnClickListener(v -> goToConnectedDevices());
        btnCnctdDevicesDetail.setOnClickListener(v -> goToConnectedDevices());
        imgCnctdDeviceIcon.setOnClickListener(v -> goToConnectedDevices());

        llBandwidthView.setOnClickListener(v -> goToBandwidthView());
        tvBandwidthHeading.setOnClickListener(v -> goToBandwidthView());
        btnBandwidthDetail.setOnClickListener(v -> goToBandwidthView());
        imgBandwidthIcon.setOnClickListener(v -> goToBandwidthView());

        llSensorView.setOnClickListener(v -> goToSensorsView());

        tvSensorHeading.setOnClickListener(v -> goToSensorsView());
        btnSensorsDetail.setOnClickListener(v -> goToSensorsView());
        imgSensorsIcon.setOnClickListener(v -> goToSensorsView());

    }

    private void goToConnectedDevices(){
        GeneralUtils.ScaleInAnimation(getActivity(),llCnctdDevicesDashboard);
        if (ConnectedDevicesCount >0){
            setBottomNavTextColors(getContext(), false, false, false, false);
            AddActivityandRemoveIntentTransition(ConnectedDeviceActivity.class);
        }else{
            showAlertMsgDialog(getActivity(), getResources().getString(R.string.no_connected_devices));
        }
    }

    private void goToAlerts() {
        GeneralUtils.ScaleInAnimation(getActivity(),rlNewAlerts);
        setBottomNavTextColors(getContext(), false, false, false, false);
        AddActivityandRemoveIntentTransition(AlertsMainActivity.class);
    }

    private void goToBandwidthView() {
        GeneralUtils.ScaleInAnimation(getActivity(),llBandwidthView);
        setBottomNavTextColors(getContext(), false, false, false, false);
        AddActivityandRemoveIntentTransition(BandwidthChartMainActivity.class);
    }

    private void goToSensorsView() {
        GeneralUtils.ScaleInAnimation(getActivity(),llSensorView);
        imgInfoBtn.setImageResource(R.drawable.info);
        DashBoardActivity.llBottomTabs.setVisibility(View.GONE);
        setBottomNavTextColors(getContext(), false, false, false, false);
        ((DashBoardActivity)getActivity()).addFragmentwitoutTabbar(new SensorMainFragment(), true, true);
    }

    @Override
    public int getLayoutId() {
        int height = Resources.getSystem().getDisplayMetrics().heightPixels;
        if(height < 1794){
            //Toast.makeText(getActivity(),"Small Screen",Toast.LENGTH_SHORT).show();
            return R.layout.dashboard_layout_small_dp;
        }
        else{
            //Toast.makeText(getActivity(),"Large Screen",Toast.LENGTH_SHORT).show();
            return R.layout.dashboard_fragment;
        }

    }

    @Override
    public String getTitle() {
        String namee = UserManager.getInstance().getLoggedInUser().name + "\'s Prytex Hardware";
        return UserManager.getInstance().getLoggedInUser().name + "\'s Prytex Hardware";
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.llCnctdDevicesDashboard) {


            goToConnectedDevices();
//                setBottomNavTextColors(getContext(), false, false, false, false);
//                AddActivityandRemoveIntentTransition(ConnectedDeviceActivity.class);
        }else if(v.getId() == R.id.sensor_layout) {


            if (UserManager.checkPauseInternetTimeStatus()) {
                SPManager.setTermsServicesStatus(getContext(), true);
                ((DashBoardActivity) getActivity()).addFragment(new SensorMainFragment(), true, true);
            } else {
                MyToast.showMessage(getContext(), getResources().getString(R.string.paused_internet_msg));
            }
        }else if(v.getId() == R.id.vulnerability_layout) {


            if (UserManager.checkPauseInternetTimeStatus()) {
                ((DashBoardActivity) getActivity()).addFragment(new ProtocolChartFragment(), true, true);
            } else {
                MyToast.showMessage(getContext(), getResources().getString(R.string.paused_internet_msg));
            }
        }else if(v.getId() == R.id.policy_layout) {


            if (UserManager.checkPauseInternetTimeStatus()) {
                ((DashBoardActivity) getActivity()).addFragment(new PoliciesFragment(), true, true);
            } else {
                MyToast.showMessage(getContext(), getResources().getString(R.string.paused_internet_msg));
            }
        }else if(v.getId() == R.id.connected_devices_layout) {


            if (UserManager.checkPauseInternetTimeStatus()) {

                AddActivityandRemoveIntentTransition(ConnectedDeviceActivity.class);
            } else {
                MyToast.showMessage(getContext(), getResources().getString(R.string.paused_internet_msg));
            }
        }else if(v.getId() == R.id.mode_layout) {


            //                getModeConfigParams
//                ((DashBoardActivity)getActivity()).addFragment(new ConnectedDeviceActivity(),true,true);
        }
    }

    private void showProgressDialog() {
        if (false) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    //new dashboard UI

    private void animatePieChart(boolean isOffline) {

        pieChart.setDescription(null);
        pieChart.setRotationEnabled(true);
//        pieChart.setUsePercentValues(true);

        pieChart.setHoleColor(getResources().getColor(R.color.background_nested_card_color));
//        pieChart.setCenterTextColor(Color.WHITE);
//        pieChart.setHoleRadius(25f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setCenterTextSize(10);

        if (isOffline) {
            pieChart.setDrawEntryLabels(false);
        } else {
            pieChart.setDrawEntryLabels(true);
        }
        pieChart.getLegend().setEnabled(false);
//        pieChart.setEntryLabelColor(Color.RED);
        pieChart.setRotationAngle(-90);
        //More options just check out the documentation!
        addDataSet(isOffline);
        if (isOffline) {
            pieChart.animate();
        } else {
            pieChart.animateY(1000);
        }
    }

    private void animateBandwidthChart() {
        //bar chart for bandwidth
        List<String> xVals = new ArrayList();
        List<BarEntry> yVals = new ArrayList();

        for (int i = 0; i < listOfBandwidthForDashboard.size(); i++) {
            String hName = listOfBandwidthForDashboard.get(i).getHostName();
            String bar;
            if (hName.length() > 6) {
                bar = hName.substring(0, 7);
            } else {
                bar = hName;
            }
            xVals.add(bar);
            float tUsg = listOfBandwidthForDashboard.get(i).getTotalUsage();

            yVals.add(new BarEntry(i + 1, tUsg));

        }

        BarDataSet set1;
        set1 = new BarDataSet(yVals, "");
        set1.setDrawIcons(false);
        set1.setDrawValues(false);
        set1.setColors(new int[]{R.color.BarColorA, R.color.BarColorB, R.color.BarColorC, R.color.BarColorD, R.color.BarColorE}, getContext());
        set1.setAxisDependency(YAxis.AxisDependency.RIGHT);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
//
        BarData data = new BarData(dataSets);
        data.setValueTextSize(10f);
        if(listOfBandwidthForDashboard.size() == 5 )
        data.setBarWidth(0.5f);                  // width of bar chart
         else
             if(listOfAllViewAlertsForDashboard.size() < 3)
             {
                 data.setBarWidth(0.3f);
             }
        data.setValueFormatter(new LargeValueFormatter());
        barchart.setData(data);
        barchart.setScaleEnabled(true);
        barchart.moveViewTo(data.getEntryCount() - 7, 50f, YAxis.AxisDependency.LEFT);
        //X-axis
        XAxis xAxis = barchart.getXAxis();
//        xAxis.setDrawGridLines(false);
        xAxis.setEnabled(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(xVals));
//        xAxis.setAvoidFirstLastClipping(true); abcd

        //Y-axis
        IAxisValueFormatter custom = new BarYAxisValueFormatter();

        barchart.getAxisRight().setEnabled(false);
        YAxis leftAxis = barchart.getAxisLeft();
//        leftAxis.setGranularity(200);
//        leftAxis.setEnabled(true);
//        leftAxis.setGranularityEnabled(true);
//        leftAxis.setValueFormatter(custom);
//        leftAxis.setSpaceTop(35f);
        leftAxis.setAxisMinimum(0.01f);


        barchart.setDescription(null);
        barchart.getLegend().setEnabled(false);
        barchart.animateY(1000);
    }

    private void addDataSet(boolean isDmoatOffline) {
        ArrayList<PieEntry> yEntrys = new ArrayList<>();
        ArrayList<String> xEntrys = new ArrayList<>();

        for (float yDatum : yData) {
            yEntrys.add(new PieEntry(yDatum, 0));
        }

        xEntrys.addAll(Arrays.asList(xData).subList(1, xData.length));

        //create the data set
        PieDataSet pieDataSet = new PieDataSet(yEntrys, "");
        pieDataSet.setSliceSpace(-20);
        pieDataSet.setValueTextSize(13);
        pieDataSet.setSelectionShift(0);
        pieDataSet.setYValuePosition(PieDataSet.ValuePosition.INSIDE_SLICE);
        pieDataSet.setValueTextColor(getResources().getColor(R.color.chart_value_color));//red_light
        pieDataSet.setValueFormatter(new LargeValueFormatter());

        if (isDmoatOffline) {
            pieDataSet.setDrawValues(false);
        } else {
            pieDataSet.setDrawValues(true);
            // Use Legend Entry
            //add colors to dataset
        }
        pieDataSet.setColors(new int[]{R.color.colorA, R.color.colorB, R.color.colorC}, getContext());

        //create pie data object
        PieData pieData = new PieData(pieDataSet);
        pieChart.setData(pieData);
        pieChart.invalidate();
    }

    private void animatePieChartSensor(PieChart pChartSensor, boolean isDmoatOffline) {

        pChartSensor.setDescription(null);
        pChartSensor.setRotationEnabled(true);
        pChartSensor.setUsePercentValues(true);
        pChartSensor.setHoleColor(getResources().getColor(R.color.white));
//        pieChart.setCenterTextColor(Color.WHITE);
        pChartSensor.setHoleRadius(90f);
        pChartSensor.setTransparentCircleAlpha(0);
        pChartSensor.setCenterTextSize(14);
        pChartSensor.setNoDataText("No Sensor Data found");
        pChartSensor.setNoDataText("No Sensor Data found");

        pChartSensor.setDrawEntryLabels(false);
        pChartSensor.getLegend().setEnabled(false);
//        pieChart.setEntryLabelColor(getResources().getColor(R.color.white));
        //More options just check out the documentation!
        pChartSensor.setRotationAngle(100);
        if (isDmoatOffline) {
            pChartSensor.animate();
        } else {
            pChartSensor.animateY(1000);
        }
    }

    private void addDataSetTemp(boolean isDmoatOffline) {
        ArrayList<PieEntry> yEntrys = new ArrayList<>();
        ArrayList<String> xEntrys = new ArrayList<>();
        float[] yDataTemp;
        if (isDmoatOffline) {
            yDataTemp = new float[]{90};
        } else {
            yDataTemp = new float[]{TemperatureValue};
        }
        for (float v : yDataTemp) {
            yEntrys.add(new PieEntry(v, 0));
        }

        xEntrys.addAll(Arrays.asList(xData).subList(1, xData.length));
        //create the data set
        PieDataSet pieDataSet = new PieDataSet(yEntrys, "");
        pieDataSet.setDrawValues(false);
        pieDataSet.setSelectionShift(0);
        //add colors to dataset

        if (isDmoatOffline) {
            pieDataSet.setColors(new int[]{R.color.dmoat_offline_clr}, getContext());
        } else {
            if (TemperatureValue >= 87) {
                //HAZARDOUS
                pieDataSet.setColors(new int[]{R.color.red}, getActivity());
            } else if (TemperatureValue >= 40) { //&& TemperatureValue <= 90) {
                //Good
                pieDataSet.setColors(new int[]{R.color.green}, getActivity());
            } else { //if (TemperatureValue < 41) {
                //bad
                pieDataSet.setColors(new int[]{R.color.temp_color}, getActivity());
            }
        }
        //create pie data object
        PieData pieData = new PieData(pieDataSet);
        pieChartTemp.setData(pieData);
        pieChartTemp.setHoleColor(getResources().getColor(R.color.background_nested_card_color));

        pieChartTemp.invalidate();
    }

    private void addDataSetHumidity(boolean isDmoatOffline) {
        ArrayList<PieEntry> yEntrys = new ArrayList<>();
        ArrayList<String> xEntrys = new ArrayList<>();
        float[] yDatHumidity = new float[]{HumidityValue};
        if (isDmoatOffline) {
            yDatHumidity = new float[]{1000};
        }
        for (float v : yDatHumidity) {
            yEntrys.add(new PieEntry(v, 0));
        }

        xEntrys.addAll(Arrays.asList(xData).subList(1, xData.length));
        //create the data set
        PieDataSet pieDataSet = new PieDataSet(yEntrys, "");
        pieDataSet.setDrawValues(false);
        pieDataSet.setSelectionShift(0);
        //add colors to dataset
        if (isDmoatOffline) {
            pieDataSet.setColors(new int[]{R.color.dmoat_offline_clr}, getContext());
        } else {
            if (HumidityValue >= 61) {
                //HAZARDOUS
                pieDataSet.setColors(new int[]{R.color.red}, getContext());
            } else if (HumidityValue >= 30) { //&& HumidityValue <= 60) {
                //Good
                pieDataSet.setColors(new int[]{R.color.green}, getContext());
            } else if (HumidityValue < 30) {
                //Bad
                pieDataSet.setColors(new int[]{R.color.temp_color}, getContext());
            }
        }
        //create pie data object
        PieData pieData = new PieData(pieDataSet);
        pieChartHumidity.setData(pieData);
        pieChartHumidity.setHoleColor(getResources().getColor(R.color.background_nested_card_color));

        pieChartHumidity.invalidate();
    }

    private void addDataSetAirQuality(boolean isDmoatOffline) {
        ArrayList<PieEntry> yEntrys = new ArrayList<>();
        ArrayList<String> xEntrys = new ArrayList<>();
        int AirQuality = AirQualityValue / 100;
        float[] yDatHumidity = new float[]{AirQuality};
        if (isDmoatOffline) {
            yDatHumidity = new float[]{91};
        }

        for (float v : yDatHumidity) {
            yEntrys.add(new PieEntry(v, 0));
        }

        xEntrys.addAll(Arrays.asList(xData).subList(1, xData.length));
        //create the data set
        PieDataSet pieDataSet = new PieDataSet(yEntrys, "");
        pieDataSet.setDrawValues(false);
        pieDataSet.setSelectionShift(0);
        //add colors to dataset
        if (isDmoatOffline) {
            pieDataSet.setColors(new int[]{R.color.dmoat_offline_clr}, getContext());
        } else {
            if (AirQualityValue >= 1000) {
                //HAZARDOUS
                pieDataSet.setColors(new int[]{R.color.red}, getContext());
            } else if (AirQualityValue >= 600 && AirQualityValue <= 999) {
                //BAD
                pieDataSet.setColors(new int[]{R.color.temp_color}, getContext());
            } else if (AirQualityValue >= 450 && AirQualityValue <= 599) {
                //FAIR
                pieDataSet.setColors(new int[]{R.color.temp_color}, getContext());
            } else if (AirQualityValue < 450) {
                //GOOD
                pieDataSet.setColors(new int[]{R.color.green}, getContext());
            }
        }
        //create pie data object
        PieData pieData = new PieData(pieDataSet);
        pieChartAirQuality.setData(pieData);
        pieChartAirQuality.setHoleColor(getResources().getColor(R.color.background_nested_card_color));
        pieChartAirQuality.invalidate();
    }

    private void runMultipleAsyncTaskA() // Run Multiple Async Task
    {
        AlertsAsyncTask asyncTask = new AlertsAsyncTask(); // First
        ConnectedDevicesAsyncTask asyncTask2 = new ConnectedDevicesAsyncTask(); // Second
         asyncTask.execute();
         asyncTask2.execute();

    }

    private void runMultipleAsyncTaskB() // Run Multiple Async Task
    {

        SensorsAsyncTask asyncTask = new SensorsAsyncTask(); // Second
        asyncTask.execute();
    }

    private void runMultipleAsyncTaskC() // Run Multiple Async Task
    {

        BandwidthAsyncTask asyncTask2 = new BandwidthAsyncTask();
        asyncTask2.execute();
    }

    private JSONObject getParams() {
        JSONObject js = new JSONObject();
        try {

            js.put("app_id", pref.getString("app_id", ""));
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    //Start First Async Task:
    private class AlertsAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            Log.i("AsyncTask", "FirstOnPreExecute()");
        }

        @Override
        protected Void doInBackground(Void... params) {

            Log.i("AsyncTask", "AlertsAsyncTask");

            Handler mainHandler = new Handler(Looper.getMainLooper());
            Runnable myRunnable = () -> {
                //Code that uses AsyncHttpClient in your case ConsultaCaract()
                isAlertsCall = true;
                Api.getAlertsDashboard(mContext, getParams(), mListener);
            };
            mainHandler.post(myRunnable);

            //call alerts get api
//            Thread.sleep(100); //call connected devices count api


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.d("AsyncTask", "FirstonPostExecute()");
        }
    }

    //Start Second Async Task:
    private class ConnectedDevicesAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            Log.i("AsyncTask", "SecondOnPreExecute()");
        }

        @Override
        protected Void doInBackground(Void... params) {

            Log.d("AsyncTask", "ConnectedDevicesAsyncTask");
            //call connected devices count api

            Handler mainHandler = new Handler(Looper.getMainLooper());
            Runnable myRunnable = () -> {
                //Code that uses AsyncHttpClient in your case ConsultaCaract()
                isConnectedDevicesCall = true;
                Api.getConnectedDevicesDashboard(mContext, getParams(), mListener);
            };
            mainHandler.post(myRunnable);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.d("AsyncTask", "SecondOnPostExecute()");
        }
    }

    //Start Sensor Async Task:
    private class SensorsAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            Log.i("AsyncTask", "FirstOnPreExecute()");
        }

        @Override
        protected Void doInBackground(Void... params) {

            Log.i("AsyncTask", "AlertsAsyncTask");

            Handler mainHandler = new Handler(Looper.getMainLooper());
            Runnable myRunnable = () -> {
                //Code that uses AsyncHttpClient in your case ConsultaCaract()
                isSensorsCall = true;
                Api.getSensorValuesDashboard(mContext, getParams(), mListenerB);
            };
            mainHandler.post(myRunnable);

            //call alerts get api
//            Thread.sleep(100); //call connected devices count api


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.d("AsyncTask", "FirstonPostExecute()");
        }
    }

    //Start bandwidth Async Task:
    private class BandwidthAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            Log.i("AsyncTask", "FirstOnPreExecute()");
        }

        @Override
        protected Void doInBackground(Void... params) {

            Log.i("AsyncTask", "bandwidthAsyncTask");

            Handler mainHandler = new Handler(Looper.getMainLooper());
            Runnable myRunnable = () -> {
                //Code that uses AsyncHttpClient in your case ConsultaCaract()

                Api.getBandwidthDashboard(mContext, getParams(), mListenerC);
            };
            mainHandler.post(myRunnable);

            //call alerts get api
//            Thread.sleep(100); //call connected devices count api


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.d("AsyncTask", "FirstonPostExecute()");
        }
    }

    // listenerB
    private final AsyncTaskListener mListenerC = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            Log.d("AsyncTaskResponse", "response");

            if (result.code == 200 || result.code == 2) {
                Log.d("AsyncTask", "success");
                try {
                    if (listOfBandwidthForDashboard.size() > 0) {
                        tvNoBandwidthDetected.setVisibility(View.GONE);
                        tv_no_active_devices.setVisibility(View.GONE);
                        tvMBUnitLabel.setVisibility(View.VISIBLE);
                        Log.d("AsyncTaskListBandwidth", String.valueOf(listOfBandwidthForDashboard.size()));
                        DashboardBandwidthAdapter mAdapterBandwidth = new DashboardBandwidthAdapter(getContext(), listOfBandwidthForDashboard, mBandwidthListener);
                        mRecyclerViewBandwidth.setAdapter(mAdapterBandwidth);
                        animateBandwidthChart();
                        barchart.invalidate();

                    } else {
                        tv_no_active_devices.setVisibility(View.VISIBLE);
                        tvNoBandwidthDetected.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    Log.d("AsyncExcepti9on", String.valueOf(e));
                }
            } else {
            }

        }
    };
    // listenerB
    private final AsyncTaskListener mListenerB = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            Log.d("AsyncTaskResponse", "response");

            if (result.code == 200 || result.code == 2) {
                try {
                    Log.d("AsyncTask", "success");
                    if (isSensorsCall) {

                        animatePieChartSensor(pieChartHumidity, false);
                        animatePieChartSensor(pieChartTemp, false);
                        animatePieChartSensor(pieChartAirQuality, false);

                        float fahrenhietCelciusTemp;
                        if (TemperatureValue == 0 && HumidityValue == 0) {
//                            pieChartTemp.setCenterText("0");
                        } else {
                            fahrenhietCelciusTemp = ((TemperatureValue * 9) / 5 + 32);
                            String convertedFahrnhtTemp = String.format("%.0f", fahrenhietCelciusTemp);
                            TemperatureValue = (int) fahrenhietCelciusTemp;
                            pieChartTemp.setCenterText(convertedFahrnhtTemp);

                            pieChartHumidity.setCenterText(String.valueOf(HumidityValue));
                            pieChartAirQuality.setCenterText(String.valueOf(AirQualityValue));
                            try {
                                addDataSetTemp(false);
                                addDataSetHumidity(false);
                                addDataSetAirQuality(false);
                            } catch (Exception e) {
                                Log.d("DataSetSensorExcep", e.toString());
                            }

                        }

                        isSensorsCall = false;
                    } else {
                    }
                } catch (Exception e) {
                    Log.d("AsyncExcepti9on2", String.valueOf(e));
                }
            } else {
                addDataSetTemp(true);
                addDataSetHumidity(true);
                addDataSetAirQuality(true);
//                MyToast.showMessage(getActivity(), getResources().getString(R.string.error_message_generic));
            }
        }
    };
    // listener
    private final AsyncTaskListener mListener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            Log.d("AsyncTaskResponse", "response");

            if (result.code == 200 || result.code == 2) {
                Log.d("AsyncTask", "success");
                try {
                    if(parentalModeStatus.equalsIgnoreCase("0"))
                    {
                        imgParentalControl.setImageResource(R.drawable.ic_parental_control_deactive);
                        tvParentalControl.setTextColor(getResources().getColor(R.color.dmoat_offline_clr));
                        tvParentalControl.setText(getActivity().getResources().getString(R.string.activate_parental_control));
                    }else
                    {
                        imgParentalControl.setImageResource(R.mipmap.ic_parental);
                        tvParentalControl.setTextColor(getResources().getColor(R.color.black));
                        tvParentalControl.setText(getActivity().getResources().getString(R.string.parental_control_enabled));
                    }
                    if(modeStatus.equalsIgnoreCase("0"))
                    {
                        imgDmoatMode.setImageResource(R.mipmap.plug_and_play);
                        tvDmoatMode.setTextColor(getResources().getColor(R.color.black));
                        tvDmoatMode.setText(getActivity().getResources().getString(R.string.plug_and_play_mode_activated));
                    }else {
                        imgDmoatMode.setImageResource(R.mipmap.ic_advance);
                        tvDmoatMode.setTextColor(getResources().getColor(R.color.black));
                        tvDmoatMode.setText(getActivity().getResources().getString(R.string.advance_mode_activated));
                    }
                    if (isAlertsCall) {
                        yData = new float[]{InfoAlertsCount, FlowAlertsCount, BlockedAlertsCount};
                        if (InfoAlertsCount > 0 || BlockedAlertsCount > 0 || FlowAlertsCount > 0) {
                            tvNoAlertsData.setVisibility(View.GONE);
                            animatePieChart(false);
                        } else {
                            yData = new float[]{33, 33, 33};
                            tvNoAlertsData.setVisibility(View.VISIBLE);
                            animatePieChart(true);
                        }
                        Log.d("AsyncTaskListAllAlerts", String.valueOf(listOfAllViewAlertsForDashboard.size()));

                        if (listOfAllViewAlertsForDashboard.size() > 0) {
                            //asd
                            tvNoAlertsData.setVisibility(View.GONE);
                        }
                        DashboardAlertsAdapter mAdapter = new DashboardAlertsAdapter(getContext(), mRecyclerView, listOfAllViewAlertsForDashboard, mSelectedListener);
                        mRecyclerView.setAdapter(mAdapter);
                        Log.d("Count_Adapter",mAdapter.getItemCount()+"");
                        Log.d("Count_RecyclerView",mRecyclerView.getChildCount()+"");

                        if (mAdapter.getItemCount()<=0){
                            tvNoAlertsData.setText(getResources().getString(R.string.no_alerts_data_available));
                            tvNoAlertsData.setVisibility(View.VISIBLE);
                        }
                        isAlertsCall = false;
                    } else if (isConnectedDevicesCall) {

                        tvConnectedDevicesCount.setText("(" + ConnectedDevicesCount + ")");
                        if (AndroidDevicesCount > 0) {
                            tvAndroidCount.setText("  " + AndroidDevicesCount + "  ");
                            rlAndroidView.setVisibility(View.VISIBLE);
                        }
                        if (MacDevicesCount > 0) {
                            tvMacCount.setText("  " + MacDevicesCount + "  ");
                            rlMacView.setVisibility(View.VISIBLE);
                        }
                        if (WindowDevicesCount > 0) {
                            tvWindowsCount.setText("  " + WindowDevicesCount + "  ");
                            rlWindowsView.setVisibility(View.VISIBLE);
                        }
                        if (OtherDevicesCount > 0) {
                            tvOtherCount.setText("  " + OtherDevicesCount + "  ");
                            rlOthersView.setVisibility(View.VISIBLE);
                        }
                        if (ConnectedDevicesCount >0){
                            tvNoConctedDevieData.setVisibility(View.GONE);
                        }else{
                            tvNoConctedDevieData.setVisibility(View.VISIBLE);
                        }

                        if (isDmoatOnline) {
                            imgdmoatStatusIcon.setBackground(getResources().getDrawable(R.drawable.filled_circle_online));
                            tvDmoatStatus.setTextColor(getResources().getColor(R.color.green));
                            tvDmoatStatus.setText(getActivity().getResources().getString(R.string.online));
                        } else {
                            imgdmoatStatusIcon.setBackground(getResources().getDrawable(R.drawable.filled_circle_offline));
                            tvDmoatStatus.setTextColor(getResources().getColor(R.color.dmoat_offline_clr));
//                            isDmoatOnline = false;
                        }
                        isConnectedDevicesCall = false;
                        //tvNoConctedDevieData.setVisibility(View.GONE);
                    } else {
                    }
                } catch (Exception e) {
                    Log.d("AsyncException", String.valueOf(e));
                }

//                MyToast.showMessage(getContext(), "successfuly received data");
            } else {
//                MyToast.showMessage(getActivity(), getResources().getString(R.string.error_message_generic));
            }
        }
    };

    public static void clearDashboardData() {

        InfoAlertsCount = 0;
        BlockedAlertsCount = 0;
        FlowAlertsCount = 0;
        TemperatureValue = 0;
        HumidityValue = 0;
        AirQualityValue = 0;

        ConnectedDevicesCount = 0;
        AndroidDevicesCount = 0;
        MacDevicesCount = 0;
        WindowDevicesCount = 0;
        OtherDevicesCount = 0;

        isAlertsCall = false;
        isConnectedDevicesCall = false;
        isSensorsCall = false;

        listOfBandwidthForDashboard.clear();
        listOfAllViewAlertsForDashboard.clear();
    }

    private void fillOfflineCharts() {
        yData = new float[]{33, 33, 33};
        animatePieChart(true);
        barchart.setNoDataText("");
        //fill sensor charts
        animatePieChartSensor(pieChartHumidity, true);
        animatePieChartSensor(pieChartTemp, true);
        animatePieChartSensor(pieChartAirQuality, true);
        TemperatureValue = 78;
        HumidityValue = 430;
        AirQualityValue = 860;
        pieChartTemp.setCenterText(String.valueOf(0));

        pieChartHumidity.setCenterText(String.valueOf(0));
        pieChartAirQuality.setCenterText(String.valueOf(0));
//        addDataSetTemp(true);
//        addDataSetHumidity(true);
//        addDataSetAirQuality(true);

        ArrayList<PieEntry> yEntrys = new ArrayList<>();
        ArrayList<String> xEntrys = new ArrayList<>();
        float[] yDataTemp = new float[]{TemperatureValue};

        for (float v : yDataTemp) {
            yEntrys.add(new PieEntry(v, 0));
        }

        xEntrys.addAll(Arrays.asList(xData).subList(1, xData.length));
        //create the data set
        PieDataSet pieDataSet = new PieDataSet(yEntrys, "");
        pieDataSet.setDrawValues(false);
        pieDataSet.setSelectionShift(0);
        //add colors to dataset

//        pieDataSet.setColors(new int[]{R.color.dmoat_offline_clr}, getContext());

        //create pie data object
        PieData pieData = new PieData(pieDataSet);
        pieChartTemp.setData(pieData);
        pieChartTemp.invalidate();
        pieChartHumidity.setData(pieData);
        pieChartHumidity.invalidate();
        pieChartAirQuality.setData(pieData);
        pieChartAirQuality.invalidate();
    }

    private synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

    private void AddActivityandRemoveIntentTransition(Class activity)
    {
        Intent intent = new Intent(getActivity(), activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivityForResult(intent, 0);
        getActivity().overridePendingTransition(0,0);
        getActivity().finish();
    }

    private final DashboardAlertsAdapter.OnAlertSelectedListener mSelectedListener = () -> goToAlerts();

    private final DashboardBandwidthAdapter.OnAlertSelectedListener mBandwidthListener = () -> goToBandwidthView();

    @Override
    public void onResume() {
        super.onResume();
        DashBoardActivity.showSwitchButton();
    }

    @Override
    public void onStop() {
        super.onStop();
        DashBoardActivity.hideSwitchButton();
    }
}
