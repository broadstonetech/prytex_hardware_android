package hardware.prytex.io.fragment.Policy.ProtocolsPolicy;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.adapter.HostSelectionAdapter;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.GeneralUtils;
import hardware.prytex.io.util.MyToast;

import org.json.JSONException;
import org.json.JSONObject;


import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;
import static hardware.prytex.io.parser.ConnectedDevicesParser.isDevicesListReceived;
import static hardware.prytex.io.parser.ConnectedDevicesParser.listOfConnetcedDevice;

@SuppressWarnings("ALL")
public class HostSelectionFragment extends BaseFragment {

    private RecyclerView mRecyclerView;
    private SharedPreferences pref;
    private ProgressDialog dialog;
    private Button btnProceed;

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        imgInfoBtn.setVisibility(View.GONE);
        pref = getContext().getSharedPreferences("MyPref", 0);

        String appId = pref.getString("app_id", "");


        mRecyclerView = parent.findViewById(R.id.recycler_view);
        btnProceed = parent.findViewById(R.id.btnProceed);

        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        btnProceed.setEnabled(false);
        if (GeneralUtils.isConnected(getContext())) {
            showProgressDialog(true);
            Api.getContactedHosts(getActivity(), getRequestparams(), listener);
        } else {
            showAlertMsgDialog(getContext(), getResources().getString(R.string.internet_connection));
        }

        btnProceed.setOnClickListener(v -> {
            int position = 0;
            if(listOfConnetcedDevice.size() > 0) {
                for (int i = 0; i < listOfConnetcedDevice.size(); i++) {
                    if (listOfConnetcedDevice.get(i).isInPolicy()) {
                        position = i;
                        break;
                    } else {
                        position = -1;
                    }
                }

                if (!(position == -1)){
                    ProtocolPolicyControlFragment fragment = new ProtocolPolicyControlFragment();
                    fragment.hostIp = listOfConnetcedDevice.get(position).getIp_cd();
                    fragment.hostName = listOfConnetcedDevice.get(position).getHostName_cd();
                    fragment.hostID = listOfConnetcedDevice.get(position).getHostId_cd();
                    fragment.isForDevice = true;

//            for (int i = 0; i < model.policyProtocolsLists.length(); i++)
                    getHelper().replaceFragment(fragment, false, true);
                    Log.d("PolicyPlanIndex", String.valueOf(position));
                }else{
                    MyToast.showMessage(getContext(), "Please select a device to proceed.");
                }
            }

        });


    }

    @Override
    public String getTitle() {
        return "Device Selection";
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_host_selection;
    }

    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private JSONObject getRequestparams() {
        JSONObject requestData = new JSONObject();
        try {
            requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            requestData.put("request_id", Dmoat.getNewRequestId());
            requestData.put("app_id", pref.getString("app_id", ""));
            requestData.put("token", UserManager.getInstance().getLoggedInUser().token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestData;
    }

    private final AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {


            showProgressDialog(false);
            if (result.code == 200 || result.code == 2) {
                if (isDevicesListReceived) {
                    if (listOfConnetcedDevice.size() > 0) {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        btnProceed.setEnabled(true);

                        mRecyclerView.setAdapter(new HostSelectionAdapter(getContext(), listOfConnetcedDevice));

                    } else {
                        showAlertMsgDialog(getContext(), getResources().getString(R.string.no_connected_devices));
                    }
                } else {
                    showAlertMsgDialog(getContext(), getResources().getString(R.string.no_connected_devices));
                }
            } else if (result.code == 503) {
                showAlertMsgDialog(getContext(), result.message);
            } else if (result.code == 409) {
                showAlertMsgDialog(getContext(), result.message);
            } else {
                showAlertMsgDialog(getContext(), getResources().getString(R.string.no_connected_devices));
            }
        }
    };



}
