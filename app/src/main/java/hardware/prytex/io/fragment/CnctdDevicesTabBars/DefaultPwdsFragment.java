package hardware.prytex.io.fragment.CnctdDevicesTabBars;


import android.app.FragmentManager;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.adapter.CredentialsDefaltAdapter;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.model.ActivePortsModel;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;


import static hardware.prytex.io.parser.ConnectedDevicesParser.listOfConnetcedDevice;


@SuppressWarnings("ALL")
public class DefaultPwdsFragment extends BaseFragment {

    public static int HostSelectedPosition = -1;
    //    public HashMap<String, String> listOfActivePorts = new HashMap<String, String>();
    public static ArrayList<ActivePortsModel> listOfDefaultDevices = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
//        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "default_passwords", null /* class override */);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, "default_passwords");
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, this.getClass().getName());

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);

    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        LinearLayout rlMainView = parent.findViewById(R.id.rlMainView);
        TextView tvHostName = parent.findViewById(R.id.tvHostName);
        TextView tvHostIP = parent.findViewById(R.id.tvHostIP);
        tvHostName.setText(listOfConnetcedDevice.get(HostSelectedPosition).getHostName_cd());
        tvHostIP.setText(listOfConnetcedDevice.get(HostSelectedPosition).getIp_cd());
        RecyclerView mRecyclerView = parent.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        if (listOfDefaultDevices.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            CredentialsDefaltAdapter mAdapter = new CredentialsDefaltAdapter(getContext(), listOfDefaultDevices);
            mRecyclerView.setAdapter(mAdapter);
        }else{
            rlMainView.setVisibility(View.GONE);
            parent.findViewById(R.id.tvError).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public String getTitle() {
        return "Default Passwords";
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_connected_device_credentails;
    }
}

