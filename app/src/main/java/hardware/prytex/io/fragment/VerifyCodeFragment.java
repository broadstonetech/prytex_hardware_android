package hardware.prytex.io.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.MyToast;

import org.json.JSONException;
import org.json.JSONObject;

import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;
import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by usman on 12/29/16.
 */

@SuppressWarnings("ALL")
public class VerifyCodeFragment extends BaseFragment implements View.OnClickListener {

    private EditText editTextUsername;
    private EditText editTextCode;
    private String username;
    private boolean isForgotPassword = true;
    private SharedPreferences pref;

    @Override
    public int getLayoutId() {
        return R.layout.verify_code_fragment;
    }

    @Override
    public void initViews(final View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);

//        imgBack = (ImageView) parent.findViewById(R.id.imgBack);

                // a potentially time consuming task
        TextInputLayout usernameLayout = parent.findViewById(R.id.input_layout_username);
        TextInputLayout codeLayout = parent.findViewById(R.id.input_layout_code);
        TextView description = parent.findViewById(R.id.txt_description);
                editTextUsername = getParentView().findViewById(R.id.edittext_username);
                editTextCode = getParentView().findViewById(R.id.edittext_code);
//        imgInfoBtn.setVisibility(View.GONE);

        parent.findViewById(R.id.submit).setOnClickListener(this);
        final Bundle bundle = this.getArguments();
        if (bundle != null) {

            String desc = bundle.getString("desription");
            description.setText(desc);
//            String regEmail = bundle.getString("registered_email");
//            description.setText(getContext().getResources().getString(R.string.verify_code_description_a) + regEmail + getContext().getResources().getString(R.string.verify_code_description_b));
            isForgotPassword = bundle.getBoolean("forgot_password");
            if (isForgotPassword) {
                username = bundle.getString("username");
                usernameLayout.setVisibility(View.GONE);
                editTextUsername.setText(username);
//                imgBack.setVisibility(View.GONE);
            } else {
                usernameLayout.setVisibility(View.GONE);
            }

        }

//        }catch (Exception e){
//            Log.d("Exception", e.toString());
//        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.submit) {

            if (editTextCode.getText().toString().equalsIgnoreCase("")) {
                showAlertMsgDialog(getContext(), "Please enter verification code.");
            } else {
                if (isForgotPassword) {
                    if (forgotPasswordValidateData())
                        getParentView().findViewById(R.id.submit).setBackgroundColor(getResources().getColor(R.color.colorPrimay));
                    forgotPasswordCodeVerifyRequest();
                } else {
                    if (resetSettingsValidateData())
                        getParentView().findViewById(R.id.submit).setBackgroundColor(getResources().getColor(R.color.colorPrimay));
                    resetSettingsCodeVerifyRequest();
                }
            }
        }else if(v.getId() == R.id.imgBack) {

            if (isForgotPassword) {
                getHelper().addFragment(new ForgotPasswordFragment(), true, true);
            } else {
                getHelper().addFragment(new ProfileFragment(), true, true);
            }
        }
    }

    private boolean forgotPasswordValidateData() {
//        if (editTextUsername.length() == 0) {
//            MyToast.showMessage(getActivity(), "Please enter your username");
//            return false;
//        }

        if (editTextCode.length() == 0) {
            MyToast.showMessage(getActivity(), "Please enter your code");
            return false;
        }
        return true;
    }

    private boolean resetSettingsValidateData() {
        if (editTextCode.length() == 0) {
            MyToast.showMessage(getActivity(), "Please enter your code");
            return false;
        }
        return true;
    }

    private void forgotPasswordCodeVerifyRequest() {
        JSONObject data = new JSONObject();
        try {
            data.put("app_id", pref.getString("app_id", ""));
            data.put("request_id", Dmoat.getNewRequestId());
            JSONObject codeData = new JSONObject();
            codeData.put("username", editTextUsername.getText().toString());
            codeData.put("code", editTextCode.getText().toString());
            data.put("data", codeData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (isDmoatOnline) {
            Api.verifyPasswordCode(getActivity(), data, mListener);
        } else {
            MyToast.showMessage(getContext(), getResources().getString(R.string.dmoat_offline));
        }
        getParentView().findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
//        getParentView().findViewById(R.id.submit).setVisibility(View.GONE);
    }

    private void resetSettingsCodeVerifyRequest() {
        JSONObject data = new JSONObject();
        try {
            data.put("app_id", pref.getString("app_id", ""));
            data.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            data.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            data.put("token", UserManager.getInstance().getLoggedInUser().token);
            data.put("request_id", Dmoat.getNewRequestId());
            JSONObject codeData = new JSONObject();
            codeData.put("code", editTextCode.getText().toString());
            data.put("data", codeData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (isDmoatOnline) {
            Api.verifyResetSettingsCode(getActivity(), data, mResetSettingsListener);
        } else {
            MyToast.showMessage(getContext(), getResources().getString(R.string.dmoat_offline));
        }
        getParentView().findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
//        getParentView().findViewById(R.id.submit).setVisibility(View.GONE);
    }

    private final AsyncTaskListener mListener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (!isAdded()) {
                return;
            }
            getParentView().findViewById(R.id.progressBar).setVisibility(View.GONE);
            getParentView().findViewById(R.id.submit).setVisibility(View.VISIBLE);
            getParentView().findViewById(R.id.submit).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            if (result.code == 200 || result.code == 2) {
                ChangePasswordFragment changePasswordFragment = new ChangePasswordFragment();
                Bundle tokenBundle = new Bundle();
                tokenBundle.putBoolean("set_new_password", true);
                tokenBundle.putString("token", result.message);
                tokenBundle.putString("username", username);
                changePasswordFragment.setArguments(tokenBundle);
                getHelper().onBack();
                getHelper().addFragment(changePasswordFragment, false, true);
            } else if (result.code == 503) {
                showAlertMsgDialog(getContext(), result.message);
            } else if (result.code == 409) {
                showAlertMsgDialog(getContext(), result.message);
            } else {
                if (result.message.equalsIgnoreCase("")) {
                    MyToast.showMessage(getActivity(), getResources().getString(R.string.error_message_generic));
                } else {
                    MyToast.showMessage(getActivity(), result.message);
                }
            }
        }
    };

    private final AsyncTaskListener mResetSettingsListener = result -> {
        if (!isAdded()) {
            return;
        }
        getParentView().findViewById(R.id.progressBar).setVisibility(View.GONE);
        getParentView().findViewById(R.id.submit).setVisibility(View.VISIBLE);
        getParentView().findViewById(R.id.submit).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        if (result.code == 200 || result.code == 2) {
            MyToast.showMessage(getActivity(), "Prytex Hardware restored to default settings.");
            getHelper().replaceFragment(new ProfileFragment(), R.id.fragment_container, true, false);
        } else {
            if (result.message.equalsIgnoreCase("")) {
                MyToast.showMessage(getActivity(), getResources().getString(R.string.error_message_generic));
            } else {
                MyToast.showMessage(getActivity(), result.message);
            }
        }
    };
}
