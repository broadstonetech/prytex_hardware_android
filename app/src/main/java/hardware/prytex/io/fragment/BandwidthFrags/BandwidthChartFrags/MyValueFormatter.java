package hardware.prytex.io.fragment.BandwidthFrags.BandwidthChartFrags;

import com.github.mikephil.charting.formatter.ValueFormatter;


import java.text.DecimalFormat;

public class MyValueFormatter extends ValueFormatter  {

    private final DecimalFormat mFormat;
//    private String mText = "";
//    private int mMaxLength = 5;

    public MyValueFormatter() {
        mFormat = new DecimalFormat("###,###,##0.0"); // use one decimal
    }

//    public void setAppendix(String appendix) {
//        this.mText = appendix;
//    }



    @Override
    public String getFormattedValue(float value) {
        return makePretty(value);
    }
//    @Override
//    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
//        // write your logic here
//        return makePretty(value);
//    }

    private String makePretty(double number) {

        String r;
        Double val ;

        if (number >= 1000000000){
            val = number/1000000000;
            r= mFormat.format(val) +"b";
        }else if (number >= 1000000){
            val = number/1000000;
            r= mFormat.format(val) +"m";
        }else if(number >= 1000){
            val = number/1000;
            r= mFormat.format(val) +"k";
        }else {
            r= mFormat.format(number);
        }


        return r;
    }
}
