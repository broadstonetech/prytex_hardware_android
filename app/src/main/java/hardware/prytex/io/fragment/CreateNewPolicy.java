package hardware.prytex.io.fragment;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.db.DBClient;
import hardware.prytex.io.fragment.Policy.AccessControl.PoliciesFragment;
import hardware.prytex.io.fragment.Policy.AccessControl.PolicyProtocolsFragment;
import hardware.prytex.io.model.DMoatAlert;
import hardware.prytex.io.model.DmoatHost;
import hardware.prytex.io.model.DmoatPolicy;
import hardware.prytex.io.model.Protocol;
import hardware.prytex.io.model.SelectedHosts;
import hardware.prytex.io.model.SelectedProtocols;
import hardware.prytex.io.model.TargetUrl;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.DmoatUtils;
import hardware.prytex.io.util.GeneralUtils;
import hardware.prytex.io.util.MyToast;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import io.realm.RealmList;


import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;
import static hardware.prytex.io.util.DmoatUtils.PROTOCOLS;
import static hardware.prytex.io.util.DmoatUtils.PROTOCOLS_KEYS;
import static hardware.prytex.io.util.DmoatUtils.PROTOCOLS_VALUE;
import static hardware.prytex.io.util.DmoatUtils.getDays;

/**
 * Created by khawarraza on 19/12/2016.
 */

@SuppressWarnings("ALL")
public class CreateNewPolicy extends BaseFragment implements View.OnClickListener {


    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private ProgressDialog dialog;
    private View savePolicy;
    private LinearLayout targetUrlsContainer;
    private EditText title, startTime, endTime, days, hostsList, protocolsList;
    private Button btnMon, btnTue, btnWed, btnThu, btnFri, btnSat, btnSun;
    private boolean isMonday = false, isTuesday = false, isWedneday = false, isThursday = false, isFriday = false, isSaturday = false, isSunday = false;

    private int a =1;
    private int b=1;
    private int c=1;
    private int d=1;
    private int e=1;
    private int f=1;
    private int g=1;

    private int selectedHostTypeIndex = 0;
    private boolean ispolicyUpdate = false;
    private DmoatPolicy policyUpdate;
    private ArrayList<DMoatAlert> selectedHosts = new ArrayList<>();
    private final ArrayList<DMoatAlert> connectedHosts = new ArrayList<>();
    private static final int[] WEEK_DAYS_WEIGHTS = {64, 32, 16, 8, 4, 2, 1};
    private int startHours, startMins, startSeconds, endHours, endMins, endSeconds;
    private static final String[] HOSTS_OPTIONS = {"All", "Choose from connected hosts"};
    private final boolean[] mSelectedWeekDays = {false, false, false, false, false, false, false};
    private final boolean[] mSelectedProtocols = {false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false};
    public static final String[] WEEK_DAYS = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
//    public static String[] PROTOCOLS;

    public static final ArrayList<SelectedProtocols> listOfSelectedProtocols = new ArrayList<>();
    public static  SelectedProtocols selectedProtocols;

    private static final ArrayList<SelectedHosts> listOfSelectedHosts = new ArrayList<>();


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_create_new_policy;
    }

    @Override
    public String getTitle() {
        return "Create Policy";
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            int policyId = getArguments().getInt("policy");
            ispolicyUpdate = getArguments().getBoolean("policy_update", false);
            policyUpdate = DBClient.getInstance().getPolicyById(policyId);
        }
//        view.setClickable(true);
        return view;
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

//        PROTOCOLS = getResources().getStringArray(R.array.protocols);
//init shared pref

        String s = String.valueOf(listOfSelectedProtocols.size());
        String sH = String.valueOf(listOfSelectedHosts.size());
        Log.d("protocolsListCreatPolcy", String.valueOf(listOfSelectedProtocols.size()));

        pref = getContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();

        title = parent.findViewById(R.id.policy_title);
        startTime = parent.findViewById(R.id.policy_start_time);
        endTime = parent.findViewById(R.id.policy_end_time);
        days = parent.findViewById(R.id.days_of_policy);
        hostsList = parent.findViewById(R.id.hosts_list);
        protocolsList = parent.findViewById(R.id.protocols_list);

        //policy days id's
        btnMon = parent.findViewById(R.id.btnMondayCP);
        btnTue = parent.findViewById(R.id.btnTuesdayCP);
        btnWed = parent.findViewById(R.id.btnWednesdayCP);
        btnThu = parent.findViewById(R.id.btnThursdayCP);
        btnFri = parent.findViewById(R.id.btnFridayCP);
        btnSat = parent.findViewById(R.id.btnSaturdayCP);
        btnSun = parent.findViewById(R.id.btnSundayCP);

        btnMon.setOnClickListener(this);
        btnTue.setOnClickListener(this);
        btnWed.setOnClickListener(this);
        btnThu.setOnClickListener(this);
        btnFri.setOnClickListener(this);
        btnSat.setOnClickListener(this);
        btnSun.setOnClickListener(this);

        hostsList.setText(HOSTS_OPTIONS[0]);

        View addTargetUrlBtn = parent.findViewById(R.id.add_target_url);
        savePolicy = parent.findViewById(R.id.save_new_policy);

        targetUrlsContainer = parent.findViewById(R.id.target_urls_container);



        startTime.setOnClickListener(this);
        endTime.setOnClickListener(this);
        addTargetUrlBtn.setOnClickListener(this);
        hostsList.setOnClickListener(this);
        protocolsList.setOnClickListener(this);
        days.setOnClickListener(this);
        savePolicy.setOnClickListener(this);

        Calendar c = Calendar.getInstance();
        startHours = c.get(Calendar.HOUR_OF_DAY);
        startMins = c.get(Calendar.MINUTE);
        startSeconds = c.get(Calendar.SECOND);
        endHours = c.get(Calendar.HOUR_OF_DAY) + 1;
        endMins = c.get(Calendar.MINUTE);
        endSeconds = c.get(Calendar.SECOND);

        if (PoliciesFragment.isCreatePolicy) {
            addFieldForTargetURL();
        }
        Api.getContactedHosts(getActivity(), getRequestparams(), connectedHostsListener);
        setPolicyData();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.policy_start_time) {

            TimePickerDialog dialogStartTime = TimePickerDialog.newInstance((view, hourOfDay, minute, second) -> {
                startHours = hourOfDay;
                startMins = minute;
                startSeconds = second;
                startTime.setText(startHours + ":" + startMins);
            }, startHours, startMins, startSeconds, true);
            dialogStartTime.show(getActivity().getFragmentManager(), "StartDateTimePicker");

        }else if(v.getId() == R.id.policy_end_time) {
            TimePickerDialog dialogEndTime = TimePickerDialog.newInstance((view, hourOfDay, minute, second) -> {
                endHours = hourOfDay;
                endMins = minute;
                endSeconds = second;
                endTime.setText(endHours + ":" + endMins);
            }, endHours, endMins, endSeconds, true);
            dialogEndTime.show(getActivity().getFragmentManager(), "EndtDateTimePicker");

        }else if(v.getId() == R.id.days_of_policy) {

            showWeekDaysPicker();

        }else if(v.getId() == R.id.add_target_url) {

            addFieldForTargetURL();

        } else if(v.getId() == R.id.hosts_list) {
//                showHostsTypeSelectionDialog();
            //get policy title, start time, end time, protocols list, urls and protocols nad store in sp.
            saveDataInSP();
            getHelper().addFragment(new SelectConnectedDeviceFragment(), false, true);


        }else if(v.getId() == R.id.protocols_list) {

//                showProtocolsListDialog();  old design of protocols dialog check boxes
            //get policy title, start time, end time, protocols list, urls and protocols nad store in sp.
//                saveDataInSP();
            showNewProtocolsDialog();

        }else if(v.getId() == R.id.icon_delete_target_url) {

            View view = (View) v.getTag();
            targetUrlsContainer.removeView(view);

        }else if(v.getId() == R.id.save_new_policy) {

            MyToast.showMessage(getContext(), "create policy");
            if (validateFields()) {
                showProgressDialog(true);
                createNewPolicy();
            }

        }else if(v.getId() == R.id.btnMondayCP) {


            if (a == 1) {
                btnMon.setBackground(getContext().getResources().getDrawable(R.drawable.day_selected));
                isMonday = true;
                a++;

            } else {
                btnMon.setBackground(getContext().getResources().getDrawable(R.drawable.policy_days_border));
                isMonday = false;
                a = 1;
            }

        } else if(v.getId() == R.id.btnTuesdayCP) {

            if (b == 1) {
                btnTue.setBackground(getContext().getResources().getDrawable(R.drawable.day_selected));
                isTuesday = true;
                b++;

            } else {
                btnTue.setBackground(getContext().getResources().getDrawable(R.drawable.policy_days_border));
                isTuesday = false;
                b = 1;
            }

        }else if(v.getId() == R.id.btnWednesdayCP) {

            if (c == 1) {
                btnWed.setBackground(getContext().getResources().getDrawable(R.drawable.day_selected));
                isWedneday = true;
                c++;

            } else {
                btnWed.setBackground(getContext().getResources().getDrawable(R.drawable.policy_days_border));
                isWedneday = false;
                c = 1;
            }

        }else if(v.getId() == R.id.btnThursdayCP) {

            if (d == 1) {
                btnThu.setBackground(getContext().getResources().getDrawable(R.drawable.day_selected));
                isThursday = true;
                d++;

            } else {
                btnThu.setBackground(getContext().getResources().getDrawable(R.drawable.policy_days_border));
                isThursday = false;
                d = 1;
            }

        } else if(v.getId() == R.id.btnFridayCP) {

            if (e == 1) {
                btnFri.setBackground(getContext().getResources().getDrawable(R.drawable.day_selected));
                isFriday = true;
                e++;

            } else {
                btnFri.setBackground(getContext().getResources().getDrawable(R.drawable.policy_days_border));
                isFriday = false;
                e = 1;
            }

        } else if(v.getId() == R.id.btnSaturdayCP) {

            if (f == 1) {
                btnSat.setBackground(getContext().getResources().getDrawable(R.drawable.day_selected));
                isSaturday = true;
                f++;

            } else {
                btnSat.setBackground(getContext().getResources().getDrawable(R.drawable.policy_days_border));
                isSaturday = false;
                f = 1;
            }

        } else if(v.getId() == R.id.btnSundayCP) {

            if (g == 1) {
                btnSun.setBackground(getContext().getResources().getDrawable(R.drawable.day_selected));
                isSunday = true;
                g++;

            } else {
                btnSun.setBackground(getContext().getResources().getDrawable(R.drawable.policy_days_border));
                isSunday = false;
                g = 1;
            }
        }

    }

    private final AsyncTaskListener createPolicyListener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            showProgressDialog(false);
            if (result.code == 200 || result.code == 2) {
//                getHelper().onBack();
                getHelper().replaceFragment(new PoliciesFragment(), false, true);
                // Do Noting!!! Wait for pubnub payload
            } else {
               /* if (!ispolicyUpdate)
                    DBClient.getInstance().deletePolicy(policyCreated);
                else {
                    DBClient.getInstance().createNewPolicy(backupPolicy);
                }*/
                savePolicy.setEnabled(true);
                MyToast.showMessage(getActivity(), "Failed. Please try later!");
            }
        }
    };

    private final AsyncTaskListener connectedHostsListener = result -> {

    };

    private void setPolicyData() {
        if (policyUpdate == null)
            return;
        // backupPolicy = DmoatUtils.createPolicyObject(policyUpdate);  // If updation fails, this object stores contain
        // previous policy data to restore. (In service listener and onMessageReceived())
        title.setText(policyUpdate.getTitle());
        startTime.setText(policyUpdate.getStartTime());
        endTime.setText(policyUpdate.getEndTime());

        int[] indexes = getDays(policyUpdate.days);
        for (int i = 0; i < indexes.length; i++) {
            if (indexes[i] == 1)
                mSelectedWeekDays[i] = true;
        }
        days.setText(DmoatUtils.getSelectedWeekDays(indexes));
        setSelectedDays(indexes);

        selectedProtocolsCount = 0;
        for (int i = 0; i < policyUpdate.getProtocols().size(); i++) {
            for (int j = 0; j < PROTOCOLS_KEYS.length; j++) {
                if (policyUpdate.getProtocols().get(i).name.equalsIgnoreCase(PROTOCOLS_KEYS[j]))
                    mSelectedProtocols[j] = true;
//                mSelectedProtocols[policyUpdate.getProtocols().get(i).getIndex()] = true;
            }
            selectedProtocolsCount++;
        }
        protocolsList.setText(DmoatUtils.getProtocols(policyUpdate));

        for (TargetUrl url : policyUpdate.getTargetUrls())
            addFieldForTargetURL();
        setTargetUrls(policyUpdate.getTargetUrls());

        for (DmoatHost hosts : policyUpdate.hosts) {
            DMoatAlert alert = new DMoatAlert();
            alert.hostId = hosts.hostId;
            alert.hostname = hosts.hostName;
            selectedHosts.add(alert);
        }
        if (!selectedHosts.isEmpty()) {
            setHostText();
            selectedHostTypeIndex = 1;
        }
    }

    private void createNewPolicy() {
        DmoatPolicy.DMoatPolicyBuilder builder = new DmoatPolicy.DMoatPolicyBuilder();
        DmoatPolicy policy = builder.setTitle(title.getText().toString())
                .setPolicyId(policyUpdate != null ? policyUpdate.id : 0)
                .setStartTime(startHours + ":" + startMins)
                .setEndTime(endHours + ":" + endMins)
                .addTargetUrls(getTargetUrls())
                .addWeekDays(getDaysWeighty()/*, getWeekDays()*/)
                .addHosts(geSelectedtHosts())
//                .addProtocols(getSelectedProtocols()).getPolicy();
                .addProtocols(getSelectedProtocols()).getPolicy();
//                .build();
        makePolicyRequest(policy);
        if (policy == null) {
            MyToast.showMessage(getActivity(), "Error! Please try again");
        }
    }

    private void makePolicyRequest(DmoatPolicy policy) {
        JSONObject requestData = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            requestData.put("request_id", Dmoat.getNewRequestId());
            requestData.put("app_id", pref.getString("app_id", ""));
            requestData.put("token", UserManager.getInstance().getLoggedInUser().token);

            data.put("type", ispolicyUpdate ? "update" : "create");
            data.put("id", policy.getId());
            data.put("title", policy.getTitle());
            data.put("s_time", policy.getStartTime());
            data.put("e_time", policy.getEndTime());
            data.put("days", getDaysWeighty());

            data.put("protocols", getRequestProtocls(policy));
            data.put("targets", getTargetUrls(policy));
            data.put("hosts_list", getConnectedHosts(policy));
            requestData.put("data", data);
            if (isDmoatOnline) {
                Api.createPolicy(getActivity(), requestData, createPolicyListener);
            }else{
                MyToast.showMessage(getContext(),getResources().getString(R.string.dmoat_offline));
            }
            int last_policy_request_id = requestData.optInt("request_id");
            savePolicy.setEnabled(false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private ArrayList<DMoatAlert> geSelectedtHosts() {
        return selectedHosts;
    }


    private JSONArray getTargetUrls(DmoatPolicy policy) {
        JSONArray jarr = new JSONArray();
        for (int i = 0; i < policy.getTargetUrls().size(); i++)
            jarr.put(policy.getTargetUrls().get(i).getUrl());
        return jarr;
    }

    private JSONArray getRequestProtocls(DmoatPolicy policy) {
        JSONArray jarr = new JSONArray();
        for (int i = 0; i < policy.getProtocols().size(); i++)
            jarr.put(policy.getProtocols().get(i).getName());
        return jarr;
    }


    private JSONArray getConnectedHosts(DmoatPolicy policy) {
        JSONArray hosts = new JSONArray();
        for (int i = 0; i < policy.getHosts().size(); i++)
            hosts.put(policy.getHosts().get(i).getHostId());
        return hosts;
    }

    private ArrayList<Protocol> getSelectedProtocols() {
        ArrayList<Protocol> protocols = new ArrayList<>();
        //old design code
//        for (int i = 0; i < mSelectedProtocols.length; i++) {
//            if (mSelectedProtocols[i]) {
//                Protocol p = new Protocol();
////                p.setIndex(i);
//                p.setName(PROTOCOLS_KEYS[i]);
//                protocols.add(p);
//            }
//        }
//        return protocols;
    //    new design code

        for (int i = 0; i < listOfSelectedProtocols.size(); i++) {
//            if (mSelectedProtocols[i]) {
                Protocol p = new Protocol();
//                p.setIndex(i);
                p.setName(listOfSelectedProtocols.get(i).getProtocolKey());
                protocols.add(p);
//            }
        }
        return protocols;
    }

    private int getDaysWeighty() {
        int weight = 0;
        //old design code
        for (int i = 0; i < mSelectedWeekDays.length; i++) {
            if (mSelectedWeekDays[i]) {
                weight = weight + WEEK_DAYS_WEIGHTS[i];
            }
        }
        return weight;

//        new design code


//        return 112;
    }

    private void setTargetUrls(RealmList<TargetUrl> targetUrls) {
        ArrayList<String> urls = new ArrayList<>();
        for (int i = 0; i < targetUrlsContainer.getChildCount(); i++) {
            EditText txt = targetUrlsContainer.getChildAt(i).findViewById(R.id.target_url);
            txt.setText(targetUrls.get(i).getUrl());
        }
    }

    private ArrayList<String> getTargetUrls() {
        ArrayList<String> urls = new ArrayList<>();
        for (int i = 0; i < targetUrlsContainer.getChildCount(); i++) {
            EditText txt = targetUrlsContainer.getChildAt(i).findViewById(R.id.target_url);
            if (txt != null && txt.getText().toString().length() > 0) {
                urls.add(txt.getText().toString());
            }
        }
        return urls;
    }

    private boolean validateFields() {
        boolean error = false;
        String ttl = title.getText().toString();
        String start = startTime.getText().toString();
        String end = endTime.getText().toString();

        if (ttl.length() == 0) {
            title.setError("Title is required");
            return false;
        } else {
            title.setError(null);
        }
        if (start.length() == 0) {
            startTime.setError("Start time is required");
            return false;
        } else {
            startTime.setError(null);
        }
        if (end.length() == 0) {
            endTime.setError("End time is required");
            return false;
        } else {
            endTime.setError(null);
        }
        if (!areWeekDaysSelected()) {
            days.setError("Please select week days");
            return false;
        } else {
            days.setError(null);
        }
        if (selectedHostTypeIndex == -1) {
            MyToast.showMessage(getActivity(), "Please select some host");
        }
        if (!GeneralUtils.dateComparison(start, end)) {
            endTime.setError("Error");
            endTime.setText("");
            MyToast.showMessage(getActivity(), "End time should come after start time");
            return false;
        } else {
            endTime.setError(null);
        }
        if (targetUrlsContainer.getChildCount() > 0) {
            for (int i = 0; i < targetUrlsContainer.getChildCount(); i++) {
                EditText txt = targetUrlsContainer.getChildAt(i).findViewById(R.id.target_url);
                if (!Patterns.WEB_URL.matcher(txt.getText().toString()).matches()) {
                    txt.setError("Incorrect target url");
                    error = true;
                }
            }
            return !error;
        }
        return true;
    }

    private boolean areWeekDaysSelected() {
        getSelectedDaysNew();
      //  old method with old design
        for (boolean mSelectedWeekDay : mSelectedWeekDays) {
            if (mSelectedWeekDay) {
                return true;
            }
        }
        return false;

        //new methos=d with new design implementation
//        getSelectedDaysNew();
//        return true;
    }

    private void getSelectedDaysNew(){
        if (isMonday){
            mSelectedWeekDays[0] = true;
        }
        if (isTuesday){
            mSelectedWeekDays[1] = true;
        }
        if (isWedneday){
            mSelectedWeekDays[2] = true;
        }
        if (isThursday){
            mSelectedWeekDays[3] = true;
        }
        if (isFriday){
            mSelectedWeekDays[4] = true;
        }
        if (isSaturday){
            mSelectedWeekDays[5] = true;
        }
        if (isSunday){
            mSelectedWeekDays[6] = true;
        }
    }

    private int selectedProtocolsCount = 0;

    private void showNewProtocolsDialog(){
        //move to new fragment and select protocols
      //  add them in list and comeback with keys list

//        getHelper().addFragment(new PolicyProtocolsFragment(), false, true);
        PolicyProtocolsFragment fragment = new PolicyProtocolsFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();


//        String names[] ={"A","B","C","D"};
//        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
//        LayoutInflater inflater = getActivity().getLayoutInflater();
//        View convertView = (View) inflater.inflate(R.layout.policy_protocol, null);
//        alertDialog.setView(convertView);
//        alertDialog.setTitle("Protocols");
//        ListView lv = (ListView) convertView.findViewById(R.id.listViewProtocls);
//        ProtocolsAdapter adapter = new ProtocolsAdapter(getContext(), PROTOCOLS_KEYS);
//
//        lv.setAdapter(adapter);
//        alertDialog.show();
    }

    private void showWeekDaysPicker() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose week days")
                .setMultiChoiceItems(WEEK_DAYS, mSelectedWeekDays,
                        (dialog, which, isChecked) -> mSelectedWeekDays[which] = isChecked)
                // Set the action buttons
                .setPositiveButton("Select", (dialog, id) -> {
                    String str = "";
                    int selectedDaysCount = getSelectedDaysCount(mSelectedWeekDays);
                    int j = 0;
                    for (int i = 0; i < mSelectedWeekDays.length; i++) {
                        if (mSelectedWeekDays[i]) {
                            if (j < selectedDaysCount - 2) {
                                str = str + WEEK_DAYS[i] + ", ";
                            } else if (j < selectedDaysCount - 1) {
                                str = str + WEEK_DAYS[i] + " and ";
                            } else {
                                str = str + WEEK_DAYS[i];
                            }
                            j++;
                        }
                    }
                    days.setText(str);
                })
                .setNegativeButton("Clear All", (dialog, id) -> {
                    for (int i = 0; i < mSelectedWeekDays.length; i++) {
                        mSelectedWeekDays[i] = false;
                        days.setText("");
                    }
                });

        builder.create().show();
    }

    private int getSelectedDaysCount(boolean[] mSelectedWeekDays) {
        int dayCount = 0;
        for (boolean day : mSelectedWeekDays) {
            if (day)
                dayCount++;

        }
        return dayCount;
    }

    private void addFieldForTargetURL() {
        View view = View.inflate(getActivity(), R.layout.target_url_item, null);
        View deleteIcon = view.findViewById(R.id.icon_delete_target_url);
        deleteIcon.setTag(view);
        deleteIcon.setOnClickListener(this);
        targetUrlsContainer.addView(view);
    }



    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    public interface OnHostsSelectedListener {
        void onHostsSelected(ArrayList<DMoatAlert> hosts);

    }

    private final OnHostsSelectedListener hostsSelectedListener = hosts -> {
        selectedHosts = hosts;
        setHostText();
    };

    private void setHostText() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < selectedHosts.size(); i++) {
            if (i == selectedHosts.size() - 1) {
                str.append(selectedHosts.get(i).hostId);
//                str = str + selectedHosts.get(i).hostname;
            } else {
                str.append(selectedHosts.get(i).hostId).append(", ");
//                str = str + selectedHosts.get(i).hostname + ", ";
            }

        }
        hostsList.setText(str.toString());
    }

    private JSONObject getRequestparams() {
        JSONObject requestData = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            requestData.put("request_id", Dmoat.getNewRequestId());
            requestData.put("app_id", pref.getString("app_id", ""));
            requestData.put("token", UserManager.getInstance().getLoggedInUser().token);

            Log.d("getConnectedHostparams", String.valueOf(requestData));
//            requestData.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestData;
    }

//set selected days in new layout
    private void setSelectedDays(int[] daysList){
        if (daysList[0] == 1){
            btnMon.setBackground(getContext().getResources().getDrawable(R.drawable.day_selected));
        }
        if (daysList[1] == 1){
            btnTue.setBackground(getContext().getResources().getDrawable(R.drawable.day_selected));
        }
        if (daysList[2] == 1){
            btnWed.setBackground(getContext().getResources().getDrawable(R.drawable.day_selected));
        }
        if (daysList[3] == 1){
            btnThu.setBackground(getContext().getResources().getDrawable(R.drawable.day_selected));
        }
        if (daysList[4] == 1){
            btnFri.setBackground(getContext().getResources().getDrawable(R.drawable.day_selected));
        }
        if (daysList[5] == 1){
            btnSat.setBackground(getContext().getResources().getDrawable(R.drawable.day_selected));
        }
        if (daysList[6] == 1){
            btnSun.setBackground(getContext().getResources().getDrawable(R.drawable.day_selected));
        }
    }

    private void saveDataInSP(){

        editor.putString("policyTitle", title.getText().toString());
        editor.putString("policyDays", "logged");
        editor.putString("policyStartTime", startHours + ":" + startMins);
        editor.putString("policyEndTime", endHours + ":" + endMins);
        editor.putString("policyProtocols", "logged");
        editor.putString("policyHosts", "logged");
        editor.putString("policyUrls", "logged");

    }
}
