package hardware.prytex.io.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.activity.AuthenticationActivity;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.MyToast;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;

/**
 * Created by khawarraza on 11/11/2016.
 */
@SuppressWarnings("ALL")
public class ChangePasswordFragment extends BaseFragment {


    private TextInputEditText oldPassword;
    private TextInputEditText newPassword;
    private TextInputEditText repeatPassword;
    private ProgressDialog dialog;
    private String token = "";
    private String username = "";

    private Boolean isSetNewPassword = false;
    private Button btnSavePass;
    public static boolean isFromProfile = false;
    private SharedPreferences pref;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_change_password;
    }

    @Override
    public String getTitle() {
        return "Change Password";
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);
        pref =getActivity().getSharedPreferences("MyPref", 0);

        TextInputLayout inputLayoutOldPassword = parent.findViewById(R.id.input_layout_password);
        oldPassword = parent.findViewById(R.id.et_old_password);
        newPassword = parent.findViewById(R.id.input_new_password_txt);
        repeatPassword = parent.findViewById(R.id.input_repeat_password_txt);
        TextView tvHeading = parent.findViewById(R.id.txt_sign_up);
        if (isFromProfile){
            tvHeading.setVisibility(View.GONE);
            imgInfoBtn.setVisibility(View.GONE);
        }else{
            tvHeading.setVisibility(View.VISIBLE);
        }


        btnSavePass = parent.findViewById(R.id.save_my_password);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            token = bundle.getString("token");
            username = bundle.getString("username");
            oldPassword.setText("dummyPassword");
            inputLayoutOldPassword.setVisibility(View.GONE);
            isSetNewPassword = bundle.getBoolean("set_new_password", false);
        }


        parent.findViewById(R.id.save_my_password).setOnClickListener(v -> {
            String old = oldPassword.getText().toString();
            String newP = newPassword.getText().toString();
            String repP = repeatPassword.getText().toString();

            if (!validateFields(old, newP, repP))
                return;

            byte[] dataPwOld;
            byte[] dataPW;
            byte[] dataPWCnfrm;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                dataPwOld = old.getBytes(StandardCharsets.UTF_8);
                dataPW = newP.getBytes(StandardCharsets.UTF_8);
                dataPWCnfrm = repP.getBytes(StandardCharsets.UTF_8);
            }else{
                dataPwOld = old.getBytes(Charset.forName("UTF-8"));
                dataPW = newP.getBytes(Charset.forName("UTF-8"));
                dataPWCnfrm = repP.getBytes(Charset.forName("UTF-8"));
            }

            String base64PassOld = Base64.encodeToString(dataPwOld, Base64.NO_WRAP);
            String base64Pass = Base64.encodeToString(dataPW, Base64.NO_WRAP);
            String base64PassCnfrm = Base64.encodeToString(dataPWCnfrm, Base64.NO_WRAP);
            Log.d("Base64PwdReQ", base64Pass);


            if (isSetNewPassword) {
                btnSavePass.setBackgroundColor(getResources().getColor(R.color.colorPrimay));
                JSONObject passwordData = new JSONObject();

                try {
                    passwordData.put("app_id", pref.getString("app_id", ""));
                    passwordData.put("request_id", Dmoat.getNewRequestId());
                    JSONObject data = new JSONObject();
                    data.put("username", username);
                    data.put("newpassword", base64Pass);
                    data.put("confirmpassword", base64PassCnfrm);
                    data.put("accesstoken", token);
                    passwordData.put("data", data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                showProgressDialog(true);
                Api.setNewPassword(getActivity(), passwordData, mForgetPasswordListener);
            } else {
//                    JSONObject passwordData = UserManager.getInstance().getUserJSON();
                JSONObject passwordData = new JSONObject();
                try {
                    passwordData.put("app_id", pref.getString("app_id", ""));
                    passwordData.put("request_id", Dmoat.getNewRequestId());
                    passwordData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
                    passwordData.put("token", UserManager.getInstance().getLoggedInUser().token);
                    passwordData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
                    JSONObject data = new JSONObject();
                    data.put("old_password", base64PassOld);
                    data.put("new_password", base64Pass);
                    data.put("confirm_password", base64PassCnfrm);
                    passwordData.put("data", data);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                showProgressDialog(true);
                Api.modifyPassword(getActivity(), passwordData, mChangePasswordListener);
            }
        });
    }

    private boolean validateFields(String oldPas, String newPas, String repPas) {
        if (oldPassword.getText().toString().length() == 0) {
            oldPassword.setError("Old Password was empty");
            return false;
        }
        if (newPassword.getText().toString().length() == 0) {
            newPassword.setError("Passwords should be at least 8 characters and include a minimum of 1 upper case, 1 lower case, 1 digit, and 1 special character");
            return false;
        }
        if (newPassword.getText().toString().length() < 8) {
            newPassword.setError("Passwords should be at least 8 characters and include a minimum of 1 upper case, 1 lower case, 1 digit, and 1 special character");
            return false;
        }
        if (isValidPassword(newPassword.getText().toString())) {
            newPassword.setError("Passwords should be at least 8 characters and include a minimum of 1 upper case, 1 lower case, 1 digit, and 1 special character");
            return false;
        }
        if (repeatPassword.getText().toString().length() == 0) {
            repeatPassword.setError("Retype password was empty");
            return false;
        }
        if (!newPassword.getText().toString().equals(repeatPassword.getText().toString())) {
            newPassword.setError("Passwords do not match. Please try again.");
            newPassword.setText("");
            repeatPassword.setText("");
            newPassword.setFocusable(true);
            return false;
        }
        if (oldPas.equalsIgnoreCase(newPas)) {
            newPassword.setError("New Password should not same as old password.");
        }
        if (!(newPas.equalsIgnoreCase(repPas))) {
            newPassword.setError("New Password and repeat password are not same.");
        }
        return true;
    }

    private final AsyncTaskListener mChangePasswordListener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            btnSavePass.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            if (!isAdded()) {
                return;
            }
            showProgressDialog(false);
            if (result.code == 200 || result.code == 2) {
                MyToast.showMessage(getActivity(), "Password changed!");
                Intent i = AuthenticationActivity.startNewIntent(getActivity());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(i);
                getActivity().finish();
            } else {
                if (result.code == 400 || result.code == 4)
                    MyToast.showMessage(getActivity(), result.message);
                else if (result.code == 503)
                    showAlertMsgDialog(getActivity(), result.message);
                else if (result.code == 409)
                    showAlertMsgDialog(getActivity(), result.message);
                else
                    MyToast.showMessage(getActivity(), result.message);
            }

        }
    };

    private final AsyncTaskListener mForgetPasswordListener = result -> {
        if (!isAdded()) {
            return;
        }
        showProgressDialog(false);
        if (result.code == 200 || result.code == 2) {
            if (result.message.equalsIgnoreCase("")) {
                result.message = "Password changed successfully";
            }
            MyToast.showMessage(getActivity(), result.message);
//                getHelper().onBack();
            getHelper().addFragment(new LoginFragment(), true, false);

        } else {
            if (result.message.equalsIgnoreCase("")) {
                result.message = "Unable to modify password. Please try again later";
            }
            if (result.code == 400 || result.code == 4)
                MyToast.showMessage(getActivity(), result.message);
            else if (result.code == 503)
                showAlertMsgDialog(getActivity(), result.message);
            else if (result.code == 409)
                showAlertMsgDialog(getActivity(), result.message);
            else
                MyToast.showMessage(getActivity(), result.message);
        }

    };

    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }
}
