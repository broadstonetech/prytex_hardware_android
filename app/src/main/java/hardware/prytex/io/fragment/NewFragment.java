//package com.prytex.io.fragment;
//
//import android.content.Context;
//import android.graphics.Color;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Looper;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ListView;
//import android.widget.TextView;
//
//import com.prytex.io.Dmoat;
//import com.prytex.io.R;
//import com.prytex.io.adapter.DashboardAlertsAdapter;
//import com.prytex.io.chart_config.BarYAxisValueFormatter;
//import com.prytex.io.config.Constants;
//import com.prytex.io.fragment.AlertsTabs.AlertsMainActivity;
//import com.prytex.io.fragment.BandwidthFrags.BandwidthChartFrags.ProtocolChartFragment;
//import com.prytex.io.fragment.CnctdDevicesTabBars.ConnectedDeviceActivity;
//import com.prytex.io.fragment.SensorTabBars.SensorMainFragment;
//import com.prytex.io.model.UserManager;
//import com.prytex.io.network.Api;
//import com.prytex.io.network.AsyncTaskListener;
//import com.prytex.io.network.TaskResult;
//import com.prytex.io.util.MyToast;
//import com.github.mikephil.charting.charts.BarChart;
//import com.github.mikephil.charting.charts.PieChart;
//import com.github.mikephil.charting.components.Legend;
//import com.github.mikephil.charting.components.XAxis;
//import com.github.mikephil.charting.components.YAxis;
//import com.github.mikephil.charting.data.BarData;
//import com.github.mikephil.charting.data.BarDataSet;
//import com.github.mikephil.charting.data.BarEntry;
//import com.github.mikephil.charting.data.PieData;
//import com.github.mikephil.charting.data.PieDataSet;
//import com.github.mikephil.charting.data.PieEntry;
//import com.github.mikephil.charting.formatter.IAxisValueFormatter;
//import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
//import com.github.mikephil.charting.formatter.LargeValueFormatter;
//import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//
//import static com.prytex.io.parser.AlertsDashboardParser.listOfAllViewAlertsForDashboard;
//import static com.prytex.io.parser.BandwidthDashboardParser.listOfBandwidthForDashboard;
//
//public class NewFragment extends BaseFragment {
//
//
//    RecyclerView mRecyclerView;
//    DashboardAlertsAdapter mAdapter;
//    LinearLayoutManager mLayoutManager;
//    private float[] yData;// = {35f, 20, 45};
//    private String[] xData = {"New Alerts", "Blocked" , "Muted"};
//    PieChart pieChart;
//    BarChart barchart;
//    public static int OpenAlertsCount = 0;
//    public static int BlockedAlertsCount = 0;
//    public static int MutedAlertsCount = 0;
//    public static int ConnectedDevicesCount = 0;
//    public static int TemperatureValue = 0;
//    public static int HumidityValue = 0;
//    public static int AirQualityValue = 0;
//    public static boolean isAlertsCall = false;
//    public static boolean isConnectedDevicesCall = false;
//    public static boolean isBandwidthCall = false;
//    public static boolean isSensorsCall = false;
//
//    private TextView tvConnectedDevicesCount, tvTemp, tvHumidity, tvAirQuality;
//    Button btnAlertsDetail, btnCnctdDevicesDetail, btnBandwidthDetail, btnSensorsDetail;
//    private ListView lvNewAlerts, lvBandwith;
//
//    @Override
//    public int getLayoutId() {
//        return R.layout.new_dashboard;
//    }
//
//    @Override
//    public void initViews(View parent, Bundle savedInstanceState) {
//        super.initViews(parent, savedInstanceState);
//
//
//        mRecyclerView = (RecyclerView) parent.findViewById(R.id.listNewAlerts);
//        mLayoutManager = new LinearLayoutManager(getActivity());
//        mRecyclerView.setLayoutManager(mLayoutManager);
//        pieChart = (PieChart) parent.findViewById(R.id.donutchart);
//        barchart = (BarChart) parent.findViewById(R.id.bandwidthchart);
//
//        tvConnectedDevicesCount = (TextView) parent.findViewById(R.id.tvConnectedDevicesCount);
//        tvTemp = (TextView) parent.findViewById(R.id.tvTemp);
//        tvHumidity = (TextView) parent.findViewById(R.id.tvHumidity);
//        tvAirQuality = (TextView) parent.findViewById(R.id.tvAirQuality);
//
//        btnAlertsDetail = (Button) parent.findViewById(R.id.btnAlerttsDetail);
//        btnCnctdDevicesDetail = (Button) parent.findViewById(R.id.btnCnctdDevicesDetail);
//        btnBandwidthDetail = (Button) parent.findViewById(R.id.btnBandwidthDetail);
//        btnSensorsDetail = (Button) parent.findViewById(R.id.btnSensorsDetail);
//
//        btnAlertsDetail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getHelper().addFragment(new AlertsMainActivity(), true, true);
//            }
//        });
//        btnCnctdDevicesDetail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getHelper().addFragment(new ConnectedDeviceActivity(), true, true);
//            }
//        });
//        btnBandwidthDetail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getHelper().addFragment(new ProtocolChartFragment(), true, true);
//            }
//        });
//        btnSensorsDetail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getHelper().addFragment(new SensorMainFragment(), true, true);
//            }
//        });
////        barchart.getDescription().setEnabled(false);
////        barchart.setDescription(null);
////        barchart.getLegend().setEnabled(false);
//        runMultipleAsyncTaskA();
//        runMultipleAsyncTaskB();
////        animatePieChart();
//
//    }
//
//    @Override
//    public String getTitle() {
//        return "dashboard";
//    }
//
//    private void animatePieChart() {
//        pieChart.setDescription(null);
//        pieChart.setRotationEnabled(true);
//        //pieChart.setUsePercentValues(true);
//        //pieChart.setHoleColor(Color.BLUE);
//        //pieChart.setCenterTextColor(Color.BLACK);
////        pieChart.setHoleRadius(25f);
//        pieChart.setTransparentCircleAlpha(0);
////        pieChart.setCenterText("Super Cool Chart");
//        pieChart.setCenterTextSize(10);
//
////        pieChart.getDescription().setEnabled(false);
////        pieChart.setDescription(null);
////        pieChart.setDrawEntryLabels(true);
//        pieChart.getLegend().setEnabled(false);
//        //pieChart.setEntryLabelTextSize(20);
//        //More options just check out the documentation!
//
//        addDataSet();
//
//        pieChart.animateY(1000);
//    }
//    private void animateBandwidthChart() {
//        //bar chart for bandwidth
//        ArrayList xVals = new ArrayList();
//        ArrayList yVals = new ArrayList();
//
//        for (int i = 0; i < listOfBandwidthForDashboard.size(); i++) {
//            String hName = listOfBandwidthForDashboard.get(i).getHostName();
//            String bar;
//            if (hName.length() > 6) {
//                bar = hName.substring(0, 7);
//            } else {
//                bar = hName;
//            }
//            xVals.add(bar);
//            int tUsg = listOfBandwidthForDashboard.get(i).getTotalUsage();
//            tUsg = tUsg / 1024;
//            if (tUsg<1){
//                tUsg = 1;
//            }else{
//                tUsg = tUsg;
//            }
//            yVals.add(new BarEntry(i + 1, (float) tUsg));
//
//        }
//
//        BarDataSet set1;
//        set1 = new BarDataSet(yVals, "");
//        set1.setDrawIcons(false);
//        set1.setColors(new int[]{R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimay}, getContext());
//
//        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
//        dataSets.add(set1);
////
//        BarData data = new BarData(dataSets);
//        data.setValueTextSize(10f);
////        data.setValueTypeface(mTfLight);
////        data.setBarWidth(barWidth);
//
////        mChart.setData(data);
//        data.setValueFormatter(new LargeValueFormatter());
//        barchart.setData(data);
//        barchart.moveViewTo(data.getEntryCount() - 7, 50f, YAxis.AxisDependency.LEFT);
//
////        Legend l = mChart.getLegend();
////        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
////        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
////        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
////        l.setDrawInside(true);
////        l.setTextSize(0f);
//
//        //X-axis
//        XAxis xAxis = barchart.getXAxis();
////        xAxis.setDrawGridLines(false);
//        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setValueFormatter(new IndexAxisValueFormatter(xVals));
////        xAxis.setAvoidFirstLastClipping(true); abcd
//
//        //Y-axis
//        IAxisValueFormatter custom = new BarYAxisValueFormatter();
//
//        barchart.getAxisRight().setEnabled(false);
//        YAxis leftAxis = barchart.getAxisLeft();
////        leftAxis.setValueFormatter(custom);
////        leftAxis.setSpaceTop(35f);
//        leftAxis.setAxisMinimum(0f);
//
//
//
//
////        set1.setDrawIcons(false);
////        dataSet.setColors(new int[]{R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimay}, getContext());
////
////        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
////        dataSets.add(dataSet);
//
////        BarData data = new BarData(dataSets);
////        data.setValueTextSize(10f);
//////        data.setValueTypeface(mTfLight);
//////        data.setBarWidth(barWidth);
////
//////        mChart.setData(data);
////        data.setValueFormatter(new LargeValueFormatter());
////        barchart.setData(data);
////        barchart.getDescription().setEnabled(false);
////        barchart.setDescription(null);
//////        pieChart.setDrawEntryLabels(true);
////        barchart.getLegend().setEnabled(false);
////        //X-axis
////        XAxis xAxis = barchart.getXAxis();
//////        xAxis.setDrawGridLines(false);
////        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
////        xAxis.setValueFormatter(new IndexAxisValueFormatter(labels));
//////        xAxis.setAvoidFirstLastClipping(true); abcd
////
////        //Y-axis
////        IAxisValueFormatter custom = new BarYAxisValueFormatter();
////
////        barchart.getAxisRight().setEnabled(false);
////        YAxis leftAxis = barchart.getAxisLeft();
//////        leftAxis.setValueFormatter(custom);
//////        leftAxis.setSpaceTop(35f);
////        leftAxis.setAxisMinimum(0f);
//
////        leftAxis.setStartAtZero(true);
////        leftAxis.setAxisMaxValue(1);
//        barchart.animateY(1000);
//    }
//    private void addDataSet() {
//        ArrayList<PieEntry> yEntrys = new ArrayList<>();
//        ArrayList<String> xEntrys = new ArrayList<>();
//
//        for (int i=0; i < yData.length; i++){
//        yEntrys.add(new PieEntry(yData[i], 0));
//        }
//
//        for(int i = 1; i < xData.length; i++){
//            xEntrys.add(xData[i]);
//        }
//
//        //create the data set
//        PieDataSet pieDataSet = new PieDataSet(yEntrys, "");
//        pieDataSet.setSliceSpace(2);
//        pieDataSet.setValueTextSize(12);
//
//        //add colors to dataset
//        ArrayList<Integer> colors = new ArrayList<>();
//        colors.add(Color.GRAY);
//        colors.add(Color.BLUE);
//        colors.add(Color.RED);
//        colors.add(Color.GREEN);
//        colors.add(Color.CYAN);
//        colors.add(Color.YELLOW);
//        colors.add(Color.MAGENTA);
//
//        pieDataSet.setColors(colors);
//
//        //add legend to chart
//        Legend legend = pieChart.getLegend();
//        legend.setForm(Legend.LegendForm.CIRCLE);
//        legend.setPosition(Legend.LegendPosition.LEFT_OF_CHART);
//
//        //create pie data object
//        PieData pieData = new PieData(pieDataSet);
//        pieChart.setData(pieData);
//        pieChart.invalidate();
//    }
//
//    private void runMultipleAsyncTaskA() // Run Multiple Async Task
//    {
//        AlertsAsyncTask asyncTask = new AlertsAsyncTask(); // First
//
//            asyncTask.execute();
//
//        ConnectedDevicesAsyncTask asyncTask2 = new ConnectedDevicesAsyncTask(); // Second
//
//            asyncTask2.execute();
//
//    }
//    private void runMultipleAsyncTaskB() // Run Multiple Async Task
//    {
//
//        SensorsAsyncTask asyncTask = new SensorsAsyncTask(); // Second
//
//        asyncTask.execute();
//
//        BandwidthAsyncTask asyncTask2 = new BandwidthAsyncTask();
//        asyncTask2.execute();
//
//
//
//    }
//    //Start First Async Task:
//    private class AlertsAsyncTask extends AsyncTask<Void, Void, Void>
//    {
//        @Override
//        protected void onPreExecute()
//        {
//            Log.i("AsyncTask" ,"FirstOnPreExecute()");
//        }
//        @Override
//        protected Void doInBackground(Void... params)
//        {
//
//                Log.i("AsyncTask" ,"AlertsAsyncTask");
//
//                Handler mainHandler = new Handler(Looper.getMainLooper());
//                Runnable myRunnable = new Runnable() {
//                    @Override
//                    public void run() {
//                        //Code that uses AsyncHttpClient in your case ConsultaCaract()
//                        isAlertsCall = true;
//                        Api.getAlertsDashboard(getContext(), getParams(), mListener);
//                    }
//                };
//                mainHandler.post(myRunnable);
//
//            //call alerts get api
////            Thread.sleep(100); //call connected devices count api
//
//
//            return null;
//        }
//        @Override
//        protected void onPostExecute(Void result)
//        {
//            Log.d("AsyncTask" ,"FirstonPostExecute()");
//        }
//    }
//    //Start Second Async Task:
//    private class ConnectedDevicesAsyncTask extends AsyncTask<Void, Void, Void>
//    {
//        @Override
//        protected void onPreExecute()
//        {
//            Log.i("AsyncTask" ,"SecondOnPreExecute()");
//        }
//        @Override
//        protected Void doInBackground(Void... params)
//        {
//
//                Log.d("AsyncTask" ,"ConnectedDevicesAsyncTask");
//            //call connected devices count api
//
//                    Handler mainHandler = new Handler(Looper.getMainLooper());
//                    Runnable myRunnable = new Runnable() {
//                        @Override
//                        public void run() {
//                            //Code that uses AsyncHttpClient in your case ConsultaCaract()
//                            isConnectedDevicesCall = true;
//                            Api.getConnectedDevicesDashboard(getContext(), getParams(), mListener);
//                        }
//                    };
//                    mainHandler.post(myRunnable);
//
//            return null;
//        }
//        @Override
//        protected void onPostExecute(Void result)
//        {
//            Log.d("AsyncTask" ,"SecondOnPostExecute()");
//        }
//    }
//
//    //Start Sensor Async Task:
//    private class SensorsAsyncTask extends AsyncTask<Void, Void, Void>
//    {
//        @Override
//        protected void onPreExecute()
//        {
//            Log.i("AsyncTask" ,"FirstOnPreExecute()");
//        }
//        @Override
//        protected Void doInBackground(Void... params)
//        {
//
//            Log.i("AsyncTask" ,"AlertsAsyncTask");
//
//            Handler mainHandler = new Handler(Looper.getMainLooper());
//            Runnable myRunnable = new Runnable() {
//                @Override
//                public void run() {
//                    //Code that uses AsyncHttpClient in your case ConsultaCaract()
//                    isSensorsCall = true;
//                    Api.getSensorValuesDashboard(getContext(), getParams(), mListenerB);
//                }
//            };
//            mainHandler.post(myRunnable);
//
//            //call alerts get api
////            Thread.sleep(100); //call connected devices count api
//
//
//            return null;
//        }
//        @Override
//        protected void onPostExecute(Void result)
//        {
//            Log.d("AsyncTask" ,"FirstonPostExecute()");
//        }
//    }
//    //Start bandwidth Async Task:
//    private class BandwidthAsyncTask extends AsyncTask<Void, Void, Void>
//    {
//        @Override
//        protected void onPreExecute()
//        {
//            Log.i("AsyncTask" ,"FirstOnPreExecute()");
//        }
//        @Override
//        protected Void doInBackground(Void... params)
//        {
//
//            Log.i("AsyncTask" ,"AlertsAsyncTask");
//
//            Handler mainHandler = new Handler(Looper.getMainLooper());
//            Runnable myRunnable = new Runnable() {
//                @Override
//                public void run() {
//                    //Code that uses AsyncHttpClient in your case ConsultaCaract()
//                    isBandwidthCall = true;
//                    Api.getBandwidthDashboard(getContext(), getParams(), mListenerB);
//                }
//            };
//            mainHandler.post(myRunnable);
//
//            //call alerts get api
////            Thread.sleep(100); //call connected devices count api
//
//
//            return null;
//        }
//        @Override
//        protected void onPostExecute(Void result)
//        {
//            Log.d("AsyncTask" ,"FirstonPostExecute()");
//        }
//    }
//
//    private JSONObject getParams() {
//        JSONObject js = new JSONObject();
//        try {
//            js.put("app_id", getActivity().getSharedPreferences(Constants.APP_ID_PREFS_NAME, Context.MODE_PRIVATE)
//                    .getString("app_id", ""));
//            js.put("request_id", Dmoat.getNewRequestId());
//            js.put("token", UserManager.getInstance().getLoggedInUser().token);
//            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
//            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return js;
//    }
//
//    // listener
//    AsyncTaskListener mListener = new AsyncTaskListener() {
//        @Override
//        public void onComplete(TaskResult result) {
//            Log.d("AsyncTaskResponse" ,"response");
//
//            if (result.code == 200 || result.code == 2) {
//                Log.d("AsyncTask" ,"success");
//                    if (isAlertsCall){
//                        yData = new float[]{OpenAlertsCount, BlockedAlertsCount, MutedAlertsCount};
//                        animatePieChart();
//                        mAdapter = new DashboardAlertsAdapter(getContext(), mRecyclerView, listOfAllViewAlertsForDashboard);
//                        mRecyclerView.setAdapter(mAdapter);
//                        isAlertsCall = false;
//                    }else if (isConnectedDevicesCall){
//                        tvConnectedDevicesCount.setText(String.valueOf(ConnectedDevicesCount));
//                        isConnectedDevicesCall = false;
//                    }else{}
//
////                MyToast.showMessage(getContext(), "successfuly received data");
//            } else {
//                MyToast.showMessage(getActivity(), getResources().getString(R.string.error_message_generic));
//            }
//        }
//    };
//    // listenerB
//    AsyncTaskListener mListenerB = new AsyncTaskListener() {
//        @Override
//        public void onComplete(TaskResult result) {
//            Log.d("AsyncTaskResponse" ,"response");
//
//            if (result.code == 200 || result.code == 2) {
//                Log.d("AsyncTask" ,"success");
//                    if (isSensorsCall){
//                        tvTemp.setText("Sensor Value for Temperature is " + String.valueOf(TemperatureValue));
//                        tvHumidity.setText("Sensor Value for Humidity is " + String.valueOf(HumidityValue));
//                        tvAirQuality.setText("Sensor Value for Air quality is " + String.valueOf(AirQualityValue));
//                        isSensorsCall = false;
//
//                    }else if (isBandwidthCall){
//                        isBandwidthCall = false;
//                        animateBandwidthChart();
//                        barchart.invalidate();
//                    }else{}
//
////                MyToast.showMessage(getContext(), "successfuly received data");
//            } else {
//                MyToast.showMessage(getActivity(), getResources().getString(R.string.error_message_generic));
//            }
//        }
//    };
//}
