package hardware.prytex.io.fragment.AlertsTabs;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.activity.AlertsMainActivity;
import hardware.prytex.io.adapter.MutedAlertsAdapter;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.model.MutedBlockedAlertsModel;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.GeneralUtils;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;
import static hardware.prytex.io.activity.AlertsMainActivity.isFirstCallMutedAlerts;
import static hardware.prytex.io.activity.AlertsMainActivity.tabLayout;
import static hardware.prytex.io.parser.MutedAlertsParser.listOfMutedAlerts;

/**
 * Created by khawarraza on 10/11/2016.
 */

@SuppressWarnings("ALL")
public class MutedAlertsFragment extends BaseFragment {

    private ProgressDialog dialog;
    private MutedAlertsAdapter mAdapter;
    private RecyclerView mRecyclerView;
    //    TextView tvMutedAlertsCount;
    private SharedPreferences pref;
    private String appId = "";

    @Override
    public int getLayoutId() {
        return R.layout.muted_alert_fragment;
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);

        appId = pref.getString("app_id", "");
        LinearLayout container = parent.findViewById(R.id.alerts_container);
//        tvMutedAlertsCount = (TextView) parent.findViewById(R.id.tvAlertsCount);
        mRecyclerView = parent.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        imgInfoBtn.setVisibility(View.VISIBLE);

        if (GeneralUtils.isConnected(getContext())) {
            if (isFirstCallMutedAlerts) {
                showProgressDialog(true);
                Api.getMutedAlerts(getActivity(), getRequestParams(), listener);
            } else {

                if (listOfMutedAlerts.size() > 0) {
                    tabLayout.getTabAt(2).setText("Muted Alerts" + " (" + listOfMutedAlerts.size() + ")");
                    mAdapter = new MutedAlertsAdapter(getContext(), listOfMutedAlerts, new ArrayList<>(), "muted_fragment", appId);
                    mRecyclerView.setAdapter(mAdapter);
                } else {
                    getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
                }
            }

//            Api.getPreMutedAlerts(getActivity(), getRequestParams(), listenerPREMUTED);
        } else {
            showAlertMsgDialog(getContext(), getResources().getString(R.string.internet_connection));
        }

//        tvMutedAlertsCount.setText(listOfMutedAlerts.size());
        //material spinner
        MaterialSpinner spinner = parent.findViewById(R.id.spinner);
        spinner.setItems("All Alerts", "New Host", "IDS Alert");
        spinner.setOnItemSelectedListener((MaterialSpinner.OnItemSelectedListener<String>) (view, position, id, item) -> {
//                Snackbar.make(view, "Clicked " + item + position, Snackbar.LENGTH_LONG).show();
            updateAlertsFilter(item);
        });


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
//        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "muted_alerts", null /* class override */);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, "muted_alerts");
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, this.getClass().getName());

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);
    }

    @Override
    public String getTitle() {
        return "Muted Alerts";
    }

    private final AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (!isAdded()) {
                return;
            }
            showProgressDialog(false);
            tabLayout.getTabAt(2).setText("Muted Alerts" + " (0)");
            if (result.code == 200) {
                if (result.message.equalsIgnoreCase("No data detected")) {
                    getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
//                    showAlertMsgDialog(getContext(), "No alerts were detected.");
                } else {
                    getParentView().findViewById(R.id.error_label).setVisibility(View.GONE);
                    AlertsMainActivity.filterCountMuted = listOfMutedAlerts.size();
                    tabLayout.getTabAt(2).setText("Muted Alerts" + " (" + listOfMutedAlerts.size() + ")");
                    mAdapter = new MutedAlertsAdapter(getContext(), listOfMutedAlerts, new ArrayList<>(), "muted_fragment", appId);
                    mRecyclerView.setAdapter(mAdapter);
                }
            } else if (result.code == 503) {
                getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
                showAlertMsgDialog(getContext(), result.message);
            } else if (result.code == 409) {
                getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
                showAlertMsgDialog(getContext(), result.message);
            } else {
                getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
            }

        }
    };

    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private JSONObject getRequestParams() {
        JSONObject requestData = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            requestData.put("request_id", Dmoat.getNewRequestId());
            requestData.put("app_id", pref.getString("app_id", ""));
            requestData.put("token", UserManager.getInstance().getLoggedInUser().token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestData;
    }

    private void updateAlertsFilter(String item) {
        ArrayList<MutedBlockedAlertsModel> filteredListOfAlerts = new ArrayList<>();
        for (int i = 0; i < listOfMutedAlerts.size(); i++) {

            if (item == "New Host") {
                if (listOfMutedAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.NEW_HOST_ALERT)) {
                    filteredListOfAlerts.add(listOfMutedAlerts.get(i));
                } else {
                }
            } else if (item == "Sensor") {
                if (listOfMutedAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.SENSOR_ALERT)) {
                    filteredListOfAlerts.add(listOfMutedAlerts.get(i));
                } else {
                }
            } else if (item == "Prytex Hardware Info") {
                if (listOfMutedAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.INFO_ALERT)) {
                    filteredListOfAlerts.add(listOfMutedAlerts.get(i));
                } else {
                }
            } else if (item == "Blocked Ips") {
                if (listOfMutedAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.BLOCK_IPS_ALERT)) {
                    filteredListOfAlerts.add(listOfMutedAlerts.get(i));
                } else {
                }
            } else if (item == "IDS Alert") {
                if (listOfMutedAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.IDS_ALERT)) {
                    filteredListOfAlerts.add(listOfMutedAlerts.get(i));
                } else {
                }
            } else if (item == "Flow Alerts") {
                if (listOfMutedAlerts.get(i).getInput_src().equalsIgnoreCase(MutedBlockedAlertsModel.FLOW_ALERT)) {
                    filteredListOfAlerts.add(listOfMutedAlerts.get(i));
                } else {
                }
            } else if (item == "All Alerts") {
                filteredListOfAlerts.add(listOfMutedAlerts.get(i));
            }

            AlertsMainActivity.filterCountMuted = filteredListOfAlerts.size();
            mAdapter = new MutedAlertsAdapter(getContext(), filteredListOfAlerts, new ArrayList<>(), "muted_fragment", appId);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

}
