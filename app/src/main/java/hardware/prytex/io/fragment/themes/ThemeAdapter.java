package hardware.prytex.io.fragment.themes;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.util.UserPrefs;

import java.util.ArrayList;
import java.util.List;

public class ThemeAdapter extends ArrayAdapter {
    final List<String> list;
    final Context context;

    final int[] online_themes = {};
    final int[] offline_themes = {};
    ArrayList<Boolean> cb_value;

    public ThemeAdapter(@NonNull Context context, List<String> list) {
        super(context, R.layout.listview_themes,list);
        this.context = context;
        this.list = list;
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View v = inflater.inflate(R.layout.listview_themes, parent,
//                false);

        ViewHolder holder;
        cb_value = new ArrayList<>();

        if(convertView==null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.listview_themes,null);
            holder.txt1 = (TextView) convertView.findViewById(R.id.tv_theme_title);
            holder.linearLayout = (LinearLayout)convertView.findViewById(R.id.ll_themes);
            holder.chkbox = (CheckBox) convertView.findViewById(R.id.cb_themes);
            holder.online = (ImageView)convertView.findViewById(R.id.online_theme);
            holder.offline = (ImageView)convertView.findViewById(R.id.offline_theme);
            holder.chkbox.setClickable(false);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        for (int i=0;i<list.size();i++){
            if (i== UserPrefs.getTheme(context)){
                cb_value.add(true);
            }else{
                cb_value.add(false);
            }
        }

        holder.txt1.setText(list.get(position));
        holder.chkbox.setChecked(cb_value.get(position));
        holder.online.setImageResource(online_themes[position]);
        holder.offline.setImageResource(offline_themes[position]);
        holder.linearLayout.setOnClickListener(v -> {
            for (int i=0;i<list.size();i++){
                if(i==position){
                    cb_value.add(i,true);
                    UserPrefs.setTheme(context,i);
                }else{
                    cb_value.add(i,false);
                }
            }
            notifyDataSetChanged();
        });



//        theme_name = v.findViewById(R.id.tv_theme_title);
//        theme = v.findViewById(R.id.ll_themes);
//        cb_theme = v.findViewById(R.id.cb_themes);
//
//
//        theme_name.setText(list.get(position));
//
//        checkedTheme = UserPrefs.getTheme(context);
//
//        for (int i=0; i<list.size();i++){
//            if ()
//        }
//
//        theme.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });



        return convertView;
    }
    static class ViewHolder{
        TextView txt1;
        LinearLayout linearLayout;
        CheckBox chkbox;
        ImageView offline,online;
    }
}
