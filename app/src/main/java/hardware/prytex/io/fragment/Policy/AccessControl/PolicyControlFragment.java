package hardware.prytex.io.fragment.Policy.AccessControl;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.activity.DashBoardActivity;
import hardware.prytex.io.adapter.PoliciesControlAdapter;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.fragment.DashBoardFragment;
import hardware.prytex.io.fragment.Policy.PoliciesMainFragment;
import hardware.prytex.io.model.PolicyPlanModel;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static hardware.prytex.io.parser.PoliciesListParser.listOfPolicies;


@SuppressWarnings("ALL")
public class PolicyControlFragment extends BaseFragment implements View.OnClickListener {

    private TextView tvNoPOilicy;
    private ProgressDialog dialog;
    private RecyclerView mRecyclerView;
    ArrayList<PolicyPlanModel> preDefPoliylist = new ArrayList<>();
    private final PolicyPlanModel objPreDefPolicy = new PolicyPlanModel();
    private final PolicyPlanModel objPreDefPolicy2 = new PolicyPlanModel();
    private SharedPreferences pref;
    private SearchView searchViewPolicies;
    PoliciesControlAdapter mAdapter;
    @Override
    public int getLayoutId() {
        return R.layout.fragment_policy_control;
    }

    @Override
    public String getTitle() {
        return "Policy";
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);


        mRecyclerView = parent.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Button btnCreatePolicy = parent.findViewById(R.id.tbMakeAplan);
        tvNoPOilicy = parent.findViewById(R.id.tvNoPolicyText);
        searchViewPolicies = parent.findViewById(R.id.searchView);

        btnCreatePolicy.setOnClickListener(this);


        PoliciesMainFragment fragment = new PoliciesMainFragment();

        objPreDefPolicy.setPolicyTitle("Home Work Time");
        objPreDefPolicy.setStartTime("19:00");
        objPreDefPolicy.setStartTimeGMT(timeToGMT("19:00"));
        objPreDefPolicy.setEndTime("21:00");
        objPreDefPolicy.setEndTimeGMT(timeToGMT("21:00"));
        objPreDefPolicy.setPolicyDays(127);
        objPreDefPolicy.setPolicyStatus(0);
        objPreDefPolicy.setPreDefPolicy(true);
        objPreDefPolicy.setDescripton("A pre-populated policy to allows you to set a distraction free time for home work. You can edit and change the start/end time as well as the devices that should be blocked during this time duration. After editing, simply enable or disable this policy.");

        objPreDefPolicy2.setPolicyTitle("Privacy Mode");
        objPreDefPolicy2.setStartTime("09:00");
        objPreDefPolicy2.setStartTimeGMT(timeToGMT("09:00"));
        objPreDefPolicy2.setEndTime("17:00");
        objPreDefPolicy2.setEndTimeGMT(timeToGMT("17:00"));
        objPreDefPolicy2.setPolicyDays(127);
        objPreDefPolicy2.setPolicyStatus(0);
        objPreDefPolicy2.setPreDefPolicy(true);
        objPreDefPolicy2.setDescripton("Do you know that many smart speakers and connected gadgets are always on and listening? You can setup a privacy mode to disallow access by these devices. You can change the start/end time.");


        searchPolicy();

        if (fragment.isBackFromPlan){
            Api.getAllPolicies(getActivity(), getParams(), listener);

        }else{
            Api.getAllPolicies(getActivity(), getParams(), listener);

        }



    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "access_control", null /* class override */);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tbMakeAplan) {//go to policy plan fragment
            //      getHelper().addFragment(new PolicyPlanFragment(), false, true);
            if (DashBoardFragment.ConnectedDevicesCount>0){
                DashBoardActivity.llBottomTabs.setVisibility(View.GONE);
                getHelper().addFragmentwitoutTabbar(new PolicyPlanFragment(), false, true);
            }else{
                DashBoardActivity.showAlertMsgDialog(getActivity(),getResources().getString(R.string.no_connected_devices));
            }

            return;
        }
    }

    private JSONObject getParams() {
        JSONObject js = new JSONObject();
        try {
            js.put("app_id", pref.getString("app_id", ""));
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    private final AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (!isAdded()) {
                return;
            }
            if (result.code == 200) {

                boolean samplePolicy= false;
                boolean samplePolicy2= false;
                for(int i=0; i < listOfPolicies.size();i++)
                {
                    if(listOfPolicies.get(i).getPolicyTitle().equalsIgnoreCase(objPreDefPolicy.getPolicyTitle()))
                    {
                        samplePolicy = true;
                    }

                    if(listOfPolicies.get(i).getPolicyTitle().equalsIgnoreCase(objPreDefPolicy2.getPolicyTitle()))
                    {
                        samplePolicy2 = true;
                    }
                }
                if(!samplePolicy)
                {
                    listOfPolicies.add(objPreDefPolicy);
                }
                if(!samplePolicy2)
                {
                    listOfPolicies.add(objPreDefPolicy2);
                }

                if (listOfPolicies.size() > 0) {
                    tvNoPOilicy.setVisibility(View.GONE);
                    mAdapter = new PoliciesControlAdapter(getContext(), listOfPolicies, mSelectedListener);
                    mRecyclerView.setAdapter(mAdapter);
                }
                else {
                    tvNoPOilicy.setVisibility(View.VISIBLE);
                    if (result.message.equalsIgnoreCase("")){
                        tvNoPOilicy.setText(result.message);
                    }
                }

//                if(preDefPoliylist.size() > 0 && listOfPolicies.size() <=0 )
//                {
//                    tvNoPOilicy.setVisibility(View.GONE);
//                    mAdapter = new PoliciesControlAdapter(getContext(), preDefPoliylist, mSelectedListener);
//                    mRecyclerView.setAdapter(mAdapter);
//                }




            }
        }
    };

    private final PoliciesControlAdapter.OnAlertSelectedListener mSelectedListener = (model, position) -> {

        PolicyPlanFragment fragment = new PolicyPlanFragment();
        fragment.policeyKey = model.getPolicyId();
        fragment.policyTitle = model.getPolicyTitle();
        fragment.policyEndTime = model.getEndTime();
        fragment.policyStartTime = model.getStartTime();
        fragment.policyStartTimeGMT = model.getStartTimeGMT();
        fragment.policyEndTimeGMT = model.getEndTimeGMT();
        fragment.policyDaysWeight = model.getPolicyDays();
        fragment.policyStatus = model.getPolicyStatus();
        fragment.isCreateNewPolicy = false;

        if(fragment.policyStatus ==  1 || (model.policyHostsLists != null && model.policyHostsLists.length() >0))
            fragment.jsonArrSelectedHosts = model.policyHostsLists;
        else
            fragment.isPreDefPolicy = true;


        getHelper().replaceFragment(fragment, false, true);
        Log.d("PolicyPlan", String.valueOf(position));



    };

    private void searchPolicy() {
        View closeButton = searchViewPolicies.findViewById(R.id.search_close_btn);

        EditText et= (EditText) searchViewPolicies.findViewById(R.id.search_src_text);
        et.setHint("Search...");
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchViewPolicies.onActionViewCollapsed();
            }
        });

        searchViewPolicies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchViewPolicies.onActionViewExpanded();
            }
        });
        searchViewPolicies.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    closeButton.performClick();
                }
            }
        });

        searchViewPolicies.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchViewPolicies.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                mAdapter.getFilter().filter(query);
                return false;
            }
        });

    }

    private String timeToGMT(String policyTime)
    {
        String policyTimeGMT ="";
        try {
            @SuppressLint("SimpleDateFormat") DateFormat utcFormat = new SimpleDateFormat("HH:mm");
            utcFormat.setTimeZone(TimeZone.getDefault());
            Date date = utcFormat.parse(policyTime);

            @SuppressLint("SimpleDateFormat") DateFormat deviceFormat = new SimpleDateFormat("HH:mm");
            deviceFormat.setTimeZone(TimeZone.getTimeZone("UTC")); //Device timezone

            policyTimeGMT = deviceFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return policyTimeGMT;
    }
}
