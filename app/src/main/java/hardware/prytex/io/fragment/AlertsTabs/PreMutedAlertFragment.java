package hardware.prytex.io.fragment.AlertsTabs;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.adapter.PreMutedAlertAdapter;
import hardware.prytex.io.adapter.preMutedFlowAdapter;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.model.PreMutedAlertsModel;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.GeneralUtils;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;
import static hardware.prytex.io.activity.AlertsMainActivity.isFirstCallPreMutedAlerts;
import static hardware.prytex.io.activity.AlertsMainActivity.tabLayout;
import static hardware.prytex.io.parser.Pre_MutedAlertsParser.listOfPreMutedAlerts;
import static hardware.prytex.io.parser.Pre_MutedAlertsParser.listOfPreMutedFlowAlerts;


@SuppressWarnings("ALL")
public class PreMutedAlertFragment extends BaseFragment implements View.OnClickListener {

    private ExpandableListView listViewExpandable;
    private PreMutedAlertAdapter preMutedAlertAdapter;
    private preMutedFlowAdapter flowAdapter;
    private TextView error_label;
    private ProgressDialog dialog;
//    private SwipeRefreshLayout mSwipeRefreshLayout;
    private static List<String> listDataHeader;
    private static List<String> listDataHeaderInputSrc;
    private static List<Integer> listDataHeaderTimeStamp;
    private HashMap<String, List<String>> listDataChild;
    private HashMap<String, List<String>> listDataChildDest;
    //    TextView tvPreMutedAlertsCount;
    private SharedPreferences pref;
    private final ArrayList<PreMutedAlertsModel> listOfFlowAlerts = new ArrayList<>();

    private RelativeLayout rlSuperView;
    private RelativeLayout rlSnortView;
    private RecyclerView recyclerViewFlow;
    private int superViewHeight = 0;
    private int childViewHeight = 0;
    private TextView tvSnortHeading;
    private TextView tvFlowheading;
    private RecyclerView mRecyclerViewswip;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    @Override
    public int getLayoutId() {
        return R.layout.fragment_pre_muted_alert;
    }

    @Override
    public String getTitle() {
        return "Muted Alerts";
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);

        imgInfoBtn.setVisibility(View.VISIBLE);
        rlSuperView = parent.findViewById(R.id.rlSuperViewFlowSnort);
        rlSnortView = parent.findViewById(R.id.rlSnortView);
        error_label = parent.findViewById(R.id.error_label);
        listViewExpandable = parent.findViewById(R.id.lvExp);
        RelativeLayout rlFlowView = parent.findViewById(R.id.rlFlowView);
        recyclerViewFlow = parent.findViewById(R.id.recycler_view);
        recyclerViewFlow.setLayoutManager(new LinearLayoutManager(getActivity()));
        tvSnortHeading = parent.findViewById(R.id.tvSnortHeading);
        tvFlowheading = parent.findViewById(R.id.tvFlowHeading);

        ViewTreeObserver vto = rlSuperView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                rlSuperView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int width  = rlSuperView.getMeasuredWidth();
                superViewHeight = rlSuperView.getMeasuredHeight();
                Log.d("rlSuperViewHght== ",String.valueOf(superViewHeight));

//                rlSnortView.getLayoutParams().height = 150;
//                rlFlowView.getLayoutParams().height = 150;
                int remainingHeight = superViewHeight - 300;
                Log.d("remainingHeight== ",String.valueOf(remainingHeight));
//                listViewExpandable.getLayoutParams().height = remainingHeight;

            }
        });

        ViewTreeObserver vtoChild = rlSnortView.getViewTreeObserver();
        vtoChild.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                rlSnortView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int width  = rlSnortView.getMeasuredWidth();
                childViewHeight = rlSnortView.getMeasuredHeight();
                Log.d("rlSuperViewHght== ",String.valueOf(superViewHeight));

//                rlSnortView.getLayoutParams().height = 150;
//                rlFlowView.getLayoutParams().height = 150;
                int remainingHeight = superViewHeight - (childViewHeight * 2);
                Log.d("remainingHeight== ",String.valueOf(remainingHeight));
                listViewExpandable.getLayoutParams().height = remainingHeight;

            }
        });

        rlSnortView.setOnClickListener(this);
        rlFlowView.setOnClickListener(this);


        if (GeneralUtils.isConnected(getContext())) {
            if (isFirstCallPreMutedAlerts || listOfPreMutedAlerts.size() < 1 || listOfFlowAlerts.size() < 1) {
                showProgressDialog(true);
                Api.getPreMutedAlerts(getActivity(), getRequestParams(), listenerPREMUTED);
            } else {
                if (listOfPreMutedAlerts.size() > 0) {
                    getParentView().findViewById(R.id.error_label).setVisibility(View.INVISIBLE);
                    prepareListData();

                    tvSnortHeading.setText(getActivity().getResources().getString(R.string.network) +"("+ listDataHeader.size() + " )");
                    tabLayout.getTabAt(2).setText("Observed Events" + " (" + (listDataHeader.size() + listOfFlowAlerts.size()) + ")");
                    preMutedAlertAdapter = new PreMutedAlertAdapter(getContext(), listDataHeader, listDataChild, listDataChildDest, listOfPreMutedAlerts, listDataHeaderInputSrc, listDataHeaderTimeStamp);
                    // setting list adapter
                    listViewExpandable.setAdapter(preMutedAlertAdapter);
                }else{
                    //hide snort alert view
                }
                if (listOfPreMutedFlowAlerts.size() > 0){
                    getParentView().findViewById(R.id.error_label).setVisibility(View.INVISIBLE);
                    tvFlowheading.setText(getActivity().getResources().getString(R.string.device) + listOfFlowAlerts.size() + " )");
                    flowAdapter = new preMutedFlowAdapter(getContext(), listOfFlowAlerts);
                    recyclerViewFlow.setAdapter(flowAdapter);

                }else{
                    //hide flow alert view
                }
            }

        } else {
            showAlertMsgDialog(getContext(), getResources().getString(R.string.internet_connection));
        }
        mRecyclerViewswip = parent.findViewById(R.id.recycler_viewswip);
        mSwipeRefreshLayout = parent.findViewById(R.id.cnctdhosts_swipe_refresh_layout);
        mRecyclerViewswip.setLayoutManager(new LinearLayoutManager(getActivity()));

        mRecyclerViewswip.setVisibility(View.GONE);

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mRecyclerViewswip.setVisibility(View.GONE);
            refreshContent();
            error_label.setVisibility(View.GONE);
        });
        // setting list adapter
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
//        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "pre_muted_alerts", null /* class override */);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, "pre_muted_alerts");
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, this.getClass().getName());

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        listDataHeaderInputSrc = new ArrayList<>();
        listDataHeaderTimeStamp = new ArrayList<>();
        listDataChild = new HashMap<>();
        listDataChildDest = new HashMap<>();

        for (int i = 0; i < listOfPreMutedAlerts.size(); i++) {
            String description = listOfPreMutedAlerts.get(i).getDescription();
            String inputSrc = listOfPreMutedAlerts.get(i).getInput_src();
            int timeStamp = listOfPreMutedAlerts.get(i).getEve_sec();
            if (inputSrc.equalsIgnoreCase("ids")) {
                if (listDataHeader.contains(description)) {
//                    Log.d("PreMuted", "element exist");
                } else {
//                    Log.d("PreMuted", "element donot exist");
                    listDataHeader.add(description);
                    listDataHeaderInputSrc.add("ids");
                    listDataHeaderTimeStamp.add(timeStamp);
                }
            }
//            else if (inputSrc.equalsIgnoreCase("flow")) {
//                String descriptionList = "";
//                JSONObject jObj;// = new JSONObject();
//                for (int j = 0; j < listOfPreMutedAlerts.get(i).flowAlertJsonArr.length(); j++) {
//
//                    try {
//                        jObj = listOfPreMutedAlerts.get(i).flowAlertJsonArr.getJSONObject(j);
//                        descriptionList += jObj.getString("description") + "\n";
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                listDataHeader.add(descriptionList);
//                listDataHeaderInputSrc.add("flow");
//                listDataHeaderTimeStamp.add(timeStamp);
//            }
            else {
            }
        }
        // Adding child data
        for (int i = 0; i < listDataHeader.size(); i++) {
            List<String> srcIpsList = new ArrayList<>();
            List<String> destIpsList = new ArrayList<>();
            for (int j = 0; j < listOfPreMutedAlerts.size(); j++) {
//                Log.d("premuted--header", listDataHeader.get(i));

                String inputSrc = listOfPreMutedAlerts.get(j).getInput_src();

                if (inputSrc.equalsIgnoreCase("ids")) {
                    if (listOfPreMutedAlerts.get(j).getDescription().equals(listDataHeader.get(i))) {
                        srcIpsList.add(listOfPreMutedAlerts.get(j).getSrc_ip());
                        destIpsList.add(listOfPreMutedAlerts.get(j).getDest_ip());
                    }
                    listDataChild.put(listDataHeader.get(i), srcIpsList);
                    listDataChildDest.put(listDataHeader.get(i), destIpsList);

                }
            }

        }
        //start
        PreMutedAlertsModel flowModel;
        for (int i = 0; i < listOfPreMutedFlowAlerts.size(); i++) {
            StringBuilder descriptionList = new StringBuilder();
            int timeStamp = listOfPreMutedFlowAlerts.get(i).getEve_sec();
            JSONArray jArray = listOfPreMutedFlowAlerts.get(i).flowAlertJsonArr;
            for (int j = 0; j < jArray.length(); j++) {
                try {
                    JSONObject jObj = jArray.getJSONObject(j);

                    try {
                        jObj = listOfPreMutedFlowAlerts.get(i).flowAlertJsonArr.getJSONObject(j);
                        descriptionList.append(jObj.getString("description")).append("\n");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            flowModel = new PreMutedAlertsModel();
            flowModel.setDescription(descriptionList.toString());
            flowModel.setEve_sec(timeStamp);
            listOfFlowAlerts.add(flowModel);

        }
        //end
    }

    private final AsyncTaskListener listenerPREMUTED = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (!isAdded()) {
                return;
            }
            listOfFlowAlerts.clear();
            showProgressDialog(false);
            tabLayout.getTabAt(2).setText("Observed Events" + " (0)");

            if (result.code == 200) {
                if (result.message.equalsIgnoreCase("No data detected")) {
                    getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
//                    showAlertMsgDialog(getContext(), "No alerts were detected.");
                } else {
                    getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
                    prepareListData();
                    tabLayout.getTabAt(2).setText("Observed Events" + " (" + listDataHeader.size() + ")");
                    if (listOfFlowAlerts.size() > 0){
                        tvFlowheading.setText(getActivity().getResources().getString(R.string.device) + listOfFlowAlerts.size() + " )");
                        flowAdapter = new preMutedFlowAdapter(getContext(), listOfFlowAlerts);
                        recyclerViewFlow.setAdapter(flowAdapter);
                    }else{
                        //hide flow view
                    }
                    if (listDataHeader.size() > 0) {
                        tvSnortHeading.setText(getActivity().getResources().getString(R.string.network)+ "(" + listDataHeader.size() + " )");
                        getParentView().findViewById(R.id.error_label).setVisibility(View.GONE);
                        preMutedAlertAdapter = new PreMutedAlertAdapter(getContext(), listDataHeader, listDataChild, listDataChildDest, listOfPreMutedAlerts, listDataHeaderInputSrc, listDataHeaderTimeStamp);
                        listViewExpandable.setAdapter(preMutedAlertAdapter);

                    }else{
                        //hide snort view
                        if (recyclerViewFlow.getVisibility() == View.VISIBLE) {

                            listViewExpandable.setVisibility(View.GONE);
                            recyclerViewFlow.setVisibility(View.VISIBLE);
                            error_label.setVisibility(View.GONE);

                        }
                    }

                }
            } else if (result.code == 503) {
                getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
                showAlertMsgDialog(getContext(), result.message);
            } else if (result.code == 409) {
                getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
                showAlertMsgDialog(getContext(), result.message);
            } else {
                getParentView().findViewById(R.id.error_label).setVisibility(View.VISIBLE);
            }


        }

    };

    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private JSONObject getRequestParams() {
        JSONObject requestData = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            requestData.put("request_id", Dmoat.getNewRequestId());
            requestData.put("app_id", pref.getString("app_id", ""));
            requestData.put("token", UserManager.getInstance().getLoggedInUser().token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestData;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.rlSnortView) {

            if (listViewExpandable.getVisibility() == View.VISIBLE) {
                listViewExpandable.setVisibility(View.GONE);
                recyclerViewFlow.setVisibility(View.VISIBLE);
//                    error_label.setVisibility(View.GONE);
            } else {
                listViewExpandable.setVisibility(View.VISIBLE);
                recyclerViewFlow.setVisibility(View.GONE);
//                    if(listDataHeader.size() <= 0)
//                    error_label.setVisibility(View.VISIBLE);
            }
        }else if(v.getId() == R.id.rlFlowView) {

            if (recyclerViewFlow.getVisibility() == View.VISIBLE) {
                listViewExpandable.setVisibility(View.VISIBLE);
                recyclerViewFlow.setVisibility(View.GONE);
//                    if(listOfFlowAlerts.size() > 0)
//                   error_label.setVisibility(View.VISIBLE);
            } else {
                listViewExpandable.setVisibility(View.GONE);
                recyclerViewFlow.setVisibility(View.VISIBLE);
//                    error_label.setVisibility(View.GONE);
            }
        }

    }

    private void refreshContent() {
        Api.getPreMutedAlerts(getActivity(), getRequestParams(), listenerPREMUTED);
        mSwipeRefreshLayout.setRefreshing(false);


    }
}
