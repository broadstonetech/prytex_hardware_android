package hardware.prytex.io.fragment.Policy.AccessControl;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.adapter.PolicyHostsAdapter;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.fragment.Policy.PoliciesMainFragment;
import hardware.prytex.io.model.ConnectedDeviceList;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.GeneralUtils;
import hardware.prytex.io.util.MyToast;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;
import static hardware.prytex.io.parser.ConnectedDevicesParser.isDevicesListReceived;
import static hardware.prytex.io.parser.ConnectedDevicesParser.listOfConnetcedDevice;


@SuppressWarnings("ALL")
public class PolicyPlanFragment extends BaseFragment implements View.OnClickListener {

    private EditText etPolicyTitle;
    private TextView tvStartTime;
    private TextView tvEndTime;
    private Button btnEveryDaySelection;
    public static Button btnAddAllDevices;
    private Button btnSat, btnSun, btnMon, btnTue, btnWed, btnThu, btnFri;
    private int endHours;
    private int endMins;
    private int endSeconds;

    public boolean isCreateNewPolicy = true;
    public boolean isPreDefPolicy = false;
    public String policyStartTime = "";
    public String policyEndTime = "";
    public String policyStartTimeGMT = "";
    public String policyEndTimeGMT = "";
    public String policeyKey = "";
    public String policyTitle;
    public int policyDaysWeight = 0;
    public int policyStatus;
    private String policyType = "";
    public JSONArray jsonArrSelectedHosts;
    private final ArrayList<Integer> selectedDaysArr = new ArrayList<>();
    private final ArrayList<ConnectedDeviceList> selectedHostsList = new ArrayList<>();
    private final ArrayList<ConnectedDeviceList> currentHostsReceived = new ArrayList<>();

    private boolean isMonday = false, isTuesday = false, isWednesday = false, isThursday = false, isFriday = false, isSaturday = false, isSunday = false;

    private ProgressDialog dialog;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    private SharedPreferences pref;

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);

        String appId = pref.getString("app_id", "");
//        llBottomTabs.setVisibility(View.GONE);

        //section 1 outlets
        etPolicyTitle = parent.findViewById(R.id.etPolicyTitle);
        TextView tvPolicyIntro = parent.findViewById(R.id.tvPolicyIntro);
//        btnPolicyStatus = parent.findViewById(R.id.btnPolicyStatus);
        //section 2 outlets-- policy Days
        btnEveryDaySelection = parent.findViewById(R.id.btnEveryDaySelection);
        btnSat = parent.findViewById(R.id.btnSaturdayCP);
        btnSun = parent.findViewById(R.id.btnSundayCP);
        btnMon = parent.findViewById(R.id.btnMondayCP);
        btnTue = parent.findViewById(R.id.btnTuesdayCP);
        btnWed = parent.findViewById(R.id.btnWednesdayCP);
        btnThu = parent.findViewById(R.id.btnThursdayCP);
        btnFri = parent.findViewById(R.id.btnFridayCP);

        //section 3 outlets--- policy Interva;
        tvStartTime = parent.findViewById(R.id.tvStartTime);
        tvEndTime = parent.findViewById(R.id.tvEndTime);

        //section 4 outlets -- policy Devices
        btnAddAllDevices = parent.findViewById(R.id.btnAddAllDevices);
        mRecyclerView = parent.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        TextView tvDeletePolicy = parent.findViewById(R.id.tvDeletePolicy);

        Button btnSavepolicy = parent.findViewById(R.id.btnSavePolicy);

        //click listeners
        btnEveryDaySelection.setOnClickListener(this);
        btnSat.setOnClickListener(this);
        btnSun.setOnClickListener(this);
        btnMon.setOnClickListener(this);
        btnTue.setOnClickListener(this);
        btnWed.setOnClickListener(this);
        btnThu.setOnClickListener(this);
        btnFri.setOnClickListener(this);
        tvStartTime.setOnClickListener(this);
        tvEndTime.setOnClickListener(this);
        btnAddAllDevices.setOnClickListener(this);
        tvDeletePolicy.setOnClickListener(this);
        btnSavepolicy.setOnClickListener(this);

        //class func's

        if (isCreateNewPolicy ) {
            policyStatus = 1;
            policeyKey = gernerateRandomKey();
            tvDeletePolicy.setVisibility(View.GONE);
            Log.d("ppolicyPlanKey", policeyKey);
            showProgressDialog(true);
            Api.getContactedHosts(getActivity(), getRequestparams(), listener);
        }else {
            if(isPreDefPolicy)
            {
                policyStatus = 1;
                policeyKey = gernerateRandomKey();
                tvDeletePolicy.setVisibility(View.GONE);
                showProgressDialog(true);
                Api.getContactedHosts(getActivity(), getRequestparams(), listener);

            }else
            {
                tvDeletePolicy.setVisibility(View.VISIBLE);
                ConnectedDeviceList device;
                for (int i = 0; i < jsonArrSelectedHosts.length(); i++) {
                    try {
                        JSONObject jObj = jsonArrSelectedHosts.getJSONObject(i);
                        String hostName = jObj.getString("hostname");
                        String ip = jObj.getString("ip");
                        String macAddress = jObj.getString("macaddress");
                        device = new ConnectedDeviceList();

                        device.setHostName_cd(hostName);
                        device.setIp_cd(ip);
                        device.setMacAddress_cd(macAddress);
                        device.setdeviceCategory("unknown");
                        device.setInPolicy(true);
                        currentHostsReceived.add(device);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                mAdapter = new PolicyHostsAdapter(getContext(), currentHostsReceived, false);
                mRecyclerView.setAdapter(mAdapter);
            }
            btnAddAllDevices.setBackground(getContext().getResources().getDrawable(R.drawable.cnctd_devices_round_btn_bg));
            calculateDaysFromWeight(policyDaysWeight);
            etPolicyTitle.setText(policyTitle);

            //set weight of days after calculation
//            calculateDaysFromWeight(policyDaysWeight);
            tvStartTime.setText(policyStartTime);
            tvEndTime.setText(policyEndTime);


        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
//        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "access_control_create", null /* class override */);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, "access_control_create");
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, this.getClass().getName());

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public String getTitle() {
        return "Access Control";
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_policy_plan;
    }

    private void setPolicyDaySelection(Button btn) {
        if (btn.isSelected()) {
            btn.setSelected(false);
            btn.setBackground(getContext().getResources().getDrawable(R.drawable.policy_days_border));
            btnEveryDaySelection.setSelected(false);
            btnEveryDaySelection.setBackground(getContext().getResources().getDrawable(R.drawable.policy_days_border));
        } else {
            btn.setSelected(true);
            btn.setBackground(getContext().getResources().getDrawable(R.drawable.day_selected));
        }
    }

    private void calculateDaysFromWeight(int weight) {

        Integer[] dayWeights = {64, 32, 16, 8, 4, 2, 1};
        for (int i = 0; i < dayWeights.length; i++) {
            if ((dayWeights[i] & weight) != 0) {
                selectedDaysArr.add(dayWeights[i]);
                weight = weight - dayWeights[i];
                Log.d("PolicyDayAdded", String.valueOf(i));
            } else {
                Log.d("PolicyDayRemoved", String.valueOf(i));
            }
            Log.d("policyDay", String.valueOf(dayWeights[i]));
        }

        if (isCreateNewPolicy) {
            btnSat.setSelected(true);
            btnSun.setSelected(true);
            btnMon.setSelected(true);
            btnTue.setSelected(true);
            btnWed.setSelected(true);
            btnThu.setSelected(true);
            btnFri.setSelected(true);
        }else{
            btnSat.setSelected(false);
            btnSun.setSelected(false);
            btnMon.setSelected(false);
            btnTue.setSelected(false);
            btnWed.setSelected(false);
            btnThu.setSelected(false);
            btnFri.setSelected(false);

        }

        for (int i = 0; i < selectedDaysArr.size(); i++) {
            if (selectedDaysArr.get(i) == 64) {
                btnMon.setSelected(false);
                isMonday = true;
                setPolicyDaySelection(btnMon);
            } else if (selectedDaysArr.get(i) == 32) {
                btnTue.setSelected(false);
                isTuesday = true;
                setPolicyDaySelection(btnTue);
            } else if (selectedDaysArr.get(i) == 16) {
                btnWed.setSelected(false);
                isWednesday = true;
                setPolicyDaySelection(btnWed);
            } else if (selectedDaysArr.get(i) == 8) {
                btnThu.setSelected(false);
                isThursday = true;
                setPolicyDaySelection(btnThu);
            } else if (selectedDaysArr.get(i) == 4) {
                btnFri.setSelected(false);
                isFriday = true;
                setPolicyDaySelection(btnFri);
            } else if (selectedDaysArr.get(i) == 2) {
                btnSat.setSelected(false);
                isSaturday = true;
                setPolicyDaySelection(btnSat);
            } else if (selectedDaysArr.get(i) == 1) {
                btnSun.setSelected(false);
                isSunday = true;
                setPolicyDaySelection(btnSun);
            }
        }

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btnEveryDaySelection) {

            btnSat.setSelected(false);
            btnSun.setSelected(false);
            btnMon.setSelected(false);
            btnTue.setSelected(false);
            btnWed.setSelected(false);
            btnThu.setSelected(false);
            btnFri.setSelected(false);
            Button[] btnsList = {btnSat, btnSun, btnMon, btnTue, btnWed, btnThu, btnFri};
            for (Button button : btnsList) {
                setPolicyDaySelection(button);
                isMonday = true;
                isTuesday = true;
                isWednesday = true;
                isThursday = true;
                isFriday = true;
                isSaturday = true;
                isSunday = true;

            }
            btnEveryDaySelection.setBackground(getContext().getResources().getDrawable(R.drawable.selected_day_policy));
        }else if(v.getId() == R.id.btnSaturdayCP) {

            setPolicyDaySelection(btnSat);
            isSaturday = !isSaturday;
        }else if(v.getId() == R.id.btnSundayCP) {

            setPolicyDaySelection(btnSun);
            isSunday = !isSunday;
        }else if(v.getId() == R.id.btnMondayCP) {

            setPolicyDaySelection(btnMon);
            isMonday = !isMonday;
        }else if(v.getId() == R.id.btnTuesdayCP) {

            setPolicyDaySelection(btnTue);
            isTuesday = !isTuesday;
        }else if(v.getId() == R.id.btnWednesdayCP) {

            isWednesday = !isWednesday;
            setPolicyDaySelection(btnWed);
        }else if(v.getId() == R.id.btnThursdayCP) {

            isThursday = !isThursday;
            setPolicyDaySelection(btnThu);
        }else if(v.getId() == R.id.btnFridayCP) {

            isFriday = !isFriday;
            setPolicyDaySelection(btnFri);
        }else if(v.getId() == R.id.tvStartTime) {

            TimePickerDialog dialogStartTime = TimePickerDialog.newInstance((view, hourOfDay, minute, second) -> {

                policyStartTime = HourMinuteFormatting(hourOfDay, minute);//policyStartHr + ":" + policyStartMin;//String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                tvStartTime.setText(policyStartTime);
//                        MyToast.showMessage(getContext(), "StartTime:"+policyStartTime);
                try {
                    DateFormat utcFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                    utcFormat.setTimeZone(TimeZone.getDefault());

                    Date date = utcFormat.parse(policyStartTime);

                    DateFormat deviceFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                    deviceFormat.setTimeZone(TimeZone.getTimeZone("UTC")); //Device timezone

                    policyStartTimeGMT = deviceFormat.format(date);

                    Log.d("PolicyPlanTimeZone", policyStartTimeGMT);
                } catch (Exception e) {

                    Log.d("PolicyPlanS_TimeExcep", String.valueOf(e));
                }

            }, endHours, endMins, endSeconds, false);
            dialogStartTime.show(getActivity().getFragmentManager(), "StartDateTimePicker");
        }else if(v.getId() == R.id.tvEndTime) {

            final TimePickerDialog dialogEndTime = TimePickerDialog.newInstance((view, hourOfDay, minute, second) -> {
                endHours = hourOfDay;
                endMins = minute;
                endSeconds = second;

                policyEndTime = HourMinuteFormatting(hourOfDay, minute);//String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
//                        MyToast.showMessage(getContext(), "EndTime:"+policyEndTime);
                tvEndTime.setText(policyEndTime);

                try {
                    DateFormat utcFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                    utcFormat.setTimeZone(TimeZone.getDefault());

                    Date date = utcFormat.parse(policyEndTime);

                    DateFormat deviceFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                    deviceFormat.setTimeZone(TimeZone.getTimeZone("UTC")); //Device timezone

                    policyEndTimeGMT = deviceFormat.format(date);

                    Log.d("PolicyPlanTimeZone", policyEndTimeGMT);
                } catch (Exception e) {

                    Log.d("PolicyPlanE_TimeExcep", String.valueOf(e));
                }

            }, endHours, endMins, endSeconds, false);
            dialogEndTime.show(getActivity().getFragmentManager(), "EndtDateTimePicker");

        }else if(v.getId() == R.id.btnAddAllDevices) {

            addAllDevicesInPolicy();
        }else if(v.getId() == R.id.tvDeletePolicy) {

            //call api to del policy
            getSelectedHosts();
            policyType = "delete";
            policyDaysWeight = getWeekDaysWeight();
            showDeletePolicyDialog();
        }else if(v.getId() == R.id.btnSavePolicy){

                //call api to create/update policy

                getSelectedHosts();
                //checkMissingFieldsAndValidation

                policyDaysWeight = getWeekDaysWeight();
                if (checkMissingFields()) {
                    if (isCreateNewPolicy) {
                        policyType = "create";
                    } else if(isPreDefPolicy){
                        policyType = "create";
                    }else  {
                        policyType = "update";
                    }
                    showProgressDialog(true);
                    Api.createPolicy(getActivity(), getRequestparams(policyType), listenerPostPolicy);
                }

        }
    }

    private void showDeletePolicyDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Prytex Hardware");
        builder.setMessage("Do you really want to delete policy?")
                .setCancelable(false)
                .setPositiveButton("Delete", (dialog, id) -> {
                    showProgressDialog(true);
                    Api.createPolicy(getActivity(), getRequestparams(policyType), listenerPostPolicy);

                })
                .setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
        alert.setCancelable(true);
    }

    private void getSelectedHosts() {
        selectedHostsList.clear();
        if (!(currentHostsReceived.size() == 0)) {
            for (int i = 0; i < PolicyHostsAdapter.HostsList.size(); i++) {
                if (PolicyHostsAdapter.HostsList.get(i).isInPolicy()) {
                    selectedHostsList.add(PolicyHostsAdapter.HostsList.get(i));
                }
            }
        }
    }

    private boolean checkMissingFields() {
        String start = tvStartTime.getText().toString();
        String end = tvEndTime.getText().toString();

        if (etPolicyTitle.getText().toString().equalsIgnoreCase("")) {
            showAlertMsgDialog(getContext(), "Policy Title was missing.");
            return false;
        } else if (policyDaysWeight == 0) {
            showAlertMsgDialog(getContext(), "No day was selected.");
            return false;
        } else if (policyStartTime.equalsIgnoreCase("") || policyEndTime.equalsIgnoreCase("")) {
            showAlertMsgDialog(getContext(), "No time was selected.");
            return false;
        } else if (!GeneralUtils.dateComparison(start, end)) {
            MyToast.showMessage(getActivity(), "Policy end time should greater than start time.");
            return false;
        } else if (selectedHostsList.size() == 0) {
            showAlertMsgDialog(getContext(), "No Device was selected.");
            return false;
        }
        return true;
    }

    private final AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (!isAdded()) {
                return;
            }

            showProgressDialog(false);
            if (result.code == 200 || result.code == 2) {
                if (isDevicesListReceived) {
                    if (listOfConnetcedDevice.size() > 0) {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        ConnectedDeviceList deviceList;
                        for (int i = 0; i < listOfConnetcedDevice.size(); i++) {
                            deviceList = new ConnectedDeviceList();
                            if (listOfConnetcedDevice.get(i).getdeviceCategory().contains("Router")) {

                            } else {
                                currentHostsReceived.add(listOfConnetcedDevice.get(i));
                            }
                        }
                        mAdapter = new PolicyHostsAdapter(getContext(), currentHostsReceived, true);
                        mRecyclerView.setAdapter(mAdapter);
                    } else {
//                        showAlertMsgDialog(getContext(), getResources().getString(R.string.no_connected_devices));
                        MyToast.showMessage(getContext(), getResources().getString(R.string.no_connected_devices));
                    }
                } else {
//                    showAlertMsgDialog(getContext(), getResources().getString(R.string.no_connected_devices));
                    MyToast.showMessage(getContext(), getResources().getString(R.string.no_connected_devices));
                }
            } else if (result.code == 503) {
                showAlertMsgDialog(getContext(), result.message);
            } else if (result.code == 409) {
                showAlertMsgDialog(getContext(), result.message);
            } else {
//                showAlertMsgDialog(getContext(), getResources().getString(R.string.no_connected_devices));
            }
        }
    };

    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private JSONObject getRequestparams() {
        JSONObject requestData = new JSONObject();
        try {
            requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            requestData.put("request_id", Dmoat.getNewRequestId());
            requestData.put("app_id", pref.getString("app_id", ""));
            requestData.put("token", UserManager.getInstance().getLoggedInUser().token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestData;
    }

    private JSONObject getRequestparams(String type) {
        JSONObject j = new JSONObject();
        JSONArray jsArrayHosts = new JSONArray(selectedHostsList);
        JSONArray jsArrayDays = new JSONArray(selectedDaysArr);
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"),
                Locale.getDefault());
        Date currentLocalTime = calendar.getTime();
        DateFormat date = new SimpleDateFormat("Z", Locale.getDefault());
        String localTime = date.format(currentLocalTime);

        DateFormat dates = new SimpleDateFormat("z", Locale.getDefault());
        String localTimes = dates.format(currentLocalTime);
        Log.d("TimezoneLocal", localTime);
        String str = new StringBuffer(localTime).insert(localTime.length() - 2, ":").toString();

        Log.d("TimezoneLocalFormat", str);

        try {
            JSONObject data = new JSONObject();

            data.put("type", type);
            data.put("id", policeyKey);
            data.put("status", policyStatus);
            data.put("title", etPolicyTitle.getText().toString());
            data.put("s_time", tvStartTime.getText().toString());
            data.put("e_time", tvEndTime.getText().toString());
            data.put("days", policyDaysWeight);
            data.put("hosts_list", getHOstsJson());
            data.put("s_time_gmt", policyStartTimeGMT);
            data.put("e_time_gmt", policyEndTimeGMT);
            data.put("time_zone", str);

            j.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            j.put("request_id", Dmoat.getNewRequestId());
            j.put("app_id", pref.getString("app_id", ""));
            j.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            j.put("token", UserManager.getInstance().getLoggedInUser().token);
            j.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("PolicPlanCreateParam", j.toString());
        return j;
    }

    private JSONArray getHOstsJson() {
        JSONObject jResult = new JSONObject();// main object
        JSONArray jArray = new JSONArray();// /ItemDetail jsonArray

        for (int i = 0; i < selectedHostsList.size(); i++) {
            JSONObject jGroup = new JSONObject();// /sub Object

            try {
                jGroup.put("hostname", selectedHostsList.get(i).getHostName_cd());
                jGroup.put("ip", selectedHostsList.get(i).getIp_cd());
                jGroup.put("macaddress", selectedHostsList.get(i).getMacAddress_cd());

                jArray.put(jGroup);
                jResult.put("itemDetail", jArray);
                // /itemDetail Name is JsonArray Name
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jArray;
    }

    private final AsyncTaskListener listenerPostPolicy = result -> {
        if (result.message.equalsIgnoreCase("")) {
            result.message = getContext().getResources().getString(R.string.error_message_generic);
        }

        showProgressDialog(false);
        if (result.code == 200 || result.code == 2) {
            //policy create successfully go back
            showPolicySuccessDialog(getContext(), getContext().getResources().getString(R.string.policy_created_text));
        } else if (result.code == 503) {
            showAlertMsgDialog(getContext(), result.message);
        } else if (result.code == 409) {
            showAlertMsgDialog(getContext(), result.message);
        } else {
            showAlertMsgDialog(getContext(), result.message);
        }
    };

    private String HourMinuteFormatting (int hourOfDay, int minute){
        String policyStartHr;
        String policyStartMin;
        if (hourOfDay < 10){
            policyStartHr = "0" + hourOfDay;
        }else{
            policyStartHr = String.valueOf(hourOfDay);
        }
        if (minute < 10){
            policyStartMin = "0" + minute;
        }else{
            policyStartMin = String.valueOf(minute);
        }
        return policyStartHr + ":" + policyStartMin;
    }

    public static String gernerateRandomKey() {
        String CharsString = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder keyString = new StringBuilder();
        Random rnd = new Random();
        while (keyString.length() < 8) { // length of the random string.
            int index = (int) (rnd.nextFloat() * CharsString.length());
            keyString.append(CharsString.charAt(index));
        }
        return keyString.toString();
    }

    private int getWeekDaysWeight() {

        int weight = 0;

        if (isMonday) {
            weight = weight + 64;
        } if (isTuesday) {
            weight = weight + 32;
        } if (isWednesday) {
            weight = weight + 16;
        } if (isThursday) {
            weight = weight + 8;
        } if (isFriday) {
            weight = weight + 4;
        } if (isSaturday) {
            weight = weight + 2;
        } if (isSunday) {
            weight = weight + 1;
        } else {

        }
        return weight;
    }

    private void addAllDevicesInPolicy() {
        if (!(currentHostsReceived.size() == 0)) {
            btnAddAllDevices.setBackground(getContext().getResources().getDrawable(R.drawable.selected_day_policy));
            for (int i = 0; i < PolicyHostsAdapter.HostsList.size(); i++) {
                PolicyHostsAdapter.HostsList.get(i).setInPolicy(true);
            }
            mAdapter.notifyDataSetChanged();
        }
    }

    private void showPolicySuccessDialog(Context ctx, String msg) {

        new AlertDialog.Builder(ctx)
                .setTitle("Prytex Hardware")
                .setMessage(msg)

                .setPositiveButton("Ok", (dialog, whichButton) -> {

                    PoliciesMainFragment fragment = new PoliciesMainFragment();
                    fragment.isBackFromPlan = true;
                    getHelper().replaceFragment(fragment, true, false);

                }).show();

    }
}
