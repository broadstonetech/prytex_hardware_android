package hardware.prytex.io.fragment.Policy.AccessControl;


import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import hardware.prytex.io.R;
import hardware.prytex.io.adapter.ProtocolsAdapter;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.fragment.CreateNewPolicy;
import hardware.prytex.io.model.SelectedProtocols;
import com.google.firebase.analytics.FirebaseAnalytics;


import static hardware.prytex.io.adapter.ProtocolsAdapter.selectedProtocolsIndices;
import static hardware.prytex.io.fragment.CreateNewPolicy.listOfSelectedProtocols;
import static hardware.prytex.io.fragment.CreateNewPolicy.selectedProtocols;
import static hardware.prytex.io.util.DmoatUtils.PROTOCOLS_KEYS;
import static hardware.prytex.io.util.DmoatUtils.PROTOCOLS_VALUE;

@SuppressWarnings("ALL")
public class PolicyProtocolsFragment extends BaseFragment {

    public static ImageView btnselectAll;
    public static boolean isAllProtocolsSelectedd = false;
    private final Integer[] protocolImage = {
//            R.drawable.apple,
//            R.drawable.icloud_logo,
//            R.drawable.itunes,
//            R.drawable.airquality,  //battlefield
            R.drawable.ciscovpnclient,
//            R.drawable.citrix,
//            R.drawable.citrix,
//            R.drawable.cloud_fare,
//            R.drawable.cnn,
//            R.drawable.deezer,
//            R.drawable.dofus,
//            R.drawable.dropbox,
//            R.drawable.ebay,
//            R.drawable.facebook,
//            R.drawable.fiesta,
//            R.drawable.airquality, //ftp client
//            R.drawable.airquality, //ftp data
//            R.drawable.gmail,
//            R.drawable.google,
//            R.drawable.google_maps,
//            R.drawable.guild_wars,
//            R.drawable.h_three_two_three,
//            R.drawable.hotmail,
//            R.drawable.http,
//            R.drawable.http,
//            R.drawable.http,
//            R.drawable.http,
//            R.drawable.airquality, //icmp
//            R.drawable.instagram,
//            R.drawable.ipp,
//            R.drawable.irc,
//            R.drawable.kakoatalk,
//            R.drawable.kakoatalk,
//            R.drawable.lastfm,
//            R.drawable.microsoft,
//            R.drawable.mqtt,
//            R.drawable.onedrive,
//            R.drawable.msn,
//            R.drawable.netflix,
//            R.drawable.office365,
//            R.drawable.openvpn,
//            R.drawable.pandora,
//            R.drawable.airquality, //pc anywhere
//            R.drawable.quake,
//            R.drawable.quick_play,
//            R.drawable.openvpn,
//            R.drawable.rdp,
//            R.drawable.remote_scan,
//            R.drawable.sip,
//            R.drawable.skype,
//            R.drawable.slack,
//            R.drawable.snapchat,
//            R.drawable.spotify,
//            R.drawable.ssh,
//            R.drawable.ssl,
//            R.drawable.ssl,
//            R.drawable.startcraft,
//            R.drawable.telegram,
//            R.drawable.telnet,
//            R.drawable.tor,
//            R.drawable.twitch,
//            R.drawable.twitter,
//            R.drawable.ubuntuone,
//            R.drawable.jabber,
//            R.drawable.vevo,
//            R.drawable.viber,
//            R.drawable.vnc,
//            R.drawable.warcraft,
//            R.drawable.waze,
//            R.drawable.webex,
//            R.drawable.whatsapp,
//            R.drawable.whatsapp,
//            R.drawable.wikipedia,
//            R.drawable.windows_update,
//            R.drawable.worldkingfu,
//            R.drawable.worldofwarcrafct,
//            R.drawable.xbox,
//            R.drawable.yahoo,
//            R.drawable.youtube
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
//        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "protocol_policy_create", null /* class override */);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, "protocol_policy_create");
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, this.getClass().getName());

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);

    }

    @Override
    public String getTitle() {
        return "Create Policy";
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        isAllProtocolsSelectedd = false;
        RecyclerView mRecyclerView = parent.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        Button btnDoneSelection = parent.findViewById(R.id.btnDoneSelectingProtocol);
        btnselectAll = parent.findViewById(R.id.btnSelectAllProtocols);



        PROTOCOLS_KEYS = getResources().getStringArray(R.array.protocols_keys);
//        Arrays.sort(PROTOCOLS_KEYS);
        PROTOCOLS_VALUE = getResources().getStringArray(R.array.protocols_value);
//        Arrays.sort(PROTOCOLS_VALUE);


        ProtocolsAdapter mAdapter = new ProtocolsAdapter(getContext(), PROTOCOLS_KEYS, PROTOCOLS_VALUE, protocolImage);
        mRecyclerView.setAdapter(mAdapter);

        btnDoneSelection.setOnClickListener(v -> {

//                //get all selected protocols and store in array list
//                getSelectedProtocols(holder);
            for (int i = 0; i<selectedProtocolsIndices.size(); i++) {
                int index = selectedProtocolsIndices.get(i);
                selectedProtocols = new SelectedProtocols();
                selectedProtocols.setId(i + 1);
                selectedProtocols.setProtocolKey(PROTOCOLS_KEYS[index]);
                selectedProtocols.setProtocolValue(PROTOCOLS_VALUE[index]);
                listOfSelectedProtocols.add(selectedProtocols);
            }
//                listOfSelectedProtocols.clear();
//                for (int i = 0; i < PROTOCOLS_VALUE.length; i++) {
//                    String btnText = holder.btnAcces.getText().toString();
//                    if (holder.checkBox.isChecked())
//                        if (holder.btnAcces.getText().toString().equalsIgnoreCase("Selected")) {
//
//                            selectedProtocols = new SelectedProtocols();
//                            selectedProtocols.setId(i + 1);
//                            selectedProtocols.setProtocolKey(PROTOCOLS_KEYS[i]);
//                            selectedProtocols.setProtocolValue(PROTOCOLS_VALUE[i]);
//                            listOfSelectedProtocols.add(selectedProtocols);
//                        }
//                }
//                Toast.makeText(getContext(), "listsize" + listOfSelectedProtocols.toString(), Toast.LENGTH_SHORT).show();
            getHelper().replaceFragment(new CreateNewPolicy(), false, true);
//                AppCompatActivity activity = (AppCompatActivity) getContext();
//                CreateNewPolicy myFragment = new CreateNewPolicy();
////                //Create a bundle to pass data, add data, set the bundle to your fragment and:
//                activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, myFragment);

        });

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_policy_protocols;
    }


}