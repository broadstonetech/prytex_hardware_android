package hardware.prytex.io.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.model.AlertAction;
import hardware.prytex.io.model.MutedBlockedAlertsModel;

import java.util.ArrayList;

/**
 * Created by khawarraza on 10/11/2016.
 */

public class AlertActionsFragment extends BaseFragment {

    private ArrayList<AlertAction> actions;
    private LinearLayout container;
    private OnAlertActionSelectionListener listener;
    private MutedBlockedAlertsModel alert;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_alert_actions;
    }

    @Override
    public String getTitle() {
        return "Prytex Hardware Alerts";
    }

    public void setActionItems(ArrayList<AlertAction> actionItems) {
        this.actions = actionItems;
    }

    public void setAlertActionListener(OnAlertActionSelectionListener listener) {
        this.listener = listener;
    }

    public void setAlert(MutedBlockedAlertsModel alert) {
        this.alert = alert;
    }

    public interface OnAlertActionSelectionListener {
        void onAlertActionSelected(AlertAction action, MutedBlockedAlertsModel alert);
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);
        container = parent.findViewById(R.id.options_container);
        parent.findViewById(R.id.parent).setOnClickListener(v -> getHelper().onBack());


        populateActions();

    }

    private void populateActions() {
        if (actions == null || actions.size() == 0) {
            return;
        }

        for (AlertAction action : actions) {
            View view = View.inflate(getActivity(), R.layout.action_item, null);
            TextView title = view.findViewById(R.id.action_title);
            title.setText(action.title);
            title.setTextColor(action.colour);
            view.setTag(action);
            view.setOnClickListener(mActionClickLilstener);

            container.addView(view);
        }
    }

    private final View.OnClickListener mActionClickLilstener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertAction action = (AlertAction) v.getTag();
            getHelper().onBack();
            listener.onAlertActionSelected(action, alert);
        }
    };


}
