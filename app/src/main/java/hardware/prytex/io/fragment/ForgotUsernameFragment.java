package hardware.prytex.io.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.activity.AuthenticationActivity;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.MyToast;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;

/**
 * Created by usman on 12/30/16.
 */

@SuppressWarnings("ALL")
public class ForgotUsernameFragment extends BaseFragment {

    private EditText edittextMacAddress;
    private Button btnForgotPass;
    private SharedPreferences pref;

    @Override
    public int getLayoutId() {
        return R.layout.forgot_username_fragment;
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);

        edittextMacAddress = getParentView().findViewById(R.id.macaddress_edit);
        Button btnQrScanner = parent.findViewById(R.id.btnProductIdQrScanner);
        btnForgotPass = parent.findViewById(R.id.submit);
        TextView imgBack = parent.findViewById(R.id.imgBack);
        registerAfterMacTextChangedCallback();

        btnQrScanner.setOnClickListener(view -> {
            showAlertMsgDialog(getContext(), getResources().getString(R.string.qr_code_scanning_error));
//                IntentIntegrator.forSupportFragment(ForgotUsernameFragment.this).initiateScan();

        });

        imgBack.setOnClickListener(view -> getHelper().replaceFragment(new LoginFragment(), R.id.fragment_container, true, false));

        parent.findViewById(R.id.submit).setOnClickListener(v -> {
            if (!dataValidated())
                return;
            btnForgotPass.setBackgroundColor(getResources().getColor(R.color.colorBtnPressed));
            JSONObject data = new JSONObject();
            try {
                data.put("app_id", pref.getString("app_id", ""));
                data.put("request_id", Dmoat.getNewRequestId());
                JSONObject userNameData = new JSONObject();
                userNameData.put("prod_id", edittextMacAddress.getText().toString());
                data.put("data", userNameData);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Api.forgotUsername(getActivity(), data, mListener);
            getParentView().findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
//                getParentView().findViewById(R.id.submit).setVisibility(View.GONE);
        });
    }

    private boolean dataValidated() {
        if (edittextMacAddress.length() < 8) {
            MyToast.showMessage(getActivity(), "Product identifier should be 8 characters long. Please try again.");
            return false;
        }
//        else if (edittextMacAddress.length())
        return true;
    }

    private final AsyncTaskListener mListener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            btnForgotPass.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            if (!isAdded()) {
                return;
            }
            getParentView().findViewById(R.id.progressBar).setVisibility(View.GONE);
//            getParentView().findViewById(R.id.submit).setVisibility(View.VISIBLE);
            if (result.code == 200 || result.code == 2) {
                MyToast.showMessage(getActivity(), getResources().getString(R.string.forgotUsernamePwdSuccess));
                Intent i = AuthenticationActivity.startNewIntent(getActivity());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(i);
                getActivity().finish();
            } else if (result.code == 503) {
                showAlertMsgDialog(getContext(), result.message);
            }
            else if (result.code == 409){
                showAlertMsgDialog(getContext(), result.message);
            }else {
                if (result.message.equalsIgnoreCase("")){
                    result.message = "Error occured, try again.";
                }else{}

                if (result.code == 400 || result.code == 4)
                    MyToast.showMessage(getActivity(), result.message);
                else
                    MyToast.showMessage(getActivity(), result.message);
            }
        }
    };

    private void registerAfterMacTextChangedCallback() {
        edittextMacAddress.addTextChangedListener(new TextWatcher() {
            String mPreviousMac = null;

            /* (non-Javadoc)
             * Does nothing.
             * @see android.text.TextWatcher#afterTextChanged(android.text.Editable)
             */
            @Override
            public void afterTextChanged(Editable arg0) {
            }

            /* (non-Javadoc)
             * Does nothing.
             * @see android.text.TextWatcher#beforeTextChanged(java.lang.CharSequence, int, int, int)
             */
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            /* (non-Javadoc)
             * Formats the MAC address and handles the cursor position.
             * @see android.text.TextWatcher#onTextChanged(java.lang.CharSequence, int, int, int)
             */
            @SuppressLint("DefaultLocale")
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String enteredMac = edittextMacAddress.getText().toString().toUpperCase();
                String cleanMac = clearNonMacCharacters(enteredMac);
                String formattedMac = formatMacAddress(cleanMac);

                int selectionStart = edittextMacAddress.getSelectionStart();
                formattedMac = handleColonDeletion(enteredMac, formattedMac, selectionStart);
                int lengthDiff = formattedMac.length() - enteredMac.length();

                setMacEdit(cleanMac, formattedMac, selectionStart, lengthDiff);
            }

            /**
             * Strips all characters from a string except A-F and 0-9.
             * @param mac       User input string.
             * @return String containing MAC-allowed characters.
             */
            private String clearNonMacCharacters(String mac) {
                return mac.replaceAll("[^A-Za-z0-9@#$&*]", "");
            }

            /**
             * Adds a colon character to an unformatted MAC address after
             * every second character (strips full MAC trailing colon)
             * @param cleanMac      Unformatted MAC address.
             * @return Properly formatted MAC address.
             */
            private String formatMacAddress(String cleanMac) {
                int grouppedCharacters = 0;
                StringBuilder formattedMac = new StringBuilder();

                for (int i = 0; i < cleanMac.length(); ++i) {
                    formattedMac.append(cleanMac.charAt(i));
                    ++grouppedCharacters;

                    if (grouppedCharacters == 4) {
//                        formattedMac += "-";
                        grouppedCharacters = 0;
                    }
                }

                // Removes trailing colon for complete MAC address
                if (cleanMac.length() == 9)
                    formattedMac = new StringBuilder(formattedMac.substring(0, formattedMac.length() - 1));

                return formattedMac.toString();
            }

            /**
             * Upon users colon deletion, deletes MAC character preceding deleted colon as well.
             * @param enteredMac            User input MAC.
             * @param formattedMac          Formatted MAC address.
             * @param selectionStart        MAC EditText field cursor position.
             * @return Formatted MAC address.
             */
            private String handleColonDeletion(String enteredMac, String formattedMac, int selectionStart) {
                if (mPreviousMac != null && mPreviousMac.length() > 1) {
                    int previousColonCount = colonCount(mPreviousMac);
                    int currentColonCount = colonCount(enteredMac);

                    if (currentColonCount < previousColonCount) {
                        formattedMac = formattedMac.substring(0, selectionStart - 1) + formattedMac.substring(selectionStart);
                        String cleanMac = clearNonMacCharacters(formattedMac);
                        formattedMac = formatMacAddress(cleanMac);
                    }
                }
                return formattedMac;
            }

            /**
             * Gets MAC address current colon count.
             * @param formattedMac      Formatted MAC address.
             * @return Current number of colons in MAC address.
             */
            private int colonCount(String formattedMac) {
                return formattedMac.replaceAll("[^:]", "").length();
            }

            /**
             * Removes TextChange listener, sets MAC EditText field value,
             * sets new cursor position and re-initiates the listener.
             * @param cleanMac          Clean MAC address.
             * @param formattedMac      Formatted MAC address.
             * @param selectionStart    MAC EditText field cursor position.
             * @param lengthDiff        Formatted/Entered MAC number of characters difference.
             */
            private void setMacEdit(String cleanMac, String formattedMac, int selectionStart, int lengthDiff) {
                edittextMacAddress.removeTextChangedListener(this);
                if (cleanMac.length() <= 8) {
                    edittextMacAddress.setText(formattedMac);
                    edittextMacAddress.setSelection(selectionStart + lengthDiff < 0 ? 0 : selectionStart + lengthDiff);
                    mPreviousMac = formattedMac;
                } else {
                    edittextMacAddress.setText(mPreviousMac);
                    edittextMacAddress.setSelection(mPreviousMac.length());
                }
                edittextMacAddress.addTextChangedListener(this);
            }
        });
    }

    //get qr code scanner result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(getContext(), "Camera permission required for scanning.", Toast.LENGTH_LONG).show();
            } else {
//                Toast.makeText(getContext(), "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
                edittextMacAddress.setText(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


}
