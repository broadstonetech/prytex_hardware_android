package hardware.prytex.io.fragment;

import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import hardware.prytex.io.R;
import hardware.prytex.io.adapter.FaqsAdapter;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;


import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;


@SuppressWarnings("ALL")
public class FaqsFragment extends BaseFragment {

    private List<String> listDataHeader;
    private List<String> listDataChild;

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        RecyclerView mRecyclerView = parent.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        imgInfoBtn.setVisibility(View.GONE);

        // preparing list data
        prepareListData();

        FaqsAdapter mAdapter = new FaqsAdapter(getContext(), listDataHeader, listDataChild);
        // setting list adapter
        mRecyclerView.setAdapter(mAdapter);


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
//        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "faqs", null /* class override */);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, "faqs");
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, this.getClass().getName());

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);
    }

    @Override
    public String getTitle() {
        return "FAQs";
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_faqs;
    }



    /*
    * Preparing the list data
    */
    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        listDataChild = new ArrayList<>();

        // Adding child data
        listDataHeader.add(" What is Prytex Hardware?");
        listDataChild.add(" Prytex Hardware is a unique product that allows you to discover network connected devices, detect cybersecurity threats, intrusions, known malware, anomalous device behavior, communication failure, and perform vulnerability assessment on discovered devices.");

        listDataHeader.add(" How does Prytex Hardware protect my network? ");
        listDataChild.add(" Prytex Hardware uses enterprise grade cybersecurity software to discover, detect, and defend against malicious attacks. During the discover phase, Prytex Hardware identifies the connected devices and learns their trusted behavior and then automatically detects any abnormal behavior to alert the user. Prytex Hardware also blocks any malicious activity to proactively shut down both insider and outside attacks. ");

        listDataHeader.add(" How does Prytex Hardware keep up with the changing threat landscape?");
        listDataChild.add(" The Prytex Hardware team is always on the lookout to research and identify any known threats that are identified in the cyber realm. As soon as a remedial action is developed, it is automatically pushed to each Prytex Hardware using our proprietary software update mechanism. No user intervention is required to update the software, so you can be assured that your Prytex Hardware is always running the latest software as long as it is connected to the Internet.");

        listDataHeader.add(" What type of information does Prytex Hardware collect about my network and usage patterns? ");
        listDataChild.add(" The majority of the processing is performed on the Prytex Hardware inside your network and very little information is transmitted. The majority of today’s network traffic is encrypted so the actual traffic payload is hidden from Prytex Hardware. Prytex Hardware only observes the network traffic metadata to perform its behavioral analysis. ");

        listDataHeader.add(" Does Prytex Hardware sell any information collected from my network?");
        listDataChild.add(" We take your privacy very seriously. We do not sell any information collected from our user’s network.  ");

        listDataHeader.add(" If I have a problem or need to report an issue how can I do that? ");
        listDataChild.add(" You can use the chat communication within the mobile io to report any issues or get help on any topic. ");

        listDataHeader.add(" Is there a help manual?");
        listDataChild.add(" A feature guide is included in the mobile io. You can access it from the Help & Tutorials section.");

        listDataHeader.add(" Do the mobile apps cost money?");
        listDataChild.add(" The Prytex Hardware mobile apps are free.");


    }

}
