package hardware.prytex.io.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AlertDialog;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.activity.DashBoardActivity;
import hardware.prytex.io.discovery_mode.DiscoveyHomeFragment;
import hardware.prytex.io.model.User;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.GeneralUtils;
import hardware.prytex.io.util.MyToast;
import hardware.prytex.io.util.SPManager;
import com.google.firebase.messaging.FirebaseMessaging;
//import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;

/**
 * Created by abubaker on 9/26/16.
 */

@SuppressWarnings("ALL")
public class LoginFragment extends BaseFragment implements View.OnClickListener {

    private TextInputEditText mEtUserName;
    //    private EditText mEtPassword;
    //  ShowHidePasswordEditText mEtPassword;
    private TextInputEditText mEtPassword;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Button btnLogin;
    private boolean isShowingPwd = false;

    @Override
    public int getLayoutId() {
        Log.d("LoginFragment","Called");
        return R.layout.login_fragment;
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();

        editor.putString("ShowDeviceDialog", "yes");
        editor.apply();

        final Handler handler = new Handler();
        handler.postDelayed(() -> UrlSelectionDialog(getContext()), 1000);

        String appMode = pref.getString("appMode", "");
        if(appMode.equalsIgnoreCase("dev"))
        {
            DashBoardActivity.BASE_URL = "https://dev.dmoat.com";
        }
        else if (appMode.equalsIgnoreCase("prod"))
        {
            DashBoardActivity.BASE_URL = "https://api.dmoat.com";
        } else {
            DashBoardActivity.BASE_URL = "";
        }
        mEtUserName = parent.findViewById(R.id.et_username);
//        mEtPassword = (EditText) parent.findViewById(R.id.et_password);
        mEtPassword = parent.findViewById(R.id.et_password);

        ImageView imgShowHidePwd = parent.findViewById(R.id.imgShowHidePwd);
        btnLogin = parent.findViewById(R.id.sign_in);
        parent.findViewById(R.id.forgot_password).setOnClickListener(this);
        parent.findViewById(R.id.forgot_username).setOnClickListener(this);
        parent.findViewById(R.id.create_account).setOnClickListener(this);
        parent.findViewById(R.id.sign_in).setOnClickListener(this);

        mEtPassword.setTransformationMethod(new PasswordTransformationMethod());
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        imgShowHidePwd.setOnClickListener(v -> {
            if (isShowingPwd) {
                Log.d("show pwd", "isShowing");
                isShowingPwd = false;
                mEtPassword.setInputType(InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_VARIATION_PASSWORD);
                mEtPassword.setSelection(mEtPassword.getText().length());
            } else {
                Log.d("show pwd", "isNotShowing");
                isShowingPwd = true;
                mEtPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                mEtPassword.setSelection(mEtPassword.getText().length());
            }
        });
        final Context context = getContext();

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.forgot_password) {

            getHelper().addFragment(new ForgotPasswordFragment(), false, true);
        }else if(v.getId() == R.id.forgot_username) {

            getHelper().addFragment(new ForgotUsernameFragment(), false, true);
        }else if(v.getId() == R.id.create_account) {

            getHelper().addFragment(new SignUpFragment(), false, true);
//                for testing purpose move directly to discover screen
//                getHelper().addFragment(new DiscoveyHomeFragment(), false, true);
        }else if(v.getId() == R.id.sign_in) {

            enableFCM();
            String appMode = pref.getString("appMode", "");
            if (appMode.equalsIgnoreCase("dev")) {
                DashBoardActivity.BASE_URL = "https://dev.dmoat.com";
            } else if (appMode.equalsIgnoreCase("prod")) {
                DashBoardActivity.BASE_URL = "https://api.dmoat.com";
            } else {
                DashBoardActivity.BASE_URL = "";
            }

            hideKeyboard();
            if (GeneralUtils.isConnected(getActivity())) {

                if (validateEmail() && validatePassword()) {
                    btnLogin.setBackgroundColor(getResources().getColor(R.color.colorBtnPressed));
                    getHelper().showProgressBar();
                    getParentView().findViewById(R.id.sign_in).setClickable(false);
                    try {
                        Api.userLogin(getActivity(), getLoginString(mEtUserName.getText().toString().toLowerCase(),
                                mEtPassword.getText().toString()), loginListener);
                    } catch (Exception e) {
                        Log.d("LogInException", e.toString());
                    }
                } else {

                    if (!validateEmail() && !validatePassword()) {
                        MyToast.showMessage(getActivity(), getString(R.string.error_message_invalid_emailid));
                    } else {

                        if (!validateEmail()) {
                            MyToast.showMessage(getActivity(), getString(R.string.err_msg_username));
                            setCursorVisibility(mEtUserName);
                            mEtUserName.requestFocus();
                        }

                        if (!validatePassword()) {
                            MyToast.showMessage(getActivity(), getString(R.string.err_msg_password));
                            setCursorVisibility(mEtPassword);
                            mEtPassword.requestFocus();
                        }
                    }
                }
            } else {

                MyToast.showMessage(getActivity(), getResources().getString(R.string.internet_connection));
            }
        }

    }


    private void enableFCM(){
        // Enable FCM via enable Auto-init service which generate new token and receive in FCMService
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
    }

    private boolean validateEmail() {
        return !mEtUserName.getText().toString().trim().isEmpty();

    }


    private boolean validatePassword() {
        return !mEtPassword.getText().toString().trim().isEmpty();
    }

    private final AsyncTaskListener loginListener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            btnLogin.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            if (!isAdded()) {
                return;
            }
            getParentView().findViewById(R.id.sign_in).setClickable(true);
            if (result.code == 200) {
                getHelper().hideProgressBar();

                User user = (User) result.getData();
                UserManager.getInstance().initWithUser(user);
//                UserManager.getInstance().updateUsernamePrivately(user.userName);
                UserManager.getInstance().writeUserToPreferences(user);
                editor.putString("userChatRestoreID", result.chatRestoreID);
                try {
                    editor.putString("app_version", getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0).versionName);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                editor.commit();
                //call intercom
                boolean isShowLoggedDevicesDialog = true;

                startActivity(new Intent(getActivity(), DashBoardActivity.class));
                getActivity().finish();

            }
            //Device is not discovered yet
            else if (result.code == 400) {
                getHelper().hideProgressBar();

                String stringToSearch = result.message;
                String pId = result.product_identifier;
                editor.putString("userChatRestoreID", result.chatRestoreID);
                editor.putString("prodID", pId);
                editor.putString("userEmail", result.userEmailForDiscovery);
                editor.commit();

                Pattern p;
                Matcher m;
                p = Pattern.compile("Device not discovered yet");   // the pattern to search for
                m = p.matcher(stringToSearch);
                if (m.find()) {
                    new AlertDialog.Builder(getContext())
                            .setTitle("Prytex Hardware")
                            .setMessage(getResources().getString(R.string.discovery_incomplete))
                            .setCancelable(false)
                            .setPositiveButton("OK", (dialog, whichButton) -> getHelper().addFragment(new DiscoveyHomeFragment(), true, true)).show();
//                    MyToast.showMessage(getActivity(), getString(R.string.discovery_incomplete));
                } else {
                    MyToast.showMessage(getActivity(), result.message);
                }
            } else if (result.code == 503) {
                getHelper().hideProgressBar();
                showAlertMsgDialog(getContext(), result.message);
            } else if (result.code == 409) {
                getHelper().hideProgressBar();
                showAlertMsgDialog(getContext(), result.message);
            } else {
                getHelper().hideProgressBar();
                if (result.message == "") {
                    MyToast.showMessage(getContext(), getResources().getString(R.string.error_message_generic));
                } else {
                    MyToast.showMessage(getContext(), result.message);
                }
//                MyToast.showMessage(getActivity(), "Error Occurred, Please Try Again!");
            }

        }
    };


    private JSONObject getLoginString(String username, String password) {
        JSONObject js = new JSONObject();
        int req_id = Dmoat.getNewRequestIdd();
        String reqId = String.valueOf(req_id);
        byte[] dataUN;
        byte[] dataPW;
        byte[] dataReqId;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            dataUN = username.getBytes(StandardCharsets.UTF_8);
            dataPW = password.getBytes(StandardCharsets.UTF_8);
            dataReqId = reqId.getBytes(StandardCharsets.UTF_8);
        }else{
            dataUN = username.getBytes(Charset.forName("UTF-8"));
            dataPW = password.getBytes(Charset.forName("UTF-8"));
            dataReqId = reqId.getBytes(Charset.forName("UTF-8"));
        }

        String base64Email = Base64.encodeToString(dataUN, Base64.DEFAULT);
        String base64Pass = Base64.encodeToString(dataPW, Base64.NO_WRAP);
        String base64ReqId = Base64.encodeToString(dataReqId, Base64.DEFAULT);
        StringXORer s = new StringXORer();
        String sXor = s.encode(password, reqId);
        String deviceToken = Settings.Secure.getString(getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("deviceToken", deviceToken);
        try {
            editor.putString("app_id", deviceToken);
            editor.commit();

            js.put("app_id", deviceToken);
            js.put("request_id", req_id);
            JSONObject data = new JSONObject();
            data.put("username", username);
            data.put("password", base64Pass);
            data.put("device_uuid", GeneralUtils.getDeviceID(getActivity()));
            data.put("fcm_device_token", SPManager.getDeviceToken(getActivity()));
            js.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js;

    }


    static class StringXORer {

        String encode(String s, String key) {
            return new String(Base64.encode(xorWithKey(s.getBytes(), key.getBytes()), Base64.DEFAULT));
        }

        private byte[] xorWithKey(byte[] a, byte[] key) {
            byte[] out = new byte[a.length];
            for (int i = 0; i < a.length; i++) {
                out[i] = (byte) (a[i] ^ key[i % key.length]);
            }
            return out;
        }

        private byte[] base64Decode(String s) {
            return Base64.decode(s, Base64.DEFAULT);
        }

    }


    private void UrlSelectionDialog(final Context context) {
//        android.io.AlertDialog.Builder alert = new android.io.AlertDialog.Builder(context);
//        alert.setTitle("Prytex Hardware");
//       alert.setMessage("Select ");
//        alert.setCancelable(false);
//        alert.setPositiveButton("Production Environment", new DialogInterface.OnClickListener() {
//           public void onClick(DialogInterface dialog, int whichButton) {
                editor.putString("appMode", "prod");
                DashBoardActivity.BASE_URL = "https://api.dmoat.com";
                editor.commit();

//           }
//       });
//
//        alert.setNegativeButton("Development Environment", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
//
//                editor.putString("appMode", "dev");
//                DashBoardActivity.BASE_URL = "https://dev.dmoat.com";
//                editor.commit();
//            }
//        });
//        alert.show();
    }
}