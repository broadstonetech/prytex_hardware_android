package hardware.prytex.io.fragment.SensorTabBars;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.util.GeneralUtils;
import hardware.prytex.io.util.MyToast;
import hardware.prytex.io.util.SPManager;

import org.json.JSONException;
import org.json.JSONObject;

import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.activity.DashBoardActivity.setBottomNavTextColors;
import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;

@SuppressWarnings("ALL")
public class SensorMainFragment extends BaseFragment {
    private ProgressDialog dialog;
    private SharedPreferences pref;


    @Override
    public int getLayoutId() {
        return R.layout.fragment_sensor_main;
    }

    @Override
    public String getTitle() {
        return "Sensor Data";
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);

        setBottomNavTextColors(getContext(), false, false, false, false);
        TabLayout tabLayout = parent.findViewById(R.id.tab_layout);
        Button initSensors = parent.findViewById(R.id.btnInitSensors);
        tabLayout.addTab(tabLayout.newTab().setText("Current Data"));
        tabLayout.addTab(tabLayout.newTab().setText("24 hour Data"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        imgInfoBtn.setVisibility(View.VISIBLE);
        final ViewPager viewPager = parent.findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getFragmentManager(), tabLayout.getTabCount(), getContext());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 0) {
                    SPManager.setTermsServicesStatus(getContext(), true);
                    Log.d("Tab", "15values TAB selsected");
                } else {
                    SPManager.setTermsServicesStatus(getContext(), false);
                    Log.d("Tab", "24 hour TAB selsected");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        initSensors.setOnClickListener(view -> {
            if (UserManager.checkPauseInternetTimeStatus()) {
                new AlertDialog.Builder(getContext())
                        .setTitle("Prytex Hardware")
                        .setMessage(getResources().getString(R.string.initialize_sensor))
                        .setPositiveButton("Initialize", (dialog, whichButton) -> {
                            if (GeneralUtils.isConnected(getContext())) {
                                showProgressDialog(true);
                                Api.initSensorRequest(getContext(), getInitSensorParams(), initSensorListener);
                            } else {
                                showAlertMsgDialog(getContext(), getResources().getString(R.string.internet_connection));
                            }
                        })
                        .setNegativeButton("cancel", null).show();
            } else {
                MyToast.showMessage(getContext(), getResources().getString(R.string.paused_internet_msg));
            }
        });

    }

    private JSONObject getInitSensorParams() {
        JSONObject requestData = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            requestData.put("request_id", Dmoat.getNewRequestId());
            requestData.put("app_id", pref.getString("app_id", ""));
            requestData.put("token", UserManager.getInstance().getLoggedInUser().token);
            data.put("resource", "initialize_sensor");
            data.put("request", "get");
            requestData.put("data", data);
            Log.d("initSensorParams", String.valueOf(requestData));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestData;

    }

    //listener init sensor
    private final AsyncTaskListener initSensorListener = result -> {
        showProgressDialog(false);
        if (result.code == 2 || result.code == 200) {
            showAlertMsgDialog(getContext(), "Sensors Initialized.");
        } else {
            showAlertMsgDialog(getContext(), getResources().getString(R.string.error_message_generic));
        }
    };

    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }
}
