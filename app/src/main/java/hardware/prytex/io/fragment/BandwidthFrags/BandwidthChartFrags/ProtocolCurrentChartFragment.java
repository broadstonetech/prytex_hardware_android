package hardware.prytex.io.fragment.BandwidthFrags.BandwidthChartFrags;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.fragment.BaseFragment;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static hardware.prytex.io.activity.DashBoardActivity.imgInfoBtn;
import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;
import static hardware.prytex.io.parser.BandwidthParser.listOfProtocolDevicesCurrentHour;

@SuppressWarnings("ALL")
public class ProtocolCurrentChartFragment extends BaseFragment {


    private ProgressDialog dialog;
    //    private ArrayList<BandWidth> listOfbandwidthDevices = new ArrayList<BandWidth>();
    private RelativeLayout llDetails;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private BarChart mChart;
    private float barWidth;
    private SharedPreferences pref;
    private TextView tvNoDatDetected;


    @Override
    public String getTitle() {
        return "Network Usage";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
//        mFirebaseAnalytics.setCurrentScreen((Activity) getContext(), "network_usage_current_chart", null /* class override */);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, "network_usage_current_chart");
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, this.getClass().getName());

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);

    }


    @Override
    public int getLayoutId() {
        return R.layout.fragment_protocol_chart;
    }

    @Override
    public void initViews(View parent, Bundle savedInstanceState) {
        super.initViews(parent, savedInstanceState);

        pref = getContext().getSharedPreferences("MyPref", 0);

        imgInfoBtn.setVisibility(View.VISIBLE);

        boolean isChartVisible = false;
        boolean isRecyclerViewVisible = false;

        llDetails = parent.findViewById(R.id.rlDetails);
        mSwipeRefreshLayout = parent.findViewById(R.id.bandwidth_swipe_refresh_layout);
        tvNoDatDetected = parent.findViewById(R.id.tv_no_bandwidth_data);

        //barhcart implementation
        mChart = parent.findViewById(R.id.chart);
        mChart.setNoDataText("");

        float barSpace = 0.2f;
        barWidth = 0.5f;      //changes by umer
        //barWidth = 0.9f;

        mChart.setFitBars(true);

        mChart.setDrawGridBackground(false);
        mChart.setTouchEnabled(true);
        mChart.setScaleXEnabled(true);
        mChart.setScaleYEnabled(true);

        mChart.setScaleEnabled(true);
        mChart.setPinchZoom(true);
        mChart.getAxisLeft().setDrawGridLines(false);
        mChart.getXAxis().setDrawGridLines(false);
       mChart.getRendererXAxis().getPaintAxisLabels().setTextAlign(Paint.Align.LEFT);

        mChart.getDescription().setEnabled(false);
        mChart.setDescription(null);
        mChart.getLegend().setEnabled(false);


        mChart.setVisibleXRangeMaximum(7);     //Changes by umer

//        if (isBackFromProtocolDetailsList) {
//            decorateChartView();
//            mChart.invalidate();
//            isBackFromProtocolDetailsList = false;
//        } else {
        showProgressDialog(true);
        Api.getBandwidthDetails(getActivity(), getBandwidthParams(), listener);
//        }
//        llDetails.setVisibility(View.VISIBLE);
//        decorateChartView();
//        mChart.invalidate();
//        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                refreshContent();
//            }
//        });
    }

    private void decorateChartView() {

        List<String> xVals = new ArrayList<>();
        List<BarEntry> yVals = new ArrayList<>();
        for (int i = 0; i < listOfProtocolDevicesCurrentHour.size(); i++) {
            String hName = listOfProtocolDevicesCurrentHour.get(i).getHostName();
            String bar;
            if (hName.length() > 6) {
                bar = hName.substring(0, 7);
            } else {
                bar = hName;
            }
            xVals.add(bar);
            float tUsg = listOfProtocolDevicesCurrentHour.get(i).getTotalUsage();

            //yVals.add(new BarEntry(i + 1, tUsg)); // changes by umer, code commented
            yVals.add(new BarEntry(i , tUsg));

        }


        //region  bar chart region need to be corrected
        BarDataSet set1;
        set1 = new BarDataSet(yVals, "");
//        set1.setDrawIcons(false);
        set1.setColors(new int[]{R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimay}, getContext());

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setValueTextSize(10f);
        data.setBarWidth(barWidth);


//        mChart.setData(data);
        data.setValueFormatter(new MyValueFormatter());
        mChart.setData(data);
        //mChart.moveViewTo(data.getEntryCount() - 7, 50f, YAxis.AxisDependency.LEFT); //changes by umer
        mChart.setVisibleXRangeMaximum(7);      //changes by umer
        mChart.setFitBars(true);
        //X-axis

        XAxis xAxis = mChart.getXAxis();
//        xAxis.setDrawGridLines(false);



        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisMinimum(-0.30f);
        xAxis.setGranularityEnabled(true);
        xAxis.setGranularity(1f);
        //xAxis.setCenterAxisLabels(true);      //Changes by umer, code commented
        xAxis.setDrawGridLines(false);
        xAxis.setTextSize(8);

        xAxis.setLabelCount(xVals.size());
        mChart.getAxisRight().setEnabled(false);
        YAxis leftAxis = mChart.getAxisLeft();
        Log.d("XVals",xVals.toString());
        xAxis.setValueFormatter(new IndexAxisValueFormatter(xVals));
        leftAxis.setAxisMinimum(0f);
        //endregion



//        //region bilawal written in this region
//                BarDataSet set1;
//
//        if (mChart.getData() != null &&
//                mChart.getData().getDataSetCount() > 0) {
//            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
//            set1.setValues(yVals);
//            mChart.getData().notifyDataChanged();
//            mChart.notifyDataSetChanged();
//        } else {
//            set1 = new BarDataSet(yVals, "Data Set");
//            set1.setColors(ColorTemplate.VORDIPLOM_COLORS);
//            set1.setDrawValues(false);
//
//            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
//            dataSets.add(set1);
//
//            BarData data = new BarData(dataSets);
//            mChart.setData(data);
//            mChart.setFitBars(true);
//
//
//            //X-axis
//            XAxis xAxis = mChart.getXAxis();
//
//
//            xAxis.setValueFormatter(new IndexAxisValueFormatter(xVals));
//            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//                    mChart.getAxisRight().setEnabled(false);
//        YAxis leftAxis = mChart.getAxisLeft();
//        leftAxis.setAxisMinimum(0f);
//        }
//
//        mChart.invalidate();
//        //endregion
    }

    private final AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (!isAdded()) {
                return;
            }
            showProgressDialog(false);
            if (result.code == 200) {
                if (listOfProtocolDevicesCurrentHour.size() > 0) {
                    tvNoDatDetected.setVisibility(View.GONE);
                    llDetails.setVisibility(View.VISIBLE);
                    decorateChartView();
                    mChart.invalidate();
                } else {
                    llDetails.setVisibility(View.GONE);
//                    showAlertMsgDialog(getContext(), getResources().getString(R.string.no_bandwidth));
                }

            } else if (result.code == 503) {
                llDetails.setVisibility(View.GONE);
                showAlertMsgDialog(getContext(), result.message);
            } else if (result.code == 409) {
                llDetails.setVisibility(View.GONE);
                showAlertMsgDialog(getContext(), result.message);
            } else {
                llDetails.setVisibility(View.GONE);
                if (result.message.equalsIgnoreCase("")) {
                    showAlertMsgDialog(getContext(), getResources().getString(R.string.error_message_generic));
                } else {
                    showAlertMsgDialog(getContext(), result.message);
                }
            }
        }
    };

    private JSONObject getBandwidthParams() {
        JSONObject js = new JSONObject();
        try {
            js.put("app_id", pref.getString("app_id", ""));
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }


    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }


}
