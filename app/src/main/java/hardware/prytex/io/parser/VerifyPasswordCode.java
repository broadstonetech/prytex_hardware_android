package hardware.prytex.io.parser;

import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONException;
import org.json.JSONObject;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by usman on 12/29/16.
 */

public class VerifyPasswordCode implements BaseParser {
    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        try {
            JSONObject jsonResponse = new JSONObject(response);
            if (jsonResponse.has("token")) {
                result.success(true);
                result.message = jsonResponse.getString("token");
                return result;
            } else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            } else if (httpCode == 503) {
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + jsonResponse.optString("message") + ".";
            } else {
                result.success(false);
                result.code = httpCode;
                result.message = jsonResponse.optString("message");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }
}
