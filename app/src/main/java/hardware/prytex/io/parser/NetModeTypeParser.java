package hardware.prytex.io.parser;

import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONException;
import org.json.JSONObject;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by macbookpro13 on 22/11/2017.
 */

public class NetModeTypeParser implements BaseParser {
    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
//            boolean check = res.optBoolean("success");

            if (httpCode == 200 || httpCode == 2) {
                result.success(true);
                result.message = "success";
                result.NetConfigType = res.optString("type");
                return result;
            }else if (httpCode==402)
            {
                result.message = res.optString("message");
            }
            else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            }else if (httpCode == 503){
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") +".";
            } else {
                result.success(false);
                result.message = "";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Invalid resonse from server: " + response;
        }
        return result;
    }
}
