package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.model.LoggedInDevices;
import hardware.prytex.io.model.User;
import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by abubaker on 8/3/16.
 */

public class    UserLogin implements BaseParser {
    private static final ArrayList<LoggedInDevices> listOfLoggedDevices = new ArrayList<>();

    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        try {
            JSONObject jsonResponse = new JSONObject(response);

            if (httpCode == 200 || httpCode == 2) {
                User userObject = new User(jsonResponse);
                result.setData(userObject);
                result.success(true);

                JSONArray current_logins = jsonResponse.getJSONArray("current_logins");

                Log.d("jsonResponseLogin", jsonResponse.toString());
                String dmoatStatus = jsonResponse.getString("dmoat_status");
                result.chatRestoreID = jsonResponse.optString("chatRestoreId");

                isDmoatOnline = dmoatStatus.equalsIgnoreCase("online");
                Log.d("jsonArrayCurrentLogin", String.valueOf(current_logins.length()));
                for (int i = 0; i < current_logins.length(); i++) {
                    LoggedInDevices logedDevice = new LoggedInDevices();
                    JSONObject b = current_logins.getJSONObject(i);
                    String ip = b.getString("IP");
                    String location = b.getString("location");
                    String timestamp = b.getString("eve_sec");
                    logedDevice.setIp(ip);
                    logedDevice.setLocation(location);
                    logedDevice.setTimeStamp(timestamp);

                    listOfLoggedDevices.add(logedDevice);

                }
                result.code = httpCode;
                result.message = "Login successfully";
                return result;
            } else if (httpCode == 400 || httpCode == 4) {
                if (jsonResponse.has("message")) {
                    result.success(false);
                    result.code = httpCode;
                    result.product_identifier = jsonResponse.optString("prod_id");
                    result.message = jsonResponse.optString("message");
                    result.userEmailForDiscovery = jsonResponse.optString("email");
                    result.chatRestoreID = jsonResponse.optString("chatRestoreId");
                } else {
                    result.success(false);
                    result.code = httpCode;
                    result.product_identifier = jsonResponse.optString("prod_id");
                    result.message = jsonResponse.optString("message");
                }
            } else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            } else if (httpCode == 503) {
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + jsonResponse.optString("message") + ".";
            } else if (jsonResponse.has("message")) {
                result.success(false);
                result.code = httpCode;
                result.message = jsonResponse.optString("message");
            } else {
                result.success(false);
                result.code = httpCode;
//                result.message = jsonResponse.optString("message");
                result.message = "Error Occurred, Please Try Again!";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }
}
