package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONException;
import org.json.JSONObject;

import hardware.prytex.io.fragment.DashBoardFragment;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by macbookpro13 on 02/02/2018.
 */

@SuppressWarnings("ALL")
public class SensorDashboardParser implements BaseParser {
    @Override
    public TaskResult parse(int httpCode, String response) {
        Log.d("Response", response);
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
            result.code = httpCode;
            if (result.code == 200 || result.code == 2) {
                JSONObject resB = res.getJSONObject("message");
                result.success(true);
                result.code = httpCode;
                result.message = res.getString("message");
                DashBoardFragment.isSensorsCall = true;
                DashBoardFragment.isConnectedDevicesCall = false;
                DashBoardFragment.TemperatureValue = resB.optInt("temperature");
                DashBoardFragment.HumidityValue = resB.optInt("humidity");
                DashBoardFragment.AirQualityValue = resB.optInt("coppm");
            } else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            } else if (httpCode == 503) {
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") + ".";
            } else {
                result.code = httpCode;
                result.success(false);
                result.message = "Error Occurred, Please Try Again!";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }
}
