package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONException;
import org.json.JSONObject;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by macbookpro13 on 26/09/2017.
 */

public class PauseInternetParser implements BaseParser {
    @Override
    public TaskResult parse(int httpCode, String response) {
        Log.d("PauseInternet response",response);
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
            if (httpCode == 200 || httpCode == 2){
                result.success(true);
                result.message = "Internet traffic Paused for 10 minutes.";

            }else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            }else if (httpCode == 503){
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") +".";
            }else {
                result.success(false);
                result.code = httpCode;
                result.message = res.getString("message");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }
}