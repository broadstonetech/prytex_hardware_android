package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.model.PolicyPlanModel;
import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by abubaker on 8/4/16.
 */

@SuppressWarnings("ALL")
public class PoliciesListParser implements BaseParser {

    public static final ArrayList<PolicyPlanModel> listOfPolicies = new ArrayList<>();

    @Override
    public TaskResult parse(int httpCode, String response) {
        Log.d("Response", response);
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
//            JSONArray jArray = new JSONArray(response);
                JSONArray jArray = res.optJSONArray("policies");
                result.message = res.optString("message");


            if (httpCode == 200 || httpCode == 2) {
                result.success(true);
                result.code = httpCode;
                result.message = "success";
                if (jArray.length() > 0) {

                    listOfPolicies.clear();
                    for (int jobNo = 0; jobNo < jArray.length(); jobNo++) {
                        JSONObject jObj = jArray.getJSONObject(jobNo);
                        PolicyPlanModel policyModel = new PolicyPlanModel();
                        Integer status = (jObj.getInt("status"));
                        Integer days = (jObj.getInt("days"));
                        String eTime = jObj.getString("e_time");
                        String title = jObj.getString("title");
                        String sTime = jObj.getString("s_time");
                        String sTimeGMT = jObj.getString("s_time_gmt");
                        String eTimeGMT = jObj.getString("e_time_gmt");
                        String id = jObj.getString("id");
                        JSONArray jArrHosts = jObj.getJSONArray("hosts_list");

                        policyModel.setEndTime(eTime);
                        policyModel.setPolicyDays(days);
                        policyModel.setPolicyId(id);
                        policyModel.setPolicyStatus(status);
                        policyModel.setPolicyTitle(title);
                        policyModel.setStartTime(sTime);
                        policyModel.setStartTimeGMT(sTimeGMT);
                        policyModel.setEndTimeGMT(eTimeGMT);
                        policyModel.policyHostsLists = jArrHosts;

                        listOfPolicies.add(policyModel);
                    }
//                    Collections.sort(listOfPolicyDurations, new PolicyDurationParser.DateComparator());
                } else {
                    listOfPolicies.clear();
                    result.message = "No data detected.";

                }

            } else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            } else if (httpCode == 503) {
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") + ".";
            } else {
                result.success(false);
                result.code = httpCode;
                result.message = "No data detected";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.code = httpCode;
        }
        return result;
    }

}
