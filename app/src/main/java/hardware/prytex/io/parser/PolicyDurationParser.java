package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.model.PolicyDurationModel;
import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


import hardware.prytex.io.fragment.Policy.PauseInternetFragment;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by macbookpro13 on 01/03/2018.
 */

public class PolicyDurationParser implements BaseParser {
    public static final ArrayList<PolicyDurationModel> listOfPolicyDurations = new ArrayList<>();

    @Override
    public TaskResult parse(int httpCode, String response) {
        Log.d("Response", response);
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
//            JSONArray jArray = new JSONArray(response);
            JSONObject jsonObject = res.getJSONObject("message");

            if (httpCode == 200 || httpCode == 2) {
                PauseInternetFragment.PolicyCallDuration = jsonObject.getString("duration");
                PauseInternetFragment.PARENTAL_CONTROL_STATUS = jsonObject.getString("parental_control");
                JSONArray jArray = jsonObject.getJSONArray("devices");
                result.success(true);
                result.code = httpCode;
                result.message = "success";
                if (jArray.length() > 0) {

                    listOfPolicyDurations.clear();
                    for (int jobNo = 0; jobNo < jArray.length(); jobNo++) {
                        JSONObject jObj = jArray.getJSONObject(jobNo);
                        String hostname = (jObj.getString("hostname"));
                        String totalDuration = jObj.getString("usage");
                        JSONArray jArraySets = jObj.getJSONArray("details");
                        for (int i = 0; i < jArraySets.length(); i++) {
                            JSONObject jObjSets = jArraySets.getJSONObject(i);
                            PolicyDurationModel alertModel = new PolicyDurationModel();
                            alertModel.setId(jobNo);
                            if (hostname.equalsIgnoreCase("Unknown")){
                                alertModel.setHostName("Unknown==" + jobNo);
                            }else{
                                alertModel.setHostName(hostname);

                            }
                            alertModel.setTotalDuration(totalDuration);
                            alertModel.setConnectTime(jObjSets.getString("time"));
                            alertModel.setSlotUsage(jObjSets.getString("interval"));
                            listOfPolicyDurations.add(alertModel);
                        }

                    }
//                    Collections.sort(listOfPolicyDurations, new PolicyDurationParser.DateComparator());
                } else {
                    listOfPolicyDurations.clear();
                    result.message = "No data detected";

                }

            } else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            } else if (httpCode == 503) {
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") + ".";
            } else {
                result.success(false);
                result.code = httpCode;
                result.message = "No data detected";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.code = httpCode;
        }
        return result;
    }

    //date order

}

