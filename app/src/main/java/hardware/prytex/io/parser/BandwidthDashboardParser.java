package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.model.BandWidth;
import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by macbookpro13 on 02/02/2018.
 */

@SuppressWarnings("ALL")
public class BandwidthDashboardParser implements BaseParser {

    public static final ArrayList<BandWidth> listOfBandwidthForDashboard = new ArrayList<>();

    @Override
    public TaskResult parse(int httpCode, String response) {
        Log.d("Response",response);
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
            result.code = httpCode;
            if (httpCode == 200 || httpCode == 2){
                JSONArray jArray = res.getJSONArray("message");
                result.success(true);
                result.message = "Success";
                result.message = res.getString("message");
                listOfBandwidthForDashboard.clear();
                if (jArray.length() > 0) {
                    //get all list of devices
                    for (int jobNo = 0; jobNo < jArray.length(); jobNo++) {
//                    for (int jobNo = 0; jobNo < 7; jobNo++) {
                        JSONObject jObj = jArray.getJSONObject(jobNo);
                        BandWidth bandwidthModal = new BandWidth();
                        int usage = jObj.getInt("totalusage");
                        if (!(usage == 0)) {

//                            let convertedMB = Double(totalUsage) / 1048576
//                            let flotMBs = Double(round(1000*convertedMB)/1000)

                            double usg = (double) usage / 1048576;//1048576;
                            if(usg <= 0.1){
                                usg = 0.1;
                            }
                            String convertedUsg = String.format("%.3f" , usg);
                            float parsedMB = Float.parseFloat(convertedUsg);
                            bandwidthModal.setTotalUsage(parsedMB);
                            bandwidthModal.setHostIP(jObj.getString("hostip"));
//                            bandwidthModal.setHostIP(jObj.getString("hostip"));
//                            bandwidthModal.setHostID(jObj.getString("hostid"));
                            String hostName = jObj.getString("hostname");
                            if (hostName.equalsIgnoreCase("") || hostName.equalsIgnoreCase("None")) {
                                bandwidthModal.setHostName("Unknown");
                            } else {
                                bandwidthModal.setHostName(hostName);
                            }
                            bandwidthModal.setHostOS(jObj.getString("parameters"));
                            String ref = jObj.getString("protocols");
                            bandwidthModal.setProtocolsList(ref);

                            listOfBandwidthForDashboard.add(bandwidthModal);
                        }

                    }
                    Collections.sort(listOfBandwidthForDashboard, new DateComparator());

                }else {
                    result.message = "No bandwidth stat received";
                    listOfBandwidthForDashboard.clear();
                }

            } else  if (httpCode == 409) {
                listOfBandwidthForDashboard.clear();
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            }else if (httpCode == 503){
                result.code = httpCode;
                listOfBandwidthForDashboard.clear();
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") +".";
            }
            else {
                result.code = httpCode;
                result.message = "Error Occurred, Please Try Again!";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }

    static class DateComparator implements Comparator<BandWidth> {

        @Override
        public int compare(BandWidth lhs, BandWidth rhs) {
            Double distance = (double) lhs.getTotalUsage();
            Double distance1 = (double) rhs.getTotalUsage();
            return Integer.compare(distance1.compareTo(distance), 0);
        }
    }
}
