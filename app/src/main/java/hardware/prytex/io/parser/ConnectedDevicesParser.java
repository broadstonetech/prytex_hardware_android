package hardware.prytex.io.parser;

import android.annotation.SuppressLint;

import hardware.prytex.io.model.ConnectedDeviceList;
import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by applepc on 25/04/2017.
 */

@SuppressWarnings("ALL")
public class ConnectedDevicesParser implements BaseParser {
    public static boolean isDevicesListReceived = false;
    private static final ArrayList<ConnectedDeviceList> listOfActiveDevice = new ArrayList<>();
    public static final ArrayList<ConnectedDeviceList> listOfConnetcedDevice = new ArrayList<>();

    @SuppressLint("LongLogTag")
    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        try {

            result.code = httpCode;
            JSONObject res = new JSONObject(response);
            if (result.code == 200 || result.code == 2) {
                JSONArray jArray = res.getJSONArray("message");

                result.success(true);
                result.code = httpCode;
                result.message = "success";


                listOfActiveDevice.clear();
                listOfConnetcedDevice.clear();
                if (jArray.length() > 0) {
                    isDevicesListReceived = true;
                    //get all list of devices
                    for (int jobNo = 0; jobNo < jArray.length(); jobNo++) {
                        JSONObject jObj = jArray.getJSONObject(jobNo);
                        ConnectedDeviceList connectedDeviceModal = new ConnectedDeviceList();

                        connectedDeviceModal.setEveSec_cd(jObj.getInt("eve_sec"));
                        connectedDeviceModal.setHostId_cd(jObj.getString("HOSTID"));
                        connectedDeviceModal.setIp_cd(jObj.getString("ip"));
                        connectedDeviceModal.setMacAddress_cd(jObj.getString("macaddress"));
//                         connectedDeviceModal.setDeviceType(jObj.getString("type"));  //active/cnctdhost

//                        int highCount = jObj.getInt("highCount");
//                        int lowCount = jObj.getInt("lowCount");
//                        int mediumCount = jObj.getInt("mediumCount");
                        String hostName = jObj.getString("hostname");
                        String recordId = jObj.getString("record_id");
                        String Os = jObj.getString("OS");
                        String parameters = jObj.getString("parameters");
                        String deviceCategory = jObj.getString("device_category");
                        String userName = jObj.getString("usersname");
                        int score = jObj.getInt("score");
                        connectedDeviceModal.jsonArrayPorts = jObj.getJSONArray("activeports");
                        connectedDeviceModal.jsonArrayDefaultPasswords = jObj.getJSONArray("default_password");
//                        if (highCount == 0) {
//                            connectedDeviceModal.setHighCount_cd(0);
//                        } else {
//                            connectedDeviceModal.setHighCount_cd(highCount);
//                        }
//                        if (lowCount == 0) {
//                            connectedDeviceModal.setLowCount_cd(0);
//                        } else {
//                            connectedDeviceModal.setLowCount_cd(lowCount);
//                        }
//                        if (mediumCount == 0) {
//                            connectedDeviceModal.setMediumCount_cd(0);
//                        } else {
//                            connectedDeviceModal.setMediumCount_cd(mediumCount);
//                        }
                        if (hostName == null || hostName.equalsIgnoreCase("null") || hostName.equalsIgnoreCase("")) {
                            connectedDeviceModal.setHostName_cd("Unknown");
                        } else {
                            connectedDeviceModal.setHostName_cd(hostName);
                        }
                        if (recordId == null || recordId.equalsIgnoreCase("null") || recordId.equalsIgnoreCase("")) {
                            connectedDeviceModal.setRecord_id("Unknown");
                        } else {
                            connectedDeviceModal.setRecord_id(recordId);
                        }
                        if (Os == null || Os.equalsIgnoreCase("null") || Os.equalsIgnoreCase("")) {
                            connectedDeviceModal.setOs_cd("Unknown");
                        } else {
                            connectedDeviceModal.setOs_cd(Os);
                        }
                        if (parameters == null || parameters.equalsIgnoreCase("null") || parameters.equalsIgnoreCase("")) {
                            connectedDeviceModal.setParametrs_cd("Unknown");
                        } else {
                            connectedDeviceModal.setParametrs_cd(parameters);
                        }
                        if (deviceCategory == null || deviceCategory.equalsIgnoreCase("null") || deviceCategory.equalsIgnoreCase("")) {
                            connectedDeviceModal.setdeviceCategory("Unknown");
                        } else {
                            connectedDeviceModal.setdeviceCategory(deviceCategory);
                        }
                        if (userName == null || userName.equalsIgnoreCase("null") || userName.equalsIgnoreCase("")) {
                            connectedDeviceModal.setUserName("Unknown");
                        } else {
                            connectedDeviceModal.setUserName(userName);
                        }
//                        if (score > 0) {
                        connectedDeviceModal.setScore(score);
                        connectedDeviceModal.setInPolicy(false);
                        listOfConnetcedDevice.add(connectedDeviceModal);
//                        }else{}
                    }
                    Collections.sort(listOfConnetcedDevice, new ConnectedDevicesParser.DateComparator());

                } else {
                    isDevicesListReceived = false;
                    result.message = "No data detected";
                }

            } else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
                listOfActiveDevice.clear();
                listOfConnetcedDevice.clear();
            } else if (httpCode == 503) {
                listOfActiveDevice.clear();
                listOfConnetcedDevice.clear();
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") + ".";
            } else {
                isDevicesListReceived = false;
                result.success(false);
                result.code = httpCode;
                result.message = "No data detected";
                listOfActiveDevice.clear();
                listOfConnetcedDevice.clear();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.code = httpCode;
            isDevicesListReceived = false;
            listOfActiveDevice.clear();
            listOfConnetcedDevice.clear();
        }
        return result;
    }

    class DateComparator implements Comparator<ConnectedDeviceList> {

        @Override
        public int compare(ConnectedDeviceList lhs, ConnectedDeviceList rhs) {

            byte[] ba1 = lhs.getIp_cd().getBytes();
            byte[] ba2 = rhs.getIp_cd().getBytes();

            // general ordering: ipv4 before ipv6
            if (ba1.length < ba2.length) return -1;
            if (ba1.length > ba2.length) return 1;

            // we have 2 ips of the same type, so we have to compare each byte
            for (int i = 0; i < ba1.length; i++) {
                int b1 = unsignedByteToInt(ba1[i]);
                int b2 = unsignedByteToInt(ba2[i]);
                if (b1 == b2)
                    continue;
                if (b1 < b2)
                    return -1;
                else
                    return 1;
            }
            return 0;
        }
    }

    private int unsignedByteToInt(byte b) {
        return (int) b & 0xFF;
    }

}