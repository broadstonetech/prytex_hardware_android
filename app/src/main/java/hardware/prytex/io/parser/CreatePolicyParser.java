package hardware.prytex.io.parser;

import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONException;
import org.json.JSONObject;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by abubaker on 8/4/16.
 */

public class CreatePolicyParser implements BaseParser {
    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
            if (httpCode == 200 || httpCode == 2) {
                result.success(true);
                result.code = httpCode;
                result.message = "Policy Created successfully";
            }else if (httpCode == 409) {
            isDmoatOnline = false;
            result.success(false);
            result.code = httpCode;
            result.message = "Prytex Hardware is inactive.";
        }else if (httpCode == 503){
            result.code = httpCode;
            result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") +".";
        }else {
            result.success(false);
            result.code = httpCode;
            result.message = res.optString("message");
        }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.code = httpCode;
        }
        return result;
    }
}
