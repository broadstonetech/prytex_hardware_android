package hardware.prytex.io.parser;

import hardware.prytex.io.model.NetConfig;
import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONException;
import org.json.JSONObject;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by macbookpro13 on 18/09/2017.
 */

public class NetConfigParser implements BaseParser {
    public static NetConfig netConfigObj;
    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
//            boolean check = res.optBoolean("success");

            if (httpCode == 200) {
                result.success(true);
                result.message = "message";

                JSONObject jsonObjNetConfig = new JSONObject(response.trim());
                JSONObject jsonObj = jsonObjNetConfig.getJSONObject("message");
                netConfigObj = new NetConfig();
                netConfigObj.setStart_ip_range(jsonObj.getString("start_ip_range"));
                netConfigObj.setEnd_ip_range(jsonObj.getString("end_ip_range"));
                netConfigObj.setRouter_ip(jsonObj.getString("router_ip"));
                netConfigObj.setIp(jsonObj.getString("ip"));
                netConfigObj.setNetmask(jsonObj.getString("netmask"));
                netConfigObj.setSubnet(jsonObj.getString("subnet"));
                netConfigObj.setDefault_lease_time(Integer.parseInt(jsonObj.getString("default_lease_time")));
                netConfigObj.setMax_lease_time(Integer.parseInt(jsonObj.getString("max_lease_time")));
                netConfigObj.setGateway(jsonObj.getString("gateway"));
                netConfigObj.setBroadcast(jsonObj.getString("broadcast"));

                return result;
            } else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            }else if (httpCode == 503){
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") +".";
            }else {
                result.success(false);
                result.message = res.optString("message");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Invalid resonse from server: " + response;
        }
        return result;
    }
}

