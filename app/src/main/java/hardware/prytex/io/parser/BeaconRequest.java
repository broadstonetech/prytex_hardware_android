package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by applepc on 08/03/2017.
 */

public class BeaconRequest implements BaseParser {
    public static String nameSpace;
    public static String KeyThirtyTwo;
    public static JSONArray numListArray;
    public static boolean isSuccessComplete = false;
    public static boolean isMessage200 = false;
    public static boolean isNameSpace200 = false;
    public static boolean isSuccess200 = false;

    @Override
    public TaskResult parse(int httpCode, String response) {
        Log.d("BeaconReq response",response);
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
            result.code = httpCode;
            String Check = String.valueOf(httpCode);
            if (httpCode == 200 || httpCode == 2) {
//                nameSpace = res.getString("message");

                Pattern p = Pattern.compile("message");   // the pattern to search for
                Pattern p2 = Pattern.compile("namespace");   // the pattern to search for
                Pattern p3 = Pattern.compile("success");   // the pattern to search for
                Matcher m = p.matcher(response);
                Matcher m2 = p2.matcher(response);
                Matcher m3 = p3.matcher(response);

                // now try to find at least one match
                if (m.find()) {
                    isMessage200 = true;
                    isSuccess200 = false;
                    isNameSpace200 = false;
                } else if (m2.find()) {
                    isNameSpace200 = true;
                    isMessage200 = false;
                    isSuccess200 = false;
                }else if (m3.find()){
                    isSuccess200 = true;
                    isNameSpace200 = false;
                    isMessage200 = false;
                }



                if (isNameSpace200){
                    nameSpace = res.getString("namespace");
                }else if (isSuccess200){
                    isSuccessComplete = true;
                }
                else if (isMessage200){
                    KeyThirtyTwo = res.getJSONObject("message").getString("key");
                    numListArray = res.getJSONObject("message").getJSONArray("num_list");
                }
                result.success(true);
                result.message = "Successfully received namespace!";

            } else  if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            }else if (httpCode == 503){
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") +".";
            }else{
                //keep on pinging/read doc
                result.success(false);
                result.code = httpCode;
                result.message = res.getString("message");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
        }
}
