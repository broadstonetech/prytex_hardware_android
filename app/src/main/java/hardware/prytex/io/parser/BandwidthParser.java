package hardware.prytex.io.parser;

import android.annotation.SuppressLint;

import hardware.prytex.io.model.BandWidth;
import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by macbookpro13 on 09/05/2017.
 */

@SuppressWarnings("ALL")
public class BandwidthParser implements BaseParser {

    public static final ArrayList<BandWidth> listOfProtocolDevices = new ArrayList<>();
    public static final ArrayList<BandWidth> listOfProtocolDevicesCurrentHour = new ArrayList<>();


    @SuppressLint("LongLogTag")
    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        try {


            JSONObject res = new JSONObject(response);
            result.code = httpCode;
            if (result.code == 200 || result.code == 2) {
                JSONArray jArray = res.getJSONArray("message_trafficstats_24hour");
                JSONArray jArrayCurHour = res.getJSONArray("message_trafficstats_1hour");
                result.success(true);
                result.code = httpCode;
                result.message = "success";

                BandWidth bandwidthModal;
                if (jArray.length() > 0) {
                    listOfProtocolDevices.clear();
                    //get all list of devices
                    for (int jobNo = 0; jobNo < jArray.length(); jobNo++) {
//                    for (int jobNo = 0; jobNo < 7; jobNo++) {
                        JSONObject jObj = jArray.getJSONObject(jobNo);
                        bandwidthModal = new BandWidth();
                        int usage = jObj.getInt("totalusage");
                        if (!(usage == 0)) {
                            double usg = (double) usage / 1048576;//1048576; //aaa
                            //String convertedUsg = String.format("%.2f" , usg);
                            if(usg <= 0.1){
                                usg = 0.1;
                            }
                            DecimalFormat decimalFormat = new DecimalFormat("#.##");
                            float parsedMB = Float.parseFloat(decimalFormat.format(usg));
                            bandwidthModal.setTotalUsage(parsedMB);
                            bandwidthModal.setHostIP(jObj.getString("hostip"));
//                            bandwidthModal.setHostIP(jObj.getString("hostip"));
                            bandwidthModal.setHostID(jObj.getString("hostid"));
                            String hostName = jObj.getString("hostname");
                            if (hostName.equalsIgnoreCase("") || hostName.equalsIgnoreCase("None")) {
                                bandwidthModal.setHostName("Unknown");
                            } else {
                                bandwidthModal.setHostName(hostName);
                            }
                            bandwidthModal.setHostOS(jObj.getString("parameters"));
                            String ref = jObj.getString("protocols");
                            bandwidthModal.setProtocolsList(ref);
                            bandwidthModal.setCurrent(false);

                            listOfProtocolDevices.add(bandwidthModal);
                        }

                    }
                    Collections.sort(listOfProtocolDevices, new DateComparator());

                } else {
                    result.message = "No bandwidth stat received";
                    listOfProtocolDevices.clear();
                }

                //cureent hour bandwidth
                if (jArrayCurHour.length() > 0) {
                    listOfProtocolDevicesCurrentHour.clear();
                    //get all list of devices
                    for (int jobNo = 0; jobNo < jArrayCurHour.length(); jobNo++) {
//                    for (int jobNo = 0; jobNo < 7; jobNo++) {
                        JSONObject jObjCur = jArrayCurHour.getJSONObject(jobNo);
                        bandwidthModal = new BandWidth();
                        int usage = jObjCur.getInt("totalusage");
                        if (!(usage == 0)) {
                            double usg = (double) usage / 1048576;//1048576;
//                            String convertedUsg = String.format("%.2f" , usg);
//                            float parsedMB = Float.parseFloat(convertedUsg);
                            if(usg <= 0.1){
                                usg = 0.1;
                            }
                            DecimalFormat decimalFormat = new DecimalFormat("#.##");
                            float parsedMB = Float.parseFloat(decimalFormat.format(usg));
                            bandwidthModal.setTotalUsage(parsedMB);
                            bandwidthModal.setHostIP(jObjCur.getString("hostip"));
//                            bandwidthModal.setHostIP(jObj.getString("hostip"));
                            bandwidthModal.setHostID(jObjCur.getString("hostid"));
                            String hostName = jObjCur.getString("hostname");
                            if (hostName.equalsIgnoreCase("") || hostName.equalsIgnoreCase("None")) {
                                bandwidthModal.setHostName("Unknown");
                            } else {
                                bandwidthModal.setHostName(hostName);
                            }
                            bandwidthModal.setHostOS(jObjCur.getString("parameters"));
                            String ref = jObjCur.getString("protocols");
                            bandwidthModal.setProtocolsList(ref);
                            bandwidthModal.setCurrent(true);

                            listOfProtocolDevicesCurrentHour.add(bandwidthModal);
                        }

                    }
                    Collections.sort(listOfProtocolDevicesCurrentHour, new DateComparator());

                } else {
                    result.message = "No bandwidth stat received";
                    listOfProtocolDevicesCurrentHour.clear();
                }

            } else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
                listOfProtocolDevices.clear();
                listOfProtocolDevicesCurrentHour.clear();
            } else if (httpCode == 503) {
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") + ".";
            } else {
                result.success(false);
                result.code = httpCode;
                result.message = "Unable to fetch Bandwidth devices!";
                listOfProtocolDevices.clear();
                listOfProtocolDevicesCurrentHour.clear();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.code = httpCode;
            listOfProtocolDevices.clear();
            listOfProtocolDevicesCurrentHour.clear();
        }
        return result;
    }

    static class DateComparator implements Comparator<BandWidth> {

        @Override
        public int compare(BandWidth lhs, BandWidth rhs) {
            Double distance = (double) lhs.getTotalUsage();
            Double distance1 = (double) rhs.getTotalUsage();
            return Integer.compare(distance1.compareTo(distance), 0);
        }
    }
}