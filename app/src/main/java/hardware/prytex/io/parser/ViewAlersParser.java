package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.model.MutedBlockedAlertsModel;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by macbookpro13 on 17/01/2018.
 */

@SuppressWarnings("ALL")
public class ViewAlersParser implements BaseParser {
    public static final ArrayList<MutedBlockedAlertsModel> listOfAllViewAlerts = new ArrayList<>();

    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();

        String seenStatus = "unseen";
        try {
            JSONObject res = new JSONObject(response);
//            JSONArray jArray = new JSONArray(response);
            Log.d("responseGetAlerts", response);

            if (httpCode == 200 || httpCode == 2) {
                JSONArray jArray = res.getJSONArray("message");
                result.success(true);
                result.code = httpCode;
                result.message = "success";
                if (jArray.length() > 0) {

                    listOfAllViewAlerts.clear();
                    for (int jobNo = 0; jobNo < jArray.length(); jobNo++) {
                        JSONObject jObj = jArray.getJSONObject(jobNo);
                        MutedBlockedAlertsModel alertModel = new MutedBlockedAlertsModel();

                        String inputSrc = jObj.getString("input_src");

                        int size = listOfAllViewAlerts.size();
                        if (inputSrc.equals(MutedBlockedAlertsModel.BLOCK_IPS_ALERT) || inputSrc.equals(MutedBlockedAlertsModel.FLOW_ALERT) ||
                                inputSrc.equals(MutedBlockedAlertsModel.IDS_ALERT) || inputSrc.equals(MutedBlockedAlertsModel.INFO_ALERT) ||
                                inputSrc.equals(MutedBlockedAlertsModel.NEW_HOST_ALERT) ||
                                inputSrc.equals(MutedBlockedAlertsModel.SENSOR_ALERT) ||
                                inputSrc.equals(MutedBlockedAlertsModel.UPDATE) )
                        {
                            if (inputSrc.equalsIgnoreCase("ids")) {

                                alertModel.setInput_src(inputSrc);
                                alertModel.setRecord_id(jObj.getString("record_id"));
                                alertModel.setSrc_port(jObj.getString("src_port"));
                                alertModel.setDescription(jObj.getString("description"));
                                alertModel.setEve_sec(jObj.getInt("eve_sec"));
                                alertModel.setDestHostName(jObj.getString("dest_hostname"));
                                alertModel.setSrcHostName(jObj.getString("src_hostname"));
                                alertModel.setIdsBlockType(jObj.getString("blocktype"));
                                alertModel.setSrc_ip(jObj.getString("src_ip"));
                                alertModel.setDest_port(jObj.getString("dest_port"));
                                alertModel.setDesc_ip(jObj.getString("dest_ip"));
                                seenStatus = jObj.getString("seen");

                            } else if (inputSrc.equalsIgnoreCase("flow")) {

                                //flowAlertJsonArr
//                                alertModel.setRecord_id(jObj.getString("record_ids"));
                                alertModel.setInput_src(jObj.getString("input_src"));
//                                alertModel.setDescription(jObj.getString("description"));
                                alertModel.setEve_sec(jObj.getInt("eve_sec"));
                                alertModel.setSlotId(jObj.getInt("slot_id"));
//                                String alertType = jObj.getString("type");
                                seenStatus = jObj.getString("seen");
                                alertModel.flowAlertJsonArr = jObj.getJSONArray("alerts");
                                alertModel.flowRecordIdsList = jObj.getJSONArray("record_ids");
//                                alertModel.setFlowType(alertType);
//                                if (jObj.optString("detail").equalsIgnoreCase("unknown") || jObj.optString("detail").equalsIgnoreCase("malicios")) {
//                                    alertModel.setflowDetail(jObj.optString("detail"));
//                                } else {
//                                    alertModel.setflowDetail("Unknown");
//                                }

                            } else if (inputSrc.equalsIgnoreCase("cnctdhost")) {

                                alertModel.setRecord_id(jObj.getString("record_id"));
                                alertModel.setInput_src(jObj.getString("input_src"));
                                alertModel.setDescription(jObj.getString("description"));
                                alertModel.setEve_sec(jObj.getInt("eve_sec"));
                                alertModel.setDeviceCategory(jObj.getString("device_category"));
                                alertModel.setHostName(jObj.getString("hostname"));
                                alertModel.setIp(jObj.getString("ip"));
                                alertModel.setOS(jObj.getString("OS"));
                                seenStatus = jObj.getString("seen");

                            } else if (inputSrc.equalsIgnoreCase("sensor")) {

                                alertModel.setRecord_id(jObj.getString("record_id"));
                                alertModel.setInput_src(jObj.getString("input_src"));
                                alertModel.setDescription(jObj.getString("description"));
                                alertModel.setEve_sec(jObj.getInt("eve_sec"));
                                alertModel.setCategory(jObj.getString("category"));
                                alertModel.setSensorReading(jObj.getLong("reading"));
                                seenStatus = jObj.getString("seen");

                            } else if (inputSrc.equalsIgnoreCase("info")) {

                                alertModel.setRecord_id(jObj.getString("record_id"));
                                alertModel.setInput_src(jObj.getString("input_src"));
                                alertModel.setDescription(jObj.getString("description"));
                                alertModel.setEve_sec(jObj.getInt("eve_sec"));
                                seenStatus = jObj.getString("seen");
                                String alertType = jObj.getString("type");
                                alertModel.setInfo_type(alertType);
                                if (alertType.equalsIgnoreCase("internet_restarted")) {
                                    alertModel.setTimeOn(jObj.getInt("on"));
                                    alertModel.setTimeOff(jObj.getInt("off"));
                                }


                            } else if (inputSrc.equalsIgnoreCase("blockips")) {

                                alertModel.setRecord_id(jObj.getString("record_id"));
                                alertModel.setInput_src(jObj.getString("input_src"));
                                alertModel.setDescription(jObj.getString("description"));
                                alertModel.setEve_sec(jObj.getInt("eve_sec"));
                                alertModel.setIp(jObj.getString("ip"));
                                seenStatus = jObj.getString("seen");

                            }else if (inputSrc.equalsIgnoreCase("update")) {

                                alertModel.setRecord_id(jObj.getString("record_id"));
                                alertModel.setInput_src(jObj.getString("input_src"));
                                alertModel.setDescription(jObj.getString("description"));
                                alertModel.setVersion(jObj.optString("version"));
                                alertModel.setEve_sec(jObj.getInt("eve_sec"));
                                seenStatus = jObj.getString("seen");
                                alertModel.setInfo_type(jObj.getString("type"));


                            }
                            else {
                            }

                            alertModel.setNamespace(UserManager.getInstance().getLoggedInUser().nameSpace);
                            alertModel.setAlertId(jobNo);
                            if (seenStatus.equalsIgnoreCase("0")) {
                                alertModel.setSeenStatus(false);
                            } else {
                                alertModel.setSeenStatus(true);
                            }
                            listOfAllViewAlerts.add(alertModel);
                        } else {
                        }

                    }
                    Collections.sort(listOfAllViewAlerts, new DateComparator());

                } else {
                    listOfAllViewAlerts.clear();
                    result.message = "No data detected.";

                }

            } else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = res.optString("message");
                if (result.message.equalsIgnoreCase("")) {
                    result.message = "Prytex Hardware is inactive.";
                }
            } else if (httpCode == 503) {
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") + ".";
            } else {
                result.success(false);
                result.code = httpCode;
                result.message = "No data detected";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.code = httpCode;
        }
        return result;
    }

    //date order
    static class DateComparator implements Comparator<MutedBlockedAlertsModel> {

        @Override
        public int compare(MutedBlockedAlertsModel lhs, MutedBlockedAlertsModel rhs) {
            Double distance = (double) lhs.getEve_sec();
            Double distance1 = (double) rhs.getEve_sec();
            return Integer.compare(distance1.compareTo(distance), 0);
        }
    }
}
