package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.model.PolicyPlanModel;
import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

@SuppressWarnings("ALL")
public class PotocolPoliciesParser implements BaseParser {

    public static final ArrayList<PolicyPlanModel> listOfProtocolPolicies = new ArrayList<>();

    @Override
    public TaskResult parse(int httpCode, String response) {
        Log.d("Response", response);
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
//            JSONArray jArray = new JSONArray(response);
            JSONArray jArray = res.optJSONArray("blockapps");
            result.message = res.optString("message");


            if (httpCode == 200 || httpCode == 2) {
                result.success(true);
                result.code = httpCode;
                result.message = "success";
                if (jArray.length() > 0) {

                    listOfProtocolPolicies.clear();
                    for (int jobNo = 0; jobNo < jArray.length(); jobNo++) {
                        JSONObject jObj = jArray.getJSONObject(jobNo);
                        PolicyPlanModel policyModel = new PolicyPlanModel();
//                        JSONArray procedure = jObj.getJSONArray("data");
                        String data_type = jObj.getString("data_type");
                        JSONObject data = jObj.getJSONObject("data");
                        String type = data.getString("type");
                        if (type.equalsIgnoreCase("network")){
                            policyModel.setHostName("Network");
                        }else{
                            String macAdress = data.getString("macaddress");
                            String hostId = data.getString("hostid");
                            String hostIp = data.getString("ip");
                            String hostName = data.getString("hostname");

                            policyModel.setHostMacAddress(macAdress);
                            policyModel.setHostIpAdress(hostIp);
                            policyModel.setHostName(hostName);
                            policyModel.setHostId(hostId);
                        }
                        String id = data.getString("id");
                        String title = data.getString("title");
                        String request = data.getString("request");
                        String procedure = data.getString("procedure");
                        JSONArray jArrHosts = data.getJSONArray("protocols");

                        policyModel.setPolicyTitle(title);
                        policyModel.setType(type);
                        policyModel.setProcedure(procedure);
                        policyModel.setRequest(request);
                        policyModel.setPolicyId(id);
                        policyModel.setDataType(data_type);
                        policyModel.policyProtocolsLists = jArrHosts;

                        listOfProtocolPolicies.add(policyModel);
                    }
//                    Collections.sort(listOfPolicyDurations, new PolicyDurationParser.DateComparator());
                } else {
                    listOfProtocolPolicies.clear();
                    result.message = "No data detected.";

                }

            } else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            } else if (httpCode == 503) {
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") + ".";
            } else {
                result.success(false);
                result.code = httpCode;
                result.message = "No data detected";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.code = httpCode;
        }
        return result;
    }

}
