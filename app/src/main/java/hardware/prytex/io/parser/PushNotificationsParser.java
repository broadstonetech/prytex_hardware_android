package hardware.prytex.io.parser;

import hardware.prytex.io.model.PushNotificationsClass;
import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONException;
import org.json.JSONObject;

public class PushNotificationsParser implements BaseParser {

    public static PushNotificationsClass objNoti= new PushNotificationsClass();
    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        result.code = httpCode;

        try {
            JSONObject jsonResponse = new JSONObject(response);

            if (httpCode == 200 || httpCode == 2) {
                result.setData(jsonResponse);
                result.success(true);
                objNoti.setCheck(true);
                JSONObject object = jsonResponse.getJSONObject("data");
                objNoti.setBlocked(object.optInt("blocked"));
                objNoti.setConnectedHost(object.optInt("cnctd_host"));
                objNoti.setDevices(object.optInt("devices"));
                objNoti.setInfo(object.optInt("info"));
                objNoti.setSensors(object.optInt("sensors"));


                return result;
            }
            else
            {

                result.message = jsonResponse.optString("message");
                return result;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }
}
