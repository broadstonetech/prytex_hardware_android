package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.model.MutedBlockedAlertsModel;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import hardware.prytex.io.fragment.DashBoardFragment;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by macbookpro13 on 02/02/2018.
 */

public class AlertsDashboardParser implements BaseParser {

    public static String parentalModeStatus= "";
    public static String modeStatus= "";
    public static final ArrayList<MutedBlockedAlertsModel> listOfAllViewAlertsForDashboard = new ArrayList<>();

    @Override
    public TaskResult parse(int httpCode, String response) {
        Log.d("Response", response);
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
            result.code = httpCode;

            if (result.code == 200 || result.code == 2) {
                JSONObject resB = res.getJSONObject("message");
                JSONArray jArray = resB.getJSONArray("alerts");
                result.success(true);
                result.code = httpCode;
                DashBoardFragment.isAlertsCall = true;
                DashBoardFragment.isConnectedDevicesCall = false;
                result.message = res.optString("message");
                DashBoardFragment.InfoAlertsCount = resB.optInt("info");
                DashBoardFragment.FlowAlertsCount = resB.optInt("flow");
                DashBoardFragment.BlockedAlertsCount = resB.optInt("blocked");

                if (jArray.length() > 0) {

                    listOfAllViewAlertsForDashboard.clear();
                    for (int jobNo = 0; jobNo < jArray.length(); jobNo++) {
                        JSONObject jObj = jArray.getJSONObject(jobNo);
                        MutedBlockedAlertsModel alertModel = new MutedBlockedAlertsModel();

                        String inputSrc = jObj.getString("input_src");

                            if (inputSrc.equalsIgnoreCase("ids")) {


                                alertModel.setInput_src(inputSrc);

                                alertModel.setIdsBlockType(jObj.getString("blocktype"));
                                alertModel.setDescription(jObj.getString("description"));
                            } else if (inputSrc.equalsIgnoreCase("flow")) {

                                alertModel.setInput_src(inputSrc);
                                alertModel.setDescription(jObj.getString("description"));
                                String alertType = jObj.getString("type");
                                alertModel.setFlowType(alertType);
                                if (jObj.optString("detail").equalsIgnoreCase("unknown") || jObj.optString("detail").equalsIgnoreCase("malicios")) {
                                    alertModel.setflowDetail(jObj.optString("detail"));
                                } else {
                                    alertModel.setflowDetail("Unknown");
                                }

                            } else if (inputSrc.equalsIgnoreCase("cnctdhost")) {

                                alertModel.setInput_src(inputSrc);
                                alertModel.setDescription(jObj.getString("description"));

                                alertModel.setNamespace(UserManager.getInstance().getLoggedInUser().nameSpace);
                                alertModel.setAlertId(jobNo);
                                listOfAllViewAlertsForDashboard.add(alertModel);

                            } else if (inputSrc.equalsIgnoreCase("sensor")) {

                                alertModel.setInput_src(inputSrc);
                                alertModel.setDescription(jObj.getString("description"));
                                alertModel.setCategory(jObj.getString("category"));
                                alertModel.setSensorReading(jObj.getLong("reading"));

                            } else if (inputSrc.equalsIgnoreCase("info")) {

                                alertModel.setInput_src(inputSrc);
                                alertModel.setDescription(jObj.getString("description"));
                                String alertType = jObj.getString("type");
                                alertModel.setInfo_type(alertType);
                                if (alertType == "internet_restarted") {
                                    alertModel.setTimeOn(jObj.getInt("on"));
                                    alertModel.setTimeOff(jObj.getInt("off"));
                                }


                            } else if (inputSrc.equalsIgnoreCase("blockips")) {

                                alertModel.setInput_src(inputSrc);
                                alertModel.setDescription(jObj.getString("description"));

                            } else if (inputSrc.equalsIgnoreCase("mode")) { //pnp or advance mode
                                Integer modeFlag = jObj.getInt("mode_flag");
                                alertModel.setModelFlag(modeFlag.toString());
                                modeStatus = modeFlag.toString();

                            }  else if (inputSrc.equalsIgnoreCase("parental_control")) {
                                Integer modeFlag = jObj.getInt("mode_flag"); //0-off, 1-on
                                alertModel.setParentalControlFlag(modeFlag.toString());
                                parentalModeStatus = modeFlag.toString();
                            }


                    }
                }

                    Collections.sort(listOfAllViewAlertsForDashboard, new DateComparator());

                }  else  if (httpCode == 409) {
                listOfAllViewAlertsForDashboard.clear();
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            }else if (httpCode == 503){
                listOfAllViewAlertsForDashboard.clear();
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") +".";
            }else {
                listOfAllViewAlertsForDashboard.clear();
                    result.message = "No data detected";

                }

            }

         catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }

    //date order
    static class DateComparator implements Comparator<MutedBlockedAlertsModel> {

        @Override
        public int compare(MutedBlockedAlertsModel lhs, MutedBlockedAlertsModel rhs) {
            Double distance = (double) lhs.getEve_sec();
            Double distance1 = (double) rhs.getEve_sec();
            return Integer.compare(distance1.compareTo(distance), 0);
        }
    }

}
