package hardware.prytex.io.parser;

import android.annotation.SuppressLint;

import hardware.prytex.io.model.SensorDataModel;
import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by macbookpro13 on 11/08/2017.
 */

public class SensorDataParser implements BaseParser {

    public static final ArrayList<SensorDataModel> listOfSensorData = new ArrayList<>();

    //
    @SuppressLint("LongLogTag")
    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        try {

            JSONObject res = new JSONObject(response);
            result.code = httpCode;
            if (result.code == 200 || result.code == 2) {
                JSONArray jArray = res.getJSONArray("message");
                result.success(true);
                result.code = httpCode;
                result.message = "success";

                listOfSensorData.clear();
                if (jArray.length() > 0) {
                    //get all list of devices
                    for (int jobNo = 0; jobNo < jArray.length(); jobNo++) {
                        JSONObject jObj = jArray.getJSONObject(jobNo);
                        SensorDataModel sensorModel = new SensorDataModel();
//                        sensorModel.setBase_res(jObj.getString("base_res"));
//                        sensorModel.setAir_quality(jObj.getString("air_quality"));
                        sensorModel.setCoppm(jObj.getInt("coppm"));
                        sensorModel.setEve_sec(jObj.getInt("eve_sec"));
                        sensorModel.setHumidity(BigDecimal.valueOf(jObj.getDouble("humidity")).floatValue());
//                        sensorModel.setMean_adc(jObj.getInt("mean_adc"));
//                        sensorModel.setMean_corrected_res(jObj.getInt("mean_corrected_res"));
//                        sensorModel.setMean_res(jObj.getInt("mean_res"));
                        sensorModel.setTemperature(BigDecimal.valueOf(jObj.getDouble("temperature")).floatValue());
//                        sensorModel.setValidInterval(jObj.getInt("validInterval"));

                        listOfSensorData.add(sensorModel);

                    }
                    Collections.sort(listOfSensorData, new DateComparator());

                } else {
                    result.message = "No data received";
                }

            } else if (httpCode == 409) {
                listOfSensorData.clear();
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            } else if (httpCode == 503) {
                listOfSensorData.clear();
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") + ".";
            } else {
                listOfSensorData.clear();
                result.success(false);
                result.code = httpCode;
                result.message = "Unable to fetch sensor data,please try again after a few minutes!";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.code = httpCode;
        }
        return result;
    }

    //date order
    static class DateComparator implements Comparator<SensorDataModel> {

        @Override
        public int compare(SensorDataModel lhs, SensorDataModel rhs) {
            Double distance = (double) lhs.getEve_sec();
            Double distance1 = (double) rhs.getEve_sec();
            return Integer.compare(distance1.compareTo(distance), 0);
        }
    }
}
