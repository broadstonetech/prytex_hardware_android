package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.fragment.AboutFragment;
import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONException;
import org.json.JSONObject;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by macbookpro13 on 16/03/2018.
 */

public class OtaVersionParser implements BaseParser {
    @Override
    public TaskResult parse(int httpCode, String response) {
        Log.d("response", response);
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
//            boolean check = res.optBoolean("success");
            if (httpCode == 200 || httpCode == 2) {
                result.success(true);
                result.code = httpCode;
                AboutFragment.otaCurrentVersion = res.optString("latest");
                AboutFragment.otaInstalledVersion = res.optString("installed");
//                return result;
            } else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            }else if (httpCode == 503){
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") +".";
            }else {
                result.success(false);
                result.code = httpCode;
                result.message = res.optString("message");
                Log.d("name", result.message);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.code = httpCode;
        }
        return result;
    }
}
