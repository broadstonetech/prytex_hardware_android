package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONException;
import org.json.JSONObject;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by macbookpro13 on 10/08/2017.
 */

@SuppressWarnings("ALL")
public class UnmuteUnblockParser implements BaseParser {
    @Override
    public TaskResult parse(int httpCode, String response) {
        Log.d("AccountSetup response", response);
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
            result.code = httpCode;
            if (result.code == 200 || result.code == 2) {
                result.success(true);
//                result.message = res.getString("message");
                result.message = "success";

            } else if (result.code == 400) {
                result.success(false);
                result.code = httpCode;
//                result.message = res.getString("message");
                if (res.getString("message").equalsIgnoreCase("")) {
                    result.message = "failure";
                } else {
                    result.message = res.getString("message");
                }
            } else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            } else if (httpCode == 503) {
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") + ".";
            } else {
                result.success(false);
                result.code = httpCode;
                result.message = "Failed, try again later";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }
}

