package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONException;
import org.json.JSONObject;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by macbookpro13 on 10/07/2017.
 */

public class InitSensorRequest implements BaseParser {
    @Override
    public TaskResult parse(int httpCode, String response) {
        Log.d("Response",response);
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
            boolean check = res.getBoolean("success");
            if (httpCode == 200 || httpCode == 2){
                result.success(true);
                result.message = "Success";

            }else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            }else if (httpCode == 503){
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") +".";
            }else {
                result.success(false);
                result.code = httpCode;
                result.message = res.getString("message");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }
}
