package hardware.prytex.io.parser;

import hardware.prytex.io.model.VulnerabilitiesListModal;
import hardware.prytex.io.model.VulnerabilityReferences;
import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by abubaker on 10/18/16.
 */

@SuppressWarnings("ALL")
public class VulnerabilitiesListParser implements BaseParser {
    public static boolean isVulnerabilitiesListReceived = false;
    public static final ArrayList<VulnerabilitiesListModal> listOfVulnerabilities = new ArrayList<>();
    private static final ArrayList<VulnerabilityReferences> listOfVulnerabilitiesReferences = new ArrayList<>();

    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        try {

//            JSONArray jArray = new JSONArray(response);

            JSONObject res = new JSONObject(response);
            ArrayList<HashMap<String, String>> refList = new ArrayList<>();
//            boolean check = res.optBoolean("success");
            result.code = httpCode;
            if (result.code == 200) {
                JSONArray jArray = res.getJSONArray("message");
                result.success(true);
                result.code = httpCode;
                result.message = "success";
                listOfVulnerabilities.clear();
                if (jArray.length() > 0) {
                    isVulnerabilitiesListReceived = true;
                    listOfVulnerabilitiesReferences.clear();

                    //get all list of devices
                    for (int jobNo = 0; jobNo < jArray.length(); jobNo++) {
                        JSONObject jObj = jArray.getJSONObject(jobNo);
                        VulnerabilitiesListModal vulnerabilitiesListModal = new VulnerabilitiesListModal();
                        vulnerabilitiesListModal.setTitle_cv(jObj.getString("title"));
                        vulnerabilitiesListModal.setDescription_cv(jObj.getString("description"));
                        vulnerabilitiesListModal.setAttackVector_cv(jObj.getString("attack_vector"));
                        vulnerabilitiesListModal.setAttackComplexity_cv(jObj.getString("attack_complexity"));
                        vulnerabilitiesListModal.setPublishedDate_cv(jObj.getString("published_date"));
                        vulnerabilitiesListModal.setRisk_cv(jObj.getString("risk"));
                        JSONArray ref = jObj.getJSONArray("references");
                        vulnerabilitiesListModal.jArrRefList = ref;
                        vulnerabilitiesListModal.setReferences_cv(String.valueOf(ref));

                        listOfVulnerabilities.add(vulnerabilitiesListModal);
                    }

                } else {
                    isVulnerabilitiesListReceived = false;
                    result.message = "No Conected Devices";
                }

            } else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            } else if (httpCode == 503) {
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") + ".";
            } else {
                result.success(false);
                result.code = httpCode;
                result.message = "Unable to fetch Connected devices!";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.code = httpCode;
        }
        return result;
    }
}
