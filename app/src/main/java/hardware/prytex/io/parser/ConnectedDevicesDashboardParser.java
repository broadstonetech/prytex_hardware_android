package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONException;
import org.json.JSONObject;

import hardware.prytex.io.fragment.DashBoardFragment;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by macbookpro13 on 02/02/2018.
 */

@SuppressWarnings("ALL")
public class ConnectedDevicesDashboardParser implements BaseParser {
    @Override
    public TaskResult parse(int httpCode, String response) {
        Log.d("Response",response);
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
            result.code = httpCode;
            if (result.code == 200 || result.code == 2) {
                JSONObject resB = res.getJSONObject("message");
                result.success(true);
                result.code = httpCode;
                DashBoardFragment.isAlertsCall = false;
                result.message = res.getString("message");
                DashBoardFragment.isConnectedDevicesCall = true;
                DashBoardFragment.ConnectedDevicesCount = resB.getInt("total");
                DashBoardFragment.AndroidDevicesCount = resB.getInt("android");
                DashBoardFragment.MacDevicesCount = resB.getInt("apple");
                DashBoardFragment.WindowDevicesCount = resB.getInt("windows");
                DashBoardFragment.OtherDevicesCount = resB.getInt("others");
                String dmoatStatus = resB.getString("dmoat_status");
                isDmoatOnline = dmoatStatus.equalsIgnoreCase("online");
            } else  if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            }else if (httpCode == 503){
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") +".";
            }else{
                result.code = httpCode;
                result.message = "Error Occured, Please try Again!";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }
}
