package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONException;
import org.json.JSONObject;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by applepc on 15/03/2017.
 */

public class AuthenticationRequest implements BaseParser {

    @Override
    public TaskResult parse(int httpCode, String response) {
        Log.d("Auth response",response);
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
            result.code = httpCode;
            String Check = String.valueOf(httpCode);
            if (httpCode == 200 || httpCode == 2){
//                responsee = res.getString("success");
                result.code = httpCode;
                result.success(true);
                result.message = "Successfully Auth UI!";

            } else  if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            }else if (httpCode == 503){
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") +".";
            }else{
                //keep on pinging/read doc
                result.success(false);
                result.code = httpCode;
                result.message = res.getString("success");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }
}
