package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by abubaker on 8/3/16.
 */

public class AccountSetup implements BaseParser {
    public static Boolean isSuccessFullySignedUp = false;
    @Override
    public TaskResult parse(int httpCode, String response) {
        Log.d("AccountSetup response",response);
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
            isSuccessFullySignedUp = false;
            Log.d("httpCode", String.valueOf(httpCode));
            if (httpCode ==  200 || httpCode == 2){
                result.success(true);
                result.message = "You have successfully signed up for an account.";
                result.code = 200;
                isSuccessFullySignedUp = true;

            }else if (httpCode == 503){
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") +".";
            }
            else{
                result.success(false);
                result.code = httpCode;
                result.message = res.getString("message");
            }


        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "An error Occurred, Please try Again.";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }
}
