package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.model.PreMutedAlertsModel;
import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by macbookpro13 on 18/12/2017.
 */

public class Pre_MutedAlertsParser implements BaseParser {
    public static final ArrayList<PreMutedAlertsModel> listOfPreMutedAlerts = new ArrayList<>();
    public static final ArrayList<PreMutedAlertsModel> listOfPreMutedFlowAlerts = new ArrayList<>();

    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
//            JSONArray jArray = new JSONArray(response);

            listOfPreMutedAlerts.clear();
            listOfPreMutedFlowAlerts.clear();

            if (httpCode == 200 || httpCode == 2) {
                JSONArray jArray = res.getJSONArray("message");
                result.success(true);
                result.code = httpCode;
                result.message = "success";
                if (jArray.length() > 0) {

                    for (int jobNo = 0; jobNo < jArray.length(); jobNo++) {
                        JSONObject jObj = jArray.getJSONObject(jobNo);
                        String inputSrc = jObj.getString("input_src");
                        PreMutedAlertsModel alertModel;
                        if (inputSrc.equalsIgnoreCase("ids")) {

                            String description = (jObj.getString("description"));
                            Integer eveSec = jObj.getInt("eve_sec");
                            JSONArray jArraySets = jObj.getJSONArray("sets");
                                alertModel = new PreMutedAlertsModel();
                            alertModel.setInput_src(inputSrc);
                                alertModel.setDescription(description);
                                alertModel.setEve_sec(eveSec);
                            for (int i = 0; i < jArraySets.length(); i++) {
                                JSONObject jObjSets = jArraySets.getJSONObject(i);
                                alertModel = new PreMutedAlertsModel();
                                alertModel.setInput_src(inputSrc);
                                alertModel.setDescription(description);
                                alertModel.setSrc_ip(jObjSets.getString("src_ip"));
                                alertModel.setDest_ip(jObjSets.getString("dest_ip"));
                                alertModel.setEve_sec(eveSec);
                                Log.d("src_ip for", i + jObjSets.getString("src_ip"));
                                Log.d("dest_ip for", i + jObjSets.getString("dest_ip"));
                                listOfPreMutedAlerts.add(alertModel);
                            }
                        }
                        else if (inputSrc.equalsIgnoreCase("flow")){
//                            alertModel.setRecord_id(jObj.getString("record_ids"));
                            alertModel = new PreMutedAlertsModel();
                            alertModel.setInput_src(inputSrc);
//                                alertModel.setDescription(jObj.getString("description"));
                            alertModel.setEve_sec(jObj.getInt("eve_sec"));
                            alertModel.setSlotId(jObj.getInt("slot_id"));
//                                String alertType = jObj.getString("type");
                            alertModel.flowAlertJsonArr = jObj.getJSONArray("alerts");
                            listOfPreMutedFlowAlerts.add(alertModel);

                        }

                    }
                    Collections.sort(listOfPreMutedAlerts, new DateComparator());
                    Collections.sort(listOfPreMutedFlowAlerts, new DateComparator());
                } else {
                    listOfPreMutedAlerts.clear();
                    listOfPreMutedFlowAlerts.clear();
                    result.message = "No data detected";

                }

            } else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            } else if (httpCode == 503) {
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") + ".";
            } else {
                result.success(false);
                result.code = httpCode;
                result.message = "No data detected";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.code = httpCode;
        }
        return result;
    }

    //date order
    static class DateComparator implements Comparator<PreMutedAlertsModel> {

        @Override
        public int compare(PreMutedAlertsModel lhs, PreMutedAlertsModel rhs) {
            Double distance = (double) lhs.getEve_sec();
            Double distance1 = (double) rhs.getEve_sec();
            return Integer.compare(distance1.compareTo(distance), 0);
        }
    }
}

