package hardware.prytex.io.parser;

import hardware.prytex.io.model.MutedBlockedAlertsModel;
import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by abubaker on 10/18/16.
 */

public class BlockedAlertParser implements BaseParser {
    public static final ArrayList<MutedBlockedAlertsModel> listOfMutedBlockedAlerts = new ArrayList<>();

    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);

            if (httpCode == 200 || httpCode == 2) {
                JSONArray jArray = res.getJSONArray("message");

                result.success(true);
                result.code = httpCode;
                result.message = "success";
                if (jArray.length() > 0) {

                    listOfMutedBlockedAlerts.clear();
                    for (int jobNo = 0; jobNo < jArray.length(); jobNo++) {
                        JSONObject jObj = jArray.getJSONObject(jobNo);
                        MutedBlockedAlertsModel alertModel = new MutedBlockedAlertsModel();

                        String inputSrc = jObj.getString("input_src");

                        if (inputSrc.equalsIgnoreCase("ids")) {

                            alertModel.setSrc_port(jObj.getString("input_src"));
                            alertModel.setDescription(jObj.getString("description"));
                            alertModel.setInput_src(inputSrc);
                            alertModel.setEve_sec(jObj.getInt("eve_sec"));
//                                alertModel.setNamespace(jObj.getString("namespace"));
                            alertModel.setDestHostName(jObj.getString("dest_hostname"));
                            alertModel.setSrcHostName(jObj.getString("src_hostname"));
//                                alertModel.setApp_id(jObj.getString("app_id"));
                            alertModel.setSrc_ip(jObj.getString("src_ip"));
                            alertModel.setSrc_port(jObj.getString("src_port"));
                            alertModel.setRecord_id(jObj.getString("record_id"));
                            alertModel.setDest_port(jObj.getString("dest_port"));
                            alertModel.setDesc_ip(jObj.getString("dest_ip"));


                        } else if (inputSrc.equalsIgnoreCase("flow")) {


                            alertModel.setInput_src(jObj.getString("input_src"));
//                                alertModel.setDescription(jObj.getString("description"));
                                alertModel.setEve_sec(jObj.getInt("eve_sec"));
                            alertModel.setSlotId(jObj.optInt("slot_id"));
//                                String alertType = jObj.getString("type");
                            alertModel.flowAlertJsonArr = jObj.getJSONArray("alerts");

                        } else if (inputSrc.equalsIgnoreCase("blockips")) {

//                                alertModel.setNamespace(jObj.getString("namespace"));
                            alertModel.setRecord_id(jObj.getString("record_id"));
                            alertModel.setInput_src(jObj.getString("input_src"));
                            alertModel.setDescription(jObj.getString("description"));
                            alertModel.setEve_sec(jObj.getInt("eve_sec"));
                            alertModel.setIp(jObj.getString("ip"));

                        } else if (inputSrc.equalsIgnoreCase("cnctdhost")) {

//                                alertModel.setNamespace(jObj.getString("namespace"));
                            alertModel.setRecord_id(jObj.getString("record_id"));
                            alertModel.setInput_src(jObj.getString("input_src"));
                            alertModel.setDescription(jObj.getString("description"));
                            alertModel.setEve_sec(jObj.getInt("eve_sec"));
//                                alertModel.setApp_id(jObj.getString("app_id"));


                            alertModel.setParameters(jObj.getString("parameters"));
                            alertModel.setMacaddress(jObj.getString("macaddress"));
                            alertModel.setHostName(jObj.getString("hostname"));
                            alertModel.setHostId(jObj.getString("HOSTID"));
                            alertModel.setIp(jObj.getString("ip"));
                            alertModel.setOS(jObj.getString("OS"));


                        }

                        listOfMutedBlockedAlerts.add(alertModel);
                    }
                    Collections.sort(listOfMutedBlockedAlerts, new DateComparator());

                } else {
                    listOfMutedBlockedAlerts.clear();
                    result.message = "No data detected";

                }

            }  else  if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            }else if (httpCode == 503){
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") +".";
            } else {
                result.success(false);
                result.code = httpCode;
                result.message = "No data detected";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.code = httpCode;
        }
        return result;
    }

    //date order
    static class DateComparator implements Comparator<MutedBlockedAlertsModel> {

        @Override
        public int compare(MutedBlockedAlertsModel lhs, MutedBlockedAlertsModel rhs) {
            Double distance = (double) lhs.getEve_sec();
            Double distance1 = (double) rhs.getEve_sec();
            return Integer.compare(distance1.compareTo(distance), 0);
        }
    }
}
