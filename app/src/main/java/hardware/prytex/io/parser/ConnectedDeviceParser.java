package hardware.prytex.io.parser;

import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONException;
import org.json.JSONObject;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by mubashar on 20/04/2017.
 */

public class ConnectedDeviceParser implements BaseParser {
    @Override
    public TaskResult parse(int httpCode, String response) {
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
            boolean check = res.optBoolean("success");
            if (check) {
                result.success(true);
                result.code = httpCode;
                result.message = "success";
            } else  if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            }else if (httpCode == 503){
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") +".";
            } else {
                result.success(false);
                result.code = httpCode;
                result.message = res.optString("message");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.code = httpCode;
        }
        return result;
    }
}

