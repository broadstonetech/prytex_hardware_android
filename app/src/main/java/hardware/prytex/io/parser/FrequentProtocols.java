package hardware.prytex.io.parser;

import android.util.Log;

import hardware.prytex.io.model.BandWidth;
import hardware.prytex.io.network.BaseParser;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

public class FrequentProtocols implements BaseParser {

    public static final ArrayList<BandWidth> listOfFrequentProtocolsReceived = new ArrayList<>();

    @Override
    public TaskResult parse(int httpCode, String response) {
        Log.d("Response",response);
        TaskResult result = new TaskResult();
        try {
            JSONObject res = new JSONObject(response);
            if (httpCode == 200 || httpCode == 2){
                result.success(true);
                result.message = "Success";


//                    for (int jobNo = 0; jobNo < 7; jobNo++) {
                BandWidth bandwidthModal = new BandWidth();

                String ref = res.getString("message");
                bandwidthModal.setProtocolsList(ref);
                listOfFrequentProtocolsReceived.add(bandwidthModal);

            }else if (httpCode == 409) {
                isDmoatOnline = false;
                result.success(false);
                result.code = httpCode;
                result.message = "Prytex Hardware is inactive.";
            }else if (httpCode == 503){
                result.code = httpCode;
                result.message = TaskResult.ERROR_TEXT_503 + res.optString("message") +".";
            }else {
                result.success(false);
                result.code = httpCode;
                result.message = res.getString("message");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.message = "Error Occurred, Please Try Again!";
            result.success(false);
            result.code = httpCode;
        }
        return result;
    }
}
