package hardware.prytex.io.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.fragment.Policy.AccessControl.PolicyPlanFragment;
import hardware.prytex.io.model.ConnectedDeviceList;

import java.util.ArrayList;

public class PolicyHostsAdapter extends RecyclerView.Adapter<PolicyHostsAdapter.ViewHolder> {

    public static ArrayList<ConnectedDeviceList> HostsList;
    private final Context context;

    public PolicyHostsAdapter(Context context, ArrayList<ConnectedDeviceList> alName, boolean isCreateNewPolicy) {
        super();
        this.context = context;
        HostsList = alName;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.policy_host_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

        viewHolder.tvHostName.setText(HostsList.get(i).getHostName_cd());
        viewHolder.tvHostIp.setText(HostsList.get(i).getIp_cd());

        if (HostsList.get(i).isInPolicy()) {
            viewHolder.imgCheckMark.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imgCheckMark.setVisibility(View.GONE);
        }

        viewHolder.cardView.setOnClickListener(v -> {
            PolicyPlanFragment.btnAddAllDevices.setBackground(context.getResources().getDrawable(R.drawable.policy_days_border));
            if (HostsList.get(i).isInPolicy()) {
                HostsList.get(i).setInPolicy(false);
            } else {
                HostsList.get(i).setInPolicy(true);
            }
//                    notifyItemChanged(pos);
            notifyItemRangeChanged(i, HostsList.size());

        });

    }

    @Override
    public int getItemCount() {
        return HostsList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView tvHostName;
        final TextView tvHostIp;
        final ImageView imgCheckMark;
        final CardView cardView;

        ViewHolder(final View itemView) {
            super(itemView);
            tvHostName = itemView.findViewById(R.id.tvHostName);
            tvHostIp = itemView.findViewById(R.id.tvHostIp);
            imgCheckMark = itemView.findViewById(R.id.imgCheckMark);
            cardView = itemView.findViewById(R.id.card_view);


        }
    }
}
