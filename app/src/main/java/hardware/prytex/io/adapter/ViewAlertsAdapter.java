package hardware.prytex.io.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.config.Constants;
import hardware.prytex.io.fragment.ProfileFragment;
import hardware.prytex.io.model.AlertAction;
import hardware.prytex.io.model.DMoatAlert;
import hardware.prytex.io.model.MutedBlockedAlertsModel;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.GeneralUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import hardware.prytex.io.activity.DashBoardActivity;
import hardware.prytex.io.fragment.AlertsTabs.ViewAlertsAdvancedFragment;

import static hardware.prytex.io.adapter.ConnectedHostsAdapter.getrTimeStamp;
import static hardware.prytex.io.model.DMoatAlert.isDmoatOffline;
import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;
import static hardware.prytex.io.parser.ViewAlersParser.listOfAllViewAlerts;

/**
 * Created by khawarraza on 28/11/2016.
 */

@SuppressWarnings("ALL")
public class ViewAlertsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int CONNECTED_HOSTS = 1; //"cnctdhost";
    private static final int IDS = 2; //"ids";
    private static final int FLOW = 3; //"flow";
    private static final int SENSOR = 4; //"sensor";
    private static final int INFO = 5; // "info";
    private static final int BLOCK_IPs = 6; //"blockips";
    private static int selectedAlertPos = -1;
    private static final int UPDATE = 0;
    private final RecyclerView recyclerView;
    private static ArrayList<MutedBlockedAlertsModel> mDataSet;
    private final Context context;
    private final OnAlertSelectedListener listener;
    private final String appID;
    public static String upDateType;
    private ProgressDialog dialog;
    private SharedPreferences pref;

    public ViewAlertsAdapter(Context context, RecyclerView recyclerView, ArrayList<MutedBlockedAlertsModel> list, OnAlertSelectedListener listener, String appID) {
        this.context = context;
        mDataSet = list;
        this.recyclerView = recyclerView;
        this.listener = listener;
        this.appID = appID;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == CONNECTED_HOSTS) {
            View view = LayoutInflater.from(context).inflate(R.layout.connected_host_item, recyclerView, false);
            return new ConnectedHostsViewHolder(view);
        } else if (viewType == IDS) {
            View view = LayoutInflater.from(context).inflate(R.layout.ids_item, recyclerView, false);
            return new IDSViewHolder(view);
        } else if (viewType == FLOW) {
            View view = LayoutInflater.from(context).inflate(R.layout.flow_item, recyclerView, false);
            return new FlowViewHolder(view);
        } else if (viewType == SENSOR) {
            View view = LayoutInflater.from(context).inflate(R.layout.sensor_reading_item, recyclerView, false);
            return new SensorReadingViewHolder(view);
        } else if (viewType == INFO) {
            View view = LayoutInflater.from(context).inflate(R.layout.device_down_item, recyclerView, false);
            return new InfoViewHolder(view);
        } else if (viewType == BLOCK_IPs) {
            View view = LayoutInflater.from(context).inflate(R.layout.flow_item, recyclerView, false);
            return new BlockIpsViewHolder(view);
        } else if (viewType == UPDATE) {
            View view = LayoutInflater.from(context).inflate(R.layout.device_down_item, recyclerView, false);
            return new InfoViewHolder(view);
        }
        else {
            View view = LayoutInflater.from(context).inflate(R.layout.flow_item, recyclerView, false);
            return new FlowViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        pref = context.getSharedPreferences("MyPref", 0);

        String inputSrc = mDataSet.get(position).getInput_src();
        Log.d("inputSrc=", inputSrc);

        if (holder.getItemViewType() == CONNECTED_HOSTS) {
            ConnectedHostsViewHolder vh = (ConnectedHostsViewHolder) holder;
            MutedBlockedAlertsModel alert = mDataSet.get(position);
            vh.bindData(alert);
        } else if (holder.getItemViewType() == FLOW) {
            FlowViewHolder vh = (FlowViewHolder) holder;
            MutedBlockedAlertsModel alert = mDataSet.get(position);
            vh.bindData(alert);
        } else if (holder.getItemViewType() == IDS) {
            IDSViewHolder vh = (IDSViewHolder) holder;
            MutedBlockedAlertsModel alert = mDataSet.get(position);
            vh.bindData(alert);
        } else if (holder.getItemViewType() == SENSOR) {
            SensorReadingViewHolder vh = (SensorReadingViewHolder) holder;
            MutedBlockedAlertsModel alert = mDataSet.get(position);
            vh.bindData(alert);
        } else if (holder.getItemViewType() == INFO) {
            InfoViewHolder vh = (InfoViewHolder) holder;
            MutedBlockedAlertsModel alert = mDataSet.get(position);
            vh.bindData(alert);
        } else if (holder.getItemViewType() == BLOCK_IPs) {
            BlockIpsViewHolder vh = (BlockIpsViewHolder) holder;
            MutedBlockedAlertsModel alert = mDataSet.get(position);
            vh.bindData(alert);
        } else if (holder.getItemViewType() == UPDATE) {
            InfoViewHolder vh = (InfoViewHolder) holder;
            MutedBlockedAlertsModel alert = mDataSet.get(position);
            vh.bindData(alert);
        }
//        else {
//            BlockIpsViewHolder vh = (BlockIpsViewHolder) holder;
//            MutedBlockedAlertsModel alert = mDataSet.get(position);
//            vh.bindData(alert);
//        }
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        String alert = mDataSet.get(position).getInput_src();
        if (alert != null) {
            if (alert.equalsIgnoreCase("cnctdhost")) {
                return CONNECTED_HOSTS;
            } else if (alert.equalsIgnoreCase("flow")) {
                return FLOW;
            } else if (alert.equalsIgnoreCase("sensor")) {
                return SENSOR;
            } else if (alert.equalsIgnoreCase("info")) {
                return INFO;
            } else if (alert.equalsIgnoreCase("ids")) {
                return IDS;

            } else if (alert.equalsIgnoreCase("blockips")) {
                return BLOCK_IPs;
//            }
            } else {
                Log.d("Alert", "alert is null");
            }
        }
        return super.getItemViewType(position);

    }

    class ConnectedHostsViewHolder extends RecyclerView.ViewHolder {

        final View view;
        final View visitedLine;
        final View actionsContainer;
        final TextView desc;
        final TextView hostName;
        final TextView os;
        final TextView mac;
        final TextView ip;
        final TextView eveSec;
        final LinearLayout llIp;
        final LinearLayout llOS;
        final LinearLayout llCategory;
        final LinearLayout llhostName;

        ConnectedHostsViewHolder(final View itemView) {
            super(itemView);
            view = itemView;

            desc = view.findViewById(R.id.txt_description);
            hostName = view.findViewById(R.id.txt_hostname);
            os = view.findViewById(R.id.txt_os);
            mac = view.findViewById(R.id.txt_mac);
            ip = view.findViewById(R.id.txt_ip);
            eveSec = view.findViewById(R.id.eve_sec_timestamp);
            visitedLine = view.findViewById(R.id.visited_line);
            actionsContainer = view.findViewById(R.id.actions_container);

            llhostName = view.findViewById(R.id.llHostName);
            llIp = view.findViewById(R.id.llHostIP);
            llOS = view.findViewById(R.id.llHostOS);
            llCategory = view.findViewById(R.id.llHostMAC);
            view.setOnClickListener(v -> {
                if (isDmoatOnline) {
                    int alertPos = getAdapterPosition();
                    MutedBlockedAlertsModel alert = mDataSet.get(getAdapterPosition());
                    selectedAlertPos = alertPos;
                    listener.onAlertSelected(alert);

                    mDataSet.get(selectedAlertPos).setSeenStatus(true);
                    int alertId = mDataSet.get(selectedAlertPos).getAlertId();
                    listOfAllViewAlerts.get(alertId).setSeenStatus(true);
                    setAlertSeen(alert);
                } else {
                    DashBoardActivity.showAlertMsgDialog(context, context.getResources().getString(R.string.dmoat_offline));
                }


            });

        }

        void bindData(MutedBlockedAlertsModel alert) {

            int time = alert.getEve_sec();
            String timeStmp = "00:00:00";
            try {
                timeStmp = getrTimeStamp(time);
            } catch (Exception e) {
                e.printStackTrace();
            }
            eveSec.setText(timeStmp);
            desc.setText(alert.getDescription());
            if (alert.getDeviceCategory().equalsIgnoreCase("Router")) {
                hostName.setText(alert.getDeviceCategory());
            } else {
                if (alert.getHostName().equalsIgnoreCase("Unknown")) {
                    llhostName.setVisibility(View.GONE);
                } else {
                    llhostName.setVisibility(View.GONE);
                }
                hostName.setText(alert.getHostName());
                llhostName.setVisibility(View.GONE);
            }
            if (alert.getOS().equalsIgnoreCase("Unknown")) {
                llOS.setVisibility(View.GONE);
            } else {
                llOS.setVisibility(View.VISIBLE);
            }
            os.setText(alert.getOS());
//            mac.setVisibility(View.GONE);
            if (alert.getDeviceCategory().equalsIgnoreCase("Unknown")) {
                llCategory.setVisibility(View.GONE);
            } else {
                llCategory.setVisibility(View.VISIBLE);
            }
            mac.setText(alert.getDeviceCategory());
            if (alert.getIp().equalsIgnoreCase("Unknown")) {
                llIp.setVisibility(View.GONE);
            } else {
                llIp.setVisibility(View.VISIBLE);
            }
            ip.setText(alert.getIp());
            if (alert.isSeenStatus()) {
                visitedLine.setVisibility(View.INVISIBLE);
            } else {
                visitedLine.setVisibility(View.VISIBLE);
            }

            actionsContainer.setVisibility(View.GONE);

        }
    }

    class FlowViewHolder extends RecyclerView.ViewHolder {

        final View view;
        final View visitedLine;
        final View actionsContainer;
        final TextView desc;
        final TextView detail;
        final TextView detail_label;
        final TextView eveSec;
        final LinearLayout llClientDetails;

        FlowViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            eveSec = view.findViewById(R.id.eve_sec_timestamp);
            desc = view.findViewById(R.id.txt_description);
            detail = view.findViewById(R.id.txt_detail);
            detail_label = view.findViewById(R.id.txt_detail_label);
            visitedLine = view.findViewById(R.id.visited_line);
            llClientDetails = view.findViewById(R.id.llClientInfoDetail);
            actionsContainer = view.findViewById(R.id.actions_container);
            view.setOnClickListener(v -> {
                if (isDmoatOnline) {
                    selectedAlertPos = getAdapterPosition();
                    MutedBlockedAlertsModel alert = mDataSet.get(getAdapterPosition());
                    listener.onAlertSelected(alert);

                    mDataSet.get(selectedAlertPos).setSeenStatus(true);
                    int alertId = mDataSet.get(selectedAlertPos).getAlertId();
                    listOfAllViewAlerts.get(alertId).setSeenStatus(true);

                    setAlertSeen(alert);
                } else {
                    DashBoardActivity.showAlertMsgDialog(context, context.getResources().getString(R.string.dmoat_offline));
                }

            });
        }

        void bindData(MutedBlockedAlertsModel alert) {

            String descriptionList = "";

            int slotId = alert.getSlotId();
            int hour = slotId % 24;
            int day = slotId / 24;

            String timeStmp = "00:00:00";
            String dy = "00:00:00";
            String format = "00:00:00";
            try {
                timeStmp = getrTimeStamp(alert.getEve_sec());
                timeStmp = getHourDayStamp(alert.getEve_sec(), true, false, false);
                dy = getHourDayStamp(alert.getEve_sec(), false, true, false);
                format = getHourDayStamp(alert.getEve_sec(), false, false, true);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (format.contains("PM")) {
                format = "PM";
            } else {
                format = "AM";
            }
            int endingHour = Integer.parseInt(timeStmp);
            endingHour = endingHour - 1;
            if (endingHour == 0) {
                endingHour = 12;
            } else if (endingHour == 13) {
                endingHour = 1;
            }
            desc.setText(descriptionList);//+ " on "+dayTitle +" between " + hourTitle +".");
//            eveSec.setText(dayTitle +" between " + hourTitle + " GMT." + dy + "between" + String.valueOf(endingHour)+ "-" + timeStmp  + format);
            eveSec.setText(dy + context.getResources().getString(R.string.between) + endingHour + "-" + timeStmp + format);
            detail.setVisibility(View.GONE);
            detail_label.setVisibility(View.GONE);
            llClientDetails.setVisibility(View.GONE);

            if (alert.isSeenStatus()) {
                visitedLine.setVisibility(View.INVISIBLE);
            } else {
                visitedLine.setVisibility(View.VISIBLE);
            }

        }
    }

    class IDSViewHolder extends RecyclerView.ViewHolder {

        final View view;
        final View visitedLine;
        final View actionsContainer;
        final TextView desc;
        final TextView msg;
        final TextView srcIP;
        final TextView destIP;
        final TextView srcPort;
        final TextView destPort;
        final TextView eveSec;

        IDSViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            desc = view.findViewById(R.id.txt_description);
            msg = view.findViewById(R.id.txt_message);
            srcIP = view.findViewById(R.id.txt_src_ip);
            destIP = view.findViewById(R.id.txt_dest_ip);
            srcPort = view.findViewById(R.id.src_port);
            destPort = view.findViewById(R.id.dest_port);
            eveSec = view.findViewById(R.id.eve_sec_timestamp);
            visitedLine = view.findViewById(R.id.visited_line);
            actionsContainer = view.findViewById(R.id.actions_container);
            view.setOnClickListener(v -> {
                if (isDmoatOnline) {
                    selectedAlertPos = getAdapterPosition();
                    MutedBlockedAlertsModel alert = mDataSet.get(getAdapterPosition());
                    listener.onAlertSelected(alert);

                    mDataSet.get(selectedAlertPos).setSeenStatus(true);
                    int alertId = mDataSet.get(selectedAlertPos).getAlertId();
                    listOfAllViewAlerts.get(alertId).setSeenStatus(true);

                    setAlertSeen(alert);
                } else {
                    DashBoardActivity.showAlertMsgDialog(context, context.getResources().getString(R.string.dmoat_offline));
                }

            });
        }

        void bindData(MutedBlockedAlertsModel alert) {

            int time = alert.getEve_sec();
            String timeStmp = "00:00:00";
            try {
                timeStmp = getrTimeStamp(time);
            } catch (Exception e) {
                e.printStackTrace();
            }
            eveSec.setText(timeStmp);

            desc.setText(alert.getDescription());
//            msg.setText(alert.msg);
//            srcIP.setText(alert.hostname + "" + alert.srcIp);
            srcIP.setText(alert.getSrc_ip());
            destIP.setText(alert.getDesc_ip());
            srcPort.setText(alert.getSrc_port());
            destPort.setText(alert.getDest_port());
            if (alert.isSeenStatus()) {
                visitedLine.setVisibility(View.INVISIBLE);
            } else {
                visitedLine.setVisibility(View.VISIBLE);
            }
            actionsContainer.setVisibility(View.GONE);
        }
    }


    class SensorReadingViewHolder extends RecyclerView.ViewHolder {

        final View view;
        final View visitedLine;
        final TextView desc;
        final TextView reading;
        public TextView category;
        final TextView eveSec;
        final TextView sensorValueGoodrange;
        final TextView sensorValueBadrange;

        SensorReadingViewHolder(final View itemView) {
            super(itemView);
            view = itemView;
            desc = view.findViewById(R.id.txt_description);
            reading = view.findViewById(R.id.txt_reading);
            sensorValueGoodrange = view.findViewById(R.id.tvSensorValueGoodRange);
            sensorValueBadrange = view.findViewById(R.id.tvSensorValueBadRange);
            eveSec = view.findViewById(R.id.eve_sec_timestamp);
            visitedLine = view.findViewById(R.id.visited_line);
            view.setOnClickListener(v -> {
                if (isDmoatOnline) {
                    selectedAlertPos = getAdapterPosition();
                    MutedBlockedAlertsModel alert = mDataSet.get(getAdapterPosition());
//                    listener.onAlertSelected(alert);


                    mDataSet.get(selectedAlertPos).setSeenStatus(true);
                    int alertId = mDataSet.get(selectedAlertPos).getAlertId();
                    listOfAllViewAlerts.get(alertId).setSeenStatus(true);

                    setAlertSeen(alert);
                } else {
                    DashBoardActivity.showAlertMsgDialog(context, context.getResources().getString(R.string.dmoat_offline));
                }

            });
        }

        void bindData(MutedBlockedAlertsModel alert) {

            int time = alert.getEve_sec();
            String timeStmp = "00:00:00";
            try {
                timeStmp = getrTimeStamp(time);
            } catch (Exception e) {
                e.printStackTrace();
            }
            eveSec.setText(timeStmp);
            desc.setText(alert.getDescription());
            String reading;
            String sensorRange;
            String sensorBadRange = "";
            if ((alert.getCategory().equals(DMoatAlert.TEMPERATURE))) {
                sensorRange = "Good temperature value is:5-31°C.";
                SharedPreferences prefs = context.getSharedPreferences(Constants.APP_ID_PREFS_NAME, Context.MODE_PRIVATE);
                int selectedScale = prefs.getInt(ProfileFragment.TEMPERATURE_SCALE, 0);
                if (selectedScale == 1) {
                    float r = -1;
                    try {
                        r = Float.parseFloat(String.valueOf(alert.getSensorReading()));
                        r = GeneralUtils.celciusToFahrenheit(r);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (r == -1) {
                        reading = alert.getSensorReading() + "°F";
                    } else {
                        reading = r + "°F";
                    }
                } else {
                    reading = alert.getSensorReading() + "°C";
                }

            } else if (alert.getCategory().equals(DMoatAlert.AIR_QUALITY)) {
                reading = alert.getSensorReading() + " CO2PPM";
                sensorRange = "Good air quality values is:0-450 CO2PPM.";
            } else {
                reading = alert.getSensorReading() + "%";
                sensorRange = "Good humidity value is:0-80%.";
            }
            this.reading.setText(reading);
            sensorValueGoodrange.setText(sensorRange);
//            sensorValueBadrange.setText(sensorBadRange);
            if (alert.isSeenStatus()) {
                visitedLine.setVisibility(View.INVISIBLE);
            } else {
                visitedLine.setVisibility(View.VISIBLE);
            }
        }
    }

    class InfoViewHolder extends RecyclerView.ViewHolder {

        final TextView timestamp;
        final TextView txtDescription;
        final View view;
        final View visitedLine;

        InfoViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            txtDescription = view.findViewById(R.id.txt_description);
            timestamp = view.findViewById(R.id.eve_sec_timestamp);
            visitedLine = view.findViewById(R.id.visited_line);
            view.setOnClickListener(v -> {
                if (isDmoatOnline) {
                    selectedAlertPos = getAdapterPosition();
                    MutedBlockedAlertsModel alert = mDataSet.get(getAdapterPosition());
                    listener.onAlertSelected(alert);

                    mDataSet.get(selectedAlertPos).setSeenStatus(true);
                    int alertId = mDataSet.get(selectedAlertPos).getAlertId();
                    listOfAllViewAlerts.get(alertId).setSeenStatus(true);

                    setAlertSeen(alert);
                } else {
                    DashBoardActivity.showAlertMsgDialog(context, context.getResources().getString(R.string.dmoat_offline));
                }

                if(mDataSet.get(getAdapterPosition()).getInput_src().equalsIgnoreCase("update"))
                {
                    if(mDataSet.get(getAdapterPosition()).getInfo_type().equalsIgnoreCase("firmware")){
                        upDateType = "firmware";
                    }else
                        upDateType ="io";

//                        BottomSheetFragment fragment = new BottomSheetFragment();
//                        fragment.show( ((AlertsMainActivity)context).getSupportFragmentManager(), Constants.TAG);
                    appFirmwareUpdateDialog(mDataSet.get(getAdapterPosition()).getVersion());
                }

            });
        }

        void bindData(MutedBlockedAlertsModel alert) {

            int time = alert.getEve_sec();
            String timeStmp = "00:00:00";
            try {
                timeStmp = getrTimeStamp(time);
            } catch (Exception e) {
                e.printStackTrace();
            }
            timestamp.setText(timeStmp);

            if (alert.getInfo_type().equalsIgnoreCase("internet_restarted")) {
                String timeOn;
                String timeOff;
                Log.d("timeOn", String.valueOf(alert.getTimeOn()));
                Log.d("timeOff", String.valueOf(alert.getTimeOff()));
                timeOn = alert.getFormattedTimeStamp(alert.getTimeOn());
                timeOff = alert.getFormattedTimeStamp(alert.getTimeOff());
                Log.d("timeOnn", String.valueOf(timeOn));
                Log.d("timeOfff", String.valueOf(timeOff));

                txtDescription.setText(context.getResources().getString(R.string.internet_unavailable) + timeOff + context.getResources().getString(R.string.to) + timeOn);
            } else {
                txtDescription.setText(alert.getDescription());
            }
            if (alert.isSeenStatus()) {
                visitedLine.setVisibility(View.INVISIBLE);
            } else {
                visitedLine.setVisibility(View.VISIBLE);
            }
        }
    }


    //region App update firwamre  update
    private void appFirmwareUpdateDialog(String version) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.add_update_firmware_update);

        TextView tvAppUpdate = dialog.findViewById(R.id.tvAppUpdate);
        TextView tvLater = dialog.findViewById(R.id.tvLater);
        TextView tvCancel = dialog.findViewById(R.id.tvCancelUpdate);



        View v2 = dialog.findViewById(R.id.view2);
        v2.setVisibility(View.GONE);


        final AlertAction action = new AlertAction();
        ArrayList<AlertAction> actions = null;

        tvAppUpdate.setOnClickListener(view -> {
            dialog.dismiss();
            if(upDateType.equalsIgnoreCase("io"))
            {
                appUpdate(version);
            }else
                firmWareUpdate(version);
        });
        tvLater.setOnClickListener(view -> dialog.dismiss());


        tvCancel.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    private void appUpdate(String appVersion)
    {
        showProgressDialog(true);
        Api.setAppUpDate(context, getRequestParams(appVersion), updateListner);
    }
    private void firmWareUpdate(String appVersion)
    {
        showProgressDialog(true);
        Api.setFirmwareUpdate(context, getRequestParams(appVersion), updateListner);
    }

    private JSONObject getRequestParams(String version) {
        JSONObject requestData = new JSONObject();
        try {
            requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            requestData.put("request_id", Dmoat.getNewRequestId());
            requestData.put("app_id", pref.getString("app_id", ""));
            requestData.put("token", UserManager.getInstance().getLoggedInUser().token);
            requestData.put("version", version);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestData;
    }

    private final AsyncTaskListener updateListner = new AsyncTaskListener() {

        @Override
        public void onComplete(TaskResult result) {
            {
                showProgressDialog(false);
                if(result.code == 200)
                {

                    Toast.makeText(context,"Request has been forwarded to Prytex Hardware.",Toast.LENGTH_LONG).show();
                    if(upDateType.equalsIgnoreCase("io"))
                        openPlayStoreApp();


                }

            }
        }
    };

    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(context, "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private void openPlayStoreApp()
    {
        final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
    //endregion


    class BlockIpsViewHolder extends RecyclerView.ViewHolder {

        final View view;
        final View visitedLine;
        final View actionsContainer;
        final TextView desc;
        final TextView detail;
        final TextView detail_label;
        final TextView eveSec;

        BlockIpsViewHolder(final View itemView) {
            super(itemView);
            view = itemView;
            eveSec = view.findViewById(R.id.eve_sec_timestamp);
            desc = view.findViewById(R.id.txt_description);
            detail = view.findViewById(R.id.txt_detail);
            detail_label = view.findViewById(R.id.txt_detail_label);
            visitedLine = view.findViewById(R.id.visited_line);
            actionsContainer = view.findViewById(R.id.actions_container);
            view.setOnClickListener(v -> {
                if (isDmoatOnline) {
                    selectedAlertPos = getAdapterPosition();
                    MutedBlockedAlertsModel alert = mDataSet.get(getAdapterPosition());
                    listener.onAlertSelected(alert);

                    mDataSet.get(selectedAlertPos).setSeenStatus(true);
                    int alertId = mDataSet.get(selectedAlertPos).getAlertId();
                    listOfAllViewAlerts.get(alertId).setSeenStatus(true);

                    setAlertSeen(alert);

                } else {
                    DashBoardActivity.showAlertMsgDialog(context, context.getResources().getString(R.string.dmoat_offline));
                }
            });
        }

        void bindData(MutedBlockedAlertsModel alert) {
            int time = alert.getEve_sec();
            String timeStmp = "00:00:00";
            try {
                timeStmp = getrTimeStamp(time);
            } catch (Exception e) {
                e.printStackTrace();
            }
            eveSec.setText(timeStmp);
            desc.setText(alert.getDescription());
            detail_label.setVisibility(View.GONE);
            detail.setText(alert.getIp());
            detail.setVisibility(View.GONE);
//            eveSec.setText(alert.getFormattedEveSecTimeStamp());
            if (alert.isSeenStatus()) {
                visitedLine.setVisibility(View.INVISIBLE);
            } else {
                visitedLine.setVisibility(View.VISIBLE);
            }
        }
    }

    public void clear() {
        mDataSet.clear();
        notifyDataSetChanged();
    }

    public void remove(MutedBlockedAlertsModel alert) {
        Log.d("AlertToRemove", alert.toString());
        if (alert != null) {
            mDataSet.remove(alert);
            this.notifyDataSetChanged();
        } else {
//            this.notifyDataSetChanged();
        }
    }

    public interface OnAlertSelectedListener {
        void onAlertSelected(MutedBlockedAlertsModel alert);
    }

    public void alertResponseCall(MutedBlockedAlertsModel alert, String url, JSONObject data) {

        Api.sendResponse(context, url, data, result -> {
            ViewAlertsAdvancedFragment.isSuccesAlertResponse = false;
            if (result.code == 200 || result.code == 2) {

                try {
                    mDataSet.remove(selectedAlertPos);
                    int alertId = mDataSet.get(selectedAlertPos).getAlertId();
                    listOfAllViewAlerts.remove(alertId);
                    notifyItemRemoved(selectedAlertPos);
//                        notifyItemRangeChanged(selectedAlertPos, mDataSet.size());
                    notifyDataSetChanged();
                } catch (Exception e) {
                    Log.d("exception==", String.valueOf(e));

                }
            } else {
                if (result.code == 400 && result.message.equalsIgnoreCase("Alert does not exist")) {
//                                                        showProgressDialog(false);

                } else if (result.code == 409) {
                    isDmoatOffline = true;
                    isDmoatOnline = false;
                } else {
                }
            }
        });

    }

    private String getDayHourValue(String dayHour) {
        String packageName = context.getPackageName();
        int identifier = context.getResources().getIdentifier(dayHour, "string", packageName);
        String str;
        if (identifier != 0) {
            str = context.getResources().getString(identifier);
        } else {
            str = "Unknown";//or null or whatever
        }
        return str;
    }

    //get hour and day from eve_sec
    public static String getHourDayStamp(int s, boolean hour, boolean day, boolean format) throws Exception {
        float fTime = Float.parseFloat(String.valueOf(s));
        long time = (long) fTime;
        time = time * 1000;
        Date date = new Date(time);

        SimpleDateFormat sourceFormat = new SimpleDateFormat("MMM-dd-yyyy h:mm a", Locale.getDefault());
//        if (hour) {
//            sourceFormat = new SimpleDateFormat("h");
//        } else if (day) {
//            sourceFormat = new SimpleDateFormat("EEEE");
//        } else {
//            sourceFormat = new SimpleDateFormat("h:mm a");
//        }
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateString = sourceFormat.format(date); // => Date is in UTC now
        Date parsed = sourceFormat.parse(dateString); // => Date is in UTC now

        SimpleDateFormat destFormat;
        if (hour) {
            destFormat = new SimpleDateFormat("h", Locale.getDefault());
        } else if (day) {
            destFormat = new SimpleDateFormat("EEEE", Locale.getDefault());
        } else {
            destFormat = new SimpleDateFormat("a", Locale.getDefault());
        }
        destFormat.setTimeZone(Calendar.getInstance().getTimeZone());
        return destFormat.format(parsed);
    }

    private void setAlertSeen(MutedBlockedAlertsModel alert) {
        AlertSeenTask asyncTask = new AlertSeenTask(); // Second
        asyncTask.alertsModel = alert;
        asyncTask.execute();
    }

    private class AlertSeenTask extends AsyncTask<Void, Void, Void> {
        MutedBlockedAlertsModel alertsModel;

        @Override
        protected void onPreExecute() {
            Log.i("AsyncTask", "FirstOnPreExecute()");
        }

        @Override
        protected Void doInBackground(Void... params) {

            Log.i("AsyncTask", "AlertSeenTask");

            Handler mainHandler = new Handler(Looper.getMainLooper());
            Runnable myRunnable = () -> {
                //Code that uses AsyncHttpClient in your case ConsultaCaract()
//                    Log.d("AsyncRecordID", alertsModel.getRecord_id());

                Api.setAlertSeen(context, getParams(alertsModel), mListener);

            };
            mainHandler.post(myRunnable);

            //call alerts get api
//            Thread.sleep(100); //call connected devices count api


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.d("AsyncTask", "FirstonPostExecute()");
        }
    }

    private JSONObject getParams(MutedBlockedAlertsModel model) {
        JSONObject js = new JSONObject();
        try {
            JSONObject alertJSON = new JSONObject();
            if (model.getInput_src().equalsIgnoreCase("flow")) {
                alertJSON.put("record_ids_list", model.flowRecordIdsList);
            } else {
                Log.d("getJsonRecordID", model.getRecord_id());
                alertJSON.put("record_id", model.getRecord_id());
            }
            js.put("app_id", appID);
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            js.put("data", alertJSON);
            Log.d("JSON==>", js.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    // listenerB
    private final AsyncTaskListener mListener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            Log.d("AsyncTaskResponse", "response");

            if (result.code == 200 || result.code == 2) {
                try {
                    Log.d("AsyncTask", "success");
                    mDataSet.get(selectedAlertPos).setSeenStatus(true);
                    notifyDataSetChanged();
                    notifyItemMoved(selectedAlertPos, mDataSet.size());

                } catch (Exception e) {
                    Log.d("AsyncExcepti9on2", String.valueOf(e));
                }
//                MyToast.showMessage(getContext(), "successfuly received data");
            } else {
//                MyToast.showMessage(getActivity(), getResources().getString(R.string.error_message_generic));
            }
        }
    };
}
