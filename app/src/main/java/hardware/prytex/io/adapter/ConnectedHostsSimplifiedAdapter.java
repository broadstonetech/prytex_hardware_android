package hardware.prytex.io.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.activity.ConnectedDeviceActivity;
import hardware.prytex.io.model.ConnectedDeviceList;
import hardware.prytex.io.model.DMoatAlert;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.model.VulnerabilitiesListModal;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.MyToast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import hardware.prytex.io.activity.DashBoardActivity;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by abubaker on 11/1/16.
 */

@SuppressWarnings("ALL")
public class ConnectedHostsSimplifiedAdapter extends RecyclerView.Adapter<ConnectedHostsSimplifiedAdapter.ConnectedHostViewHolder> {

    private final Context mContext;
    private final ArrayList<DMoatAlert> mDataSet;
    private ProgressDialog dialog;
    private static ArrayList<ConnectedDeviceList> listCnctdDevices;
    ArrayList<VulnerabilitiesListModal> listCnctdVulnerabilities;
    public static int vulnPosition;
    private int pos;
    private String inputName;
    public static boolean isChangeOS = false;
    public static boolean isChangeUserName = false;
    private String eveSec;
    RecyclerView.LayoutParams lp;
    private final OnAlertSelectedListener listenerCnctdDevice;
    private final String appID;
    private final String DEVICE_IP;

    public ConnectedHostsSimplifiedAdapter(Context context, ArrayList<ConnectedDeviceList> listDevices, ArrayList<DMoatAlert> list, OnAlertSelectedListener listener, String appID, String deviceIp) {
        this.mContext = context;
        listCnctdDevices = listDevices;
        this.mDataSet = list;
        this.listenerCnctdDevice = listener;
        this.appID = appID;
        this.DEVICE_IP = deviceIp;
    }

    @NonNull
    @Override
    public ConnectedHostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.host_item_simplified_ui, parent, false);
//        v.setMinimumHeight(200);
        return new ConnectedHostViewHolder(v);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final ConnectedHostViewHolder holder, final int position) {

        String userName = listCnctdDevices.get(position).getUserName();
        String hostName = listCnctdDevices.get(position).getHostName_cd();
        String ip = listCnctdDevices.get(position).getIp_cd();
        String OS = listCnctdDevices.get(position).getOs_cd();
        String deviceCategory = listCnctdDevices.get(position).getdeviceCategory();
        String macAddress = listCnctdDevices.get(position).getMacAddress_cd();


        holder.name.setText(hostName);
        holder.ip.setText(ip);

        int a = listCnctdDevices.get(position).getEveSec_cd();
        String b = getFormattedEveSecTimeStamp(a);
        if(!TextUtils.isEmpty(b)) {
            String[] splitStr = b.split("\\s+");
            holder.timeStamp.setText(splitStr[0].substring(0, splitStr[0].length() - 5)+"   "+splitStr[1]+" "+splitStr[2]);
        }

        if(OS.equalsIgnoreCase("Android"))
        {
            holder.imgOSICon.setImageResource(R.drawable.android_cnctddevice);
        }else if(OS.equalsIgnoreCase("Windows"))
        {
            holder.imgOSICon.setImageResource(R.drawable.windows_cnctddevice);
        }else if(OS.equalsIgnoreCase("iOS"))
        {
            holder.imgOSICon.setImageResource(R.drawable.apple_cnctddevice);
        }else
        {
            holder.imgOSICon.setImageResource(R.drawable.others_cnctddevice);
        }



        holder.mainView.setOnClickListener(v -> {
            mContext.startActivity(new Intent(mContext, ConnectedDeviceActivity.class));

        });

    }


    @Override
    public int getItemCount() {
        return listCnctdDevices.size();
    }

    public static class ConnectedHostViewHolder extends RecyclerView.ViewHolder {

        final TextView name;
        final TextView ip;
        final View mainView;
        final TextView timeStamp;
        final ImageView imgOSICon;
        ConnectedHostViewHolder(View itemView) {
            super(itemView);
            mainView = itemView;
            name = mainView.findViewById(R.id.device_name);
            ip = mainView.findViewById(R.id.device_ip);
            timeStamp = mainView.findViewById(R.id.eve_sec_timestamp);
            imgOSICon = mainView.findViewById(R.id.imgOSIcon);

        }

    }

    public ArrayList<DMoatAlert> getDataSet() {
        return mDataSet;
    }

    public void addItem(DMoatAlert dataObj) {
        mDataSet.add(dataObj);
        notifyDataSetChanged();
    }

    public void addList(ArrayList<DMoatAlert> dataList) {
        mDataSet.addAll(dataList);
        notifyDataSetChanged();
    }

    public void clearAdapter() {
        mDataSet.clear();
        notifyDataSetChanged();

    }

    private void blockDeviceCall(final int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Prytex Hardware");
        builder.setMessage("Do you really want to block device?")
                .setCancelable(false)
                .setPositiveButton("Block", (dialog, id) -> {
                    if (isDmoatOnline) {
                        showProgressDialog(true);
                        Api.blockConnectedDevice(mContext, getRequestParams(position), listener);
                    } else {
                        MyToast.showMessage(mContext, mContext.getResources().getString(R.string.dmoat_offline));
                    }
                })
                .setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
        alert.setCancelable(true);
    }
    private void blockOwnDeviceCall(final int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Prytex Hardware");
        builder.setMessage("You are going to block current device. Do you really want to block device?")
                .setCancelable(false)
                .setPositiveButton("Block", (dialog, id) -> {
                    if (isDmoatOnline) {
                        showProgressDialog(true);
                        Api.blockConnectedDevice(mContext, getRequestParams(position), listener);
                    } else {
                        MyToast.showMessage(mContext, mContext.getResources().getString(R.string.dmoat_offline));
                    }
                })
                .setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
        alert.setCancelable(true);
    }


    private void showEditAlert(final int positn) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Prytex Hardware");
        builder.setMessage("Rename the device name or operating system label.")
                .setCancelable(false)
                .setPositiveButton("Change device name", (dialog, id) -> {
                    isChangeUserName = true;
                    isChangeOS = false;
                    showDeviceNameInput(positn, "Enter device name");
                })
                .setNegativeButton("Rename operating system", (dialog, id) -> {
                    isChangeOS = true;
                    isChangeUserName = false;
                    showDeviceNameInput(positn, "Rename operating system label.");
                    dialog.cancel();
                });
        AlertDialog alert = builder.create();
        alert.show();
        alert.setCancelable(true);
    }

    private void showDeviceNameInput(final int position, String title) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(mContext);

        final LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(20, 0, 20, 0);
        final EditText edittext = new EditText(mContext);
        edittext.setLayoutParams(lp);
        edittext.setSingleLine(true);
        edittext.setImeOptions(EditorInfo.IME_ACTION_DONE);
        edittext.setMaxLines(1);
        alert.setMessage("");
        alert.setTitle(title);


        alert.setView(edittext);

        alert.setPositiveButton("Ok", (dialog, whichButton) -> {
            inputName = edittext.getText().toString();
            if (!inputName.equalsIgnoreCase("") || !inputName.contentEquals(" ")){
                if (isChangeOS) {
                    Api.renameConnectedDevice(mContext, getRenameRequestParams(position, listCnctdDevices.get(position).getUserName(), inputName), listenerRename);
                } else if (isChangeUserName) {
                    Api.renameConnectedDevice(mContext, getRenameRequestParams(position, inputName, listCnctdDevices.get(position).getOs_cd()), listenerRename);

                }
            }else{
                DashBoardActivity.showAlertMsgDialog(mContext, "Please enter a valid name.");
            }

        });

        alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
            // dismiss dialog

        });

        alert.show();

    }

    private final AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            showProgressDialog(false);

            if (result.code == 200) {
//                Toast.makeText(mContext, "Device Blocked", Toast.LENGTH_SHORT).show();
                listCnctdDevices.remove(pos);
                Log.d("device blocked at", String.valueOf(pos));
                notifyDataSetChanged();
            } else {
                Toast.makeText(mContext, result.message, Toast.LENGTH_SHORT).show();
            }

        }
    };

    private final AsyncTaskListener listenerRename = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            showProgressDialog(false);

            if (result.code == 200) {
                if (isChangeOS) {
                    listCnctdDevices.get(pos).setOs_cd(inputName);
                    notifyDataSetChanged();
                } else if (isChangeUserName) {
                    listCnctdDevices.get(pos).setUserName(inputName);
                    notifyDataSetChanged();
                }
            } else {
                Toast.makeText(mContext, result.message, Toast.LENGTH_SHORT).show();
            }

        }
    };

    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(mContext, "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private JSONObject getRequestParams(int position) {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();
        JSONObject response = new JSONObject();
        try {

            data.put("macaddress", listCnctdDevices.get(position).getMacAddress_cd());
            data.put("HOSTID", listCnctdDevices.get(position).getHostId_cd());
            data.put("parameters", listCnctdDevices.get(position).getParametrs_cd());
            data.put("eve_sec", listCnctdDevices.get(position).getEveSec_cd());
            data.put("ip", listCnctdDevices.get(position).getIp_cd());
            data.put("hostname", listCnctdDevices.get(position).getHostName_cd());
            data.put("record_id", listCnctdDevices.get(position).getRecord_id());
            data.put("host_id", listCnctdDevices.get(position).getHostId_cd());
            data.put("OS", listCnctdDevices.get(position).getOs_cd());

            response.put("interval", "");
            response.put("type", "Block");
            response.put("mode", "Forever");

            data.put("response", response);
            data.put("device_category", listCnctdDevices.get(position).getdeviceCategory());
            js.put("data", data);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            js.put("app_id", appID);
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    private JSONObject getRenameRequestParams(int position, String userName, String deviceName) {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();
        try {

            data.put("record_id", listCnctdDevices.get(position).getRecord_id());
            data.put("devicename", deviceName);
            data.put("usersname", userName);
            js.put("data", data);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            js.put("app_id", appID);
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    //eve sec to time stamp
    private String getFormattedEveSecTimeStamp(int es) {
        try {
            return getrTimeStamp(es);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return eveSec;
    }

    public static String getrTimeStamp(int s) throws Exception {
        float fTime = Float.parseFloat(String.valueOf(s));
        long time = (long) fTime;
        time = time * 1000;
        Date date = new Date(time);
        SimpleDateFormat sourceFormat = new SimpleDateFormat("MMM-dd-yyyy h:mm a");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateString = sourceFormat.format(date); // => Date is in UTC now
        Date parsed = sourceFormat.parse(dateString); // => Date is in UTC now

        SimpleDateFormat destFormat = new SimpleDateFormat("MMM-dd-yyyy h:mm a", Locale.getDefault());
//        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        destFormat.setTimeZone(Calendar.getInstance().getTimeZone());
        return destFormat.format(parsed);
    }

    public interface OnAlertSelectedListener {
        void onAlertSelected(ConnectedDeviceList alert, int position, boolean isDefaultDevices, boolean isActivePorts);
    }

    private void showActionSheet(final int pos) {

        Log.d("selectedPosition==", String.valueOf(pos));
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.unmute_actions_fragment);

        TextView tvCustomizeOS = dialog.findViewById(R.id.tvAllow);
        TextView tvCustomizeDeviceName = dialog.findViewById(R.id.tvUnMute);
        TextView tvBlockDevice = dialog.findViewById(R.id.tvBlockForever);
        TextView tvActivePorts = dialog.findViewById(R.id.tvAllowForever);
        TextView tvDefaultDevices = dialog.findViewById(R.id.tvButtonFive);
        TextView tvCancel = dialog.findViewById(R.id.tvCancel);
        tvDefaultDevices.setVisibility(View.VISIBLE);

        tvCustomizeOS.setText(mContext.getResources().getString(R.string.customize_operating_system));
        tvCustomizeDeviceName.setText(mContext.getResources().getString(R.string.customize_device_name));
        tvBlockDevice.setText(mContext.getResources().getString(R.string.block_device));
        tvActivePorts.setText(mContext.getResources().getString(R.string.view_risk_profile));
        tvDefaultDevices.setText(mContext.getResources().getString(R.string.cancel));

        if (listCnctdDevices.get(pos).getdeviceCategory().equalsIgnoreCase("Router")) {
            tvBlockDevice.setVisibility(View.GONE);
        }
        if (listCnctdDevices.get(pos).jsonArrayPorts.length() > 0 || listCnctdDevices.get(pos).jsonArrayDefaultPasswords.length() > 0) {
            tvActivePorts.setVisibility(View.VISIBLE);
        } else {
            tvActivePorts.setVisibility(View.GONE);
        }

        tvCustomizeOS.setOnClickListener(view -> {
            isChangeOS = true;
            isChangeUserName = false;
            showDeviceNameInput(pos, "Change operating system");
            dialog.dismiss();
        });
        tvCustomizeDeviceName.setOnClickListener(view -> {
            isChangeOS = false;
            isChangeUserName = true;
            showDeviceNameInput(pos, "Enter device name");
            dialog.dismiss();
        });
        tvBlockDevice.setOnClickListener(view -> {
           if ( listCnctdDevices.get(pos).getIp_cd().equalsIgnoreCase(DEVICE_IP)){
               blockOwnDeviceCall(pos);
               dialog.dismiss();
           }else{
              blockDeviceCall(pos);
                dialog.dismiss();
           }
        });
        tvActivePorts.setOnClickListener(view -> {

            ConnectedDeviceList model = listCnctdDevices.get(pos);
            listenerCnctdDevice.onAlertSelected(model, pos, false, true);
            dialog.dismiss();
        });
        tvDefaultDevices.setOnClickListener(view -> dialog.dismiss());
        tvCancel.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

}


