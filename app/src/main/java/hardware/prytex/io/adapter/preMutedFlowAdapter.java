package hardware.prytex.io.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.model.PreMutedAlertsModel;

import java.util.ArrayList;

public class preMutedFlowAdapter extends RecyclerView.Adapter<preMutedFlowAdapter.ViewHolder> {

    private final ArrayList<PreMutedAlertsModel> listFlowAlerts;
    final Context c;

    public preMutedFlowAdapter(Context context, ArrayList<PreMutedAlertsModel> alName) {
        super();
        this.listFlowAlerts = alName;
        this.c = context;
    }

    @NonNull
    @Override
    public preMutedFlowAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.flow_item_short, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(preMutedFlowAdapter.ViewHolder viewHolder, int i) {

        viewHolder.tvDescription.setText(listFlowAlerts.get(i).getDescription());

        String timeStmp = "00:00:00";
        String dy = "";
        String format = "00:00:00";
        try {
            long alertEveSec = listFlowAlerts.get(i).getEve_sec();
//            long alertEveSecLon = listPreMutedAlerts.get(groupPosition).getEve_sec();
//                timeStmp = getrTimeStamp(alertEveSec);
            format = PreMutedAlertAdapter.getConvertedFormat(alertEveSec);//getHourDayStamp(alertEveSec, false, false, true);
            timeStmp = PreMutedAlertAdapter.getConvertedHour(alertEveSec);//getHourDayStamp(alertEveSec, true, false, false);
            dy = PreMutedAlertAdapter.getConvertedDay(alertEveSec);//getHourDayStamp(alertEveSec, false, true, false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (format.contains("PM") || format.contains("pm")) {
            format = "PM";
        } else {
            format = "AM";
        }
        int endingHour = Integer.parseInt(timeStmp);
        endingHour = endingHour + 1;
        if (endingHour == 0) {
            endingHour = 1;
        } else if (endingHour == 13) {
            endingHour = 1;
        }
        viewHolder.tvTimestamp.setText(dy +  c.getResources().getString(R.string.selected) + timeStmp + "-" + endingHour +" "+ format);

    }

    @Override
    public int getItemCount() {
        return listFlowAlerts.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView tvDescription;
        final TextView tvTimestamp;

        ViewHolder(final View itemView) {
            super(itemView);
            tvDescription = itemView.findViewById(R.id.txt_description);
            tvTimestamp = itemView.findViewById(R.id.txt_timestamp);


        }

    }
}
