package hardware.prytex.io.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hardware.prytex.io.R;

import java.util.List;

/**
 * Created by macbookpro13 on 12/01/2018.
 */

public class FaqsAdapter extends RecyclerView.Adapter<FaqsAdapter.FaqViewHolder> {

    private static List<String> questionList;
    private static List<String> ansList;

    public FaqsAdapter(Context context, List<String> questionsList, List<String> ansList) {
        questionList = questionsList;
        FaqsAdapter.ansList = ansList;
    }


    @NonNull
    @Override
    public FaqViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.faq_list_item, parent, false);
        return new FaqViewHolder(v);
    }

    @Override
    public void onBindViewHolder(FaqViewHolder holder, final int position) {

        holder.tvHeading.setText(questionList.get(position));
        holder.description.setText(ansList.get(position));
    }

    @Override
    public int getItemCount() {
        return questionList.size();
    }

    public static class FaqViewHolder extends RecyclerView.ViewHolder {

        final TextView description;
        final TextView tvHeading;

        final View mainView;

        FaqViewHolder(final View itemView) {
            super(itemView);
            mainView = itemView;
            tvHeading = mainView.findViewById(R.id.tvHeading);
            description = mainView.findViewById(R.id.tvDetailAns);
        }

    }


}
