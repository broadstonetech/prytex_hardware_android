package hardware.prytex.io.adapter;

import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;
import static hardware.prytex.io.util.GeneralUtils.getFilteredResults;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.model.PolicyPlanModel;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import hardware.prytex.io.activity.DashBoardActivity;

public class PoliciesControlAdapter extends RecyclerView.Adapter<PoliciesControlAdapter.ViewHolder> implements Filterable {

    private ArrayList<PolicyPlanModel> policiesList;
    private final ArrayList<PolicyPlanModel> searchAbledataList;
    private final Context context;
    private final OnAlertSelectedListener listenerPolicy;
    boolean policyControl = false;
    int policyPosition;
    private ProgressDialog dialog;
    private final String appId = "";
    private int selectedIndex = -1;
    private int policyStatus = 1;
    private ArrayList<PolicyPlanModel> filteredList;

    private final AsyncTaskListener listenerPostPolicy = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            if (result.message.equalsIgnoreCase("")) {
                result.message = context.getResources().getString(R.string.error_message_generic);
            }

            showProgressDialog(false);
            if (result.code == 200 || result.code == 2) {
                //policy create successfully change switch btn status
                policiesList.get(selectedIndex).setPolicyStatus(policyStatus);
                notifyDataSetChanged();

            } else if (result.code == 503) {
                showAlertMsgDialog(context, result.message);
            } else if (result.code == 409) {
                showAlertMsgDialog(context, result.message);
            } else {
                PolicyPlanModel polModel = policiesList.get(policyPosition);
                listenerPolicy.onAlertSelected(polModel, policyPosition);
                // showAlertMsgDialog(context, result.message);
            }
        }
    };

    public PoliciesControlAdapter(Context context, ArrayList<PolicyPlanModel> alName, OnAlertSelectedListener listener) {
        super();
        this.context = context;
        this.policiesList = alName;
        this.searchAbledataList = alName;
        this.listenerPolicy = listener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                policiesList = (ArrayList<PolicyPlanModel>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                ArrayList<PolicyPlanModel> filteredResults = null;
                String charString = constraint.toString();
                if (charString.isEmpty()) {
                    filteredResults = searchAbledataList;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase(),policiesList);
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.policy_plan_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

        policyPosition = viewHolder.getAdapterPosition();
        viewHolder.tvPolicyTitle.setText(policiesList.get(i).getPolicyTitle());
        viewHolder.tvPolicyInterval.setText(policiesList.get(i).getStartTime() + " - " + policiesList.get(i).getEndTime());
        if (policiesList.get(i).getPreDefPolicy()) {
            viewHolder.tvDevicesCountHeading.setText(policiesList.get(i).getDescripton());
        } else
            viewHolder.tvDevicesCount.setText(String.valueOf(policiesList.get(i).policyHostsLists.length()));

        viewHolder.flMain.setOnClickListener(v -> {

            PolicyPlanModel model = policiesList.get(i);
            listenerPolicy.onAlertSelected(model, i);
        });
        viewHolder.tvDevicesCountHeading.setOnClickListener(v -> {

            PolicyPlanModel model = policiesList.get(i);
            listenerPolicy.onAlertSelected(model, i);
        });

        if (policiesList.get(i).getPolicyStatus() == 1) {
            viewHolder.policySwitchBtn.setSelected(true);
            viewHolder.policySwitchBtn.setChecked(true);
            viewHolder.imgSwitchBtn.setImageResource(R.drawable.switch_on);
        } else {
            viewHolder.policySwitchBtn.setSelected(false);
            viewHolder.policySwitchBtn.setChecked(false);
            viewHolder.imgSwitchBtn.setImageResource(R.drawable.switch_off);
        }

        viewHolder.imgSwitchBtn.setOnClickListener(v -> {
            PolicyPlanModel model = policiesList.get(i);
            if (model.getPolicyStatus() == 1) {

                Log.d("PolicySwitchBtn", "was on");
                policyStatus = 0;
                changePolicyStatusPrompt(policyStatus, i, true, viewHolder, model);
            } else {

                Log.d("PolicySwitchBtn", "was off");
//                    if (isCancelDialog){
                policyStatus = 1;
                changePolicyStatusPrompt(policyStatus, i, false, viewHolder, model);

            }
        });


    }

    private void changePolicyStatusPrompt(final int policyStatus, final int position, final boolean isSwitchSelected, final ViewHolder viewHolder, final PolicyPlanModel model) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("d.moat");
        builder.setMessage("Do you really want to update policy status?")
                .setCancelable(false)
                .setPositiveButton("Update", (dialog, id) -> {
                    showProgressDialog(true);
                    selectedIndex = position;
                    Api.createPolicy(context, getRequestparams(policyStatus, model, position), listenerPostPolicy);

                })
                .setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
        alert.setCancelable(true);
    }

    @Override
    public int getItemCount() {
        return policiesList.size();
    }

    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(context, "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private JSONObject getRequestparams(int policyStatus, PolicyPlanModel model, int position) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        @SuppressLint("CommitPrefEdits")

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"),
                Locale.getDefault());
        Date currentLocalTime = calendar.getTime();
        @SuppressLint("SimpleDateFormat") DateFormat date = new SimpleDateFormat("Z");
        String localTime = date.format(currentLocalTime);

        DateFormat dates = new SimpleDateFormat("z", Locale.getDefault());
        String localTimes = dates.format(currentLocalTime);
        Log.d("TimezoneLocal", localTime);
        String str = new StringBuffer(localTime).insert(localTime.length() - 2, ":").toString();

        Log.d("TimezoneLocalFormat", str);

        JSONObject j = new JSONObject();

        try {
            JSONObject data = new JSONObject();

            data.put("type", "update");
            data.put("id", model.getPolicyId());
            data.put("status", policyStatus);
            data.put("title", model.getPolicyTitle());
            data.put("s_time", model.getStartTime());
            data.put("e_time", model.getEndTime());
            data.put("s_time_gmt", model.getStartTimeGMT());
            data.put("e_time_gmt", model.getEndTimeGMT());
            data.put("days", model.getPolicyDays());
            data.put("time_zone", str);
            data.put("hosts_list", model.policyHostsLists);

            j.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            j.put("request_id", Dmoat.getNewRequestId());
            j.put("app_id", pref.getString("app_id", ""));
            j.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            j.put("token", UserManager.getInstance().getLoggedInUser().token);
            j.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("PolicPlanCreateParam", j.toString());
        return j;
    }

    public interface OnAlertSelectedListener {
        void onAlertSelected(PolicyPlanModel policyModel, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        final TextView tvPolicyTitle;
        final TextView tvPolicyInterval;
        final TextView tvDevicesCount;
        final TextView tvDevicesCountHeading;
        final CardView cardView;
        final LinearLayout llItemView;
        final FrameLayout flMain;
        final SwitchCompat policySwitchBtn;
        final ImageView imgSwitchBtn;

        ViewHolder(final View itemView) {
            super(itemView);
            tvPolicyTitle = itemView.findViewById(R.id.tvPolicyTitle);
            tvPolicyInterval = itemView.findViewById(R.id.tvPolicyInterval);
            tvDevicesCount = itemView.findViewById(R.id.tvDevicesCount);
            tvDevicesCountHeading = itemView.findViewById(R.id.tvDevicesCountHeading);
            cardView = itemView.findViewById(R.id.card_view);
            llItemView = itemView.findViewById(R.id.llItemDetails);
            flMain = itemView.findViewById(R.id.flMain);
            policySwitchBtn = itemView.findViewById(R.id.swicth_policyStatus);
            imgSwitchBtn = itemView.findViewById(R.id.imgSwitchBtn);

        }
    }


}

