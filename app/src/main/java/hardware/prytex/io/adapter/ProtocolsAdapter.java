package hardware.prytex.io.adapter;

import android.content.Context;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import hardware.prytex.io.R;

import java.util.HashMap;

import hardware.prytex.io.fragment.Policy.AccessControl.PolicyProtocolsFragment;

/** not in use now. usage in plicies screen to select protocols
 * Created by macbookpro13 on 29/05/2017.
 */

public class ProtocolsAdapter extends RecyclerView.Adapter<ProtocolsAdapter.ViewHolder> {

    private final String[] listProtocolKeys;
    private final String[] listProtocolVALUES;
    private final Integer[] protocolImages;
    final Context c;

    // 1. This will store all our selected entries
   public static HashMap<Integer,Integer> selectedProtocolsIndices;

    public ProtocolsAdapter(Context context, String[] listProtocolKeys, String[] listProtocolVALUES, Integer[] listProtocolImages) {
        super();
        this.listProtocolKeys = listProtocolKeys;
        this.listProtocolVALUES = listProtocolVALUES;
        this.protocolImages = listProtocolImages;

        // 2. init list
        selectedProtocolsIndices = new HashMap<>();
        this.c = context;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.policy_protocol_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ProtocolsAdapter.ViewHolder holder, final int position) {
        holder.protocolName.setText(listProtocolKeys[position]);
        holder.protocolValue.setText(listProtocolVALUES[position]);
        holder.imgProtocol.setImageResource(protocolImages[position]);
        holder.checkBox.setTag(position);



        if (PolicyProtocolsFragment.isAllProtocolsSelectedd) {
//            holder.btnAcces.setImageResource(R.drawable.selected);
            holder.btnAcces.setText(c.getResources().getString(R.string.selected));
            holder.checkBox.setChecked(true);
        } else {
//            holder.btnAcces.setImageResource(R.drawable.select);

//            holder.btnAcces.setText("Select");
//            holder.checkBox.setChecked(false);


            // 3. If select all button is not clicked then
            // check every item in the selected list & if
            // found mark it as selected
            if(selectedProtocolsIndices.containsValue(position))
                holder.btnAcces.setText(c.getResources().getString(R.string.selected));
            else
                holder.btnAcces.setText(c.getResources().getString(R.string.select));

        }

        PolicyProtocolsFragment.btnselectAll.setOnClickListener(v -> {

            if (PolicyProtocolsFragment.isAllProtocolsSelectedd) {
                PolicyProtocolsFragment.isAllProtocolsSelectedd = false;
                PolicyProtocolsFragment.btnselectAll.setImageResource(R.drawable.select_all_white);
                notifyDataSetChanged();
//                    notify();
            } else {
                PolicyProtocolsFragment.isAllProtocolsSelectedd = true;
                PolicyProtocolsFragment.btnselectAll.setImageResource(R.drawable.select_all);
               notifyDataSetChanged();

            }
        });

        holder.checkBox.setOnClickListener(v -> {
            if (PolicyProtocolsFragment.isAllProtocolsSelectedd) {

                PolicyProtocolsFragment.btnselectAll.setImageResource(R.drawable.select_all_white);
                PolicyProtocolsFragment.isAllProtocolsSelectedd = false;
            }

        });

        //btn visibility gone
        holder.btnAcces.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {

//                String SelectAllBtn = btnselectAll.gettext
                String AccessBtn = holder.btnAcces.getText().toString();
                if (AccessBtn.equalsIgnoreCase("SELECTED")) {
                    PolicyProtocolsFragment.btnselectAll.setImageResource(R.drawable.select_all_white);
//                    holder.btnAcces.setImageResource(R.drawable.select);
                    holder.btnAcces.setText(c.getResources().getString(R.string.select));
                    holder.checkBox.setChecked(false);
                    PolicyProtocolsFragment.isAllProtocolsSelectedd = false;
                    holder.checkBox.setChecked(false);

                    try{
                        // 4. Remove item from selected list
                        selectedProtocolsIndices.remove(position);
//                        ProtocolsAdapter.this.notify();
                        notify();
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                } else {
//                    holder.btnAcces.setImageResource(R.drawable.selected);
                    holder.btnAcces.setText(c.getResources().getString(R.string.selected));
                    holder.checkBox.setChecked(true);

                    // 5. Add item to the selected list
                    selectedProtocolsIndices.put(position, position);
                    notifyDataSetChanged();
                }


//                if (isAllProtocolsSelectedd) {
//                    btnselectAll.setImageResource(R.drawable.select_all_white);
////                    holder.btnAcces.setImageResource(R.drawable.select);
//                    holder.btnAcces.setText("Select");
//                    holder.checkBox.setChecked(false);
//                    isAllProtocolsSelectedd = false;
//                } else if (issingleProtocolSelected) {
////                    holder.btnAcces.setImageResource(R.drawable.select);
//                    holder.btnAcces.setText("Select");
//                    issingleProtocolSelected = false;
//                    holder.checkBox.setChecked(false);
//                }
//
////                    else {
////                        btnselectAll.setImageResource(R.drawable.select_all_white);
////                        if (issingleProtocolSelected) {
////                            btnAcces.setImageResource(R.drawable.select);
////                            issingleProtocolSelected = false;
////                            checkBox.setChecked(false);
////                        }
//
//                else {
////                    holder.btnAcces.setImageResource(R.drawable.selected);
//                    holder.btnAcces.setText("Selected");
//                    issingleProtocolSelected = true;
//                    holder.checkBox.setChecked(true);
//                }

            }
        });

        //done button of fragment click listener
//        btnDoneSelection.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                //get all selected protocols and store in array list
////                getSelectedProtocols(holder);
//
//                listOfSelectedProtocols.clear();
//                for (int i = 0; i < PROTOCOLS_VALUE.length; i++) {
//                    String btnText = holder.btnAcces.getText().toString();
//                    if (holder.checkBox.isChecked())
//                        if (holder.btnAcces.getText().toString().equalsIgnoreCase("Selected")) {
//
//                            selectedProtocols = new SelectedProtocols();
//                            selectedProtocols.setId(i + 1);
//                            selectedProtocols.setProtocolKey(PROTOCOLS_KEYS[i]);
//                            selectedProtocols.setProtocolValue(PROTOCOLS_VALUE[i]);
//                            listOfSelectedProtocols.add(selectedProtocols);
//                        }
//                }
//                Log.d("protocolSelectedList", listOfSelectedProtocols.toString());
//                getHelper().replaceFragment(new CreateNewPolicy(), false, true);
//
//            }
//        });
    }


    @Override
    public int getItemCount() {
        return listProtocolKeys.length;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        final TextView protocolName;
        final TextView protocolValue;
        final ImageView imgProtocol;
        final Button btnAcces;
        final CheckBox checkBox;

        ViewHolder(final View itemView) {
            super(itemView);
            protocolName = itemView.findViewById(R.id.tvProtocolName);
            protocolValue = itemView.findViewById(R.id.tvProtocolValue);
            imgProtocol = itemView.findViewById(R.id.imgProtocol);
            btnAcces = itemView.findViewById(R.id.btnAccess);
            btnAcces.setText(c.getResources().getString(R.string.selected));
            checkBox = itemView.findViewById(R.id.cBox);

            itemView.setClickable(false);


        }


    }

}
