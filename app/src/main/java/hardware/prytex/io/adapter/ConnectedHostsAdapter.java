package hardware.prytex.io.adapter;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.model.ConnectedDeviceList;
import hardware.prytex.io.model.DMoatAlert;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.model.VulnerabilitiesListModal;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.MyToast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import hardware.prytex.io.activity.DashBoardActivity;

import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;
import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by abubaker on 11/1/16.
 */

@SuppressWarnings("ALL")
public class ConnectedHostsAdapter extends RecyclerView.Adapter<ConnectedHostsAdapter.ConnectedHostViewHolder> implements Filterable {

    private final Context mContext;
    private  ArrayList<DMoatAlert> mDataSet;
    private final ArrayList<ConnectedDeviceList> searchAbledataList = new ArrayList<>();
    private ProgressDialog dialog;
    private static ArrayList<ConnectedDeviceList> listCnctdDevices;
    ArrayList<VulnerabilitiesListModal> listCnctdVulnerabilities;
    public static int vulnPosition;
    private int pos;
    private String inputName;
    public static boolean isChangeOS = false;
    public static boolean isChangeUserName = false;
    private String eveSec;
    RecyclerView.LayoutParams lp;
    private final OnAlertSelectedListener listenerCnctdDevice;
    private final String appID;
    private final String DEVICE_IP;

    public ConnectedHostsAdapter(Context context, ArrayList<ConnectedDeviceList> listDevices, ArrayList<DMoatAlert> list, OnAlertSelectedListener listener, String appID, String deviceIp) {
        this.mContext = context;
        listCnctdDevices = listDevices;
        this.mDataSet = list;
        this.searchAbledataList.addAll(listDevices);
        this.listenerCnctdDevice = listener;
        this.appID = appID;
        this.DEVICE_IP = deviceIp;
    }

    @Override
    public ConnectedHostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.host_item, parent, false);
//        v.setMinimumHeight(200);
        return new ConnectedHostViewHolder(v);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final ConnectedHostViewHolder holder, @SuppressLint("RecyclerView") final int postion) {

        int position = holder.getAdapterPosition();
        String userName = listCnctdDevices.get(position).getUserName();
        String hostName = listCnctdDevices.get(position).getHostName_cd();
        String ip = listCnctdDevices.get(position).getIp_cd();
        String OS = listCnctdDevices.get(position).getOs_cd();
        String deviceCategory = listCnctdDevices.get(position).getdeviceCategory();
        String macAddress = listCnctdDevices.get(position).getMacAddress_cd();

        if (userName.equalsIgnoreCase("unknown") || userName.equalsIgnoreCase("Unknown")) {
            holder.llUserName.setVisibility(View.GONE);
        } else {
            holder.llUserName.setVisibility(View.VISIBLE);
        }
        if (ip.equalsIgnoreCase("unknown") || ip.equalsIgnoreCase("Unknown")) {
            holder.llIP.setVisibility(View.GONE);
        } else {
            holder.llIP.setVisibility(View.VISIBLE);
        }
        if (OS.equalsIgnoreCase("unknown") || OS.equalsIgnoreCase("Unknown")) {
            holder.llOS.setVisibility(View.GONE);
        } else {
            holder.llOS.setVisibility(View.VISIBLE);
        }
        if (deviceCategory.equalsIgnoreCase("unknown") || deviceCategory.equalsIgnoreCase("Unknown")) {
            holder.llDevciceCategory.setVisibility(View.GONE);
        } else {
            holder.llDevciceCategory.setVisibility(View.VISIBLE);
        }
        if (macAddress.equalsIgnoreCase("unknown") || macAddress.equalsIgnoreCase("Unknown")) {
            holder.llMacAddress.setVisibility(View.GONE);
        } else {
            holder.llMacAddress.setVisibility(View.VISIBLE);
        }
        if (listCnctdDevices.get(position).jsonArrayPorts.length() > 0) {
            holder.activePorts.setVisibility(View.VISIBLE);
            holder.llRisk.setVisibility(View.VISIBLE);
        } else {
            holder.activePorts.setVisibility(View.GONE);
            holder.llRisk.setVisibility(View.GONE);
        }

        if (listCnctdDevices.get(position).getdeviceCategory().equalsIgnoreCase("Router")) {
            holder.btnBlockDevices.setVisibility(View.GONE);
        } else {
            holder.btnBlockDevices.setVisibility(View.VISIBLE);
        }

        holder.name.setText(hostName);
        holder.ip.setText(ip);
        holder.os.setText(OS);
        holder.device_category.setText(deviceCategory);
        holder.macAddress.setText(macAddress);

        int a = listCnctdDevices.get(position).getEveSec_cd();
        String b = getFormattedEveSecTimeStamp(a);
        holder.timeStamp.setText(b);

        if (listCnctdDevices.get(position).getUserName().equalsIgnoreCase("unknown")) {
            holder.btnNameDevice.setText("Customize"); //SET NAME
            holder.userName.setText(listCnctdDevices.get(position).getUserName());
        } else {
            holder.btnNameDevice.setText("Customize");
            holder.userName.setText(listCnctdDevices.get(position).getUserName());
        }

        int y = listCnctdDevices.get(position).jsonArrayDefaultPasswords.length(); // pos change to position by bilawal
        if (listCnctdDevices.get(position).jsonArrayPorts.length() > 0 || listCnctdDevices.get(position).jsonArrayDefaultPasswords.length() > 0)  {
            holder.risk.setText(mContext.getResources().getString(R.string.risk_factor));
        } else {
            holder.llRisk.setVisibility(View.GONE);
        }


        holder.mainView.setOnClickListener(v -> {
            if (isDmoatOnline) {
                holder.mainView.setClickable(true);
                pos = position;
                showActionSheet(position);
            } else {
                showAlertMsgDialog(mContext, mContext.getResources().getString(R.string.dmoat_offline));
            }
        });

    }


    @Override
    public int getItemCount() {
        return listCnctdDevices.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listCnctdDevices = (ArrayList<ConnectedDeviceList>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                ArrayList<ConnectedDeviceList> filteredResults = null;
                String charString = constraint.toString();
                if (charString.isEmpty()) {
                    filteredResults = searchAbledataList;
                } else {
                    try {
                        filteredResults = getConnectedDevice(constraint.toString().toLowerCase(),listCnctdDevices);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

    private ArrayList<ConnectedDeviceList> getConnectedDevice(String constraint,ArrayList<ConnectedDeviceList> list) throws JSONException {
        ArrayList<ConnectedDeviceList> results = new ArrayList<>();

        for (ConnectedDeviceList item : list) {
            if (item.getHostName_cd().toLowerCase().contains(constraint) || item.getIp_cd().toLowerCase().contains(constraint) || item.getOs_cd().toLowerCase().contains(constraint) ){
                results.add(item);
            }
        }
        return results;
    }


    public static class ConnectedHostViewHolder extends RecyclerView.ViewHolder {

        final TextView name;
        final TextView ip;
        final TextView os;
        final TextView device_category;
        final TextView timeStamp;
        final TextView macAddress;
        final Button btnBlockDevices;
        final TextView highCount;
        final TextView lowCount;
        final TextView mediumCount;
        final ImageView rightArraow;
        final TextView userName;
        final TextView risk;
        final TextView defaultDevices;
        final TextView llVulnHeading;
        final Button activePorts;
        final Button btnNameDevice;
        final LinearLayout llMAin;
        final LinearLayout llVulnView;
        final LinearLayout llNoVulnFound;
        final LinearLayout llUserName;
        final LinearLayout llHostName;
        final LinearLayout llOS;
        final LinearLayout llIP;
        final LinearLayout llRisk;
        final LinearLayout llDefaultPwds;
        final LinearLayout llDevciceCategory;
        final LinearLayout llMacAddress;

        final View mainView;

        ConnectedHostViewHolder(View itemView) {
            super(itemView);
            mainView = itemView;
            name = mainView.findViewById(R.id.device_name);
            ip = mainView.findViewById(R.id.device_ip);
            os = mainView.findViewById(R.id.operating_system);
            device_category = mainView.findViewById(R.id.device_category);
            risk = itemView.findViewById(R.id.tv_risk);
            defaultDevices = itemView.findViewById(R.id.tv_DefaultPwds);
            timeStamp = mainView.findViewById(R.id.eve_sec_timestamp);
            macAddress = mainView.findViewById(R.id.mac_address);
            btnBlockDevices = mainView.findViewById(R.id.btn_block_Devices);
            highCount = mainView.findViewById(R.id.tvHighVulnerability);
            mediumCount = mainView.findViewById(R.id.tvMediumVulnerability);
            lowCount = mainView.findViewById(R.id.tvLowVulnerability);
            rightArraow = mainView.findViewById(R.id.tvLeftArrowColoured);
            userName = mainView.findViewById(R.id.device_username);
            btnNameDevice = mainView.findViewById(R.id.btn_rename_device);
            llMAin = mainView.findViewById(R.id.llMain);
            llVulnView = mainView.findViewById(R.id.llVulnView);
            llNoVulnFound = mainView.findViewById(R.id.llNo_VulnView);
            llVulnHeading = mainView.findViewById(R.id.tvPotentialVuln);
            activePorts = mainView.findViewById(R.id.tvActivePorts);
            llUserName = mainView.findViewById(R.id.llUsernamme);
            llHostName = mainView.findViewById(R.id.llHostname);
            llOS = mainView.findViewById(R.id.llOS);
            llIP = mainView.findViewById(R.id.llDeviceIp);
            llDevciceCategory = mainView.findViewById(R.id.llDeviceCategory);
            llMacAddress = mainView.findViewById(R.id.llMacaddress);
            llRisk = itemView.findViewById(R.id.llRisk);
            llDefaultPwds = itemView.findViewById(R.id.llDefaultPwds);

        }

    }

    public ArrayList<DMoatAlert> getDataSet() {
        return mDataSet;
    }

    public void addItem(DMoatAlert dataObj) {
        mDataSet.add(dataObj);
        notifyDataSetChanged();
    }

    public void addList(ArrayList<DMoatAlert> dataList) {
        mDataSet.addAll(dataList);
        notifyDataSetChanged();
    }

    public void clearAdapter() {
        mDataSet.clear();
        notifyDataSetChanged();

    }

    private void blockDeviceCall(final int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("d.moat");
        builder.setMessage("Do you really want to block device?")
                .setCancelable(false)
                .setPositiveButton("Block", (dialog, id) -> {
                    if (isDmoatOnline) {
                        showProgressDialog(true);
                        Api.blockConnectedDevice(mContext, getRequestParams(position), listener);
                    } else {
                        MyToast.showMessage(mContext, mContext.getResources().getString(R.string.dmoat_offline));
                    }
                })
                .setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
        alert.setCancelable(true);
    }

    private void blockOwnDeviceCall(final int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("d.moat");
        builder.setMessage("You are going to block current device. Do you really want to block device?")
                .setCancelable(false)
                .setPositiveButton("Block", (dialog, id) -> {
                    if (isDmoatOnline) {
                        showProgressDialog(true);
                        Api.blockConnectedDevice(mContext, getRequestParams(position), listener);
                    } else {
                        MyToast.showMessage(mContext, mContext.getResources().getString(R.string.dmoat_offline));
                    }
                })
                .setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
        alert.setCancelable(true);
    }


    private void showEditAlert(final int positn) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("d.moat");
        builder.setMessage("Rename the device name or operating system label.")
                .setCancelable(false)
                .setPositiveButton("Change device name", (dialog, id) -> {
                    isChangeUserName = true;
                    isChangeOS = false;
                    showDeviceNameInput(positn, "Enter device name");
                })
                .setNegativeButton("Rename operating system", (dialog, id) -> {
                    isChangeOS = true;
                    isChangeUserName = false;
                    showDeviceNameInput(positn, "Rename operating system label.");
                    dialog.cancel();
                });
        AlertDialog alert = builder.create();
        alert.show();
        alert.setCancelable(true);
    }

    private void showDeviceNameInput(final int position, String title) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(mContext);

        final LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(20, 0, 20, 0);
        final EditText edittext = new EditText(mContext);
        edittext.setLayoutParams(lp);
        edittext.setSingleLine(true);
        edittext.setImeOptions(EditorInfo.IME_ACTION_DONE);
        edittext.setMaxLines(1);
        alert.setMessage("");
        alert.setTitle(title);


        alert.setView(edittext);

        alert.setPositiveButton("Ok", (dialog, whichButton) -> {
            inputName = edittext.getText().toString();
            if (!inputName.equalsIgnoreCase("") || !inputName.contentEquals(" ")){
                if (isChangeOS) {
                    Api.renameConnectedDevice(mContext, getRenameRequestParams(position, listCnctdDevices.get(position).getUserName(), inputName), listenerRename);
                } else if (isChangeUserName) {
                    Api.renameConnectedDevice(mContext, getRenameRequestParams(position, inputName, listCnctdDevices.get(position).getOs_cd()), listenerRename);

                } else {

                }
            }else{
                showAlertMsgDialog(mContext, "Please enter a valid name.");
            }

        });

        alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
            // dismiss dialog

        });

        alert.show();

    }

    private final AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            showProgressDialog(false);

            if (result.code == 200) {
//                Toast.makeText(mContext, "Device Blocked", Toast.LENGTH_SHORT).show();
                listCnctdDevices.remove(pos);
                Log.d("device blocked at", String.valueOf(pos));
                notifyDataSetChanged();
            } else {
                Toast.makeText(mContext, result.message, Toast.LENGTH_SHORT).show();
            }

        }
    };

    private final AsyncTaskListener listenerRename = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            showProgressDialog(false);

            if (result.code == 200) {
                if (isChangeOS) {
                    listCnctdDevices.get(pos).setOs_cd(inputName);
                    notifyDataSetChanged();
                } else if (isChangeUserName) {
                    listCnctdDevices.get(pos).setUserName(inputName);
                    notifyDataSetChanged();
                } else {
                }
            } else {
                Toast.makeText(mContext, result.message, Toast.LENGTH_SHORT).show();
            }

        }
    };

    private void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(mContext, "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private JSONObject getRequestParams(int position) {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();
        JSONObject response = new JSONObject();
        try {

            data.put("macaddress", listCnctdDevices.get(position).getMacAddress_cd());
            data.put("HOSTID", listCnctdDevices.get(position).getHostId_cd());
            data.put("parameters", listCnctdDevices.get(position).getParametrs_cd());
            data.put("eve_sec", listCnctdDevices.get(position).getEveSec_cd());
            data.put("ip", listCnctdDevices.get(position).getIp_cd());
            data.put("hostname", listCnctdDevices.get(position).getHostName_cd());
            data.put("record_id", listCnctdDevices.get(position).getRecord_id());
            data.put("host_id", listCnctdDevices.get(position).getHostId_cd());
            data.put("OS", listCnctdDevices.get(position).getOs_cd());

            response.put("interval", "");
            response.put("type", "Block");
            response.put("mode", "Forever");

            data.put("response", response);
            data.put("device_category", listCnctdDevices.get(position).getdeviceCategory());
            js.put("data", data);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            js.put("app_id", appID);
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    private JSONObject getRenameRequestParams(int position, String userName, String deviceName) {
        JSONObject js = new JSONObject();
        JSONObject data = new JSONObject();
        try {

            data.put("record_id", listCnctdDevices.get(position).getRecord_id());
            data.put("devicename", deviceName);
            data.put("usersname", userName);
            js.put("data", data);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            js.put("app_id", appID);
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    //eve sec to time stamp
    private String getFormattedEveSecTimeStamp(int es) {
        try {
            return getrTimeStamp(es);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return eveSec;
    }

    public static String getrTimeStamp(int s) throws Exception {
        float fTime = Float.parseFloat(String.valueOf(s));
        long time = (long) fTime;
        time = time * 1000;
        Date date = new Date(time);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sourceFormat = new SimpleDateFormat("MMM-dd-yyyy h:mm a");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateString = sourceFormat.format(date); // => Date is in UTC now
        Date parsed = sourceFormat.parse(dateString); // => Date is in UTC now

        @SuppressLint("SimpleDateFormat") SimpleDateFormat destFormat = new SimpleDateFormat("MMM-dd-yyyy h:mm a");
//        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        destFormat.setTimeZone(Calendar.getInstance().getTimeZone());
        return destFormat.format(parsed);
    }

    public interface OnAlertSelectedListener {
        void onAlertSelected(ConnectedDeviceList alert, int position, boolean isDefaultDevices, boolean isActivePorts);
    }

    private void showActionSheet(final int pos) {

        Log.d("selectedPosition==", String.valueOf(pos));
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.unmute_actions_fragment);

        TextView tvCustomizeOS = dialog.findViewById(R.id.tvAllow);
        TextView tvCustomizeDeviceName = dialog.findViewById(R.id.tvUnMute);
        TextView tvBlockDevice = dialog.findViewById(R.id.tvBlockForever);
        TextView tvActivePorts = dialog.findViewById(R.id.tvAllowForever);
        TextView tvDefaultDevices = dialog.findViewById(R.id.tvButtonFive);
        TextView tvCancel = dialog.findViewById(R.id.tvCancel);
        tvDefaultDevices.setVisibility(View.VISIBLE);

        tvCustomizeOS.setText("Customize Operating System");
        tvCustomizeDeviceName.setText("Customize Device Name");
        tvBlockDevice.setText("Block Device");
        tvActivePorts.setText("View Risk Profile");
        tvDefaultDevices.setText("Cancel");

        if (listCnctdDevices.get(pos).getdeviceCategory().equalsIgnoreCase("Router")) {
            tvBlockDevice.setVisibility(View.GONE);
        }
        if (listCnctdDevices.get(pos).jsonArrayPorts.length() > 0 || listCnctdDevices.get(pos).jsonArrayDefaultPasswords.length() > 0) {
            tvActivePorts.setVisibility(View.VISIBLE);
        } else {
            tvActivePorts.setVisibility(View.GONE);
        }

        tvCustomizeOS.setOnClickListener(view -> {
            isChangeOS = true;
            isChangeUserName = false;
            showDeviceNameInput(pos, "Change operating system");
            dialog.dismiss();
        });
        tvCustomizeDeviceName.setOnClickListener(view -> {
            isChangeOS = false;
            isChangeUserName = true;
            showDeviceNameInput(pos, "Enter device name");
            dialog.dismiss();
        });
        tvBlockDevice.setOnClickListener(view -> {
            if ( listCnctdDevices.get(pos).getIp_cd().equalsIgnoreCase(DEVICE_IP)){
                blockOwnDeviceCall(pos);
                dialog.dismiss();
            }else{
                blockDeviceCall(pos);
                dialog.dismiss();
            }
        });
        tvActivePorts.setOnClickListener(view -> {

            ConnectedDeviceList model = listCnctdDevices.get(pos);
            listenerCnctdDevice.onAlertSelected(model, pos, false, true);
            dialog.dismiss();
        });
        tvDefaultDevices.setOnClickListener(view -> dialog.dismiss());
        tvCancel.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

}


