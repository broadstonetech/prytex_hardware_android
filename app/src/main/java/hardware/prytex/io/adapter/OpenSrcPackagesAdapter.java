package hardware.prytex.io.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hardware.prytex.io.R;

import java.util.List;

public class OpenSrcPackagesAdapter extends RecyclerView.Adapter<OpenSrcPackagesAdapter.ViewHolder> {

    private final List<String> packagesList;
     private final List<String> versionsList;

    public OpenSrcPackagesAdapter(Context context, List<String> packagesList, List<String> versionsList) {
        this.packagesList = packagesList;
        this.versionsList = versionsList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.open_src_row, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.tvPackageName.setText(packagesList.get(position));
        holder.tvVersion.setText(versionsList.get(position));
    }

    @Override
    public int getItemCount() {
        return packagesList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView tvPackageName;
        final TextView tvVersion;

        final View mainView;

        ViewHolder(final View itemView) {
            super(itemView);
            mainView = itemView;
            tvPackageName = mainView.findViewById(R.id.tvPackageName);
            tvVersion = mainView.findViewById(R.id.tvVersion);
        }

    }


}