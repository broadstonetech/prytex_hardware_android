package hardware.prytex.io.adapter;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.model.PolicyPlanModel;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.MyToast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import hardware.prytex.io.fragment.Policy.ProtocolsPolicy.ProtocolPoliciesFragment;
import hardware.prytex.io.fragment.Policy.ProtocolsPolicy.ProtocolPolicyControlFragment;

import static hardware.prytex.io.fragment.Policy.ProtocolsPolicy.ProtocolPoliciesFragment.getDeleteRequestparams;
import static hardware.prytex.io.fragment.Policy.ProtocolsPolicy.ProtocolPolicyControlFragment.prepareProtoclValuesForBlocking;
import static hardware.prytex.io.fragment.Policy.ProtocolsPolicy.ProtocolPolicyControlFragment.protocolsValuesDisctionarForBlocking;
import static hardware.prytex.io.parser.PotocolPoliciesParser.listOfProtocolPolicies;


public class ProtocolPolicyListAdapter extends RecyclerView.Adapter<ProtocolPolicyListAdapter.ViewHolder> implements Filterable {

    private ArrayList<PolicyPlanModel> policiesList;
    private final ArrayList<PolicyPlanModel> searchAbledataList;
    private final Context context;
    private final ProtocolPolicyListAdapter.OnAlertSelectedListener listenerPolicy;
    private ProgressDialog dialog;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String appId = "";
    private int selectedIndex = -1;
    private final int policyStatus = 1;

    public ProtocolPolicyListAdapter(Context context, ArrayList<PolicyPlanModel> alName, ProtocolPolicyListAdapter.OnAlertSelectedListener listener) {
        super();
        this.context = context;
        this.policiesList = alName;
        this.searchAbledataList = alName;
        this.listenerPolicy = listener;
    }


    @Override
    public ProtocolPolicyListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.policy_plan_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ProtocolPolicyListAdapter.ViewHolder viewHolder, final int i) {

        viewHolder.tvPolicyTitle.setText(policiesList.get(i).getPolicyTitle());
        if (policiesList.get(i).getType().equalsIgnoreCase("network")) {
            viewHolder.tvPolicyInterval.setText(policiesList.get(i).getType());
            viewHolder.imgPolicyType.setImageResource(R.drawable.network_policy);
        } else {
            viewHolder.tvPolicyInterval.setText(policiesList.get(i).getHostName());
            viewHolder.imgPolicyType.setImageResource(R.drawable.devices_policy);
        }

        viewHolder.imgSwitchBtn.setImageResource(R.drawable.delete_policy);
        String protocolString = getSelectedProtocolsInPolicy(policiesList, i);
        viewHolder.tvDevicesCount.setText(protocolString);

        viewHolder.llItemView.setOnClickListener(v -> {

            PolicyPlanModel model = policiesList.get(i);
            listenerPolicy.onAlertSelected(model, i);
        });

        viewHolder.imgSwitchBtn.setOnClickListener(v -> promptDeletePolicy(context, i));

    }

    @Override
    public int getItemCount() {
        return policiesList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                policiesList = (ArrayList<PolicyPlanModel>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                ArrayList<PolicyPlanModel> filteredResults = null;
                String charString = constraint.toString();
                if (charString.isEmpty()) {
                    filteredResults = searchAbledataList;
                } else {
                    try {
                        filteredResults = getBlockedProtocls(constraint.toString().toLowerCase(),policiesList);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

    private ArrayList<PolicyPlanModel> getBlockedProtocls(String constraint,ArrayList<PolicyPlanModel> list) throws JSONException {
        ArrayList<PolicyPlanModel> results = new ArrayList<>();

        for (PolicyPlanModel item : list) {
            for(int x = 0; x < item.policyProtocolsLists.length(); x++) {
                if (item.policyProtocolsLists.getJSONObject(x).getString("protocols").toLowerCase().contains(constraint)){
                    results.add(item);
                }
            }
        }
        return results;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        final TextView tvPolicyTitle;
        final TextView tvPolicyInterval;
        final TextView tvDevicesCount;
        final TextView tvDevicesCountHeading;
        final TextView tvIntervalHeading;
        final TextView tvActionsLabel;
        final CardView cardView;
        final LinearLayout llItemView;
        final SwitchCompat policySwitchBtn;
        final ImageView imgSwitchBtn;
        final ImageView imgPolicyType;

        ViewHolder(final View itemView) {
            super(itemView);
            tvPolicyTitle = itemView.findViewById(R.id.tvPolicyTitle);
            tvPolicyInterval = itemView.findViewById(R.id.tvPolicyInterval);
            tvIntervalHeading = itemView.findViewById(R.id.tvIntervalHeading);
            tvDevicesCountHeading = itemView.findViewById(R.id.tvDevicesCountHeading);
            tvDevicesCount = itemView.findViewById(R.id.tvDevicesCount);
            cardView = itemView.findViewById(R.id.card_view);
            llItemView = itemView.findViewById(R.id.llItemDetails);
            policySwitchBtn = itemView.findViewById(R.id.swicth_policyStatus);
            imgSwitchBtn = itemView.findViewById(R.id.imgSwitchBtn);
            imgPolicyType = itemView.findViewById(R.id.imgPolicyType);
            tvActionsLabel = itemView.findViewById(R.id.tvActionsLabel);

            imgPolicyType.setVisibility(View.VISIBLE);
            tvActionsLabel.setVisibility(View.GONE);
            imgSwitchBtn.setVisibility(View.VISIBLE);
            policySwitchBtn.setVisibility(View.GONE);
            tvDevicesCountHeading.setText("Protocols:");
            tvIntervalHeading.setText("Applied on:");

        }
    }

    public interface OnAlertSelectedListener {
        void onAlertSelected(PolicyPlanModel policyModel, int position);
    }

    private String getSelectedProtocolsInPolicy(ArrayList<PolicyPlanModel> list, int i) {
        JSONObject jsonObject;
        StringBuilder protocolString = new StringBuilder();
        prepareProtoclValuesForBlocking();
        for (int j = 0; j < list.get(i).policyProtocolsLists.length(); j++) {
            try {
                jsonObject = list.get(i).policyProtocolsLists.getJSONObject(j);//(j);
                Log.d("jonj", jsonObject.toString());
                String protocols = jsonObject.getString("protocols");

                for (int k = 0; k < protocolsValuesDisctionarForBlocking.size(); k++) {
                    if (protocolsValuesDisctionarForBlocking.containsKey(protocols)) {
                        String protocolUiName = protocolsValuesDisctionarForBlocking.get(protocols);

                        if (j + 1 == list.get(i).policyProtocolsLists.length()) {
                            protocolString.append(protocolUiName);
                        } else {
                            protocolString.append(protocolUiName).append(",");
                        }
                        break;
                    } else {
                        Log.d("ProtocolControl", "not found in dictionary");
                    }

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return protocolString.toString();
    }

    @SuppressLint("CommitPrefEdits")
    private void promptDeletePolicy(final Context ctx, final int position) {
        new AlertDialog.Builder(ctx)
                .setTitle("d.moat")
                .setMessage(ctx.getResources().getString(R.string.delete_protocol_policy))
                .setPositiveButton("Delete", (dialog, whichButton) -> {
                    selectedIndex = position;
                    pref = ctx.getSharedPreferences("MyPref", 0);
                    editor = pref.edit();
                    appId = pref.getString("app_id", "");

                    Api.createProtocolPolicy(context, getDeleteRequestparams(appId, "delete", position), listener);

                })

                .setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss()).show();

    }

    private final AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            if (result.code == 200) {
                if (listOfProtocolPolicies.size() > 0) {
                    listOfProtocolPolicies.remove(selectedIndex);
//                    policiesList.remove(selectedIndex);
                    notifyDataSetChanged();

                } else {
                    if (result.message.equalsIgnoreCase("")) {
                        MyToast.showMessage(context, context.getResources().getString(R.string.no_bandwidth));
                    } else {
                        MyToast.showMessage(context, result.message);
                    }
                }

            }
        }
    };


}