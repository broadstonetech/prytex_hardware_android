package hardware.prytex.io.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.model.PreMutedAlertsModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static hardware.prytex.io.adapter.ConnectedHostsAdapter.getrTimeStamp;

/**
 * Created by macbookpro13 on 18/12/2017.
 */


@SuppressWarnings("ALL")
public class PreMutedAlertAdapter extends BaseExpandableListAdapter {

    private final Context _context;
    private final ArrayList<PreMutedAlertsModel> listPreMutedAlerts;
    private final List<String> _listDataHeader; // header titles
    private final List<String> _listDataHeaderInputSrc; // header titles
    private final List<Integer> _listDataHeaderTimeStamp; // header titles
    // child data in format of header title, child title
    private final HashMap<String, List<String>> _listDataChild;
    private final HashMap<String, List<String>> _listDataChildDest;

    public PreMutedAlertAdapter(Context context, List<String> listDataHeader,
                                HashMap<String, List<String>> listChildData, HashMap<String, List<String>> listChildDataDest, ArrayList<PreMutedAlertsModel> listPreMuted, List<String> listHeaderInputSrc, List<Integer> listDataHeaderTimeStamp) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this._listDataChildDest = listChildDataDest;
        this.listPreMutedAlerts = listPreMuted;
        this._listDataHeaderInputSrc = listHeaderInputSrc;
        this._listDataHeaderTimeStamp = listDataHeaderTimeStamp;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        Log.d("preMutedChildGet" ,String.valueOf(this._listDataChildDest.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon)));
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.pre_muted_child_item, null);
        }

        TextView tvSrcIp = convertView
                .findViewById(R.id.tv_src_ip);
        TextView tvDestIp = convertView
                .findViewById(R.id.tv_dest_ip);

        tvSrcIp.setText(this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosition));
        tvDestIp.setText(this._listDataChildDest.get(this._listDataHeader.get(groupPosition))
                .get(childPosition));
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        Log.d("preMutedChildCount", String.valueOf(this._listDataChildDest.get(this._listDataHeader.get(groupPosition)).size()));
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        Log.d("preMutedGroupCount",String.valueOf(this._listDataHeader.size()));
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.muted_alert_item_group, null);
        }

        String inputSrc = _listDataHeaderInputSrc.get(groupPosition);//.getInput_src();
        TextView lblListHeader = convertView
                .findViewById(R.id.lblListHeader);
        TextView tvtimeStamp = convertView.findViewById(R.id.tvEveSec);
        TextView tvActionsBtn = convertView.findViewById(R.id.tvActions);
        Log.d("Observed INPUTSRC==", inputSrc);
        if (inputSrc.equalsIgnoreCase("ids")) {
//        lblListHeader.setTypeface(null, Typeface.BOLD);
            lblListHeader.setText(headerTitle);
            int timeStamp = _listDataHeaderTimeStamp.get(groupPosition);//.getEve_sec();
            String timeStmp = "00:00:00";
            try {
                timeStmp = getrTimeStamp(timeStamp);
            } catch (Exception e) {
                e.printStackTrace();
            }
            tvtimeStamp.setText(timeStmp);
        } else if (inputSrc.equalsIgnoreCase("flow")) {
            tvActionsBtn.setVisibility(View.GONE);
            String timeStmp = "00:00:00";
            String dy = "";
            String format = "00:00:00";
            try {
                int alertEveSec = _listDataHeaderTimeStamp.get(groupPosition);//.getEve_sec();
                long alertEveSecLon = listPreMutedAlerts.get(groupPosition).getEve_sec();
//                timeStmp = getrTimeStamp(alertEveSec);
                format = getConvertedFormat(alertEveSec);//getHourDayStamp(alertEveSec, false, false, true);
                timeStmp = getConvertedHour(alertEveSec);//getHourDayStamp(alertEveSec, true, false, false);
                dy = getConvertedDay(alertEveSec);//getHourDayStamp(alertEveSec, false, true, false);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (format.contains("PM") || format.contains("pm")) {
                format = "PM";
            } else {
                format = "AM";
            }
            int endingHour = Integer.parseInt(timeStmp);
            endingHour = endingHour + 1;
            if (endingHour == 0) {
                endingHour = 1;
            } else if (endingHour == 13) {
                endingHour = 1;
            }
            lblListHeader.setText(headerTitle);//+ " on "+dayTitle +" between " + hourTitle +".");
            tvtimeStamp.setText(dy + _context.getResources().getString(R.string.between) + timeStmp + "-" + endingHour +" "+ format);
            Log.d("FlowAlertObserved", headerTitle + "eveSec==" + _listDataHeaderTimeStamp.get(groupPosition) + "slot==" + timeStmp + "day==" + dy);
        } else {
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public static String getConvertedDay(long dateTime) throws ParseException {
        float fTime = Float.parseFloat(String.valueOf(dateTime));
        long time = (long) fTime;
        time = time * 1000;
        Date date = new Date(time);
        SimpleDateFormat sourceFormat = new SimpleDateFormat("MMM-dd-yyyy h:mm a", Locale.getDefault());
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateString = sourceFormat.format(date); // => Date is in UTC now
        Date parsed = sourceFormat.parse(dateString); // => Date is in UTC now

        SimpleDateFormat destFormat = new SimpleDateFormat("EEEE", Locale.getDefault());
//        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        destFormat.setTimeZone(Calendar.getInstance().getTimeZone());
        return destFormat.format(parsed);
    }
    public static String getConvertedHour(long dateTime) throws ParseException {
        float fTime = Float.parseFloat(String.valueOf(dateTime));
        long time = (long) fTime;
        time = time * 1000;
        Date date = new Date(time);
        SimpleDateFormat sourceFormat = new SimpleDateFormat("MMM-dd-yyyy h:mm a", Locale.getDefault());
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateString = sourceFormat.format(date); // => Date is in UTC now
        Date parsed = sourceFormat.parse(dateString); // => Date is in UTC now

        SimpleDateFormat destFormat = new SimpleDateFormat("h", Locale.getDefault());
//        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        destFormat.setTimeZone(Calendar.getInstance().getTimeZone());
        return destFormat.format(parsed);
    }
    public static String getConvertedFormat(long dateTime) throws ParseException {
        float fTime = Float.parseFloat(String.valueOf(dateTime));
        long time = (long) fTime;
        time = time * 1000;
        Date date = new Date(time);
        SimpleDateFormat sourceFormat = new SimpleDateFormat("MMM-dd-yyyy h:mm a", Locale.getDefault());
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateString = sourceFormat.format(date); // => Date is in UTC now
        Date parsed = sourceFormat.parse(dateString); // => Date is in UTC now

        SimpleDateFormat destFormat = new SimpleDateFormat("a", Locale.getDefault());
//        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        destFormat.setTimeZone(Calendar.getInstance().getTimeZone());
        return destFormat.format(parsed);
    }

}