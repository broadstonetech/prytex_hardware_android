package hardware.prytex.io.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.model.DMoatAlert;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by abubaker on 11/1/16.
 */

public class ConnectedHostsWithCheckboxAdapter extends RecyclerView.Adapter<ConnectedHostsWithCheckboxAdapter.ConnectedHostViewHolder> {

    private final Context mContext;
    private final ArrayList<DMoatAlert> mDataSet;
    private ArrayList<DMoatAlert> selectedList;
    private final HashMap<DMoatAlert, DMoatAlert> selectedItems = new HashMap<>();

    public ConnectedHostsWithCheckboxAdapter(Context context, ArrayList<DMoatAlert> list) {
        this.mContext = context;
        this.mDataSet = list;
    }

    public ConnectedHostsWithCheckboxAdapter(Context context, ArrayList<DMoatAlert> list, ArrayList<DMoatAlert>
            selectedList) {
        this.mContext = context;
        this.mDataSet = list;
        this.selectedList = selectedList;
    }

    @NonNull
    @Override
    public ConnectedHostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.connected_host_with_checkbox_item, parent, false);
        return new ConnectedHostViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ConnectedHostViewHolder holder, int position) {
        DMoatAlert object = mDataSet.get(position);
        holder.name.setText(object.hostname);
        holder.ip.setText(object.ip);
        holder.os.setText(object.OS);
        holder.macAddress.setText(object.macAddress);
        if (selectedList != null && !selectedList.isEmpty()) {
            for (int i = 0; i < selectedList.size(); i++) {
                if (object.hostId.equals(selectedList.get(i).hostId)) {
                    holder.checkBox.setChecked(true);
                    selectedItems.put(object, object);
                    break;
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public class ConnectedHostViewHolder extends RecyclerView.ViewHolder {

        final TextView name;
        final TextView ip;
        final TextView os;
        final TextView macAddress;
        final CheckBox checkBox;

        final View mainView;

        ConnectedHostViewHolder(View itemView) {
            super(itemView);
            mainView = itemView;
            name = mainView.findViewById(R.id.device_name);
            ip = mainView.findViewById(R.id.device_ip);
            os = mainView.findViewById(R.id.operating_system);
            macAddress = mainView.findViewById(R.id.mac_address);
            checkBox = mainView.findViewById(R.id.checkbox);
            checkBox.setOnClickListener(v -> {
                if (checkBox.isChecked()) {
                    selectedItems.put(mDataSet.get(getAdapterPosition()), mDataSet.get(getAdapterPosition()));
                } else {
                    selectedItems.remove(mDataSet.get(getAdapterPosition()));
                }
            });
        }


    }

    public ArrayList<DMoatAlert> getDataSet() {
        return mDataSet;
    }

    public ArrayList<DMoatAlert> getSelectedItems() {
        ArrayList<DMoatAlert> list = new ArrayList<>();
        list.addAll(selectedItems.keySet());
        return list;
    }

    public void addItem(DMoatAlert dataObj) {
        mDataSet.add(dataObj);
        notifyDataSetChanged();

    }

    public void addList(ArrayList<DMoatAlert> dataList) {
        mDataSet.addAll(dataList);
        notifyDataSetChanged();

    }

    public void clearAdapter() {
        mDataSet.clear();
        notifyDataSetChanged();

    }
}
