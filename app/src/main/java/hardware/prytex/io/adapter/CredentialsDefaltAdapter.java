package hardware.prytex.io.adapter;

import android.app.ProgressDialog;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.model.ActivePortsModel;
import hardware.prytex.io.model.ConnectedDeviceList;

import java.util.ArrayList;

/**
 * Created by macbookpro13 on 05/04/2018.
 */

public class CredentialsDefaltAdapter  extends RecyclerView.Adapter<CredentialsDefaltAdapter.CredentialsDefaltViewHolder> {

    private final Context mContext;
    private ProgressDialog dialog;
    //    private int pos;
private final ArrayList<ActivePortsModel> defaultDevicesList;


    public CredentialsDefaltAdapter(Context context, ArrayList<ActivePortsModel> list) {
        this.mContext = context;
        this.defaultDevicesList = list;
    }

    @NonNull
    @Override
    public CredentialsDefaltAdapter.CredentialsDefaltViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.default_credentials_row, parent, false);
        return new CredentialsDefaltAdapter.CredentialsDefaltViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final CredentialsDefaltAdapter.CredentialsDefaltViewHolder holder, final int position) {

//        Log.d("defaultDevicesFound", String.valueOf(listCnctdDevices.get(position).jsonArrayDefaultPasswords));

        holder.modelName.setText(defaultDevicesList.get(position).getModelName());
        holder.deviceUserName.setText(defaultDevicesList.get(position).getUserName());
        holder.devicePwd.setText(defaultDevicesList.get(position).getDefaultPwd());

    }

    @Override
    public int getItemCount() {
        return defaultDevicesList.size();
    }

    public static class CredentialsDefaltViewHolder extends RecyclerView.ViewHolder {

        final TextView deviceUserName;
        final TextView modelName;
        final TextView devicePwd;
        final View mainView;

        CredentialsDefaltViewHolder(View itemView) {
            super(itemView);
            mainView = itemView;
            deviceUserName = mainView.findViewById(R.id.tvDefaultUsername);
            modelName = mainView.findViewById(R.id.tvDeviceModel);
            devicePwd = mainView.findViewById(R.id.tvDefaultPwd);


        }


    }

}