package hardware.prytex.io.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.model.BandWidth;
import hardware.prytex.io.model.DMoatAlert;

import java.util.ArrayList;

/**
 * Created by macbookpro13 on 09/08/2017.
 */

public class ProtocolDetailAdapter extends RecyclerView.Adapter<ProtocolDetailAdapter.ProtocolDetailViewHolder> {

    private final Context mContext;
    private ArrayList<DMoatAlert> mDataSet;
    private ProgressDialog dialog;
    private final ArrayList<BandWidth> listDetectedProtocolHosts;

    public ProtocolDetailAdapter(Context context, ArrayList<BandWidth> listDevices) {
        this.mContext = context;
        this.listDetectedProtocolHosts = listDevices;
    }

    @NonNull
    @Override
    public ProtocolDetailAdapter.ProtocolDetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.protocol_detail_new, parent, false);
        return new ProtocolDetailAdapter.ProtocolDetailViewHolder(v);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)

    @Override
    public void onBindViewHolder(ProtocolDetailAdapter.ProtocolDetailViewHolder holder, final int position) {

        holder.name.setText(listDetectedProtocolHosts.get(position).getHostName());

        holder.mainView.setOnClickListener(v -> {

        });
    }

    @Override
    public int getItemCount() {
        return listDetectedProtocolHosts.size();
    }

    public static class ProtocolDetailViewHolder extends RecyclerView.ViewHolder {

        final TextView name;
        public TextView ip;
        final View mainView;

        ProtocolDetailViewHolder(View itemView) {
            super(itemView);
            mainView = itemView;
            name = mainView.findViewById(R.id.tbHostProtocolName);
        }

    }

    public ArrayList<DMoatAlert> getDataSet() {
        return mDataSet;
    }

    public void addItem(DMoatAlert dataObj) {
        mDataSet.add(dataObj);
        notifyDataSetChanged();

    }

    public void addList(ArrayList<DMoatAlert> dataList) {
        mDataSet.addAll(dataList);
        notifyDataSetChanged();

    }


}


