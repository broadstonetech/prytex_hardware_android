package hardware.prytex.io.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import hardware.prytex.io.R;
import hardware.prytex.io.model.ConnectedDeviceList;

import java.util.ArrayList;

import hardware.prytex.io.fragment.SelectConnectedDeviceFragment;

/** usage in policy screen
 * Created by macbookpro13 on 05/06/2017.
 */

public class HostsAdapter extends RecyclerView.Adapter<HostsAdapter.ViewHolder> {
    private final Context context;
    private static ArrayList<ConnectedDeviceList> listCnctdDevices;
    private boolean isAllHostsSelectedd = false;

    public HostsAdapter(Context context, ArrayList<ConnectedDeviceList> listDevices) {
        this.context = context;
        listCnctdDevices = listDevices;
    }

    @NonNull
    @Override
    public HostsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.policy_connected_host_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final HostsAdapter.ViewHolder holder, int position) {
        holder.hostName.setText(listCnctdDevices.get(position).getHostName_cd());
        holder.hostIp.setText(listCnctdDevices.get(position).getIp_cd());


        if (isAllHostsSelectedd) {
//            holder.btnAcces.setImageResource(R.drawable.selected);
            holder.btnAcces.setText(context.getResources().getString(R.string.selected));
            holder.checkBox.setChecked(true);
        } else {
//            holder.btnAcces.setImageResource(R.drawable.select);
            holder.btnAcces.setText(context.getResources().getString(R.string.select));
            holder.checkBox.setChecked(false);
        }

        SelectConnectedDeviceFragment.btnSelectAll.setOnClickListener(v -> {

            if (isAllHostsSelectedd) {
                isAllHostsSelectedd = false;
                SelectConnectedDeviceFragment.btnSelectAll.setImageResource(R.drawable.select_all_white);
                notifyDataSetChanged();
            } else {
                isAllHostsSelectedd = true;
                SelectConnectedDeviceFragment.btnSelectAll.setImageResource(R.drawable.select_all);
                notifyDataSetChanged();

            }
        });

        holder.checkBox.setOnClickListener(v -> {
            if (isAllHostsSelectedd) {

                SelectConnectedDeviceFragment.btnSelectAll.setImageResource(R.drawable.select_all_white);
                isAllHostsSelectedd = false;
            }

        });

        //btn visibility gone
        holder.btnAcces.setOnClickListener(v -> {

//                String SelectAllBtn = btnselectAll.gettext
            String AccessBtn = holder.btnAcces.getText().toString();
            if (AccessBtn.equalsIgnoreCase("SELECTED")) {
                SelectConnectedDeviceFragment.btnSelectAll.setImageResource(R.drawable.select_all_white);
//                    holder.btnAcces.setImageResource(R.drawable.select);
                holder.btnAcces.setText(context.getResources().getString(R.string.select));
                holder.checkBox.setChecked(false);
                isAllHostsSelectedd = false;
                holder.checkBox.setChecked(false);
            } else {
//                    holder.btnAcces.setImageResource(R.drawable.selected);
                holder.btnAcces.setText(context.getResources().getString(R.string.selected));
                holder.checkBox.setChecked(true);
            }
//                if (isAllProtocolsSelectedd) {
//                    btnselectAll.setImageResource(R.drawable.select_all_white);
////                    holder.btnAcces.setImageResource(R.drawable.select);
//                    holder.btnAcces.setText("Select");
//                    holder.checkBox.setChecked(false);
//                    isAllProtocolsSelectedd = false;
//                } else if (issingleProtocolSelected) {
////                    holder.btnAcces.setImageResource(R.drawable.select);
//                    holder.btnAcces.setText("Select");
//                    issingleProtocolSelected = false;
//                    holder.checkBox.setChecked(false);
//                }
//
////                    else {
////                        btnselectAll.setImageResource(R.drawable.select_all_white);
////                        if (issingleProtocolSelected) {
////                            btnAcces.setImageResource(R.drawable.select);
////                            issingleProtocolSelected = false;
////                            checkBox.setChecked(false);
////                        }
//
//                else {
////                    holder.btnAcces.setImageResource(R.drawable.selected);
//                    holder.btnAcces.setText("Selected");
//                    issingleProtocolSelected = true;
//                    holder.checkBox.setChecked(true);
//                }

        });
        SelectConnectedDeviceFragment.btnDoneSelectinHosts.setOnClickListener(v -> Toast.makeText(context, "done pressed", Toast.LENGTH_SHORT).show());

    }


    @Override
    public int getItemCount() {
        return listCnctdDevices.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        final TextView hostName;
        final TextView hostIp;
        final ImageView imgProtocol;
        final Button btnAcces;
        final CheckBox checkBox;

        ViewHolder(final View itemView) {
            super(itemView);
            hostName = itemView.findViewById(R.id.tvHostNameP);
            hostIp = itemView.findViewById(R.id.tvHostIpP);
            imgProtocol = itemView.findViewById(R.id.imgProtocol);
            btnAcces = itemView.findViewById(R.id.btnAccess);
            btnAcces.setText("Select");
            checkBox = itemView.findViewById(R.id.cBox);

            itemView.setClickable(false);


        }


    }
}
