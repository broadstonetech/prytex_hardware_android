package hardware.prytex.io.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.model.ActivePortsModel;

import java.util.ArrayList;

/**
 * Created by macbookpro13 on 26/10/2017.
 */

public class ActiveHostsAdapter extends RecyclerView.Adapter<ActiveHostsAdapter.ActiveHostViewHolder> {

    private final ArrayList<ActivePortsModel> activePortsList;


    public ActiveHostsAdapter(ArrayList<ActivePortsModel> activePortsList) {
        this.activePortsList = activePortsList;
    }

    @NonNull
    @Override
    public ActiveHostsAdapter.ActiveHostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.active_port_row, parent, false);
        return new ActiveHostsAdapter.ActiveHostViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ActiveHostsAdapter.ActiveHostViewHolder holder, final int position) {

//        Log.d("activeports", String.valueOf(listCnctdDevices.get(pos).jsonArrayPorts));

        holder.portNo.setText(activePortsList.get(position).getPortNumber());
        holder.portName.setText(activePortsList.get(position).getPortName());

    }

    @Override
    public int getItemCount() {
        return activePortsList.size();
    }

    public static class ActiveHostViewHolder extends RecyclerView.ViewHolder {

        final TextView portNo;
        final TextView portName;
        final View mainView;

        ActiveHostViewHolder(View itemView) {
            super(itemView);
            mainView = itemView;
            portNo = mainView.findViewById(R.id.tvPortNumber);
            portName = mainView.findViewById(R.id.tvPortName);


        }


    }

}

