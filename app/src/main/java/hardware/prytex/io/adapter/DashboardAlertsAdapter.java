package hardware.prytex.io.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.model.MutedBlockedAlertsModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static hardware.prytex.io.adapter.ConnectedHostsAdapter.getrTimeStamp;

/**
 * Created by macbookpro13 on 06/02/2018.
 */

@SuppressWarnings("ALL")
public class DashboardAlertsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int CONNECTED_HOSTS = 1; //"cnctdhost";
    private static final int IDS = 2; //"ids";
    private static final int FLOW = 3; //"flow";
    private static final int SENSOR = 4; //"sensor";
    //    private static final int DEVICE_DOWN = 5;
//    private static final int DEVICE_RESTARTED = 6;
    private static final int INFO = 5; // "info";
    private static final int BLOCK_IPs = 6; //"blockips";
    public static int selectedAlertPos = -1;

    private final RecyclerView recyclerView;
    //    public static ArrayList<DMoatAlert> mDataSet;
    private static ArrayList<MutedBlockedAlertsModel> mDataSet;
    private final Context context;
    private final DashboardAlertsAdapter.OnAlertSelectedListener listener;

    public DashboardAlertsAdapter(Context context, RecyclerView recyclerView, ArrayList<MutedBlockedAlertsModel> list,  OnAlertSelectedListener listener) {
        this.context = context;
        mDataSet = list;
        this.recyclerView = recyclerView;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == CONNECTED_HOSTS) {
            View view = LayoutInflater.from(context).inflate(R.layout.dashboard_alert_item, recyclerView, false);
            return new DashboardAlertsAdapter.ConnectedHostsViewHolder(view);
        }
        else if (viewType == IDS) {
            View view = LayoutInflater.from(context).inflate(R.layout.dashboard_alert_item, recyclerView, false);
            return new IDSViewHolder(view);
        }
        else if (viewType == FLOW) {
            View view = LayoutInflater.from(context).inflate(R.layout.dashboard_alert_item, recyclerView, false);
            return new FlowViewHolder(view);
        } else if (viewType == SENSOR) {
            View view = LayoutInflater.from(context).inflate(R.layout.dashboard_alert_item, recyclerView, false);
            return new SensorReadingViewHolder(view);
        } else if (viewType == INFO) {
            View view = LayoutInflater.from(context).inflate(R.layout.dashboard_alert_item, recyclerView, false);
            return new InfoViewHolder(view);
        }
        else if (viewType == BLOCK_IPs) {
            View view = LayoutInflater.from(context).inflate(R.layout.dashboard_alert_item, recyclerView, false);
            return new BlockIpsViewHolder(view);
        }
        else {
            View view = LayoutInflater.from(context).inflate(R.layout.dashboard_alert_item, recyclerView, false);
            return new FlowViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        String inputSrc = mDataSet.get(position).getInput_src();
        Log.d("inputSrc=", inputSrc);

        if (holder.getItemViewType() == CONNECTED_HOSTS) {
            DashboardAlertsAdapter.ConnectedHostsViewHolder vh = (DashboardAlertsAdapter.ConnectedHostsViewHolder) holder;
            MutedBlockedAlertsModel alert = mDataSet.get(position);
            vh.bindData(alert);
        } else if (holder.getItemViewType() == FLOW) {
            DashboardAlertsAdapter.FlowViewHolder vh = (DashboardAlertsAdapter.FlowViewHolder) holder;
            MutedBlockedAlertsModel alert = mDataSet.get(position);
            vh.bindData(alert);
        } else if (holder.getItemViewType() == IDS) {
            DashboardAlertsAdapter.IDSViewHolder vh = (DashboardAlertsAdapter.IDSViewHolder) holder;
            MutedBlockedAlertsModel alert = mDataSet.get(position);
            vh.bindData(alert);
        } else if (holder.getItemViewType() == SENSOR) {
            DashboardAlertsAdapter.SensorReadingViewHolder vh = (DashboardAlertsAdapter.SensorReadingViewHolder) holder;
            MutedBlockedAlertsModel alert = mDataSet.get(position);
            vh.bindData(alert);
        } else if (holder.getItemViewType() == INFO) {
            DashboardAlertsAdapter.InfoViewHolder vh = (DashboardAlertsAdapter.InfoViewHolder) holder;
            MutedBlockedAlertsModel alert = mDataSet.get(position);
            vh.bindData(alert);
        } else if (holder.getItemViewType() == BLOCK_IPs) {
            DashboardAlertsAdapter.BlockIpsViewHolder vh = (DashboardAlertsAdapter.BlockIpsViewHolder) holder;
            MutedBlockedAlertsModel alert = mDataSet.get(position);
            vh.bindData(alert);
        }
        else {
//            BlockIpsViewHolder vh = (BlockIpsViewHolder) holder;
//            MutedBlockedAlertsModel alert = mDataSet.get(position);
//            vh.bindData(alert);
        }
    }

    @Override
    public int getItemCount() {
        Log.d("datasize==", String.valueOf(mDataSet.size()));
        return mDataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        String alert = mDataSet.get(position).getInput_src();
        if (alert != null) {
            if (alert.equalsIgnoreCase("cnctdhost")) {
                return CONNECTED_HOSTS;
            } else if (alert.equalsIgnoreCase("flow")) {
                return FLOW;
            } else if (alert.equalsIgnoreCase("sensor")) {
                return SENSOR;
            } else if (alert.equalsIgnoreCase("info")) {
                return INFO;
            } else if (alert.equalsIgnoreCase("ids")) {
                return IDS;

            } else if (alert.equalsIgnoreCase("blockips")) {
                return BLOCK_IPs;
//            }
            } else {
                return BLOCK_IPs;
//                Log.d("Alert", "alert is null");
            }
        }
        return super.getItemViewType(position);

    }

    class ConnectedHostsViewHolder extends RecyclerView.ViewHolder {

        final View view;
        final View visitedLine;
        final View actionsContainer;
        final TextView desc;
        public TextView hostName;
        public TextView os;
        public TextView mac;
        public TextView ip;
        final TextView eveSec;

        ConnectedHostsViewHolder(final View itemView) {
            super(itemView);
            view = itemView;

            desc = view.findViewById(R.id.txt_description);
            eveSec = view.findViewById(R.id.eve_sec_timestamp);
            eveSec.setVisibility(View.GONE);
//            hostName = (TextView) view.findViewById(R.id.txt_hostname);
//            os = (TextView) view.findViewById(R.id.txt_os);
//            mac = (TextView) view.findViewById(R.id.txt_mac);
//            ip = (TextView) view.findViewById(R.id.txt_ip);
//            eveSec = (TextView) view.findViewById(R.id.eve_sec_timestamp);
            visitedLine = view.findViewById(R.id.visited_line);
            actionsContainer = view.findViewById(R.id.actions_container);
//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    int alertPos = getAdapterPosition();
//                    MutedBlockedAlertsModel alert = mDataSet.get(getAdapterPosition());
//                    listener.onAlertSelected(alert);
//                    selectedAlertPos = alertPos;
//                }
//            });

            itemView.setOnClickListener(v -> {

//                    showData(getAdapterPosition());
                listener.onAlertSelected();

            });
        }

        void bindData(MutedBlockedAlertsModel alert) {

            int time = alert.getEve_sec();
            String timeStmp = "00:00:00";
            try {
                timeStmp = getrTimeStamp(time);
            } catch (Exception e) {
                e.printStackTrace();
            }

            desc.setText(alert.getDescription());
//            eveSec.setText(timeStmp);
//            if (alert.getDeviceCategory().equalsIgnoreCase("Router")) {
//                hostName.setText(alert.getDeviceCategory());
//            } else {
//                hostName.setText(alert.getHostName());
//            }
//            os.setText(alert.getOS());
////            mac.setVisibility(View.GONE);
//            mac.setText(alert.getDeviceCategory());
//            ip.setText(alert.getIp());
//
//            eveSec.setVisibility(View.GONE);
//            hostName.setVisibility(View.GONE);
//            os.setVisibility(View.GONE);
//            mac.setVisibility(View.GONE);
//            ip.setVisibility(View.GONE);
//            visitedLine.setVisibility(View.GONE);

//            actionsContainer.setVisibility(View.GONE);

        }
    }

    static class FlowViewHolder extends RecyclerView.ViewHolder {

        final View view;
        View visitedLine;
        View actionsContainer;
        final TextView desc;
        public TextView detail;
        public TextView detail_label;
        final TextView eveSec;
        LinearLayout llClientDetails;

        FlowViewHolder(View itemView) {
            super(itemView);
            view = itemView;
//            eveSec = (TextView) view.findViewById(R.id.eve_sec_timestamp);
            desc = view.findViewById(R.id.txt_description);
            eveSec = view.findViewById(R.id.eve_sec_timestamp);
            eveSec.setVisibility(View.GONE);
//            detail = (TextView) view.findViewById(R.id.txt_detail);
//            detail_label = (TextView) view.findViewById(R.id.txt_detail_label);
//            visitedLine = view.findViewById(R.id.visited_line);
//            llClientDetails = (LinearLayout) view.findViewById(R.id.llClientInfoDetail);
//            actionsContainer = view.findViewById(R.id.actions_container);

//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    selectedAlertPos = getAdapterPosition();
//                    MutedBlockedAlertsModel alert = mDataSet.get(getAdapterPosition());
//                    listener.onAlertSelected(alert);
//                }
//            });
        }

        void bindData(MutedBlockedAlertsModel alert) {

            int time = alert.getEve_sec();
            String timeStmp = "00:00:00";
            try {
                timeStmp = getrTimeStamp(time);
            } catch (Exception e) {
                e.printStackTrace();
            }
//            eveSec.setText(timeStmp);
//            if (alert.getflowDetail().equalsIgnoreCase("")) {
//                llClientDetails.setVisibility(View.GONE);
//            } else if (alert.getFlowType().equalsIgnoreCase("malicious") || alert.getFlowType().equalsIgnoreCase("unknown")) {
//                detail.setVisibility(View.VISIBLE);
//                detail.setText(alert.getflowDetail());
//            } else {
//                llClientDetails.setVisibility(View.GONE);
//            }
//            if (alert.getFlowType().equalsIgnoreCase("normal") || alert.getFlowType().equalsIgnoreCase("limit_exceed") | alert.getFlowType().equalsIgnoreCase("metadata")) {
//                String day = "";
//                String hour = "";
//                String format = "";
//                int nexthour = 0;
//                try {
//                    day = getHourDayStamp(Integer.parseInt(String.valueOf(alert.getEve_sec())), false, true, false);
//                    hour = getHourDayStamp(Integer.parseInt(String.valueOf(alert.getEve_sec())), true, false, false);
//                    format = getHourDayStamp(Integer.parseInt(String.valueOf(alert.getEve_sec())), false, false, true);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                nexthour = Integer.parseInt(hour) + 1;
//                if (format.contains("AM")) {
//                    format = "am";
//                } else if (format.contains("PM")) {
//                    format = "pm";
//                } else {
//                    format = "";
//                }
//                Log.d("client info forA", alert.getDescription() + "=||A||=" + alert.getflowDetail());
//                desc.setText(alert.getDescription() + " on " + day + " between " + hour + format + "-" + nexthour + format + ".");
////                llClientDetails.setVisibility(View.GONE);
//            } else {
////                detail.setVisibility(View.VISIBLE);
//                Log.d("client info for", alert.getDescription() + "=||||=" + alert.getDescription());
                desc.setText(alert.getDescription());
//            }
//
//
//            eveSec.setVisibility(View.GONE);
//            detail.setVisibility(View.GONE);
//            detail_label.setVisibility(View.GONE);
//            llClientDetails.setVisibility(View.GONE);
//            actionsContainer.setVisibility(View.GONE);
//            visitedLine.setVisibility(View.GONE);

        }
    }

    static class IDSViewHolder extends RecyclerView.ViewHolder {

        final View view;
        View visitedLine;
        View actionsContainer;
        final TextView desc;
        public TextView msg;
        public TextView srcIP;
        public TextView destIP;
        public TextView srcPort;
        public TextView destPort;
        final TextView eveSec;

        IDSViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            desc = view.findViewById(R.id.txt_description);
            eveSec = view.findViewById(R.id.eve_sec_timestamp);
            eveSec.setVisibility(View.GONE);
//            msg = (TextView) view.findViewById(R.id.txt_message);
//            srcIP = (TextView) view.findViewById(R.id.txt_src_ip);
//            destIP = (TextView) view.findViewById(R.id.txt_dest_ip);
//            srcPort = (TextView) view.findViewById(R.id.src_port);
//            destPort = (TextView) view.findViewById(R.id.dest_port);
//            eveSec = (TextView) view.findViewById(R.id.eve_sec_timestamp);
//            visitedLine = view.findViewById(R.id.visited_line);
//            actionsContainer = view.findViewById(R.id.actions_container);

//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    selectedAlertPos = getAdapterPosition();
//                    MutedBlockedAlertsModel alert = mDataSet.get(getAdapterPosition());
//                    listener.onAlertSelected(alert);
//                }
//            });
        }

        void bindData(MutedBlockedAlertsModel alert) {

            int time = alert.getEve_sec();
            String timeStmp = "00:00:00";
            try {
                timeStmp = getrTimeStamp(time);
            } catch (Exception e) {
                e.printStackTrace();
            }
//            eveSec.setText(timeStmp);

            desc.setText(alert.getDescription());
//            msg.setText(alert.msg);
//            srcIP.setText(alert.hostname + "" + alert.srcIp);
//            srcIP.setText(alert.getSrc_ip());
//            destIP.setText(alert.getDesc_ip());
//            srcPort.setText(alert.getSrc_port());
//            destPort.setText(alert.getDest_port());
//
//            visitedLine.setVisibility(View.GONE);
//            actionsContainer.setVisibility(View.GONE);
//
//            msg.setVisibility(View.GONE);
//            srcIP.setVisibility(View.GONE);
//            destIP.setVisibility(View.GONE);
//            srcPort.setVisibility(View.GONE);
//            destPort.setVisibility(View.GONE);
//            eveSec.setVisibility(View.GONE);
        }
    }


    static class SensorReadingViewHolder extends RecyclerView.ViewHolder {

        final View view;
        View visitedLine;
        final TextView desc;
        public TextView reading;
        public TextView category;
        final TextView eveSec;
        public TextView sensorValuerange;

        SensorReadingViewHolder(final View itemView) {
            super(itemView);
            view = itemView;
            desc = view.findViewById(R.id.txt_description);
            eveSec = view.findViewById(R.id.eve_sec_timestamp);
            eveSec.setVisibility(View.GONE);
//            reading = (TextView) view.findViewById(R.id.txt_reading);
//            sensorValuerange = (TextView) view.findViewById(R.id.tvSensorValueRange);
//            visitedLine = view.findViewById(R.id.visited_line);

//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    selectedAlertPos = getAdapterPosition();
//                    MutedBlockedAlertsModel alert = mDataSet.get(getAdapterPosition());
//                    listener.onAlertSelected(alert);
//                }
//            });
        }

        void bindData(MutedBlockedAlertsModel alert) {

            int time = alert.getEve_sec();
            String timeStmp = "00:00:00";
            try {
                timeStmp = getrTimeStamp(time);
            } catch (Exception e) {
                e.printStackTrace();
            }
//            eveSec.setText(timeStmp);
            desc.setText(alert.getDescription());
//            String reading = "";
//            String sensorRange = "";
//            if ((alert.getCategory().equals(DMoatAlert.TEMPERATURE))) {
//                sensorRange = "Normal temperature is (5-35)°C.";
//                SharedPreferences prefs = context.getSharedPreferences(Constants.APP_ID_PREFS_NAME, Context.MODE_PRIVATE);
//                int selectedScale = prefs.getInt(ProfileFragment.TEMPERATURE_SCALE, 0);
//                if (selectedScale == 1) {
//                    float r = -1;
//                    try {
//                        r = Float.parseFloat(String.valueOf(alert.getSensorReading()));
//                        r = GeneralUtils.celciusToFahrenheit(r);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    if (r == -1) {
//                        reading = String.valueOf(alert.getSensorReading()) + "°F";
//                    } else {
//                        reading = r + "°F";
//                    }
//                } else {
//                    reading = String.valueOf(alert.getSensorReading()) + "°C";
//                }
//
//            } else if (alert.getCategory().equals(DMoatAlert.AIR_QUALITY)) {
//                reading = String.valueOf(alert.getSensorReading()) + " CO2PPM";
//                sensorRange = "Air Quality is good between (0-450)CO2PPM.";
//            } else {
//                reading = String.valueOf(alert.getSensorReading()) + "%";
//                sensorRange = "Humidity is good below 80%.";
//            }
//            this.reading.setText(reading);
//            sensorValuerange.setText(sensorRange);
//
//
//            sensorValuerange.setVisibility(View.GONE);
//            eveSec.setVisibility(View.GONE);
//            visitedLine.setVisibility(View.GONE);
        }
    }

    static class InfoViewHolder extends RecyclerView.ViewHolder {

        final TextView timestamp;
        final TextView txtDescription;
        final View view;
        View visitedLine;

        InfoViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            txtDescription = view.findViewById(R.id.txt_description);
            timestamp = view.findViewById(R.id.eve_sec_timestamp);
            timestamp.setVisibility(View.GONE);
//            timestamp = (TextView) view.findViewById(R.id.eve_sec_timestamp);
//            visitedLine = view.findViewById(R.id.visited_line);

//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    selectedAlertPos = getAdapterPosition();
//                    MutedBlockedAlertsModel alert = mDataSet.get(getAdapterPosition());
//                    listener.onAlertSelected(alert);
//                }
//            });
        }

        void bindData(MutedBlockedAlertsModel alert) {

            int time = alert.getEve_sec();
            String timeStmp = "00:00:00";
            try {
                timeStmp = getrTimeStamp(time);
            } catch (Exception e) {
                e.printStackTrace();
            }
//            timestamp.setText(timeStmp);

//            if (alert.getInfo_type().equalsIgnoreCase("internet_restarted")) {
//                String timeOn = "dd-mm-yyy hh:mm a";
//                String timeOff = "dd-mm-yyy hh:mm a";
//                timeOn = alert.getFormattedTimeStamp(alert.getTimeOn());
//                timeOff = alert.getFormattedTimeStamp(alert.getTimeOff());
//
//                txtDescription.setText("Internet access was unavailable from " + timeOff + " to " + timeOn);
//            } else {
                txtDescription.setText(alert.getDescription());
//            }
//
//            timestamp.setVisibility(View.GONE);
//            visitedLine.setVisibility(View.GONE);
        }
    }


    static class BlockIpsViewHolder extends RecyclerView.ViewHolder {

        final View view;
        View visitedLine;
        View actionsContainer;
        final TextView desc;
        public TextView detail;
        public TextView detail_label;
        final TextView eveSec;

        BlockIpsViewHolder(final View itemView) {
            super(itemView);
            view = itemView;
//            eveSec = (TextView) view.findViewById(R.id.eve_sec_timestamp);
            desc = view.findViewById(R.id.txt_description);
            eveSec = view.findViewById(R.id.eve_sec_timestamp);
            eveSec.setVisibility(View.GONE);
//            detail = (TextView) view.findViewById(R.id.txt_detail);
//            detail_label = (TextView) view.findViewById(R.id.txt_detail_label);
//            visitedLine = view.findViewById(R.id.visited_line);
//            actionsContainer = view.findViewById(R.id.actions_container);

//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    selectedAlertPos = getAdapterPosition();
//                    MutedBlockedAlertsModel alert = mDataSet.get(getAdapterPosition());
//                    listener.onAlertSelected(alert);
//                }
//            });
        }

        void bindData(MutedBlockedAlertsModel alert) {
            int time = alert.getEve_sec();
            String timeStmp = "00:00:00";
            try {
                timeStmp = getrTimeStamp(time);
            } catch (Exception e) {
                e.printStackTrace();
            }
//            eveSec.setText(timeStmp);
            desc.setText(alert.getDescription());
//            detail_label.setVisibility(View.GONE);
//            detail.setText(alert.getIp());
//            detail.setVisibility(View.GONE);
////            eveSec.setText(alert.getFormattedEveSecTimeStamp());
//            desc.setVisibility(View.GONE);
//            detail.setVisibility(View.GONE);
//            detail_label.setVisibility(View.GONE);
//            eveSec.setVisibility(View.GONE);
//            visitedLine.setVisibility(View.GONE);
        }
    }

    public void clear() {
        mDataSet.clear();
        notifyDataSetChanged();
    }

    public void remove(MutedBlockedAlertsModel alert) {
        Log.d("AlertToRemove", alert.toString());
        if (alert != null) {
            mDataSet.remove(alert);
            this.notifyDataSetChanged();
        } else {
//            this.notifyDataSetChanged();
        }
    }

    public interface OnAlertSelectedListener {
        void onAlertSelected();
    }


    //get hour and day from eve_sec
    public static String getHourDayStamp(int s, boolean hour, boolean day, boolean format) throws Exception {
        float fTime = Float.parseFloat(String.valueOf(s));
        long time = (long) fTime;
        time = time * 1000;
        Date date = new Date(time);
        SimpleDateFormat sourceFormat;
        if (hour) {
            sourceFormat = new SimpleDateFormat(" h", Locale.getDefault());
        } else if (day) {
            sourceFormat = new SimpleDateFormat("EEEE", Locale.getDefault());
        } else {
            sourceFormat = new SimpleDateFormat("h:mm a", Locale.getDefault());
        }
//        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateString = sourceFormat.format(date); // => Date is in UTC now
        Date parsed = sourceFormat.parse(dateString); // => Date is in UTC now

        SimpleDateFormat destFormat;
        if (hour) {
            destFormat = new SimpleDateFormat("h", Locale.getDefault());
        } else if (day) {
            destFormat = new SimpleDateFormat("EEEE", Locale.getDefault());
        } else {
            destFormat = new SimpleDateFormat("h:mm a", Locale.getDefault());
        }
//        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        destFormat.setTimeZone(Calendar.getInstance().getTimeZone());
//        destFormat.setTimeZone(TimeZone.getDefault());
        return destFormat.format(parsed);
    }

}
