package hardware.prytex.io.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.model.BandWidth;

import java.util.ArrayList;

/**
 * Created by macbookpro13 on 09/02/2018.
 */

public class DashboardBandwidthAdapter extends RecyclerView.Adapter<DashboardBandwidthAdapter.ViewHolder> {

    private final ArrayList<BandWidth> HostsList;
    private final Context context;

    private final DashboardBandwidthAdapter.OnAlertSelectedListener listener;

    public DashboardBandwidthAdapter(Context context, ArrayList<BandWidth> HostsList, OnAlertSelectedListener listener) {
        super();
        this.context = context;
        this.HostsList = HostsList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DashboardBandwidthAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.dashboard_alert_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DashboardBandwidthAdapter.ViewHolder viewHolder, int i) {

        viewHolder.tvhostName.setText(HostsList.get(i).getHostName());
        if ( i == 0){
            viewHolder.viewLegendClr.setBackgroundColor(context.getResources().getColor(R.color.BarColorA));
        }
        else if ( i == 1){
            viewHolder.viewLegendClr.setBackgroundColor(context.getResources().getColor(R.color.BarColorB));
        }
        else if ( i == 2){
            viewHolder.viewLegendClr.setBackgroundColor(context.getResources().getColor(R.color.BarColorC));
        }
        else if ( i == 3){
            viewHolder.viewLegendClr.setBackgroundColor(context.getResources().getColor(R.color.BarColorD));
        }
        else if ( i == 4){
            viewHolder.viewLegendClr.setBackgroundColor(context.getResources().getColor(R.color.BarColorE));
        }
        viewHolder.view.setOnClickListener(v -> listener.onAlertSelected());

    }

    @Override
    public int getItemCount() {
        return HostsList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView tvhostName;
        final View viewLegendClr;
        final CardView view;

        ViewHolder(final View itemView) {
            super(itemView);
            tvhostName = itemView.findViewById(R.id.txt_description);
            viewLegendClr = itemView.findViewById(R.id.viewLegendBar);
            view = itemView.findViewById(R.id.card_view);

        }

    }

    public interface OnAlertSelectedListener {
        void onAlertSelected();
    }
}
