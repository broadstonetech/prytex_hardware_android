package hardware.prytex.io.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.model.ProtocolsListModal;

import java.util.ArrayList;

/**
 * Created by macbookpro13 on 12/05/2017.
 */

public class BandwidthHostDetailAdapter extends RecyclerView.Adapter<BandwidthHostDetailAdapter.ViewHolder> {

    private final ArrayList<ProtocolsListModal> HostProtolsList;
    private final Context context;

    public BandwidthHostDetailAdapter(Context context, ArrayList<ProtocolsListModal> alName, ArrayList<Integer> alImage) {
        super();
        this.context = context;
        this.HostProtolsList = alName;
    }



    @NonNull
    @Override
    public BandwidthHostDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.protocolo_host_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BandwidthHostDetailAdapter.ViewHolder viewHolder, int i) {

        String protocolName = HostProtolsList.get(i).getProtocolName();
        int checkExistence = context.getResources().getIdentifier(protocolName, "drawable", context.getPackageName());

        if (checkExistence != 0) {  // the resouce exists...
            Drawable drawable = context.getResources().getDrawable(checkExistence);
            viewHolder.imgProtocol.setImageDrawable(drawable);
        } else {  // checkExistence == 0  // the resouce does NOT exist!!
            viewHolder.imgProtocol.setImageResource(R.drawable.protocol);
        }

        String[] androidStrings = context.getResources().getStringArray(R.array.protocols_keys);
        for (String s : androidStrings) {
            int j = s.indexOf(protocolName);
            if (j >= 0) {
                viewHolder.protocolDetail.setText(context.getResources().getString(R.string.no_description_found));
                break;
            } else { //else case eill be rare. hashmap contains noDesc msg
                viewHolder.protocolDetail.setText(context.getResources().getString(R.string.no_description_found));
            }
        }

        viewHolder.protocolNme.setText(protocolName);
        String hostUsg = String.format("%.02f", HostProtolsList.get(i).getProtocolUsage());
        viewHolder.protocolUsage.setText(hostUsg + context.getResources().getString(R.string.MB));

    }

    @Override
    public int getItemCount() {
        return HostProtolsList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        final ImageView imgProtocol;
        final TextView protocolNme;
        final TextView protocolUsage;
        final TextView protocolDetail;

        ViewHolder(final View itemView) {
            super(itemView);
            imgProtocol = itemView.findViewById(R.id.imgProtocol);
            protocolNme = itemView.findViewById(R.id.tvProtocolName);
            protocolUsage = itemView.findViewById(R.id.tvProtocolUsage);
            protocolDetail = itemView.findViewById(R.id.tvProtocolDetail);

            itemView.setOnClickListener(v -> {
            });

        }
    }
}