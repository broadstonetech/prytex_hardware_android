package hardware.prytex.io.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.model.ProtocolsListModal;
import hardware.prytex.io.util.MyToast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import hardware.prytex.io.fragment.Policy.ProtocolsPolicy.ProtocolPolicyControlFragment;

@SuppressWarnings("ALL")
public class ProtocolPolicyAdapter extends RecyclerView.Adapter<ProtocolPolicyAdapter.ViewHolder> {

    private final Context context;
    private int listSize = 0;
    private final ArrayList<ProtocolsListModal> HostProtolsList;
    private final HashMap<String, String> protocolsDict;

    public ProtocolPolicyAdapter(Context context, ArrayList<ProtocolsListModal> HostsList, int listCount, HashMap<String, String> pValues, ProtocolPolicyAdapter.OnAlertSelectedListener listener) {
        super();
        this.context = context;
        this.HostProtolsList = HostsList;
        this.listSize = listCount;
        this.protocolsDict = pValues;
    }

    @NonNull
    @Override
    public ProtocolPolicyAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.policy_host_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ProtocolPolicyAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.tvHostName.setText(HostProtolsList.get(i).getProtocolName());


        if (HostProtolsList.get(i).isInPolicy()) {
            viewHolder.imgCheckMark.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imgCheckMark.setVisibility(View.GONE);
        }
        String protocolName = HostProtolsList.get(i).getProtocolName();

        if (protocolsDict.containsValue(protocolName)) {
            String protocolKey = getKeyForValueFromMap(protocolName);
            int checkExistence = context.getResources().getIdentifier(protocolKey, "drawable", context.getPackageName());
            viewHolder.imgProtocolIcon.setVisibility(View.VISIBLE);
            if (checkExistence != 0) {  // the resouce exists...
                Drawable drawable = context.getResources().getDrawable(checkExistence);
                viewHolder.imgProtocolIcon.setImageDrawable(drawable);
            } else {  // checkExistence == 0  // the resouce does NOT exist!!
                viewHolder.imgProtocolIcon.setImageResource(R.drawable.protocol);
            }
        }else{
            viewHolder.imgProtocolIcon.setImageResource(R.drawable.protocol);
        }


        viewHolder.cardView.setOnClickListener(v -> {
            if (HostProtolsList.get(i).isInPolicy()) {
                HostProtolsList.get(i).setInPolicy(false);
                for (int j = 0; j < ProtocolPolicyControlFragment.listOfSelectedProtocolsForBlocking.size(); j++) {
                    if (ProtocolPolicyControlFragment.listOfSelectedProtocolsForBlocking.get(j).equalsIgnoreCase(HostProtolsList.get(i).getProtocolName())) {
                        ProtocolPolicyControlFragment.listOfSelectedProtocolsForBlocking.remove(j);
                    }
                }
            } else {
                if (ProtocolPolicyControlFragment.listOfSelectedProtocolsForBlocking.size() < 5) {
                    HostProtolsList.get(i).setInPolicy(true);
                    ProtocolPolicyControlFragment.listOfSelectedProtocolsForBlocking.add(HostProtolsList.get(i).getProtocolName());
                } else {
                    MyToast.showMessage(context, "You can select only 5 protocols.");
                }
            }
            notifyItemRangeChanged(i, HostProtolsList.size());

        });

    }

    @Override
    public int getItemCount() {
        return listSize;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView tvHostName;
        final ImageView imgCheckMark;
        final CardView cardView;
        final LinearLayout llInfo;
        final ImageView imgProtocolIcon;

        ViewHolder(final View itemView) {
            super(itemView);
            tvHostName = itemView.findViewById(R.id.tvHostName);
            imgCheckMark = itemView.findViewById(R.id.imgCheckMark);
            cardView = itemView.findViewById(R.id.card_view);
            llInfo = itemView.findViewById(R.id.llClientInfoDetail);
            imgProtocolIcon = itemView.findViewById(R.id.imgProtocolIcon);

            llInfo.setVisibility(View.GONE);
        }

    }

    public interface OnAlertSelectedListener {
    }

    private String getKeyForValueFromMap(String paramName) {
        String keyForValue = null;
        if(paramName!=null) {
            Set<Map.Entry<String,String>> entrySet = protocolsDict.entrySet();
            if(entrySet!=null && entrySet.size()>0) {
                for(Map.Entry<String,String> entry : entrySet) {
                    if(entry!=null && paramName.equalsIgnoreCase(entry.getValue())) {
                        keyForValue = entry.getKey();
                    }
                }
            }
        }
        return keyForValue;
    }

}
