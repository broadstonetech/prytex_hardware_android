package hardware.prytex.io.adapter;

import static hardware.prytex.io.util.GeneralUtils.getBandwidthData;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.fragment.BandwidthFrags.BandwidthHostDetailsFragment;
import hardware.prytex.io.fragment.BandwidthFrags.BandwidthChartFrags.ProtocolChartFragment;
import hardware.prytex.io.model.BandWidth;

import java.util.ArrayList;


public class BandwidthAdapter extends RecyclerView.Adapter<BandwidthAdapter.ViewHolder> implements Filterable {

    private ArrayList<BandWidth> HostsList;
    private final ArrayList<BandWidth> searchAbledataList;
    private final Context context;
    public static int posClicked = 0;
    private final OnAlertSelectedListener listenerBandwidth;

    public BandwidthAdapter(Context context, ArrayList<BandWidth> alName, ArrayList<Integer> alImage, OnAlertSelectedListener listener) {
        super();
        this.context = context;
        this.HostsList = alName;
        this.searchAbledataList = alName;
        this.listenerBandwidth = listener;
    }

    public BandwidthAdapter(Context context, ArrayList<BandWidth> HostsList, OnAlertSelectedListener listener) {
        super();
        this.context = context;
        this.HostsList = HostsList;
        this.searchAbledataList = HostsList;
        this.listenerBandwidth = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.protocol_detail_new, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.imgThumbnail.setText(HostsList.get(i).getHostName());
        String stringToSearch = HostsList.get(i).getHostOS();
        viewHolder.tvhostOS.setText(HostsList.get(i).getHostOS());
        viewHolder.tvhostOS.setVisibility(View.GONE);
        viewHolder.tvHostUsage.setText(HostsList.get(i).getTotalUsage() +" "+"MB");

        if (stringToSearch.contains("android") || stringToSearch.contains("Android") || stringToSearch.contains("Android OS") || stringToSearch.contains("watchOS")) {
            viewHolder.imgOS.setImageResource(R.drawable.android_logo);
        } else if ((stringToSearch.contains("apple")) || stringToSearch.contains("Apple") || stringToSearch.contains("iOS") || stringToSearch.contains("macOS") || stringToSearch.contains("watch OS") || stringToSearch.contains("mac") || stringToSearch.contains("Mac")) {
            viewHolder.imgOS.setImageResource(R.drawable.apple);
        } else if ((stringToSearch.contains("windows")) || stringToSearch.contains("Windows") || stringToSearch.contains("Windows OS")) {
            viewHolder.imgOS.setImageResource(R.drawable.windows);
        } else if ((stringToSearch.contains("linux")) || stringToSearch.contains("Linux") || stringToSearch.contains("Linux/Debian Linux/Ubuntu")) {
            viewHolder.imgOS.setImageResource(R.drawable.linux);
        } else {
            viewHolder.imgOS.setImageResource(R.drawable.laptop_logo);
        }

    }

    @Override
    public int getItemCount() {
        return HostsList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                HostsList = (ArrayList<BandWidth>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                ArrayList<BandWidth> filteredResults = null;
                String charString = constraint.toString();
                if (charString.isEmpty()) {
                    filteredResults = searchAbledataList;
                } else {
                    filteredResults = getBandwidthData(constraint.toString().toLowerCase(),HostsList);
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        final TextView imgThumbnail;
        final TextView tvhostOS;
        final ImageView imgOS;
        final TextView tvHostUsage;

        ViewHolder(final View itemView) {
            super(itemView);
            imgThumbnail = itemView.findViewById(R.id.tbHostProtocolName);
            tvhostOS = itemView.findViewById(R.id.host_os);
            imgOS = itemView.findViewById(R.id.imgOsLogo);
            tvHostUsage = itemView.findViewById(R.id.tvHostUsage);

            itemView.setOnClickListener(v -> {
                if (HostsList.get(getAdapterPosition()).isCurrent()){
                    BandwidthHostDetailsFragment.isCurrentBandwidthDatList = true;
                    BandwidthHostDetailsFragment.isAllBandwidthDataList = false;
                }else{
                    BandwidthHostDetailsFragment.isAllBandwidthDataList = true;
                    BandwidthHostDetailsFragment.isCurrentBandwidthDatList = false;
                }
//                    showData(getAdapterPosition());
                BandWidth model = HostsList.get(getAdapterPosition());
                listenerBandwidth.onAlertSelected(model, getAdapterPosition());

            });

        }

    }

    public void showData(int pos) {

        posClicked = pos;
        AppCompatActivity activity = (AppCompatActivity) context;

        BandwidthHostDetailsFragment myFragment = new BandwidthHostDetailsFragment();
        //Create a bundle to pass data, add data, set the bundle to your fragment and:
        activity.getSupportFragmentManager().beginTransaction().remove(new ProtocolChartFragment()).replace(R.id.fragment_container, myFragment).addToBackStack(null).commit();

    }

    public interface OnAlertSelectedListener {
        void onAlertSelected(BandWidth alert, int position);
    }
}