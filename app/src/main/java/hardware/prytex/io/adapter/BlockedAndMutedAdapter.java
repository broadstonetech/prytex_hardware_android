package hardware.prytex.io.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.model.AlertAction;
import hardware.prytex.io.model.DMoatAlert;
import hardware.prytex.io.model.MutedBlockedAlertsModel;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.DmoatUtils;
import hardware.prytex.io.util.MyToast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import hardware.prytex.io.activity.DashBoardActivity;

import static hardware.prytex.io.adapter.ConnectedHostsAdapter.getrTimeStamp;
import static hardware.prytex.io.adapter.ViewAlertsAdapter.getHourDayStamp;
import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

/**
 * Created by abubaker on 10/24/16.
 */

public class BlockedAndMutedAdapter extends RecyclerView.Adapter<BlockedAndMutedAdapter.MyViewHolder> {

    private static ArrayList<MutedBlockedAlertsModel> listAlerts;
    private final ArrayList<DMoatAlert> mDataSet;
    private int removePos = 0;
    private final Context mContext;
    private final String appID;


    private final String MutedOrBlocked;

    public BlockedAndMutedAdapter(Context context, ArrayList<MutedBlockedAlertsModel> listAlrts, ArrayList<DMoatAlert> list, String mutedOrBlocked, String appID) {
        this.mContext = context;
        listAlerts = listAlrts;
        this.mDataSet = list;
        this.MutedOrBlocked = mutedOrBlocked;
        this.appID = appID;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.blocked_muted_alert_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        String inputSrc = listAlerts.get(position).getInput_src();

        holder.llMutedBlockedView.setVisibility(View.GONE);
        int time = listAlerts.get(position).getEve_sec();
        String timeStmp = "00:00:00";
        try {
            timeStmp = getrTimeStamp(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
        switch (inputSrc) {

            case "cnctdhost":
                //set height of main layout to 100dp for flow
                RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) holder.llMain.getLayoutParams();
//                lp.height = 400;
                lp.height = RecyclerView.LayoutParams.WRAP_CONTENT;

                holder.llMutedBlockedView.setVisibility(View.GONE);
                holder.llIdsAlertView.setVisibility(View.GONE);
                holder.llFlowAlertView.setVisibility(View.GONE);
                holder.llHostNameCnctdHost.setVisibility(View.GONE);
                holder.llIpCnctdHost.setVisibility(View.GONE);
                holder.llOsCnctdHost.setVisibility(View.GONE);
                holder.llConnectedHostView.setVisibility(View.VISIBLE);
                holder.description_cnctdHost.setText(listAlerts.get(position).getDescription());
                holder.hostName_cnctdHost.setText(listAlerts.get(position).getHostName());
                holder.ip_cnctdHost.setText(listAlerts.get(position).getIp());
                holder.macAddress_cnctdHost.setText(listAlerts.get(position).getMacaddress());
                holder.os_cnctdHost.setText(listAlerts.get(position).getOS());
                holder.timestamp_cnctdHost.setText(timeStmp);

                break;
            case "ids":
                //set height of main layout to 100dp for flow
                lp = (RecyclerView.LayoutParams) holder.llMain.getLayoutParams();
//                lp.height = 450;
                lp.height = RecyclerView.LayoutParams.WRAP_CONTENT;
//hide all view ezxcept ids
                holder.llMutedBlockedView.setVisibility(View.GONE);
                holder.llConnectedHostView.setVisibility(View.GONE);
                holder.llFlowAlertView.setVisibility(View.GONE);

                holder.llIdsAlertView.setVisibility(View.VISIBLE);
                holder.description_ids.setText(listAlerts.get(position).getDescription());
                holder.srcIp_ids.setText(listAlerts.get(position).getSrcHostName()); //+"-"+listAlerts.get(position).getSrc_ip());
                holder.srcPort_ids.setText(listAlerts.get(position).getSrc_port());
                holder.destIp_ids.setText(listAlerts.get(position).getDesc_ip());
                holder.portDest_ids.setText(listAlerts.get(position).getDest_port());
                holder.timestamp_ids.setText(timeStmp);

                break;
            case "flow":

                //set height of main layout to 100dp for flow
                lp = (RecyclerView.LayoutParams) holder.llMain.getLayoutParams();
//                lp.height = 250;
                lp.height = RecyclerView.LayoutParams.WRAP_CONTENT;
//hide all view ezxcept flow
                holder.llMutedBlockedView.setVisibility(View.GONE);
                holder.llConnectedHostView.setVisibility(View.GONE);
                holder.llIdsAlertView.setVisibility(View.GONE);

                holder.llFlowAlertView.setVisibility(View.GONE);
                //hide all view except flow and settext

                StringBuilder descriptionList = new StringBuilder();
                JSONObject jObj;
                int total = listAlerts.get(position).flowAlertJsonArr.length();
                for (int i = 0; i < total; i++) {

                    try {
                        jObj = listAlerts.get(position).flowAlertJsonArr.getJSONObject(i);

                            descriptionList.append(jObj.getString("description")).append(".\n");


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                int slotId = listAlerts.get(position).getSlotId();
                int hour = slotId % 24;
                int day = slotId / 24;
                Log.d("slotIDday-hour== ", "onBindViewHolder: ");
                String dayTitle = DmoatUtils.getDayFromSlot(day);
                String hourTitle = DmoatUtils.gethourFromSlot(hour);

                String startingHour = "00:00:00";
                String dy = "00:00:00";
                String format = "00:00:00";
                try {
                    startingHour = getrTimeStamp(listAlerts.get(position).getEve_sec());
                    startingHour = getHourDayStamp(listAlerts.get(position).getEve_sec(), true, false, false);
                    dy =  getHourDayStamp(listAlerts.get(position).getEve_sec(), false, true, false);
                    format =  getHourDayStamp(listAlerts.get(position).getEve_sec(), false, false, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                Log.d("slotIDday-hour== ", dayTitle + hourTitle);
                Log.d("slotIDday-hour== ", timeStmp +"/" + dy +"/" + format);
                if (format.contains("PM")){
                    format = "PM";
                }else{
                    format = "AM";
                }
                int endingHour = Integer.parseInt(startingHour);
                endingHour = endingHour - 1;
                if (endingHour == 0){
                    endingHour = 12;
                }else if (endingHour == 13){
                    endingHour = 1;
                }
//            eveSec.setText(dayTitle +" between " + hourTitle + " GMT." + dy + "between" + String.valueOf(endingHour)+ "-" + timeStmp  + format);
                holder.timestamp_flow.setText(dy + mContext.getResources().getString(R.string.between) + endingHour + "-" + startingHour  + format);


                holder.description_flow.setText(descriptionList.toString());

                holder.timestamp_flow.setVisibility(View.VISIBLE);
//                holder.timestamp_flow.setText(dayTitle +" between " + hourTitle + " GMT.");
                holder.ipLayout.setVisibility(View.GONE);
                holder.portLayout.setVisibility(View.GONE);

                break;
            case "blockips":

                //set height of main layout to 100dp for flow
                lp = (RecyclerView.LayoutParams) holder.llMain.getLayoutParams();
                lp.height = RecyclerView.LayoutParams.WRAP_CONTENT;
//hide all view ezxcept flow
                holder.llMutedBlockedView.setVisibility(View.GONE);
                holder.llConnectedHostView.setVisibility(View.GONE);
                holder.llIdsAlertView.setVisibility(View.GONE);

                holder.llFlowAlertView.setVisibility(View.VISIBLE);
                holder.description_flow.setText(listAlerts.get(position).getDescription());
                holder.ip_blockIps.setText(listAlerts.get(position).getIp());
                holder.timestamp_flow.setText(timeStmp);
                holder.ipLayout.setVisibility(View.GONE);
                holder.portLayout.setVisibility(View.GONE);

                break;

        }
        holder.mainView.setOnClickListener(view -> {
            removePos = position;
            if (UserManager.checkPauseInternetTimeStatus()) {
                if (isDmoatOnline){
                if (MutedOrBlocked.equalsIgnoreCase("muted_fragment")) {
                    unMuteActions(position);
                } else {
                    unBlockActions(position);
                }
            }else {
                DashBoardActivity.showAlertMsgDialog(mContext, mContext.getResources().getString(R.string.dmoat_offline));
            }
            } else {
                MyToast.showMessage(mContext, mContext.getResources().getString(R.string.paused_internet_msg));
            }

        });

    }

    @Override
    public int getItemCount() {
        return listAlerts.size();
    }

    private JSONObject createResponsePayload(Context context, int pos, AlertAction action, String type) {
        JSONObject j = new JSONObject();
        try {
            JSONObject alertJSON = null;
            if (listAlerts.get(pos).getInput_src() != null && listAlerts.get(pos).getInput_src().equalsIgnoreCase(DMoatAlert.FLOW)) {
                alertJSON = responseFlowtoJSON(pos);
            } else if (listAlerts.get(pos).getInput_src() != null && listAlerts.get(pos).getInput_src().equalsIgnoreCase(DMoatAlert.IDS)) {
                alertJSON = toJSON(pos);
            } else if (listAlerts.get(pos).getInput_src() != null && listAlerts.get(pos).getInput_src().equalsIgnoreCase(DMoatAlert.CONNECTED_HOSTS)) {
                alertJSON = cntdHoststoJSON(pos);
            } else if (listAlerts.get(pos).getInput_src() != null && listAlerts.get(pos).getInput_src().equalsIgnoreCase(DMoatAlert.BLOCK_IPs)) {
                alertJSON = blockIpstoJSON(pos);
            }
            Objects.requireNonNull(alertJSON).putOpt("response", action.toJSON());
            j.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            j.put("request_id", Dmoat.getNewRequestId());
            j.put("app_id", appID);
            j.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            j.put("token", UserManager.getInstance().getLoggedInUser().token);
            j.put("data", alertJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return j;
    }

    //
    private JSONObject toJSON(int pos) {
        JSONObject j = new JSONObject();
        try {
            j.put("input_src", listAlerts.get(pos).getInput_src());
            j.put("eve_sec", listAlerts.get(pos).getEve_sec());
            j.put("description", listAlerts.get(pos).getDescription());
            j.put("src_ip", listAlerts.get(pos).getSrc_ip());
            j.put("src_port", listAlerts.get(pos).getSrc_port());
            j.put("dest_ip", listAlerts.get(pos).getDesc_ip());
            j.put("dest_port", listAlerts.get(pos).getDest_port());
            j.put("alert_id", listAlerts.get(pos).getRecord_id());
            j.put("record_id", listAlerts.get(pos).getRecord_id());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return j;
    }

    private JSONObject responseFlowtoJSON(int pos) {
        JSONObject j = new JSONObject();
        try {
            j.put("alerts", listAlerts.get(pos).flowAlertJsonArr);
            j.put("input_src", listAlerts.get(pos).getInput_src());
//            j.put("eve_sec", listAlerts.get(pos).getEve_sec());
//            j.put("record_id", listAlerts.get(pos).getRecord_id());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return j;
    }

    private JSONObject blockIpstoJSON(int pos) {
        JSONObject j = new JSONObject();
        try {
            j.put("description", listAlerts.get(pos).getDescription());
            j.put("ip", listAlerts.get(pos).getIp());
            j.put("input_src", listAlerts.get(pos).getInput_src());
            j.put("eve_sec", listAlerts.get(pos).getEve_sec());
            j.put("record_id", listAlerts.get(pos).getRecord_id());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return j;
    }

    private JSONObject cntdHoststoJSON(int pos) {
        JSONObject j = new JSONObject();
        try {
            j.put("description", listAlerts.get(pos).getDescription());
            j.put("input_src", listAlerts.get(pos).getInput_src());
            j.put("ip", listAlerts.get(pos).getIp());
            j.put("HOSTID", listAlerts.get(pos).getHostId());
            j.put("hostname", listAlerts.get(pos).getHostName());
            j.put("eve_sec", listAlerts.get(pos).getEve_sec());
            j.put("macaddress", listAlerts.get(pos).getMacaddress());
            j.put("OS", listAlerts.get(pos).getOS());
            j.put("record_id", listAlerts.get(pos).getRecord_id());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return j;
    }


    private void unMuteActions(final int pos) {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.unmute_actions_fragment);

        TextView tvAllow = dialog.findViewById(R.id.tvAllow);
        TextView tvUnmute = dialog.findViewById(R.id.tvUnMute);
        TextView tvBlockForever = dialog.findViewById(R.id.tvBlockForever);
        TextView tvAllowForever = dialog.findViewById(R.id.tvAllowForever);
        TextView tvCancel = dialog.findViewById(R.id.tvCancel);
        Log.d("InputSrc==", listAlerts.get(pos).getInput_src());

        if (listAlerts.get(pos).getInput_src().equalsIgnoreCase("flow")) {
            tvBlockForever.setVisibility(View.GONE);
            tvAllowForever.setVisibility(View.GONE);
            tvAllow.setVisibility(View.GONE);
            tvUnmute.setVisibility(View.VISIBLE);
        }

        tvUnmute.setVisibility(View.GONE);
        final AlertAction action = new AlertAction();
        ArrayList<AlertAction> actions = null;

        tvAllow.setOnClickListener(view -> {

            action.mode = "";
            action.interval = "";
            action.type = "Allow";
            dialog.dismiss();
            JSONObject j = createResponsePayload(mContext, pos, action, "unmute");
            Api.muteAlert(mContext, j, mListener);
        });
        tvUnmute.setOnClickListener(view -> {
            action.colour = Color.rgb(0, 117, 160);
            action.mode = "";
            action.interval = "";
            action.type = "Unmute";
            dialog.dismiss();
            JSONObject j = createResponsePayload(mContext, pos, action, "unmute");
            Api.muteAlert(mContext, j, mListener);
        });
        tvBlockForever.setOnClickListener(view -> {
            action.colour = Color.rgb(0, 117, 160);
            action.type = "Block";
            action.mode = "Forever";
            action.interval = "";
            dialog.dismiss();
            JSONObject j = createResponsePayload(mContext, pos, action, "unmute");
            Api.muteAlert(mContext, j, mListener);
        });
        tvAllowForever.setOnClickListener(view -> {
            action.colour = Color.rgb(0, 117, 160);
            action.type = "Allow";
            action.mode = "Forever";
            action.interval = "";
            dialog.dismiss();
            JSONObject j = createResponsePayload(mContext, pos, action, "unmute");
            Api.muteAlert(mContext, j, mListener);
        });

        tvCancel.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    private void unBlockActions(final int pos) {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.unmute_actions_fragment);

        TextView tvAllow = dialog.findViewById(R.id.tvAllow);
        TextView tvUnmute = dialog.findViewById(R.id.tvUnMute);
        tvUnmute.setText(mContext.getResources().getString(R.string.unblock));
        TextView tvBlockForever = dialog.findViewById(R.id.tvBlockForever);
        TextView tvAllowForever = dialog.findViewById(R.id.tvAllowForever);
        tvBlockForever.setVisibility(View.GONE);
        TextView tvCancel = dialog.findViewById(R.id.tvCancel);
        View v2 = dialog.findViewById(R.id.view2);
        v2.setVisibility(View.GONE);
        Log.d("InputSrc==", listAlerts.get(pos).getInput_src());
        if (listAlerts.get(pos).getInput_src().equalsIgnoreCase("flow")) {
            tvBlockForever.setVisibility(View.GONE);
            tvAllowForever.setVisibility(View.GONE);
        } else {
            tvBlockForever.setVisibility(View.GONE);
            tvAllowForever.setVisibility(View.GONE);
        }

        final AlertAction action = new AlertAction();
        ArrayList<AlertAction> actions = null;

        tvAllow.setOnClickListener(view -> {

            action.mode = "";
            action.interval = "";
            action.type = "Allow";
            dialog.dismiss();
            JSONObject j = createResponsePayload(mContext, pos, action, "unblock");
            Api.blockAlert(mContext, j, mListener);
        });
        tvUnmute.setOnClickListener(view -> {
            action.mode = "";
            action.interval = "";
            action.type = "Unblock";
            dialog.dismiss();
            JSONObject j = createResponsePayload(mContext, pos, action, "unblock");
            Api.blockAlert(mContext, j, mListener);
        });


        tvCancel.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        final View mainView;
        //flow tv
        private final TextView timestamp_flow;
        private final TextView description_flow;
        private final TextView ip_blockIps;
        //ids tv
        private final TextView description_ids;
        private final TextView srcIp_ids;
        private final TextView srcPort_ids;
        private final TextView destIp_ids;
        private final TextView portDest_ids;
        private final TextView timestamp_ids;
        //cncted host tv
        private final TextView description_cnctdHost;
        private final TextView hostName_cnctdHost;
        private final TextView ip_cnctdHost;
        private final TextView os_cnctdHost;
        private final TextView macAddress_cnctdHost;
        private final TextView timestamp_cnctdHost;
        private final LinearLayout ipLayout;
        private final LinearLayout llHostNameCnctdHost;
        private final LinearLayout llIpCnctdHost;
        private final LinearLayout llOsCnctdHost;
        private final LinearLayout portLayout;
        private final LinearLayout llMain;
        private final LinearLayout llConnectedHostView;
        private final LinearLayout llFlowAlertView;
        private final LinearLayout llIdsAlertView;
        private final LinearLayout llMutedBlockedView;

        MyViewHolder(View itemView) {
            super(itemView);
            mainView = itemView;

            llMain = mainView.findViewById(R.id.llMain);
            llConnectedHostView = mainView.findViewById(R.id.llCnctdHostView);
            llFlowAlertView = mainView.findViewById(R.id.llFlowView);
            llIdsAlertView = mainView.findViewById(R.id.llIdsView);
            llMutedBlockedView = mainView.findViewById(R.id.llMutedBlockedView);

            //now get all ids by specific view and call them

            TextView description_muteBloked = mainView.findViewById(R.id.activity_detected);
            ipLayout = mainView.findViewById(R.id.ip_layout);
            portLayout = mainView.findViewById(R.id.port_layout);
            TextView laptopLabel = mainView.findViewById(R.id.txt_laptop);
            TextView alertDesIp = mainView.findViewById(R.id.txt_laptop_ip);
            TextView smartTvLabel = mainView.findViewById(R.id.txt_smart_tv);
            TextView alertSrcIP = mainView.findViewById(R.id.txt_smalltv_ip);
            RelativeLayout descLayout = mainView.findViewById(R.id.desc_layout);
            TextView portNumber = mainView.findViewById(R.id.port_number);

            //flow views init
            description_flow = mainView.findViewById(R.id.txt_description_flow);
            TextView ipLabelHeading = mainView.findViewById(R.id.tvIpLabelHeading);
            ip_blockIps = mainView.findViewById(R.id.txt_ip_blockIps);
            timestamp_flow = mainView.findViewById(R.id.eve_sec_timestamp_flow);

            //ids views init
            description_ids = mainView.findViewById(R.id.txt_description_ids);
            srcIp_ids = mainView.findViewById(R.id.txt_src_ip_ids);
            srcPort_ids = mainView.findViewById(R.id.src_port_ids);
            destIp_ids = mainView.findViewById(R.id.txt_dest_ip_ids);
            portDest_ids = mainView.findViewById(R.id.dest_port_ids);
            timestamp_ids = mainView.findViewById(R.id.eve_sec_timestamp_ids);

            //cnctd host views init

            description_cnctdHost = mainView.findViewById(R.id.txt_description_cnctdhost);
            hostName_cnctdHost = mainView.findViewById(R.id.txt_hostname_cnctdhost);
            llHostNameCnctdHost = mainView.findViewById(R.id.llCnctdHostName);
            llIpCnctdHost = mainView.findViewById(R.id.llCnctdHostIP);
            llOsCnctdHost = mainView.findViewById(R.id.llCnctdHostOS);
            ip_cnctdHost = mainView.findViewById(R.id.txt_ip_cnctdhost);
            os_cnctdHost = mainView.findViewById(R.id.txt_os_cnctdhost);
            macAddress_cnctdHost = mainView.findViewById(R.id.txt_mac_cnctdhost);
            timestamp_cnctdHost = mainView.findViewById(R.id.eve_sec_timestamp_cnctdhost);

        }


    }

    private final AsyncTaskListener mListener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
//            if (!isAdded()) {
//                        return;
//                    }
//                    showProgressDialog(false);
            if (result.code == 200 || result.code == 2) {
                listAlerts.remove(removePos);
                notifyDataSetChanged();
            } else {
                MyToast.showMessage(mContext, result.message);
            }
        }
    };

}
