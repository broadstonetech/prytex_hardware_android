package hardware.prytex.io.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.model.ProtocolsListModal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by macbookpro13 on 07/11/2017.
 */

public class ProtocolsDetailExpandabaleAdapter extends BaseExpandableListAdapter {

    private final Context _context;
    private final ArrayList<ProtocolsListModal> HostProtolsList;
    private final HashMap<String, String> protocolsDict;
    private final HashMap<String, String> protocolsValuesDict;


    public ProtocolsDetailExpandabaleAdapter(Context context, ArrayList<ProtocolsListModal> HostsList, HashMap<String, String> pValuesDict, HashMap<String, String> pDict, List<String> listDataHeader,
                                             HashMap<String, List<String>> listChildData, OnAlertSelectedListener listener) {
        this._context = context;
        this.HostProtolsList = HostsList;
        this.protocolsValuesDict = pValuesDict;
        this.protocolsDict = pDict;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return HostProtolsList.get(groupPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.protocol_host_group, null);
        }

        TextView txtListChild = convertView
                .findViewById(R.id.lblListHeader);
        if (protocolsDict.containsKey(HostProtolsList.get(groupPosition).getProtocolName())) {
            txtListChild.setText(protocolsDict.get(HostProtolsList.get(groupPosition).getProtocolName()));
        } else {
            txtListChild.setText(_context.getResources().getString(R.string.selected));
        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.HostProtolsList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.HostProtolsList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.protocolo_host_item, null);
        }

        TextView protocolNameTV = convertView
                .findViewById(R.id.tvProtocolName);
        TextView protocolUsgTV = convertView
                .findViewById(R.id.tvProtocolUsage);
        ImageView imgProtocol = convertView.findViewById(R.id.imgProtocol);

        String protocolName = HostProtolsList.get(groupPosition).getProtocolName();
        int checkExistence = _context.getResources().getIdentifier(protocolName, "drawable", _context.getPackageName());

        if (checkExistence != 0) {  // the resouce exists...
            Drawable drawable = _context.getResources().getDrawable(checkExistence);
            imgProtocol.setImageDrawable(drawable);
        } else {  // checkExistence == 0  // the resouce does NOT exist!!
            if (protocolName == "office365"){
                imgProtocol.setImageResource(R.drawable.office);
            }else {
                imgProtocol.setImageResource(R.drawable.protocol);
            }
        }

        if (protocolsValuesDict.containsKey(HostProtolsList.get(groupPosition).getProtocolName())) {
            protocolNameTV.setText(protocolsValuesDict.get(HostProtolsList.get(groupPosition).getProtocolName()));
        } else {
            protocolNameTV.setText(HostProtolsList.get(groupPosition).getProtocolName());
        }

        String hostUsg = String.format("%.02f", HostProtolsList.get(groupPosition).getProtocolUsage());

        protocolUsgTV.setText(hostUsg + _context.getResources().getString(R.string.MB));

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public interface OnAlertSelectedListener {
    }
}