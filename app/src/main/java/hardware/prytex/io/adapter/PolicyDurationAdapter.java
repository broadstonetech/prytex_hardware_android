package hardware.prytex.io.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.model.PolicyDurationModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by macbookpro13 on 01/03/2018.
 */

@SuppressWarnings("ALL")
public class PolicyDurationAdapter extends BaseExpandableListAdapter {

    private final Context _context;
    private final List<String> _listDataHeader; // header titles
    private final List<String> _listDataHeaderDuration; // header titles duration
    // child data in format of header title, child title
    private final HashMap<String, List<String>> _listDataChild;
    private final HashMap<String, List<String>> _listDataChildDest;

    public PolicyDurationAdapter(Context context, List<String> listDataHeader,List<String> listDataHeaderDuration,
                                 HashMap<String, List<String>> listChildData,HashMap<String,List<String>> listChildDataDest, ArrayList<PolicyDurationModel> list) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataHeaderDuration = listDataHeaderDuration;
        this._listDataChild = listChildData;
        this._listDataChildDest = listChildDataDest;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
//        return listPreMutedAlerts.get(groupPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.policy_duration_child_item, null);
        }

        TextView tvConnectionTime = convertView
                .findViewById(R.id.tvTime);
        TextView tvslotInterval = convertView
                .findViewById(R.id.tvSlotInterval);

        String timeInterval = this._listDataChildDest.get(this._listDataHeader.get(groupPosition))
                .get(childPosition);
        Log.d("HostIndexing", groupPosition + "===" + childPosition);
        if (timeInterval.equalsIgnoreCase("date")){
            tvConnectionTime.setText(this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .get(childPosition));
//            tvConnectionTime.setTypeface(Typeface.DEFAULT_BOLD);
            tvConnectionTime.setTextColor(_context.getResources().getColor(R.color.colorPrimary));
            tvslotInterval.setVisibility(View.GONE);
        }else {
            tvConnectionTime.setText(this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .get(childPosition));

            tvConnectionTime.setTextColor(_context.getResources().getColor(R.color.black));
            tvslotInterval.setVisibility(View.VISIBLE);
            tvslotInterval.setText(timeInterval + " min");
        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.policy_duration_header_item, null);
        }

        TextView tvHostName = convertView
                .findViewById(R.id.lblHostName);
        TextView tvTotalDuration = convertView.findViewById(R.id.lblHostTotalDuration);
        TextView tvHostNameHEADING= convertView.findViewById(R.id.lblHostNameHeading);
        TextView tvTotalDurationHEADING = convertView.findViewById(R.id.lblHostTotalDurationHeading);

        String hostName = _listDataHeader.get(groupPosition);
        if (hostName.contains("Unknown")) {
            hostName = hostName.substring(0, hostName.indexOf("=="));
        }
        tvHostName.setText(hostName);

        tvTotalDuration.setText(_listDataHeaderDuration.get(groupPosition));

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}