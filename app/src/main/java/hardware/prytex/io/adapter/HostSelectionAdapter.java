package hardware.prytex.io.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.model.ConnectedDeviceList;

import java.util.ArrayList;

public class HostSelectionAdapter extends RecyclerView.Adapter<HostSelectionAdapter.ViewHolder> {

    private final ArrayList<ConnectedDeviceList> policiesList;
    final Context context;



    public HostSelectionAdapter(Context context, ArrayList<ConnectedDeviceList> alName) {
        super();
        this.policiesList = alName;
        this.context = context;
    }


    @NonNull
    @Override
    public HostSelectionAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.policy_plan_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final HostSelectionAdapter.ViewHolder viewHolder, final int i) {

        viewHolder.tvPolicyTitle.setText(policiesList.get(i).getHostName_cd());
        viewHolder.tvDevicesCount.setText(policiesList.get(i).getIp_cd());

        if (policiesList.get(i).isInPolicy()) {
            viewHolder.imgCheckMark.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imgCheckMark.setVisibility(View.GONE);
        }


        viewHolder.cardView.setOnClickListener(v -> {

            //pori list ko false kro r jis py click kia sirf usi ko true krna h
            for (int j = 0; j < policiesList.size(); j++ ) {
                policiesList.get(j).setInPolicy(false);
            }
            if (policiesList.get(i).isInPolicy()) {
                policiesList.get(i).setInPolicy(false);
            } else {
                policiesList.get(i).setInPolicy(true);
            }
//                    notifyItemChanged(pos);
            notifyDataSetChanged();
//                notifyItemRangeChanged(i, policiesList.size());
        });


    }

    @Override
    public int getItemCount() {
        return policiesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        final TextView tvPolicyTitle;
        final TextView tvPolicyInterval;
        final TextView tvDevicesCount;
        final TextView tvDevicesCountHeading;
        final TextView tvIntervalHeading;
        final TextView tvActionsLabel;
        final CardView cardView;
        final LinearLayout llItemView;
        final SwitchCompat policySwitchBtn;
        final ImageView imgSwitchBtn;
        final ImageView imgPolicyType;
        final ImageView imgCheckMark;
        final LinearLayout llInterval;
        final LinearLayout llDevices;

        ViewHolder(final View itemView) {
            super(itemView);
            tvPolicyTitle = itemView.findViewById(R.id.tvPolicyTitle);
            tvPolicyInterval = itemView.findViewById(R.id.tvPolicyInterval);
            tvIntervalHeading = itemView.findViewById(R.id.tvIntervalHeading);
            tvDevicesCountHeading = itemView.findViewById(R.id.tvDevicesCountHeading);
            tvDevicesCount = itemView.findViewById(R.id.tvDevicesCount);
            cardView = itemView.findViewById(R.id.card_view);
            llItemView = itemView.findViewById(R.id.llItemDetails);
            policySwitchBtn = itemView.findViewById(R.id.swicth_policyStatus);
            imgSwitchBtn = itemView.findViewById(R.id.imgSwitchBtn);
            imgPolicyType = itemView.findViewById(R.id.imgPolicyType);
            tvActionsLabel = itemView.findViewById(R.id.tvActionsLabel);
            llInterval = itemView.findViewById(R.id.llInterval);
            llDevices = itemView.findViewById(R.id.llDevices);
            imgCheckMark = itemView.findViewById(R.id.imgCheckMark);

            llInterval.setVisibility(View.GONE);
            imgPolicyType.setVisibility(View.GONE);
            tvActionsLabel.setVisibility(View.GONE);
            imgSwitchBtn.setVisibility(View.GONE);
            policySwitchBtn.setVisibility(View.GONE);
            tvDevicesCountHeading.setText(context.getResources().getString(R.string.selected));
            tvIntervalHeading.setText(context.getResources().getString(R.string.applied_on));

        }
    }

}
