package hardware.prytex.io.model;

/**
 * Created by macbookpro13 on 11/05/2017.
 */

public class ProtocolsListModal {
    private int id;
    private String protocolName;
    private Float protocolUsage;
    private boolean isInPolicy;

    public void setId(int id) {
        this.id = id;
    }

    public String getProtocolName() {
        return protocolName;
    }

    public void setProtocolName(String protocolName) {
        this.protocolName = protocolName;
    }

    public Float getProtocolUsage() {
        return protocolUsage;
    }

    public void setProtocolUsage(Float protocolUsage) {
        this.protocolUsage = protocolUsage;
    }

    public boolean isInPolicy() {
        return isInPolicy;
    }

    public void setInPolicy(boolean inPolicy) {
        isInPolicy = inPolicy;
    }

}
