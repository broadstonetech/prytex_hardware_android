package hardware.prytex.io.model;

import org.json.JSONArray;

/**
 * Created by macbookpro13 on 14/12/2017.
 */

public class PreMutedAlertsModel {
    private int id;
    private String description;
    private int eve_sec;
    private String src_ip;
    private String dest_ip;

    //flow
    private Integer alertId;
    private int slotId;
    private String flowType;
    private String flowDetail;
    public JSONArray flowAlertJsonArr;

    private String input_src;

    public String getInput_src() {
        return input_src;
    }

    public void setInput_src(String input_src) {
        this.input_src = input_src;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSrc_ip() {
        return src_ip;
    }

    public void setSrc_ip(String src_ip) {
        this.src_ip = src_ip;
    }

    public String getDest_ip() {
        return dest_ip;
    }

    public void setDest_ip(String dest_ip) {
        this.dest_ip = dest_ip;
    }
    public int getEve_sec() {
        return eve_sec;
    }

    public void setEve_sec(int eve_sec) {
        this.eve_sec = eve_sec;
    }

    public void setSlotId(int slotId) {
        this.slotId = slotId;
    }

}
