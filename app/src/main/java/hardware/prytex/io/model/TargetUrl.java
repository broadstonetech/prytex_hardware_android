package hardware.prytex.io.model;

import io.realm.RealmObject;

/**
 * Created by khawarraza on 22/12/2016.
 */

public class TargetUrl extends RealmObject {

    private String url;

    public TargetUrl(){}

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
