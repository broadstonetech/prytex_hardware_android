package hardware.prytex.io.model;

/**
 * Created by macbookpro13 on 11/08/2017.
 */

public class SensorDataModel {
    private String base_res;
    private String air_quality;
    private int mean_adc;
    private float temperature;
    private int eve_sec;
    private int coppm;
    private  float humidity;
    private int validInterval;
    private int mean_res;
    private int mean_corrected_res;

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public int getEve_sec() {
        return eve_sec;
    }

    public void setEve_sec(int eve_sec) {
        this.eve_sec = eve_sec;
    }

    public int getCoppm() {
        return coppm;
    }

    public void setCoppm(int coppm) {
        this.coppm = coppm;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

}
