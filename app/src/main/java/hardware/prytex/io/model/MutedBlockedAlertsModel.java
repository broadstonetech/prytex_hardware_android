package hardware.prytex.io.model;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by macbookpro13 on 09/08/2017.
 */

@SuppressWarnings("ALL")
public class MutedBlockedAlertsModel {

    public static final String NEW_HOST_ALERT = "cnctdhost";
    public static final String SENSOR_ALERT = "sensor";
    public static final String INFO_ALERT = "info";
    public static final String BLOCK_IPS_ALERT = "blockips";
    public static final String IDS_ALERT = "ids";
    public static final String FLOW_ALERT = "flow";
    public static final String UPDATE = "update";
    //sensor
    private float sensorReading;
    private String category;

    //connected host
    private String deviceCategory;
    private String hostId;
    private String OS;
    private String hostName;
    private String macaddress;
    private String parameters;

    //flow
    private Integer alertId;
    private int slotId;
    private String flowType;
    private String flowDetail;
    public JSONArray flowAlertJsonArr;
    public JSONArray flowRecordIdsList;

    //ids
    private String destHostName;
    private String srcHostName;
    private String dest_port;
    private String desc_ip;
    private String src_ip;
    private String src_port;


    private String idsBlockType;

    //blockIps

    //info
    private String info_type;
    private int timeOn;
    private int timeOff;



    //similar
    private int id;
    private String recordId;
    private String description;
    private int eve_sec;
    private String input_src;
    private String namespace;
    private String app_id;
    private String record_id;
    private String ip;
    private boolean seenStatus;

    private String parentalControlFlag="";
    private String modelFlag="";
    private String version="";


    public void setParentalControlFlag(String parentalControlFlag) {
        this.parentalControlFlag = parentalControlFlag;
    }

    public void setModelFlag(String modelFlag) {
        this.modelFlag = modelFlag;
    }


    private String type="";


    public boolean isSeenStatus() {
        return seenStatus;
    }

    public void setSeenStatus(boolean seenStatus) {
        this.seenStatus = seenStatus;
    }

    public void setflowDetail(String flowDetail) {
        this.flowDetail = flowDetail;
    }


    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getHostId() {
        return hostId;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }

    public String getMacaddress() {
        return macaddress;
    }

    public void setMacaddress(String macaddress) {
        this.macaddress = macaddress;
    }

    public String getOS() {
        return OS;
    }

    public void setOS(String OS) {
        this.OS = OS;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }


    public String getSrc_port() {
        return src_port;
    }

    public void setSrc_port(String src_port) {
        this.src_port = src_port;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInput_src() {
        return input_src;
    }

    public void setInput_src(String input_src) {
        this.input_src = input_src;
    }

    public int getEve_sec() {
        return eve_sec;
    }

    public void setEve_sec(int eve_sec) {
        this.eve_sec = eve_sec;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public void setDestHostName(String destHostName) {
        this.destHostName = destHostName;
    }

    public String getSrcHostName() {
        return srcHostName;
    }

    public void setSrcHostName(String srcHostName) {
        this.srcHostName = srcHostName;
    }

    public String getSrc_ip() {
        return src_ip;
    }

    public void setSrc_ip(String src_ip) {
        this.src_ip = src_ip;
    }

    public String getRecord_id() {
        return record_id;
    }

    public void setRecord_id(String record_id) {
        this.record_id = record_id;
    }

    public String getDest_port() {
        return dest_port;
    }

    public void setDest_port(String dest_port) {
        this.dest_port = dest_port;
    }

    public String getDesc_ip() {
        return desc_ip;
    }

    public void setDesc_ip(String desc_ip) {
        this.desc_ip = desc_ip;
    }

    public float getSensorReading() {
        return sensorReading;
    }

    public void setSensorReading(float sensorReading) {
        this.sensorReading = sensorReading;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDeviceCategory() {
        return deviceCategory;
    }

    public void setDeviceCategory(String deviceCategory) {
        this.deviceCategory = deviceCategory;
    }

    public Integer getAlertId() {
        return alertId;
    }

    public void setAlertId(Integer alertId) {
        this.alertId = alertId;
    }

    public int getSlotId() {
        return slotId;
    }

    public void setSlotId(int slotId) {
        this.slotId = slotId;
    }

    public void setFlowType(String flowType) {
        this.flowType = flowType;
    }

    public String getInfo_type() {
        return info_type;
    }

    public void setInfo_type(String info_type) {
        this.info_type = info_type;
    }


    public String getIdsBlockType() {
        return idsBlockType;
    }

    public void setIdsBlockType(String idsBlockType) {
        this.idsBlockType = idsBlockType;
    }

    public int getTimeOn() {
        return timeOn;
    }

    public void setTimeOn(int timeOn) {
        this.timeOn = timeOn;
    }

    public int getTimeOff() {
        return timeOff;
    }

    public void setTimeOff(int timeOff) {
        this.timeOff = timeOff;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }


    public String getFormattedTimeStamp(int es) {
        try {
            return getrTimeStamp(es);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.valueOf(es);
    }

    private static String getrTimeStamp(int s) throws Exception {
        float fTime = Float.parseFloat(String.valueOf(s));
        long time = (long) fTime;
        time = time * 1000;
        Date date = new Date(time);
        SimpleDateFormat sourceFormat = new SimpleDateFormat("MMM-dd-yyyy h:mm a", Locale.getDefault());
//        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateString = sourceFormat.format(date); // => Date is in UTC now
        Date parsed = sourceFormat.parse(dateString); // => Date is in UTC now

        SimpleDateFormat destFormat = new SimpleDateFormat("MMM-dd-yyyy h:mm a", Locale.getDefault());
//        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        destFormat.setTimeZone(Calendar.getInstance().getTimeZone());
//        destFormat.setTimeZone(TimeZone.getDefault());
        return destFormat.format(parsed);
    }
}
