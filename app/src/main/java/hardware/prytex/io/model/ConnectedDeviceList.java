package hardware.prytex.io.model;

import org.json.JSONArray;

/**
 * Created by applepc on 24/04/2017.
 */

public class ConnectedDeviceList {
    private int id_cd;
    private int highCount_cd;
    private int lowCount_cd;
    private int mediumCount_cd;
    private String macAddress_cd;
    private String hostId_cd;
    private String parametrs_cd;
    private int eveSec_cd;
    private String ip_cd;
    private String hostName_cd;
    private String os_cd;
    private String userName;
    private String deviceCategory;
    private String deviceType;
    private int score;
    private boolean isInPolicy;
    public JSONArray jsonArrayPorts;
    public JSONArray jsonArrayDefaultPasswords;

    public String getdeviceCategory() {
        return deviceCategory;
    }

    public void setdeviceCategory(String inputSrc) {
        this.deviceCategory = inputSrc;
    }


    public String getRecord_id() {
        return record_id;
    }

    public void setRecord_id(String record_id) {
        this.record_id = record_id;
    }

    private String record_id;


    public String getMacAddress_cd() {
        return macAddress_cd;
    }

    public void setMacAddress_cd(String macAddress_cd) {
        this.macAddress_cd = macAddress_cd;
    }

    public String getHostId_cd() {
        return hostId_cd;
    }

    public void setHostId_cd(String hostId_cd) {
        this.hostId_cd = hostId_cd;
    }

    public String getParametrs_cd() {
        return parametrs_cd;
    }

    public void setParametrs_cd(String parametrs_cd) {
        this.parametrs_cd = parametrs_cd;
    }

    public int getEveSec_cd() {
        return eveSec_cd;
    }

    public void setEveSec_cd(int eveSec_cd) {
        this.eveSec_cd = eveSec_cd;
    }

    public String getIp_cd() {
        return ip_cd;
    }

    public void setIp_cd(String ip_cd) {
        this.ip_cd = ip_cd;
    }

    public String getHostName_cd() {
        return hostName_cd;
    }

    public void setHostName_cd(String hostName_cd) {
        this.hostName_cd = hostName_cd;
    }

    public String getOs_cd() {
        return os_cd;
    }

    public void setOs_cd(String os_cd) {
        this.os_cd = os_cd;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isInPolicy() {
        return isInPolicy;
    }

    public void setInPolicy(boolean inPolicy) {
        isInPolicy = inPolicy;
    }

    public void setScore(int score) {
        this.score = score;
    }

}
