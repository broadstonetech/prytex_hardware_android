package hardware.prytex.io.model;

/**
 * Created by macbookpro13 on 08/09/2017.
 */

public class LoggedInDevices {
    private String Ip;
    private String timeStamp;
    private String location;

    public void setLocation(String location) {
        this.location = location;
    }


    public void setIp(String ip) {
        Ip = ip;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

}
