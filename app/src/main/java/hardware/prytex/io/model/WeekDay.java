package hardware.prytex.io.model;

import io.realm.RealmObject;

/**
 * Created by khawarraza on 22/12/2016.
 */

public class WeekDay extends RealmObject {

    private int index;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
