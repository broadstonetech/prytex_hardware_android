package hardware.prytex.io.model;

import org.json.JSONArray;

/**
 * Created by macbookpro13 on 01/03/2018.
 */

public class PolicyDurationModel {
    private int id;
    private String hostName;
    private String totalDuration;
    private int eve_sec;
    private String connectTime;
    private String disConnectTime;
    private String slotUsage;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(String totalDuration) {
        this.totalDuration = totalDuration;
    }

    public String getConnectTime() {
        return connectTime;
    }

    public void setConnectTime(String connectTime) {
        this.connectTime = connectTime;
    }

    public String getSlotUsage() {
        return slotUsage;
    }

    public void setSlotUsage(String slotUsage) {
        this.slotUsage = slotUsage;
    }

}
