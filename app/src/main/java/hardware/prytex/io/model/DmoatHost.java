package hardware.prytex.io.model;

import io.realm.RealmObject;

/**
 * Created by khawarraza on 22/12/2016.
 */

public class DmoatHost extends RealmObject {

    public String hostId;
    public String hostName;

    public DmoatHost (){

    }

    public String getHostId() {
        return hostId;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }
}
