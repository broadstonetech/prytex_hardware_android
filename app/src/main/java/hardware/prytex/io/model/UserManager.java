package hardware.prytex.io.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import hardware.prytex.io.db.DBClient;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import hardware.prytex.io.fragment.DashBoardFragment;


/**
 * Created by abubaker on 9/3/15.
 */
@SuppressWarnings("ALL")
public class UserManager {

    private static final String USER_PREFS = "user_prefs";
    public static String USER_TOKEN_PREFS = "user_token_prefs";
    private static UserManager instance = null;

    private User mUser;
    private final Context mContext;

    private UserManager(Context context){
        this.mContext = context;
        mUser = readUserFromPreferences();
    }

    private User readUserFromPreferences() {
        SharedPreferences prefs = mContext.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE);
        String userData = prefs.getString("user", null);
        if(userData == null || userData.length() == 0) {
            return null;
        }
        JSONObject j;
        try {
            j = new JSONObject(userData);
        } catch (Exception e) {
            return null;
        }
        return new User(j);
    }

    public void writeUserToPreferences(User user) {
        SharedPreferences prefs = mContext.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        String s = user.toString();
        editor.putString("user", user.toString());
        editor.putBoolean("isLoggedIn", false);
        editor.apply();
        this.mUser = user;
    }

    // Check for old user data
    public void updateUsernamePrivately(String user){
        SharedPreferences prefs = mContext.getSharedPreferences("user_name", Context.MODE_PRIVATE);
        if (!prefs.getString("username", "").equalsIgnoreCase(user)) {
            DBClient.getInstance().deleteAllAlerts();
            prefs.edit().putString("username", user).apply();
        }
    }

    public String getUsername(){
        return mContext.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE).getString("username", "");
    }

    public static void initUserManager(Context context) {
        instance = new UserManager(context);
    }

    public static UserManager getInstance(){
        return instance;
    }

    public void initWithUser(User user) {
        this.mUser = user;
    }

    public boolean isUserLoggedIn() {
//        SharedPreferences prefs = mContext.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE);
//        prefs.getBoolean("isLoggedIn", false);
        return mUser != null && mUser.isLoggedIn();
    }

    public void logout() {
        this.mUser = null;
        SharedPreferences prefs = mContext.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE);
//        prefs.edit().putBoolean("isLoggedIn", false);
        prefs.edit().clear().apply();
    }

    public User getLoggedInUser() {
        return mUser;
    }

    public JSONObject getUserJSON() {
        if (isUserLoggedIn() && this.mUser != null) {
            return this.getLoggedInUser().toJSONObject();
        }
        return null;
    }

//time check for pause internet
    public static boolean checkPauseInternetTimeStatus() {
        Date date1 = null;
        Date date2 = null;
        Calendar calender;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        calender = Calendar.getInstance();
        try {
            if (DashBoardFragment.savedPauseTime.equalsIgnoreCase("")){
                Log.d("pauseInternet time", "saved time empty");
                return true;
            }
            date1 = sdf.parse(DashBoardFragment.savedPauseTime);

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String formatedDate = df.format(calender.getTime());
            date2 = sdf.parse(formatedDate);
            Log.d("time1Pause", String.valueOf(date1));
            Log.d("time2Pause", String.valueOf(date2));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date2.before(date1)) {
            Log.d("pauseInternet time", "time remaining");
            return false;
        }
        Log.d("pauseInternet time", "time out");
        return true;
    }
}
