package hardware.prytex.io.model;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.db.DBClient;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by khawarraza on 16/12/2016.
 */

public class DmoatPolicy extends RealmObject implements Serializable {

    @PrimaryKey
    public int id;
    public int days;
    private String type;
    public String title;
    @JsonProperty("s_time")
    public String startTime;
    @JsonProperty("e_time")
    public String endTime;
    private String policyStatus;


    @JsonProperty("targets")
    private RealmList<TargetUrl> targetUrls;
//    public RealmList<WeekDay> weekDays;
    @JsonProperty("hosts_list")
    public RealmList<DmoatHost> hosts;
    @JsonProperty("protocols")
    private RealmList<Protocol> protocols;

    public DmoatPolicy() {
        targetUrls = new RealmList<>();
//        weekDays = new RealmList<>();
        hosts = new RealmList<>();
        protocols = new RealmList<>();
    }


    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    private void setTitle(String title) {
        this.title = title;
    }

    public String getStartTime() {
        return startTime;
    }

    private void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    private void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public RealmList<TargetUrl> getTargetUrls() {
        return targetUrls;
    }

    /*public RealmList<WeekDay> getWeekDays() {
        return weekDays;
    }

    public void setWeekDays(RealmList<WeekDay> weekDays) {
        this.weekDays = weekDays;
    }*/

    public RealmList<DmoatHost> getHosts() {
        return hosts;
    }

    public RealmList<Protocol> getProtocols() {
        return protocols;
    }

    public static class DMoatPolicyBuilder {

        final DmoatPolicy policy;

        public DMoatPolicyBuilder() {
            policy = new DmoatPolicy();
        }

        public DMoatPolicyBuilder setPolicyId(int id) {
            policy.setId(id);
            return this;
        }

        public DMoatPolicyBuilder setStartTime(String time) {
            policy.setStartTime(time);
            return this;
        }

        public DMoatPolicyBuilder setEndTime(String endTime) {
            policy.setEndTime(endTime);
            return this;
        }

        public DMoatPolicyBuilder setTitle(String title) {
            policy.setTitle(title);
            return this;
        }

        public DMoatPolicyBuilder addTargetUrls(ArrayList<String> urls) {
            if (urls == null) {
                return this;
            }
            for (String url : urls) {
                TargetUrl u = new TargetUrl();
                u.setUrl(url);
                policy.targetUrls.add(u);
            }
            return this;
        }

        public DMoatPolicyBuilder addWeekDays(int weight/*, ArrayList<Integer> indexes*/) {
            policy.days = weight;
            /*if (indexes == null) {
                return this;
            }
            for (Integer index : indexes) {
                WeekDay wd = new WeekDay();
                wd.setIndex(index);
                policy.weekDays.add(wd);
            }*/
            return this;
        }

        public DMoatPolicyBuilder addHosts(ArrayList<DMoatAlert> hosts) {
            if (hosts == null) {
                return this;
            }
            for (DMoatAlert host : hosts) {
                DmoatHost dhost = new DmoatHost();
                dhost.setHostId(host.hostId);
                dhost.setHostName(host.hostname);
                policy.hosts.add(dhost);
            }
            return this;
        }

        public DMoatPolicyBuilder addProtocols(ArrayList<Protocol> protocols) {
            if (protocols == null) {
                return this;
            }
            for (Protocol protocol : protocols) {
                Protocol mProtocol = new Protocol();
                mProtocol.setName(protocol.name);
//                mProtocol.setIndex(protocol.index);
                policy.protocols.add(mProtocol);
            }
            return this;
        }

        public DmoatPolicy getPolicy() {
            policy.id = policy.getId() != 0 ? policy.getId() : Dmoat.getUniquePolicyId();
            return policy;
        }

    }
}
