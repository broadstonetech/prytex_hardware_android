package hardware.prytex.io.model;

/**
 * Created by khawarraza on 24/11/2016.
 */

public class Subscriber {

    public String email;

    public Subscriber(String email) {
        this.email = email;
    }
}
