package hardware.prytex.io.model;

import org.json.JSONArray;

public class PolicyPlanModel {

    private String policyId;
    private String policyTitle;
    private String startTime;
    private String endTime;
    private String startTimeGMT;
    private String endTimeGMT;
    private int policyDays;
    private int policyStatus;
    public JSONArray policyHostsLists;

    //protocolPolicy
    private String procedure;
    private String type;
    private String dataType;
    private String request;
    private String hostMacAddress;
    private String hostIpAdress;
    private String hostName;
    private String hostId;
    public JSONArray policyProtocolsLists;

    private Boolean preDefPolicy = false;
    private String descripton = "";

    public String getDescripton() {
        return descripton;
    }

    public void setDescripton(String descripton) {
        this.descripton = descripton;
    }



    public Boolean getPreDefPolicy() {
        return preDefPolicy;
    }

    public void setPreDefPolicy(Boolean preDefPolicy) {
        this.preDefPolicy = preDefPolicy;
    }



    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getHostId() {
        return hostId;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }


    public void setRequest(String request) {
        this.request = request;
    }


    public void setHostMacAddress(String hostMacAddress) {
        this.hostMacAddress = hostMacAddress;
    }

    public String getHostIpAdress() {
        return hostIpAdress;
    }

    public void setHostIpAdress(String hostIpAdress) {
        this.hostIpAdress = hostIpAdress;
    }


    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }


    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getPolicyTitle() {
        return policyTitle;
    }

    public void setPolicyTitle(String policyTitle) {
        this.policyTitle = policyTitle;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStartTimeGMT() {
        return startTimeGMT;
    }

    public void setStartTimeGMT(String startTimeGMT) {
        this.startTimeGMT = startTimeGMT;
    }

    public String getEndTimeGMT() {
        return endTimeGMT;
    }

    public void setEndTimeGMT(String endTimeGMT) {
        this.endTimeGMT = endTimeGMT;
    }

    public int getPolicyDays() {
        return policyDays;
    }

    public void setPolicyDays(int policyDays) {
        this.policyDays = policyDays;
    }

    public int getPolicyStatus() {
        return policyStatus;
    }

    public void setPolicyStatus(int policyStatus) {
        this.policyStatus = policyStatus;
    }


}
