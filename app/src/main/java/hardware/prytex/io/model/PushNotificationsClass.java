package hardware.prytex.io.model;

public class PushNotificationsClass {

    private Integer info=0;
    private Integer connectedHost=0;
    private Integer blocked=0;
    private Integer devices=0;
    private Integer sensors=0;
    private Boolean isCheck = false;

    public Boolean getCheck() {
        return isCheck;
    }

    public void setCheck(Boolean check) {
        isCheck = check;
    }


    public Integer getInfo() {
        return info;
    }

    public void setInfo(Integer info) {
        this.info = info;
    }

    public Integer getConnectedHost() {
        return connectedHost;
    }

    public void setConnectedHost(Integer connectedHost) {
        this.connectedHost = connectedHost;
    }

    public Integer getBlocked() {
        return blocked;
    }

    public void setBlocked(Integer blocked) {
        this.blocked = blocked;
    }

    public Integer getDevices() {
        return devices;
    }

    public void setDevices(Integer devices) {
        this.devices = devices;
    }

    public Integer getSensors() {
        return sensors;
    }

    public void setSensors(Integer sensors) {
        this.sensors = sensors;
    }
}
