package hardware.prytex.io.model;

import androidx.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.realm.RealmObject;

/**
 * Created by usmanarshad on 30/1/2017.
 */
@JsonIgnoreProperties({"valid", "loaded"})
public class Led extends RealmObject {

    public String mode, frequency;

    public Led(){}

    @NonNull
    @Override
    public String toString() {
        return "Led{" +
                "mode='" + mode + '\'' +
                ", frequency='" + frequency + '\'' +
                '}';
    }
}
