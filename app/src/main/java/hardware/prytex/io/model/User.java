package hardware.prytex.io.model;

import android.util.Log;

import androidx.annotation.NonNull;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.activity.SplashActivity;
import hardware.prytex.io.db.DBClient;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by abubaker on 9/2/15.
 * Edited by usmanarshad on 1/31/17.
 */
public class User implements Serializable {

    public String nameSpace;
    public String postKey;
    public Led led;
    public NetConfig net;
    public String email, name, userName, token;
    public final ArrayList<Subscriber> subscribers = new ArrayList<>();
    private final ArrayList<DmoatPolicy> policy = new ArrayList<>();
    public static final String netConfigTypeSaved = "null";


    public User(JSONObject j) {

        nameSpace = j.optString("namespace");
        postKey = j.optString("postkey");
        email = j.optString("email");
        name = j.optString("name");
        userName = j.optString("username");
        token = j.optString("token");
        JSONArray subs = j.optJSONArray("subscriber");
        if (subs != null && subs.length() > 0) {
            try {
                for (int i = 0; i < subs.length(); i++) {
                    subscribers.add(new Subscriber((String) subs.get(i)));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
//        try {
//            String ledObject = j.optString("led");
//            if (ledObject != null) {
//                ObjectMapper mapper = new ObjectMapper();
//                led = mapper.readValue(ledObject, Led.class);
//                Log.d("Led", led.toString());
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
///commenting on 24-april-2019
//        try {
//            String netObject = j.getString("net");
//            JSONObject jobject = j.getJSONObject("net");
//
//            netConfigTypeSaved = jobject.getString("type");
//
//            if (netObject != null) {
//                ObjectMapper mapper = new ObjectMapper();
//                Log.d("netConfigObject", netObject);
////                String a = j.getString("type");
//                net = mapper.readValue(netObject, NetConfig.class);
//                Log.d("Mode", net.toString());
//            }
//        } catch (JSONException e){
//            e.printStackTrace();
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
//        if (j.has("policy") && j.optJSONArray("policy").toString().length() >= 0)
//            setPoliciesData(j.optJSONArray("policy"));

    }

    private void setPoliciesData(JSONArray policies){
        JsonFactory factory = new JsonFactory();
        ObjectMapper mapper = new ObjectMapper(factory);
        try {
            ArrayList<DmoatPolicy> list = mapper.readValue(policies.toString(), new TypeReference<List<DmoatPolicy>>(){ });
            policy.addAll(list);
            DBClient.getInstance().deleteAllPolicies();
            for (int i = 0; i < list.size(); i++){
                DBClient.getInstance().createNewPolicy(list.get(i));
            }
            Log.d("LIst", list.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public User() {

    }

    public boolean isLoggedIn() {
        return nameSpace != null && nameSpace.length() != 0 && postKey != null && postKey.length() != 0;
    }

    @NonNull
    public String toString() {
        return toJSONObject().toString();
    }

    public JSONObject toJSONObject() {
        JSONObject j = new JSONObject();
        try {
            j.put("namespace", nameSpace);
            j.put("postkey", postKey);
            j.put("email", email);
            j.put("name", name);
            j.put("username", userName);
            j.put("app_id", SplashActivity.androidId);
            j.put("request_id", Dmoat.getNewRequestId());
            j.put("token", token);

            JSONArray subs = new JSONArray();
            for (int i = 0; i < subscribers.size(); i++) {
                subs.put(subscribers.get(i).email);
            }
//            j.put("subscriber", subs);
//
//            ObjectMapper mapper = new ObjectMapper();
//            String ledjson = mapper.writeValueAsString(led);
//            String netjson = mapper.writeValueAsString(net);
//
//            j.put("led", ledjson);
//            j.put("net", netjson);
        } catch (Exception ignored) {

        }

        return j;
    }
}
