package hardware.prytex.io.model;

import org.json.JSONObject;

/**
 * Created by khawarraza on 10/11/2016.
 */

public class AlertAction {

    public String title, type, mode, interval;
    public int colour;

    public JSONObject toJSON() {
        JSONObject j = new JSONObject();
        try {
            j.put("type", type);
            j.put("mode", mode);
            j.put("interval", interval);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return j;
    }

}
