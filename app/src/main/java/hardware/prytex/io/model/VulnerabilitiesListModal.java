package hardware.prytex.io.model;

import com.fasterxml.jackson.databind.JsonNode;

import org.json.JSONArray;

/**
 * Created by applepc on 24/04/2017.
 */

public class VulnerabilitiesListModal {

    private int id;
    private String risk_cv;
    private String title_cv;
    private String attackVector_cv;
    private String references_cv;
    private String attackComplexity_cv;
    private String publishedDate_cv;
    private String description_cv;
    public final JsonNode refAr = null;
    public JSONArray jArrRefList;

    public String getRisk_cv() {
        return risk_cv;
    }

    public void setRisk_cv(String risk_cv) {
        this.risk_cv = risk_cv;
    }

    public String getTitle_cv() {
        return title_cv;
    }

    public void setTitle_cv(String title_cv) {
        this.title_cv = title_cv;
    }

    public String getAttackVector_cv() {
        return attackVector_cv;
    }

    public void setAttackVector_cv(String attackVector_cv) {
        this.attackVector_cv = attackVector_cv;
    }

    public void setReferences_cv(String references_cv) {
        this.references_cv = references_cv;
    }

    public String getAttackComplexity_cv() {
        return attackComplexity_cv;
    }

    public void setAttackComplexity_cv(String attackComplexity_cv) {
        this.attackComplexity_cv = attackComplexity_cv;
    }

    public String getPublishedDate_cv() {
        return publishedDate_cv;
    }

    public void setPublishedDate_cv(String publishedDate_cv) {
        this.publishedDate_cv = publishedDate_cv;
    }

    public String getDescription_cv() {
        return description_cv;
    }

    public void setDescription_cv(String description_cv) {
        this.description_cv = description_cv;
    }

}
