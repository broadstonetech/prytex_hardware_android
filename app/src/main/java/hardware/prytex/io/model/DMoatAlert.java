package hardware.prytex.io.model;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.network.Api;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import hardware.prytex.io.activity.DashBoardActivity;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;


/**
 * Created by abubaker on 8/16/16.
 */

@SuppressWarnings("ALL")
public class DMoatAlert extends RealmObject implements Serializable {

    public static final String CONNECTED_HOSTS = "cnctdhost";
    public static final String IDS = "ids";
    public static final String FLOW = "flow";
    public static final String AIR_QUALITY = "air_quality";
    public static final String HUMIDITY = "humidity";
    public static final String TEMPERATURE = "temperature";
    public static final String BLOCKTYPE_PRE = "pre";
    public static final String BLOCKTYPE_POST = "post";
    public static final String BLOCK_IPs = "blockips";

    private static final int VISITED = 1;
    private static final int NOT_VISITED = 2;

    public static boolean isDmoatOnline = true;
    public static boolean isDmoatOffline = false;

    public String alertId = "";

    @PrimaryKey
    public String recordId = "";

    private String deviceId = "";
    private String postKey = "";
    private String nameSpace = "";
    public String blockType = "";
    private String categoryId = "";
    public String srcIp = "";
    public String srcPort = "";
    public String destPort = "";
    private String eventId = "";
    private String eveSec = "";
    private String priority = "";
    private String appearance = "";
    private String day = "";
    private String hour = "";
    public String description = "";
    private String detail = "";
    public String channel = "";
    public String ip = "";
    public String OS = "";
    public String hostname = "";
    public String timestamp = "";
    public String destIp = "";
    private String slotId = "";
    public String msg = "";
    public String category = "";
    private String direction = "";
    private String sidId = "";
    private String status = "";
    public String inputSrc = "";
    private String device_category = "";
    public String macAddress = "";
    public String reading = "";
    public String tempeature = "";
    public String airQuanlity = "";
    public String humidity = "";
    private String eveId = "";
    private String sigId = "";
    public String type = "";

    private String netmask = "";
    private String subnet = "";
    private String startIpRange = "";
    private String endIpRange = "";
    private String routerIp = "";
    private String defaultLeaseTime = "";
    private String maxLeaseTime = "";
    private String broadcast = "";
    public String hostId = "";

    // vulnerabilty
    private String date;
    public String generatedOnDateTime;
    public String accessVendor;
    public String integrityImpace;
    private String accessComplexity;
    public String score;
    public String source;
    public String authentication;
    public String availabilityImpace;
    public String confidentiallyImpact;

    private int isVisited = NOT_VISITED;

    private boolean success = false;

    public int request_id = -1;

    @Ignore
    public final ArrayList<VulnProduct> products = new ArrayList<>();

    public DMoatAlert() {

    }


    public DMoatAlert(JsonNode j) {alertId = asText("alert_id", j);
        appearance = asText("appearance", j);
        blockType = asText("blocktype", j);
        day = asText("day", j);
//        description = asText("description", j);
        description = asText("message", j);
        detail = asText("detail", j);
        destIp = asText("dest_ip", j);
        destPort = asText("dest_port", j);
        eveId = asText("eve_id", j);
        eveSec = asText("eve_sec", j);
        hour = asText("hour", j);
        inputSrc = asText("input_src", j);
        device_category = asText("device_category", j);
        msg = asText("msg", j);
        priority = asText("priority", j);
        recordId = asText("record_id", j);
        sidId = asText("sig_id", j);
        srcIp = asText("src_ip", j);
        srcPort = asText("src_port", j);


        int internetOn = asInt("on", j);
        int internetOff = asInt("off", j);
        slotId = asText("slot_id", j);
        category = asText("category", j);
        direction = asText("direction", j);
        type = asText("type", j);


        ip = asText("ip", j);
        hostname = asText("hostname", j);
        macAddress = asText("macaddress", j);
        OS = asText("OS", j);

//        destPort = asText("dest_port", j);
        nameSpace = asText("namespace", j);
        status = asText("status", j);
        reading = asText("reading", j);

        tempeature = asText("temperature", j);
        airQuanlity = asText("airpressure", j);
        humidity = asText("humidity", j);

        hostId = asText("host_id", j);
        String os = asText("os", j);
        String risk = asText("risk", j);
        String title = asText("title", j);
        String attackComplexity = asText("attack_complexity", j);
        String attackVector = asText("attack_vector", j);
        macAddress = asText("macaddress", j);
        timestamp = asText("timestamp", j);

        eventId = asText("event_id", j);
        deviceId = asText("device_id", j);
        sigId = asText("sig_id", j);
        String appId = asText("app_id", j);

        netmask = asText("netmask", j);
        subnet = asText("subnet", j);
        startIpRange = asText("start_ip_range", j);
        endIpRange = asText("end_ip_range", j);
        routerIp = asText("router_ip", j);
        defaultLeaseTime = asText("default_lease_time", j);
        maxLeaseTime = asText("max_lease_time", j);
        broadcast = asText("broadcast", j);
        String publishedDate = asText("published_date", j);

        success = asBoolean(j);
        request_id = asInt("request_id", j);

        date = asText("date", j);

        JsonNode productsJs = j.get("vuln_products");
        if (productsJs != null) {
            if (productsJs.getNodeType() == JsonNodeType.ARRAY) {
                for (int i = 0; i < productsJs.size(); i++) {
                    JsonNode node = productsJs.get(i);
                    VulnProduct product = new VulnProduct();
                    product.product = asText("Product", node);
                    product.version = asText("Version", node);
                    product.vendor = asText("Vendor", node);

                    products.add(product);
                }
            }
        }

        JsonNode impactJs = j.get("impact");
        if (impactJs != null) {
            generatedOnDateTime = asText("Generated-On-Datetime", impactJs);
            accessVendor = asText("Access-Vector", impactJs);
            integrityImpace = asText("Integrity-Impact", impactJs);
            accessComplexity = asText("Access-Complexity", impactJs);
            score = asText("Score", impactJs);
            source = asText("Source", impactJs);
            authentication = asText("Authentication", impactJs);
            availabilityImpace = asText("Availability-Impact", impactJs);
            confidentiallyImpact = asText("Confidentiality-Impact", impactJs);
        }

        JsonNode refs = j.get("references");
        if (refs != null && refs.getNodeType() == JsonNodeType.ARRAY) {
            String[] references = new String[refs.size()];
            for (int i = 0; i < refs.size(); i++) {
                JsonNode r = refs.get(i);
                references[i] = r.asText();
            }
        }
    }

    private boolean asBoolean(JsonNode node) {
        JsonNode n = node.get("success");
        if (n != null) {
            return n.asBoolean();
        }
        return false;
    }

    private String asText(String key, JsonNode node) {
        JsonNode n = node.get(key);
        if (n != null) {
            String value = n.asText();
            if (value != null) {
                return value;
            } else {
                return "";
            }
        }
        return "";
    }

    private int asInt(String key, JsonNode node) {
        JsonNode n = node.get(key);
        if (n != null) {
            return n.asInt();
        }
        return 0;
    }


    public boolean isMuted() {
        return blockType.equals("abc");
    }


    public boolean isBlocked() {
        return channel.equals("blocked_f" + UserManager.getInstance().getLoggedInUser().nameSpace);
    }

    public String getAlertId() {
        return alertId;
    }

    public void setAlertId(String alertId) {
        this.alertId = alertId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getPostKey() {
        return postKey;
    }

    public void setPostKey(String postKey) {
        this.postKey = postKey;
    }

    public String getNameSpace() {
        return nameSpace;
    }

    public void setNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }

    public String getBlockType() {
        return blockType;
    }

    public void setBlockType(String blockType) {
        this.blockType = blockType;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSrcIp() {
        return srcIp;
    }

    public void setSrcIp(String srcIp) {
        this.srcIp = srcIp;
    }

    public String getSrcPort() {
        return srcPort;
    }

    public void setSrcPort(String srcPort) {
        this.srcPort = srcPort;
    }

    public String getDestPort() {
        return destPort;
    }

    public void setDestPort(String destPort) {
        this.destPort = destPort;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEveSec() {
        return eveSec;
    }

    public void setEveSec(String eveSec) {
        this.eveSec = eveSec;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getAppearance() {
        return appearance;
    }

    public void setAppearance(String appearance) {
        this.appearance = appearance;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getOS() {
        return OS;
    }

    public void setOS(String OS) {
        this.OS = OS;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getDestIp() {
        return destIp;
    }

    public void setDestIp(String destIp) {
        this.destIp = destIp;
    }

    public String getSlotId() {
        return slotId;
    }

    public void setSlotId(String slotid) {
        this.slotId = slotid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getSidId() {
        return sidId;
    }

    public void setSidId(String sidId) {
        this.sidId = sidId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInputSrc() {
        return inputSrc;
    }

    public void setInputSrc(String inputSrc) {
        this.inputSrc = inputSrc;
    }

    public String getDevice_category() {
        return device_category;
    }

    public void setDevice_category(String device_category) {
        this.device_category = device_category;
    }


    public String getSigId() {
        return sigId;
    }

    public void setSigId(String sigId) {
        this.sigId = sigId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getReading() {
        return reading;
    }

    public void setReading(String reading) {
        this.reading = reading;
    }

    public String getTempeature() {
        return tempeature;
    }

    public void setTempeature(String tempeature) {
        this.tempeature = tempeature;
    }

    public String getAirQuanlity() {
        return airQuanlity;
    }

    public void setAirQuanlity(String airQuanlity) {
        this.airQuanlity = airQuanlity;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getEveId() {
        return eveId;
    }

    public void setEveId(String eveId) {
        this.eveId = eveId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getGeneratedOnDateTime() {
        return generatedOnDateTime;
    }

    public void setGeneratedOnDateTime(String generatedOnDateTime) {
        this.generatedOnDateTime = generatedOnDateTime;
    }

    public String getAccessVendor() {
        return accessVendor;
    }

    public void setAccessVendor(String accessVendor) {
        this.accessVendor = accessVendor;
    }

    public String getIntegrityImpace() {
        return integrityImpace;
    }

    public void setIntegrityImpace(String integrityImpace) {
        this.integrityImpace = integrityImpace;
    }

    public String getAccessComplexity() {
        return accessComplexity;
    }

    public void setAccessComplexity(String accessComplexity) {
        this.accessComplexity = accessComplexity;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public String getAvailabilityImpace() {
        return availabilityImpace;
    }

    public void setAvailabilityImpace(String availabilityImpace) {
        this.availabilityImpace = availabilityImpace;
    }

    public String getConfidentiallyImpact() {
        return confidentiallyImpact;
    }

    public String getNetmask() {
        return netmask;
    }

    public void setNetmask(String netmask) {
        this.netmask = netmask;
    }

    public String getSubnet() {
        return subnet;
    }

    public void setSubnet(String subnet) {
        this.subnet = subnet;
    }

    public String getStartIpRange() {
        return startIpRange;
    }

    public void setStartIpRange(String startIpRange) {
        this.startIpRange = startIpRange;
    }

    public String getEndIpRange() {
        return endIpRange;
    }

    public void setEndIpRange(String endIpRange) {
        this.endIpRange = endIpRange;
    }

    public String getRouterIp() {
        return routerIp;
    }

    public void setRouterIp(String routerIp) {
        this.routerIp = routerIp;
    }

    public String getDefaultLeaseTime() {
        return defaultLeaseTime;
    }

    public void setDefaultLeaseTime(String defaultLeaseTime) {
        this.defaultLeaseTime = defaultLeaseTime;
    }

    public String getMaxLeaseTime() {
        return maxLeaseTime;
    }

    public void setMaxLeaseTime(String maxLeaseTime) {
        this.maxLeaseTime = maxLeaseTime;
    }

    public String getBroadcast() {
        return broadcast;
    }

    public void setBroadcast(String broadcast) {
        this.broadcast = broadcast;
    }

    public void setConfidentiallyImpact(String confidentiallyImpact) {
        this.confidentiallyImpact = confidentiallyImpact;
    }

    public int getRequest_id() {
        return request_id;
    }

    public void setRequest_id(int request_id) {
        this.request_id = request_id;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getFormattedTimeStamp() {
        String result;
        try {
            long time = Long.parseLong(timestamp);
            time = time * 1000;
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(time);
            result = c.get(Calendar.YEAR) + "-" + c.get(Calendar.MONTH) + "-" + c.get(Calendar.DAY_OF_MONTH) +
                    " " + c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE);
        } catch (Exception e) {
            result = timestamp;
        }

        return result;
    }



    public float getHumidityValue() {
        try {
            return Float.parseFloat(this.humidity);
        } catch (NumberFormatException ignored) {
        }
        return 0;
    }

    public float getAirQualityValue() {
        try {
            return Float.parseFloat(this.airQuanlity);
        } catch (NumberFormatException ignored) {
        }
        return 0;
    }

    public float getTimeStampValue() {
        try {
            return Float.parseFloat(this.eveSec) * 1000;
        } catch (NumberFormatException ignored) {
        }
        return 0;
    }

    public static JSONObject toJSON(MutedBlockedAlertsModel alert) {
        JSONObject j = new JSONObject();
        try {
            j.put("input_src", alert.getInput_src());
            j.put("eve_sec", alert.getEve_sec());
            j.put("description", alert.getDescription());
            j.put("src_ip", alert.getSrc_ip());
            j.put("src_port", alert.getSrc_port());
            j.put("dest_ip", alert.getDesc_ip());
            j.put("dest_port", alert.getDest_port());
            j.put("record_id", alert.getRecord_id());
            j.put("blocktype", alert.getIdsBlockType());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return j;
    }

    private static JSONObject responseFlowtoJSON(MutedBlockedAlertsModel alert) {
        JSONObject j = new JSONObject();
        try {
            j.put("record_ids_list", alert.flowRecordIdsList);
            j.put("input_src", alert.getInput_src());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return j;
    }
    public static JSONObject blockIpstoJSON(MutedBlockedAlertsModel alert) {
        JSONObject j = new JSONObject();
        try {
            j.put("description", alert.getDescription());
            j.put("input_src", alert.getInput_src());
            j.put("eve_sec", alert.getEve_sec());
            j.put("ip", alert.getIp());
            j.put("record_id", alert.getRecord_id());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return j;
    }

    private static JSONObject cntdHoststoJSON(MutedBlockedAlertsModel alert) {
        JSONObject j = new JSONObject();
        try {
            j.put("description", alert.getDescription());
            j.put("input_src", alert.getInput_src());
            j.put("device_category", alert.getDeviceCategory());
            j.put("ip", alert.getIp());
            j.put("hostname", alert.getHostName());
            j.put("eve_sec", alert.getEve_sec());
            j.put("OS", alert.getOS());
            j.put("record_id", alert.getRecord_id());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return j;
    }



    public static JSONObject createResponsePayload(Context context, MutedBlockedAlertsModel alert, AlertAction action, String appId) {
        JSONObject j = new JSONObject();
        try {
            JSONObject alertJSON = new JSONObject();
            String inputSrc = alert.getInput_src();
            if (inputSrc != null && inputSrc.equals(DMoatAlert.FLOW)) {
                alertJSON = responseFlowtoJSON(alert);
            }else{

                alertJSON.put("record_id", alert.getRecord_id());
            }
// else if (inputSrc != null && inputSrc.equals(DMoatAlert.IDS)) {
//                alertJSON = toJSON(alert);
//            } else if (inputSrc != null && inputSrc.equals(DMoatAlert.CONNECTED_HOSTS)) {
//                alertJSON = cntdHoststoJSON(alert);
//            } else if (inputSrc != null && inputSrc.equals(DMoatAlert.BLOCK_IPs)){
//                    alertJSON = blockIpstoJSON(alert);
//            }


            alertJSON.putOpt("response", action.toJSON());
            j.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            j.put("request_id", Dmoat.getNewRequestId());
            j.put("app_id", appId);
            j.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            j.put("token", UserManager.getInstance().getLoggedInUser().token);
            j.put("data", alertJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("AlertActionPayload", j.toString());
        return j;
    }


    public static ArrayList<AlertAction> getIDSPreActions() {
        ArrayList<AlertAction> actions = new ArrayList<>();
        AlertAction action = new AlertAction();
        action.title = "Unblock";
        action.colour = Color.argb(255, 0, 174, 239);
        action.type = "Unblock";
        action.mode = "";
        action.interval = "";
        actions.add(action);

//        action = new AlertAction();
//        action.title = "Mute";
//        action.colour = Color.argb(255, 0, 174, 239);
//        action.type = "Mute";
//        action.mode = "";
//        action.interval = "";
//        actions.add(action);

//        action = new AlertAction();
//        action.title = "Block Once";
//        action.colour = Color.argb(255, 0, 174, 239);
//        action.type = "Block";
//        action.mode = "Once";
//        action.interval = "";
//        actions.add(action);

        action = new AlertAction();
        action.title = "Keep Block";
        action.colour = Color.argb(255, 0, 174, 239);
        action.type = "Keepblock";
        action.mode = "";
        action.interval = "";
        actions.add(action);

        action = new AlertAction();
        action.title = "Block for 15 min";
        action.colour = Color.argb(255, 0, 174, 239);
        action.type = "Block";
        action.mode = "Interval";
        action.interval = "900";
        actions.add(action);

        return actions;
    }

    public static ArrayList<AlertAction> getFlowAction() {

        ArrayList<AlertAction> actions = new ArrayList<>();
        AlertAction action = new AlertAction();

//        action = new AlertAction();
//        action.title = "Mute";
//        action.colour = Color.argb(255, 0, 174, 239);
//        action.type = "Mute";
//        action.mode = "";
//        action.interval = "";
//        actions.add(action);

        action = new AlertAction();
        action.title = "Allow";
        action.colour = Color.argb(255, 0, 174, 239);
        action.type = "Allow";
        action.mode = "";
        action.interval = "";
        actions.add(action);

//        action = new AlertAction();
//        action.title = "Block Once";
//        action.colour = Color.rgb(0, 117, 160);
//        action.type = "Block";
//        action.mode = "Once";
//        action.interval = "";
//        actions.add(action);

//        action = new AlertAction();
//        action.title = "Block Forever";
//        action.colour = Color.rgb(0, 117, 160);
//        action.type = "Block";
//        action.mode = "Forever";
//        action.interval = "";
//        actions.add(action);


        return actions;

    }

    public static ArrayList<AlertAction> getBlockIpsAction() {

        ArrayList<AlertAction> actions = new ArrayList<>();
        AlertAction action = new AlertAction();

        action = new AlertAction();
        action.title = "Allow";
        action.colour = Color.argb(255, 0, 174, 239);
        action.type = "Allow";
        action.mode = "";
        action.interval = "";
        actions.add(action);

        action = new AlertAction();
        action.title = "Keep Block";
        action.colour = Color.argb(255, 0, 174, 239);
        action.type = "Keepblock";
        action.mode = "";
        action.interval = "";
        actions.add(action);

        return actions;

    }


    public static ArrayList<AlertAction> getFlowAllAction() {

        ArrayList<AlertAction> actions = new ArrayList<>();
        AlertAction action = new AlertAction();

        action = new AlertAction();
        action.title = "Trust This Device";
        action.colour = Color.rgb(0, 117, 160);
        action.type = "Allow";
        action.mode = "Forever";
        action.interval = "";
        actions.add(action);

//        action = new AlertAction();
//        action.title = "Allow For One Hour";
//        action.colour = Color.rgb(0, 117, 160);
//        action.type = "Allow";
//        action.mode = "";
//        action.interval = "";
//        actions.add(action);

//        action = new AlertAction();
//        action.title = "Mute";
//        action.colour = Color.argb(255, 0, 174, 239);
//        action.type = "Mute";
//        action.mode = "";
//        action.interval = "";
//        actions.add(action);

//        action = new AlertAction();
//        action.title = "Block Forever";
//        action.colour = Color.rgb(0, 117, 160);
//        action.type = "Block";
//        action.mode = "Forever";
//        action.interval = "";
//        actions.add(action);


        return actions;

    }

    public static ArrayList<AlertAction> getIDSPostActions() {
        ArrayList<AlertAction> actions = new ArrayList<>();
        AlertAction action = new AlertAction();
        action.title = "Allow";
        action.colour = Color.argb(255, 0, 174, 239);
        action.type = "Allow";
        action.mode = "";
        action.interval = "";
        actions.add(action);

//        action = new AlertAction();
//        action.title = "Mute";
//        action.colour = Color.argb(255, 0, 174, 239);
//        action.type = "Mute";
//        action.mode = "";
//        action.interval = "";
//        actions.add(action);

//        action = new AlertAction();
//        action.title = "Block Once";
//        action.colour = Color.rgb(0, 117, 160);
//        action.type = "Block";
//        action.mode = "Once";
//        action.interval = "";
//        actions.add(action);

        action = new AlertAction();
        action.title = "Block Forever";
        action.colour = Color.argb(255, 0, 174, 239);
        action.type = "Block";
        action.mode = "Forever";
        action.interval = "";
        actions.add(action);

        action = new AlertAction();
        action.title = "Block for 15 min";
        action.colour = Color.argb(255, 0, 174, 239);
        action.type = "Block";
        action.mode = "Interval";
        action.interval = "900";
        actions.add(action);

        return actions;
    }

    //unmute action
    public static ArrayList<AlertAction> getUnmutePostActions() {
        ArrayList<AlertAction> actions = new ArrayList<>();
        AlertAction action = new AlertAction();
        action.title = "Allow";
        action.colour = Color.argb(255, 0, 174, 239);
        action.type = "Allow";
        action.mode = "";
        action.interval = "";
        actions.add(action);

//        action = new AlertAction();
//        action.title = "Unmute";
//        action.colour = Color.argb(255, 0, 174, 239);
//        action.type = "Unmute";
//        action.mode = "";
//        action.interval = "";
//        actions.add(action);

//        action = new AlertAction();
//        action.title = "Block Once";
//        action.colour = Color.rgb(0, 117, 160);
//        action.type = "Block";
//        action.mode = "Once";
//        action.interval = "";
//        actions.add(action);

        action = new AlertAction();
        action.title = "Block Forever";
        action.colour = Color.argb(255, 0, 174, 239);
        action.type = "Block";
        action.mode = "Forever";
        action.interval = "";
        actions.add(action);


        return actions;
    }


    //unmute action
    public static ArrayList<AlertAction> getUnBlockPostActions() {
        ArrayList<AlertAction> actions = new ArrayList<>();
        AlertAction action = new AlertAction();
        action.title = "Allow";
        action.colour = Color.argb(255, 0, 174, 239);
        action.type = "Allow";
        action.mode = "";
        action.interval = "";
        actions.add(action);

        action = new AlertAction();
        action.title = "Unblock";
        action.colour = Color.argb(255, 0, 174, 239);
        action.type = "Unblock";
        action.mode = "";
        action.interval = "";
        actions.add(action);


        return actions;
    }

    public static String getResponseUrl(MutedBlockedAlertsModel alert) {
        String inputSrc = alert.getInput_src();
        if (inputSrc != null && inputSrc.equals(DMoatAlert.FLOW)) {
            return DashBoardActivity.BASE_URL+ Api.RESPONSE_FLOW_URL;
        } else if (inputSrc != null && inputSrc.equals(DMoatAlert.IDS)) {
            return DashBoardActivity.BASE_URL+Api.RESPONSE_IDS_URL;
        } else if (inputSrc != null && inputSrc.equals(DMoatAlert.CONNECTED_HOSTS)) {
            return DashBoardActivity.BASE_URL+Api.RESPONSE_CONNECTED_HOSTS_URL;
        }
        else if (inputSrc != null && inputSrc.equals(DMoatAlert.BLOCK_IPs)){
            return DashBoardActivity.BASE_URL+Api.RESPONSE_BLOCK_IPS_URL;
        }
        return null;
    }



    private int getIsVisited() {
        return isVisited;
    }

    public void setIsVisited(int isVisited) {
        this.isVisited = isVisited;
    }


    public boolean isVistedByUser() {
        return this.getIsVisited() == VISITED;
    }

    public String getFormattedEveSecTimeStamp() {
        try {
            return getrTimeStamp();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return eveSec;
    }

    private String getrTimeStamp() throws Exception {
//        Log.d("dmoatAlertEveSec", eveSec);
        float fTime = Float.parseFloat(eveSec);
        long time = (long) fTime;
        time = time * 1000;
        Date date = new Date(time);
//        Calendar c = Calendar.getInstance();
//        c.setTimeInMillis(time);
//        String source = c.get(Calendar.YEAR) + "-" + c.get(Calendar.MONTH) + "-" + c.get(Calendar.DAY_OF_MONTH) +
//                " " + c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) + ":00";

//        SimpleDateFormat sourceFormat = new SimpleDateFormat("MMM-dd-yyyy h:mm:ss");
        SimpleDateFormat sourceFormat = new SimpleDateFormat("MMM-dd-yyyy h:mm a", Locale.getDefault());
//        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateString = sourceFormat.format(date); // => Date is in UTC now
        Date parsed = sourceFormat.parse(dateString); // => Date is in UTC now

        SimpleDateFormat destFormat = new SimpleDateFormat("MMM-dd-yyyy h:mm a", Locale.getDefault());
//        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        destFormat.setTimeZone(Calendar.getInstance().getTimeZone());
//        destFormat.setTimeZone(TimeZone.getDefault());
        return destFormat.format(parsed);
    }


    //time conversion by passsing time
    //eve sec to time stamp
    public String getFormattedTimeStamp(int es) {
        try {
            return getrTimeStamp(es);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return eveSec;
    }

    private static String getrTimeStamp(int s) throws Exception {
        float fTime = Float.parseFloat(String.valueOf(s));
        long time = (long) fTime;
        time = time * 1000;
        Date date = new Date(time);
        SimpleDateFormat sourceFormat = new SimpleDateFormat("MMM-dd-yyyy h:mm a", Locale.getDefault());
//        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateString = sourceFormat.format(date); // => Date is in UTC now
        Date parsed = sourceFormat.parse(dateString); // => Date is in UTC now

        SimpleDateFormat destFormat = new SimpleDateFormat("MMM-dd-yyyy h:mm a", Locale.getDefault());
//        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        destFormat.setTimeZone(Calendar.getInstance().getTimeZone());
//        destFormat.setTimeZone(TimeZone.getDefault());
        return destFormat.format(parsed);
    }

    public boolean is24HoursOldAlert() {

        try {
            float fTime = Float.parseFloat(eveSec);
            long time = (long) fTime;
            time = time * 1000;
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(time);

            Calendar now = Calendar.getInstance();
            now.add(Calendar.DAY_OF_MONTH, -1);

            if (calendar.before(now)) {
                return true;
            }

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return false;
    }

    //date order
    private static class DateComparator implements Comparator<DMoatAlert> {

        @Override
        public int compare(DMoatAlert dMoatAlert, DMoatAlert t1) {
            Double distance = Double.valueOf(dMoatAlert.eveSec);
            Double distance1 = Double.valueOf(t1.eveSec);
            return Integer.compare(distance1.compareTo(distance), 0);
        }
    }
}
