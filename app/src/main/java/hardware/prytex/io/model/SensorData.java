package hardware.prytex.io.model;

import com.fasterxml.jackson.databind.JsonNode;

import io.realm.RealmObject;

/**
 * Created by khawarraza on 04/11/2016.
 */

public class SensorData extends RealmObject {

    public String tempeature;
    public String humidity;
    public String airQuanlity;
    public String timestamp;

    public SensorData() {

    }


}
