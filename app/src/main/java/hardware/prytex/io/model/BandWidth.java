package hardware.prytex.io.model;

/**
 * Created by macbookpro13 on 08/05/2017.
 */

public class BandWidth {
    private int id;
    private String hostIP;
    private String hostID;
    private String hostName;
    private String ProtocolsList;
    private float totalUsage;
    private String protocolName;
    private String protocolValue;
    private String hostOS;
    private boolean isCurrent;
    private String[] listOfProtocols;

    private boolean isInPolicy;

    public String getHostOS() {
        return hostOS;
    }

    public void setHostOS(String hostOS) {
        this.hostOS = hostOS;
    }


    public String getHostIP() {
        return hostIP;
    }

    public void setHostIP(String hostIP) {
        this.hostIP = hostIP;
    }

    public void setHostID(String hostID) {
        this.hostID = hostID;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getProtocolsList() {
        return ProtocolsList;
    }

    public void setProtocolsList(String protocolsList) {
        ProtocolsList = protocolsList;
    }

    public float getTotalUsage() {
        return totalUsage;
    }

    public void setTotalUsage(float totalUsage) {
        this.totalUsage = totalUsage;
    }

    public boolean isCurrent() {
        return isCurrent;
    }

    public void setCurrent(boolean current) {
        isCurrent = current;
    }

}
