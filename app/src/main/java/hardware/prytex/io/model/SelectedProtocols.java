package hardware.prytex.io.model;

/**
 * Created by macbookpro13 on 08/06/2017.
 */

public class SelectedProtocols {
    private int id;
    private String protocolKey;
    private String protocolValue;

    public void setId(int id) {
        this.id = id;
    }

    public String getProtocolKey() {
        return protocolKey;
    }

    public void setProtocolKey(String protocolKey) {
        this.protocolKey = protocolKey;
    }

    public void setProtocolValue(String protocolValue) {
        this.protocolValue = protocolValue;
    }

}
