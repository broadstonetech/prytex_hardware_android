package hardware.prytex.io.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.realm.RealmObject;

/**
 * Created by UsmanArshad on 22/12/2016.
 */
@JsonIgnoreProperties({"index"})
public class Protocol extends RealmObject {

//    public int index;
    public String name;

    public Protocol(){}

    /*    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
