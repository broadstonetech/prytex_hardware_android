package hardware.prytex.io.model;

import androidx.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by usmanarshad on 30/1/2017.
 */

@JsonIgnoreProperties({"valid", "loaded"})
public class NetConfig  {

    public String subnet;
    private String iface;
    public String ip;
    private String app_id;
    public String start_ip_range;
    public String broadcast;
    public String netmask;
    public String router_ip;
    private String request_id;
    public String end_ip_range;

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    private String gateway;
    private String type;
    public int max_lease_time, default_lease_time;

    public NetConfig(){}

    public NetConfig(String subnet, String iface, int max_lease_time, String ip, String start_ip_range, String broadcast, String netmask, String router_ip, int default_lease_time, String end_ip_range, String type) {
        this.subnet = subnet;
        this.iface = iface;
        this.max_lease_time = max_lease_time;
        this.ip = ip;
        this.start_ip_range = start_ip_range;
        this.broadcast = broadcast;
        this.netmask = netmask;
        this.router_ip = router_ip;
        this.default_lease_time = default_lease_time;
        this.end_ip_range = end_ip_range;
        this.type = type;
    }

    public String getSubnet() {
        return subnet;
    }

    public void setSubnet(String subnet) {
        this.subnet = subnet;
    }

    public String getIface() {
        return iface;
    }

    public void setIface(String iface) {
        this.iface = iface;
    }

    public int getMax_lease_time() {
        return max_lease_time;
    }

    public void setMax_lease_time(int max_lease_time) {
        this.max_lease_time = max_lease_time;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getStart_ip_range() {
        return start_ip_range;
    }

    public void setStart_ip_range(String start_ip_range) {
        this.start_ip_range = start_ip_range;
    }

    public String getBroadcast() {
        return broadcast;
    }

    public void setBroadcast(String broadcast) {
        this.broadcast = broadcast;
    }

    public String getNetmask() {
        return netmask;
    }

    public void setNetmask(String netmask) {
        this.netmask = netmask;
    }

    public String getRouter_ip() {
        return router_ip;
    }

    public void setRouter_ip(String router_ip) {
        this.router_ip = router_ip;
    }

    public int getDefault_lease_time() {
        return default_lease_time;
    }

    public void setDefault_lease_time(int default_lease_time) {
        this.default_lease_time = default_lease_time;
    }

    public String getEnd_ip_range() {
        return end_ip_range;
    }

    public void setEnd_ip_range(String end_ip_range) {
        this.end_ip_range = end_ip_range;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @NonNull
    @Override
    public String toString() {
        return "NetConfig{" +
                "subnet='" + subnet + '\'' +
                ", iface='" + iface + '\'' +
                ", max_lease_time='" + max_lease_time + '\'' +
                ", ip='" + ip + '\'' +
                ", app_id='" + app_id + '\'' +
                ", start_ip_range='" + start_ip_range + '\'' +
                ", broadcast='" + broadcast + '\'' +
                ", netmask='" + netmask + '\'' +
                ", router_ip='" + router_ip + '\'' +
                ", request_id='" + request_id + '\'' +
                ", default_lease_time='" + default_lease_time + '\'' +
                ", end_ip_range='" + end_ip_range + '\'' +
                ", gateway='" + gateway + '\'' +
                ", type='" + type + '\'' +
                '}';

//            new NetConfig( subnet,  iface,  max_lease_time,  ip,  start_ip_range,  broadcast,  netmask,  router_ip,  default_lease_time,  end_ip_range,  type);


    }
}
