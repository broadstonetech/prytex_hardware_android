package hardware.prytex.io.util;

import hardware.prytex.io.fragment.CreateNewPolicy;
import hardware.prytex.io.model.DMoatAlert;
import hardware.prytex.io.model.DmoatPolicy;
import hardware.prytex.io.model.Protocol;
import hardware.prytex.io.model.TargetUrl;
import hardware.prytex.io.model.WeekDay;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by usman on 12/28/16.
 */

@SuppressWarnings("ALL")
public class DmoatUtils {

    public static String[] PROTOCOLS_KEYS, PROTOCOLS_VALUE;       // It will be initialized in Policies Fragment
    public static final HashMap<String, String> PROTOCOLS = new HashMap<>();       // It will be initialized in Policies Fragment

    /*public static String getWeekDaysFromIndex(DmoatPolicy policy){
        String weekDays = "";
        for (int i = 0; i < policy.getWeekDays().size(); i++){
            if (i < policy.getWeekDays().size() - 2)
                weekDays = weekDays + CreateNewPolicy.WEEK_DAYS[policy.getWeekDays().get(i).getIndex()] + ", ";
            else if (i < policy.getWeekDays().size() - 1)
                weekDays = weekDays + CreateNewPolicy.WEEK_DAYS[policy.getWeekDays().get(i).getIndex()] + " and ";
            else
                weekDays = weekDays + CreateNewPolicy.WEEK_DAYS[policy.getWeekDays().get(i).getIndex()];
        }
        return weekDays;
    }*/

    public static String getSelectedWeekDays(int[] indexes){
        StringBuilder weekDays = new StringBuilder();
        ArrayList<WeekDay> days = new ArrayList<>();
        for (int i = 0; i < indexes.length; i++){
            if (indexes[i] == 1){
                WeekDay wd = new WeekDay();
                wd.setIndex(i);
                days.add(wd);
            }
        }
        for (int i = 0; i < days.size(); i++){
            if (i < days.size() - 2)
                weekDays.append(CreateNewPolicy.WEEK_DAYS[days.get(i).getIndex()]).append(", ");
            else if (i < days.size() - 1)
                weekDays.append(CreateNewPolicy.WEEK_DAYS[days.get(i).getIndex()]).append(" and ");
            else
                weekDays.append(CreateNewPolicy.WEEK_DAYS[days.get(i).getIndex()]);
        }
        return weekDays.toString();
    }

    public static String getProtocols(DmoatPolicy policy){
        StringBuilder protocols = new StringBuilder();
        for (int i = 0; i < policy.getProtocols().size(); i++){
            if (i < policy.getProtocols().size() - 2)
                protocols.append(getCorrectProtocolName(policy, i)).append(", ");
            else if (i < policy.getProtocols().size() - 1)
                protocols.append(getCorrectProtocolName(policy, i)).append(" and ");
            else
                protocols.append(getCorrectProtocolName(policy, i));
        }
        return protocols.toString();
    }

    private static String getCorrectProtocolName(DmoatPolicy policy, int i){
        return PROTOCOLS.get(policy.getProtocols().get(i).getName()) == null ? PROTOCOLS_VALUE[i] : PROTOCOLS.get
                (policy.getProtocols().get(i).getName());
    }

    public static int[] getDays(int weight){
        int[] indexes = {0, 0, 0, 0, 0, 0, 0};
        int j = 0;
        for (int i = 6; i >= 0; i--) {
            int result = (int) Math.pow(2, i);
            if (weight >= result){
                weight = weight - result;
                indexes[j] = 1;
            }
            j++;
        }
        return indexes;
    }

    public static long ipToLong(InetAddress ip) {
        byte[] octets = ip.getAddress();
        long result = 0;
        for (byte octet : octets) {
            result <<= 8;
            result |= octet & 0xff;
        }
        return result;
    }


    public static String getDayFromSlot(int slot){
        if (slot == 0){
            return "Monday";
        }else if (slot == 1){
            return "Tuesday";
        }else if (slot == 2){
            return "Wednesday";
        }else if (slot == 3){
            return "Thursday";
        }else if (slot == 4){
            return "Friday";
        }else if (slot == 5){
            return "Saturday";
        }else if (slot == 6){
            return "Sunday";
        }
        return "";
    }

    public static String gethourFromSlot(int slot){
        if (slot == 0){
            return "12am-1am";
        }else if (slot == 1){
            return "1am-2am";
        }else if (slot == 2){
            return "2am-3am";
        }else if (slot == 3){
            return "3am-4am";
        }else if (slot == 4){
            return "4am-5am";
        }else if (slot == 5){
            return "5am-6am";
        }else if (slot == 6){
            return "6am-7am";
        }else if (slot == 7){
            return "7am-8am";
        }else if (slot == 8){
            return "8am-9am";
        }else if (slot == 9){
            return "9am-10am";
        }else if (slot == 10){
            return "10am-11am";
        }else if (slot == 11){
            return "11am-12Pm";
        }else if (slot == 12){
            return "12Pm-1Pm";
        }else if (slot == 13){
            return "1Pm-2Pm";
        }else if (slot == 14){
            return "2Pm-3Pm";
        }else if (slot == 15){
            return "3Pm-4Pm";
        }else if (slot == 16){
            return "4Pm-5Pm";
        }else if (slot == 17){
            return "5Pm-6Pm";
        }else if (slot == 18){
            return "6Pm-7Pm";
        }else if (slot == 19){
            return "7Pm-8Pm";
        }else if (slot == 20){
            return "8Pm-9Pm";
        }else if (slot == 21){
            return "9Pm-10Pm";
        }else if (slot == 22){
            return "10Pm-11Pm";
        }else if (slot == 23){
            return "11Pm-12Am";
        }
        return "";
    }

    // method for selecting environment

}
