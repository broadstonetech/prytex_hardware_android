package hardware.prytex.io.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import hardware.prytex.io.config.Constants;


public class SPManager {

    private static final String TERM_AND_SERVICES = "check";
    private static final String CurrentSensorData = "current";
    private static final String CurrentAppModeDev = "dev";


    public static void setTermsServicesStatus(Context context, boolean check) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
//        editor.putString("APP_ID", Dmoat.getAppId());
        editor.putBoolean(TERM_AND_SERVICES, check);
        editor.apply();
    }

    public static void setDeviceToken(Context context, String token) {
        SharedPreferences preferences =  PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.FCM_TOKEN, token);

//        editor.commit();
        editor.apply();

    }

    public static String getDeviceToken(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(Constants.FCM_TOKEN, null);
    }
}
