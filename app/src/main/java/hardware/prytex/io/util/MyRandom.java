package hardware.prytex.io.util;

import java.util.Random;

/**
 * Created by usman on 1/4/17.
 */

public class MyRandom extends Random {
    public MyRandom() {}

    public int nextNonNegative() {
        return next(Integer.SIZE - 1);
    }
}
