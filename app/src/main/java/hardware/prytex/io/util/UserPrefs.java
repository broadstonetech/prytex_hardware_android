package hardware.prytex.io.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import hardware.prytex.io.R;

public class UserPrefs {
    public static int getTheme(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return Integer.parseInt(sharedPreferences.getString(context.getString(R.string.theme_key),context.getString(R.string.theme_default)));
    }

    public static void setTheme(Context context, int value){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(context.getString(R.string.theme_key),value+"").apply();
    }
    public static void setDarkTheme(Context context, String theme){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString("theme",theme).apply();
    }

    public static String getDarkTheme(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString("theme","light");
    }

}
