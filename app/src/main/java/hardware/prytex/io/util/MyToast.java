package hardware.prytex.io.util;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Khawar on 3/16/2016.
 */
public class MyToast {

    public static void showMessage(Context mContext, String text) {
        Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
    }
}
