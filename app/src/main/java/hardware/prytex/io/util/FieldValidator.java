package hardware.prytex.io.util;

import android.util.Patterns;
import android.widget.EditText;

import java.util.regex.Pattern;


public class FieldValidator {

    private static final String FIRST_IP_ADDRESS_PATTERN = "^192\\.168\\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2" +
            "([0-4][0-9]|5[0-5]))\\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))$";
    private static final String SECOND_IP_ADDRESS_PATTERN = "^172\\.(1[6-9]|2[0-9]|3[0-1])\\.([0-9]|[1-9][0-9]|1" +
            "([0-9][0-9])|2([0-4][0-9]|5[0-5]))\\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))$";
    private static final String THIRD_IP_ADDRESS_PATTERN = "^10\\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2" +
            "([0-4][0-9]|5[0-5]))\\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))\\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))$";
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    // Error Messages
    private static final String REQUIRED_MSG = "Required";
    private static final String INVALID_IP_ADDRESS_MSG = "Invalid IP Address";
    private static final String EMAIL_MSG = "Invalid email";
    private static final String LENGTH_MSG = "Password must contain atleast six characters!";
    private static final String LEASE_TIME_ERROR_MSG = "Default lease time should be less than maximum lease time!";

    private static final Pattern firstPattern = Pattern.compile(FIRST_IP_ADDRESS_PATTERN);
    private static final Pattern secondPattern = Pattern.compile(SECOND_IP_ADDRESS_PATTERN);
    private static final Pattern thirdPattern = Pattern.compile(THIRD_IP_ADDRESS_PATTERN);


    private static boolean isValid(EditText editText, boolean required) {
        String text = editText.getText().toString().trim();
        editText.setError(null);
        if (required && !hasText(editText)) return false;
        if (required && !Pattern.matches(FieldValidator.EMAIL_REGEX, text)) {
            editText.setError(FieldValidator.EMAIL_MSG);
            return false;
        }
        return true;
    }

    private static boolean hasText(EditText editText) {
        String text = editText.getText().toString().trim();
        editText.setError(null);
        if (text.length() == 0) {
            editText.setError(REQUIRED_MSG);
            return false;
        }
        return true;
    }


    public static boolean isValidFieldData(String editText, EditText et, String errorMessage) {
        String text = editText.trim();
        et.setError(null);
        if (text.length() == 0) {
            et.setError(errorMessage);
            return false;
        } else
            return isValidIPAddress(text, et);
    }

    private static boolean isValidIPAddress(String text, EditText editText){
        if (!Patterns.IP_ADDRESS.matcher(text).matches()) {
            editText.setError(INVALID_IP_ADDRESS_MSG);
            return false;
        }
        return true;
    }

    public static boolean validLeaseTime(EditText editText1, EditText editText2){
        if (hasText(editText1) && hasText(editText2)){
            if (Integer.parseInt(editText1.getText().toString()) > Integer.parseInt(editText2.getText().toString())) {
                editText1.setError(LEASE_TIME_ERROR_MSG);
                return false;
            }
        }
        return true;
    }

    public static boolean ipComparsionValidity(EditText firstIp,EditText firstIpB, EditText secondIp,EditText secondIpB, String errorMessage){
        String ipA = firstIp.getText().toString().trim() + firstIpB.getText().toString().trim();
        String ipB = secondIp.getText().toString().trim() + secondIpB.getText().toString().trim();
        String[] ipTokens = ipA.split("\\.");
        String[] startIPTokens = ipB.split("\\.");

        if (Integer.parseInt(ipTokens[1]) > Integer.parseInt(startIPTokens[1])){
            firstIpB.setError(errorMessage);
            return false;
        } else if (Integer.parseInt(ipTokens[1]) == Integer.parseInt(startIPTokens[1])) {
            if (Integer.parseInt(ipTokens[2]) > Integer.parseInt(startIPTokens[2])){
                firstIpB.setError(errorMessage);
                return false;
            } else if (Integer.parseInt(ipTokens[2]) == Integer.parseInt(startIPTokens[2])){
                if (Integer.parseInt(ipTokens[3]) > Integer.parseInt(startIPTokens[3]) || Integer.parseInt
                        (ipTokens[3]) == Integer.parseInt(startIPTokens[3])){
                    firstIpB.setError(errorMessage);
                    return false;
                }
            }
        }
        return true;
    }


    public static boolean isPrivateIP(EditText editText, EditText editTextB){
        String ip = editText.getText().toString().trim() + editTextB.getText().toString().trim();
//        String ipB = editTextB.getText().toString().trim();
        if (firstPattern.matcher(ip).matches() || secondPattern.matcher(ip).matches() || thirdPattern.matcher(ip)
                .matches()) {
            editTextB.setError(null);
            return true;
        } else {
            editTextB.setError("IP Address must be private");
            return false;
        }
    }

}
