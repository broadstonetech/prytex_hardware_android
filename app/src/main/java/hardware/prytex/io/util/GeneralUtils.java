package hardware.prytex.io.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.EditText;

import androidx.appcompat.widget.SearchView;

import hardware.prytex.io.R;
import hardware.prytex.io.adapter.BandwidthAdapter;
import hardware.prytex.io.model.BandWidth;
import hardware.prytex.io.model.PolicyPlanModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by abubaker on 5/25/16.
 */
@SuppressWarnings("ALL")
public class GeneralUtils {


    public static boolean isConnected(Context context) {

        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }


    public static ArrayList<BandWidth> getBandwidthData(String constraint, ArrayList<BandWidth> hostsList) {
        ArrayList<BandWidth> results = new ArrayList<>();
        for (BandWidth item : hostsList) {
            if (item.getHostName().toLowerCase().contains(constraint)){
                results.add(item);
            }
        }
        return results;
    }

    public static void searchBandwidth(SearchView searchView, BandwidthAdapter mAdapter) {
        View closeButton = searchView.findViewById(R.id.search_close_btn);

        EditText et= (EditText) searchView.findViewById(R.id.search_src_text);
        et.setHint("Search...");

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.onActionViewCollapsed();
            }
        });

        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.onActionViewExpanded();
            }
        });
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    closeButton.performClick();
                }
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                mAdapter.getFilter().filter(query);
                return false;
            }
        });

    }

    public static ArrayList<PolicyPlanModel> getFilteredResults(String constraint,ArrayList<PolicyPlanModel> list) {
        ArrayList<PolicyPlanModel> results = new ArrayList<>();

        for (PolicyPlanModel item : list) {
            if (item.getPolicyTitle().toLowerCase().contains(constraint)) {
                results.add(item);
            }
        }
        return results;
    }

    public static String getDeviceID(Context context){
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }


    public static float celciusToFahrenheit(float value) {
        return 32 + (value * 9 / 5);
    }

    public static boolean dateComparison(String startDate, String endDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
        int d_compare = 0;
        try {
            Date d_min = sdf.parse(startDate);
            Date d_max = sdf.parse(endDate);
            d_compare = d_max.compareTo(d_min);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d_compare > 0;
    }

    public static void ScaleInAnimation(Context context,View view){
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.scale_in);
        animation.setDuration(300);
        view.startAnimation(animation);
//        try {
//            Thread.sleep(300);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

}