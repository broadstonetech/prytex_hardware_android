package hardware.prytex.io.network;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;

public class CacheManager {

	private static final String CACHE_KEY = "perm_cache_key";
	
	private static CacheManager instance = null;
	
	private final HashMap<String, String> map;
	private final SharedPreferences prefs;
	private final Editor editor;
	
	@SuppressLint("CommitPrefEdits")
	private CacheManager(Context context) {
		Context context1 = context.getApplicationContext();
		map = new HashMap<>();
		prefs = context.getSharedPreferences(CACHE_KEY, Context.MODE_PRIVATE);
		editor = prefs.edit();
		
		init();
	}
	
	@SuppressWarnings("unchecked")
	private void init() {
		HashMap<String, ?> temp = (HashMap<String, ?>) prefs.getAll();
		for (String key : temp.keySet()) {
			if (temp.get(key) instanceof String) {
				map.put(key, (String) temp.get(key));
			}
		}
	}

    public static CacheManager getInstance() {
		return instance;
	}
	
	public void addToCache(String key, String value) {
		map.put(key, value);
		editor.putString(key, value);
		editor.commit();
	}
	
	public String obtain(String key) {
		return map.get(key);
	}

}
