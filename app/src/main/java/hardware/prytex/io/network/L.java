package hardware.prytex.io.network;

import android.util.Log;


@SuppressWarnings("ALL")
public class L {

	private static final String TAG = "DMOAT";
	
	private static final int DEBUG_MODE = 0;
    private static final int DEVELOPMENT_MODE = 2;
	
	private static final int MODE = DEVELOPMENT_MODE;
	private static final boolean FB = false;
	
	public static void d(String msg) {
		if(MODE == DEBUG_MODE || MODE == DEVELOPMENT_MODE) {
			Log.d(TAG, TAG + "#> " + msg);
		}
	}

}
