package hardware.prytex.io.network;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import hardware.prytex.io.activity.AuthenticationActivity;
import hardware.prytex.io.util.MyToast;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import hardware.prytex.io.activity.DashBoardActivity;

public class HttpAsyncRequest {

    private static final AsyncHttpClient client = new AsyncHttpClient();

    private final AsyncTaskListener mTaskListener;
    private BaseParser mParser;
    private CacheOptions mCacheOptions;
    private final String mUrl;
    private final Context mContext;
    private final RequestType type;
    private RequestParams params;
    private ArrayList<KeyValue> headers;
    private JSONObject jsonData;

    public HttpAsyncRequest(Context mContext, String url, RequestType type,
                            BaseParser parser, AsyncTaskListener listener) {
        this.mContext = mContext;
        this.mUrl = url;
        this.type = type;
        this.mParser = parser;
        this.mTaskListener = listener;
        this.params = new RequestParams();
        this.headers = new ArrayList<>();
    }


    public void setJsonData(JSONObject jsonData) {
        this.jsonData = jsonData;
    }

    public void execute() {
        String data = null;
        if (mCacheOptions != null && mCacheOptions.shouldCache()) {
            data = lookIntoCache();
        }
        if (!isNetworkConnected(mContext)) {
            if (data == null) {
                noInternetConnection();
                return;
            }
        }
        if (data != null) {
            L.d("HTTP_TAG-> URL: " + mUrl);
            L.d("HTTP_TAG-> **FOUND IN CACHE**");
            L.d("HTTP_TAG-> " + data);
            TaskResult result = mParser.parse(200, data);
            mTaskListener.onComplete(result);
        } else {
            if (type == RequestType.GET) {
                client.get(mUrl, params, responseHandler);

            } else if (type == RequestType.POST) {
                Header[] h = new Header[headers.size()];
                for (int i = 0; i < headers.size(); i++) {
                    h[i] = new BasicHeader(headers.get(i).key, headers.get(i).value);
                }
                client.post(mContext, mUrl, h, params, null, responseHandler);
//				client.post(mContext, mUrl, params, responseHandler);
            } else if (type == RequestType.JSONDATA) {

                StringEntity be = null;
                try {
                    be = new StringEntity(jsonData.toString());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
//                client.setTimeout(180 * 1000);
                client.post(null, mUrl, be, "application/json", responseHandler);
            }
        }
    }

    private String lookIntoCache() {
        if (mCacheOptions.getType() == CacheOptions.CacheType.PREFS) {
            return CacheManager.getInstance().obtain(mCacheOptions.getKey());
        } else if (mCacheOptions.getType() == CacheOptions.CacheType.TEMP) {
            return TempCacheManager.getInstance().get(mCacheOptions);
        }
        return null;
    }


    private final AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {
        public void onSuccess(int code, Header[] headers, byte[] data) {
            if (mTaskListener == null) {
                return;
            }
            String serverResponse = new String(data);
            L.d("HTTP_TAG-> URL: " + mUrl);
            L.d("HTTP_TAG-> " + serverResponse);
            TaskResult result = mParser.parse(code, serverResponse);
            mTaskListener.onComplete(result);
            if (mCacheOptions != null && mCacheOptions.shouldCache() && result.isSuccess()) {
                if (mCacheOptions.getType() == CacheOptions.CacheType.PREFS) {
                    CacheManager.getInstance().addToCache(mCacheOptions.getKey(), serverResponse);
                } else if (mCacheOptions.getType() == CacheOptions.CacheType.TEMP) {
                    TempCacheManager.getInstance().addToCache(mCacheOptions.getKey(), serverResponse);
                }
            }
        }

        public void onFailure(int arg0, Header[] arg1, byte[] data, Throwable arg3) {
            if (mTaskListener != null) {
                if (data == null) {
                    TaskResult result = new TaskResult();
                    mTaskListener.onComplete(result);
                    return;
                }
                TaskResult result = mParser.parse(arg0, new String(data));
                if (result.code == 401) {
                    MyToast.showMessage(mContext, "Your session has been expired");

                    Log.d("nameSpaceLogOut", DashBoardActivity.usernameSpacePN);
                    //PubNubDataHandler.unsubPushNotificaations();
                    Intent i = AuthenticationActivity.startNewIntent(mContext);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(i);
                    ((Activity)mContext).finish();
                    return;
                }
//				result.message = new String(data);
                result.code = arg0;
                L.d("* " + mUrl + "*\n" + result.message);
                mTaskListener.onComplete(result);
            }
        }

    };

    public enum RequestType {
        GET, POST, JSONDATA
    }

    private void noInternetConnection() {
        TaskResult result = new TaskResult();
        result.code = TaskResult.CODE_NO_INTERNET_CONNECTION;
        result.message = TaskResult.MSG_NO_INTERNET_CONNECTION;
        mTaskListener.onComplete(result);
        return;
    }

    private boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        // There are no active networks.
        return ni != null;
    }

    static class KeyValue {
        final String key;
        final String value;

        KeyValue(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }


}