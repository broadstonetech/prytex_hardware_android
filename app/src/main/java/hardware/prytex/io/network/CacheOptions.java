package hardware.prytex.io.network;

public interface CacheOptions {

	String getKey();
	boolean shouldCache();
	int getCacheTimeOut();
	CacheType getType();
	
	
	enum CacheType {
		PREFS, TEMP
	}
	
}
