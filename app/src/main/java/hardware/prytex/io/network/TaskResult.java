package hardware.prytex.io.network;

public class TaskResult {

    private static final int CODE_FAILURE = 1;
    private static final int CODE_SUCCESS = 2;
    public static final int CODE_NO_INTERNET_CONNECTION = 4;
    public static final String ERROR_TEXT_503 = "Error code 503 received from server. Please report this error if you contact support.";

    public static final String MSG_NO_INTERNET_CONNECTION = "Prytex Hardware is unable to reach the Internet. Please check your network connection and try again.";

    private Object data;
    public String message = "";
    public String product_identifier;
    public String chatRestoreID = "unknown";
    public String NetConfigType;
    public String userEmailForDiscovery;
    public int code = CODE_FAILURE;


    public boolean isSuccess() {
        return code == CODE_SUCCESS;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public void success(boolean b) {
        if (b) {
            code = CODE_SUCCESS;
        } else {
            code = CODE_FAILURE;
        }
    }

    public Object getData() {
        return data;
    }
}
