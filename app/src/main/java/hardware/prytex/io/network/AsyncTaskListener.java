package hardware.prytex.io.network;



public interface AsyncTaskListener {

	void onComplete(TaskResult result);
	
}
