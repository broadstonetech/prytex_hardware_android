package hardware.prytex.io.network;

public interface BaseParser {

	int SUCCESS = 200;

    TaskResult parse(int httpCode, String response);
}
