package hardware.prytex.io.network;

import android.content.Context;
import android.util.Log;

import hardware.prytex.io.parser.AccountSetup;
import hardware.prytex.io.parser.AckUiRequest;
import hardware.prytex.io.parser.AlertsDashboardParser;
import hardware.prytex.io.parser.AuthenticationRequest;
import hardware.prytex.io.parser.BandwidthDashboardParser;
import hardware.prytex.io.parser.BandwidthParser;
import hardware.prytex.io.parser.BeaconRequest;
import hardware.prytex.io.parser.BlockedAlertParser;
import hardware.prytex.io.parser.ConnectedDeviceParser;
import hardware.prytex.io.parser.ConnectedDevicesDashboardParser;
import hardware.prytex.io.parser.ConnectedDevicesParser;
import hardware.prytex.io.parser.CreatePolicyParser;
import hardware.prytex.io.parser.FrequentProtocols;
import hardware.prytex.io.parser.InitSensorRequest;
import hardware.prytex.io.parser.LedConfigParser;
import hardware.prytex.io.parser.ModifyPassword;
import hardware.prytex.io.parser.MutedAlertsParser;
import hardware.prytex.io.parser.NetConfigParser;
import hardware.prytex.io.parser.NetModeTypeParser;
import hardware.prytex.io.parser.OtaVersionParser;
import hardware.prytex.io.parser.PauseInternetParser;
import hardware.prytex.io.parser.PoliciesListParser;
import hardware.prytex.io.parser.PolicyDurationParser;
import hardware.prytex.io.parser.PotocolPoliciesParser;
import hardware.prytex.io.parser.Pre_MutedAlertsParser;
import hardware.prytex.io.parser.PushNotificationsParser;
import hardware.prytex.io.parser.SensorDashboardParser;
import hardware.prytex.io.parser.SensorDataParser;
import hardware.prytex.io.parser.UnmuteUnblockParser;
import hardware.prytex.io.parser.UserLogin;
import hardware.prytex.io.parser.VerifyPasswordCode;
import hardware.prytex.io.parser.ViewAlersParser;
import hardware.prytex.io.parser.VulnerabilitiesListParser;

import org.json.JSONObject;

import hardware.prytex.io.activity.DashBoardActivity;

import static hardware.prytex.io.adapter.ConnectedHostsAdapter.isChangeOS;
import static hardware.prytex.io.adapter.ConnectedHostsAdapter.isChangeUserName;


/**
 * Created by abubaker on 5/20/16.
 */
public  class Api {

    //    public static final String BASE_URL = "http://192.241.190.107:5005";
  //  public static  String BASE_URL = "https://api.dmoat.com";
      // public static  String BASE_URL = "https://dev.dmoat.com"; //development


    private static final String USER_LOGIN = "/userlogin";
    private static final String LOGOUT_URL =  "/signout";
    public static final String RESPONSE_FLOW_URL =  "/response/flow";
    public static final String RESPONSE_BLOCK_IPS_URL =  "/response/blockips";
    public static final String RESPONSE_IDS_URL =  "/response/ids";
    public static final String RESPONSE_CONNECTED_HOSTS_URL =  "/response/cnctdhost";
    private static final String VIEW_ALERTS =  "/get/alerts";
    private static final String MUTED_ALERTS =  "/get/mutedalerts";
    private static final String PRE_MUTED_ALERTS =  "/get/premuted";
    public static final String BLOCK_ALERT =  "/response/blockedalert";
    public static final String MUTE_ALERT =  "/response/mutedalert";
    private static final String SENSOR_DATA =  "/get/sensordata";
    private static final String ADD_SUBSCRIBER =  "/add/subscriber";
    private static final String REMOVE_SUBSCRIBER =  "/remove/subscriber";
    private static final String GET_NET_CONFIG =  "/get/netconfig";
    private static final String CHANGE_LED_MODE =  "/config/led";
    private static final String GET_LED_MODE =  "/demand/led";
    private static final String POST_POLICTY =  "/post/policy";
    private static final String POST_PROTOCOL_POLICTY =  "/post/blockapp";
    private static final String GET_POLICTY =  "/get/policy";
    private static final String GET_PROTOCOL_POLICTY =  "/get/blockapp";
    private static final String VERIFY_RESET_DEVICE_SETTINGS_CODE =  "/confirm/settings/code";
    //DASHBOARD DATA API'S
    private static final String ALERTS_DASHBOARD =  "/get/alerts/top";
    private static final String CONNECTED_DEVICES_DASHBOARD =  "/get/cnctdhosts/top";
    private static final String BANDWIDTH_DASHBOARD =  "/get/trafficstats/top";
    private static final String SENSOR_DASHBOARD =  "/get/sensordata/latest";

    private static final String POLICY_DURATIONS =  "/usage/hours/cnctdhosts";
    private static final String VIEW_ALERTS_SEEN =  "/alert/status";
    private static final String OTA_VERSION =  "/test/scripts/version";
    private static final String FREQUENT_PROTOCOLS =  "/get/protocols";

    private static final String GET_NOTIFICATIONS =  "/get/notification";
    private static final String SET_NOTIFICATIONS =  "/set/notification";
    private static final String APP_UPDATE =  "/io/ack";
    private static final String FIRMWARE_UPDATE =  "/firmware/ack";

    private static final String REBOOT_dmoat =  "/demand/reboot";

    private static final String DEVICE_RESET = "/device/reset";

    public static final String ACCOUNT_CREATE_UI_KEY = "ui111";


    //social Media Url's
    public static final String DMOAT_WEB_URL = "http://www.dmoat.com";
    public static final String DMOAT_FACEBOOK_URL = "https://www.facebook.com/Dmoat-Inc-1808973365833603/";
    public static final String DMOAT_TWITTER_URL = "https://twitter.com/dmoat_inc";
    public static final String DMOAT_INSTAGRAM_URL = "https://www.instagram.com/dmoat.inc/";

    public static final String video_account_setup_url = "https://prytex.nyc3.digitaloceanspaces.com/videos/Account+setup.mp4";
    public static final String video_connected_devices_url = "https://prytex.nyc3.digitaloceanspaces.com/videos/Connected+Devices.mp4";
    public static final String video_dashboard_url = "https://prytex.nyc3.digitaloceanspaces.com/videos/Dashboard.mp4";
    public static final String video_full_url = "https://prytex.nyc3.digitaloceanspaces.com/videos/Full+Video+Tutorial.mp4";
    public static final String video_network_usage_url = "https://prytex.nyc3.digitaloceanspaces.com/videos/Network+Usage.mp4";
    public static final String video_policy_url = "https://prytex.nyc3.digitaloceanspaces.com/videos/Policy.mp4";
    public static final String video_profile_settings_url = "https://prytex.nyc3.digitaloceanspaces.com/videos/Profile+and+Settings.mp4";
    public static final String video_sensors_data_url = "https://prytex.nyc3.digitaloceanspaces.com/videos/Sensor+Data.mp4";
    public static final String video_dmoat_alerts_url = "https://prytex.nyc3.digitaloceanspaces.com/videos/d.moat+Alerts.mp4";

    public static final String dmoatQuickStartDocURL = "https://s3.amazonaws.com/dmoat.public.files/Quick+Start.jpg";
    public static final String dmoatFeatureGuideDocURL = "https://s3.amazonaws.com/dmoat.public.files/dmoat_help_content.pdf";

    public static final String dmoatprivacyPolicyDocURL = "https://bstlegal.s3.amazonaws.com/dmoat/pdfs/d.moat_Privacy+Policy.pdf";
    public static final String dmoatTOSDocURL = "https://bstlegal.s3.amazonaws.com/dmoat/pdfs/d.moat_Terms+of+Service.pdf";
    public static final String dmoatEULADocURL = "https://bstlegal.s3.amazonaws.com/dmoat/pdfs/dmoat_EULA.pdf";



    public static void accountSetup(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        String ACCOUNT_SETUP = "/accountsetup";
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ ACCOUNT_SETUP,
                HttpAsyncRequest.RequestType.JSONDATA, new AccountSetup(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void userLogin(Context context, JSONObject object, AsyncTaskListener callback) {
        Log.d("JSON==>", object.toString());
        Log.d("USRLl==>", DashBoardActivity.BASE_URL+USER_LOGIN);
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+USER_LOGIN, HttpAsyncRequest.RequestType.JSONDATA, new
                UserLogin(), callback);
        req.setJsonData(object);
        req.execute();
    }

    //initialize sensor request
    public static void initSensorRequest(Context context, JSONObject object, AsyncTaskListener callback) {
        Log.d("JSON==>", object.toString());
        String SENSOR_API = "/initialize/sensor";
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ SENSOR_API, HttpAsyncRequest.RequestType.JSONDATA, new
                InitSensorRequest(), callback);
        req.setJsonData(object);
        req.execute();
    }

    //pause internet request
    public static void pauseInternetRequest(Context context, JSONObject object, AsyncTaskListener callback) {
        Log.d("JSON==>", object.toString());
        String PAUSE_Internet_API = "/post/policy";
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ PAUSE_Internet_API, HttpAsyncRequest.RequestType.JSONDATA, new
                PauseInternetParser(), callback);
        req.setJsonData(object);
        req.execute();
    }

    //discovery mode beacon request (1st)
    public static void beaconrequest(Context context, JSONObject object, AsyncTaskListener callback) {
        Log.d("JSON==>", object.toString());
        String BEACON_API = "http://hoth.dmoat.com:49124/beacon";
        Log.d("URL==>", BEACON_API);
        HttpAsyncRequest req = new HttpAsyncRequest(context, BEACON_API, HttpAsyncRequest.RequestType.JSONDATA, new
                BeaconRequest(), callback);
        req.setJsonData(object);
        req.execute();
    }

    //discovery mode acknowledgement ui after  beacon request success (2nd)
    public static void ackuiRequest(Context context, JSONObject object, AsyncTaskListener callback) {
        Log.d("JSON==>", object.toString());
        String ACK_UI = "/ack/ui";
        Log.d("URL==>", DashBoardActivity.BASE_URL+ ACK_UI);
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ ACK_UI, HttpAsyncRequest.RequestType.JSONDATA, new
                AckUiRequest(), callback);
        req.setJsonData(object);
        req.execute();
    }

    //authentication
    public static void authrequest(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        String AUTH_UI = "/auth";
        Log.d("URL==>", DashBoardActivity.BASE_URL+ AUTH_UI);
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ AUTH_UI,
                HttpAsyncRequest.RequestType.JSONDATA, new AuthenticationRequest(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void setNewPassword(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        String SET_NEW_PASSWORD = "/set/new/password";
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ SET_NEW_PASSWORD, HttpAsyncRequest.RequestType.JSONDATA
                , new ModifyPassword(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void modifyPassword(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        String MODIFY_PASSWORD = "/modify/password";
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ MODIFY_PASSWORD, HttpAsyncRequest.RequestType.JSONDATA
                , new ModifyPassword(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void createPolicy(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+POST_POLICTY, HttpAsyncRequest.RequestType.JSONDATA
//        HttpAsyncRequest req = new HttpAsyncRequest(context, "", HttpAsyncRequest.RequestType.JSONDATA
                , new CreatePolicyParser(), callback);
        req.setJsonData(data);
        req.execute();

    }
  public static void createProtocolPolicy(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+POST_PROTOCOL_POLICTY, HttpAsyncRequest.RequestType.JSONDATA
                , new CreatePolicyParser(), callback);
        req.setJsonData(data);
        req.execute();

    }

    public static void getAllPolicies(Context context, JSONObject data, AsyncTaskListener callback) {
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+GET_POLICTY, HttpAsyncRequest.RequestType.JSONDATA
                , new PoliciesListParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

  public static void getAllProtocolPolicies(Context context, JSONObject data, AsyncTaskListener callback) {
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+GET_PROTOCOL_POLICTY, HttpAsyncRequest.RequestType.JSONDATA
                , new PotocolPoliciesParser(), callback);
        req.setJsonData(data);
        req.execute();
    }


    public static void forgotPassword(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        String FORGOT_PASSWORD = "/forgot/password";
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ FORGOT_PASSWORD, HttpAsyncRequest.RequestType.JSONDATA
                , new ModifyPassword(), callback);
        req.setJsonData(data);
        req.execute();
    }



    public static void verifyResetSettingsCode(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+VERIFY_RESET_DEVICE_SETTINGS_CODE, HttpAsyncRequest.RequestType.JSONDATA
                , new ModifyPassword(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void forgotUsername(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        String FORGOT_USERNAME = "/forgot/username";
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ FORGOT_USERNAME, HttpAsyncRequest.RequestType.JSONDATA
                , new ModifyPassword(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void verifyPasswordCode(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        String VERIFY_PASSWORD_CODE = "/confirm/password/code";
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ VERIFY_PASSWORD_CODE, HttpAsyncRequest.RequestType.JSONDATA
                , new VerifyPasswordCode(), callback);
        req.setJsonData(data);
        req.execute();
    }


    public static void getBlockedAlerts(Context context, JSONObject json, AsyncTaskListener callback) {
        Log.d("JSON==>", json.toString());
        String BLOCKED_ALERTS = "/get/blockedalerts";
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ BLOCKED_ALERTS, HttpAsyncRequest.RequestType.JSONDATA
                , new BlockedAlertParser(), callback);
        req.setJsonData(json);
        req.execute();
    }

    public static void blockConnectedDevice(Context context, JSONObject json, AsyncTaskListener callback) {
        Log.d("JSON==>", json.toString());
        String CONNECTED_DEVICE_BLOCK = "/response/datacnctdhost";
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ CONNECTED_DEVICE_BLOCK, HttpAsyncRequest.RequestType.JSONDATA
                , new ConnectedDeviceParser(), callback);
        req.setJsonData(json);
        req.execute();
    }

    public static void renameConnectedDevice(Context context, JSONObject json, AsyncTaskListener callback) {
        Log.d("JSON==>", json.toString());
        if (isChangeOS) {
            String CONNECTED_OS_RENAME = "/response/os";
            HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ CONNECTED_OS_RENAME, HttpAsyncRequest.RequestType.JSONDATA
                    , new ConnectedDeviceParser(), callback);
            req.setJsonData(json);
            req.execute();
        } else if (isChangeUserName) {
            String CONNECTED_DEVICE_USER_RENAME = "/response/username";
            HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ CONNECTED_DEVICE_USER_RENAME, HttpAsyncRequest.RequestType.JSONDATA
                    , new ConnectedDeviceParser(), callback);
            req.setJsonData(json);
            req.execute();
        }

    }


    public static void getSensorData(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+SENSOR_DATA, HttpAsyncRequest.RequestType.JSONDATA
                , new SensorDataParser(), callback);
        req.setJsonData(data);
        req.execute();
    }

    public static void getVulnerabilitiesAlerts(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        String VULNERABILITTIES_ALERTS = "/get/vulnerabilities";
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ VULNERABILITTIES_ALERTS, HttpAsyncRequest.RequestType.JSONDATA
                , new VulnerabilitiesListParser(), callback);
        req.setJsonData(data);
        req.execute();
    }


    public static void getBandwidthDetails(Context context, JSONObject data, AsyncTaskListener callback) {
        Log.d("JSON==>", data.toString());
        String bandwidth_Url = "/get/trafficstats";
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ bandwidth_Url, HttpAsyncRequest.RequestType.JSONDATA
                , new BandwidthParser(), callback);
        req.setJsonData(data);
        req.execute();
    }


    public static void getContactedHosts(Context context, JSONObject json, AsyncTaskListener callback) {
        Log.d("JSON==>", json.toString());
        String CONTACTED_HOST = "/get/cnctdhosts";
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ CONTACTED_HOST, HttpAsyncRequest.RequestType.JSONDATA
                , new ConnectedDevicesParser(), callback);
        req.setJsonData(json);
        req.execute();
    }



    public static void sendResponse(Context context, String url, JSONObject j, AsyncTaskListener listener) {
        Log.d("JSON==>", j.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, url, HttpAsyncRequest.RequestType.JSONDATA
                , new UnmuteUnblockParser(), listener);
        req.setJsonData(j);
        req.execute();
    }


    public static void getViewAlerts(Context context, JSONObject json, AsyncTaskListener callback) {
        Log.d("JSON==>", json.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+VIEW_ALERTS, HttpAsyncRequest.RequestType.JSONDATA
                , new ViewAlersParser(), callback);
        req.setJsonData(json);
        req.execute();
    }

    public static void getMutedAlerts(Context context, JSONObject json, AsyncTaskListener callback) {
        Log.d("JSON==>", json.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+MUTED_ALERTS, HttpAsyncRequest.RequestType.JSONDATA
                , new MutedAlertsParser(), callback);
        req.setJsonData(json);
        req.execute();
    }

    public static void getPreMutedAlerts(Context context, JSONObject json, AsyncTaskListener callback) {
        Log.d("JSON==>", json.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+PRE_MUTED_ALERTS, HttpAsyncRequest.RequestType.JSONDATA
                , new Pre_MutedAlertsParser(), callback);
        req.setJsonData(json);
        req.execute();
    }

    public static void blockAlert(Context context, JSONObject j, AsyncTaskListener listener) {
        Log.d("JSON==>", j.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+BLOCK_ALERT, HttpAsyncRequest.RequestType.JSONDATA,
                new UnmuteUnblockParser(), listener);
        req.setJsonData(j);
        req.execute();
    }

    public static void muteAlert(Context context, JSONObject j, AsyncTaskListener listener) {
        Log.d("JSON==>", j.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+MUTE_ALERT, HttpAsyncRequest.RequestType.JSONDATA,
                new UnmuteUnblockParser(), listener);
        req.setJsonData(j);
        req.execute();
    }

    public static void addSubscriber(Context applicationContext, JSONObject j, AsyncTaskListener mAddSubscriberListener) {
        Log.d("JSON==>", j.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(applicationContext, DashBoardActivity.BASE_URL+ADD_SUBSCRIBER, HttpAsyncRequest.RequestType.JSONDATA,
                new BlockedAlertParser(), mAddSubscriberListener);
        req.setJsonData(j);
        req.execute();
    }

    public static void removeSubscriber(Context applicationContext, JSONObject j, AsyncTaskListener listener) {
        Log.d("JSON==>", j.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(applicationContext, DashBoardActivity.BASE_URL+REMOVE_SUBSCRIBER, HttpAsyncRequest.RequestType.JSONDATA,
                new BlockedAlertParser(), listener);
        req.setJsonData(j);
        req.execute();
    }

    public static void logout(Context context, JSONObject data, AsyncTaskListener mLogoutListener) {
        Log.d("JSON==>", data.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+LOGOUT_URL, HttpAsyncRequest.RequestType.JSONDATA,
                new BlockedAlertParser(), mLogoutListener);
        req.setJsonData(data);
        req.execute();
    }

    public static void getModeType(Context context, JSONObject json, AsyncTaskListener listener) {
        Log.d("JSON==>", json.toString());
        String GET_MODE_TYPE = "/demand/net";
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ GET_MODE_TYPE, HttpAsyncRequest.RequestType.JSONDATA,
                new NetModeTypeParser(), listener);
        req.setJsonData(json);
        req.execute();
    }

    public static void changeMode(Context context, JSONObject json, AsyncTaskListener listener) {
        Log.d("JSON==>", json.toString());
        String CHANGE_MODE = "/config/net";
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ CHANGE_MODE, HttpAsyncRequest.RequestType.JSONDATA,
                new NetConfigParser(), listener);
        req.setJsonData(json);
        req.execute();
    }

    public static void getNetConfig(Context context, JSONObject json, AsyncTaskListener listener) {
        Log.d("JSON==>", json.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+GET_NET_CONFIG, HttpAsyncRequest.RequestType.JSONDATA,
                new NetConfigParser(), listener);
        req.setJsonData(json);
        req.execute();
    }

    public static void changeLEDMode(Context context, JSONObject json, AsyncTaskListener listener) {
        Log.d("JSON==>", json.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+CHANGE_LED_MODE, HttpAsyncRequest.RequestType.JSONDATA,
                new NetConfigParser(), listener);
        req.setJsonData(json);
        req.execute();

    }

    public static void getLEDMode(Context context, JSONObject json, AsyncTaskListener listener) {
        Log.d("JSON==>", json.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+GET_LED_MODE, HttpAsyncRequest.RequestType.JSONDATA,
                new LedConfigParser(), listener);
        req.setJsonData(json);
        req.execute();

    }

    public static void clearBaseline(Context context, JSONObject json, AsyncTaskListener listener) {
        Log.d("JSON==>", json.toString());
        String CLEAR_BASELINE_API = "/clear/baseline";
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ CLEAR_BASELINE_API, HttpAsyncRequest.RequestType.JSONDATA,
                new InitSensorRequest(), listener);
        req.setJsonData(json);
        req.execute();

    }

    public static void getAlertsDashboard(Context context, JSONObject json, AsyncTaskListener listener) {
        Log.d("JSON==>", json.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+ALERTS_DASHBOARD, HttpAsyncRequest.RequestType.JSONDATA,
                new AlertsDashboardParser(), listener);
        req.setJsonData(json);
        req.execute();

    }

    public static void  getConnectedDevicesDashboard(Context context, JSONObject json, AsyncTaskListener listener) {
        Log.d("JSON==>", json.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+CONNECTED_DEVICES_DASHBOARD, HttpAsyncRequest.RequestType.JSONDATA,
                new ConnectedDevicesDashboardParser(), listener);
        req.setJsonData(json);
        req.execute();

    }

    public static void getBandwidthDashboard(Context context, JSONObject json, AsyncTaskListener listener) {
        Log.d("JSON==>", json.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+BANDWIDTH_DASHBOARD, HttpAsyncRequest.RequestType.JSONDATA,
                new BandwidthDashboardParser(), listener);
        req.setJsonData(json);
        req.execute();

    }

    public static void getSensorValuesDashboard(Context context, JSONObject json, AsyncTaskListener listener) {
        Log.d("JSON==>", json.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+SENSOR_DASHBOARD, HttpAsyncRequest.RequestType.JSONDATA,
                new SensorDashboardParser(), listener);
        req.setJsonData(json);
        req.execute();

    }

    public static void getPolicyDuration(Context context, JSONObject json, AsyncTaskListener listener) {
        Log.d("JSON==>", json.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+POLICY_DURATIONS, HttpAsyncRequest.RequestType.JSONDATA,
                new PolicyDurationParser(), listener);
        req.setJsonData(json);
        req.execute();

    }

    public static void setAlertSeen(Context context, JSONObject json, AsyncTaskListener listener) {
        Log.d("JSON==>", json.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+VIEW_ALERTS_SEEN, HttpAsyncRequest.RequestType.JSONDATA,
                new ModifyPassword(), listener);
        req.setJsonData(json);
        req.execute();

    }

    public static void getOtaVersion(Context context, JSONObject json, AsyncTaskListener listener) {
        Log.d("JSON==>", json.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+OTA_VERSION, HttpAsyncRequest.RequestType.JSONDATA,
                new OtaVersionParser(), listener);
        req.setJsonData(json);
        req.execute();

    }

    public static void getFrequentProtocols(Context context, JSONObject json, AsyncTaskListener listener) {
        Log.d("JSON==>", json.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+FREQUENT_PROTOCOLS, HttpAsyncRequest.RequestType.JSONDATA,
                new FrequentProtocols(), listener);
        req.setJsonData(json);
        req.execute();

    }



    public static void getNotifications(Context context, JSONObject object, AsyncTaskListener callback) {
        Log.d("JSON==>", object.toString());
        Log.d("USRLl==>", DashBoardActivity.BASE_URL+GET_NOTIFICATIONS);
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+GET_NOTIFICATIONS, HttpAsyncRequest.RequestType.JSONDATA, new
                PushNotificationsParser(), callback);
        req.setJsonData(object);
        req.execute();
    }
    public static void setNotifications(Context context, JSONObject object, AsyncTaskListener callback) {
        Log.d("JSON==>", object.toString());
        Log.d("USRLl==>", DashBoardActivity.BASE_URL+SET_NOTIFICATIONS);
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+SET_NOTIFICATIONS, HttpAsyncRequest.RequestType.JSONDATA, new
                ModifyPassword(), callback);
        req.setJsonData(object);
        req.execute();
    }
    public static void setFirmwareUpdate(Context context, JSONObject object, AsyncTaskListener callback) {
        Log.d("JSON==>", object.toString());
        Log.d("USRLl==>", DashBoardActivity.BASE_URL+FIRMWARE_UPDATE);
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+FIRMWARE_UPDATE, HttpAsyncRequest.RequestType.JSONDATA, new
                ModifyPassword(), callback);
        req.setJsonData(object);
        req.execute();
    }
    public static void setAppUpDate(Context context, JSONObject object, AsyncTaskListener callback) {
        Log.d("JSON==>", object.toString());
        Log.d("USRLl==>", DashBoardActivity.BASE_URL+APP_UPDATE);
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+APP_UPDATE, HttpAsyncRequest.RequestType.JSONDATA, new
                ModifyPassword(), callback);
        req.setJsonData(object);
        req.execute();
    }

    public static void rebootDmoat(Context context, JSONObject object, AsyncTaskListener callback) {
        Log.d("JSON==>", object.toString());
        Log.d("USRLl==>", DashBoardActivity.BASE_URL+REBOOT_dmoat);
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+REBOOT_dmoat, HttpAsyncRequest.RequestType.JSONDATA, new
                ModifyPassword(), callback);
        req.setJsonData(object);
        req.execute();
    }

    public static void deviceReset(Context context, JSONObject object, AsyncTaskListener callback) {
        Log.d("JSON==>", object.toString());
        HttpAsyncRequest req = new HttpAsyncRequest(context, DashBoardActivity.BASE_URL+DEVICE_RESET, HttpAsyncRequest.RequestType.JSONDATA, new
                ModifyPassword(), callback);
        req.setJsonData(object);
        req.execute();
    }


}
