package hardware.prytex.io.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.Nullable;
import androidx.multidex.MultiDex;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;

import hardware.prytex.io.VersionCheckApi.VersionCheck;
import hardware.prytex.io.config.Constants;
import hardware.prytex.io.model.UserManager;


import hardware.prytex.io.fragment.DashBoardFragment;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;

import static hardware.prytex.io.FCM.MyFCMListenerService.removeNotificaitonFromStatusBar;
import static hardware.prytex.io.VersionCheckApi.CheckEnvirenmentParser.environemntObj;
import static hardware.prytex.io.VersionCheckApi.VersionCheckParser.versionCheckObj;


/**
 * Created by abubaker on 5/17/16.
 */
public class SplashActivity extends BaseActivity {

    private static final int SPLASH_TIME = 2 * 1000;

    private SharedPreferences prefs;
    public static String androidId = "qwert";
    private String url;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_splash);
        MultiDex.install(getApplicationContext());
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();

        prefs = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);

        String package_name = getPackageName();

        androidId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
//        Log.d("Androi_UUID", androidId);
        String playStoreAppVersion = "1.6.43";

        String versionName = null;
        try {
            versionName = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Log.d("AppVersion==", versionName);


        if (Dmoat.APP_ID == null || Dmoat.APP_ID.equalsIgnoreCase("")) {
//            Dmoat.APP_ID = Dmoat.getAppId();
            Dmoat.APP_ID = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            editor.putString("appId", Dmoat.APP_ID);
            editor.apply();
        }

        showProgressDialog(true);
        VersionCheck.getEnvironMent(this,environmentCheckListner);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash;
    }


    private final AsyncTaskListener environmentCheckListner = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {
            VersionCheck.getVersionCheck(SplashActivity.this,versionCheckListner);
        }
    };

    private final AsyncTaskListener versionCheckListner = result -> {
        showProgressDialog(false);
        if(!versionCheckObj.isStatus()) {
            showVersionCheckAlert();
        }else {
            new Handler().postDelayed(() -> {

                if (UserManager.getInstance().isUserLoggedIn()) {

                    AddActivityandRemoveIntentTransition(DashBoardActivity.class);
                    finish();
                } else {

                    AddActivityandRemoveIntentTransition(AuthenticationActivity.class);
                    SplashActivity.this.finish();
                }


            }, SPLASH_TIME);
        }
    };

    private void showVersionCheckAlert() {
        if(environemntObj.getData().getEnvironment().equalsIgnoreCase("testflight")){
            url= "https://play.google.com/apps/internaltest/4698350995299045259";
        }else {
            url = "https://play.google.com/apps/internaltest/4698350995299045259";
        }
        new AlertDialog.Builder(this)
                .setTitle("d.moat")
                .setMessage("App update available")// + "(" + playStoreAppVersion + ")" + ".")
                .setCancelable(false)
                .setPositiveButton("OK", (dialog, whichButton) -> {

                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url + appPackageName)));
                    }
                    finish();
                    dialog.dismiss();
                }).show();
    }


    private void AddActivityandRemoveIntentTransition(Class activity)
    {
        Intent intent = new Intent(this, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivityForResult(intent, 0);
        this.overridePendingTransition(0,0);
    }
}
