package hardware.prytex.io.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;

import hardware.prytex.io.R;
import hardware.prytex.io.config.Constants;
import hardware.prytex.io.fragment.BaseFragment;

import java.util.Stack;

public abstract class BaseActivity extends AppCompatActivity implements BaseFragment.FragmentNavigationHelper {
    //
    private BaseFragment mCurrentFragment;
    private SharedPreferences prefs;
    private final Stack<Fragment> mFragments = new Stack<>();
    private Dialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);

        setContentView(getLayoutId());

        initViews(savedInstanceState);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);

    }


    public void initViews(Bundle savedInstanceState) {

    }

    protected abstract int getLayoutId();

    @Override
    public void addFragment(BaseFragment f, boolean clearBackStack, boolean addToBackstack) {
        addFragment(f, android.R.id.content, clearBackStack, addToBackstack);
    }

    @Override
    public void   addFragmentwitoutTabbar(BaseFragment f, boolean clearBackStack, boolean addToBackstack) {
        addFragment(f, R.id.fragment_fullscreen, clearBackStack, addToBackstack);
    }
    private void addFragment(BaseFragment f, int layoutId, boolean clearBackStack, boolean addToBackstack) {
        if (clearBackStack) {
            clearFragmentBackStack();
        }
//        if (Constants.dashBoardActivity != null){
//            if (f instanceof DashBoardFragment) {
//                DashBoardActivity.showSwitchButton();
//            } else {
//                DashBoardActivity.hideSwitchButton();
//            }
//        }


        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, f);
        if (addToBackstack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();

        mCurrentFragment = f;
        mFragments.push(f);

        onFragmentBackStackChanged();
    }


    @Override
    public void replaceFragment(BaseFragment f, boolean clearBackStack, boolean addToBackstack) {
        replaceFragment(f, R.id.fragment_container, clearBackStack, addToBackstack);
    }

    public void replaceFragment(BaseFragment f, int layoutId, boolean clearBackStack, boolean addToBackstack) {
        try {
//            if (Constants.dashBoardActivity != null){
//                if (f instanceof DashBoardFragment) {
//                    DashBoardActivity.showSwitchButton();
//                } else {
//                    DashBoardActivity.hideSwitchButton();
//                }
//            }

            if (clearBackStack) {
                clearFragmentBackStack();
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(layoutId, f);
            if (addToBackstack) {
                transaction.addToBackStack(null);
            }


            transaction.commit();

            mCurrentFragment = f;
            mFragments.push(f);

            onFragmentBackStackChanged();
        } catch (Exception e) {
            Log.d("BaseActivityReplaceFrag", e.toString());
        }
    }


    public void replaceFragmentWithAnim(BaseFragment f)
    {
//        if(Constants.dashBoardActivity != null) {
//            if (f instanceof DashBoardFragment) {
//                DashBoardActivity.showSwitchButton();
//            } else {
//                DashBoardActivity.hideSwitchButton();
//            }
//        }
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();

        ft.setCustomAnimations(R.anim.fadein, R.anim.fadeout);
        //ft.setCustomAnimations(android.R.anim.slide_out_right, android.R.anim.slide_in_left);
        //ft.setCustomAnimations(android.R.anim.fade_out, android.R.anim.fade_in);
        ft.replace( R.id.fragment_container, f);
        ft.commit();

        onFragmentBackStackChanged();
    }


    @Override
    public void onBack() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
          //  finish();
            return;
        }
        getSupportFragmentManager().popBackStack();
        mFragments.pop();
        mCurrentFragment = (BaseFragment) (mFragments.isEmpty() ? null : ((mFragments.peek() instanceof BaseFragment) ? mFragments.peek() : null));

        onFragmentBackStackChanged();
    }

    void clearFragmentBackStack() {
        try {
            FragmentManager fm = getSupportFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount() - 1; i++) {
                fm.popBackStack();
            }

            if (!mFragments.isEmpty()) {
                Fragment homeFragment = mFragments.get(0);
                mFragments.clear();
                mFragments.push(homeFragment);
            }
        } catch (Exception e) {
            Log.d("ClearfrgtBackStackExcep", e.toString());
        }

    }

    void onFragmentBackStackChanged() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            if (isFromBandwidthChartsDetails) {
//                return false;
//            } else if (isFromBandwidthListViewDetails) {
//                return false;
//            } else if (isFromBandwidthHostDetails) {
//                return false;
//            } else {
//            }

            boolean flag = false;
            if (mCurrentFragment != null) {
                flag = mCurrentFragment.onKeyDown(keyCode, event);
            }
            if (flag) {
                return true;
            }

//            code commented to disable device back button
//            if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
//                finish();
//                return true;
//            } else {
//                onBack();
//                return true;
//            }

            //code for dashboard device backbtn only
//            if (isOnDashboardScreen){
//                finish();
//                return true;
//            }
        }

        return super.onKeyDown(keyCode, event);

    }

    BaseFragment getCurrentFragment() {
        return mCurrentFragment;
    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }
    void popBackStack(){
        this.overridePendingTransition(0,0);
        android.app.FragmentManager fragmentManager = this.getFragmentManager();
        fragmentManager.popBackStack(null, android.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
        this.finish();
    }

    protected void backButoonPressed()
    {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if(count > 0)

        {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        else {
            AddActivityandRemoveIntentTransition();
            popBackStack();
            finish();
        }
    }


    void screenInfoDialog(String infoText)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(getResources().getString(R.string.app_name));
        alert.setMessage(infoText);
        alert.setPositiveButton("Ok", (dialog, whichButton) -> {

            // Do something with value!
        });

//        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
//            }
//        });
        alert.show();
    }

    private void AddActivityandRemoveIntentTransition()
    {
        Intent intent = new Intent(this, DashBoardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivityForResult(intent, 0);
        this.overridePendingTransition(0,0);
    }

    public void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(this, "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }
}