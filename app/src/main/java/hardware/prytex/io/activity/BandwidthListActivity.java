package hardware.prytex.io.activity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import hardware.prytex.io.R;
import hardware.prytex.io.fragment.BandwidthFrags.ProtocolsListViewMainFragment;

public class BandwidthListActivity extends BaseActivity implements View.OnClickListener{
    @Override
    public int getLayoutId() {
        return R.layout.bandwidth_list_activity;
    }

    @Override
    public void initViews(Bundle savedInstanceState) {
        super.initViews(savedInstanceState);

        ImageView backimage = findViewById(R.id.img_back);
        backimage.setOnClickListener(this);
        backimage.setVisibility(View.GONE);
        backimage.setEnabled(false);
        ImageView imgInfoBtn = findViewById(R.id.img_info);
        imgInfoBtn.setOnClickListener(this);
        ProtocolsListViewMainFragment fragment = new ProtocolsListViewMainFragment();
                addFragment(fragment, false, true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.img_back) {
            popBackStack();
            finish();
        }else if(v.getId() == R.id.img_info) {

            screenInfoDialog(getResources().getString(R.string.bandwidth_screen_txt));
        }
    }


    @Override
    public void onBackPressed()
    {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if(count == 2)
        {
            finish();
            getIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivityForResult(getIntent(), 0);
            overridePendingTransition(0,0);
        }
        else {

            popBackStack();
            finish();
        }

    }
}
