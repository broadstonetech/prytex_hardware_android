package hardware.prytex.io.activity;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.adapter.ConnectedHostsAdapter;
import hardware.prytex.io.fragment.CnctdDevicesTabBars.DefaultPwdsFragment;
import hardware.prytex.io.fragment.CnctdDevicesTabBars.OpenPortsFragment;
import hardware.prytex.io.fragment.CnctdDevicesTabBars.RiskProfileMainFragment;
import hardware.prytex.io.model.ActivePortsModel;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.network.Api;
import hardware.prytex.io.network.AsyncTaskListener;
import hardware.prytex.io.network.TaskResult;
import hardware.prytex.io.util.DeviceUtils;
import hardware.prytex.io.util.GeneralUtils;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static hardware.prytex.io.activity.DashBoardActivity.setBottomNavTextColors;
import static hardware.prytex.io.activity.DashBoardActivity.showAlertMsgDialog;
import static hardware.prytex.io.parser.ConnectedDevicesParser.isDevicesListReceived;
import static hardware.prytex.io.parser.ConnectedDevicesParser.listOfConnetcedDevice;

/**
 * Created by abubaker on 10/31/16.
 */


public class ConnectedDeviceActivity extends BaseActivity implements View.OnClickListener {

    private static String DEVICE_IP;
    ImageView backimage, imgInfoBtn;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private SharedPreferences pref;
    private String appId = "";
    SearchView searchViewConnectedDevice;
    ConnectedHostsAdapter mAdapter;

    private final AsyncTaskListener listener = new AsyncTaskListener() {
        @Override
        public void onComplete(TaskResult result) {

            showProgressDialog(false);
            if (result.code == 200 || result.code == 2) {
                if (isDevicesListReceived) {
                    if (listOfConnetcedDevice.size() > 0) {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mAdapter = new ConnectedHostsAdapter(ConnectedDeviceActivity.this, listOfConnetcedDevice, new ArrayList<>(), mAlertSelectedListener, appId, DEVICE_IP);
                        mRecyclerView.setAdapter(mAdapter);
                    } else {
                        showAlertMsgDialog(ConnectedDeviceActivity.this, getResources().getString(R.string.no_connected_devices));
                    }
                } else {
                    showAlertMsgDialog(ConnectedDeviceActivity.this, getResources().getString(R.string.no_connected_devices));
                }
            } else if (result.code == 503) {
                showAlertMsgDialog(ConnectedDeviceActivity.this, result.message);
            } else if (result.code == 409) {
                showAlertMsgDialog(ConnectedDeviceActivity.this, result.message);
            } else {
                showAlertMsgDialog(ConnectedDeviceActivity.this, getResources().getString(R.string.no_connected_devices));
            }
        }
    };

    private final ConnectedHostsAdapter.OnAlertSelectedListener mAlertSelectedListener = (alert, position, isDefaultPasswords, isActivePorts) -> {

        ArrayList<ActivePortsModel> listOfDevices = new ArrayList<>();
        ActivePortsModel defaultPwdsModel;

        JSONArray jArray = alert.jsonArrayDefaultPasswords;
        for (int jobNo = 0; jobNo < jArray.length(); jobNo++) {
            try {
                JSONObject jObj = jArray.getJSONObject(jobNo);
                String deviceUsername = jObj.getString("username");
                String devicePwd = jObj.getString("password");
                String deviceModel = jObj.getString("productname");
                defaultPwdsModel = new ActivePortsModel();
                defaultPwdsModel.setUserName(deviceUsername);
                defaultPwdsModel.setModelName(deviceModel);
                defaultPwdsModel.setDefaultPwd(devicePwd);
                listOfDevices.add(defaultPwdsModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        //get ports list from json
        ArrayList<ActivePortsModel> listOfPorts = new ArrayList<>();
        ActivePortsModel activePortsModel;

        JSONArray jArrayPorts = alert.jsonArrayPorts;
        for (int jobNo = 0; jobNo < jArrayPorts.length(); jobNo++) {
            try {
                JSONObject jObj = jArrayPorts.getJSONObject(jobNo);
                String portNo = jObj.getString("port");
                String portName = jObj.getString("service_name");
                activePortsModel = new ActivePortsModel();
                activePortsModel.setPortNumber(portNo);
                activePortsModel.setPortName(portName);
                listOfPorts.add(activePortsModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        RiskProfileMainFragment fragment = new RiskProfileMainFragment();
        DefaultPwdsFragment.listOfDefaultDevices = listOfDevices;
        DefaultPwdsFragment.HostSelectedPosition = position;
        OpenPortsFragment.listOfActivePorts = listOfPorts;
        OpenPortsFragment.HostSelectedPosition = position;
        replaceFragment(fragment, false, true);


    };


    @Override
    public int getLayoutId() {
        return R.layout.connected_fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this, "connected_devices", null /* class override */);

    }

    @Override
    public void initViews(Bundle savedInstanceState) {
        super.initViews(savedInstanceState);

        pref = this.getSharedPreferences("MyPref", 0);
        appId = pref.getString("app_id", "");
        setBottomNavTextColors(this, false, false, false, false);
        DashBoardActivity.isOnDashboardScreen = false;
        DEVICE_IP = DeviceUtils.getIPAddress(true); // IPv4
        mSwipeRefreshLayout = findViewById(R.id.cnctdhosts_swipe_refresh_layout);
        mRecyclerView = findViewById(R.id.recycler_view);
        searchViewConnectedDevice = findViewById(R.id.searchView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        DashBoardActivity.imgInfoBtn.setVisibility(View.VISIBLE);

        mRecyclerView.setVisibility(View.GONE);
        if (GeneralUtils.isConnected(this)) {
            showProgressDialog(true);
            Api.getContactedHosts(this, getRequestparams(), listener);
        } else {
            showAlertMsgDialog(this, getResources().getString(R.string.internet_connection));
        }

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mRecyclerView.setVisibility(View.GONE);
            refreshContent();
        });

        backimage = findViewById(R.id.img_back);
        backimage.setVisibility(View.GONE);
        backimage.setEnabled(false);
        backimage.setOnClickListener(this);
        imgInfoBtn = findViewById(R.id.img_info);
        imgInfoBtn.setOnClickListener(this);

        searchViewConnectedDevices();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void searchViewConnectedDevices() {
        View closeButton = searchViewConnectedDevice.findViewById(R.id.search_close_btn);

        EditText et= (EditText) searchViewConnectedDevice.findViewById(R.id.search_src_text);
        et.setHint("Search...");

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchViewConnectedDevice.onActionViewCollapsed();
            }
        });

        searchViewConnectedDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchViewConnectedDevice.onActionViewExpanded();
            }
        });
        searchViewConnectedDevice.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    closeButton.performClick();
                }
            }
        });

        searchViewConnectedDevice.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchViewConnectedDevice.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                mAdapter.getFilter().filter(query);
                return false;
            }
        });

    }

    private JSONObject getRequestparams() {
        JSONObject requestData = new JSONObject();
        try {
            requestData.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
            requestData.put("postkey", UserManager.getInstance().getLoggedInUser().postKey);
            requestData.put("request_id", Dmoat.getNewRequestId());
            requestData.put("app_id", pref.getString("app_id", ""));
            requestData.put("token", UserManager.getInstance().getLoggedInUser().token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestData;
    }

    private void refreshContent() {
        Api.getContactedHosts(ConnectedDeviceActivity.this, getRequestparams(), listener);
        mSwipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == backimage.getId()) {
            onBackPressed();
        } else if (v.getId() == imgInfoBtn.getId()) {
            Fragment fragmentInFrame = getSupportFragmentManager()
                    .findFragmentById(R.id.fragment_container);
            if (fragmentInFrame instanceof RiskProfileMainFragment) {
                screenInfoDialog(getResources().getString(R.string.default_passwords_screen_txt));

            } else {
                screenInfoDialog(getResources().getString(R.string.connectedDevices_screen_txt));
            }
        }

    }

    @Override
    public void onBackPressed() {
        backButoonPressed();

    }
}

