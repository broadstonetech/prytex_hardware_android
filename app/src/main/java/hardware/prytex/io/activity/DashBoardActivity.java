package hardware.prytex.io.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import hardware.prytex.io.Dmoat;
import hardware.prytex.io.R;
import hardware.prytex.io.config.Constants;
import hardware.prytex.io.fragment.AboutFragment;
import hardware.prytex.io.fragment.AlertsTabs.BlockedAlertsFragment;
import hardware.prytex.io.fragment.AlertsTabs.MutedAlertsFragment;
import hardware.prytex.io.fragment.BandwidthFrags.BandwidthHostDetailsFragment;
import hardware.prytex.io.fragment.BandwidthFrags.ProtocolsListViewMainFragment;
import hardware.prytex.io.fragment.ChangePasswordFragment;
import hardware.prytex.io.fragment.ChooseFilterFragment;
import hardware.prytex.io.fragment.CnctdDevicesTabBars.DefaultPwdsFragment;
import hardware.prytex.io.fragment.CnctdDevicesTabBars.OpenPortsFragment;
import hardware.prytex.io.fragment.CnctdDevicesTabBars.RiskProfileMainFragment;
import hardware.prytex.io.fragment.DashBoardFragment;
import hardware.prytex.io.fragment.FaqsFragment;
import hardware.prytex.io.fragment.FragmentThemeOne;
import hardware.prytex.io.AgentChatActivity.HelpTutorialFragment;
import hardware.prytex.io.fragment.NetConfigFragment;
import hardware.prytex.io.fragment.Policy.AccessControl.PoliciesFragment;
import hardware.prytex.io.fragment.Policy.AccessControl.PolicyPlanFragment;
import hardware.prytex.io.fragment.Policy.PauseInternetFragment;
import hardware.prytex.io.fragment.Policy.PoliciesMainFragment;
import hardware.prytex.io.fragment.Policy.ProtocolsPolicy.HostSelectionFragment;
import hardware.prytex.io.fragment.Policy.ProtocolsPolicy.ProtocolPolicyControlFragment;
import hardware.prytex.io.fragment.ProfileFragment;
import hardware.prytex.io.fragment.SensorTabBars.SensorMainFragment;
import hardware.prytex.io.fragment.VerifyCodeFragment;
import hardware.prytex.io.fragment.VulnerabilityDetailsFragment;
import hardware.prytex.io.fragment.VulnerabilityFragment;
import hardware.prytex.io.fragment.VulnerabilityListFragment;
import hardware.prytex.io.FCM.QuickstartPreferences;
import hardware.prytex.io.fragment.themes.ThemeFragment;
import hardware.prytex.io.model.User;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.util.GeneralUtils;
import hardware.prytex.io.util.UserPrefs;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.zopim.android.sdk.api.ZopimChat;
import com.zopim.android.sdk.model.VisitorInfo;
import com.zopim.android.sdk.prechat.PreChatForm;

import org.json.JSONException;
import org.json.JSONObject;

import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;
import static hardware.prytex.io.model.User.netConfigTypeSaved;

/**
 * Created by abubaker on 9/27/16.
 */

@SuppressWarnings("ALL")
public class DashBoardActivity extends BaseActivity implements View.OnClickListener {

    @SuppressLint("StaticFieldLeak")
    public static TextView mTitle;
    private SharedPreferences pref;
    public static boolean isOnDashboardScreen = true;
    @SuppressLint("StaticFieldLeak")
    public static ImageView imgInfoBtn;
    @SuppressLint("StaticFieldLeak")
    public static ImageView imgTopLogo;
    @SuppressLint("StaticFieldLeak")
    public static ImageView imgHomeTab;
    @SuppressLint("StaticFieldLeak")
    public static ImageView imgPolicyTab;
    @SuppressLint("StaticFieldLeak")
    private static ImageView imgAccountTab;
    @SuppressLint("StaticFieldLeak")
    private static ImageView imgHelpTab;
    @SuppressLint("StaticFieldLeak")
    public static Button btnHomeDashboard;
    @SuppressLint("StaticFieldLeak")
    public static Button btnPolicyDashboard;
    @SuppressLint("StaticFieldLeak")
    private static Button btnAccountDashboard;
    @SuppressLint("StaticFieldLeak")
    private static Button btnHelpDashboard;
    public static String dashboardSelectedbutton = "home";
    public static final String usernameSpacePN = "";
    @SuppressLint("StaticFieldLeak")
    public static LinearLayout llBottomTabs;


    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "dashboardActivity";
    public static String BASE_URL = "";

    //Changes by umer, get title to change it on theme change
    static RelativeLayout title_layout;
    LinearLayout menubar;
    static LinearLayout llToggleTheme1;
    ToggleButton tbSwitchTheme1;
    @Override
    public int getLayoutId() {
        return R.layout.activity_dashboard;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        }

    @Override
    public void initViews(Bundle savedInstanceState) {
        super.initViews(savedInstanceState);
        Context mContext = DashBoardActivity.this;
        Constants.dashBoardActivity = this;
//        UserManager.USER_TOKEN_PREFS = Settings.Secure.getString(getApplicationContext().getContentResolver(),
//                Settings.Secure.ANDROID_ID);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0);

//        String appMode = pref.getString("appMode", "");
//        if(appMode.equalsIgnoreCase("dev"))
//        {
//            BASE_URL = "https://dev.dmoat.com";
//        }
//        else if (appMode.equalsIgnoreCase("prod"))
//        {
            BASE_URL = "https://api.dmoat.com";
//        } else {
//            BASE_URL = "";
//        }

        String appId = pref.getString("appId", "");
        if (appId == null || appId.equalsIgnoreCase("")) {
            Dmoat.APP_ID = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        } else {
            Dmoat.APP_ID = appId;
        }
        try {
            setUserSettings();
        } catch (Exception e) {
            Log.d("UsetSettingException", e.toString());
        }
        final User user = UserManager.getInstance().getLoggedInUser();
        Dmoat.isAppRunning = true;
        isOnDashboardScreen = true;

        tbSwitchTheme1 = findViewById(R.id.tbToggleTheme1);

        mTitle = findViewById(R.id.txt_title);
        TextView tvProfileUserName = findViewById(R.id.tvProfileMenu);
        TextView tvBadgeCountChat = findViewById(R.id.tvbadge);
        tvProfileUserName.setText(getResources().getString(R.string.profile_and_setting));

        title_layout = (RelativeLayout)findViewById(R.id.title_layout);

        menubar = (LinearLayout)findViewById(R.id.ll_menubar);

        imgTopLogo = findViewById(R.id.img_logo);
        imgInfoBtn = findViewById(R.id.img_info);
        llToggleTheme1 = findViewById(R.id.llToggleTheme1);
        imgInfoBtn.setVisibility(View.GONE);
        imgInfoBtn.setOnClickListener(view -> ShowScreenInfo());

        //bottom bar buttons
        llBottomTabs = findViewById(R.id.llBottomBtns);
        btnHomeDashboard = findViewById(R.id.btnHomeDashbaord);
        btnPolicyDashboard = findViewById(R.id.btnPolicyDashboard);
        btnHelpDashboard = findViewById(R.id.btnHelpDashboard);
        btnAccountDashboard = findViewById(R.id.btnAccountDashboard);
        imgHomeTab = findViewById(R.id.img_tab_home);
        imgPolicyTab = findViewById(R.id.img_tab_policy);
        imgAccountTab = findViewById(R.id.img_tab_account);
        imgHelpTab = findViewById(R.id.img_tab_help);
        subscribeToGCM();

        setTabBarSelection();
        findViewById(R.id.menu_HelpTutorial).setOnClickListener(v -> {

            isOnDashboardScreen = false;
            //call web service for initializin sensor
            setBottomNavTextColors(getApplicationContext(), false, false, false, true);
            addFragment(new HelpTutorialFragment(), true, true);

        });
        findViewById(R.id.menu_about).setOnClickListener(v -> {

            isOnDashboardScreen = false;
            //call web service for initializin sensor
            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
      //      addFragment(new AboutFragment(), true, true);
        });


        findViewById(R.id.menu_profile).setOnClickListener(v -> {
            isOnDashboardScreen = false;

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            addFragment(new ProfileFragment(), true, true);
        });

        findViewById(R.id.menu_logout).setOnClickListener(v -> {
            isOnDashboardScreen = false;
            new AlertDialog.Builder(DashBoardActivity.this)
                    .setTitle("Signing Out")
                    .setMessage("Are you sure you want to sign out?")
                    .setPositiveButton("sign out", (dialog, whichButton) -> {
                        isDmoatOnline = true;
                        if (GeneralUtils.isConnected(DashBoardActivity.this)) {

                            DashBoardActivity.BASE_URL = "";
//                                    logout();
                        }
                    })
                    .setNegativeButton("cancel", null).show();
        });

        findViewById(R.id.menu_Faqs).setOnClickListener(v -> {
            isOnDashboardScreen = false;

            isOnDashboardScreen = false;
            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            addFragment(new FaqsFragment(), true, true);

        });

        findViewById(R.id.menu_open_src).setOnClickListener(v -> {
            isOnDashboardScreen = false;

            isOnDashboardScreen = false;
            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            //addFragment(new OpenSrcPackagesFragment(), true, true);
            Intent myIntent = new Intent(DashBoardActivity.this, AboutFragment.class);
            startActivity(myIntent);
        });



        findViewById(R.id.btnHomeDashbaord).setOnClickListener(v -> {
//                MyToast.showMessage(getApplicationContext(), "go to dashboard");
            if (UserManager.checkPauseInternetTimeStatus()) {
                dashboardSelectedbutton = "dasboardPressed";
                setBottomNavTextColors(getApplicationContext(), true, false, false, false);

                Fragment fragmentInFrame = getSupportFragmentManager()
                        .findFragmentById(R.id.fragment_container);
                if (!(fragmentInFrame instanceof DashBoardFragment)) {

                    finish();
                    getIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivityForResult(getIntent(), 0);
                    overridePendingTransition(0,0);
                }
                } else {
                showAlertMsgDialog(getApplicationContext(), getResources().getString(R.string.paused_internet_msg));
            }
        });

        findViewById(R.id.btnPolicyDashboard).setOnClickListener(v -> {
            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if (currentFragment instanceof PoliciesMainFragment) {

            }
            else
            {
                if (UserManager.checkPauseInternetTimeStatus()) {
                    dashboardSelectedbutton = "dasboardPressed";


                    imgInfoBtn.setImageResource(R.drawable.info);
                    setBottomNavTextColors(getApplicationContext(), false, true, false, false);
//                    addFragment(new PauseInternetFragment(), true, true);
                    addFragment(new PoliciesMainFragment(), true, true);

                } else {
                    showAlertMsgDialog(getApplicationContext(), getResources().getString(R.string.paused_internet_msg));
                }
            }

        });

        findViewById(R.id.btnAccountDashboard).setOnClickListener(v -> {
            dashboardSelectedbutton = "dasboardPressed";

            imgInfoBtn.setImageResource(R.drawable.info);
            setBottomNavTextColors(getApplicationContext(), false, false, true, false);
            addFragment(new ProfileFragment(), true, true);

        });

        findViewById(R.id.btnHelpDashboard).setOnClickListener(v -> {
            setBottomNavTextColors(getApplicationContext(), false, false, false, true);
            isOnDashboardScreen = false;
//                menu.setVisibility(View.INVISIBLE);
//                openSupportConversation();
            dashboardSelectedbutton = "dasboardPressed";

            imgInfoBtn.setImageResource(R.drawable.info);
            addFragment(new HelpTutorialFragment(), true, true);
        });

//        PubNubHandler.getInstance().subscribeAllChannelsToNameSpace(UserManager.getInstance().getLoggedInUser().nameSpace);
//
//        AlertsHandler.getInstance(DashBoardActivity.this.getApplicationContext()).init();
        if(Constants.activityContext !=null && Constants.activityContext instanceof AboutFragment) {
            Constants.activityContext = null;
            replaceFragment(new ProfileFragment(), true, true);
        }
        else{       //Changes by umer, to load user pref about theme selection and load the theme accordingly
            loadTheme();
        }


//        replaceFragment(new NewFragment(), true, true);

//        startPubNubService();

//        Bundle extras = getIntent().getExtras();
//        if (extras != null && extras.containsKey("alert")) {
//            if (getCurrentFragment() != null && !(getCurrentFragment() instanceof ViewAlertsAdvancedFragment)) {
//                Intent intent = new Intent(this, AlertsMainActivity.class);
//                startActivity(intent);
////                popBackStack();
//
//            } else {
//                Log.d("DashboardActivityPN", "getCurrentFragment() != null");
//            }
//        }

        tbSwitchTheme1.setOnClickListener(this);
    }

    public static void hideSwitchButton()
    {
        llToggleTheme1.setVisibility(View.GONE);
    }
    public static void showSwitchButton()
    {
        llToggleTheme1.setVisibility(View.VISIBLE);
    }
    private void setUserSettings() {
//        String netConfigType = UserManager.getInstance().getLoggedInUser().net.type;
        String netConfigType = netConfigTypeSaved;
        if (netConfigType.equalsIgnoreCase("null") || netConfigType.equalsIgnoreCase("")) {
            netConfigType = "P&P";
        }
        String ledMode = UserManager.getInstance().getLoggedInUser().led.frequency;
        if (netConfigType != null && netConfigType.equalsIgnoreCase("P&P"))
            setConfigMode(DashBoardFragment.MODE_P_AND_P);
        else
            setConfigMode(DashBoardFragment.MODE_ADVANCE);

        if (ledMode != null && ledMode.equalsIgnoreCase("OFF"))
            setLedMode(ProfileFragment.LED_OFF);
        else if (ledMode != null && ledMode.equalsIgnoreCase("ON")) {
            String ledFrequency = UserManager.getInstance().getLoggedInUser().led.frequency;
            if (ledFrequency.equalsIgnoreCase("5"))
                setLedMode(ProfileFragment.LED_ON_5_SECONDS);
            else
                setLedMode(ProfileFragment.LED_ON_10_SECONDS);
        } else {
            setLedMode(ProfileFragment.LED_ON_5_SECONDS);
        }

    }

    private void setConfigMode(int mode) {
        getSharedPreferences(Constants.SETTINGS_PREFS_NAME, MODE_PRIVATE).edit().putInt(DashBoardFragment.MODE
                + "_" + UserManager.getInstance().getLoggedInUser().userName, mode).apply();
    }

    private void setLedMode(int mode) {
        getSharedPreferences(Constants.SETTINGS_PREFS_NAME, MODE_PRIVATE).edit().putInt(ProfileFragment.LED +
                "_" + UserManager.getInstance().getLoggedInUser().userName, mode).apply();
    }



    private JSONObject getLogoutParams() {
        JSONObject js = new JSONObject();
        try {
            js.put("request_id", Dmoat.getNewRequestId());
            js.put("app_id", pref.getString("app_id", ""));
            js.put("d", UserManager.getInstance().getLoggedInUser().postKey);
            js.put("token", UserManager.getInstance().getLoggedInUser().token);
            js.put("namespace", UserManager.getInstance().getLoggedInUser().nameSpace);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }




//    private void startPubNubService() {
////        Intent intent = new Intent(this, PubNubBackgroundService.class);
////        startService(intent);
//    }



    @Override
    public void onFragmentBackStackChanged() {
        super.onFragmentBackStackChanged();
        if (getCurrentFragment() != null)
            mTitle.setText(getCurrentFragment().getTitle());
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.img_back) {

            AboutFragment.isFromAboutScreen = false;
            onBack();
        }else if(v.getId() == R.id.menu_container) {
            AboutFragment.isFromAboutScreen = false;
        }else if(v.getId() == R.id.tbToggleTheme1){

                if(tbSwitchTheme1.isChecked())
                {
                    UserPrefs.setTheme(this,1);
                    loadTheme();
                }else {
                    UserPrefs.setTheme(this,0);
                    loadTheme();
                }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Dmoat.isAppRunning = false;
        Constants.dashBoardActivity = null;
//        AlertsHandler.getInstance(getApplicationContext()).onDestroy();

    }

//    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        if (getCurrentFragment() != null && !(getCurrentFragment() instanceof ViewAlertsAdvancedFragment)) {
//            Intent AlertsMainFragmentintent = new Intent(this, AlertsMainActivity.class);
//            startActivity(AlertsMainFragmentintent);
////            popBackStack();
//        }
//    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (menuContainer.getVisibility() == View.VISIBLE) {
//
//            return true;
//        }
//        if ( keyCode == KeyEvent.KEYCODE_MENU ) {
//
//        //    return super.onKeyDown(keyCode, event);
//        }
        return super.onKeyDown(keyCode, event);
    }


    private ProgressDialog dialog;

    public void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(this, "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }


    private boolean doubleBackToExit = false;
    @Override
    public void onBackPressed() {
//        int count = getSupportFragmentManager().getBackStackEntryCount();
//        if(count==1)
//
//        {
//            if(doubleBackToExit)
//                finish();
//            this.doubleBackToExit = true;
//            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
//
//            new Handler().postDelayed(() -> doubleBackToExit=false, 2000);
//        }
//        else
//        {
//            Backbuttoninfragments();
//        }
        Backbuttoninfragments();
    }



    public static void showAlertMsgDialog(Context ctx, String msg) {
        if (msg.equalsIgnoreCase("") || msg == null || msg.isEmpty() || msg.equals(null)) {
            msg = ctx.getResources().getString(R.string.error_message_generic);
        } else {
        }
        new AlertDialog.Builder(ctx)
                .setTitle("Prytex Hardware")
                .setMessage(msg)
                .setPositiveButton("Ok", (dialog, whichButton) -> {
                }).show();

    }

    private static void showAlertInfoDialog(Context ctx, String msg) {

        new AlertDialog.Builder(ctx)
                .setTitle("Prytex Hardware")
                .setMessage(msg)
                .setPositiveButton("OK", (dialog, whichButton) -> dialog.dismiss()).show();
//                .setNegativeButton("Cancel", null).show();

    }

    private void ShowScreenInfo() {
        Fragment fragmentInFrame = getSupportFragmentManager()
                .findFragmentById(R.id.fragment_container);
         if (fragmentInFrame instanceof DashBoardFragment) {
            Log.d("debug", "you are in ChooseFilterFragment");

        } else if (fragmentInFrame instanceof ChooseFilterFragment) {
            Log.d("debug", "you are in ChooseFilterFragment");

        } else if (fragmentInFrame instanceof BlockedAlertsFragment) {

            Log.d("debug", "you are in BlockedAlertsFragment");

        } else if (fragmentInFrame instanceof MutedAlertsFragment) {
            Log.d("debug", "you are in MutedAlertsFragment");
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.mutedAlerts_screen_txt));

        }

        else if (fragmentInFrame instanceof ProtocolsListViewMainFragment) {
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.bandwidth_screen_txt));
            Log.d("debug", "you are in BandwidthHostDetailsFragment");

        } else if (fragmentInFrame instanceof BandwidthHostDetailsFragment) {
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.bandwidthDetails_screen_txt));
            Log.d("debug", "you are in BandwidthHostDetailsFragment");

        } else if (fragmentInFrame instanceof SensorMainFragment) {
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.sensor_screen_txt));
            Log.d("debug", "you are in SensorMainFragment");

        }

        else if (fragmentInFrame instanceof OpenPortsFragment || fragmentInFrame instanceof RiskProfileMainFragment || fragmentInFrame instanceof DefaultPwdsFragment) {
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.open_ports_screen_txt));
            Log.d("debug", "you are in open ports fragment");

        }

        else if (fragmentInFrame instanceof VulnerabilityFragment) {

            Log.d("debug", "you are in VulnerabilityFragment");
        }

        else if (fragmentInFrame instanceof FaqsFragment) {

            Log.d("debug", "you are in FaqsFragment");
        } else if (fragmentInFrame instanceof ChangePasswordFragment) {

            Log.d("debug", "you are in ChangePasswordFragment");
        } else if (fragmentInFrame instanceof VerifyCodeFragment) {

            Log.d("debug", "you are in VerifyCodeFragment");
        } else if (fragmentInFrame instanceof PoliciesMainFragment) {
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.policy_screen_txt));
            Log.d("debug", "you are in PoliciesFragment");
        } else if (fragmentInFrame instanceof PolicyPlanFragment) {
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.policy_screen_txt));
            Log.d("debug", "you are in PoliciesFragment");
        } else if (fragmentInFrame instanceof ProfileFragment) {
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.profileAccount_screen_txt));
            Log.d("debug", "you are in PoliciesFragment");
        } else if (fragmentInFrame instanceof ProtocolPolicyControlFragment) {
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.protocol_manager_screen_text));
            Log.d("debug", "you are in PoliciesFragment");
        } else if (fragmentInFrame instanceof HostSelectionFragment){
            imgInfoBtn.setVisibility(View.GONE);
        }
        else {

        }

    }


    private void setTabBarSelection() {
        Fragment fragmentInFrame = getSupportFragmentManager()
                .findFragmentById(R.id.fragment_container);

//        if (fragmentInFrame instanceof AlertsMainActivity) {
//            Log.d("debug", "you are in ViewAlertsAdvancedFragment");
//            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
//
//        } else
            if (fragmentInFrame instanceof DashBoardFragment) {
            Log.d("debug", "you are in ChooseFilterFragment");
            setBottomNavTextColors(getApplicationContext(), true, false, false, false);

        }
//        else if (fragmentInFrame instanceof ConnectedDeviceActivity) {
//            Log.d("debug", "you are in ChooseFilterFragment");
//            setBottomNavTextColors(getApplicationContext(), true, false, false, false);
//
//        }
//        else if (fragmentInFrame instanceof BandwidthChartMainActivity) {
//            Log.d("debug", "you are in ChooseFilterFragment");
//            setBottomNavTextColors(getApplicationContext(), true, false, false, false);
//
//        }
        else if (fragmentInFrame instanceof SensorMainFragment) {
            Log.d("debug", "you are in ChooseFilterFragment");
            setBottomNavTextColors(getApplicationContext(), true, false, false, false);

        }
//        else if (fragmentInFrame instanceof ConnectedDeviceActivity) {
//            Log.d("debug", "you are in ChooseFilterFragment");
//            setBottomNavTextColors(getApplicationContext(), true, false, false, false);
//
//        }
        else if (fragmentInFrame instanceof ChooseFilterFragment) {
            Log.d("debug", "you are in ChooseFilterFragment");
            setBottomNavTextColors(getApplicationContext(), false, false, false, false);

        } else if (fragmentInFrame instanceof BlockedAlertsFragment) {

            Log.d("debug", "you are in BlockedAlertsFragment");
            setBottomNavTextColors(getApplicationContext(), false, false, false, false);

        } else if (fragmentInFrame instanceof MutedAlertsFragment) {
            Log.d("debug", "you are in MutedAlertsFragment");

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
        }
//        else if (fragmentInFrame instanceof ProtocolChartFragment || fragmentInFrame instanceof BandwidthChartMainActivity || fragmentInFrame instanceof ProtocolsListViewMainFragment || fragmentInFrame instanceof ProtocolListViewFragment) {
//            Log.d("debug", "you are in ProtocolChartFragment");
//            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
//
//        }
        else if (fragmentInFrame instanceof ProtocolsListViewMainFragment) {

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in BandwidthHostDetailsFragment");

        } else if (fragmentInFrame instanceof BandwidthHostDetailsFragment) {

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in BandwidthHostDetailsFragment");

        } else if (fragmentInFrame instanceof SensorMainFragment) {

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in SensorMainFragment");

        }
//        else if (fragmentInFrame instanceof ConnectedDeviceActivity) {
//
//            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
//            Log.d("debug", "you are in ConnectedDeviceActivity");
//
//        }
        else if (fragmentInFrame instanceof VulnerabilityListFragment) {

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in VulnerabilityListFragment");

        } else if (fragmentInFrame instanceof VulnerabilityDetailsFragment) {

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in VulnerabilityDetailsFragment");

        } else if (fragmentInFrame instanceof VulnerabilityFragment) {

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in VulnerabilityFragment");
        }
     //   else if (fragmentInFrame instanceof AboutFragment) {

       //     setBottomNavTextColors(getApplicationContext(), false, false, false, false);
       //     Log.d("debug", "you are in AboutFragment");
        //}
        else if (fragmentInFrame instanceof FaqsFragment) {

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in FaqsFragment");
        } else if (fragmentInFrame instanceof ChangePasswordFragment) {
            llBottomTabs.setVisibility(View.GONE);
            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in ChangePasswordFragment");
        }
        //else if (fragmentInFrame instanceof TermsPolicyAgreementFragment) {
           // llBottomTabs.setVisibility(View.GONE);
         //   Log.d("debug", "you are in ChangePasswordFragment");
       // }
        else if (fragmentInFrame instanceof VerifyCodeFragment) {

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in VerifyCodeFragment");
        } else if (fragmentInFrame instanceof PoliciesFragment || fragmentInFrame instanceof PauseInternetFragment) {
            setBottomNavTextColors(getApplicationContext(), false, true, false, false);
            Log.d("debug", "you are in PoliciesFragment");
        } else if (fragmentInFrame instanceof ProfileFragment) {
            setBottomNavTextColors(getApplicationContext(), false, false, true, false);
            Log.d("debug", "you are in PoliciesFragment");
        } else if (fragmentInFrame instanceof PolicyPlanFragment) {
            llBottomTabs.setVisibility(View.GONE);
            setBottomNavTextColors(getApplicationContext(), false, true, false, false);
            Log.d("debug", "you are in PoliciesFragment");
        } else {

        }
    }

    public static void setBottomNavTextColors(Context context, boolean isHome, boolean isPolicy, boolean isAccount, boolean isHelp) {
        if (UserPrefs.getTheme(context) == 0) {
            try {
                btnHomeDashboard.setTextColor(context.getResources().getColor(R.color.unselected_text_color));
                btnPolicyDashboard.setTextColor(context.getResources().getColor(R.color.unselected_text_color));
                btnAccountDashboard.setTextColor(context.getResources().getColor(R.color.unselected_text_color));
                btnHelpDashboard.setTextColor(context.getResources().getColor(R.color.unselected_text_color));
                imgHomeTab.setImageResource(R.drawable.tab_home_unselected);
                imgPolicyTab.setImageResource(R.drawable.tab_policy_unselected);
                imgAccountTab.setImageResource(R.drawable.tab_account_unselected);
                imgHelpTab.setImageResource(R.drawable.tab_help_unselected);
                if (isHome) {
                    imgHomeTab.setImageResource(R.drawable.tab_home_selected);
                    btnHomeDashboard.setTextColor(context.getResources().getColor(R.color.selected_text_color));
                } else if (isPolicy) {
                    imgPolicyTab.setImageResource(R.drawable.tab_policy_selected);
                    btnPolicyDashboard.setTextColor(context.getResources().getColor(R.color.selected_text_color));
                } else if (isAccount) {
                    imgAccountTab.setImageResource(R.drawable.tab_account_selected);
                    btnAccountDashboard.setTextColor(context.getResources().getColor(R.color.selected_text_color));
                } else if (isHelp) {
                    imgHelpTab.setImageResource(R.drawable.tab_help_selected);
                    btnHelpDashboard.setTextColor(context.getResources().getColor(R.color.selected_text_color));
                } else {
                }
            } catch (Exception e) {
                Log.d("setNavBar", e.toString());
            }
        }else if (UserPrefs.getTheme(context) == 1){

            btnHomeDashboard.setTextColor(context.getResources().getColor(R.color.unselected_text_color));
            btnPolicyDashboard.setTextColor(context.getResources().getColor(R.color.unselected_text_color));
            btnAccountDashboard.setTextColor(context.getResources().getColor(R.color.unselected_text_color));
            btnHelpDashboard.setTextColor(context.getResources().getColor(R.color.unselected_text_color));
            imgHomeTab.setImageResource(R.drawable.tab_home_unselected);
            imgPolicyTab.setImageResource(R.drawable.tab_policy_unselected);
            imgAccountTab.setImageResource(R.drawable.tab_account_unselected);
            imgHelpTab.setImageResource(R.drawable.tab_help_unselected);

            if (isHome) {
                imgHomeTab.setImageResource(R.drawable.tab_help_selected);
                btnHomeDashboard.setTextColor(context.getResources().getColor(R.color.theme1_tabbar_txt_color_selected));
                title_layout.setVisibility(View.GONE);
            } else if (isPolicy) {
                imgPolicyTab.setImageResource(R.mipmap.policy_icon_selected);
                btnPolicyDashboard.setTextColor(context.getResources().getColor(R.color.selected_text_color));
                title_layout.setVisibility(View.VISIBLE);
            } else if (isAccount) {
                imgAccountTab.setImageResource(R.mipmap.account_icon_selected);
                btnAccountDashboard.setTextColor(context.getResources().getColor(R.color.selected_text_color));
                title_layout.setVisibility(View.VISIBLE);
            } else if (isHelp) {
                imgHelpTab.setImageResource(R.mipmap.support_icon_selected);
                btnHelpDashboard.setTextColor(context.getResources().getColor(R.color.selected_text_color));
                title_layout.setVisibility(View.VISIBLE);
            }
        }

    }

    private void subscribeToGCM() {

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    Log.d("TokenSent", "sent to server");

                } else {
                    Log.d("TokenSent", "not sent to server");
                }
            }
        };

        // Registering BroadcastReceiver

      //  registerReceiver();
//        if (checkPlayServices()) {
//            // Start IntentService to register this application with GCM.
//            Intent intent = new Intent(getApplicationContext(), RegistrationIntentService.class);
//            getApplicationContext().startService(intent);
//        }

    }

    private void registerReceiver() {
        if (!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(getApplicationContext());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog((Activity) getApplicationContext(), resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }



    private void openSupportConversation() {

        if ( UserManager.getInstance().getLoggedInUser().nameSpace == null) {
            PreChatForm preChatForm = new PreChatForm.Builder()
                    .name(PreChatForm.Field.REQUIRED_EDITABLE)
                    .email(PreChatForm.Field.REQUIRED_EDITABLE)
                    .phoneNumber(PreChatForm.Field.REQUIRED_EDITABLE)
                    .department(PreChatForm.Field.REQUIRED_EDITABLE)
                    .message(PreChatForm.Field.OPTIONAL_EDITABLE)
                    .build();

//            // build chat config
            ZopimChat.SessionConfig config = new ZopimChat.SessionConfig().preChatForm(preChatForm).department("Support");

            // start chat activity with config
//            Intent intent  = new Intent(getApplicationContext(), ZopimChatActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//            ZopimChatActivity.startActivity(getApplicationContext(), config);

            // Sample breadcrumb
            ZopimChat.trackEvent("Started chat with pre-set visitor information");
        }
        else {
            VisitorInfo visitorInfo = new VisitorInfo.Builder()
//                    .phoneNumber("+0909090900909")
                    .email(UserManager.getInstance().getLoggedInUser().email)
                    .name(UserManager.getInstance().getLoggedInUser().name)
                    .build();

            // visitor info can be set at any point when that information becomes available
            ZopimChat.setVisitorInfo(visitorInfo);

            // set pre chat fields as mandatory
            PreChatForm preChatForm = new PreChatForm.Builder()
                    .name(PreChatForm.Field.REQUIRED_EDITABLE)
                    .email(PreChatForm.Field.REQUIRED_EDITABLE)
                    .phoneNumber(PreChatForm.Field.NOT_REQUIRED)
                    .department(PreChatForm.Field.REQUIRED_EDITABLE)
                    .message(PreChatForm.Field.REQUIRED_EDITABLE)
                    .build();

            // build chat config
//            ZopimChat.SessionConfig config = new ZopimChat.SessionConfig().preChatForm(preChatForm).department("Support");
//
//            // start chat activity with config
//            Intent intent  = new Intent(getApplicationContext(), ZopimChatActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//            ZopimChatActivity.startActivity(getApplicationContext(), config);

            // Sample breadcrumb
            ZopimChat.trackEvent("Started chat with pre-set visitor information");

        }
    }

    private void loadFragment(android.app.Fragment fragment, String x) {

        android.app.FragmentManager fm = getFragmentManager();

        android.app.FragmentTransaction fragmentTransaction = fm.beginTransaction();


        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.addToBackStack(x);
        fragmentTransaction.commit(); // save the changes
    }

    public void HideScreen() {
        Fragment fragmentInFrame = getSupportFragmentManager()
                .findFragmentById(R.id.fragment_container);

//        if (fragmentInFrame instanceof AlertsMainActivity) {
//            Log.d("debug", "you are in ViewAlertsAdvancedFragment");
//            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.viewAlerts_screen_txt));
//
//        } else
        if (fragmentInFrame instanceof DashBoardFragment) {
            Log.d("debug", "you are in ChooseFilterFragment");

        } else if (fragmentInFrame instanceof ChooseFilterFragment) {
            Log.d("debug", "you are in ChooseFilterFragment");

        } else if (fragmentInFrame instanceof BlockedAlertsFragment) {

            Log.d("debug", "you are in BlockedAlertsFragment");

        } else if (fragmentInFrame instanceof MutedAlertsFragment) {
            Log.d("debug", "you are in MutedAlertsFragment");
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.mutedAlerts_screen_txt));

        }
//        else if (fragmentInFrame instanceof ProtocolChartFragment || fragmentInFrame instanceof BandwidthChartMainActivity || fragmentInFrame instanceof ProtocolsListViewMainFragment || fragmentInFrame instanceof ProtocolListViewFragment) {
//            Log.d("debug", "you are in ProtocolChartFragment");
//            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.bandwidth_screen_txt));
//
//        }
        else if (fragmentInFrame instanceof ProtocolsListViewMainFragment) {
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.bandwidth_screen_txt));
            Log.d("debug", "you are in BandwidthHostDetailsFragment");

        } else if (fragmentInFrame instanceof BandwidthHostDetailsFragment) {
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.bandwidthDetails_screen_txt));
            Log.d("debug", "you are in BandwidthHostDetailsFragment");

        } else if (fragmentInFrame instanceof SensorMainFragment) {
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.sensor_screen_txt));
            Log.d("debug", "you are in SensorMainFragment");

        }
//        else if (fragmentInFrame instanceof ConnectedDeviceActivity) {
//            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.connectedDevices_screen_txt));
//            Log.d("debug", "you are in ConnectedDeviceActivity");
//
//        }
        else if (fragmentInFrame instanceof OpenPortsFragment || fragmentInFrame instanceof RiskProfileMainFragment || fragmentInFrame instanceof DefaultPwdsFragment) {
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.open_ports_screen_txt));
            Log.d("debug", "you are in open ports fragment");

        }
//        else if (fragmentInFrame instanceof DefaultPwdsFragment) {
//            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.default_passwords_screen_txt));
//            Log.d("debug", "you are in DefaultPwdsFragment default pwd's fragment");
//
//        }
        else if (fragmentInFrame instanceof VulnerabilityFragment) {

            Log.d("debug", "you are in VulnerabilityFragment");
        }
        //else if (fragmentInFrame instanceof AboutFragment) {

          //  Log.d("debug", "you are in AboutFragment");
        //}
        else if (fragmentInFrame instanceof FaqsFragment) {

            Log.d("debug", "you are in FaqsFragment");
        } else if (fragmentInFrame instanceof ChangePasswordFragment) {

            Log.d("debug", "you are in ChangePasswordFragment");
        } else if (fragmentInFrame instanceof VerifyCodeFragment) {

            Log.d("debug", "you are in VerifyCodeFragment");
        } else if (fragmentInFrame instanceof PoliciesMainFragment) {
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.policy_screen_txt));
            Log.d("debug", "you are in PoliciesFragment");
        } else if (fragmentInFrame instanceof PolicyPlanFragment) {
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.policy_screen_txt));
            Log.d("debug", "you are in PoliciesFragment");
        } else if (fragmentInFrame instanceof ProfileFragment) {
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.profileAccount_screen_txt));
            Log.d("debug", "you are in PoliciesFragment");
        } else if (fragmentInFrame instanceof ProtocolPolicyControlFragment) {
            showAlertInfoDialog(this, getApplicationContext().getResources().getString(R.string.protocol_manager_screen_text));
            Log.d("debug", "you are in PoliciesFragment");
        } else if (fragmentInFrame instanceof HostSelectionFragment){
            imgInfoBtn.setVisibility(View.GONE);
        }
        else {

        }

    }

    private void Backbuttoninfragments()
       {
        Fragment fragmentInFrame = getSupportFragmentManager()
                .findFragmentById(R.id.fragment_container);
        if (fragmentInFrame instanceof ChangePasswordFragment) {
            dashboardSelectedbutton = "dasboardPressed";

            imgInfoBtn.setImageResource(R.drawable.info);
            setBottomNavTextColors(getApplicationContext(), false, false, true, false);
            addFragment(new ProfileFragment(), true, true);
        }
        else if (fragmentInFrame instanceof ThemeFragment){
            imgInfoBtn.setImageResource(R.drawable.info);
            setBottomNavTextColors(getApplicationContext(), false, false, true, false);
            addFragment(new ProfileFragment(), true, true);
        }
//        if (fragmentInFrame instanceof PoliciesMainFragment)
//        {
//            BacktoDashboardfromfrag();
//        }
        else if (fragmentInFrame instanceof PolicyPlanFragment)
        {
            imgInfoBtn.setImageResource(R.drawable.info);
            setBottomNavTextColors(getApplicationContext(), false, true, false, false);
            addFragment(new PoliciesMainFragment(), true, true);
        }
        else if (fragmentInFrame instanceof HostSelectionFragment)
        {
            imgInfoBtn.setImageResource(R.drawable.info);
            setBottomNavTextColors(getApplicationContext(), false, true, false, false);
            addFragment(new PoliciesMainFragment(), true, true);
        }
        else if (fragmentInFrame instanceof ProtocolPolicyControlFragment)
        {
            imgInfoBtn.setImageResource(R.drawable.info);
            setBottomNavTextColors(getApplicationContext(), false, true, true, false);
            addFragment(new PoliciesMainFragment(), true, true);
        }

        else if (fragmentInFrame instanceof SensorMainFragment)
        {
            BacktoDashboardfromfrag();
        }
        else if (fragmentInFrame instanceof NetConfigFragment)
        {
            imgInfoBtn.setImageResource(R.drawable.info);
            setBottomNavTextColors(getApplicationContext(), false, false, true, false);
            addFragment(new ProfileFragment(), true, true);
        }
        else if (fragmentInFrame instanceof DashBoardFragment || fragmentInFrame instanceof PoliciesMainFragment ||
                fragmentInFrame instanceof ProfileFragment || fragmentInFrame instanceof HelpTutorialFragment){
            doubleClickBackFunctionality();
        }
        else if (fragmentInFrame instanceof FaqsFragment){
            setBottomNavTextColors(getApplicationContext(), false, false, false, true);
            addFragment(new HelpTutorialFragment(), true, true);
        }
    //    if (fragmentInFrame instanceof AboutFragment)
     //   {
//            imgInfoBtn.setImageResource(R.drawable.info);
//            setBottomNavTextColors(getApplicationContext(), false, false, true, false);
//            addFragment(new ProfileFragment(), true, true);
      //  }
    }

    private void doubleClickBackFunctionality(){
        if(doubleBackToExit)
            finish();
        this.doubleBackToExit = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExit=false, 2000);
    }

    private void BacktoDashboardfromfrag()
    {
        finish();
        getIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivityForResult(getIntent(), 0);
        overridePendingTransition(0,0);


    }


    //Changes by umer, this method will load theme according to the user preference and call api accordingly
    public void loadTheme(){
        if (UserPrefs.getTheme(getApplicationContext())==0){
            title_layout.setVisibility(View.VISIBLE);
            llBottomTabs.setBackgroundColor(getResources().getColor(R.color.dashboard_background));
            tbSwitchTheme1.setChecked(false);

            replaceFragmentWithAnim(new DashBoardFragment());
            if (UserManager.getInstance().isUserLoggedIn()){
                mTitle.setText(UserManager.getInstance().getLoggedInUser().name + "\'s Prytex Hardware");
            }
        }
        else if (UserPrefs.getTheme(getApplicationContext())==1){
            title_layout.setVisibility(View.GONE);
            replaceFragmentWithAnim(new FragmentThemeOne());
        }

        changeMenuBar();
    }

    public void changeMenuBar(){
      //  if (UserPrefs.getTheme(getApplicationContext()) == 0){
            menubar.setBackgroundColor(getResources().getColor(R.color.dashboard_background));

            imgHomeTab.setImageResource(R.drawable.tab_home_selected);
            imgPolicyTab.setImageResource(R.drawable.tab_policy_unselected);
            imgAccountTab.setImageResource(R.drawable.tab_account_unselected);
            imgHelpTab.setImageResource(R.drawable.tab_help_unselected);


            btnHomeDashboard.setTextColor(getResources().getColor(R.color.unselected_text_color));
            btnPolicyDashboard.setTextColor(getResources().getColor(R.color.unselected_text_color));
            btnAccountDashboard.setTextColor(getResources().getColor(R.color.unselected_text_color));
            btnHelpDashboard.setTextColor(getResources().getColor(R.color.unselected_text_color));
            llBottomTabs.setVisibility(View.VISIBLE);
     //   }
//        else{
//            imgHomeTab.setImageResource(R.mipmap.home_icon_selected);
//            imgPolicyTab.setImageResource(R.mipmap.policy_unselected);
//            imgAccountTab.setImageResource(R.mipmap.account_icon_unselected);
//            imgHelpTab.setImageResource(R.mipmap.support_icon_unselected);
//
//            btnHomeDashboard.setTextColor(getResources().getColor(R.color.theme1_tabbar_txt_color_selected));
//            btnPolicyDashboard.setTextColor(getResources().getColor(android.R.color.white));
//            btnAccountDashboard.setTextColor(getResources().getColor(android.R.color.white));
//            btnHelpDashboard.setTextColor(getResources().getColor(android.R.color.white));
//            if (isDmoatOnline){
//                menubar.setBackgroundResource(R.mipmap.menubar_online);
//            }else{
//                menubar.setBackgroundResource(R.mipmap.menubar_online);
//            }
//            llBottomTabs.setVisibility(View.VISIBLE);
//
//      }
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        Fragment fragmentInFrame = getSupportFragmentManager()
//                .findFragmentById(R.id.fragment_container);
//        if (fragmentInFrame instanceof DashBoardFragment || fragmentInFrame instanceof FragmentThemeOne){
//            llToggleTheme1.setVisibility(View.VISIBLE);
//            imgInfoBtn.setVisibility(View.GONE);
//        }else{
//            imgInfoBtn.setImageResource(R.drawable.info);
//            llToggleTheme1.setVisibility(View.GONE);
//            imgInfoBtn.setVisibility(View.VISIBLE);
//        }
//
//    }
}
