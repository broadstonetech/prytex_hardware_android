package hardware.prytex.io.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import hardware.prytex.io.R;
import hardware.prytex.io.fragment.BandwidthFrags.BandwidthChartFrags.TabsPagerAdapter;


public class BandwidthChartMainActivity extends BaseActivity implements View.OnClickListener {


    @Override
    public int getLayoutId() {
        return R.layout.fragment_bandwidth_chart_main;
    }



    @Override
    public void initViews(Bundle savedInstanceState) {
  super.initViews(savedInstanceState);

        Context mContext = this;
        DashBoardActivity.setBottomNavTextColors(mContext, false, false, false, false);
        Button btnHostsDetails = findViewById(R.id.btnHostsDetails);
        btnHostsDetails.setText(getResources().getString(R.string.show_details));

        btnHostsDetails.setOnClickListener(view -> {
           Intent intent = new Intent(getApplicationContext(), BandwidthListActivity.class);
            startActivity(intent);

                   });

        ImageView backimage = findViewById(R.id.img_back);
        backimage.setEnabled(false);
        backimage.setVisibility(View.GONE);
        backimage.setOnClickListener(this);
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Current Data"));
        tabLayout.addTab(tabLayout.newTab().setText("24 hour Data"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        //final ViewPager viewPager = findViewById(R.id.pager);
        final NonSwipeableViewPager viewPager = findViewById(R.id.non_swipable_view_pagers);
        final TabsPagerAdapter adapter = new TabsPagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount(), mContext);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.beginFakeDrag();
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 0) {
                 //   SPManager.setTermsServicesStatus(mContext, true);     // commented by bilawal when fragment is replaced with activity
                    Log.d("Tab", "curent hour TAB selsected");
                } else {
               //     SPManager.setTermsServicesStatus(mContext, false);   // commented by bilawal when fragment is replaced with activity
                    Log.d("Tab", "24 hour TAB selsected");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        ImageView imgInfoBtn = findViewById(R.id.img_info);
        imgInfoBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.img_back) {
            onBackPressed();
        }else if(v.getId() == R.id.img_info) {
            screenInfoDialog(getResources().getString(R.string.bandwidth_screen_txt));
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        backButoonPressed();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

}
