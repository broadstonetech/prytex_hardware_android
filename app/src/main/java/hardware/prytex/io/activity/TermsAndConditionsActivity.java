package hardware.prytex.io.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import hardware.prytex.io.R;
import hardware.prytex.io.util.SPManager;

/**
 * Created by abubaker on 10/14/16.
 */

public class TermsAndConditionsActivity extends BaseActivity implements View.OnClickListener{

    private SharedPreferences.Editor editor;


    @Override
    public int getLayoutId() {
        return R.layout.terms_of_service_fragment;
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public void initViews(Bundle savedInstanceState) {
        super.initViews(savedInstanceState);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();

        RelativeLayout agreeButton = findViewById(R.id.agree);
        RelativeLayout disAgreeButton = findViewById(R.id.disagree);
        agreeButton.setOnClickListener(this);
        disAgreeButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.agree) {


            editor.putString("isLogged", "logged");
            editor.commit();
            SPManager.setTermsServicesStatus(TermsAndConditionsActivity.this, true);
            startActivity(new Intent(TermsAndConditionsActivity.this, AuthenticationActivity.class));
            TermsAndConditionsActivity.this.finish();

        }else if(v.getId() == R.id.disagree)

                finish();
    }
}
