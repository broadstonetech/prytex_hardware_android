package hardware.prytex.io.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import hardware.prytex.io.Interfaces.ChatListener;
import com.zopim.android.sdk.api.Chat;
import com.zopim.android.sdk.api.ZopimChat;
import com.zopim.android.sdk.embeddable.ChatActions;

import com.zopim.android.sdk.prechat.PreChatForm;
import com.zopim.android.sdk.prechat.ZopimChatFragment;
import com.zopim.android.sdk.chatlog.ZopimChatLogFragment;
import com.zopim.android.sdk.widget.ChatWidgetService;

import hardware.prytex.io.R;

///**
// * Activity that configures the {@link ZopimChatFragment} to start the chat with pre chat fields
// */
public class ZopimChatActivity extends AppCompatActivity implements ChatListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zopim_pre_chat_activity);
        // use toolbar as action bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Chat");

        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        setSupportActionBar(toolbar);

//        toolbar.setNavigationIcon(R.drawable.backarrow);
//
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(ZopimChatActivity.this, DashBoardActivity.class));
//                finish();
//            }
//        });
        // orientation change
        if (savedInstanceState != null) {
            return;
        }

        boolean widgetWasActive = stopService(new Intent(this, ChatWidgetService.class));
        if (widgetWasActive) {
            resumeChat();
            return;
        }

        if (getIntent() != null) {
            String action = getIntent().getAction();
            if (ChatActions.ACTION_RESUME_CHAT.equals(action)) {
                resumeChat();
                return;
            }
        }

        Chat chat = ZopimChat.resume(this);
        if (!chat.hasEnded()) {
            resumeChat();
            return;
        }

        {
            // set pre chat fields as mandatory
            PreChatForm preChatForm = new PreChatForm.Builder()
                    .name(PreChatForm.Field.REQUIRED_EDITABLE)
                    .email(PreChatForm.Field.REQUIRED_EDITABLE)
                    .phoneNumber(PreChatForm.Field.OPTIONAL_EDITABLE)
                    .department(PreChatForm.Field.REQUIRED_EDITABLE)
                    .message(PreChatForm.Field.REQUIRED_EDITABLE)
                    .build();
            // build chat config
            ZopimChat.SessionConfig config = new ZopimChat.SessionConfig()
                    .preChatForm(preChatForm);
            // prepare chat fragment
            ZopimChatFragment fragment = ZopimChatFragment.newInstance(config);
            // show fragment
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.chat_fragment_container, fragment, ZopimChatFragment.class.getName());
            transaction.commit();
        }
    }

    /**
     * Resumes the chat and loads the {@link ZopimChatLogFragment}
     */
    private void resumeChat() {

        FragmentManager manager = getSupportFragmentManager();
        // find the retained fragment
        if (manager.findFragmentByTag(ZopimChatLogFragment.class.getName()) == null) {
            ZopimChatLogFragment chatLogFragment = new ZopimChatLogFragment();

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(com.zopim.android.sdk.R.id.chat_fragment_container, chatLogFragment, ZopimChatLogFragment.class.getName());
            transaction.commit();
        }
    }

}

