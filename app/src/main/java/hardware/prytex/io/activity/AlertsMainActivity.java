package hardware.prytex.io.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import hardware.prytex.io.R;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import hardware.prytex.io.fragment.AlertsTabs.AlertsPagerAdapter;
import hardware.prytex.io.fragment.Policy.PoliciesMainFragment;
import hardware.prytex.io.fragment.ProfileFragment;
import hardware.prytex.io.model.UserManager;
import com.zopim.android.sdk.api.ZopimChat;
import com.zopim.android.sdk.model.VisitorInfo;
import com.zopim.android.sdk.prechat.PreChatForm;

import java.util.ArrayList;
import java.util.List;

import static hardware.prytex.io.activity.AuthenticationActivity.alertsList;
import static hardware.prytex.io.activity.DashBoardActivity.setBottomNavTextColors;
import static hardware.prytex.io.parser.Pre_MutedAlertsParser.listOfPreMutedAlerts;

public class AlertsMainActivity extends BaseActivity implements View.OnClickListener {
    private ProgressDialog dialog;
    private Context mContext;
    private ViewPager viewPager;
    public static boolean isFirstCallViewAlerts = false;
    public static boolean isFirstCallBlockedAlerts = false;
    public static boolean isFirstCallMutedAlerts = false;
    public static boolean isFirstCallPreMutedAlerts = false;
    public static int filterCountAlerts = 0;
    public static int filterCountBlocked = 0;

    public static int filterCountMuted = 0;
    public static final List<String> alertfilterItems = new ArrayList<>();
    public static TabLayout tabLayout;

    private TextView vViewAlerts, vBlockedAlerts, vPreMutedAlerts;
    private TextView tvViewAlertHeading;
    private TextView tvBlockedHeading;
    private TextView tvPreMutedHeading;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_alerts_main;
    }



    @Override
    public void initViews(Bundle savedInstanceState) {
        super.initViews(savedInstanceState);
        mContext = AlertsMainActivity.this;
        setBottomNavTextColors(mContext, false, false, false, false);
        alertsList.clear();
        tabLayout = findViewById(R.id.tab_layout);


        ImageView backimage = findViewById(R.id.img_back);
        ImageView imgInfoBtn = findViewById(R.id.img_info);
        tvViewAlertHeading = findViewById(R.id.tvViewAlertsHeading);
        tvBlockedHeading = findViewById(R.id.tvBlockedheading);
        tvPreMutedHeading = findViewById(R.id.tvPreMutedHeading);



        vViewAlerts = findViewById(R.id.vAllAlerts);
        vBlockedAlerts = findViewById(R.id.vBlockedAlerts);
        vPreMutedAlerts = findViewById(R.id.vPreMutedAlerts);

        tabLayout.addTab(tabLayout.newTab().setText("New Alerts"));
        tabLayout.addTab(tabLayout.newTab().setText("Blocked Events"));
//        tabLayout.addTab(tabLayout.newTab().setText("Muted Alerts"));
        tabLayout.addTab(tabLayout.newTab().setText("Observed Events"));

        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);


        viewPager = findViewById(R.id.pager);
        final AlertsPagerAdapter adapter = new AlertsPagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount(), 0);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 0) {
                    changeTopNavBarSelection(true, false, false);
                    if (isFirstCallViewAlerts) {
                    }
                    isFirstCallViewAlerts = false;
                    Log.d("Tab", "view alerts TAB selsected");
                } else if (tab.getPosition() == 1) {
                    changeTopNavBarSelection(false, true, false);
                    isFirstCallBlockedAlerts = false;
                    Log.d("Tab", "blocked alerts TAB selsected");
                }
                else {
                    changeTopNavBarSelection(false, false, true);
                    listOfPreMutedAlerts.size();
                    isFirstCallPreMutedAlerts = false;
                    Log.d("Tab", "pre-muted TAB selsected");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        backimage.setOnClickListener(this);
        imgInfoBtn.setOnClickListener(this);


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isFirstCallViewAlerts = true;
        isFirstCallBlockedAlerts = true;
        isFirstCallMutedAlerts = true;
        isFirstCallPreMutedAlerts = true;
    }


    public void showProgressDialog(boolean show) {
        if (show) {
            if (dialog == null) {
                dialog = ProgressDialog.show(mContext, "", "Please wait...", true);
                dialog.setCancelable(false);
            }
            dialog.show();
        } else {
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    private void changeTopNavBarSelection(boolean isViewAlerts, boolean isBlocked, boolean isPreMuted) {
        if (isViewAlerts) {
            tvViewAlertHeading.setTextColor(getResources().getColor(R.color.white));
            tvBlockedHeading.setTextColor(getResources().getColor(R.color.light_grey));
            tvPreMutedHeading.setTextColor(getResources().getColor(R.color.light_grey));

            vViewAlerts.setBackgroundColor(getResources().getColor(R.color.white));
            vBlockedAlerts.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            vPreMutedAlerts.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        } else if (isBlocked) {
            tvBlockedHeading.setTextColor(getResources().getColor(R.color.white));
            tvViewAlertHeading.setTextColor(getResources().getColor(R.color.light_grey));
            tvPreMutedHeading.setTextColor(getResources().getColor(R.color.light_grey));

            vViewAlerts.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            vBlockedAlerts.setBackgroundColor(getResources().getColor(R.color.white));
            vPreMutedAlerts.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        } else  if (isPreMuted) {
            tvPreMutedHeading.setTextColor(getResources().getColor(R.color.white));
            tvViewAlertHeading.setTextColor(getResources().getColor(R.color.light_grey));
            tvBlockedHeading.setTextColor(getResources().getColor(R.color.light_grey));

            vViewAlerts.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            vBlockedAlerts.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            vPreMutedAlerts.setBackgroundColor(getResources().getColor(R.color.white));
        }
    }





    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.img_back){
            onBackPressed();
        }else if(v.getId() == R.id.img_info){
            screenInfoDialog(getResources().getString(R.string.viewAlerts_screen_txt));
        }
    }


    @Override
    public void onBackPressed()
    {
        backButoonPressed();

    }



}
