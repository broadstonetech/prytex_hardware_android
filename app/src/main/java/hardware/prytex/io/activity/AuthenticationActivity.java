package hardware.prytex.io.activity;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import hardware.prytex.io.R;
import hardware.prytex.io.fragment.LoginFragment;
import hardware.prytex.io.FCM.MyFCMListenerService;
import hardware.prytex.io.FCM.QuickstartPreferences;

import hardware.prytex.io.model.UserManager;

import java.util.ArrayList;
import java.util.List;

public class AuthenticationActivity extends BaseActivity {

    private ProgressBar mProgressBar;
    public static final List<String> alertsList = new ArrayList<>();

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    // This method calls from HttpAsyncRequest class, when response is failure and code is 401.
    public static Intent startNewIntent(Context context){
        UserManager.getInstance().logout();
        ((BaseActivity) context).clearFragmentBackStack();
        MyFCMListenerService.removeNotificaitonFromStatusBar(context, 0);
        return new Intent(context, AuthenticationActivity.class);
    }

    @Override
    public void initViews(Bundle savedInstanceState) {
        super.initViews(savedInstanceState);
        mProgressBar = findViewById(R.id.progressbar);
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);

            }
        };

//         Registering BroadcastReceiver
      //  registerReceiver();
//
//        if (checkPlayServices()) {
//            // Start IntentService to register this application with GCM.
//            Intent intent = new Intent(this, RegistrationIntentService.class);
//            startService(intent);
//        }

        Log.d("Authentication Activity","Called");
        replaceFragment(new LoginFragment(),R.id.fragment_container, true, false);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showProgressBar() {
        super.showProgressBar();
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        super.hideProgressBar();
        mProgressBar.setVisibility(View.GONE);
    }


}
