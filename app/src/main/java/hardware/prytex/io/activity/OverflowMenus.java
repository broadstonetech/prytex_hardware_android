package hardware.prytex.io.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import hardware.prytex.io.R;
import hardware.prytex.io.fragment.AlertsTabs.BlockedAlertsFragment;
import hardware.prytex.io.fragment.AlertsTabs.MutedAlertsFragment;
import hardware.prytex.io.fragment.AlertsTabs.ViewAlertsAdvancedFragment;
import hardware.prytex.io.fragment.BandwidthFrags.BandwidthHostDetailsFragment;
import hardware.prytex.io.fragment.BandwidthFrags.ProtocolsListViewMainFragment;
import hardware.prytex.io.fragment.ChangePasswordFragment;
import hardware.prytex.io.fragment.ChooseFilterFragment;
import hardware.prytex.io.fragment.DashBoardFragment;
import hardware.prytex.io.fragment.FaqsFragment;
import hardware.prytex.io.AgentChatActivity.HelpTutorialFragment;
import hardware.prytex.io.fragment.Policy.AccessControl.PoliciesFragment;
import hardware.prytex.io.fragment.Policy.AccessControl.PolicyPlanFragment;
import hardware.prytex.io.fragment.Policy.PauseInternetFragment;
import hardware.prytex.io.fragment.Policy.PoliciesMainFragment;
import hardware.prytex.io.fragment.ProfileFragment;
import hardware.prytex.io.fragment.SensorTabBars.SensorMainFragment;
import hardware.prytex.io.fragment.VerifyCodeFragment;
import hardware.prytex.io.fragment.VulnerabilityDetailsFragment;
import hardware.prytex.io.fragment.VulnerabilityFragment;
import hardware.prytex.io.fragment.VulnerabilityListFragment;
import hardware.prytex.io.model.UserManager;
import hardware.prytex.io.util.GeneralUtils;
import com.zopim.android.sdk.api.ZopimChat;
import com.zopim.android.sdk.model.VisitorInfo;
import com.zopim.android.sdk.prechat.PreChatForm;

import hardware.prytex.io.fragment.AboutFragment;

import static hardware.prytex.io.FCM.MyFCMListenerService.removeNotificaitonFromStatusBar;
import static hardware.prytex.io.model.DMoatAlert.isDmoatOnline;

@SuppressWarnings("ALL")
public class OverflowMenus extends BaseActivity implements View.OnClickListener {
    private static ImageView backImage;
    private SharedPreferences.Editor editor;
    private static boolean isOnDashboardScreen = true;
    private static ImageView imgInfoBtn;
    private static ImageView imgHomeTab;
    private static ImageView imgPolicyTab;
    private static ImageView imgAccountTab;
    private static ImageView imgHelpTab;
    private static Button btnHomeDashboard;
    private static Button btnPolicyDashboard;
    private static Button btnAccountDashboard;
    private static Button btnHelpDashboard;
    private static LinearLayout llBottomTabs;

    @Override
    public int getLayoutId() {
        return R.layout.activity_dashboard;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imgInfoBtn.setVisibility(View.GONE);

    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public void initViews(Bundle savedInstanceState) {
        super.initViews(savedInstanceState);
//        Context mContext = OverflowMenus.this;

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();


        TextView mTitle = findViewById(R.id.txt_title);
        TextView tvProfileUserName = findViewById(R.id.tvProfileMenu);
//        tvProfileUserName.setText("Profile & Setting");
        backImage = findViewById(R.id.img_back);
        backImage.setVisibility(View.INVISIBLE);
        assert backImage != null;
        backImage.setOnClickListener(this);

        ImageView imgTopLogo = findViewById(R.id.img_logo);
        imgInfoBtn = findViewById(R.id.img_info);
        imgInfoBtn.setVisibility(View.GONE);

        //bottom bar buttons
        llBottomTabs = findViewById(R.id.llBottomBtns);
        btnHomeDashboard = findViewById(R.id.btnHomeDashbaord);
        btnPolicyDashboard = findViewById(R.id.btnPolicyDashboard);
        btnHelpDashboard = findViewById(R.id.btnHelpDashboard);
        btnAccountDashboard = findViewById(R.id.btnAccountDashboard);
        imgHomeTab = findViewById(R.id.img_tab_home);
        imgPolicyTab = findViewById(R.id.img_tab_policy);
        imgAccountTab = findViewById(R.id.img_tab_account);
        imgHelpTab = findViewById(R.id.img_tab_help);


        setTabBarSelection();
        findViewById(R.id.menu_HelpTutorial).setOnClickListener(v -> {

            isOnDashboardScreen = false;
            //call web service for initializin sensor
            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            addFragment(new HelpTutorialFragment(), true, true);

        });
        findViewById(R.id.menu_about).setOnClickListener(v -> {

            isOnDashboardScreen = false;
            //call web service for initializin sensor
            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
          //  addFragment(new AboutFragment(), true, true);
        });


        findViewById(R.id.menu_profile).setOnClickListener(v -> {
            isOnDashboardScreen = false;

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            addFragment(new ProfileFragment(), true, true);
        });

        findViewById(R.id.menu_logout).setOnClickListener(v -> {
            isOnDashboardScreen = false;
            new AlertDialog.Builder(OverflowMenus.this)
                    .setTitle("Signing Out")
                    .setMessage("Are you sure you want to sign out?")
                    .setPositiveButton("sign out", (dialog, whichButton) -> {
                        isDmoatOnline = true;
                        if (GeneralUtils.isConnected(OverflowMenus.this)) {
                            logout();
                        }
                    })
                    .setNegativeButton("cancel", null).show();
        });

        findViewById(R.id.menu_Faqs).setOnClickListener(v -> {
            isOnDashboardScreen = false;

            isOnDashboardScreen = false;
            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            addFragment(new FaqsFragment(), true, true);

        });

        findViewById(R.id.menu_open_src).setOnClickListener(v -> {
            isOnDashboardScreen = false;

            isOnDashboardScreen = false;
            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
          //  addFragment(new OpenSrcPackagesFragment(), true, true);

        });

        findViewById(R.id.btnHomeDashbaord).setOnClickListener(v -> {
//                MyToast.showMessage(getApplicationContext(), "go to dashboard");
            if (UserManager.checkPauseInternetTimeStatus()) {
                setBottomNavTextColors(getApplicationContext(), true, false, false, false);
                addFragment(new DashBoardFragment(), true, true);

            } else {
                showAlertMsgDialog(getApplicationContext(), getResources().getString(R.string.paused_internet_msg));
            }
        });

        findViewById(R.id.btnPolicyDashboard).setOnClickListener(v -> {
            if (UserManager.checkPauseInternetTimeStatus()) {
                setBottomNavTextColors(getApplicationContext(), false, true, false, false);
//                    addFragment(new PauseInternetFragment(), true, true);
                addFragment(new PoliciesMainFragment(), true, true);

            } else {
                showAlertMsgDialog(getApplicationContext(), getResources().getString(R.string.paused_internet_msg));
            }
        });

        findViewById(R.id.btnAccountDashboard).setOnClickListener(v -> {
            setBottomNavTextColors(getApplicationContext(), false, false, true, false);
            addFragment(new ProfileFragment(), true, true);
        });

        findViewById(R.id.btnHelpDashboard).setOnClickListener(v -> {
            setBottomNavTextColors(getApplicationContext(), false, false, false, true);
            isOnDashboardScreen = false;

        });


        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("alert")) {
            if (getCurrentFragment() != null && !(getCurrentFragment() instanceof ViewAlertsAdvancedFragment)) {
                Intent intent = new Intent(this, AlertsMainActivity.class);
                startActivity(intent);
            } else {
                Log.d("DashboardActivityPN", "getCurrentFragment() != null");
            }
        }

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.img_back) {

            AboutFragment.isFromAboutScreen = false;
            onBack();
        }else if(v.getId() == R.id.menu_container) {


            AboutFragment.isFromAboutScreen = false;

            if (isOnDashboardScreen) {
                backImage.setVisibility(View.GONE);
            } else {
                backImage.setVisibility(View.VISIBLE);
            }
        }


    }

    private static void setBottomNavTextColors(Context context, boolean isHome, boolean isPolicy, boolean isAccount, boolean isHelp) {
        try {
            btnHomeDashboard.setTextColor(context.getResources().getColor(R.color.unselected_text_color));
            btnPolicyDashboard.setTextColor(context.getResources().getColor(R.color.unselected_text_color));
            btnAccountDashboard.setTextColor(context.getResources().getColor(R.color.unselected_text_color));
            btnHelpDashboard.setTextColor(context.getResources().getColor(R.color.unselected_text_color));
            imgHomeTab.setImageResource(R.drawable.tab_home_unselected);
            imgPolicyTab.setImageResource(R.drawable.tab_policy_unselected);
            imgAccountTab.setImageResource(R.drawable.tab_account_unselected);
            imgHelpTab.setImageResource(R.drawable.tab_help_unselected);
            if (isHome) {
                imgHomeTab.setImageResource(R.drawable.tab_home_selected);
                btnHomeDashboard.setTextColor(context.getResources().getColor(R.color.selected_text_color));
            } else if (isPolicy) {
                imgPolicyTab.setImageResource(R.drawable.tab_policy_selected);
                btnPolicyDashboard.setTextColor(context.getResources().getColor(R.color.selected_text_color));
            } else if (isAccount) {
                imgAccountTab.setImageResource(R.drawable.tab_account_selected);
                btnAccountDashboard.setTextColor(context.getResources().getColor(R.color.selected_text_color));
            } else if (isHelp) {
                imgHelpTab.setImageResource(R.drawable.tab_help_selected);
                btnHelpDashboard.setTextColor(context.getResources().getColor(R.color.selected_text_color));
            } else {
            }
        } catch (Exception e) {
            Log.d("setNavBar", e.toString());
        }
    }

    private static void showAlertMsgDialog(Context ctx, String msg) {
        if (msg.equalsIgnoreCase("") || msg == null || msg.isEmpty() || msg.equals(null)) {
            msg = ctx.getResources().getString(R.string.error_message_generic);
        } else {
        }
        new AlertDialog.Builder(ctx)
                .setTitle("Prytex Hardware")
                .setMessage(msg)
                .setPositiveButton("Ok", (dialog, whichButton) -> {
                }).show();

    }
    private void setTabBarSelection() {
        Fragment fragmentInFrame = getSupportFragmentManager()
                .findFragmentById(R.id.fragment_container);

//        if (fragmentInFrame instanceof AlertsMainActivity) {
//            Log.d("debug", "you are in ViewAlertsAdvancedFragment");
//            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
//
//        } else
        if (fragmentInFrame instanceof DashBoardFragment) {
            Log.d("debug", "you are in ChooseFilterFragment");
            setBottomNavTextColors(getApplicationContext(), true, false, false, false);

        }
//        else if (fragmentInFrame instanceof ConnectedDeviceActivity) {
//            Log.d("debug", "you are in ChooseFilterFragment");
//            setBottomNavTextColors(getApplicationContext(), true, false, false, false);
//
//        }
//        else if (fragmentInFrame instanceof BandwidthChartMainActivity) {
//            Log.d("debug", "you are in ChooseFilterFragment");
//            setBottomNavTextColors(getApplicationContext(), true, false, false, false);
//
//        }
        else if (fragmentInFrame instanceof SensorMainFragment) {
            Log.d("debug", "you are in ChooseFilterFragment");
            setBottomNavTextColors(getApplicationContext(), true, false, false, false);

        }
//        else if (fragmentInFrame instanceof ConnectedDeviceActivity) {
//            Log.d("debug", "you are in ChooseFilterFragment");
//            setBottomNavTextColors(getApplicationContext(), true, false, false, false);
//
//        }
        else if (fragmentInFrame instanceof ChooseFilterFragment) {
            Log.d("debug", "you are in ChooseFilterFragment");
            setBottomNavTextColors(getApplicationContext(), false, false, false, false);

        } else if (fragmentInFrame instanceof BlockedAlertsFragment) {

            Log.d("debug", "you are in BlockedAlertsFragment");
            setBottomNavTextColors(getApplicationContext(), false, false, false, false);

        } else if (fragmentInFrame instanceof MutedAlertsFragment) {
            Log.d("debug", "you are in MutedAlertsFragment");

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
        }
//        else if (fragmentInFrame instanceof ProtocolChartFragment || fragmentInFrame instanceof BandwidthChartMainActivity || fragmentInFrame instanceof ProtocolsListViewMainFragment || fragmentInFrame instanceof ProtocolListViewFragment) {
//            Log.d("debug", "you are in ProtocolChartFragment");
//            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
//
//        }
        else if (fragmentInFrame instanceof ProtocolsListViewMainFragment) {

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in BandwidthHostDetailsFragment");

        } else if (fragmentInFrame instanceof BandwidthHostDetailsFragment) {

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in BandwidthHostDetailsFragment");

        } else if (fragmentInFrame instanceof SensorMainFragment) {

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in SensorMainFragment");

        }
//        else if (fragmentInFrame instanceof ConnectedDeviceActivity) {
//
//            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
//            Log.d("debug", "you are in ConnectedDeviceActivity");
//
//        }
        else if (fragmentInFrame instanceof VulnerabilityListFragment) {

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in VulnerabilityListFragment");

        } else if (fragmentInFrame instanceof VulnerabilityDetailsFragment) {

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in VulnerabilityDetailsFragment");

        } else if (fragmentInFrame instanceof VulnerabilityFragment) {

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in VulnerabilityFragment");
        }
//        else if (fragmentInFrame instanceof AboutFragment) {
//
//            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
//            Log.d("debug", "you are in AboutFragment");
//        }
        else if (fragmentInFrame instanceof FaqsFragment) {

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in FaqsFragment");
        } else if (fragmentInFrame instanceof ChangePasswordFragment) {
            llBottomTabs.setVisibility(View.GONE);
            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in ChangePasswordFragment");
        }
        //else if (fragmentInFrame instanceof TermsPolicyAgreementFragment) {
          //  llBottomTabs.setVisibility(View.GONE);
            //Log.d("debug", "you are in ChangePasswordFragment");
        //}
        else if (fragmentInFrame instanceof VerifyCodeFragment) {

            setBottomNavTextColors(getApplicationContext(), false, false, false, false);
            Log.d("debug", "you are in VerifyCodeFragment");
        } else if (fragmentInFrame instanceof PoliciesFragment || fragmentInFrame instanceof PauseInternetFragment) {
            setBottomNavTextColors(getApplicationContext(), false, true, false, false);
            Log.d("debug", "you are in PoliciesFragment");
        } else if (fragmentInFrame instanceof ProfileFragment) {
            setBottomNavTextColors(getApplicationContext(), false, false, true, false);
            Log.d("debug", "you are in PoliciesFragment");
        } else if (fragmentInFrame instanceof PolicyPlanFragment) {
            llBottomTabs.setVisibility(View.GONE);
            setBottomNavTextColors(getApplicationContext(), false, true, false, false);
            Log.d("debug", "you are in PoliciesFragment");
        } else {

        }
    }

    private void logout() {

        editor.putString("ShowDeviceDialog", "yes");
        editor.commit();
        DashBoardFragment.clearDashboardData();
        removeNotificaitonFromStatusBar(this, 0);

    }
}
