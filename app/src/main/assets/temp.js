function populatealerts(alert){
          $("#status").css("display", "None");
          alerts.push(alert);
          var aid = "Allow-" + String(i);
          var biid = "BlockInterval-" + String(i);
          var bfid = "BlockForever-" + String(i);
          var boid = "BlockOnce-" + String(i);
          var mid = "Mute-" + String(i);
          var uid = "Unblock-" + String(i);
          var kid = "Keepblock-" + String(i);
          //console.log(alert);
          if (alert.input_src == 'cnctdhost'){
            $("#alerts1").append("<li><div id=\"hostsalert\" <a class=\"ui-btn\" ><div><p style=\"white-space: pre-line; font-size: 1em;\"><br>Description: "+alert.description+"<br>Message: "+alert.msg+"<br>-"+alert.ip+" - "+alert.mac_address+"</p></div>\
                 <div style=\"display:inline\">\
                 <div style=\"display:inline-block\"><input name=\"Allow\" id=\""+aid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Allow\" onclick=\"ResponseToServer(this, document.getElementById('hostsalert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Mute\" id=\""+mid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Mute\" onclick=\"ResponseToServer(this, document.getElementById('hostsalert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Mute\" id=\""+boid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Block Once\" onclick=\"ResponseToServer(this, document.getElementById('hostsalert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Mute\" id=\""+bfid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Block Forever\" onclick=\"ResponseToServer(this, document.getElementById('hostsalert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Block\" id=\""+biid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Block for 1 min\" onclick=\"ResponseToServer(this, document.getElementById('hostsalert'))\">\
                 </div></div></a></div></li>");

          }
          else if (alert.input_src == 'ids'){
            if(alert.blocktype == 'post'){
              $("#alerts2").append("<li><div id=\"snortpostalert\" <a class=\"ui-btn\" ><div><p style=\"white-space: pre-line; font-size: 1em;\">Post-Block# "+ snort_post +"<br>Description: "+alert.description+"<br>Message: "+alert.msg+"<br>"+alert.src_ip+" - "+alert.src_port+" - "+alert.dest_ip+" - "+alert.dest_port+"</p></div>\
                 <div style=\"display:inline\">\
                 <div style=\"display:inline-block\"><input name=\"Allow\" id=\""+aid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Allow\" onclick=\"ResponseToServer(this, document.getElementById('snortpostalert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Mute\" id=\""+mid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Mute\" onclick=\"ResponseToServer(this, document.getElementById('snortpostalert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Mute\" id=\""+boid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Block Once\" onclick=\"ResponseToServer(this, document.getElementById('snortpostalert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Mute\" id=\""+bfid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Block Forever\" onclick=\"ResponseToServer(this, document.getElementById('snortpostalert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Block\" id=\""+biid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Block for 1 min\" onclick=\"ResponseToServer(this, document.getElementById('snortpostalert'))\">\
                 </div></div></a></div></li>");
              snort_post = snort_post + 1;
            }
            else if(alert.blocktype == 'pre'){
              $("#alerts2").append("<li><div id=\"snortprealert\" <a class=\"ui-btn\" ><div><p style=\"white-space: pre-line; font-size: 1em;\">Pre-Blocked# "+ snort_pre +" <br>Description: "+alert.description+"<br>"+alert.src_ip+" - "+alert.src_port+" - "+alert.dest_ip+" - "+alert.dest_port+"</p></div>\
                 <div style=\"display:inline\">\
                 <div style=\"display:inline-block\"><input name=\"Unblock\" id=\""+uid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Unblock\" onclick=\"ResponseToServer(this, document.getElementById('snortprealert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Mute\" id=\""+mid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Mute\" onclick=\"ResponseToServer(this, document.getElementById('snortprealert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Mute\" id=\""+boid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Block Once\" onclick=\"ResponseToServer(this, document.getElementById('snortprealert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Keepblock\" id=\""+kid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Keepblock\" onclick=\"ResponseToServer(this, document.getElementById('snortprealert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Block\" id=\""+biid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Block for 1 min\" onclick=\"ResponseToServer(this, document.getElementById('snortprealert'))\">\
                 </div></div></a></div></li>");
              snort_pre = snort_pre + 1;
           }
        }
        else if (alert.input_src == 'flow'){
            if(alert.blocktype == 'post'){
              $("#alerts1").append("<li><div id=\"ndpipostalert\" <a class=\"ui-btn\" ><div><p style=\"white-space: pre-line; font-size: 1em;\">Post-Block# "+ ndpi_post +"<br>Description: "+alert.description+"<br>"+alert.src_ip+" - "+alert.src_port+" - "+alert.dest_ip+" - "+alert.dest_port+"</p></div>\
                 <div style=\"display:inline\">\
                 <div style=\"display:inline-block\"><input name=\"Allow\" id=\""+aid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Allow\" onclick=\"ResponseToServer(this, document.getElementById('ndpipostalert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Mute\" id=\""+mid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Mute\" onclick=\"ResponseToServer(this, document.getElementById('ndpipostalert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Mute\" id=\""+boid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Block Once\" onclick=\"ResponseToServer(this, document.getElementById('ndpipostalert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Mute\" id=\""+bfid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Block Forever\" onclick=\"ResponseToServer(this, document.getElementById('ndpipostalert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Block\" id=\""+biid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Block for 1 min\" onclick=\"ResponseToServer(this, document.getElementById('ndpipostalert'))\">\
                 </div></div></a></div></li>");
              ndpi_post = ndpi_post + 1;
            }
            else if(alert.blocktype == 'pre'){
              $("#alerts1").append("<li><div id=\"ndpiprealert\" <a class=\"ui-btn\" ><div><p style=\"white-space: pre-line; font-size: 1em;\">Pre-Blocked# "+ ndpi_pre +" <br>Description: "+alert.description+"<br>Message: "+alert.msg+"<br>"+alert.src_ip+" - "+alert.src_port+" - "+alert.dest_ip+" - "+alert.dest_port+"</p></div>\
                 <div style=\"display:inline\">\
                 <div style=\"display:inline-block\"><input name=\"Unblock\" id=\""+uid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Unblock\" onclick=\"ResponseToServer(this, document.getElementById('ndpiprealert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Mute\" id=\""+mid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Mute\" onclick=\"ResponseToServer(this, document.getElementById('ndpiprealert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Mute\" id=\""+boid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Block Once\" onclick=\"ResponseToServer(this, document.getElementById('ndpiprealert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Keepblock\" id=\""+kid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Keepblock\" onclick=\"ResponseToServer(this, document.getElementById('ndpiprealert'))\">\
                 </div>\
                 <div style=\"display:inline-block\"><input name=\"Block\" id=\""+biid+"\" class=\"ui-btn\" type=\"button\" data-inline=\"true\" value=\"Block for 1 min\" onclick=\"ResponseToServer(this, document.getElementById('ndpiprealert'))\">\
                 </div></div></a></div></li>");
              ndpi_pre = ndpi_pre + 1;
           }
        }   else if(alert.category == "air_quality" ||alert.category == "humidity" ||alert.category == "temprature") {
           $("#alerts3").append("<li><div id=\"sensoralert\" <a class=\"ui-btn\" ><div><p style=\"white-space: pre-line; font-size: 1em;\"><br>Description: "+alert.desc+
            "<br>category: "+alert.category+
            " - time: "+alert.timestamp+
             " - reading: "+alert.reading+"</p></div></li>"
            );

        }


        i = i+1;
      }