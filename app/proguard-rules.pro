-optimizationpasses 5
-dontskipnonpubliclibraryclassmembers
-dontpreverify
-dump class_files.txt
-printseeds seeds.txt
-printusage unused.txt
-printmapping mapping.txt
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
-ignorewarnings
-keepattributes *Annotation*
-keep public class * extends java.lang.Exception
-keepattributes SourceFile,LineNumberTable
-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**

-dontwarn io.realm.**
-dontwarn com.github.mikephil.charting.**
-keep class com.github.mikephil.charting.** { *; }

-dontwarn com.zopim.android.sdk.**
-keep class com.zopim.android.sdk.** { *; }

#-dontwarn com.zopim.android.sdk.embeddable.** { *; }
#-keep class com.zopim.android.sdk.embeddable.** { *; }
#
#-dontwarn com.zopim.android.sdk.prechat.** { *; }
#-keep class com.zopim.android.sdk.prechat.** { *; }
#
#-dontwarn com.zopim.android.sdk.prechat.** { *; }
#-keep class com.zopim.android.sdk.prechat.** { *; }




-keep class com.loopj.android.http.** { *; }
-keep class com.parse.*{ *; }
-dontwarn com.parse.**
-dontwarn com.squareup.picasso.**
-keepclasseswithmembernames class * {
    native <methods>;
}


-keep class com.wdullaer.materialdatetimepicker.** { *; }
-dontwarn com.wdullaer.materialdatetimepicker.**

-dontwarn com.jaredrummler.**
-keep class com.jaredrummler.** { *; }

-keep class com.google.android.gms.** { *; }
-dontwarn com.google.android.gms.**
-keep class org.json.** { *; }
-dontwarn org.json.**

-keep class com.google.firebase.*.* { *; }
-keep class  com.broadstonetech.aegis.activity.* { *; }

-keep class com.google.zxing.integration.android.* { *; }
-keep class com.pubnub.api.* { *; }

-keepclassmembers class **.R$* {public static <fields>;}
-keep class **.R$*